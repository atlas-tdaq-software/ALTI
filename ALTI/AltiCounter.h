#ifndef _ALTICOUNTER_H_
#define _ALTICOUNTER_H_

//******************************************************************************
// file: AltiCounter.h
// desc: class for ALTI counter (scalars)
// auth: 04-DEC-2017 P. Kuzmanovic
//******************************************************************************

#include "ALTI/AltiModule.h"

namespace LVL1 {

// ALTI counter class
class AltiCounter {

  public:

    // public constants
    static const unsigned int   COUNTER_MASK            = 0x7fffffff;
    static const unsigned int   OVERFLOW_MASK           = 0x80000000;

    static const std::string CNT_NAME[AltiModule::CNT_SIZE];

    // constructor/destructor
    AltiCounter();
    AltiCounter(RCD::CMEMSegment *);
   ~AltiCounter();

    // public methods
   unsigned int &operator[](const unsigned int i)        { return(m_data[i]); }
   const unsigned int &operator[](const unsigned int i) const  { return(m_data[i]); }
   
    unsigned int size() const                           { return(AltiModule::CNT_SIZE); }
    unsigned int dsize() const                          { return(DSIZE); }
    RCD::CMEMSegment *getCmemSegment() const            { return(m_segment); };

    unsigned int counter(const unsigned int i) const    { return(m_data[i] & COUNTER_MASK); };
    bool overflow(const unsigned int i) const           { return(m_data[i] & OVERFLOW_MASK); };
    unsigned int l1a(const unsigned int i) const        { return(COUNTER_MASK); };
    bool l1aOverflow(const unsigned int i) const        { return(true); };
    unsigned int overflow() const;
    void turn(const unsigned int t)                     { m_turn = t; }
    unsigned int turn() const                           { return(m_turn & COUNTER_MASK); };
    bool turnOverflow() const                           { return(m_turn & OVERFLOW_MASK); };

    void dump() const;

    friend class AltiCounterRate;

  private:

    // private constants
    static const unsigned int   DSIZE;

    // private members
    RCD::CMEMSegment           *m_segment;
    unsigned int               *m_data;
    unsigned int                m_turn;

};      // class AltiCounter

//------------------------------------------------------------------------------

// ALTI counter class for rates (normalized to ORBIT)
class AltiCounterRate {

  public:

    // constructor/destructor
    AltiCounterRate();
    AltiCounterRate(const AltiCounter &);
    AltiCounterRate(const AltiCounterRate &);
    AltiCounterRate &operator=(const AltiCounter &);
    AltiCounterRate &operator=(const AltiCounterRate &);
   ~AltiCounterRate();

    // public methods
          double &operator[](const unsigned int i)              { return(m_data[i]); }
    const double &operator[](const unsigned int i) const        { return(m_data[i]); }
    AltiCounterRate operator/(const AltiCounterRate &) const;
    int scale(const double);
    int setFraction();

    std::string name() const                    { return(m_name); }
    void name(const std::string &n)             { m_name = n; }
    unsigned int size() const                   { return(m_size); }
    unsigned int size(const unsigned int);

    double counter(const unsigned int i) const  { return(m_data[i]); }
    double turn() const                         { return(m_turn); }
    void turn(const double t)                   { m_turn = t; }
    unsigned int fraction() const               { return(m_fraction); }
    unsigned int error() const                  { return(m_error); }
    unsigned int infinity() const               { return(m_infinity); }

    int check(const AltiCounterRate &);

    void dump() const;
    void dump(const AltiCounterRate &) const;
    void dumpSigma(const AltiCounterRate &) const;

  private:

    // private constant
    static const unsigned int   WORD_SIZE = 8*sizeof(uint32_t);
    static const double         EPSILON;

    // private members
    std::string                 m_name;
    unsigned int                m_size;
    double                     *m_data;
    double                      m_turn;
    int                         m_fraction;
    int                         m_error;
    int                         m_infinity;

    // private method
    int checkWord(const unsigned int, const unsigned int) const;
    void resetState();

};      // class AltiCounterRate

}       // namespace LVL1

#endif  // define _ALTICOUNTER_H_
