#ifndef _EVENTCOUNTER_H_
#define _EVENTCOUNTER_H_

//*****************************************************************************
// file: EventCounter.h
// desc: class for Level1 event counter
// auth: 12-JAN-2010 R. Spiwoks, taken on 15-OCT-2018 by P. Kuzmanovic
//*****************************************************************************

// $Id$

#include <iostream>

namespace LVL1 {

// Level1 event counter
class EventCounter {

  public:

    // public types

    // public constants

    // public constructor/destructor
    EventCounter(const unsigned int = 0x00000000);
    EventCounter(const std::string&, const unsigned int = 0x00000000);
    EventCounter(const EventCounter&);
   ~EventCounter();
    EventCounter& operator=(const EventCounter&);

    // public methods
    std::string  name() const                                   { return(m_name); };
    void         name(const std::string& n)                     { m_name = n; };
    unsigned int mask() const                                   { return(m_mask); };
    void         mask(const unsigned int);

    unsigned int data() const                                   { return(m_data & m_mask); };
    void         data(const unsigned int);
    void         data(const unsigned int, const double);
    double       time() const                                   { return(m_time); };
    void         time(const double t);
    double       rate() const;

    bool         empty() const                                  { return(m_data_num == 0); }
    bool         oneValue() const                               { return(m_data_num == 1); }
    bool         twoValues() const                              { return(m_data_num == 2); }

    void         reset(const unsigned int = 0x00000000);
    std::string  string() const;
    void         dump(std::ostream& = std::cout) const;

  private:

    // private constants
    static const unsigned int   MASK                            = 0xffffffff;

    // private members
    std::string                 m_name;
    unsigned int                m_mask;

    unsigned int                m_data;
    unsigned int                m_data_old;
    unsigned int                m_data_num;
    double                      m_time;
    double                      m_time_old;

};      // class EventCounter

}       // namespace LVL1

#endif  // _EVENTCOUNTER_H_

