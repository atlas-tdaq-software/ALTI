#ifndef _PATTERNGENERATOR
#define _PATTERNGENERATOR

//******************************************************************************
// file: PatternGenerator.h
// desc: test class for pattern generation by miniCTP
// auth: 08-NOV-2019 M. Saimpert
//******************************************************************************

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

class PatternGenerator {

  public:

    // public type definition
    struct LeakyBucket {
      // members
      unsigned int size=0;     // [BC]
      unsigned int content=0;  // [BC]
      unsigned int inv_rate=0; // [BC]
      unsigned int counter=0;  // [BC]
      unsigned int nbusy=0;    // [BC]
      // methods
      void reset();
    };

    struct Configuration {
      // members
      std::string output_file = "pg_alti.dat";
      float       rate          = 1;   // [Hz]
      int         smpl_deadtime = 4;   // [BC]
      int         seed          = -99; // random based on time by default
      bool        applyBCRVeto  = true; 
      bool        applyCalibReq = true; 
      bool        loopTTYP      = true;
      std::vector<LeakyBucket> bucket_vector;          // empty by default
      std::pair<int, int> sliding_window = {-99, -99}; // nl1a in window, turn off by default
      // methods
      int  config(int argc, char* argv[]);
      void print();
      void print_detail();
    };

    // public constructor/destructor
    PatternGenerator();
    ~PatternGenerator();

    // public methods
    void init(const Configuration&);
    void generate();
    void stat();
    void exit();

  private:

    // private type definition
    struct Command {
      // members
      unsigned int ORB=0;
      unsigned int BUSY=0;
      unsigned int CREQ[3]={0,0,0};
      std::string  TTYP=getTTYP(0);
      unsigned int BGO[4]={0,0,0,0};
      unsigned int TTR[3]={0,0,0};
      unsigned int L1A=0;
      unsigned int MULTIPLICITY=0;
      // methods
      void reset();
      std::string getWord();
      std::string getTTYP(unsigned int);
    };

    // private constants
    static const float lhc_rev_freq;         // kHz

    static const int   mem_size             = 1048575;
    static const int   line_orbit_sgn       = 3;
    static const int   line_l1a_sgn         = 3;

    static const int   nbunch_orbit         = 3564;
    static const int   nbcid_for_mem_reset  = 3;

    static const int   nbunch_orbit_sgn     = 1;
    static const int   nbunch_l1a_sgn       = 1;

    // private data members
    Configuration    m_config;

    std::ofstream    m_output_file;

    double           m_nl1a_orbit;	// l1a per orbit
    float            m_nlines_orbit;	// lines per orbit

    unsigned int     m_norbit_tot;	// total orbits
    unsigned int     m_nbcid_tot;	// total bcid
    unsigned int     m_nl1a_tot;	// total  l1a

    double           m_prob_l1a_bcid;   // prob l1a per bcid

    std::vector<unsigned int> m_bcid_l1a_tot_vector; // list of l1a (bef. deadtime)
    std::vector<unsigned int> m_bcid_l1a_act_vector; // list of l1a (aft. deadtime)

    unsigned int     m_nveto_bcr;       // nveto (BCR veto)
    unsigned int     m_nveto_calibreq;  // nveto (calib request)
    unsigned int     m_nbusy_smpl;      // nbusy (simple deadtime) 
    unsigned int     m_nbusy_sw;        // nbusy (sliding window deadtime)
    unsigned int     m_nbusy_cmplx;     // nbusy (complex deadtime)
    unsigned int     m_nbusy_tot;       // nbusy (total deadtime)

    double           m_nl1a_orbit_act;	// actual l1a per orbit (aft. deadtime)
    double           m_trig_rate_act;	// actual trigger rate (aft. deadtime)

    bool             m_is_sw;           // sw status (on/off)

    // private methods
    void computeParameters();

    void resetBuckets();
    void fillBuckets();
    void leakBuckets();
    bool fullBuckets();
    void printBuckets();

    void writeHeader();
    bool drawL1ASignal();
    void fillCommand(Command&);

};    // class PatternGenerator

#endif // _PATTERNGENERATOR
