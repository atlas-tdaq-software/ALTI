#ifndef _TESTALTIINITIAL_H_
#define _TESTALTIINITIAL_H_

//******************************************************************************
// file: TestAltiInitial.h
// desc: test class for ALTI initialization
// auth: 09-MAR-2018 P. Kuzmanovic
//*****************************************************************************

#include <string>
#include <tmgr/tmresult.h>

#include "ALTI/AltiModule.h"

namespace LVL1 {

  class TestAltiInitial {

  public:

    // public type definition
    class Configuration {
    public:
      Configuration();
      ~Configuration();
      void config(int, char*[]);
      void print() const;
      int check() const;
    public:
      bool                              module_reset;
      bool                              module_setup;
      bool                              module_check;
      bool                              module_config;
      std::string                       config_file;
      bool                              module_rconf;
      //////////////////////////////////////////////////
      std::string                       rconf_file;
      unsigned int                      module_slot;
      unsigned int                      module_base;
      bool                              module_setbase;
      //////////////////////////////////////////////////
    };


    // public constructor/destructor
    TestAltiInitial();
    ~TestAltiInitial();

    // public methods
    daq::tmgr::TestResult       result() const          { return(m_result); }
    daq::tmgr::TestResult       test(const Configuration& = Configuration());

  private:

    // private constants
    static const int            RESULT_LENGTH   =         80;
    static const int            VALUE_LENGTH    =         27;

    // private data members
    Configuration               m_cfg;
    AltiModule*                 m_alti;
    daq::tmgr::TestResult       m_result;

    // private methods
    daq::tmgr::TestResult init();
    daq::tmgr::TestResult exec();
    daq::tmgr::TestResult exit();

    // private print methods
    void printResult(const std::string&, daq::tmgr::TestResult);

  };    // class TestAltiInitial

}       // namespace LVL1

#endif  // _TESTALTIINITIAL_H_

