#ifndef _ALTIMINICTPCOUNTER_H_
#define _ALTIMINICTPCOUNTER_H_

//******************************************************************************
// file: AltiMiniCtpCounter.h
// desc: class for ALTI counter (scalars)
//******************************************************************************

#include "ALTI/AltiModule.h"

namespace LVL1 {

// ALTI mini-CTP counter class
class AltiMiniCtpCounter {

  public:

    // public type definition
    class Error {
    public:
      typedef enum {TBP, TAP, TAV, L1A} TYPE;
      static const unsigned int TYPE_NUMBER = L1A + 1;
      static const std::string  TYPE_NAME[TYPE_NUMBER];
      static const unsigned int TYPE_MASK[TYPE_NUMBER];

      Error(const int = 0);
     ~Error();

            int64_t &operator[](const TYPE typ)           { return(m_error[typ]); }
      const int64_t &operator[](const TYPE typ) const     { return(m_error[typ]); }

      void reset(const int = 0);
      int64_t error() const;
      void print(const Error &, const int) const;

      static std::string        printTypeMask(const unsigned int);

      friend class AltiMiniCtpCounterRate;

    private:
      int64_t                   m_error[TYPE_NUMBER];
      int64_t                   m_bit_error[TYPE_NUMBER];
    };

    // public constants
    static const unsigned int   COUNTER_MASK            = 0x7fffffff;
    static const unsigned int   OVERFLOW_MASK           = 0x80000000;

    static const unsigned int DSIZE[AltiModule::MINICTP_CNT_NUMBER];
    static const unsigned int LSIZE[AltiModule::MINICTP_CNT_NUMBER];

    // constructor/destructor
    AltiMiniCtpCounter(const AltiModule::MINICTP_CNT_TYPE);
    AltiMiniCtpCounter(RCD::CMEMSegment *, const AltiModule::MINICTP_CNT_TYPE);
   ~AltiMiniCtpCounter();

    // public methods
          unsigned int &operator[](const unsigned int i)        { return(m_data[i]); }
    const unsigned int &operator[](const unsigned int i) const  { return(m_data[i]); }

    AltiModule::MINICTP_CNT_TYPE type() const                   { return(m_type); }
    unsigned int size() const                           { return(LSIZE[m_type]); }
    unsigned int dsize() const                          { return(DSIZE[m_type]); }
    unsigned int lsize() const                          { return(LSIZE[m_type]); }
    RCD::CMEMSegment *getCmemSegment() const            { return(m_segment); };

    unsigned int counter(const unsigned int i) const    { return(m_data[i] & COUNTER_MASK); };
    bool overflow(const unsigned int i) const           { return(m_data[i] & OVERFLOW_MASK); };
    unsigned int l1a(const unsigned int i) const        { return(m_type == AltiModule::MINICTP_TAV ? m_data[dsize() + i] & COUNTER_MASK : COUNTER_MASK); };
    bool l1aOverflow(const unsigned int i) const        { return(m_type == AltiModule::MINICTP_TAV ? m_data[dsize() + i] & OVERFLOW_MASK : true); };
    unsigned int overflow() const;
    void turn(const unsigned int t)                     { m_turn = t; }
    unsigned int turn() const                           { return(m_turn & COUNTER_MASK); };
    bool turnOverflow() const                           { return(m_turn & OVERFLOW_MASK); };

    void dump() const;
    void dump(const AltiMiniCtpCounter &, const Error &) const;

    friend class AltiMiniCtpCounterRate;

  private:
 
    // private members
    RCD::CMEMSegment             *m_segment;
    unsigned int                 *m_data;
    AltiModule::MINICTP_CNT_TYPE  m_type;
    unsigned int                  m_turn;

};      // class AltiMiniCtpCounter

//------------------------------------------------------------------------------

// ALTI counter class for rates (normalized to ORBIT)
class AltiMiniCtpCounterRate {

  public:

    // constructor/destructor
    AltiMiniCtpCounterRate(const AltiModule::MINICTP_CNT_TYPE);
    AltiMiniCtpCounterRate(const AltiMiniCtpCounter &);
    AltiMiniCtpCounterRate(const AltiMiniCtpCounterRate &);
    AltiMiniCtpCounterRate &operator=(const AltiMiniCtpCounter &);
    AltiMiniCtpCounterRate &operator=(const AltiMiniCtpCounterRate &);
   ~AltiMiniCtpCounterRate();

    // public methods
          double &operator[](const unsigned int i)              { return(m_data[i]); }
    const double &operator[](const unsigned int i) const        { return(m_data[i]); }
    AltiMiniCtpCounterRate operator/(const AltiMiniCtpCounterRate &) const;
    int scale(const double);
    int setFraction();

    std::string name() const                    { return(m_name); }
    void name(const std::string &n)             { m_name = n; }
    AltiModule::MINICTP_CNT_TYPE type() const           { return(m_type); }
    unsigned int size() const                   { return(m_size); }
    unsigned int size(const unsigned int);

    double counter(const unsigned int i) const  { return(m_data[i]); }
    double turn() const                         { return(m_turn); }
    void turn(const double t)                   { m_turn = t; }
    unsigned int fraction() const               { return(m_fraction); }
    unsigned int error() const                  { return(m_error); }
    unsigned int infinity() const               { return(m_infinity); }

    int check(const AltiMiniCtpCounterRate &);
    int check(const AltiMiniCtpCounterRate &, const AltiMiniCtpCounter::Error &, AltiMiniCtpCounter::Error &);
    int check(const AltiMiniCtpCounterRate &, const AltiMiniCtpCounterRate &, const AltiMiniCtpCounter::Error &, AltiMiniCtpCounter::Error &);

    void dump() const;
    void dump(const AltiMiniCtpCounterRate &) const;
    void dump(const AltiMiniCtpCounterRate &, const AltiMiniCtpCounter::Error&) const;
    void dump(const AltiMiniCtpCounterRate &, const AltiMiniCtpCounterRate &, const AltiMiniCtpCounter::Error &) const;
    void dumpSigma(const AltiMiniCtpCounterRate &) const;

  private:

    // private constant
    static const unsigned int   WORD_SIZE = 8*sizeof(uint32_t);
    static const double         EPSILON;    

    // private members
    std::string                   m_name;
    unsigned int                  m_size;
    AltiModule::MINICTP_CNT_TYPE  m_type;
    double                       *m_data;
    double                        m_turn;
    int                           m_fraction;
    int                           m_error;
    int                           m_infinity;

    // private method
    int checkWord(const unsigned int, const unsigned int) const;
    void resetState();

};      // class AltiMiniCtpCounterRate

}       // namespace LVL1

#endif  // define _ALTICOUNTER_H_
