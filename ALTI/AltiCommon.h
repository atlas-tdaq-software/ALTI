#ifndef _ALTICOMMON_H_
#define _ALTICOMMON_H_

//******************************************************************************
// file: AltiCommon.h
// desc: pure static class for the ALTI common parameters
// auth: 05-DEC-2017 P. Kuzmanovic
//******************************************************************************

#include <string>
#include <vector>
#include <sys/types.h>
#include <cstdint>

//#include <map>
//#include <iostream>

//#include <tmgr/tmresult.h>

namespace LVL1 {

// ALTI common class
class AltiCommon {

  public:

    // public type definitions

    // public constructor & destructor

    // public methods
    static double       getTime();
    static std::string  getFrequency(const double);
    static std::string  getDataRate(const double);
    /*static std::string  getDataSize(const double);
    static std::string  getInterval(const double);
    static std::string  getTime(const double);
    static std::string  getTempFileName();
    static int          getCheckSum(const std::string&, u_int&);*/
    static int          printData(const uint32_t*, const int);
    static int          printData(std::vector<u_int>&, const int num = -1);
    static int          printData(const unsigned int[], const unsigned int, const int = 8, const int = 8, const int = 8);
    static int          printData(const bool[], const unsigned int, const int = 8, const int = 8);
    static int          readFile(const std::string&, const int, std::vector<u_int>&);
    static int          readFile(const std::string&, const int, u_int []);
    static int          readFile(const std::string&, const int, std::vector<u_int>&, int&);
    static int          readFile(const std::string&, const int, u_int [], int&);
    static int          readFile(const std::string&, std::vector<u_int>&);
    static int          readFile(std::ifstream&, const unsigned int, unsigned int[]);
    //////////////////////////////////////////////////////////////////////////////////
    static int          readFile(std::ifstream&, const unsigned int, unsigned int[], unsigned int[]); // 2 arrays
    //////////////////////////////////////////////////////////////////////////////////
    static int          writeFile(const std::string&, const int, std::vector<u_int>&);
    static int          writeFile(const std::string&, const int, u_int []);
    static int          writeFile(std::ofstream&, const unsigned int, const unsigned int[]);
    //////////////////////////////////////////////////////////////////////////////////
    static int          writeFile(std::ofstream&, const unsigned int, const unsigned int[], const unsigned int[]); // 2 arrays
    //////////////////////////////////////////////////////////////////////////////////
    static int          readNumber(const std::string&, int&);
    static int          readNumber(const std::string&, unsigned int&);
    static int          readNumber(const std::string&, double&);
    /*static int          checkRange(const int, const int, const int, const std::string&);*/
    static void         substr(const unsigned int[], const unsigned int, const unsigned int, std::vector<unsigned int>&);
    static void         substr(const unsigned int[], const unsigned int, const unsigned int, unsigned int[]);
    //static std::string  getError(const int);
    static std::string  centerString(const std::string&, const u_int, const char = ' ');
    static std::string  rightFillString(const std::string&, const u_int, const char = ' ');
    static std::string  leftFillString(const std::string&, const u_int, const char = ' ');
    static std::string  rightTrimString(const std::string&);
    static std::string  leftTrimString(const std::string&);
    static std::string  trimString(const std::string&);
    static std::string  printableString(const std::string&);
    static int          splitString(const std::string&, const std::string&, std::vector<std::string>&);
    /*static void         printResult(const std::string&, const u_int, const daq::tmgr::TestResult&);
    static std::string  programCall(int, char**);
    static int          readData(std::istream&, u_int&);
    static int          writeData(std::ostream&, const u_int);
    static std::string  INT2HEX(const u_int);
    static int          HEX2INT(const std::string&);
    static std::string  BOOL2HEX(const bool);
    static bool         HEX2BOOL(const std::string&);
    static bool         MioctValidSlot(const int);
    static int          MioctNumberToSlot(const int);
    static int          MioctSlotToNumber(const int);
    static int          OrbitWrap(const int, const int = ORBIT_LENGTH);
    static u_int        WindowWidth(const u_int wdw)     { return(2 * wdw + 1); }
    static std::string  FIFOFlagsShort(const bool, const bool, const bool, const bool);
    static std::string  FIFOFlagsLong(const bool, const bool, const bool, const bool);
    static void         pusage();
    static int          saffinity(unsigned int&);*/
    static void         parseRevision(const unsigned int, unsigned int &, unsigned int &, unsigned int &, unsigned int &);
    static float        enterFloat(const std::string &, const float, const float);
    static unsigned int getFirstSetBitPos(const int n);

    // public constants
    static const int            SUCCESS                 =   0;
    static const int            FAILURE                 =  -1;
    /*static const int            ERROR_VMEBUS            =  -2;
    static const int            ERROR_TIMEOUT           =  -3;*/
    static const int            ERROR_WRONGPAR          =  -4;
    static const int            ERROR_FILE              =  -5;
    //static const int            ERROR_RANGE             =  -6;
    static const int            ERROR_WRONGSCAN         =  -7;
    /*static const int            ERROR_NOMEMORY          =  -8;
    static const int            ERROR_NOEVENT           =  -9;
    static const int            ERROR_EMPTYEVENT        = -10;
    static const int            ERROR_NOHEADER          = -11;
    static const int            ERROR_WRONGHEADER       = -12;
    static const int            ERROR_WRONGTRAILER      = -13;
    static const int            ERROR_WRONGDATASIZE     = -14;
    static const int            ERROR_NUMBER            = 1 - ERROR_WRONGDATASIZE;*/

    // This is a hack to avoid out of bounds crashes when handling errors
    // Strategy is to make a functor class to wrap the access to the array
    // holding the errors. This substitutes the following line:
    //
    //static const std::string    ERROR_NAME[ERROR_NUMBER];

    /*static const std::string    ERROR_NAMEs[ERROR_NUMBER];

    struct ErrorIndexer {
        ErrorIndexer();
        virtual ~ErrorIndexer();

        inline const std::string &operator [] (size_t i) { return getError(i); }

        inline const std::string &getError(size_t i) {
            // If someone changes above error definitions this check is insufficient....
            if(i < static_cast<size_t>(ERROR_NUMBER)) { 
                return knownErrors[i];
            } else if (improErrors.find(i) != improErrors.end()) {
                return improErrors[i];
            }
            improErrors[i] = "ERROR" + std::to_string(i);
            return improErrors[i];
        }
        
        protected:

        std::map<size_t, std::string> improErrors;
        const std::string * knownErrors;
    };
    static ErrorIndexer         ERROR_NAME;*/

    static const int            STRING_LENGTH           = 1024;
    static const unsigned int   WORD_SIZE               =   32;

    static const int            ORBIT_LENGTH            = 3564;
    static const double         BC_FREQUENCY;
    static const int            NANOSECONDS             = 1000000000;

    /*static const int            MIOCT_MIN_SLOT_LO       =  4;
    static const int            MIOCT_MAX_SLOT_LO       = 11;
    static const int            MIOCT_MIN_SLOT_HI       = 14;
    static const int            MIOCT_MAX_SLOT_HI       = 21;
    static const int            MIOCT_NUMBER_LO         = MIOCT_MAX_SLOT_LO - MIOCT_MIN_SLOT_LO + 1;
    static const int            MIOCT_NUMBER_HI         = MIOCT_MAX_SLOT_HI - MIOCT_MIN_SLOT_HI + 1;
    static const int            MIOCT_NUMBER            = MIOCT_NUMBER_LO + MIOCT_NUMBER_HI;

    static const int            MEMORY_DATA_OFFSET      = 12;
    static const int            SECTOR_BCID_OFFSET      =  2;
    static const int            MULTIPLICITY_OFFSET     =  5;
    static const int            TRIGGERWORD_OFFSET      =  8;*/

  private:

     static const unsigned      SUBSTR_MASK[WORD_SIZE+1];

};      // class AltiCommon

}       // namespace LVL1

#endif  // define _ALTICOMMON_H_
