#ifndef _ALTICONFIGURATION_H_
#define _ALTICONFIGURATION_H_

//******************************************************************************
// file: AltiConfiguration.h
// desc: configuration for the ALTI module
// auth: 28-NOV-2017 P. Kuzmanovic
//*****************************************************************************

#include "ALTI/AltiModule.h"
      
#include <string>
#include <iostream>

namespace LVL1 {
  
  class AltiConfiguration {
    
  public:
    
    // type definition 
    class BucketConfiguration {
    public:
      static const unsigned int VALUE_NUMBER = 3;
      BucketConfiguration() { rate = 0; level = 0; enable = false; }
      ~BucketConfiguration() {}
      std::string print() const;
    public:
      unsigned int            rate;
      unsigned int            level;
      bool                    enable;
    };
    class SlidingWindowConfiguration {
    public:
      static const unsigned int VALUE_NUMBER = 3;
      SlidingWindowConfiguration() { window = 0; number = 0; enable = false; }
      ~SlidingWindowConfiguration() {}
      std::string print() const;
    public:
      unsigned int            window;
      unsigned int            number;
      bool                    enable;
    };
    
    typedef AltiModule::ECR_GENERATION ECR_GENERATION;
    
    // public constant
    static const unsigned int   STRING_LENGTH           = 1024;
    
    // constructor and destructor
    AltiConfiguration();
    AltiConfiguration(const std::string &);
   ~AltiConfiguration();
    AltiConfiguration &operator=(const AltiConfiguration &);

    // methods
    void clearFileNames();
    void setDefaultFileNames();
    void addFileNamePrefix(const std::string &);
    void config(const bool);
    void print(std::ostream & = std::cout) const;
    int read(const std::string &);
    int write(const std::string &) const;
  
    void readSWXIndividualToBundleTTYP();

  public:

    bool                                          clk_config;
    AltiModule::CLK_PLL_TYPE                      clk_select;
    AltiModule::CLK_JC_TYPE                       clk_jc_select;
    std::string                                   clk_jc_file;
    int                                           clk_phase_shift;

    bool                                          sig_config;
    bool                                          sig_fp_bgo2enable;
    bool                                          sig_fp_bgo3enable;
    AltiModule::SIGNAL_SOURCE                     sig_swx[AltiModule::SIGNAL_NUMBER_ROUTED][AltiModule::SIGNAL_DESTINATION_NUMBER];
    AltiModule::SIGNAL_SOURCE                     sig_swx_ttyp[AltiModule::SIGNAL_DESTINATION_NUMBER];
  
    bool                                          sig_eqz_ctp_in_enable;
    bool                                          sig_eqz_alti_in_enable;
    AltiModule::EQUALIZER_CONFIG                  sig_eqz_ctp_in_mode;
    AltiModule::EQUALIZER_CONFIG                  sig_eqz_alti_in_mode;
   
    std::vector<unsigned int>                     sig_io_sync_phase;
    std::vector<unsigned int>                     sig_io_shaping;

    // source of the output multiplexer
    std::string                                   sig_orb_source;
    std::string                                   sig_l1a_source;
    std::string                                   sig_ttr_source[3];
    std::string                                   sig_bgo_source[4];
    std::string                                   sig_ttyp_source;
    
    // Orbit input selector
    std::string                                   sig_orb_input_source;

    unsigned int                                  turn_cnt_mask;
    unsigned int                                  turn_cnt_max;

    unsigned int                                  bgo2_l1a_delay;

    bool                                          bsy_config;
    std::vector<bool>                             bsy_in_select;
    std::vector<AltiModule::BUSY_SOURCE>          bsy_out_select;
    bool                                          bsy_data_vme;
    bool                                          bsy_l1a_masked;
    std::vector<bool>                             bsy_ttr_masked;
    std::vector<bool>                             bsy_bgo_masked;
    AltiModule::BUSY_LEVEL                        bsy_level;

    bool                                          crq_config;
    std::vector<AltiModule::CALREQ_INPUT>         crq_in_select;
    std::vector<AltiModule::CALREQ_SOURCE>        crq_out_select[AltiModule::CALREQ_OUTPUT_NUMBER];
    std::vector<bool>                             crq_data_vme;

    bool                                          mem_config;
    bool                                          mem_pg_enable;
    bool                                          mem_pg_repeat;
    unsigned int                                  mem_pg_start_addr;
    unsigned int                                  mem_pg_stop_addr;
    std::string                                   mem_pg_file;
    std::vector<bool>                             mem_snap_masked;

    bool                                          ttc_config;
    bool                                          ttc_tx_enable[AltiModule::TRANSMITTER_NUMBER];
    unsigned int                                  ttc_tx_delay[AltiModule::TRANSMITTER_NUMBER];
    std::vector<bool>                             ttc_l1a_source;  
    std::string                                   ttc_orb_source;  
    std::vector<std::string>                      ttc_bgo_source; 
    std::string                                   ttc_ttyp_source;   
    std::vector<unsigned int>                     ttc_bgo_inhibit_width; 
    std::vector<unsigned int>                     ttc_bgo_inhibit_delay;
    unsigned int                                  ttc_ttyp_delay;
    unsigned int                                  ttc_ttyp_addr;
    unsigned int                                  ttc_ttyp_subaddr;
    AltiModule::TTC_ADDRESS_SPACE                 ttc_ttyp_address_space;
    std::vector<AltiModule::TTC_BGO_MODE>         ttc_bgo_mode;
    std::vector<bool>                             ttc_fifo_retransmit;
    std::vector<unsigned int>                     ttc_fifo_word;

    bool                                          cnt_config;
    bool                                          cnt_enable;
    std::string                                   cnt_ttyp_lut_file;
    std::vector<unsigned int>                     cnt_ttyp_lut;

    bool                                          pbm_config;
    unsigned int                                  pbm_bcid_offset;

    // mini-CTP
    bool                                          ctp_config;
    ECR_GENERATION                                ctp_ecr_generation;

    std::string                                   ctp_l1a_source;
    std::string                                   ctp_bgo2_source;
    std::string                                   ctp_bgo3_source;
    std::string                                   ctp_ttr_source[3];
    std::string                                   ctp_crq_source[3];
    
    std::string                                   ctp_item_lut_file;
    std::vector<unsigned int>                     ctp_item_lut;

    std::string                                   ctp_ttyp_lut_file;
    std::vector<unsigned int>                     ctp_ttyp_lut;

    std::vector<unsigned int>                     ctp_rndm_seed;
    bool                                          ctp_rndm_seed_valid;
    std::vector<unsigned int>                     ctp_rndm_threshold;

    std::string                                   ctp_bgrp_file;
    std::vector<unsigned int>                     ctp_bgrp;
    unsigned int                                  ctp_bgrp_bcid_offset;
    std::string                                   ctp_bgrp_mask_file;
    std::vector<unsigned int>                     ctp_bgrp_mask;

    bool                                          ctp_prsc_config;
    std::string                                   ctp_prsc_seed_file;
    std::vector<unsigned int>                     ctp_prsc_seed;
    bool                                          ctp_prsc_seed_valid;
    std::string                                   ctp_prsc_threshold_file;
    std::vector<unsigned int>                     ctp_prsc_threshold;

    std::string                                   ctp_tav_enable_file;
    unsigned int                                  ctp_tav_enable;

    unsigned int                                  ctp_smpl_deadtime;
    BucketConfiguration                           ctp_cplx_bucket[AltiModule::LEAKY_BUCKET_NUMBER];
    SlidingWindowConfiguration                    ctp_cplx_sliding_window;

    // private method;
 private:

    // private method
    int readNextRecord(std::istream&, std::string&, std::string&);

    // private utility methods
    static std::string bool2string(const bool);

};      // class AltiConfiguration

}       // namespace LVL1

#endif  // _ALTICONFIGURATION_H_
