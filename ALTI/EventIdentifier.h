#ifndef _EVENTIDENTIFIER_H_
#define _EVENTIDENTIFIER_H_

//*****************************************************************************
// file: EventIdentifier.h
// desc: class for Level1 event identifier
// auth: 10-AUG-2009 R. Spiwoks, taken on 15-OCT-2018 by P. Kuzmanovic
//*****************************************************************************

// $Id$

#include <iostream>
#include <cstdint>

namespace LVL1 {

// Level1 event identifier
class EventIdentifier {

  public:

    // public types

    // public constants
    static const uint32_t       ECRC_MAX;
    static const uint32_t       L1ID_MAX;

    // public constructor/destructor
    EventIdentifier(const uint32_t = L1ID_MAX);
    EventIdentifier(const EventIdentifier&);
   ~EventIdentifier();
    EventIdentifier& operator=(const EventIdentifier&);

    EventIdentifier& operator=(const uint32_t);
                              operator uint32_t() const;

    // public methods
    uint32_t  data() const                      { return(*m_data); };
    void      data(const uint32_t data)         { *m_data = data; };
    uint32_t* addr() const                      { return(m_data); };
    void      addr(uint32_t*);

    uint32_t ECRC() const;
    void ECRC(const uint32_t);

    uint32_t L1ID() const;
    void L1ID(const uint32_t);

    void ECR()                                  { ECRC(ECRC() + 1); L1ID(L1ID_MASK); }
    void L1A()                                  { L1ID(L1ID() + 1); }
    void L1A(const uint32_t l)                  { L1ID(L1ID() + l); }

    void reset()                                { *m_data = L1ID_MASK; };
    int read(std::istream& = std::cin);
    int write(std::ostream& = std::cout);

    void dump(std::ostream& = std::cout) const;
    void dumpLine(std::ostream& = std::cout) const;
    static void dumpHeader(std::ostream& = std::cout);

  private:

    // private constants
    static const uint32_t ECRC_MASK             = 0xff000000;
    static const uint32_t ECRC_SHFT             =         24;
    static const uint32_t L1ID_MASK             = 0x00ffffff;

    static const int            STRING_LENGTH           = 1024;
    static const int            SUCCESS                 =   0;
    static const int            FAILURE                 =  -1;
    static const int            ERROR_VMEBUS            =  -2;
    static const int            ERROR_TIMEOUT           =  -3;
    static const int            ERROR_WRONGPAR          =  -4;
    static const int            ERROR_FILE              =  -5;
    static const int            ERROR_RANGE             =  -6;
    static const int            ERROR_WRONGSCAN         =  -7;
    static const int            ERROR_NOMEMORY          =  -8;
    static const int            ERROR_NOEVENT           =  -9;
    static const int            ERROR_EMPTYEVENT        = -10;
    static const int            ERROR_NOHEADER          = -11;
    static const int            ERROR_WRONGHEADER       = -12;
    static const int            ERROR_WRONGTRAILER      = -13;
    static const int            ERROR_WRONGDATASIZE     = -14;
    static const int            ERROR_NUMBER            = 1 - ERROR_WRONGDATASIZE;


    // private members
    uint32_t*                   m_data;
    bool                        m_data_flag;

};      // class EventIdentifier

}       // namespace LVL1

#endif  // _EVENTIDENTIFIER_H_

