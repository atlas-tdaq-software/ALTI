#ifndef _ALTIBUSYCOUNTER_H_
#define _ALTIBUSYCOUNTER_H_

//******************************************************************************
// file: AltiBusyCounter.h
// desc: class for ALTI busy counter
// auth: 10-NOV-2014 R. Spiwoks, from MirodCounter.h
// modified for ALTI: 17-MAY-2019 F. Lucca
//******************************************************************************

#include <string>

#include "RCDVme/RCDCmemSegment.h" //?
#include "ALTI/AltiModule.h"

namespace LVL1 {


class AltiBusyCounter {

  public:

    // public type definition
    typedef enum {CTP, ALTI, FP, LOCAL, PG, VME, ECR, INTERVAL} BUSY;
    static const unsigned int   BUSY_NUMBER             = INTERVAL + 1;
    static const std::string    BUSY_NAME[BUSY_NUMBER];
//    static const BUSY           BUSY_FIFO[AltiModule::RDT_FIFO_NUMBER];
//    static const BUSY           BUSY_LINK[AltiModule::RDT_SLINK_NUMBER];

    // public constant
    static const unsigned int   SIZE                   = ALTI::BSY_BUSYFIFO_INDEX_NUMBER;
    static const unsigned int   BSY_CNT_MASK            = ALTI::MASK_BSY_BUSYFIFO;
    static const unsigned int   BSY_SEL_MASK            = ALTI::MASK_BSY_WRITECOUNTER;

    // constructor/destructor
    AltiBusyCounter();
    AltiBusyCounter(RCD::CMEMSegment*);
    AltiBusyCounter(const AltiBusyCounter&);
    AltiBusyCounter& operator=(const AltiBusyCounter&);
   ~AltiBusyCounter();

    // friend declaration
    friend class AltiModule;

    // public methods
    uint32_t  operator[](int i) const                   { return(m_data[i]&BSY_CNT_MASK); }
    uint32_t& operator[](int i)                         { return(m_data[i]); }
    uint32_t* addr() const                              { return(m_data); }

    RCD::CMEMSegment* segment() const                   { return(m_cseg); }
    void segment(RCD::CMEMSegment*);
    unsigned int size() const                           { return(selectSize()*m_values); }
    bool autoMode() const                               { return(m_wmod); }
    void autoMode(const bool a)                         { m_wmod = a; }
    bool firstMode() const                              { return(m_rmod); }
    void firstMode(const bool f)                        { m_rmod = f; }
    void select(const BUSY b, const bool m)             { m_select[static_cast<int>(b)] = m; }
    bool select(const BUSY b) const                     { return(m_select[static_cast<int>(b)]); }
    void selectAll(const unsigned int m)                { for(unsigned int b=0; b<BUSY_NUMBER; b++) m_select[b]=(m&(0x00000001<<b)); }
    void selectAll(const bool m)                        { for(unsigned int b=0; b<BUSY_NUMBER; b++) m_select[b]=m; }
    unsigned int selectSize() const;
    unsigned int interval() const                       { return(m_interval&BSY_CNT_MASK); }
    void interval(const unsigned int i)                 { m_interval = i&BSY_CNT_MASK; }
    unsigned int values() const                         { return(m_values); }
    void values(const unsigned int s)                   { m_values = s; }
    bool overflow() const                               { return(m_overflow); }
    void overflow(const bool o)                         { m_overflow = o; }

    void resetCTRL();
    void resetDATA();
    void setStatus(const unsigned int);
    void dump() const;
    void selectDump() const;
    std::string selectPrint() const;
    bool check() const;

  private:

    // private members
    uint32_t*                   m_data;
    RCD::CMEMSegment*           m_cseg;
    bool                        m_wmod;
    bool                        m_rmod;
    bool                        m_select[BUSY_NUMBER];
    unsigned int                m_interval;
    unsigned int                m_values;
    bool                        m_overflow;
    bool                        m_status;

    // private method
    void dumpStatus() const;

};      // class AltiBusyCounter

//------------------------------------------------------------------------------

// Alti busy counter rate class (normalized to interval time)
class AltiBusyCounterRate {

  public:

    // public constant
    static const unsigned int   SIZE    = AltiBusyCounter::SIZE;

    // constructor/destructor
    AltiBusyCounterRate();
    AltiBusyCounterRate(const AltiBusyCounter&);
   ~AltiBusyCounterRate();

    // friend declaration
    friend class AltiModule;

    // public operators
    AltiBusyCounterRate& operator=(const AltiBusyCounter&);
    AltiBusyCounterRate& operator=(const AltiBusyCounterRate&);
    AltiBusyCounterRate& operator+(const AltiBusyCounter&);
    AltiBusyCounterRate& operator+(const AltiBusyCounterRate&);
    AltiBusyCounterRate& operator+=(const AltiBusyCounter& cnt)     { return(*this + cnt); }
    AltiBusyCounterRate& operator+=(const AltiBusyCounterRate& rte) { return(*this + rte); }
    double                operator[](int i) const                       { return(m_data[i]); }
    double&               operator[](int i)                             { return(m_data[i]); }

    // public methods
    bool autoMode() const                                               { return(m_wmod); }
    void autoMode(const bool a)                                         { m_wmod = a; }
    bool firstMode() const                                              { return(m_rmod); }
    void firstMode(const bool f)                                        { m_rmod = f; }
    void select(const AltiBusyCounter::BUSY b, const bool m)     { m_select[static_cast<int>(b)] = m; }
    bool select(const AltiBusyCounter::BUSY b) const             { return(m_select[static_cast<int>(b)]); }
    void selectAll(const unsigned int m)                                { for(unsigned int b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) m_select[b]=(m&(0x00000001<<b)); }
    void selectAll(const bool m)                                        { for(unsigned int b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) m_select[b]=m; }
    unsigned int selectSize() const;
    double interval() const                                             { return(m_interval); }
    void interval(const double i)                                       { m_interval = i; }
    int values() const                                                  { return(m_values); }
    void values(const int v)                                            { m_values = v; }
    bool overflow() const                                               { return(m_overflow); }
    void overflow(const bool o)                                         { m_overflow = o; }

    void reset();
    void scale(double);
    void normalize();
    void average();
    void dump() const;
    void dumpBusy() const;
    void selectDump() const;

    // public static method
    static std::string getTime(const double);

  private:

    // private type definition
    typedef enum {UNKNOWN, DISABLED, ENABLED} CTRL;
    static const unsigned int   CTRL_NUMBER             = ENABLED + 1;
    static const std::string    CTRL_NAME[CTRL_NUMBER];

    // private members
    double*                     m_data;
    double*                     m_dmin;
    double*                     m_dmax;
    bool                        m_wmod;
    bool                        m_rmod;
    bool                        m_select[AltiBusyCounter::BUSY_NUMBER];
    double                      m_interval;
    unsigned int                m_values;
    bool                        m_overflow;
    bool                        m_average;
    // CTRL                        m_busy_ondemand;
    // CTRL                        m_fmt_master;
    // CTRL                        m_fmt_gps_enable;
    // CTRL                        m_fmt_gps_status;
    // CTRL                        m_fifo_ctrl[AltiModule::RDT_FIFO_NUMBER];
    // CTRL                        m_busy_ctrl[AltiModule::RDT_FIFO_NUMBER];

    // private method
    void average(const AltiBusyCounterRate&);
    void dumpAverage() const;

};      // class AltiBusyCounterRate

}       // namespace LVL1

#endif  // _AltiBUSYCOUNTER_H_
