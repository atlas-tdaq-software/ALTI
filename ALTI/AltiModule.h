#ifndef _ALTIMODULE_H_
#define _ALTIMODULE_H_

//******************************************************************************
// file: AltiModule.h
// desc: class for the ALTI Module
// auth: 24-OCT-2017 P. Kuzmanovic
//******************************************************************************

#include "ALTI/ALTI.h"
#include "ALTI/ALTI_BITSTRING.h"
#include "I2C/I2C.h"
#include "I2C/I2CNetwork.h"
#include "ALTI/EventIdentifier.h"
#include "ALTI/AltiBusyCounter.h"

#include "DS1WM/DS1WM.h"

// friend menu classes declaration
class AltiSIGEqualizersConfigRead;
class AltiSIGEqualizersConfigWrite;
class AltiENCBgoCommandModeRead;
class AltiENCBgoCommandModeWrite;

namespace LVL1 {

// forward class declaration
class AltiConfiguration;
class AltiCounter;
class AltiMiniCtpCounter;
class AltiBusyCounter;
class AltiBusyCounterRate;

// ALTI class
class AltiModule {

  public:
    // public type definition
    class DataSegment {
      public:
        DataSegment(const std::string &, const unsigned int);
       ~DataSegment();
              unsigned int &operator[](const unsigned int i)            { return (m_data[i]); }
        const unsigned int &operator[](const unsigned int i) const      { return (m_data[i]); }
                       int  operator()() const                          { return ((m_segment) ? SUCCESS : FAILURE); }
        std::string name() const                                        { return (m_name); }
        RCD::CMEMSegment *segment() const                               { return (m_segment); }
        unsigned int *data() const                                      { return (m_data); }
      private:
        std::string             m_name;
        RCD::CMEMSegment       *m_segment;
        unsigned int           *m_data;
    };

    // public constructor & destructor
    AltiModule(unsigned int/*, unsigned int = 0x08000000*/);
   ~AltiModule();

    static int setBaseAddress(unsigned int, unsigned int);

    // public members
    //static RCD::VME*                            m_vme;

    int operator()() { return (m_status); }

    static const int SUCCESS = 0;
    static const int FAILURE = -1;
    static const int WRONGPAR = -2;
    static const int HIST_TIMEOUT_EXCEEDED = -3;
    static const int ALTI_RAM_MEMORY_BUSY = -4; // error when you try to read/write from the PAT/SNP/DEC memory while it's running
    static const int TIMEOUT = -5;
    
    static const unsigned int WORD_SIZE = 8*sizeof(uint32_t);

    static const unsigned int MAX_MEMORY = 0x00100000; // 1M words is enough for CMEM segment

    // public types
    typedef enum {BC, ORB, L1A, TTR1, TTR2, TTR3, TTYP0, TTYP1, TTYP2, TTYP3, TTYP4, TTYP5, TTYP6, TTYP7, BGO0, BGO1, BGO2, BGO3, BUSY, CALREQ0, CALREQ1, CALREQ2} SIGNAL;
    static const unsigned int SIGNAL_NUMBER = CALREQ2 + 1;
    static const unsigned int SIGNAL_NUMBER_ROUTED = SIGNAL_NUMBER - 4; // all except BUSY & CALREQ0..2
    static const std::string SIGNAL_NAME[SIGNAL_NUMBER];

    typedef enum {CTP_IN, ALTI_IN, NIM_IN, FROM_FPGA, INVALID_CONFIG} SIGNAL_SOURCE;
    static const unsigned int SIGNAL_SOURCE_NUMBER = INVALID_CONFIG + 1;
    static const std::string SIGNAL_SOURCE_NAME[SIGNAL_SOURCE_NUMBER];

    typedef enum {CTP_OUT, ALTI_OUT, NIM_OUT, TO_FPGA} SIGNAL_DESTINATION;
    static const unsigned int SIGNAL_DESTINATION_NUMBER = TO_FPGA + 1;
    static const std::string SIGNAL_DESTINATION_NAME[SIGNAL_DESTINATION_NUMBER];

    typedef enum {MASTER, CTP_SLAVE, ALTI_SLAVE, LEMO_SLAVE} ALTI_MODE;
    static const unsigned int ALTI_MODE_NUMBER = LEMO_SLAVE + 1;
    static const std::string ALTI_MODE_NAME[ALTI_MODE_NUMBER];

    typedef enum {V_PEAK, V_POLE, V_GAIN, V_OFFSET} EQUALIZER_VOLTAGE;
    static const unsigned int EQUALIZER_VOLTAGE_NUMBER = V_OFFSET + 1;
    static const std::string EQUALIZER_VOLTAGE_NAME[EQUALIZER_VOLTAGE_NUMBER];

    typedef enum {SHORT_CABLE, LONG_CABLE, CUSTOM_CONFIG} EQUALIZER_CONFIG;
    static const unsigned int EQUALIZER_CONFIG_NUMBER = CUSTOM_CONFIG + 1;
    static const std::string EQUALIZER_CONFIG_NAME[EQUALIZER_CONFIG_NUMBER];
    
    typedef enum {JITTER_CLEANER, FROM_SWITCH} CLK_PLL_TYPE;
    static const unsigned int CLK_NUMBER = FROM_SWITCH + 1;
    static const std::string CLK_PLL_NAME[CLK_NUMBER];
    
    typedef enum {OSCILLATOR, SWITCH} CLK_JC_TYPE;
    static const std::string CLK_JC_NAME[CLK_NUMBER];
    
    typedef enum {EXT_JC, EXT_CTP_IN, EXT_ALTI_IN, EXT_NIM_IN} EXT_CLK_SOURCE;
    static const unsigned int EXT_CLK_SOURCE_NUMBER = EXT_NIM_IN + 1;
    static const std::string EXT_CLK_SOURCE_NAME[EXT_CLK_SOURCE_NUMBER];

    typedef enum {CNT_BC, CNT_ORB, CNT_L1A, CNT_TTR1, CNT_TTR2, CNT_TTR3, CNT_BGO0, CNT_BGO1, CNT_BGO2, CNT_BGO3} CNT_TYPE;
    static const unsigned int CNT_NUMBER = CNT_BGO3 + 1;
    static const std::string CNT_NAME[CNT_NUMBER];
    static const unsigned int CNT_SIZE = 17; // (8 TTYP + 9 "regular")

    typedef enum {ALTI_LINE, LTP_LINE} SNAPSHOT_TYPE;
    static const unsigned int SNAPSHOT_TYPE_NUMBER = LTP_LINE + 1;
    static const std::string SNAPSHOT_TYPE_NAME[SNAPSHOT_TYPE_NUMBER];

    // busy enums
    static const unsigned int   BSY_FIFO_SIZE           = ALTI::BSY_BUSYFIFO_INDEX_NUMBER;
    static const unsigned int   BSY_FIFO_MASK           = ALTI::BSY_BUSYFIFO_INDEX_NUMBER - 1;
    typedef enum {BUSY_FROM_FRONT_PANEL, BUSY_FROM_CTP, BUSY_FROM_ALTI, BUSY_FROM_PG, BUSY_FROM_VME, BUSY_FROM_ECR} BUSY_INPUT;
    static const unsigned int BUSY_INPUT_NUMBER = BUSY_FROM_ECR + 1;
    static const std::string BUSY_INPUT_NAME[BUSY_INPUT_NUMBER];

    typedef enum {BUSY_INACTIVE, BUSY_LOCAL, BUSY_CTP, BUSY_ALTI} BUSY_SOURCE;
    static const unsigned int BUSY_SOURCE_NUMBER = BUSY_ALTI + 1;
    static const std::string BUSY_SOURCE_NAME[BUSY_SOURCE_NUMBER];

    typedef enum {BUSY_TO_CTP, BUSY_TO_ALTI} BUSY_OUTPUT;
    static const unsigned int BUSY_OUTPUT_NUMBER = BUSY_TO_ALTI + 1;
    static const std::string BUSY_OUTPUT_NAME[BUSY_OUTPUT_NUMBER];

    typedef enum {NIM, TTL} BUSY_LEVEL;
    static const unsigned int BUSY_LEVEL_NUMBER = TTL + 1;
    static const std::string BUSY_LEVEL_NAME[BUSY_LEVEL_NUMBER];

    // calibration requests enums
    typedef enum {CALREQ_FROM_RJ45, CALREQ_FROM_FRONT_PANEL, CALREQ_FROM_CTP, CALREQ_FROM_ALTI, CALREQ_FROM_PG, CALREQ_FROM_VME} CALREQ_INPUT;
    static const unsigned int CALREQ_INPUT_NUMBER = CALREQ_FROM_VME + 1;
    static const std::string CALREQ_INPUT_NAME[CALREQ_INPUT_NUMBER];

    typedef enum {CALREQ_INACTIVE, CALREQ_LOCAL, CALREQ_CTP, CALREQ_ALTI} CALREQ_SOURCE;
    static const unsigned int CALREQ_SOURCE_NUMBER = CALREQ_ALTI + 1;
    static const std::string CALREQ_SOURCE_NAME[CALREQ_SOURCE_NUMBER];

    typedef enum {CALREQ_TO_CTP, CALREQ_TO_ALTI} CALREQ_OUTPUT;
    static const unsigned int CALREQ_OUTPUT_NUMBER = CALREQ_TO_ALTI + 1;
    static const std::string CALREQ_OUTPUT_NAME[CALREQ_OUTPUT_NUMBER];

    typedef enum {TTC_DECODER_COMMAND, TTC_DECODER_TIMESTAMP, PATTERN_SNAPSHOT} ALTI_RAM_MEMORY;
    static const unsigned int ALTI_RAM_MEMORY_NUMBER = PATTERN_SNAPSHOT + 1;
    static const std::string ALTI_RAM_MEMORY_NAME[ALTI_RAM_MEMORY_NUMBER];

    typedef enum {SHORT, LONG} TTC_FRAME_TYPE;
    static const unsigned int TTC_FRAME_TYPE_NUMBER = LONG + 1;
    static const std::string TTC_FRAME_TYPE_NAME[TTC_FRAME_TYPE_NUMBER];

    typedef enum {INTERNAL, EXTERNAL} TTC_ADDRESS_SPACE;
    static const unsigned int TTC_ADDRESS_SPACE_NUMBER = EXTERNAL + 1;
    static const std::string TTC_ADDRESS_SPACE_NAME[TTC_ADDRESS_SPACE_NUMBER];

    /////////////////////
    // BGO/TTYP TTC modes (low-level)
    typedef enum {BGO_SIGNAL, VME} TTC_BGO_MODE_TRIGGER;
    static const unsigned int TTC_BGO_MODE_TRIGGER_NUMBER = VME + 1;
    static const std::string TTC_BGO_MODE_TRIGGER_NAME[TTC_BGO_MODE_TRIGGER_NUMBER];

    typedef enum {SYNCHRONOUS, ASYNCHRONOUS} TTC_BGO_MODE_COMMAND;
    static const unsigned int TTC_BGO_MODE_COMMAND_NUMBER = ASYNCHRONOUS + 1;
    static const std::string TTC_BGO_MODE_COMMAND_NAME[TTC_BGO_MODE_COMMAND_NUMBER];

    typedef enum {SINGLE, REPETITIVE} TTC_BGO_MODE_REPEAT;
    static const unsigned int TTC_BGO_MODE_REPEAT_NUMBER = REPETITIVE + 1;
    static const std::string TTC_BGO_MODE_REPEAT_NAME[TTC_BGO_MODE_REPEAT_NUMBER];

    typedef enum {WHEN_NOT_EMPTY, ON_TRIGGER} TTC_BGO_MODE_FIFO;
    static const unsigned int TTC_BGO_MODE_FIFO_NUMBER = ON_TRIGGER + 1;
    static const std::string TTC_BGO_MODE_FIFO_NAME[TTC_BGO_MODE_FIFO_NUMBER];

    typedef enum {ON_INHIBIT, OTHER_MODES} TTC_BGO_MODE_INHIBIT;
    static const unsigned int TTC_BGO_MODE_INHIBIT_NUMBER = OTHER_MODES + 1;
    static const std::string TTC_BGO_MODE_INHIBIT_NAME[TTC_BGO_MODE_INHIBIT_NUMBER];

    // BGO/TTYP TTC modes (high-level)
    typedef enum {SYNCHRONOUS_SINGLE_BGO_SIGNAL, SYNCHRONOUS_REPETITIVE, ASYNCHRONOUS_BGO_SIGNAL, ASYNCHRONOUS_VME_ON_TRIGGER, ASYNCHRONOUS_FIFO_MODE, INVALID_MODE} TTC_BGO_MODE;
    static const unsigned int TTC_BGO_MODE_NUMBER = INVALID_MODE + 1;
    static const std::string TTC_BGO_MODE_NAME[TTC_BGO_MODE_NUMBER];
    /////////////////////

    typedef enum {EVENT, ORBIT} TTYP_COUNTER_SOURCE;
    static const unsigned int TTYP_COUNTER_SOURCE_NUMBER = ORBIT + 1;
    static const std::string TTYP_COUNTER_SOURCE_NAME[TTYP_COUNTER_SOURCE_NUMBER];


    typedef enum {BGO0_FIFO, BGO1_FIFO, BGO2_FIFO, BGO3_FIFO} TTC_FIFO;
    static const unsigned int TTC_FIFO_NUMBER = BGO3_FIFO + 1;
    static const std::string TTC_FIFO_NAME[TTC_FIFO_NUMBER];

    typedef enum {PG_ORB, PG_L1A, PG_TTR1, PG_TTR2, PG_TTR3, PG_TTYP, PG_BGO0, PG_BGO1, PG_BGO2, PG_BGO3, PG_CALREQ0, PG_CALREQ1, PG_CALREQ2, PG_BUSY} SIGNAL_PG;
    static const unsigned int SIGNAL_PG_NUMBER = PG_BUSY + 1;
    static const std::string SIGNAL_PG_NAME[SIGNAL_PG_NUMBER];

    // input signals that go to the synchronizer
    typedef enum
    {
      SWX_L1A, SWX_ORB, SWX_BGO0, SWX_BGO1, SWX_BGO2, SWX_BGO3, SWX_TTYP0, SWX_TTYP1, SWX_TTYP2, SWX_TTYP3, SWX_TTYP4, SWX_TTYP5, SWX_TTYP6, SWX_TTYP7, SWX_TTR1, SWX_TTR2, SWX_TTR3,
      LEMO_ORB, LEMO_L1A, LEMO_TTR1, LEMO_TTR2, LEMO_TTR3, LEMO_BGO2, LEMO_BGO3, LEMO_BUSY, RJ45_CALREQ0, RJ45_CALREQ1, RJ45_CALREQ2,
      CTP_BUSY, CTP_CALREQ0, CTP_CALREQ1, CTP_CALREQ2,
      ALTI_BUSY, ALTI_CALREQ0, ALTI_CALREQ1, ALTI_CALREQ2,
      CDR_LOCK, CDR_L1A, CDR_BRCST, CDR_INDIV, CDR_BCR, CDR_ECR, CDR_SERR, CDR_DERR
    } ASYNC_INPUT_SIGNAL;
    static const unsigned int ASYNC_INPUT_SIGNAL_NUMBER = CDR_DERR + 1;
    static const std::string ASYNC_INPUT_SIGNAL_NAME[ASYNC_INPUT_SIGNAL_NUMBER];
    
    typedef enum {IN_SYNCHRONIZED, IN_SHAPED_ONE, IN_SHAPED_TWO} SYNC_SHAPE;
    static const unsigned int SYNC_SHAPE_NUMBER = IN_SHAPED_TWO + 1;
    static const std::string SYNC_SHAPE_NAME[SYNC_SHAPE_NUMBER];
    
    typedef enum
    {
      CTP_CRQ0, CTP_CRQ1, CTP_CRQ2, ALTI_CRQ0, ALTI_CRQ1, ALTI_CRQ2, RJ45_CRQ0, RJ45_CRQ1, RJ45_CRQ2
    } ASYNC_INPUT_CALREQ;
    static const unsigned int ASYNC_INPUT_CALREQ_NUMBER = RJ45_CRQ2 + 1;
    static const std::string ASYNC_INPUT_CALREQ_NAME[ASYNC_INPUT_CALREQ_NUMBER];
    
    typedef enum
    {
      SYNC_IN_L1A, SYNC_IN_ORB, SYNC_IN_BGO0, SYNC_IN_BGO1, SYNC_IN_BGO2, SYNC_IN_BGO3, SYNC_IN_TTYP0, SYNC_IN_TTYP1, SYNC_IN_TTYP2, SYNC_IN_TTYP3, SYNC_IN_TTYP4, SYNC_IN_TTYP5, SYNC_IN_TTYP6, SYNC_IN_TTYP7, SYNC_IN_TTR1, SYNC_IN_TTR2, SYNC_IN_TTR3
    } SYNCHRONIZER_INPUT_SIGNAL;
    static const unsigned int SYNCHRONIZER_INPUT_SIGNAL_NUMBER = SYNC_IN_TTR3 + 1;
    static const std::string SYNCHRONIZER_INPUT_SIGNAL_NAME[SYNCHRONIZER_INPUT_SIGNAL_NUMBER];

    typedef enum
    {
      PBM_L1A, PBM_TTR1, PBM_TTR2, PBM_TTR3, PBM_BGO0, PBM_BGO1, PBM_BGO2, PBM_BGO3
    }PBM_SIGNAL;
    static const unsigned int PBM_SIGNAL_NUMBER = PBM_BGO3 + 1;
    static const std::string PBM_SIGNAL_NAME[PBM_SIGNAL_NUMBER];
      
    typedef enum                {ECR_VME, ECR_INTERNAL} ECR_TYPE;
    static const unsigned int   ECR_TYPE_NUMBER         = ECR_INTERNAL + 1;
    static const std::string    ECR_TYPE_NAME[ECR_TYPE_NUMBER];
    static const unsigned int   ECR_LENGTH              = 4;
    static const unsigned int   ECR_FREQUENCY_MASK      = ALTI::MASK_ECR_FREQUENCY;
    static const unsigned int   ECR_BUSY_BEFORE_MASK    = ALTI::MASK_ECR_BUSYBEFORE;
    static const unsigned int   ECR_BUSY_AFTER_MASK     = ALTI::MASK_ECR_BUSYAFTER;
    static const unsigned int   ECR_ORBIT_OFFSET_MASK   = ALTI::MASK_ECR_ORBITOFFSET;
    class ECR_GENERATION {
      public:
        ECR_GENERATION();
        ECR_GENERATION(const ECR_GENERATION &);
        ECR_GENERATION &operator=(const ECR_GENERATION &);
       ~ECR_GENERATION();
        int read(const std::string &);
        void dump(std::ostream & = std::cout) const;
        std::string print() const;
      public:
        ECR_TYPE                type;
        unsigned int            length;
        unsigned int            frequency;
        unsigned int            busy_before;
        unsigned int            busy_after;
        unsigned int            orbit_offset;
    };
    
    static const unsigned int   T2L_FIFO_SIZE           = ALTI::T2L_L1IDFIFO_INDEX_NUMBER;

    static const unsigned int   BCID_NUMBER              = 4096;
    static const unsigned int   BCID_WORD_NUMBER         = BCID_NUMBER/WORD_SIZE;
    static const unsigned int   CNT_TTYP_LUT_WORD_NUMBER = 256;
    static const unsigned int   CNT_TTYP_NUMBER          = 8;
	
    static const unsigned int   ORBIT_LENGTH            = 3564;
    static const int            STRING_LENGTH           = 1024;
    static const unsigned int   TRANSMITTER_NUMBER      = 11;

    static const unsigned int   ALTI_SLOT_MIN           = 1;
    static const unsigned int   ALTI_SLOT_MAX           = 21;
    static const unsigned int   CABLE_NUMBER            = 2; // CTP, ALTI
    
    static const unsigned int   MEM_SIGNAL_BITS         = 21; // 21 LSb in PG/SNAPSHOT
    static const unsigned int   MEM_MULT_BITS           = 11; // 11 MSb in PG/SNAPSHOT
    static const unsigned int   MEM_SIGNAL_MASK         = ((1 << MEM_SIGNAL_BITS) - 1);
    static const unsigned int   MEM_MULT_MASK           = ~MEM_SIGNAL_MASK;
    static const unsigned int   MEM_MULT_MAX            = (MEM_MULT_MASK >> MEM_SIGNAL_BITS);

    static const unsigned int   SHIFT_STEPS_PER_NS      = 72;

    // public high-level functions
    int AltiResetControl(const bool, const bool, const bool, const bool);

    int AltiReset();
    int AltiSetup();
    int AltiCheck();


    int AltiConfigRead(AltiConfiguration &cfg, std::ostream &os = std::cerr) { return CFGRead(cfg, os); }
    int AltiConfigWrite(const AltiConfiguration &cfg, std::ostream &os = std::cerr) { return CFGWrite(cfg, os); }
    
    // public CR/CSR methods
    int CSRManufacturerIdRead(unsigned int &);
    int CSRBoardIdRead(unsigned int &);
    int CSRRevisionIdRead(unsigned int &);
    int CSRBARRead(unsigned int &);
    int CSRADERRead(unsigned int &);
    int CSRADERWrite(const unsigned int);
    
    // public CLK methods
    // PLL
    int CLKStatusReadPLL(bool &, bool &, bool &, bool &);
    int CLKStatusClearPLL();
    int CLKMonitorReadPLL(const EXT_CLK_SOURCE, bool &, bool &, bool &);
    int CLKMonitorStatusClearPLL();
    int CLKResetPLL();
    int CLKStickyBitsReset();

    int CLKInputSelectReadPLL(CLK_PLL_TYPE &);
    int CLKInputSelectWritePLL(const CLK_PLL_TYPE);
    int CLKInputSelectTogglePLL();
   
    int CLKPhaseShiftPLL(const bool, const unsigned int);
    int CLKPhasePositionReadPLL(int &);
    int CLKPhaseShiftBusy(bool &);
    int CLKPhaseResetPLL();

    // Jitter cleaner
    int CLKStatusReadJitterCleaner(bool &, bool &, bool &, bool &, bool &, bool &);
    int CLKStatusClearJitterCleaner();
    int CLKResetJitterCleaner();

    int CLKInputSelectReadJitterCleaner(CLK_JC_TYPE &);
    int CLKInputSelectWriteJitterCleaner(const CLK_JC_TYPE);

    // from I2C
    int CLKJitterCleanerResync();
    int CLKJitterCleanerStatusRead(std::vector<bool> &, std::vector<bool> &, bool &, bool &);
    int CLKJitterCleanerStickyStatusRead(std::vector<bool> &, std::vector<bool> &, bool &, bool &);
    int CLKJitterCleanerStatusClear();
    int CLKJitterCleanerDesignIDRead(std::string &);
    int CLKJitterCleanerConfigRead(const std::string &, const std::string &); // read configuration (addresses given in input file) into output file
    int CLKJitterCleanerConfigWrite(const std::string &); // write configuration from input file
    int CLKJitterCleanerConfigWrite(); // write default configuration
   
    
    // public SIG methods
    // Cross-point switches (SWX)
    int SIGSwitchConfigRead(const SIGNAL, const SIGNAL_DESTINATION, SIGNAL_SOURCE &); // 1 signal, 1 output
    int SIGSwitchConfigRead(const SIGNAL, std::vector<SIGNAL_SOURCE> &); // 1 signal, all outputs
    int SIGSwitchConfigWrite(const SIGNAL, const SIGNAL_DESTINATION, const SIGNAL_SOURCE); // 1 signal, 1 output
    int SIGSwitchConfigWrite(const SIGNAL_DESTINATION, const SIGNAL_SOURCE); // all signals, 1 output
    int SIGSwitchConfigWrite(const SIGNAL, const SIGNAL_SOURCE); // 1 signal, all outputs
    int SIGSwitchConfigWrite(const SIGNAL_SOURCE); // all signals, all outputs
    int SIGSwitchConfigWrite(const ALTI_MODE);
    int SIGSwitchPowerDownEnableRead(const SIGNAL, const SIGNAL_DESTINATION, bool &); // 1 signal, 1 output
    int SIGSwitchPowerDownEnableRead(const SIGNAL, std::vector<bool> &); // 1 signal, all outputs
    int SIGSwitchPowerDownEnableWrite(const SIGNAL, const SIGNAL_DESTINATION, const bool); // 1 signal, 1 output
    int SIGSwitchPowerDownEnableWrite(const SIGNAL_DESTINATION, const bool); // all signals, 1 output
    int SIGSwitchPowerDownEnableWrite(const SIGNAL, const bool); // 1 signal, all outputs
    int SIGSwitchPowerDownEnableWrite(const bool); // all signals, all outputs
    // Equalizers (EQZ)
    int SIGEqualizersConfigRead(const SIGNAL_SOURCE, bool &, EQUALIZER_CONFIG &);
    int SIGEqualizersConfigWrite(const SIGNAL_SOURCE, bool , const EQUALIZER_CONFIG);
    int SIGEqualizersEnableRead(const SIGNAL_SOURCE, bool &);
    int SIGEqualizersEnableWrite(const SIGNAL_SOURCE, const bool);
    int SIGEqualizersConfigRead(const SIGNAL_SOURCE, EQUALIZER_VOLTAGE, unsigned int &);
    int SIGEqualizersConfigWrite(const SIGNAL_SOURCE, const EQUALIZER_VOLTAGE, const unsigned int);
    int SIGEqualizersConfigRead(const SIGNAL_SOURCE, unsigned int &, unsigned int &, unsigned int &, unsigned int &);
    int SIGEqualizersConfigWrite(const SIGNAL_SOURCE, const unsigned int, const unsigned int, const unsigned int, const unsigned int);

    // Input synchronization & shaping
    // sync
    int SIGInputSyncEnableRead(const SYNCHRONIZER_INPUT_SIGNAL, unsigned int &);
    int SIGInputSyncEnableWrite(const SYNCHRONIZER_INPUT_SIGNAL, const unsigned int);
    int SIGInputSyncEnableRead(std::vector<unsigned int> &); // all
    int SIGInputSyncEnableWrite(const std::vector<unsigned int> &); // all
    int SIGInputSyncEnableWrite(const unsigned int); // all
    int SIGInputSyncHistogramsRead(const SYNCHRONIZER_INPUT_SIGNAL, std::vector<unsigned int> &);
    int SIGInputSyncHistogramsReset();
    // shape
    int SIGSyncShapeSwitchRead(std::vector<unsigned int> &);
    int SIGSyncShapeSwitchRead(const SYNCHRONIZER_INPUT_SIGNAL, SYNC_SHAPE &);
    int SIGSyncShapeSwitchWrite(const SYNCHRONIZER_INPUT_SIGNAL, const unsigned int);
    int SIGSyncShapeSwitchWrite(const std::vector<unsigned int>);

    // BGO LEMO outputs multiplexer
    int SIGMuxBGOOutputLEMORead(bool &, bool &); // both
    int SIGMuxBGOOutputLEMOWrite(const bool, const bool); // both

    // LVDS output selection
    int SIGOutputSelectL1aRead(std::string&);
    int SIGOutputSelectL1aWrite(const std::string);
    int SIGOutputSelectOrbitRead(std::string&);
    int SIGOutputSelectOrbitWrite(const std::string);
    int SIGOutputSelectBgoRead(const int, std::string&);
    int SIGOutputSelectBgoWrite(const int, const std::string);
    int SIGOutputSelectTestTriggerRead(const int, std::string&);
    int SIGOutputSelectTestTriggerWrite(const int, const std::string);
    int SIGOutputSelectTriggerTypeRead(std::string&);
    int SIGOutputSelectTriggerTypeWrite(const std::string);
    
    // orbit input selector
    int SIGInputSelectOrbitRead(std::string&);
    int SIGInputSelectOrbitWrite(const std::string);    

    // turn counter
    int SIGTurnCounterOrbitSourceRead(std::string &);
    int SIGTurnCounterOrbitSourceWrite(const std::string);
    int SIGTurnCounterMaskRead(unsigned int &);
    int SIGTurnCounterMaskWrite(const unsigned int);
    int SIGTurnCounterReset();
    int SIGTurnCounterFreqRead(unsigned int &);
    int SIGTurnCounterFreqWrite(const unsigned int);
    int SIGTurnCounterValueRead(unsigned int &);

    // LAr calibration
    int SIGBGo2L1aDelayRead(unsigned int &);
    int SIGBGo2L1aDelayWrite(const unsigned int);
    int SIGBGo2L1aCounterRead(unsigned int &);

    // public CRQ methods
    int CRQInputSyncHistogramsRead(const ASYNC_INPUT_CALREQ, std::vector<unsigned int> &);
    int CRQInputSyncHistogramsReset();
    int CRQInputSyncEnableRead(const ASYNC_INPUT_CALREQ, unsigned int &);
    int CRQInputSyncEnableWrite(const ASYNC_INPUT_CALREQ, const unsigned int);
    int CRQInputSyncEnableRead(std::vector<unsigned int> &);
    int CRQInputSyncEnableWrite(const std::vector<unsigned int> &);
    int CRQInputSyncEnableWrite(const unsigned int);
    int CRQRJ45ShapeEnableRead(const unsigned int, SYNC_SHAPE &);
    int CRQRJ45ShapeEnableWrite(const unsigned int, const unsigned int);
    int CRQLocalSourceRead(const unsigned int, CALREQ_INPUT &); // 1 bit
    int CRQLocalSourceWrite(const unsigned int, const CALREQ_INPUT); // 1 bit
    int CRQInputSelectRead(std::vector<CALREQ_INPUT> &); // all 3 bits
    int CRQInputSelectWrite(const std::vector<CALREQ_INPUT> &); // all 3 bits
    int CRQInputSelectWrite(const CALREQ_INPUT); // all 3 bits
    int CRQOutputSelectRead(const CALREQ_OUTPUT, const unsigned int, CALREQ_SOURCE &); // 1 output, 1 bit
    int CRQOutputSelectWrite(const CALREQ_OUTPUT, const unsigned int, const CALREQ_SOURCE); // 1 output, 1 bit
    int CRQOutputSelectRead(const CALREQ_OUTPUT, std::vector<CALREQ_SOURCE> &); // 1 output, all 3 bits
    int CRQOutputSelectWrite(const CALREQ_OUTPUT, const std::vector<CALREQ_SOURCE> &); // 1 output, all 3 bits
    int CRQOutputSelectRead(const unsigned int, std::vector<CALREQ_SOURCE> &); // all outputs, 1 bit
    int CRQOutputSelectWrite(const unsigned int, const std::vector<CALREQ_SOURCE> &); // all outputs, 1 bit
    int CRQOutputSelectWrite(const CALREQ_OUTPUT, const CALREQ_SOURCE); // 1 output, all bits
    int CRQOutputSelectWrite(const unsigned int, const CALREQ_SOURCE); // all outputs, 1 bit
    int CRQOutputSelectWrite(const CALREQ_SOURCE); // all outputs, all bits
    int CRQConstLevelRegRead(const unsigned int, bool &);
    int CRQConstLevelRegWrite(const unsigned int, const bool);
 
    // public functions for shared memory for pattern and snapshot
    int PRMModeRead(std::string &); // SNAPSHOT or PATTERN
    int PRMModeWrite(const std::string);
    int PRMReset();
    int PRMStatusRead(bool &, std::string &);
    int PRMEnableRead(bool &); // global RAM enable bit
    int PRMEnableWrite(const bool); // global RAM enable bit
    int PRMSnapshotSignalMaskRead(ALTI_PRM_SNAPSHOTMASK_BITSTRING &);
    int PRMSnapshotSignalMaskWrite(const ALTI_PRM_SNAPSHOTMASK_BITSTRING);
    int PRMSnapshotSignalMaskRead(const SIGNAL, bool &);
    int PRMSnapshotSignalMaskWrite(const SIGNAL, const bool);
    int PRMSnapshotSignalMaskRead(u_int &); // all 
    int PRMSnapshotSignalMaskWrite(const u_int); // all
    int PRMSnapshotSignalMaskWrite(const bool); // all
    int PRMRepeatRead(bool &);
    int PRMRepeatWrite(const bool);
    int PRMCurrentAddressRead(unsigned int &);
    int PRMStartAddressRead(unsigned int &);
    int PRMStartAddressWrite(const unsigned int);
    int PRMStopAddressRead(unsigned int &);
    int PRMStopAddressWrite(const unsigned int);
    int PRMTriggerEnableRead(bool &);
    int PRMTriggerEnableWrite(const bool);
    int PRMTriggerRepeatRead(bool &);
    int PRMTriggerRepeatWrite(const bool);
    int PRMTriggerSignalRead(const SIGNAL_PG, bool &);
    int PRMTriggerSignalWrite(const SIGNAL_PG, const bool);
    int PRMTriggerTypeRead(unsigned int &);
    int PRMTriggerTypeWrite(const unsigned int);
    int PRMRead(RCD::CMEMSegment *, const unsigned int start = 0, const unsigned int stop = 0); // read pattern generation memory
    int PRMReadFile(RCD::CMEMSegment *, std::ofstream &, const unsigned int start = 0, const unsigned int stop = 0); // read snapshot memory into file
    int PRMWriteFile(RCD::CMEMSegment *, std::ifstream &); // write pattern generation memory from file
    int PRMReadFile(std::ofstream &, const unsigned int start = 0, const unsigned int stop = 0); // read snapshot memory into file  
    int PRMWriteFile(std::ifstream &); // write pattern generation memory from file
    
    // public ENC methods
    int ENCTransmitterEnableRead(const unsigned int, bool &);
    int ENCTransmitterEnableRead(std::vector<bool> &); // all
    int ENCTransmitterEnableWrite(const unsigned int, const bool);
    int ENCTransmitterEnableWrite(const bool); // all
    int ENCTransmitterEnableWrite(const std::vector<bool> &); // all
    int ENCTransmitterDelayRead(const unsigned int, u_int &);
    int ENCTransmitterDelayRead(std::vector<u_int> &); // all
    int ENCTransmitterDelayWrite(const unsigned int, const u_int);
    int ENCTransmitterDelayWrite(const std::vector<u_int>); // all
    int ENCTransmitterFaultRead(const unsigned int, bool &);
    int ENCTransmitterFaultRead(std::vector<bool> &); // all
    int ENCTransmitterFaultClear(); // all
    int ENCL1ASourceRead(bool &, bool &, bool &, bool &, bool &, bool &, bool &);
    int ENCL1ASourceWrite(const bool, const bool, const bool, const bool, const bool, const bool, const bool);
    int ENCOrbitSourceRead(std::string &);
    int ENCOrbitSourceWrite(const std::string);
    int ENCBgoSourceRead(const u_int, std::string &);
    int ENCBgoSourceWrite(const u_int, const std::string);
    int ENCTtypSourceRead(std::string &);
    int ENCTtypSourceWrite(const std::string);
    int ENCInhibitParamsRead(const unsigned int, unsigned int &, unsigned int &);
    int ENCInhibitParamsRead(std::vector<unsigned int> &, std::vector<unsigned int> &); // all
    int ENCInhibitParamsWrite(const unsigned int, const unsigned int, const unsigned int);
    int ENCInhibitParamsWrite(const std::vector<unsigned int> &, const std::vector<unsigned int> &); // all
    int ENCBgoCommandModeRead(const unsigned int, TTC_BGO_MODE &);
    int ENCBgoCommandModeRead(std::vector<TTC_BGO_MODE> &); // all
    int ENCBgoCommandModeWrite(const unsigned int, const TTC_BGO_MODE);
    int ENCBgoCommandModeWrite(const std::vector<TTC_BGO_MODE> &); // all
    // TTYP
    int ENCTriggerTypeControlRead(bool &, unsigned int &, TTC_ADDRESS_SPACE &, unsigned int &);
    int ENCTriggerTypeControlWrite(const unsigned int, const TTC_ADDRESS_SPACE, const unsigned int);
    int ENCTriggerTypeEnable(const bool);
    int ENCTriggerTypeDelayRead(unsigned int &);
    int ENCTriggerTypeDelayWrite(const unsigned int);
    int ENCTriggerTypeFifoStatusRead(bool &, bool &, u_int &);
    int ENCTriggerTypeCounterReset(const bool, const bool);
    int ENCTriggerTypeCounterSourceRead(TTYP_COUNTER_SOURCE &);
    int ENCTriggerTypeCounterSourceWrite(const TTYP_COUNTER_SOURCE);
    int ENCTriggerTypeCounterRead(u_int &, u_int &);
    // BGo FIFOs
    int ENCFifoStatusRead(const TTC_FIFO, bool &, bool &, bool &, u_int &);
    int ENCFifoStatusRead(std::vector<bool> &, std::vector<bool> &, std::vector<bool> &, std::vector<u_int> &);
    int ENCFifoReset(const TTC_FIFO); 
    int ENCFifoReset(); // all
    int ENCFifoRead(const TTC_FIFO, unsigned int &);
    int ENCFifoRead(const TTC_FIFO, std::vector<unsigned int> &);
    int ENCBgoFifoRetransmitRead(const TTC_FIFO, bool &);
    int ENCBgoFifoRetransmitRead(std::vector<bool> &); // all
    int ENCBgoFifoRetransmitWrite(const TTC_FIFO, const bool);
    int ENCBgoFifoRetransmitWrite(const std::vector<bool> &); // all
    int ENCBgoFifoRetransmitWrite(const bool); // all
    int ENCBgoCommandPut(const unsigned int, const TTC_FRAME_TYPE, const uint16_t, const TTC_ADDRESS_SPACE, const uint8_t, const uint8_t);
    int ENCBgoCommandPutShort(const unsigned int, const uint8_t);
    int ENCBgoCommandPutLong(const unsigned int, const uint16_t, const TTC_ADDRESS_SPACE, const uint8_t, const uint8_t);
    int ENCBgoCommandModeRead(const unsigned int, TTC_BGO_MODE_TRIGGER &, TTC_BGO_MODE_COMMAND &, TTC_BGO_MODE_REPEAT &, TTC_BGO_MODE_FIFO &, TTC_BGO_MODE_INHIBIT &);
    int ENCBgoCommandModeWrite(const unsigned int, const TTC_BGO_MODE_TRIGGER, const TTC_BGO_MODE_COMMAND, const TTC_BGO_MODE_REPEAT, const TTC_BGO_MODE_FIFO, const TTC_BGO_MODE_INHIBIT);
    int ENCBgoCommandModeRead(std::vector<TTC_BGO_MODE_TRIGGER> &, std::vector<TTC_BGO_MODE_COMMAND> &, std::vector<TTC_BGO_MODE_REPEAT> &, std::vector<TTC_BGO_MODE_FIFO> &, std::vector<TTC_BGO_MODE_INHIBIT> &);
    int ENCBgoCommandModeWrite(const std::vector<TTC_BGO_MODE_TRIGGER> &, const std::vector<TTC_BGO_MODE_COMMAND> &, const std::vector<TTC_BGO_MODE_REPEAT> &, const std::vector<TTC_BGO_MODE_FIFO> &, const std::vector<TTC_BGO_MODE_INHIBIT> &);
    int ENCBgoGenerate(const unsigned int);
    int ENCAsyncCommandPutShort(const uint8_t);
    int ENCAsyncCommandPutLong(const uint16_t, const TTC_ADDRESS_SPACE, const uint8_t, const uint8_t);
    int ENCAsyncCommandPendingShort(bool &);
    int ENCAsyncCommandPendingLong(bool &);
    int ENCBgoRequestStatusRead(bool &, bool &, bool &, bool &, bool &, bool &);
    int ENCBgoRequestReset(const TTC_FIFO);
    int ENCBgoRequestReset(); // all
    
    // BGo counters
    int ENCBgoCounterReset(const TTC_FIFO);
    int ENCBgoCounterReset(); // all
    int ENCBgoCounterRead(const TTC_FIFO, u_int &);
    int ENCBgoCounterRead(std::vector<u_int> &); // all


    static TTC_BGO_MODE ENCLowLevelToHighLevelBgoMode(const TTC_BGO_MODE_TRIGGER, const TTC_BGO_MODE_COMMAND, const TTC_BGO_MODE_REPEAT, const TTC_BGO_MODE_FIFO, const TTC_BGO_MODE_INHIBIT);
    
    // public DEC methods
    int DECEnableRead(bool &);
    int DECEnableWrite(const bool);
    int DECCurrentAddressRead(unsigned int &);
    int DECCurrentAddressWrite(const unsigned int );
    int DECStatusRead(bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &);
    int DECStatusClear();
    int DECCounterReset();
    int DECCountersRead(u_int &, u_int &, u_int &);
    int DECOrbitCountersRead(u_int &, u_int &, u_int &, u_int &, u_int &);
    int DECReset();
    int DECLoopRead(bool &);
    int DECLoopWrite(const bool);
    int DECTriggerWordWrite(const bool, const short, const short , const short , const short , const short , const short , const bool , const bool , const bool , const bool , const bool , const bool );
    int DECTriggerWordRead(bool &,short &, short &, short &, short &, short &, short &, bool &, bool &, bool &, bool &, bool &, bool &);
    int DECRead(RCD::CMEMSegment *, RCD::CMEMSegment *, const unsigned int, const unsigned int, unsigned int, int); // read TTC decoder memory
    int DECReadFile(RCD::CMEMSegment *, RCD::CMEMSegment *, const unsigned int, const unsigned int, unsigned int, int, const int, std::ofstream &); // read TTC decoder memory into file
    
    // public MEM methods (low-level)
    int MEMSelectWrite(const std::string);
    int MEMDataRead(const ALTI_RAM_MEMORY, unsigned int &, const unsigned int = 0); // number
    int MEMDataWrite(const ALTI_RAM_MEMORY, const unsigned int, const unsigned int = 0); // number
    int MEMDataRead(const ALTI_RAM_MEMORY, std::vector<unsigned int> &, const unsigned int = 0); // vector
    int MEMDataWrite(const ALTI_RAM_MEMORY, const std::vector<unsigned int> &, const unsigned int = 0); // vector
    int MEMDataRead(const ALTI_RAM_MEMORY, RCD::CMEMSegment *, const unsigned int, const unsigned int = 0, const unsigned int = 0); // block
    int MEMDataWrite(const ALTI_RAM_MEMORY, RCD::CMEMSegment *, const unsigned int, const unsigned int = 0, const unsigned int = 0); // block

    static int MEMSnapshotToString(const unsigned int, std::string &); // snapshot memory entry -> user readable snapshot line
    static int MEMStringToPattern(std::string &, std::vector<unsigned int> &, const SNAPSHOT_TYPE, uint8_t[4]  = 0); // user readable pattern generation line -> pattern generation memory entry/entries
    static int MEMCommandTimestampToScopeString(const unsigned int, const unsigned int, std::string &); // TTC decoder command/timestamp memory entries -> user readable ttcscope line
    static int MEMScopeStringToCommandTimestamp(const std::string &, unsigned int &, TTC_SPY_COMMAND &, unsigned int &, TTC_SPY_TIMESTAMP &); // user readable ttcscope line -> TTC decoder command/timestamp memory entries

    static int MEMCommandTimestampToBitstrings(const unsigned int, const unsigned int,  TTC_SPY_COMMAND &, TTC_SPY_TIMESTAMP &); // TTC decoder command/timestamp memory entries -> corresponding bistrings


    // public CNT methods
    int CNTBcidMaskRead(const unsigned int, bool &);
    int CNTBcidMaskWrite(const unsigned int, const bool);
    int CNTBcidMaskRead(std::vector<bool> &);
    int CNTBcidMaskWrite(const std::vector<bool> &);
    int CNTBcidMaskRead(std::vector<unsigned int> &);
    int CNTBcidMaskWrite(const std::vector<unsigned int> &);
    int CNTBcidMaskRead(const std::string &);
    int CNTBcidMaskWrite(const std::string &);
    int CNTBcidOffsetRead(unsigned int &);
    int CNTBcidOffsetWrite(const unsigned int);
    int CNTControlPrint();
    int CNTEnableRead(bool &, bool &);
    int CNTEnableWrite(const bool);
    int CNTEnableWait(const bool);
    int CNTClearWrite();
    int CNTClearWait();
    int CNTCopyWrite();
    int CNTCopyWait();
    int CNTCopyClearWrite();
    int CNTCopyClearWait();
    int CNTTurnCountRead(unsigned int &);
    int CNTCounterRead(AltiCounter *);
    int CNTRequestPrint();
    int CNTRequestWrite(const bool, const bool, const bool);
    int CNTEdgeDetectionRead(const CNT_TYPE, bool &);
    int CNTEdgeDetectionWrite(const CNT_TYPE, const bool);
    int CNTEdgeDetectionWrite(const bool);

    // orbit counters
    int CNTOrbitReset();
    int CNTOrbitRead(unsigned int &, unsigned int &, unsigned int &);
    int CNTOrbitLengthRead(unsigned int &, unsigned int &);

    int CNTTtypLutRead(std::vector<unsigned int> &);
    int CNTTtypLutWrite(const std::vector<unsigned int>);
    int CNTTtypLutRead(const std::string);
    int CNTTtypLutWrite(const std::string);
    int CNTTtypLutPrint();
    
    // firmware
    int FMWControlEnableBoot(bool = false, bool = false);
    int FMWControlReset();
    int FMWFIFOStatusRead(bool &, bool &, bool &, bool &, unsigned int &, unsigned int &, unsigned int &);
    int FMWQuickBootStatusRead(bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &, bool &);
    int FMWWaitUntilDone(bool &);
    int FMWReceivedDataRead(unsigned int &);
    int FMWBootSafetyEnable(bool);

    int FMWFIFOWrite(const unsigned int wr) { return m_alti->FMW_QuickBootFlashFifo_Write(wr); }
    int FMWFIFOWrite(const std::vector<unsigned int> &wr) { return m_alti->FMW_QuickBootFlashFifo_Write(wr); }
    int FMWFIFOWrite(RCD::CMEMSegment* seg) { return m_alti->FMW_QuickBootFlashFifo_Write(seg); }
    
    int FMWIcapCommandRead(u_int &);
    int FMWIcapCommandWrite(const u_int);
    int FMWIcapWarmBootStartAddressRead(u_int &);
    int FMWIcapWarmBootStartAddressWrite(const u_int);

    // power and temperature status
    int MONPowerAndTemperatureAlarm(bool &, bool &, bool &, bool &, bool &, bool &, bool &);
    // voltage & temperature monitoring, LTC2991
    int MONVoltageReadLTC2991(std::vector<float> &, std::vector<float> &);
    int MONInternalTempReadLTC2991(float &);
    // local & remote temperature monitoring, MAX1617
    int MONLocalTempReadMAX1617(float &);
    // local & remote temperature monitoring, MAX6695
    int MONLocalTempReadMAX6695(float &);
    int MONLocalTempRead(float &);

    // ECR block
    int ECRGenerationRead(ECR_GENERATION &);
    int ECRGenerationWrite(const ECR_GENERATION &);
    int ECRGenerate();
    int ECRReadyWait();

    // T2L block
    int T2LControlReset(const bool, const bool, const bool);
    int T2LEventIdentifierRead(EventIdentifier &);
    int T2LFifoStatusRead(bool &, bool &, unsigned int &);
    int T2LFifoWrite();
    int T2LFifoRead(unsigned int &);
    int T2LFifoRead(std::vector<unsigned int> &);
    
    // I2C block
    int I2CSetup();
    int I2CDump();
    int I2CTransferTypeRead(const unsigned int, const unsigned int, const unsigned int, I2CInterface::TRANSFER_TYPE &);
    int I2CDisplayTypeRead(const unsigned int, const unsigned int, const unsigned int, I2CNetwork::DISPLAY_TYPE &);
    int I2CNameRead(const unsigned int, std::string &);
    int I2CNameRead(const unsigned int, const unsigned int, std::string &);
    int I2CNameRead(const unsigned int, const unsigned int, const unsigned int, std::string &);
    int I2CDataRead(const unsigned int, const unsigned int, const unsigned int, std::vector<unsigned int> &);
    int I2CDataRead(const unsigned int, const unsigned int, const unsigned int, unsigned int &);
    int I2CDataRead(const unsigned int, const unsigned int, const unsigned int, float &);
    int I2CDataRead(const unsigned int, const unsigned int, const unsigned int, std::string &);
    int I2CDataRead(const unsigned int, const unsigned int, const unsigned int, const unsigned int, const unsigned int, std::vector<unsigned int> &);
    int I2CDataRead(const I2CNetwork::I2CNode &, std::vector<unsigned int> &);
    int I2CDataRead(const I2CNetwork::I2CNode &, unsigned int &);
    int I2CDataRead(const I2CNetwork::I2CNode &, float &);
    int I2CDataRead(const I2CNetwork::I2CNode &, std::string &);
    int I2CDataRead(const I2CNetwork::I2CNode &, const unsigned int, const unsigned int, std::vector<unsigned int> &);
    int I2CNodeRead(const std::string &n, I2CNetwork::I2CNode &o)                       { return (m_net->getI2CNode(n, o)); }
    int I2CDataWrite(const unsigned int, const unsigned int, const unsigned int, const unsigned int);
    int I2CDataWrite(const unsigned int, const unsigned int, const unsigned int, const float);
    int I2CDataWrite(const unsigned int, const unsigned int, const unsigned int, const std::string &);
    unsigned int I2CNumBusses() const                                                   { return (m_net->numI2CBusses()); }
    unsigned int I2CNumDevices(const unsigned int b) const                              { return (m_net->numI2CDevices(b)); }
    unsigned int I2CNumQuantities(const unsigned int b, const unsigned int d) const     { return (m_net->numI2CQuantities(b, d)); }
    int I2CReadableQuantity(const unsigned b, const unsigned d, const unsigned q, bool &r) const { return (m_net->readable(b, d, q, r)); }
    int I2CWritableQuantity(const unsigned b, const unsigned d, const unsigned q, bool &w) const { return (m_net->writable(b, d, q, w)); }
    unsigned int I2CQuantityGetNumber(const unsigned int b, const unsigned int d, const unsigned int q) const { return (m_net->getNumberI2CQuantity(b, d, q)); }
    int I2CQuantityGetName(const unsigned int b, const unsigned int d, const unsigned int q, std::string &name) const { return (m_net->name(b, d, q, name)); } 
    int I2CDataPrint(const unsigned int b, const unsigned int d, const unsigned int q)  { return (m_net->dumpI2CQuantity(b, d, q)); }
    int I2CDataPrint(const I2CNetwork::I2CNode &n)                                      { return (m_net->dumpI2CQuantity(n)); }
    
    // 1-Wire serial number
    int OWSerialNumber(std::string &s)                                                  { return (m_ds1wm->detectSerialNumber(s)); }
    
    // BM block is inside BSY now
    int BSYStatusRead(AltiBusyCounter&);
    int BSYMonitoringControlRead(bool&, bool&);
    int BSYMonitoringControlWrite(const bool, const bool);
    int BSYFifoReset();
    int BSYFifoWrite();
    int BSYSelectRead(AltiBusyCounter&);
    int BSYSelectWrite(const AltiBusyCounter&);
    int BSYCounterReset();
    int BSYCounterReset(const AltiBusyCounter&);
    int BSYIntervalRead(unsigned int&);
    int BSYIntervalWrite(const unsigned int);
    int BSYFifoWatermarkRead(unsigned int&);
    int BSYFifoWatermarkWrite(const unsigned int);
    int BSYFifoStatusRead(unsigned int&, bool&);
    int BSYFifoLevel(unsigned int&);
    int BSYFifoWait(unsigned int&, const unsigned int = 1);
    int BSYFifoReady(bool&);
    int BSYFifoFull(bool&);
    int BSYFifoRead(unsigned int&);
    int BSYCounterConfigRead(AltiBusyCounter&);
    int BSYFifoRead(AltiBusyCounter&, const int = -1, const int = -1);
    int BSYFifoRead(AltiBusyCounterRate&, AltiBusyCounter&, const int = -1, const int = -1);
    //int BSYGlobalStatusPrint();

    // public BSY methods
    int BSYInputSelectRead(const BUSY_INPUT, bool &); // 1 input
    int BSYInputSelectWrite(const BUSY_INPUT, const bool); // 1 input
    int BSYInputSelectRead(std::vector<bool> &); // all inputs
    int BSYInputSelectWrite(const std::vector<bool> &); // all inputs
    int BSYOutputSelectRead(const BUSY_OUTPUT, BUSY_SOURCE &); // 1 output
    int BSYOutputSelectWrite(const BUSY_OUTPUT, const BUSY_SOURCE); // 1 output
    int BSYOutputSelectRead(std::vector<BUSY_SOURCE> &); // all outputs
    int BSYOutputSelectWrite(const std::vector<BUSY_SOURCE> &); // all outputs
    int BSYConstLevelRegRead(bool &);
    int BSYConstLevelRegWrite(const bool);
    int BSYFrontPanelLevelRead(BUSY_LEVEL &);
    int BSYFrontPanelLevelWrite(const BUSY_LEVEL);
    // busy masking of input signals 
    int BSYMaskingL1aEnableRead(bool &);
    int BSYMaskingL1aEnableWrite(const bool);
    int BSYMaskingTTREnableRead(bool &,bool &,bool &);
    int BSYMaskingTTREnableWrite(const short,const bool);
    int BSYMaskingBGoEnableRead(bool &,bool &,bool &,bool &);
    int BSYMaskingBGoEnableWrite(const short,const bool);
    // busy masking of output signals from the pattern generator 
    int BSYMaskingPatL1aEnableRead(bool &);
    int BSYMaskingPatL1aEnableWrite(const bool);
    int BSYMaskingPatTTREnableRead(bool &,bool &,bool &);
    int BSYMaskingPatTTREnableWrite(const short,const bool);
    int BSYMaskingPatBGoEnableRead(bool &,bool &,bool &,bool &);
    int BSYMaskingPatBGoEnableWrite(const short,const bool);
    
    // public PBM methods
    int PBMStatusRead(bool &,bool &,unsigned int &,unsigned int &);
    int PBMEnableCounters(const bool);
    int PBMWriteOffset(const unsigned int);
    int PBMClearCounters();
    int PBMReadTurnCounter(u_int &turn);
    int PBMReadMemory(PBM_SIGNAL,RCD::CMEMSegment *);
	 
	
    // MINICTP
    static const unsigned int   ITEM_NUMBER          = ALTI_CTP_DTM_TAVENABLE_BITSTRING::ENABLE_NUMBER; // 8
    static const unsigned int   BGRP_NUMBER          = ALTI::CTP_BGM_BCIDLUT_NUMBER;
    static const unsigned int   SIMPLE_LENGTH        = 0xffff;
    static const unsigned int   LEAKY_BUCKET_NUMBER   = ALTI_CTP_DTM_DEADTIMECONFIG_BITSTRING::LEAKYBUCKETENABLE_NUMBER; // 4 
    static const unsigned int   LEAKY_BUCKET_RATE     = 0x00000fff;
    static const unsigned int   LEAKY_BUCKET_LEVEL    = 0x000000ff;
    static const unsigned int   SLIDING_WINDOW_SIZE   = 0x00003fff;
    static const unsigned int   SLIDING_WINDOW_NUMBER = 0x000000ff;    
    static const unsigned int   BGRP_WORD_TOTAL      = BGRP_NUMBER*BCID_WORD_NUMBER;
    static const unsigned int   RNDM_NUMBER          = ALTI::CTP_RND_THRESHOLD_NUMBER;
    static const unsigned int   RNDM_SEED_MASK       = ALTI::MASK_CTP_RND_SEED;
    static const unsigned int   RNDM_THRESHOLD_MASK  = ALTI::MASK_CTP_RND_THRESHOLD;
    static const unsigned int   PRSC_SEED_MASK       = ALTI::MASK_CTP_PSC_SEED;
    static const unsigned int   PRSC_THRESHOLD_MASK  = ALTI::MASK_CTP_PSC_THRESHOLD;
    static const unsigned int   PRSC_RANDOM_FACTOR   = (static_cast<unsigned long long>(PRSC_SEED_MASK) + 1ULL) /
                                                       (static_cast<unsigned long long>(PRSC_THRESHOLD_MASK) + 1ULL);
    static const unsigned int   ITEM_LUT_SIZE        = ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER;
    static const unsigned int   TTYP_LUT_SIZE        = ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER;

    typedef enum
    {
      MINICTP_L1A, MINICTP_TTR1, MINICTP_TTR2, MINICTP_TTR3, MINICTP_BGO2, MINICTP_BGO3, MINICTP_CALREQ0, MINICTP_CALREQ1, MINICTP_CALREQ2, MINICTP_RND0, MINICTP_RND1} MINICTP_INPUT;
    static const unsigned int MINICTP_INPUT_NUMBER = MINICTP_RND1 + 1;
    static const std::string MINICTP_INPUT_NAME[MINICTP_INPUT_NUMBER];

    typedef enum {MINICTP_TBP, MINICTP_TAP, MINICTP_TAV} MINICTP_CNT_TYPE;
    static const unsigned int MINICTP_CNT_NUMBER = MINICTP_TAV + 1;
    static const std::string MINICTP_CNT_NAME[MINICTP_CNT_NUMBER];
    static const unsigned int MINICTP_CNT_SIZE_TOTAL = MINICTP_CNT_NUMBER*ITEM_NUMBER + 1; // 8 items * 3 + L1A 

    int CTPInputL1aRead(std::string &);
    int CTPInputL1aWrite(const std::string); 
    int CTPInputBgo2Read(std::string &);
    int CTPInputBgo2Write(const std::string);
    int CTPInputBgo3Read(std::string &);
    int CTPInputBgo3Write(const std::string);
    int CTPInputTtrRead(const u_int, std::string &);
    int CTPInputTtrWrite(const u_int, const std::string);
    int CTPInputCalreqRead(const u_int, std::string &);
    int CTPInputCalreqWrite(const u_int, const std::string);

    int CTPBunchGroupMaskRead(const unsigned int, unsigned int &);
    int CTPBunchGroupMaskWrite(const unsigned int, const unsigned int);
    int CTPBunchGroupMaskRead(const unsigned int, bool &, bool &);
    int CTPBunchGroupMaskWrite(const unsigned int, const bool, const bool);
    int CTPBunchGroupMaskRead(std::vector<unsigned int>&);
    int CTPBunchGroupMaskWrite(const std::vector<unsigned int>);
    int CTPBunchGroupMaskRead(const std::string&); 
    int CTPBunchGroupMaskWrite(const std::string&);
    int CTPBunchGroupDataRead(const unsigned int, const unsigned int, bool&);
    int CTPBunchGroupDataWrite(const unsigned int, const unsigned int, const bool);
    int CTPBunchGroupDataRead(const unsigned int, std::vector<bool>&);
    int CTPBunchGroupDataWrite(const unsigned int, const std::vector<bool>);
    int CTPBunchGroupDataRead(std::vector<unsigned int>&);
    int CTPBunchGroupDataWrite(const std::vector<unsigned int>);
    int CTPBunchGroupDataRead(const std::string&); 
    int CTPBunchGroupDataWrite(const std::string&);
    int CTPBunchGroupBcidOffsetRead(unsigned int&);
    int CTPBunchGroupBcidOffsetWrite(const unsigned int);
    int CTPBunchGroupItemEnabledRead(const unsigned int, bool&);
    int CTPBunchGroupItemEnabledWrite(const unsigned int, const bool);
    int CTPBunchGroupItemEnabledRead(unsigned int&);
    int CTPBunchGroupItemEnabledWrite(const unsigned int);

    int CTPPrescalerSeedRead(const unsigned int, unsigned int&);
    int CTPPrescalerSeedWrite(const unsigned int, const unsigned int);
    int CTPPrescalerSeedRead(std::vector<unsigned int>&);
    int CTPPrescalerSeedWrite(const std::vector<unsigned int>);
    int CTPPrescalerSeedWrite(const unsigned int);
    int CTPPrescalerSeedRead(const std::string);
    int CTPPrescalerSeedWrite(const std::string);
    int CTPPrescalerThresholdRead(const unsigned int, unsigned int&);
    int CTPPrescalerThresholdRead(std::vector<unsigned int>&);
    int CTPPrescalerThresholdRead(const std::string);
    int CTPPrescalerThresholdWrite(const unsigned int, const unsigned int);
    int CTPPrescalerThresholdWrite(const std::vector<unsigned int>);
    int CTPPrescalerThresholdWrite(const unsigned int);
    int CTPPrescalerThresholdWrite(const std::string);
    int CTPPrescalerToThreshold(const double, unsigned int&);
    int CTPThresholdToPrescaler(const unsigned int, double& );

    int CTPRandomTriggerThresholdWrite(const unsigned int, const unsigned int);
    int CTPRandomTriggerThresholdRead(const unsigned int, unsigned int&);
    int CTPRandomTriggerSeedWrite(const unsigned int, const unsigned int);
    int CTPRandomTriggerSeedRead(const unsigned int, unsigned int&);
    
    int CTPLeakyBucketConfigRead(const unsigned int, unsigned int&, unsigned int&);
    int CTPLeakyBucketConfigWrite(const unsigned int, const unsigned int, const unsigned int);
    int CTPLeakyBucketEnableRead(const unsigned int, bool&);
    int CTPLeakyBucketEnableWrite(const unsigned int, const bool);
    int CTPSlidingWindowConfigRead(unsigned int&, unsigned int&);
    int CTPSlidingWindowConfigWrite(const unsigned int, const unsigned int);
    int CTPSlidingWindowEnableRead(bool&);
    int CTPSlidingWindowEnableWrite(const bool);
    int CTPSimpleDeadtimeWidthRead(unsigned int&);
    int CTPSimpleDeadtimeWidthWrite(const unsigned int);
    int CTPTavEnableRead(const unsigned int, bool&);
    int CTPTavEnableWrite(const unsigned int, const bool);
    int CTPTavEnableRead(unsigned int&);
    int CTPTavEnableWrite(const unsigned int);

    int CTPTriggerLutRead(std::vector<unsigned int> &);
    int CTPTriggerLutWrite(const std::vector<unsigned int>);
    int CTPTriggerLutRead(const std::string);
    int CTPTriggerLutWrite(const std::string);
    int CTPTtypLutRead(std::vector<unsigned int> &);
    int CTPTtypLutWrite(const std::vector<unsigned int>);
    int CTPTtypLutRead(const std::string );
    int CTPTtypLutWrite(const std::string);
    
    int CTPCntBcidMaskRead(const unsigned int, bool &);
    int CTPCntBcidMaskWrite(const unsigned int, const bool);
    int CTPCntBcidMaskRead(std::vector<bool> &);
    int CTPCntBcidMaskWrite(const std::vector<bool> &);
    int CTPCntBcidMaskRead(std::vector<unsigned int> &);
    int CTPCntBcidMaskWrite(const std::vector<unsigned int> &);
    int CTPCntBcidMaskRead(const std::string &);
    int CTPCntBcidMaskWrite(const std::string &);
    int CTPCntBcidOffsetRead(unsigned int &);
    int CTPCntBcidOffsetWrite(const unsigned int);
    int CTPCntControlPrint();
    int CTPCntEnableRead(bool &, bool &);
    int CTPCntEnableWrite(const bool);
    int CTPCntEnableWait(const bool);
    int CTPCntClearWrite();
    int CTPCntClearWait();
    int CTPCntCopyWrite();
    int CTPCntCopyWait();
    int CTPCntCopyClearWrite();
    int CTPCntCopyClearWait();
    int CTPCntTurnCountRead(unsigned int &);
    int CTPCntCounterRead(AltiMiniCtpCounter *);
    int CTPCntTbpCounterRead(const unsigned int, unsigned int&);
    int CTPCntTapCounterRead(const unsigned int, unsigned int&);
    int CTPCntTavCounterRead(const unsigned int, unsigned int&);
    int CTPCntL1aCounterRead(unsigned int&);
    int CTPCntRequestPrint();
    int CTPCntRequestWrite(const bool, const bool, const bool);

    // public CS/CSR constants
    //static const unsigned int   SLOT_NUMBER             = 18;

    // 0x42000000: D32

    static const unsigned int   MANUF_ID_BASE           = 0x40000023;
    static const unsigned int   MANUF_ID_MASK           = 0x00ffffff;
    static const unsigned int   BOARD_ID_BASE           = 0x40000033;
    static const unsigned int   REV_ID_BASE             = 0x40000043;
    static const unsigned int   SLAVE_CHARACT_ID_BASE   = 0x400000e0;

    static const unsigned int   BAR                     = 0x1007ffff;
    static const unsigned int   ADER_BASE               = 0x4007ff63;
    static const unsigned int   ICSR_BASE               = 0x2007fbd3;
    static const unsigned int   ISTAT_BASE              = 0x4007fb93;
    static const unsigned int   RCSR_BASE               = 0x1007fbf3;

    static const unsigned int   MANUF_ID                = 0x00080030;
    static const unsigned int   BOARD_ID                = 0x000001a9;
    static const unsigned int   REV_ID                  = 0x1241a001; //= 0x00000001;
    static const unsigned int   SLAVE_CHARACT_ID        = 0x00000007; //= 0x000000ef;

    // public helpers
    // undefined
    //static unsigned int swapEndian(const unsigned int); // 4 bytes
    //static unsigned int swapEndian(const unsigned int, const unsigned int); // n msb/lsb are swapped
    static unsigned int pollSlots(std::vector<unsigned int> &);

    // public function to enable access to the low-level functions generated from the XML
    ALTI* get_alti() { return m_alti; } 

  private:

    // private I2C constants
    static const unsigned int I2C_PRESCALE_VALUE = 0x4f;

    // private SWX constants
    static const SIGNAL_SOURCE SWX_MAP_SRC[SIGNAL_NUMBER_ROUTED][SIGNAL_SOURCE_NUMBER - 1];
    static const SIGNAL_SOURCE SWX_MAP_SRC_INV[SIGNAL_NUMBER_ROUTED][SIGNAL_SOURCE_NUMBER - 1];
    static const SIGNAL_DESTINATION SWX_MAP_DST[SIGNAL_NUMBER_ROUTED][SIGNAL_DESTINATION_NUMBER];

    // private EQZ constants
    static const unsigned int EQUALIZER_CONFIG_VOLTAGE[EQUALIZER_CONFIG_NUMBER][EQUALIZER_VOLTAGE_NUMBER];

    // private TTC constants
    typedef struct BgoMode {
        TTC_BGO_MODE_TRIGGER mode_trigger;
        TTC_BGO_MODE_COMMAND mode_command;
        TTC_BGO_MODE_REPEAT mode_repeat;
        TTC_BGO_MODE_FIFO mode_fifo;
        TTC_BGO_MODE_INHIBIT mode_inh;
    } BgoMode;
    static const BgoMode TTC_BGO_MODE_HL[TTC_BGO_MODE_NUMBER];

    // private MEM signal bit-assignments
    static const unsigned int MEM_MASK[SIGNAL_NUMBER];
    //const unsigned int MEM_MASK[SIGNAL_NUMBER];

    // private JitterCleaner constants
    static const uint8_t        PAGE_ADDR_SHFT          =    8;
    static const uint8_t        PAGE_ADDR_MASK          = 0xff;
    static const uint8_t        ADDR_ADDR_MASK          = 0xff;
    static const unsigned int   THOUSAND                = 1000;
    
    // private JitterCleaner methods
    int CLKJitterCleanerIntFlagsRead(std::vector<bool> &, std::vector<bool> &, bool &, bool &, bool &, bool &, bool &, bool &);
    int CLKJitterCleanerIntMasksRead(std::vector<bool> &, std::vector<bool> &, bool &, bool &, bool &, bool &, bool &, bool &);
    
    // private high-level methods
    int CFGRead(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWrite(const AltiConfiguration &, std::ostream & = std::cerr);                         
    int CFGReadCLK(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWriteCLK(const AltiConfiguration &, std::ostream & = std::cerr);
    int CFGReadSIG(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWriteSIG(const AltiConfiguration &, std::ostream & = std::cerr);
    int CFGReadBSY(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWriteBSY(const AltiConfiguration &, std::ostream & = std::cerr);
    int CFGReadCRQ(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWriteCRQ(const AltiConfiguration &, std::ostream & = std::cerr);
    int CFGReadMEM(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWriteMEM(const AltiConfiguration &, std::ostream & = std::cerr);
    int CFGReadTTC(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWriteTTC(const AltiConfiguration &, std::ostream & = std::cerr);
    int CFGReadCNT(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWriteCNT(const AltiConfiguration &, std::ostream & = std::cerr);
    int CFGReadCTP(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWriteCTP(const AltiConfiguration &, std::ostream & = std::cerr);
    int CFGReadPBM(AltiConfiguration &, std::ostream & = std::cerr);
    int CFGWritePBM(const AltiConfiguration &, std::ostream & = std::cerr);

    // private SWX methods
    int SIGSwitchMasterConfigurationWrite();
    int SIGSwitchCtpSlaveConfigurationWrite();
    int SIGSwitchAltiSlaveConfigurationWrite();
    int SIGSwitchLemoSlaveConfigurationWrite();

   
    // private CR/CSR methods
    // for setting base address with knowing slot number only, without the corresponding AltiModule object
    static int CSRBoardIdRead(int, unsigned int &);
    static int CSRBARRead(int, unsigned int &);
    static int CSRADERRead(int, unsigned int &);
    static int CSRADERWrite(int, const unsigned int);
    static int CSRADER1Read(int, unsigned int &);
    static int CSRADER1Write(int, const unsigned int);
    
    // private BSY constants
    static const unsigned int BSY_CONTROL_TIMEOUT     = 100000;
    // private BM constant
    static const unsigned int BSY_CNT_TIMEOUT          = 100000;

    // private ENC constants
    static const unsigned int TTYP_DELAY_FOR_LATCHING = 0x002;
    
    // private CNT constants
    static const unsigned int CNT_CONTROL_TIMEOUT     = 1000;
     
    // private helpers
    //unsigned int swapEndian(unsigned int);
    int CLKSetup();
    int CLKCheck();
    int I2CInit();
    int I2CCheck();
    int JCSetup();
    int ENCSetup();
    int ENCCheck();
    int runDelay(const unsigned int, const std::string &);
    int pagedI2CDeviceRead(const I2CNetwork::I2CNode &, const uint8_t, const uint8_t, unsigned int &);
    int pagedI2CDeviceWrite(const I2CNetwork::I2CNode &, const uint8_t, const uint8_t, const unsigned int);
      
    // private members
    int                                         m_status;
    RCD::VME*                                   m_vme;
    RCD::VMEMasterMap*                          m_vmm;
    unsigned int                                m_slot;
    unsigned int                                m_ader;

    ALTI*                                       m_alti;
    RCD::CMEMSegment*                           m_segment;
    I2C*                                        m_i2c;
    I2CNetwork*                                 m_net;
    
    std::string                                 m_mem_pg_file;
    std::string                                 m_cnt_ttyp_lut_file;
    
    // private I2C nodes
    // switches
    I2CNetwork::I2CNode                         m_SignalSwitchConfig[SIGNAL_NUMBER_ROUTED]; // BUSY & CALREQ0..2 don't get routed
    I2CNetwork::I2CNode                         m_SignalSwitchCtrl[SIGNAL_NUMBER_ROUTED]; // BUSY & CALREQ0..2 don't get routed
    I2CNetwork::I2CNode                         m_SignalSwitchLos[SIGNAL_NUMBER_ROUTED]; // BUSY & CALREQ0..2 don't get routed
    // DACs for equalizers
    // CTP_IN & ALTI_IN have equalizers
    I2CNetwork::I2CNode                         m_EqualizerConfig[SIGNAL_SOURCE_NUMBER - 3][EQUALIZER_VOLTAGE_NUMBER];

    // SI5344B jitter cleaner
    I2CNetwork::I2CNode                         m_SI5344BStatusFlg;
    I2CNetwork::I2CNode                         m_SI5344BOofLosFlg;
    I2CNetwork::I2CNode                         m_SI5344BHoldLolFlg;
    I2CNetwork::I2CNode                         m_SI5344BPllCalFlg;
    I2CNetwork::I2CNode                         m_SI5344BStatusStickyFlg;
    I2CNetwork::I2CNode                         m_SI5344BOofLosStickyFlg;
    I2CNetwork::I2CNode                         m_SI5344BHoldLolStickyFlg;
    I2CNetwork::I2CNode                         m_SI5344BPllCalStickyFlg;
    I2CNetwork::I2CNode                         m_SI5344BStatusIntrMsk;
    I2CNetwork::I2CNode                         m_SI5344BOofLosIntrMsk;
    I2CNetwork::I2CNode                         m_SI5344BHoldLolIntrMsk;
    I2CNetwork::I2CNode                         m_SI5344BPllCalIntrMsk;
    I2CNetwork::I2CNode                         m_SI5344BPdnHardRstSync;
    I2CNetwork::I2CNode                         m_SI5344BDesignId;
    
    DS1WM*                                      m_ds1wm;
    std::string                                 m_identifier;
    /*
    // menu program is for experts, but individual EQZ volatage configuration is not in the public API
    friend class ::AltiSIGEqualizersConfigRead;
    friend class ::AltiSIGEqualizersConfigWrite;

    // menu program is for experts, but individual bits for BGO command mode should not be in the public API
    friend class  ::AltiENCBgoCommandModeRead;
    friend class  ::AltiENCBgoCommandModeWrite;
      */
};     // class AltiModule

}      // namespace LVL1

#endif // _ALTIMODULE_H_
