# ATLAS Local Trigger Interface (ALTI): low-level software (30.1.2019.)


## Installation and documentation 

https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LevelOneCentralTriggerALTI

## Single-board computer (SBC) configuration
Before running the ALTI low-level software, one must make sure that the PCI-to-VME static mappings of the SBC have been properly configured. This configuration is embodied in the **vmetab** file, which configures the PCI-to-VME bridge chip on the SBC. Depending on the type of the SBC and the corresponding bridge chip (**Tundra Universe II** vs. **TSI148**), one should use the appropriate program from the [vme_rcc](https://gitlab.cern.ch/atlas-tdaq-software/vme_rcc) driver library (**vmeconfig** vs. **tsiconfig**, respectively). The following example is for the **sbctest-917** Concurrent Technologies (CCT) **VP-917** type of the SBC:

<code>
<pre>
[pkuzmano@sbctest-917 l1ct-07-01-16] <b>tsiconfig -a vmetab/vmetab.sbc-lar-vp917</b>
Board type: VP917
Notice: Enabling MANUAL mode, as 4 MANUALLY configured master map(s) were found.
update_all called
hm_base  = 0x0000000250000000
hm_limit = 0x0000000370000000
b1 = 0x01, b2 = 0x50, b3 = 0x01, b4 = 0x70
config_data = 0x70015001
config_data = 0x00000002
config_data = 0x00000003
Tsi148 registers programmed
VME_RCC driver synchronized
tsiconfig: VMEbus interface initialised
[pkuzmano@sbctest-917 l1ct-07-01-16]$ 
</pre>
</code>

An example of **VPE24** SBCs is **sbcl1ct-27**:

<code>
<pre>
amarzin@sbcl1ct-27:/mnt/ctpfs/amarzin/alti$ vmeconfig -a /daq_area/detectors/sbcl1ct/vmetab/vmetab.sbcl1ct-27
Notice: Enabling MANUAL mode, as 5 MANUALLY configured master map(s) were found.
Universe registers programmed
VME_RCC driver synchronized
vmeconfig: VMEbus interface initialised
</pre>
</code>


After this, the SBC configuration can be checked using the **cctscope**/**tsiscope** program (correspoding to vmeconfig and tsiconfig, respectively):

<code>
<pre>
[pkuzmano@sbctest-917 l1ct-07-01-16]$ <b>tsiscope</b>
Searching for Board Id Structure
Board ID table found
Board Id Structure Version:2
Board Type:2
Board Id:31,0



This is TSISCOPE for a VP917
\=======================================

Select an option:
   1 Help                        2 Decode VME registers
   3 Decode PCI registers        4 Show special registers
   5 Toggle SYSRESET bit         6 Modify a register
   7 Test self addressing
   0 Quit
Your choice: [1] :<b>2</b>

Select an option:
   1 Help                        2 Decode master mapping
   3 Decode DMA registers        4 Decode interrupt status
   5 Decode misc. registers      6 Decode BERR information
   7 Decode slave mapping        8 Master map raw register values
   9 Modify master map 1
   0 Main menu
Your choice: [1] :<b>2</b>

=======================================================================================
LSI|                       VME address range|                       PCI address range|  EN| MRPFD|     PFS|     2eSST|        TM|       DBW|  SUP|  PGM|     AM|    OTBS
===|========================================|========================================|====|======|========|==========|==========|==========|=====|=====|=======|========
  0| 0x0000000000000000 - 0x0000000015ffffff| 0x0000000250000000 - 0x0000000265ffffff| Yes|    No| 2 lines|  160 MB/s|    single|    32 bit| User| Data|    A32| 0x00000
  1| 0x0000000000000000 - 0x0000000000ffffff| 0x0000000266000000 - 0x0000000266ffffff| Yes|    No| 2 lines|  160 MB/s|    single|    32 bit| User| Data| CR/CSR| 0x00000
  2| 0x0000000000000000 - 0x0000000000ffffff| 0x0000000267000000 - 0x0000000267ffffff| Yes|    No| 2 lines|  160 MB/s|    single|    32 bit| User| Data|    A24| 0x00000
  3| 0x0000000000000000 - 0x000000000000ffff| 0x0000000268000000 - 0x000000026800ffff| Yes|    No| 2 lines|  160 MB/s|    single|    32 bit| User| Data|    A16| 0x00000
  4| 0x0000000000000000 - 0x000000000000ffff| 0x0000000000000000 - 0x000000000000ffff|  No|    No| 2 lines|  160 MB/s|    single|    16 bit| User| Data|    A16| 0x00000
  5| 0x0000000000000000 - 0x000000000000ffff| 0x0000000000000000 - 0x000000000000ffff|  No|    No| 2 lines|  160 MB/s|    single|    16 bit| User| Data|    A16| 0x00000
  6| 0x0000000000000000 - 0x000000000000ffff| 0x0000000000000000 - 0x000000000000ffff|  No|    No| 2 lines|  160 MB/s|    single|    16 bit| User| Data|    A16| 0x00000
  7| 0x0000000000000000 - 0x000000000000ffff| 0x0000000000000000 - 0x000000000000ffff|  No|    No| 2 lines|  160 MB/s|    single|    16 bit| User| Data|    A16| 0x00000
\=======================================================================================

Select an option:
   1 Help                        2 Decode master mapping
   3 Decode DMA registers        4 Decode interrupt status
   5 Decode misc. registers      6 Decode BERR information
   7 Decode slave mapping        8 Master map raw register values
   9 Modify master map 1
   0 Main menu
Your choice: [2] :
</pre>
</code>

From the static mappings information, one can see which address ranges can be used for addressing the ALTI module(s). Besides the A32 mapping for the ALTI, it is important that the configuration has a properly defined CR/CSR mapping and the A24 mapping (for addressing A24 modules like LTP and LTPI).

## Getting started

After power-on or reset, the first thing to do is to **set up the base address(es)** of the ALTI module(s) and do the software **reset**, **setup** and **check** of the module(s). All of these functions can be done using the **testAltiInitial** program with the proper command line arguments. For example, the ALTI module in slot 9 can be initialised in the following way:

<code>
<pre>
[pkuzmano@sbctest-917 l1ct-07-01-16]$ <b>testAltiInitial -s 9 -B -b 0x08000000 -R -S -c</b>
2018-11-30 15:21:46 - INFO  in "daq::tmgr::TestResult LVL1::TestAltiInitial::init()"
--------------------------------------------------------------------------------
testAltiInitial:
--------------------------------------------------------------------------------
ALTI slot number             = 09
ALTI VME base address        = 0x08000000
--------------------------------------------------------------------------------
Reset  ALTI                  = "YES"
Setup  ALTI                  = "YES"
Config ALTI                  = "NO"
Change ALTI VME base address = "YES", 0x08000000
--------------------------------------------------------------------------------
Check  ALTI                  = "YES"
Read   ALTI config           = "NO"
--------------------------------------------------------------------------------
ALTI: INFO - ALTI detected, BAR = 0x00f80009 <-> slot 9
ALTI: INFO - ALTI detected, revision = 0x12b01001 (#1, 01.11.2018.), BAR = 0x00f80009 <-> slot 9
ALTI: INFO - ALTI opened at vme = 0x08000000, size = 0x00800000, type = "A32"
DS1WM::DS1WM: INFO - opened DS1WM of type "ALTI" at offset 0x000080a0
I2C::I2C: INFO - opened I2C of type "ALTI" at offset 0x00009080
2018-11-30 15:21:46 - INFO  in "LVL1::AltiModule::AltiModule(unsigned int)":
>> opened CMEM segment "AltiModule" of size 0x00400000, phys 0ix0ec00000, virt 0x30454000
--------------------------------------------------------------------------------
ALTI[slot  9]: INFO - ALTI "20DATALTI00002" opened
--------------------------------------------------------------------------------
>> TestAltiInitial::init ................................................ [PASS]
--------------------------------------------------------------------------------
wrote 326 (addr,data) lines to CLK jitter cleaner
2018-11-30 15:21:48 - INFO  in "int LVL1::AltiModule::AltiCheck()":
>> ALTI: INFO - CLK mux               = "SETUP"
2018-11-30 15:21:48 - INFO  in "int LVL1::AltiModule::AltiCheck()":
>> ALTI: INFO - I2C core              = "SETUP"
2018-11-30 15:21:48 - INFO  in "int LVL1::AltiModule::AltiCheck()":
>> ALTI: INFO - PLL                   = "LOCKED"
2018-11-30 15:21:48 - INFO  in "int LVL1::AltiModule::AltiCheck()":
>> ALTI: INFO - jitter cleaner design = "ALTI_001" (DEFAULT)
2018-11-30 15:21:48 - INFO  in "int LVL1::AltiModule::AltiCheck()":
>> ALTI: INFO - ADER1 page register   = "SETUP"
2018-11-30 15:21:48 - INFO  in "int LVL1::AltiModule::AltiCheck()":
>> ALTI: INFO - TTC encoder           = "SETUP"
>> TestAltiInitial::exec ................................................ [PASS]
--------------------------------------------------------------------------------
>> TestAltiInitial::test ................................................ [PASS]
--------------------------------------------------------------------------------
[pkuzmano@sbctest-917 l1ct-07-01-16]$ 
</pre>
</code>

As can be seen from the above, the address **0x08000000** fits in the A32 static mapping, and thus can be used as a valid VME base address of the ALTI module. The reset procedure (**-R**) initialises the FPGA firmware of the module properly, while the setup procedure (**-S**) configures the necessary board parameters like jitter cleaner settings and cable equalizers. These settings should normally not be modified by the user, and the check procedure (**-c**) checks their consistency.

After the module initialisation, the user can configure the module at will. One way to do this is with the use of interactive **menuAltiModule** program. Another way is to use the global configuration of the module. The configuration is given in a file that containts the list of all the configurable parameters, specified in a form of key and value pairs. Several configuration files have been prepared in order to use the module in a commonly used mode (master mode, various slave modes, etc.). Of course, custom configuration is also possible. Configuration is also done with the **testAltiInitial** program, using the **-C** flag and specifying the configuration file path (**-f**):

<code>
<pre>
[pkuzmano@sbctest-917 l1ct-07-01-16]$ <b>testAltiInitial -s 9 -C -f ALTI/data/AltiModule_Pattern_Generator_cfg.dat</b>
2018-12-03 10:22:30 - INFO  in "daq::tmgr::TestResult LVL1::TestAltiInitial::init()"
--------------------------------------------------------------------------------
testAltiInitial:
--------------------------------------------------------------------------------
ALTI slot number             = 09
ALTI VME base address        = 0x10000000
--------------------------------------------------------------------------------
Reset  ALTI                  = "NO"
Setup  ALTI                  = "NO"
Config ALTI                  = "YES", "ALTI/data/AltiModule_Pattern_Generator_cfg.dat"
Change ALTI VME base address = "NO"
--------------------------------------------------------------------------------
Check  ALTI                  = "NO"
Read   ALTI config           = "NO"
--------------------------------------------------------------------------------
ALTI: INFO - ALTI detected, revision = 0x12b01001 (#1, 01.11.2018.), BAR = 0x00f80009 <-> slot 9
ALTI: INFO - ALTI opened at vme = 0x08000000, size = 0x00800000, type = "A32"
DS1WM::DS1WM: INFO - opened DS1WM of type "ALTI" at offset 0x000080a0
I2C::I2C: INFO - opened I2C of type "ALTI" at offset 0x00009080
2018-12-03 10:22:30 - INFO  in "LVL1::AltiModule::AltiModule(unsigned int)":
>> opened CMEM segment "AltiModule" of size 0x00400000, phys 0ix0ec00000, virt 0x26666000
--------------------------------------------------------------------------------
ALTI[slot  9]: INFO - ALTI "20DATALTI00002" opened
--------------------------------------------------------------------------------
>> TestAltiInitial::init ................................................ [PASS]
--------------------------------------------------------------------------------
2018-12-03 10:22:30 - INFO  in "int LVL1::AltiConfiguration::read(const string&)":
>> read AltiConfiguration from file "ALTI/data/AltiModule_Pattern_Generator_cfg.dat"
>> TestAltiInitial::exec ................................................ [PASS]
--------------------------------------------------------------------------------
>> TestAltiInitial::test ................................................ [PASS]
--------------------------------------------------------------------------------
[pkuzmano@sbctest-917 l1ct-07-01-16]$ 
</pre>
</code>

Initialisation and configuration procuedures can also be combined and done with a single call of the testAltiInitial program.

## Expect scripts
Several **expect**/**tcl** scripts have been prepared in order to execute some common procedures more easily. These include the following:

1) **ALTI_Pattern.exp**: used for setting the pattern generator with the given file,
2) **ALTI_Snapshot.exp** : used for taking the snapshot and storing it in a given file (human-readable form),
3) **ALTI_BGO_Inhibit.exp** : used for setting the width/delay parameters of the given BGO channel.

All of these expect scripts are written around the **menuAltiModule** program, i.e. they automate the interaction between the user and this menu program in order to perform a specific task.

### ALTI_Pattern.exp

The expect script has the following parameters (ordered):
* **$alti_slot** : slot number of the ALTI module
* **$pg_filename** : name of the pattern file to be loaded

Example of use:

<code>
<pre>
[pkuzmano@sbctest-917 l1ct-07-01-16]$ <b>expect ALTI/expect/ALTI_Pattern.exp 13 ALTI/data/pg_alti.dat</b>
</pre>
</code>

Note that the module in slot <b>13</b> should be configured in master (pattern generator) mode before running this script.

### ALTI_Snapshot.exp

The expect script has the following parameters (ordered):
* **$alti_slot** : slot number of the ALTI module
* **$snap_filename** : name of the snapshot file to be saved
* **$num_entries** : number of snapshot entries to be read (decimal)

Example of use:

<code>
<pre>
[pkuzmano@sbctest-917 l1ct-07-01-16]$ <b>expect ALTI/expect/ALTI_Snapshot.exp 18 snap.out 1048576</b>
</pre>
</code>

Note that the module in slot <b>18</b> should be configured in slave mode before running this script.

### ALTI_BGO_Inhibit.exp

The expect script has the following parameters (ordered):
* **$alti_slot** : slot number of the ALTI module
* **$bgo_index** : BGO channel index (0..3)
* **$bgo_inh_width** : Inhibit signal width, in BCs (0..4095)
* **$bgo_inh_delay** : Inhibit signal delay, in BCs (0..4095)

Example of use:

<code>
<pre>
[pkuzmano@sbctest-917 l1ct-07-01-16]$ <b>expect ALTI/expect/ALTI_BGO_Inhibit.exp 18 0 48 100</b>
</pre>
</code>


## Remote firmware update

The remote firmware update feature is implemented using the [QuickBoot metod](https://www.xilinx.com/support/documentation/application_notes/xapp1081-quickboot-remote-update.pdf). It allows the user to update the firmware over the VME, not having to use the JTAG programmer and the Xilinx tools. For this, one should use the **testAltiQuickBoot** program. One just has to specify the file path to the appropriate **bin** file of the update image (**-f**):

<code>
<pre>
[pkuzmano@sbctest-917 l1ct-07-01-16]$ <b>testAltiQuickBoot -s 9 -f fw/bin/top_alti_update_20181101.bin</b>
ALTI: INFO - ALTI detected, revision = 0x12b01001 (#1, 01.11.2018.), BAR = 0x00f80009 <-> slot 9
ALTI: INFO - ALTI opened at vme = 0x08000000, size = 0x00800000, type = "A32"
DS1WM::DS1WM: INFO - opened DS1WM of type "ALTI" at offset 0x000080a0
I2C::I2C: INFO - opened I2C of type "ALTI" at offset 0x00009080
2018-12-03 11:42:08 - INFO  in "LVL1::AltiModule::AltiModule(unsigned int)":
>> opened CMEM segment "AltiModule" of size 0x00400000, phys 0ix0ec00000, virt 0x72145000
--------------------------------------------------------------------------------
ALTI[slot  9]: INFO - ALTI "20DATALTI00002" opened
--------------------------------------------------------------------------------
FIFO pointers and state machine reset
file length (bytes): 0x01000000
update_img_len (32b words): 0x00200000
Empty
...
<b>Update done: OK</b>
FIFO pointers and state machine reset
[pkuzmano@sbctest-917 l1ct-07-01-16]$ 
</pre>
</code>

After the update has finished successfully, one must reset the board (or power-cycle the crate) in order for the new FPGA firmware to be loaded. Then, the same testAltiQuickBoot program can be used to check (**-C**) and verify (**-V**) the updated image:

<code>
<pre>
[pkuzmano@sbctest-917 l1ct-07-01-16]$ <b>testAltiQuickBoot -s 9 -C</b>
ALTI: INFO - ALTI detected, revision = 0x12b01001 (#1, 01.11.2018.), BAR = 0x00f80009 <-> slot 9
ALTI: INFO - ALTI opened at vme = 0x08000000, size = 0x00800000, type = "A32"
DS1WM::DS1WM: INFO - opened DS1WM of type "ALTI" at offset 0x000080a0
I2C::I2C: INFO - opened I2C of type "ALTI" at offset 0x00009080
2018-12-03 11:53:35 - INFO  in "LVL1::AltiModule::AltiModule(unsigned int)":
>> opened CMEM segment "AltiModule" of size 0x00400000, phys 0ix0ec00000, virt 0xf5fe8000
--------------------------------------------------------------------------------
ALTI[slot  9]: INFO - ALTI "20DATALTI00002" opened
--------------------------------------------------------------------------------
FIFO pointers and state machine reset
<b>Check ID done: OK</b>
FIFO pointers and state machine reset
[pkuzmano@sbctest-917 l1ct-07-01-16]$ <b>testAltiQuickBoot -s 9 -V</b>
ALTI: INFO - ALTI detected, revision = 0x12b01001 (#1, 01.11.2018.), BAR = 0x00f80009 <-> slot 9
ALTI: INFO - ALTI opened at vme = 0x08000000, size = 0x00800000, type = "A32"
DS1WM::DS1WM: INFO - opened DS1WM of type "ALTI" at offset 0x000080a0
I2C::I2C: INFO - opened I2C of type "ALTI" at offset 0x00009080
2018-12-03 11:53:38 - INFO  in "LVL1::AltiModule::AltiModule(unsigned int)":
>> opened CMEM segment "AltiModule" of size 0x00400000, phys 0ix0ec00000, virt 0x387da000
--------------------------------------------------------------------------------
ALTI[slot  9]: INFO - ALTI "20DATALTI00002" opened
--------------------------------------------------------------------------------
FIFO pointers and state machine reset
<b>Verify done: OK</b>
<b>CRC32 byte 3: 0x95</b>
FIFO pointers and state machine reset
[pkuzmano@sbctest-917 l1ct-07-01-16]$ 
</pre>
</code>

More on the QuickBoot mechanism and the instructions on how to generate the initial/update images can be found in the [ALTI firmware repository](https://gitlab.cern.ch/atlas-ttc-firmware/alti-firmware) documentation.
