//******************************************************************************
// file: AltiCommon.cc
// desc: pure status class for ALTI common parameters
// auth: 05-DEC-2017 P. Kuzmanovic
//******************************************************************************

// $Id: AltiCommon.cc,v 1.15 2009/06/24 09:27:16 spiwoks Exp $

#include <sys/time.h>
//#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <algorithm>
//#include <stdexcept>
//#include "boost/crc.hpp"
//#include <sys/resource.h>
#include <cmath>

//#include "rcc_error/rcc_error.h"
#include "RCDUtilities/RCDUtilities.h"
#include "ALTI/AltiCommon.h"

using namespace LVL1;

//------------------------------------------------------------------------------

const double AltiCommon::BC_FREQUENCY = 40079000.0;

const unsigned int AltiCommon::SUBSTR_MASK[WORD_SIZE+1] = {
    0x00000000, 0x00000001, 0x00000003, 0x00000007, 0x0000000f, 0x0000001f, 0x0000003f, 0x0000007f, 0x000000ff,
                0x000001ff, 0x000003ff, 0x000007ff, 0x00000fff, 0x00001fff, 0x00003fff, 0x00007fff, 0x0000ffff,
                0x0001ffff, 0x0003ffff, 0x0007ffff, 0x000fffff, 0x001fffff, 0x003fffff, 0x007fffff, 0x00ffffff,
                0x01ffffff, 0x03ffffff, 0x07ffffff, 0x0fffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff, 0xffffffff
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

double AltiCommon::getTime() {

    struct timeval      tv;
    gettimeofday(&tv,0);
    return(static_cast<double>(tv.tv_sec) + static_cast<double>(tv.tv_usec)/1000000.0);
}
//------------------------------------------------------------------------------

std::string AltiCommon::getFrequency(const double freq) {

    char buf[STRING_LENGTH];

    if(freq < 1.0e3) {
        std::sprintf(buf,"%7.3f  Hz",freq);
    }
    else if(freq < 1.0e6) {
        std::sprintf(buf,"%7.3f kHz",freq/1.0e3);
    }
    else if(freq < 1.0e9) {
        std::sprintf(buf,"%7.3f MHz",freq/1.0e6);
    }
    else if(freq < 1.0e12) {
        std::sprintf(buf,"%7.3f GHz",freq/1.0e9);
    }
    else if(freq < 1.0e15) {
        std::sprintf(buf,"%7.3f THz",freq/1.0e12);
    }
    else {
        std::sprintf(buf,"%10.3e THz",freq/1.0e12);
    }

    return(std::string(buf));
}

//------------------------------------------------------------------------------

std::string AltiCommon::getDataRate(const double rate) {

    char        buf[32];

    if(rate < 1.024e3) {
        std::sprintf(buf,"%7.3f  Byte/s",rate);
    }
    else if(rate < 1.048576e6) {
        std::sprintf(buf,"%7.3f kByte/s",rate/1.024e3);
    }
    else if(rate < 1.073741824e9) {
        std::sprintf(buf,"%7.3f MByte/s",rate/1.048576e6);
    }
    else if(rate < 1.099511627776e12) {
        std::sprintf(buf,"%7.3f GByte/s",rate/1.073741824e9);
    }
    else if(rate < 1.125899906842624e15) {
        std::sprintf(buf,"%7.3f TByte/s",rate/1.099511627776e12);
    }
    else {
        std::sprintf(buf,"%10.3e TByte/s",rate/1.099511627776e12);
    }

    return(std::string(buf));
}

//------------------------------------------------------------------------------

int AltiCommon::printData(const uint32_t* dptr, const int num) {

    int i;

    if(num<0) return(ERROR_WRONGPAR);

    for(i=0; i<num; i++) {
        if(i%8==0) std::printf("%6d:",i);
        std::printf(" 0x%08x",dptr[i]);
        if(i%8==7) std::cout << std::endl;
    }
    if(i%8!=0) std::cout << std::endl;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::printData(std::vector<u_int>& data, const int num) {

    int dnum = (num < 0) ? data.size() : num;
    int i;

    for(i=0; i<dnum; i++) {
        if(i%8==0) std::printf("%6d:",i);
        std::printf(" 0x%08x",data[i]);
        if(i%8==7) std::cout << std::endl;
    }
    if(i%8!=0) std::cout << std::endl;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::printData(const unsigned int d[], const unsigned int s, const int x, const int y, const int l) {

    if(s == 0) return(SUCCESS);
    if(y == 0) return(SUCCESS);
    if(l == 0) return(SUCCESS);

    int locy, locx, locl, i, j, v, w;
    char achr[STRING_LENGTH], dchr[STRING_LENGTH], xchr[STRING_LENGTH];
    bool aflg;

    // address print format
    locx = std::abs(x); if(locx > 8) locx = 8;
    if((x > 0) && (x <= 8)) {
        aflg = true;
        std::sprintf(achr,"%s%%0%dx:",x>0?"0x":"",locx);
    }
    else {
        aflg = false;
    }

    // data print format
    locy = std::abs(y); if((locy > 8)) locy = 8;
    std::sprintf(dchr," %s%%0%dx",y>0?"0x":"",locy);
    std::sprintf(xchr," %s",y>0?"  ":"");  for(i=0; i<locy; i++) strcat(xchr," ");

    // line length
    locl = std::abs(l);

    // print in order (from left to right)
    if(l>0) {
        for(i=0; i<(int)s; i++) {
            if(((i%locl) == 0) && aflg) std::printf(achr,i);
            std::printf(dchr,d[i]);
            if((i%locl) == (locl-1)) std::cout << std::endl;
        }
       if((i%locl) != 0) std::cout << std::endl;
    }

    // print in inverted order (from right to left)
    else {
        v = 0;
        for(i=0; i<((((int)s-1)/locl)+1); i++) {
            if(aflg) std::printf(achr,i);
            w = v + (locl-1);
            for(j=0; j<locl; j++) {
                if(w<(int)s) std::printf(dchr,d[w]); else std::cout << xchr;
                w--;
            }
            v += locl;
            std::cout << std::endl;
        }
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::printData(const bool d[], const unsigned int s, const int x, const int l) {

    if(s == 0) return(SUCCESS);
    if(l == 0) return(SUCCESS);

    int locx, locl, i, j, v, w;
    char achr[STRING_LENGTH];
    bool aflg;

    // address print format
    locx = std::abs(x); if(locx > 8) locx = 8;
    if((x > 0) && (x <= 8)) {
        aflg = true;
        std::sprintf(achr,"%s%%0%dx:",x>0?"0x":"",locx);
    }
    else {
        aflg = false;
    }

    // line length
    locl = std::abs(l);

    // print in order (from left to right)
    if(l>0) {
        for(i=0; i<(int)s; i++) {
            if(((i%locl) == 0) && aflg) std::printf(achr,i);
            std::printf(" %s",d[i]?"1":"0");
            if((i%locl) == (locl-1)) std::cout << std::endl;
        }
       if((i%locl) != 0) std::cout << std::endl;
    }

    // print in inverted order (from right to left)
    else {
        v = 0;
        for(i=0; i<((((int)s-1)/locl)+1); i++) {
            if(aflg) std::printf(achr,i);
            w = v + (locl-1);
            for(j=0; j<locl; j++) {
                if(w<(int)s) std::printf(" %s",d[w]?"Y":"n"); else std::cout << "  ";
                w--;
            }
            v += locl;
            std::cout << std::endl;
        }
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiCommon::readFile(const std::string& fn, const int size, std::vector<u_int>& data) {

    // check vector size
    if(static_cast<int>(data.size()) != size) {
        CERR("vector size (%d) is not equal to transfer size (%d)",data.size(),size);
        return(ERROR_WRONGPAR);
    }

    // open input file
    std::ifstream inf(fn.c_str()); if(! inf) {
        CERR("cannot open input file \"%s\"",fn.c_str());
        return(ERROR_FILE);
    }

    u_int value;
    std::string buf;
    int i;

    // read from file
    for(i=0; i<size; i++) {
        if(!(inf >> buf)) {
            CERR("reading word %d from input file \"%s\"",i,fn.c_str());
            return(ERROR_FILE);
        }
        if(buf[0] == '#') {
            if(!getline(inf,buf)) {
                CERR("reading comment before word %d from input file \"%s\"",i,fn.c_str());
                return(ERROR_FILE);
            }
            i--;
            continue;
        }
        if(sscanf(buf.c_str(),"%x",&value) != 1) {
            CERR("scanning word %d from input file \"%s\"",i,fn.c_str());
            return(ERROR_WRONGSCAN);
        }
        data[i] = value;
    }
    COUT("read %d data words from input file \"%s\"",size,fn.c_str());

    // close input file
    inf.close();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::readFile(const std::string& fn, const int size, u_int data[]) {

    // open input file
    std::ifstream inf(fn.c_str()); if(! inf) {
        CERR("cannot open input file \"%s\"",fn.c_str());
        return(ERROR_FILE);
    }

    u_int value;
    std::string buf;
    int i;

    // read from file
    for(i=0; i<size; i++) {
        if(!(inf >> buf)) {
            CERR("reading word %d from input file \"%s\"",i,fn.c_str());
            return(ERROR_FILE);
        }
        if(buf[0] == '#') {
            if(!getline(inf,buf)) {
                CERR("reading comment before word %d from input file \"%s\"",i,fn.c_str());
                return(ERROR_FILE);
            }
            i--;
            continue;
        }
        if(sscanf(buf.c_str(),"%x",&value) != 1) {
            CERR("scanning word %d from input file \"%s\"",i,fn.c_str());
            return(ERROR_WRONGSCAN);
        }
        data[i] = value;
    }
    COUT("read %d data words from input file \"%s\"",size,fn.c_str());

    // close input file
    inf.close();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::readFile(const std::string& fn, const int size, std::vector<u_int>& data, int& osiz) {

    // check vector size
    if(static_cast<int>(data.size()) < size) {
        CERR("vector size (%d) is smaller than transfer size (%d)",data.size(),size);
        osiz = 0;
        return(ERROR_WRONGPAR);
    }

    // open input file
    std::ifstream inf(fn.c_str()); if(! inf) {
        CERR("cannot open input file \"%s\"",fn.c_str());
        osiz = 0;
        return(ERROR_FILE);
    }

    u_int value;
    std::string buf;

    // read from file
    for(osiz=0; osiz<size; osiz++) {
        if(!(inf >> buf)) {
            if(inf.eof()) break;
            CERR("reading word %d from input file \"%s\"",osiz,fn.c_str());
            return(ERROR_FILE);
        }
        if(buf[0] == '#') {
            if(!getline(inf,buf)) {
                CERR("reading comment before word %d from input file \"%s\"",osiz,fn.c_str());
                return(ERROR_FILE);
            }
            osiz--;
            continue;
        }
        if(sscanf(buf.c_str(),"%x",&value) != 1) {
            CERR("scanning word %d from input file \"%s\"",osiz,fn.c_str());
            return(ERROR_WRONGSCAN);
        }
        data[osiz] = value;
    }
    COUT("read %d data words from input file \"%s\"",osiz,fn.c_str());

    // close input file
    inf.close();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::readFile(const std::string& fn, const int size, u_int data[], int& osiz) {

    // open input file
    std::ifstream inf(fn.c_str()); if(! inf) {
        CERR("cannot open input file \"%s\"",fn.c_str());
        return(ERROR_FILE);
    }

    u_int value;
    std::string buf;

    // read from file
    for(osiz=0; osiz<size; osiz++) {
        if(!(inf >> buf)) {
            if(inf.eof()) break;
            CERR("reading word %d from input file \"%s\"",osiz,fn.c_str());
            return(ERROR_FILE);
        }
        if(buf[0] == '#') {
            if(!getline(inf,buf)) {
                CERR("reading comment before word %d from input file \"%s\"",osiz,fn.c_str());
                return(ERROR_FILE);
            }
            osiz--;
            continue;
        }
        if(sscanf(buf.c_str(),"%x",&value) != 1) {
            CERR("scanning word %d from input file \"%s\"",osiz,fn.c_str());
            return(ERROR_WRONGSCAN);
        }
        data[osiz] = value;
    }
    COUT("read %d data words from input file \"%s\"",osiz,fn.c_str());

    // close input file
    inf.close();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::readFile(const std::string& fn, std::vector<u_int>& data) {

    // clear data vector
    data.clear();

    // open input file
    std::ifstream inf(fn.c_str()); if(! inf) {
        CERR("cannot open input file \"%s\"",fn.c_str());
        return(ERROR_FILE);
    }

    u_int value;
    std::string buf;

    // read from file
    while((inf >> buf)) {
        if(buf[0] == '#') {
            if(!getline(inf,buf)) {
                CERR("reading comment before word %d from input file \"%s\"",data.size(),fn.c_str());
                return(ERROR_FILE);
            }
            continue;
        }
        if(sscanf(buf.c_str(),"%x",&value) != 1) {
            CERR("scanning word %d from input file \"%s\"",data.size(),fn.c_str());
            return(ERROR_WRONGSCAN);
        }
        data.push_back(value);
    }
    COUT("read %d data words from input file \"%s\"",data.size(),fn.c_str());

    // close input file
    inf.close();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::readFile(std::ifstream& f, const unsigned int size, unsigned int data[]) {

    std::string s;
    unsigned int val, i;

    // read from file
    for(i=0; i<size; i++) {
        if(!(f >> s)) {
            CERR("reading word %d from file",i);
            return(ERROR_FILE);
        }
        if(s[0] == '#') {
            if(!getline(f,s)) {
                CERR("reading comment before word %d from file",i);
                return(ERROR_FILE);
            }
            i--;
            continue;
        }
        if(sscanf(s.c_str(),"%x",&val) != 1) {
            CERR("scanning word %d from file \"%s\"",i);
            return(ERROR_WRONGSCAN);
        }
        data[i] = val;
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::readFile(std::ifstream& f, const unsigned int size, unsigned int data[], unsigned int datb[]) { // 2 arrays

    std::string s;
    unsigned int vala, valb, i;

    // read from file
    for(i=0; i<size; i++) {
        if(!(f >> s)) {
            CERR("reading word %d from file",i);
            return(ERROR_FILE);
        }
        if(s[0] == '#') {
            if(!getline(f,s)) {
                CERR("reading comment before word %d from file",i);
                return(ERROR_FILE);
            }
            i--;
            continue;
        }
        if(sscanf(s.c_str(),"%x%x",&vala,&valb) != 1) {
            CERR("scanning words %d from file \"%s\"",i);
            return(ERROR_WRONGSCAN);
        }
        data[i] = vala;
        datb[i] = valb;
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::writeFile(const std::string& fn, const int size, std::vector<u_int>& data) {

    // check vector size
    if(static_cast<int>(data.size()) != size) {
        CERR("vector size (%d) is not equal to transfer size (%d)",data.size(),size);
        return(ERROR_WRONGPAR);
    }

    // open output file
    std::ofstream out(fn.c_str()); if(! out) {
        CERR("cannot open output file \"%s\"",fn.c_str());
        return(ERROR_FILE);
    }

    std::stringstream buf;
    int i;

    // write to file
    buf << std::hex << std::setfill('0');
    for(i=0;  i<size; i++) {
        buf.str(""); buf << "0x" << std::setw(8) << data[i];
        if(! (out << buf.str() << std::endl)) {
            CERR("writing word %d into output file \"%s\"",i,fn.c_str());
            return(FAILURE);
        }
    }
    COUT("wrote %d data words into output file \"%s\"",size,fn.c_str());

    // close output file
    out.close();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::writeFile(const std::string& fn, const int size, u_int data[]) {

    // open output file
    std::ofstream out(fn.c_str()); if(! out) {
        CERR("cannot open output file \"%s\"",fn.c_str());
        return(ERROR_FILE);
    }

    std::stringstream buf;
    int i;

    // write to file
    buf << std::hex << std::setfill('0');
    for(i=0;  i<size; i++) {
        buf.str(""); buf << "0x" << std::setw(8) << data[i];
        if(! (out << buf.str() << std::endl)) {
            CERR("writing word %d into output file \"%s\"",i,fn.c_str());
            return(FAILURE);
        }
    }
    COUT("wrote %d data words into output file \"%s\"",size,fn.c_str());

    // close output file
    out.close();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::writeFile(std::ofstream& f, const unsigned int size, const unsigned int data[]) {

    char s[STRING_LENGTH];
    unsigned int i;

    // write to file
    for(i=0;  i<size; i++) {
        std::sprintf(s,"0x%08x",data[i]);
        if(!(f << s << std::endl)) {
            CERR("writing word %d to file",i);
            return(FAILURE);
        }
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::writeFile(std::ofstream& f, const unsigned int size, const unsigned int data[], const unsigned datb[]) { // 2 arrays

    char s[STRING_LENGTH];
    unsigned int i;

    // write to file
    for(i=0;  i<size; i++) {
        std::sprintf(s,"0x%08x 0x%08x",data[i], datb[i]);
        if(!(f << s << std::endl)) {
            CERR("writing word %d to file",i);
            return(FAILURE);
        }
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiCommon::readNumber(const std::string& str, int& num) {

    // copy string
    std::string s(str);

    // remove space(s)
    s.erase(remove(s.begin(),s.end(),' '),s.end());

    // check if string empty
    if(s.empty()) {
        CERR("input string is empty","");
        return(FAILURE);
    }

    int base(10);
    char* end;

    // check if string is hexadecimal
    if((s.size() >= 2) && ((s.substr(0,2) == "0x") || s.substr(0,2) == "0X")) base = 16;

    // try to convert string to integer
    errno = 0;
    num = std::strtol(s.c_str(),&end,base);
    if(end[0] != '\0') {
        CERR("illegal character(s)","");
        return(FAILURE);
    }
    else if(errno == ERANGE) {
        CERR("number conversion range exception","");
        return(FAILURE);
    }
    else if(errno == EINVAL) {
        CERR("invalid number conversion","");
        return(FAILURE);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::readNumber(const std::string& str, unsigned int& num) {

    // copy string
    std::string s(str);

    // remove space(s)
    s.erase(remove(s.begin(),s.end(),' '),s.end());

    // check if string empty
    if(s.empty()) {
        CERR("input string is empty","");
        return(FAILURE);
    }

    int base(10);
    char* end;

    // check if string is hexadecimal
    if((s.size() >= 2) && ((s.substr(0,2) == "0x") || s.substr(0,2) == "0X")) base = 16;

    // check if string starts with minus sign
    if((s.size() >= 1) && (s.substr(0,1) == "-")) {
        CERR("negative value","");
        return(FAILURE);
    }

    // try to convert string to integer
    errno = 0;
    num = std::strtoul(s.c_str(),&end,base);
    if(end[0] != '\0') {
        CERR("illegal character(s)","");
        return(FAILURE);
    }
    else if(errno == ERANGE) {
        CERR("number conversion range exception","");
        return(FAILURE);
    }
    else if(errno == EINVAL) {
        CERR("invalid number conversion","");
        return(FAILURE);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCommon::readNumber(const std::string& str, double& num) {

    // copy string
    std::string s(str);

    // remove space(s)
    s.erase(remove(s.begin(),s.end(),' '),s.end());

    // check if string empty
    if(s.empty()) {
        CERR("input string is empty","");
        return(FAILURE);
    }

    char* end;

    // try to convert string to integer
    errno = 0;
    num = std::strtod(s.c_str(),&end);
    if(end[0] != '\0') {
        CERR("illegal character(s)","");
        return(FAILURE);
    }
    else if(errno == ERANGE) {
        CERR("number conversion range exception","");
        return(FAILURE);
    }
    else if(errno == EINVAL) {
        CERR("invalid number conversion","");
        return(FAILURE);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void AltiCommon::substr(const unsigned int data[], const unsigned int off, const unsigned int num, std::vector<unsigned int>& rslt) {

    unsigned int loshft, losize, lomask, hishft(0), himask(0), dat, wrd(off/WORD_SIZE), tot(0), nxt, msk;

    // reset vector
    rslt.clear();

    loshft = off%WORD_SIZE;
    losize = WORD_SIZE - loshft;
    lomask = 0xffffffff >> loshft;

    if(loshft != 0) {
        hishft = WORD_SIZE - loshft;
        himask = 0xffffffff << hishft;
    }
    CDBG("off = %d => losize = %d, loshft = %d, lomask = 0x%08x, hishft = %d, himask = 0x%08x",off,losize,loshft,lomask,hishft,himask);

    while(tot < num) {

        nxt = num - tot; if(nxt > WORD_SIZE) nxt = WORD_SIZE;
        CDBG("num = %d, tot = %d, wrd = %d => nxt = %d",num,tot,wrd,nxt);

        if(nxt == WORD_SIZE) {
            if(loshft == 0) {
                dat = data[wrd];
            }
            else {
                dat = ((data[wrd] >> loshft) & lomask) | ((data[wrd+1] << hishft) & himask);
            }
            rslt.push_back(dat);
            CDBG("L0: data = 0x%08x",dat);
        }
        else {
            if(nxt <= losize) {
                msk = SUBSTR_MASK[nxt];
                dat = ((data[wrd] >> loshft) & msk);
                rslt.push_back(dat);
                CDBG("L1: msk = 0x%08x, data = 0x%08x",msk,dat);
            }
            else {
                msk = SUBSTR_MASK[nxt - losize] << hishft;
                dat = ((data[wrd] >> loshft) & lomask) | ((data[wrd+1] << hishft) & msk);
                rslt.push_back(dat);
                 CDBG("L2: msk = 0x%08x, data = 0x%08x",msk,dat);
             }
         }

         tot += nxt;
         wrd++;
    }
}

//------------------------------------------------------------------------------

void AltiCommon::substr(const unsigned int data[], const unsigned int off, const unsigned int num, unsigned int rslt[]) {

    unsigned int loshft, losize, lomask, hishft(0), himask(0), dat, wrd(off/WORD_SIZE), tot(0), nxt, msk, rdx(0);

    loshft = off%WORD_SIZE;
    losize = WORD_SIZE - loshft;
    lomask = 0xffffffff >> loshft;

    if(loshft != 0) {
        hishft = WORD_SIZE - loshft;
        himask = 0xffffffff << hishft;
    }
    CDBG("off = %d => losize = %d, loshft = %d, lomask = 0x%08x, hishft = %d, himask = 0x%08x",off,losize,loshft,lomask,hishft,himask);

    while(tot < num) {

        nxt = num - tot; if(nxt > WORD_SIZE) nxt = WORD_SIZE;
        CDBG("num = %d, tot = %d, wrd = %d => nxt = %d",num,tot,wrd,nxt);

        if(nxt == WORD_SIZE) {
            if(loshft == 0) {
                dat = data[wrd];
            }
            else {
                dat = ((data[wrd] >> loshft) & lomask) | ((data[wrd+1] << hishft) & himask);
            }
            rslt[rdx++] = dat;
            CDBG("L0: data = 0x%08x",dat);
        }
        else {
            if(nxt <= losize) {
                msk = SUBSTR_MASK[nxt];
                dat = ((data[wrd] >> loshft) & msk);
                rslt[rdx++] = dat;
                CDBG("L1: msk = 0x%08x, data = 0x%08x",msk,dat);
            }
            else {
                msk = SUBSTR_MASK[nxt - losize] << hishft;
                dat = ((data[wrd] >> loshft) & lomask) | ((data[wrd+1] << hishft) & msk);
                rslt[rdx++] = dat;
                CDBG("L2: msk = 0x%08x, data = 0x%08x",msk,dat);
            }
        }

        tot += nxt;
        wrd++;
    }
}

//------------------------------------------------------------------------------

std::string AltiCommon::centerString(const std::string& str, const u_int len, const char chr) {

    if((str.length()    ) >= len) return(str);
    if((str.length() + 1) == len) return(" "+str);
    if((str.length() + 2) == len) return(" "+str+" ");
    if((str.length() + 3) == len) return("  "+str+" ");

    u_int dif, i;
    std::ostringstream buf;

    dif = len - str.length() - 2;
    for(i=0; i<(dif/2 + dif%2); i++) buf << chr;

    buf << " " << str << " ";

    for(i=0; i<(dif/2); i++) buf << chr;

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string AltiCommon::rightFillString(const std::string& str, const u_int len, const char chr) {

    if(str.length() >= len) return(str);

    u_int dif, i;
    std::ostringstream buf;

    buf << str;

    dif = len - str.length();
    for(i=0; i<dif; i++) buf << chr;

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string AltiCommon::leftFillString(const std::string& str, const u_int len, const char chr) {

    if(str.length() >= len) return(str);

    u_int dif, i;
    std::ostringstream buf;

    dif = len - str.length();
    for(i=0; i<dif; i++) buf << chr;

    buf << str;

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string AltiCommon::leftTrimString(const std::string& s) {

    std::string::size_type idx(0), siz(s.size());

    while ((idx < siz) && (std::isspace(s[idx]))) {
        idx++;
    }
    CDBG("str = \"%s\", siz = %d, idx = %d",s.c_str(),siz,idx);

    return(s.substr(idx,siz-idx));
}

//------------------------------------------------------------------------------

std::string AltiCommon::rightTrimString(const std::string& s) {

    std::string::size_type idx(s.size());

    while ((idx > 0) && (std::isspace(s[idx-1]))) {
        idx--;
    }
    CDBG("str = \"%s\", siz = %d, idx = %d",s.c_str(),s.size(),idx);

    return(s.substr(0,idx));
}

//------------------------------------------------------------------------------

std::string AltiCommon::trimString(const std::string& s) {

    return(rightTrimString(leftTrimString(s)));
}

//------------------------------------------------------------------------------

std::string AltiCommon::printableString(const std::string& s) {

    std::ostringstream buf;

    for(unsigned int i=0; i<s.size(); i++) {
        if((uint8_t)s[i] < 32) buf << "?";
        else if ((uint8_t)s[i] >= 127 ) buf << ".";
        else buf << s[i];
    }

    return(buf.str());
}

//------------------------------------------------------------------------------

int AltiCommon::splitString(const std::string& str, const std::string& del, std::vector<std::string>& rslt) {

    rslt.clear();

    if(str.empty()) return(0);

    std::string::size_type idx0(0), idx1(0);

    while(true) {
        idx1 = str.find(del,idx0);
        if(idx1 != std::string::npos) {
            rslt.push_back(str.substr(idx0,idx1-idx0));
        }
        else {
            rslt.push_back(str.substr(idx0));
            break;
        }
        idx0 = idx1+1;
    }

    return(rslt.size());
}

//------------------------------------------------------------------------------

void AltiCommon::parseRevision(const unsigned int revid, unsigned int &dd, unsigned int &mm, unsigned int &yy, unsigned int &revnum) {
    yy = 2000 + ((revid & 0xff000000) >> 24);
    mm = ((revid & 0x00f00000) >> 20);
    dd = ((revid & 0x000ff000) >> 12);
    revnum = (revid & 0x00000fff);
}

//------------------------------------------------------------------------------

float AltiCommon::enterFloat(const std::string &msg, const float down, const float up) {
    char buf[20];
    float fdat(0.0);
    while (true) {
        std::printf("%s [%.1f, %.1f]: ", msg.c_str(), down, up);
        buf[0] = '\0';
        if (!std::cin) std::cin.clear();
        std::cin >> buf;
        if (buf[0] == '\0') {
            continue;
        }
        else {
            fdat = strtof(buf, nullptr);
            break;
        }
    }
    std::cin.getline(buf, 1);

    if (fdat < down) fdat = down;
    if (fdat > up) fdat = up;

    return fdat;
}

//------------------------------------------------------------------------------

unsigned int AltiCommon::getFirstSetBitPos(const int n) {
    return log2(n & (-n));
}
