//******************************************************************************
// file: AltiCounter.cc
// desc: library for ALTI counter (scalars)
// auth: 04-DEC-2017 P. Kuzmanovic
//******************************************************************************

#include "ALTI/AltiCommon.h"
#include "ALTI/AltiCounter.h"
#include "RCDUtilities/RCDUtilities.h"

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>

using namespace LVL1;

const unsigned int AltiCounter::DSIZE = AltiModule::CNT_SIZE;
const std::string AltiCounter::CNT_NAME[AltiModule::CNT_SIZE] = {
    "TTYP_BUCKET0",
    "TTYP_BUCKET1",
    "TTYP_BUCKET2",
    "TTYP_BUCKET3",
    "TTYP_BUCKET4",
    "TTYP_BUCKET5",
    "TTYP_BUCKET6",
    "TTYP_BUCKET7",
    "L1A",
    "TTR1",
    "TTR2",
    "TTR3",
    "BGO0",
    "BGO1",
    "BGO2",
    "BGO3",
    "BC"
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

AltiCounter::AltiCounter() : m_segment(0), m_turn(0) {

    // create data array
    m_data = new unsigned int[size()];
}

//------------------------------------------------------------------------------

AltiCounter::AltiCounter(RCD::CMEMSegment *seg) : m_segment(seg), m_turn(0) {

    // get virtual address
    m_data = (unsigned int *) m_segment->VirtualAddress();
}

//------------------------------------------------------------------------------

AltiCounter::~AltiCounter() {

    // delete data array
    if (!m_segment) delete [] m_data;
}

//------------------------------------------------------------------------------

unsigned int AltiCounter::overflow() const {

    unsigned int ovf(0), i;

    for (i = 0; i < size(); i++) if (overflow(i)) ovf++;

    return (ovf);
}

//------------------------------------------------------------------------------

void AltiCounter::dump() const {

    unsigned int i;
   
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI: counters" << std::endl;

    for (i = 0; i < DSIZE; i++) {
      std::printf("%-14s:  %s0x%08x", ("\"" + CNT_NAME[i] + "\"").c_str(), overflow(i) ? "O" : " ", counter(i));
      std::cout << std::endl;
    }
    std::printf("TURN          :  %s0x%08x (= %10d)\n", turnOverflow() ? "O" : " ", m_turn, m_turn);
    std::printf("              :   overflow    = %10d\n", overflow());
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

// double has at least 15 decimal digits precision: 4 before decimal point + 11 after decimal point
const double AltiCounterRate::EPSILON = 0.00000000001;

//------------------------------------------------------------------------------

AltiCounterRate::AltiCounterRate() {

    m_name = AltiModule::CNT_NAME[0];
    m_size = AltiModule::CNT_SIZE;
    m_data = new double[m_size];
    m_turn = 0.0;

    // reset state
    resetState();

    // reset data
    for (unsigned int i = 0; i < m_size; i++) m_data[i] = 0.0;
}

//------------------------------------------------------------------------------

AltiCounterRate::AltiCounterRate(const AltiCounter &cnt) {

    m_name = AltiModule::CNT_NAME[static_cast<int>(0)];
    m_size = cnt.size();
    m_data = new double[m_size];
    m_turn = static_cast<double>(cnt.turn());

    // reset state
    resetState();

    // set data and update state
    for (unsigned int i = 0; i < m_size; i++) {
        m_data[i] = (m_turn <= 0.0) ? 0.0 : (static_cast<double>(cnt.counter(i))/m_turn);
        if (std::abs(m_data[i] - round(m_data[i])) > EPSILON) m_fraction++;
    }
}

//------------------------------------------------------------------------------

AltiCounterRate::AltiCounterRate(const AltiCounterRate &cnt) {

    m_name = cnt.name();
    m_size = cnt.size();
    m_data = new double[m_size];
    m_turn = cnt.turn();

    // reset state with state from counter
    m_fraction = cnt.fraction();
    m_error = -1;
    m_infinity = cnt.infinity();

    // set data
    for (unsigned int i = 0; i < m_size; i++) m_data[i] = cnt.counter(i);
}

//------------------------------------------------------------------------------

AltiCounterRate::~AltiCounterRate() {

    if (m_data) delete [] m_data;
}

//------------------------------------------------------------------------------

AltiCounterRate &AltiCounterRate::operator=(const AltiCounter &cnt) {

  m_name = AltiModule::CNT_NAME[static_cast<int>(0)];
    if (m_size != cnt.size()) {
        m_size = cnt.size();
        if (m_data) delete [] m_data;
        m_data = new double[m_size];
    }
    m_turn = static_cast<double>(cnt.turn());

    // reset state
    resetState();

    // copy data and update state
    for (unsigned int i = 0; i < m_size; i++) {
        m_data[i] = (m_turn <= 0.0) ? 0.0 : (static_cast<double>(cnt.counter(i))/m_turn);
        if (std::abs(m_data[i] - round(m_data[i])) > EPSILON) m_fraction++;
    }

    return (*this);
}

//------------------------------------------------------------------------------

AltiCounterRate &AltiCounterRate::operator=(const AltiCounterRate &cnt) {

    m_name = cnt.name();
    if (m_size != cnt.size()) {
        m_size = cnt.size();
        if (m_data) delete [] m_data;
        m_data = new double[m_size];
    }
    m_turn = cnt.turn();

    // reset state with state from counter
    m_fraction = cnt.fraction();
    m_error = -1;
    m_infinity = cnt.infinity();

    // copy data
    for (unsigned int i = 0; i < m_size; i++) {
        m_data[i] = cnt.counter(i);
    }

    return (*this);
}

//------------------------------------------------------------------------------

AltiCounterRate AltiCounterRate::operator/(const AltiCounterRate &cnt) const {

    if (m_size != cnt.m_size) {
        CERR("wrong size for division %d != %d expected", cnt.m_size, m_size);
        return (*this);
    }

    AltiCounterRate frq(*this);

    // reset state
    frq.resetState();

    // copy data and update state
    for (unsigned int i = 0; i < m_size; i++) {
        if (m_data[i] <= 0.0) {
            frq.m_data[i] = 0.0;
            if (cnt.m_data[i] <= 0.0) frq.m_infinity++;
        }
        else if (cnt.m_data[i] <= 0) {
            frq.m_data[i] = 0.0;
            frq.m_infinity++;
        }
        else {
            frq.m_data[i] = (m_data[i]/cnt.m_data[i]);
            if (std::abs(frq.m_data[i] - round(frq.m_data[i])) > EPSILON) frq.m_fraction++;
        }
    }

    return (frq);
}

//------------------------------------------------------------------------------

int AltiCounterRate::setFraction() {

    // reset state
    resetState();

    // fill data and update state
    for (unsigned int i = 0; i < AltiCounter::DSIZE; i++) {
        if (std::abs(m_data[i] - round(m_data[i])) > EPSILON) m_fraction++;
    }

    return (AltiModule::SUCCESS);
}

//------------------------------------------------------------------------------

int AltiCounterRate::scale(const double scl) {

    // reset state
    resetState();

    for (unsigned int i = 0; i < m_size; i++) {
        m_data[i] = m_data[i]*scl;
        if (std::abs(m_data[i] - round(m_data[i])) > EPSILON) m_fraction++;
    }

    return (AltiModule::SUCCESS);
}

//------------------------------------------------------------------------------

unsigned int AltiCounterRate::size(const unsigned int n)  {

    m_size = n;

    if (m_data) delete [] m_data;
    m_data = new double[m_size];
    for (unsigned int i = 0; i < m_size; i++) m_data[i] = 0.0;

    m_turn = 0.0;
    m_fraction = -1;
    m_error = -1;
    m_infinity = -1;

    return (m_size);
}


//------------------------------------------------------------------------------

void AltiCounterRate::dump() const {

    unsigned int i;
    
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI: rates in kHz" << std::endl;

    for (i = 0; i < AltiCounter::DSIZE; i++) {
     
      if(i<AltiCounter::DSIZE-1) std::printf("%-14s: %12.6f", ("\"" + AltiCounter::CNT_NAME[i] + "\"").c_str(), m_data[i]*11.2455);
      else          std::printf("%-14s: %12.6f", ("\"" + AltiCounter::CNT_NAME[i] + "\"").c_str(), m_data[i]);
      std::cout << std::endl;
    }
 
    std::printf("TURN          :  %12.3f\n", m_turn);
    if ((m_fraction > -1) || (m_infinity > -1)) std::cout << "            " << "  : ";
    if (m_fraction > -1) std::cout << ((m_fraction) ? " FRACTION = " : " fraction = ") << m_fraction;
    if (m_infinity > -1) std::cout << ((m_infinity) ? ", INFINITY = " : ", infinity = ") << m_infinity;
    if ((m_fraction > -1) || (m_infinity > -1)) std::cout << std::endl;
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

void AltiCounterRate::dump(const AltiCounterRate &cnt) const {

    unsigned int i;
    char s[AltiCommon::STRING_LENGTH];

    std::cout << ">> --------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI: rate in kHz" << std::endl;

    for (i = 0; i < AltiCounter::DSIZE; i++) {
        if ((i%8) == 0) {
            (size() > 1) ? std::sprintf(s, "%3d", i) : std::sprintf(s, "   ");
            std::printf("CounterRate  %s: ", s);
        }
        std::printf(" %s%12.6f", (std::abs(m_data[i] - cnt[i]) > EPSILON) ? "E" : " ", m_data[i]*11.2455);
        if ((i%8) == 7) std::cout << std::endl;
    }
    if ((i%8) != 0) std::cout << std::endl;

    std::cout << "                          -------------------------------------------------------------------------------------------------------" << std::endl;
    std::printf("TURN          %12.3f\n", m_turn);
    if ((m_fraction > -1) || (m_error > -1) || (m_infinity > -1)) std::printf("                : ");
    if (m_fraction > -1) std::cout << ((m_fraction) ? " FRACTION = "  : " fraction = ") << m_fraction;
    if (m_error > -1)    std::cout << ((m_error)    ? ", ERROR = "    : ", error = ") << m_error;
    if (m_infinity > -1) std::cout << ((m_infinity) ? ", INFINITY = " : ", infinity = ") << m_infinity;
    if ((m_fraction > -1) || (m_error > -1) || (m_infinity > -1)) std::cout << std::endl;
    std::cout << ">> --------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

void AltiCounterRate::dumpSigma(const AltiCounterRate &sig) const {

    unsigned int i, j;
    char s[AltiCommon::STRING_LENGTH];

    std::cout << ">> ------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI:" << std::endl;

    for (i = 0; i < AltiCounter::DSIZE; i++) {
        if ((i%8) == 0) {
            (size() > 1) ? std::sprintf(s," %3d", i) : std::sprintf(s, "   ");
            std::printf("CounterRate  %s: ", s);
        }
        std::printf(" %12.6f", m_data[i]);
        if ((i%8) == 7) {
            std::cout << std::endl;
            std::printf("SigmaRate    %3d: ", i - 7);
            for (j = i - 7; j <= i; j++) {
                std::sprintf(s, "+-%.6f", sig[j]);
                std::printf(" %12s", s);
            }
            std::cout << std::endl;
            std::cout << "                          -------------------------------------------------------------------------------------------------------" << std::endl;
        }
    }
    if ((i%8) != 0) {
        std::cout << std::endl;
        std::printf("SigmaRate    %3d: ", i - i%8);
        for (j = i - i%8; j < i; j++) {
            std::sprintf(s, "+-%.6f", sig[j]);
            std::printf(" %12s", s);
        }
        std::cout << std::endl;
        std::cout << "                          -------------------------------------------------------------------------------------------------------" << std::endl;
    }

    std::printf("TURN            :  %12.3f\n", m_turn);
    if ((m_fraction > -1) || (m_infinity > -1)) std::cout << "            " << "    : ";
    if (m_fraction > -1) std::cout << ((m_fraction) ? " FRACTION = " : " fraction = ") << m_fraction;
    if (m_infinity > -1) std::cout << ((m_infinity) ? ", INFINITY = " : ", infinity = ") << m_infinity;
    if ((m_fraction > -1) || (m_infinity > -1)) std::cout << std::endl;
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

int AltiCounterRate::check(const AltiCounterRate &cnt) {

    // reset error
    m_error = 0;

    // for all DATA
    for (unsigned int i = 0; i < m_size; i++) {
        if (std::abs(m_data[i] - cnt[i]) > EPSILON) m_error++;
    }

    return (m_error);
}

//------------------------------------------------------------------------------

int AltiCounterRate::checkWord(const unsigned int data, const unsigned int datb) const {

    unsigned int i;
    int nbit(0);

    // count number of bit errors
    for (i = 0; i < WORD_SIZE; i++) {
        if ((data & (1U << i)) != (datb & (1U << i))) nbit++;
    }

    // return bit error
    return (nbit);
}

//------------------------------------------------------------------------------

void AltiCounterRate::resetState() {

    // reset state
    m_fraction = 0;
    m_error = -1;
    m_infinity = -1;
}
