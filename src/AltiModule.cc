//******************************************************************************
// file: AltiModule.cc
// desc: class for ALTI Module
// auth: 24-OCT-2017 P. Kuzmanovic
//******************************************************************************

#include "RCDUtilities/RCDUtilities.h"
#include "RCDBitString/BitSet.h"

#include "ALTI/AltiModule.h"

#include "ALTI/AltiCommon.h"
#include "ALTI/AltiConfiguration.h"
#include "ALTI/AltiCounter.h"
#include "ALTI/AltiMiniCtpCounter.h"
#include "ALTI/AltiBusyCounter.h"

#include "unistd.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>

#include <cmath>
#include <bitset>

#ifndef QUIET
#define MERR(s,x) (s << "ERROR in " << __PRETTY_FUNCTION__ << ":\n>> " << x << "\n")
#else
#define MERR(s,x)
#endif
#define CHECKRANGE0(val, hi, txt) if (val > hi) { CERR("ALTI: %s \"%d\" not in allowed range [0..%d]", txt, val, hi); return (-1); }
#define CHECKRANGE(val, lo, hi, txt) if (val < lo || val > hi) { CERR("ALTI: %s \"%d\" not in allowed range [%d..%d]", txt, val, lo, hi); return (-1); }

namespace LVL1 {

    struct _jitter {
#include "ALTI/Si5344-RevB-AL250619-Registers.h"
        static const unsigned int NUMBER = SI5344_REVB_REG_CONFIG_NUM_REGS;
    };
    static _jitter JITTER;
}

using namespace LVL1;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

AltiModule::DataSegment::DataSegment(const std::string &name, const unsigned int size) : m_segment(0), m_data(0) {

    m_name = name;
    m_segment = new RCD::CMEMSegment(m_name, size*sizeof(uint32_t), true);
    if (((*m_segment))() != CMEM_RCC_SUCCESS) {
        CERR("ALTI: opening CMEM segment \"%s\" of size 0x%08x", m_name.c_str(), m_segment->Size());
        m_segment = 0;
        return;
    }
    m_data = (unsigned int*) (m_segment->VirtualAddress());
    CDBG("ALTI: INFO - CMEM segment \"%s\" opened of size 0x%08x, phys 0x%08x, virt 0x%08x", m_name.c_str(), m_segment->Size(), (unsigned int) m_segment->PhysicalAddress(), (unsigned int) m_segment->VirtualAddress());
}

//------------------------------------------------------------------------------

AltiModule::DataSegment::~DataSegment() {

    if (m_segment) {
        delete m_segment;
        CDBG("ALTI: INFO - closed CMEM segment \"%s\"", m_name.c_str());
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

AltiModule::ECR_GENERATION::ECR_GENERATION() {

    // default values
    type                = ECR_VME;
    length              = 1;
    frequency           = 0;
    busy_before         = 0;
    busy_after          = 0;
    orbit_offset        = 0;
}

//------------------------------------------------------------------------------

AltiModule::ECR_GENERATION::ECR_GENERATION(const ECR_GENERATION &ecr) {

    // copy values
    type                = ecr.type;
    length              = ecr.length;
    frequency           = ecr.frequency;
    busy_before         = ecr.busy_before;
    busy_after          = ecr.busy_after;
}

//------------------------------------------------------------------------------

AltiModule::ECR_GENERATION &AltiModule::ECR_GENERATION::operator=(const ECR_GENERATION &ecr) {

    // copy values if necessary
    if (this != &ecr) {
        type            = ecr.type;
        length          = ecr.length;
        frequency       = ecr.frequency;
        busy_before     = ecr.busy_before;
        busy_after      = ecr.busy_after;
        orbit_offset    = ecr.orbit_offset;
    }

    return (*this);
}

//------------------------------------------------------------------------------

AltiModule::ECR_GENERATION::~ECR_GENERATION() {

    // nothing to be done
}

//------------------------------------------------------------------------------

int AltiModule::ECR_GENERATION::read(const std::string &str) {

    std::string s;
    std::string::size_type idx;
    unsigned int i;
    bool flag(false);

    // get ECR type
    if ((idx = str.find(",")) == std::string::npos) return (FAILURE);
    s = str.substr(0, idx);
    for (i = 0; i < ECR_TYPE_NUMBER; i++) {
        if (s == ECR_TYPE_NAME[i]) {
            flag = true;
            break;
        }
    }
    if (!flag) return (FAILURE);
    type = static_cast<ECR_TYPE>(i);
 
    // get ECR parameters
    s = AltiCommon::leftTrimString(str.substr(idx + 1));
    long length_i, frequency_i, busy_before_i, busy_after_i, orbit_offset_i;
    if (std::sscanf(s.c_str(), "LEN=%ld,FRQ=%ld,BEF=%ld,AFT=%ld,OFF=%ld", &length_i, &frequency_i, &busy_before_i, &busy_after_i, &orbit_offset_i) != 5) return (FAILURE);
    if ((length_i < 0) || (length_i > (long) (ECR_LENGTH))) return (FAILURE);
    else length = length_i;
    if ((frequency_i < 0) || (frequency_i > (long) ECR_FREQUENCY_MASK)) return (FAILURE);
    else frequency = frequency_i;
    if ((busy_before_i < 0) || (busy_before_i > (long) ECR_BUSY_BEFORE_MASK)) return (FAILURE);
    else busy_before = busy_before_i;
    if ((busy_after_i < 0) || (busy_after_i > (long) ECR_BUSY_AFTER_MASK)) return (FAILURE);
    else busy_after = busy_after_i;
    if ((orbit_offset_i < 0) || (orbit_offset_i > (long) ECR_ORBIT_OFFSET_MASK)) return (FAILURE);
    else orbit_offset = orbit_offset_i;

    return (SUCCESS);
}

//------------------------------------------------------------------------------

void AltiModule::ECR_GENERATION::dump(std::ostream &os) const {

    char s[AltiCommon::STRING_LENGTH];

    std::sprintf(s, "ECR type         = %s\n", AltiModule::ECR_TYPE_NAME[type].c_str()); os << s;
    std::sprintf(s, "ECR length       = %d\n", length); os << s;
    if (type == AltiModule::ECR_INTERNAL) {
        std::sprintf(s, "ECR frequency    = 0x%08x (= %10d)\n", frequency, frequency); os << s;
    }
    std::sprintf(s, "ECR BUSY before  =   0x%06x (=   %8d)\n", busy_before, busy_before); os << s;
    std::sprintf(s, "ECR BUSY after   =   0x%06x (=   %8d)\n", busy_after, busy_after); os << s;
    std::sprintf(s, "ECR OBRIT offset =      0x%03x (=       %4d)\n", orbit_offset, orbit_offset); os << s;
}

//------------------------------------------------------------------------------

std::string AltiModule::ECR_GENERATION::print() const {

    char s[AltiCommon::STRING_LENGTH];

    std::sprintf(s, "%s,LEN=%d,FRQ=%d,BEF=%d,AFT=%d,OFF=%d", AltiModule::ECR_TYPE_NAME[type].c_str(), length, frequency, busy_before, busy_after, orbit_offset);

    return (s);
}

//------------------------------------------------------------------------------

const std::string AltiModule::ECR_TYPE_NAME[ECR_TYPE_NUMBER] {
    "VME", "INTERNAL"
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

const std::string AltiModule::SIGNAL_NAME[SIGNAL_NUMBER] =
  { "BC", "ORB", "L1A", "TTR1", "TTR2", "TTR3", "TTYP0", "TTYP1", "TTYP2", "TTYP3", "TTYP4", "TTYP5", "TTYP6", "TTYP7", "BGO0", "BGO1", "BGO2", "BGO3", "BUSY", "CALREQ0", "CALREQ1", "CALREQ2" };

const unsigned int AltiModule::MEM_MASK[SIGNAL_NUMBER] =
  {
    0x00000000,                             // BC,         not mapped
    SNAPSHOT::STAT_MASK_ORB,                // ORB,        bit     20
    SNAPSHOT::STAT_MASK_L1A,                // L1A,        bit      0
    SNAPSHOT::STAT_MASK_TTR[0],             // TTR1,       bit      1
    SNAPSHOT::STAT_MASK_TTR[1],             // TTR2,       bit      2
    SNAPSHOT::STAT_MASK_TTR[2],             // TTR3,       bit      3
    SNAPSHOT::STAT_MASK_TTYP[0],            // TTYP0,      bit      8
    SNAPSHOT::STAT_MASK_TTYP[1],            // TTYP1,      bit      9
    SNAPSHOT::STAT_MASK_TTYP[2],            // TTYP2,      bit     10
    SNAPSHOT::STAT_MASK_TTYP[3],            // TTYP3,      bit     11
    SNAPSHOT::STAT_MASK_TTYP[4],            // TTYP4,      bit     12
    SNAPSHOT::STAT_MASK_TTYP[5],            // TTYP5,      bit     13
    SNAPSHOT::STAT_MASK_TTYP[6],            // TTYP6,      bit     14
    SNAPSHOT::STAT_MASK_TTYP[7],            // TTYP7,      bit     15
    SNAPSHOT::STAT_MASK_BGO[0],             // BGO0,       bit      4
    SNAPSHOT::STAT_MASK_BGO[1],             // BGO1,       bit      5
    SNAPSHOT::STAT_MASK_BGO[2],             // BGO2,       bit      6
    SNAPSHOT::STAT_MASK_BGO[3],             // BGO3,       bit      7
    SNAPSHOT::STAT_MASK_BUSY,               // BUSY,       bit     19
    SNAPSHOT::STAT_MASK_CALREQ[0],          // CALREQ0,    bits    16
    SNAPSHOT::STAT_MASK_CALREQ[1],          // CALREQ1,    bits    17
    SNAPSHOT::STAT_MASK_CALREQ[2]           // CALREQ2,    bits    18
  };

const std::string AltiModule::SIGNAL_SOURCE_NAME[SIGNAL_SOURCE_NUMBER] =
  { "CTP_IN", "ALTI_IN", "NIM_IN", "FROM_FPGA", "INVALID_CONFIG" };

const std::string AltiModule::SIGNAL_DESTINATION_NAME[SIGNAL_DESTINATION_NUMBER] =
  { "CTP_OUT", "ALTI_OUT", "NIM_OUT", "TO_FPGA" };

const std::string AltiModule::ALTI_MODE_NAME[ALTI_MODE_NUMBER] =
  { "MASTER", "CTP_SLAVE", "ALTI_SLAVE", "LEMO_SLAVE" };

// indexing: what SIGNAL and what SIGNAL_SOURCE we want
// output: actual SIGNAL_SOURCE for the switch of that particular signal
const AltiModule::SIGNAL_SOURCE AltiModule::SWX_MAP_SRC[SIGNAL_NUMBER_ROUTED][SIGNAL_SOURCE_NUMBER - 1] = {
    { CTP_IN , NIM_IN, ALTI_IN  , FROM_FPGA }, // BC
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    }, // ORB
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    }, // L1A
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    },
    { ALTI_IN, NIM_IN, FROM_FPGA, CTP_IN    }
  };

// indexing: what SIGNAL and what actual SIGNAL_SOURCE we want
// output: SIGNAL_SOURCE for the switch of that particular signal
const AltiModule::SIGNAL_SOURCE AltiModule::SWX_MAP_SRC_INV[SIGNAL_NUMBER_ROUTED][SIGNAL_SOURCE_NUMBER - 1] = {
    { CTP_IN   , NIM_IN, ALTI_IN, FROM_FPGA }, // BC
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    }, // ORB
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    }, // L1A
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
    { FROM_FPGA, CTP_IN, ALTI_IN, NIM_IN    },
  };

// indexing: what SIGNAL and what SIGNAL_DESTINATION we want
// output: actual SIGNAL_DESTINATION for the switch of that particular signal
const AltiModule::SIGNAL_DESTINATION AltiModule::SWX_MAP_DST[SIGNAL_NUMBER_ROUTED][SIGNAL_DESTINATION_NUMBER] = {
    { CTP_OUT , NIM_OUT, ALTI_OUT, TO_FPGA }, // BC
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT }, // ORB
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT }, // L1A
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT },
    { ALTI_OUT, NIM_OUT, TO_FPGA , CTP_OUT }
  };

const std::string AltiModule::EQUALIZER_VOLTAGE_NAME[EQUALIZER_VOLTAGE_NUMBER] =
  { "V_PEAK", "V_POLE", "V_GAIN", "V_OFFSET" };

const std::string AltiModule::EQUALIZER_CONFIG_NAME[EQUALIZER_CONFIG_NUMBER] =
  { "SHORT_CABLE", "LONG_CABLE", "CUSTOM_CONFIG" };

const unsigned int AltiModule::EQUALIZER_CONFIG_VOLTAGE[EQUALIZER_CONFIG_NUMBER][EQUALIZER_VOLTAGE_NUMBER] = {
    { (unsigned int) round(0.00/2.5*255.0), (unsigned int) round(0.00/2.5*255.0), (unsigned int) round(0.00/2.5*255.0), (unsigned int) round(0.38/2.5*255.0) }, // short cable
    { (unsigned int) round(1.00/2.5*255.0), (unsigned int) round(0.60/2.5*255.0), (unsigned int) round(2.00/2.5*255.0), (unsigned int) round(0.32/2.5*255.0) }  // long  cable
  };

const std::string AltiModule::CLK_PLL_NAME[CLK_NUMBER] =
  { "JITTER_CLEANER", "OSCILLATOR" };

const std::string AltiModule::CLK_JC_NAME[CLK_NUMBER] =
  { "OSCILLATOR", "SWITCH" };

const std::string AltiModule::EXT_CLK_SOURCE_NAME[EXT_CLK_SOURCE_NUMBER] =
  { "Jitter cleaner", "CTP_IN", "ALTI_IN", "NIM_IN" };

const std::string AltiModule::CNT_NAME[CNT_NUMBER] =
  { "CNT_BC", "CNT_ORB", "CNT_L1A", "CNT_TTR1", "CNT_TTR2", "CNT_TTR3", "CNT_BGO0", "CNT_BGO1", "CNT_BGO2", "CNT_BGO3" };

const std::string AltiModule::SNAPSHOT_TYPE_NAME[SNAPSHOT_TYPE_NUMBER] =
  { "ALTI_LINE", "LTP_LINE" };

// busy
const std::string AltiModule::BUSY_INPUT_NAME[BUSY_INPUT_NUMBER] =
  { "BUSY_FROM_FRONT_PANEL", "BUSY_FROM_CTP", "BUSY_FROM_ALTI", "BUSY_FROM_PG", "BUSY_FROM_VME", "BUSY_FROM_ECR" };

const std::string AltiModule::BUSY_SOURCE_NAME[BUSY_SOURCE_NUMBER] =
  { "BUSY_INACTIVE", "BUSY_LOCAL", "BUSY_CTP", "BUSY_ALTI" };

const std::string AltiModule::BUSY_OUTPUT_NAME[BUSY_OUTPUT_NUMBER] =
  { "BUSY_TO_CTP", "BUSY_TO_ALTI" };

const std::string AltiModule::BUSY_LEVEL_NAME[BUSY_LEVEL_NUMBER] =
  { "NIM", "TTL" };

// calibration requests
const std::string AltiModule::CALREQ_INPUT_NAME[CALREQ_INPUT_NUMBER] =
  { "CALREQ_FROM_RJ45", "CALREQ_FROM_FRONT_PANEL", "CALREQ_FROM_CTP", "CALREQ_FROM_ALTI", "CALREQ_FROM_PG", "CALREQ_FROM_VME" };

const std::string AltiModule::CALREQ_SOURCE_NAME[CALREQ_SOURCE_NUMBER] =
  { "CALREQ_INACTIVE", "CALREQ_LOCAL", "CALREQ_CTP", "CALREQ_ALTI" };

const std::string AltiModule::CALREQ_OUTPUT_NAME[CALREQ_OUTPUT_NUMBER] =
  { "CALREQ_TO_CTP", "CALREQ_TO_ALTI" };

const std::string AltiModule::ALTI_RAM_MEMORY_NAME[ALTI_RAM_MEMORY_NUMBER] =
  { "TTC_DECODER_COMMAND", "TTC_DECODER_TIMESTAMP", "PATTERN_SNAPSHOT" };

const std::string AltiModule::TTC_FRAME_TYPE_NAME[TTC_FRAME_TYPE_NUMBER] =
  { "SHORT", "LONG" };

const std::string AltiModule::TTC_ADDRESS_SPACE_NAME[TTC_ADDRESS_SPACE_NUMBER] =
  { "INTERNAL", "EXTERNAL" };

const std::string AltiModule::TTC_BGO_MODE_TRIGGER_NAME[TTC_BGO_MODE_TRIGGER_NUMBER] =
  { "BGO_SIGNAL", "VME" };

const std::string AltiModule::TTC_BGO_MODE_COMMAND_NAME[TTC_BGO_MODE_COMMAND_NUMBER] =
  { "SYNCHRONOUS", "ASYNCHRONOUS" };

const std::string AltiModule::TTC_BGO_MODE_REPEAT_NAME[TTC_BGO_MODE_REPEAT_NUMBER] =
  { "SINGLE", "REPETITIVE" };

const std::string AltiModule::TTC_BGO_MODE_FIFO_NAME[TTC_BGO_MODE_FIFO_NUMBER] =
  { "WHEN_NOT_EMPTY", "ON_TRIGGER" };
  
const std::string AltiModule::TTC_BGO_MODE_INHIBIT_NAME[TTC_BGO_MODE_INHIBIT_NUMBER] =
  { "ON_INHIBIT", "OTHER_MODES" };

const std::string AltiModule::TTC_BGO_MODE_NAME[TTC_BGO_MODE_NUMBER] =
  { "SYNCHRONOUS_SINGLE_BGO_SIGNAL", "SYNCHRONOUS_REPETITIVE", "ASYNCHRONOUS_BGO_SIGNAL", "ASYNCHRONOUS_VME_ON_TRIGGER", "ASYNCHRONOUS_FIFO_MODE", "INVALID_MODE" };
     
const std::string AltiModule::TTYP_COUNTER_SOURCE_NAME[TTYP_COUNTER_SOURCE_NUMBER] =
  { "EVENT", "ORBIT" };

const AltiModule::BgoMode AltiModule::TTC_BGO_MODE_HL[TTC_BGO_MODE_NUMBER] =
  {
    { BGO_SIGNAL        , SYNCHRONOUS,  SINGLE                   , ON_TRIGGER                , OTHER_MODES}, // SYNCHRONOUS_SINGLE_BGO_SIGNAL
    { BGO_SIGNAL        , SYNCHRONOUS,  REPETITIVE               , ON_TRIGGER /*don't care*/ , ON_INHIBIT }, // SYNCHRONOUS_REPETITIVE
    { BGO_SIGNAL        , ASYNCHRONOUS, REPETITIVE /*don't care*/, ON_TRIGGER /*don't care*/ , OTHER_MODES}, // ASYNCHRONOUS_BGO_SIGNAL
    { VME               , ASYNCHRONOUS, SINGLE                   , ON_TRIGGER                , OTHER_MODES}, // ASYNCHRONOUS_VME_ON_TRIGGER
    { VME               , ASYNCHRONOUS, REPETITIVE /*don't care*/, WHEN_NOT_EMPTY            , OTHER_MODES}  // ASYNCHRONOUS_VME_WHEN_NOT_EMPTY
  };

const std::string AltiModule::TTC_FIFO_NAME[TTC_FIFO_NUMBER] =
  { "BGO0_FIFO", "BGO1_FIFO", "BGO2_FIFO", "BGO3_FIFO" };

const std::string AltiModule::SIGNAL_PG_NAME[SIGNAL_PG_NUMBER] =
  { "ORB", "L1A", "TTR1", "TTR2", "TTR3", "TTYP", "BGO0", "BGO1", "BGO2", "BGO3", "CALREQ0", "CALREQ1", "CALREQ2", "BUSY" };

const std::string AltiModule::ASYNC_INPUT_SIGNAL_NAME[ASYNC_INPUT_SIGNAL_NUMBER] =
  {
    "SWX_ORB", "SWX_L1A", "SWX_BGO0", "SWX_BGO1", "SWX_BGO2", "SWX_BGO3", "SWX_TTYP0", "SWX_TTYP1", "SWX_TTYP2", "SWX_TTYP3", "SWX_TTYP4", "SWX_TTYP5", "SWX_TTYP6", "SWX_TTYP7", "SWX_TTR1", "SWX_TTR2", "SWX_TTR3",
    "LEMO_ORB", "LEMO_L1A", "LEMO_TTR1", "LEMO_TTR2", "LEMO_TTR3", "LEMO_BGO2", "LEMO_BGO3", "LEMO_BUSY", "RJ45_CALREQ0", "RJ45_CALREQ1", "RJ45_CALREQ2",
    "CTP_BUSY", "CTP_CALREQ0", "CTP_CALREQ1", "CTP_CALREQ2",
    "ALTI_BUSY", "ALTI_CALREQ0", "ALTI_CALREQ1", "ALTI_CALREQ2",
    "CDR_LOCK", "CDR_L1A", "CDR_BRCST", "CDR_INDIV", "CDR_BCR", "CDR_ECR", "CDR_SERR", "CDR_DERR"
  };
const std::string AltiModule::ASYNC_INPUT_CALREQ_NAME[ASYNC_INPUT_CALREQ_NUMBER] =
  {
    "CTP_CRQ0", "CTP_CRQ1", "CTP_CRQ2", "ALTI_CRQ0", "ALTI_CRQ1", "ALTI_CRQ2", "RJ45_CRQ0", "RJ45_CRQ1", "RJ45_CRQ2"
  };
const std::string AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NAME[SYNCHRONIZER_INPUT_SIGNAL_NUMBER] =
  {
    "SYNC_IN_L1A", "SYNC_IN_ORB", "SYNC_IN_BGO0", "SYNC_IN_BGO1", "SYNC_IN_BGO2", "SYNC_IN_BGO3", "SYNC_IN_TTYP0", "SYNC_IN_TTYP1", "SYNC_IN_TTYP2", "SYNC_IN_TTYP3", "SYNC_IN_TTYP4", "SYNC_IN_TTYP5", "SYNC_IN_TTYP6",
    "SYNC_IN_TTYP7", "SYNC_IN_TTR1", "SYNC_IN_TTR2", "SYNC_IN_TTR3"
  };
const std::string AltiModule::SYNC_SHAPE_NAME[SYNC_SHAPE_NUMBER] =
  { "IN_SYNCHRONIZED", "IN_SHAPED_ONE", "IN_SHAPED_TWO" };
  
const std::string AltiModule::PBM_SIGNAL_NAME[PBM_SIGNAL_NUMBER] =
  { "L1A", "TTR1", "TTR2", "TTR3", "BGO0", "BGO1", "BGO2", "BGO3"};

const std::string AltiModule::MINICTP_INPUT_NAME[MINICTP_INPUT_NUMBER] =
  { "L1A", "TTR1", "TTR2", "TTR3", "BGO2", "BGO3", "CRQ0", "CRQ1", "CRQ2", "RND0", "RND1"};

const std::string AltiModule::MINICTP_CNT_NAME[MINICTP_CNT_NUMBER] =
  { "TBP", "TAP", "TAV"};

//------------------------------------------------------------------------------

// open VME
//RCD::VME *AltiModule::m_vme = RCD::VME::Open();

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


AltiModule::AltiModule(unsigned int slot) : m_vme(0), m_vmm(0), m_alti(0), m_segment(0), m_i2c(0), m_net(0), m_mem_pg_file(""), m_cnt_ttyp_lut_file(""), m_ds1wm(0), m_identifier("") {

    if (slot < ALTI_SLOT_MIN || slot > ALTI_SLOT_MAX) {
        std::printf("Invalid slot number!\n");
        m_status = -1;
        return;
    }
    else m_slot = slot;

    // open VME
    m_vme = RCD::VME::Open();

    // read MANUFACTURER ID
    unsigned int manufid;
    if ((m_status = CSRManufacturerIdRead(manufid)) != 0) {
        CERR("ALTI: reading MANUFACTURER ID from CSR in slot %2d", m_slot);
        return;
    }
    if (manufid != MANUF_ID) {
        CERR("ALTI: MANUFACTURER ID mismatch, read 0x%08x, expected 0x%08x", manufid, MANUF_ID);
        m_status = -1;
        return;
    }

    // read BOARD ID
    unsigned int boardid;
    if ((m_status = CSRBoardIdRead(boardid)) != 0) {
        CERR("ALTI: reading BOARD ID from CSR in slot %2d", m_slot);
        return;
    }
    if (boardid != BOARD_ID) {
        CERR("ALTI: BOARD ID mismatch, read 0x%08x, expected 0x%08x", boardid, BOARD_ID);
        m_status = -1;
        return;
    }

    // read REVISION ID
    unsigned int revid;
    if ((m_status = CSRRevisionIdRead(revid)) != 0) {
        CERR("ALTI: reading REVISION ID from CSR in slot %2d", m_slot);
        return;
    }

    unsigned int dd, mm, yy, revnum;
    AltiCommon::parseRevision(revid, dd, mm, yy, revnum);

    // read BAR
    unsigned int bar = 0;
    if ((m_status = CSRBARRead(bar)) != 0) {
        CERR("ALTI: reading BAR from CSR in slot %2d", m_slot);
        return;
    }
    if (bar != (m_slot << 3)) {
        CERR("ALTI[slot %2d]: BAR mismatch, read 0x%02x <-> slot %2d, expected 0x%02x", m_slot, bar, (bar >> 3) , m_slot);
        m_status = -1;
        return;
    }
    std::printf("ALTI: INFO - ALTI detected, revision = 0x%08x (#%d, %02d.%02d.%d), BAR = 0x%08x <-> slot %d\n", revid, revnum, dd, mm, yy, bar, bar  >> 3);

    // read ADER
    if ((m_status = CSRADERRead(m_ader)) != 0) {
        CERR("ALTI: reading ADER from CSR in slot %2d", m_slot);
        return;
    }

    // create low-level ALTI
    m_alti = new ALTI(m_vme, m_ader);
    if ((*m_alti)()) {
        CERR("ALTI: opening ALTI at vme = 0x%08x, size = 0x%08x, type = \"%s\"", m_ader, ALTI::size(), ALTI::code().c_str());
        m_status = -1;
        return;
    }
    std::printf("ALTI: INFO - ALTI opened at vme = 0x%08x, size = 0x%08x, type = \"%s\"\n", m_ader, ALTI::size(), ALTI::code().c_str());
    
    // fetch VME master mapping
    m_vmm = m_alti->getVmeMasterMap();

    std::string serial;

    // create DS1WM, initialize and read serial number
    m_ds1wm = new DS1WM(DS1WM::ALTI, m_vmm);
    if ((m_status = m_ds1wm->detectSerialNumber(serial)) != SUCCESS) {
        m_identifier = "PRE_PRODUCTION_MODULE";
        m_status = SUCCESS;
    }
    else {
        DS1WM::getIdentifier(serial, m_identifier);
    }

    // create and initialize I2C network
    if ((m_status = I2CInit()) != SUCCESS) {
        CERR("ALTI: initializing I2C network", "");
        return;
    }

    // create CMEM segements
    std::string snam = "AltiModule";
    m_segment = new RCD::CMEMSegment(snam, sizeof(u_int)*MAX_MEMORY, true);
    COUT("opened CMEM segment \"%s\" of size 0x%08x, phys 0ix%08x, virt 0x%08x", snam.c_str(), m_segment->Size(), m_segment->PhysicalAddress(), m_segment->VirtualAddress());

    std::printf("--------------------------------------------------------------------------------\n");
    std::printf("ALTI[slot %2d]: INFO - ALTI \"%s\" opened\n", m_slot, m_identifier.c_str());
    std::printf("--------------------------------------------------------------------------------\n");

}

//------------------------------------------------------------------------------

AltiModule::~AltiModule() {

    // delete CMEM segment
    if (m_segment) delete m_segment;

    // delete I2C network
    if (m_net) delete m_net;

    // delete I2C bus
    if (m_i2c) delete m_i2c;

    // delete DS1WM
    if (m_ds1wm) delete m_ds1wm;

    // delete low-level ALTI
    if (m_alti) delete m_alti;

    // close VME
    RCD::VME::Close();

}

//------------------------------------------------------------------------------

int AltiModule::setBaseAddress(unsigned int slot, unsigned int base) {

    if (slot < ALTI_SLOT_MIN || slot > ALTI_SLOT_MAX) {
        std::printf("Invalid slot number!\n");
        return (FAILURE);
    }

    int status;

    // read BAR
    unsigned int bar = 0;
    if ((status = CSRBARRead(slot, bar)) != SUCCESS) {
        CERR("ALTI: reading BAR from CSR in slot %2d", slot);
        return (status);
    }
    if (bar != (slot << 3)) {
        CERR("ALTI[slot %2d]: BAR mismatch, read 0x%02x <-> slot %2d, expected 0x%02x", slot, bar, (bar >> 3), slot);
        status = FAILURE;
        return (status);
    }
    std::printf("ALTI: INFO - ALTI detected, BAR = 0x%08x <-> slot %d\n", bar, bar >> 3);

    // read BOARD ID
    unsigned int boardid = 0;
    if ((status = CSRBoardIdRead(slot, boardid)) != 0) {
        CERR("ALTI: reading BOARD ID from CSR in slot %2d", slot);
        return (status);
    }
    if (boardid != BOARD_ID) {
        CERR("ALTI: BOARD ID mismatch, read 0x%08x, expected 0x%08x", boardid, BOARD_ID);
        status = FAILURE;
        return (status);
    }

    // read ADER
    unsigned int ader = 0;
    if ((status = CSRADERRead(slot, ader)) != SUCCESS) {
        CERR("ALTI: reading ADER from CSR in slot %2d", slot);
        return (status);
    }
    if (ader != base) {
        // write ADER
        if ((status = CSRADERWrite(slot, base)) != SUCCESS) {
            CERR("ALTI: writing ADER 0x%08x to CSR in slot %2d", base, slot);
            return (status);
        }
        // read ADER
        if ((status = CSRADERRead(slot, ader)) != SUCCESS) {
            CERR("ALTI: reading ADER from CSR in slot %2d", slot);
            return (status);
        }
        if (ader != base) {
            CERR("ALTI: ADER not written to properly!", "");
            status = FAILURE;
            return (status);
        }
    }

    return (SUCCESS);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::AltiResetControl(const bool GlobalReset, const bool TtcEncoderReset, const bool PatSnapReset, const bool SpyReset) {

  ALTI_CTL_RESETSCONTROL_BITSTRING bs;
  // READ
  if ((m_status = m_alti->CTL_ResetsControl_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading CTL reset control", "");
    return (m_status);
  }
  // MODIFY
  bs.GlobalReset(GlobalReset);
  bs.TtcEncoderReset(TtcEncoderReset);
  bs.PatSnapReset(PatSnapReset);
  bs.SpyReset(SpyReset);
  // WRITE
  if ((m_status = m_alti->CTL_ResetsControl_Write(bs)) != SUCCESS) {
    CERR("ALTI: writing CTL reset control", "");
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::AltiReset() {
 
  // Golbal reset
  if ((m_status = AltiResetControl(true, true, true, true)) != SUCCESS) {
    CERR("ALTI: doing global CTL reset", "");
    return (m_status);
  }

  // resetting QuickBoot control
  if ((m_status = FMWControlReset()) != SUCCESS) {
    CERR("ALTI: resetting QuickBoot control", "");
    return (m_status);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::AltiSetup() {
  
  // set up I2C
  if ((m_status = I2CSetup()) != SUCCESS) {
    CERR("ALTI: setting up I2C", "");
    return (m_status);
  }
  
  // set up jitter cleaner
  if ((m_status = JCSetup()) != SUCCESS) {
    CERR("ALTI: setting up jitter cleaner", "");
    return (m_status);
  }
  
  // set up CLK multiplexer
  if ((m_status = CLKSetup()) != SUCCESS) {
    CERR("ALTI: setting up CLK", "");
    return (m_status);
  }
  
  // set up CLK encoder
  if ((m_status = ENCSetup()) != SUCCESS) {
    CERR("ALTI: setting up encoder", "");
    return (m_status);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::AltiCheck() {

    bool ok(true);

    // check CLK multiplexer
    if ((m_status = CLKCheck()) != SUCCESS) {
        CERR("ALTI: checking CLK mux", "");
        ok = false;
    }
    else {
        COUT("ALTI: INFO - CLK mux                         = \"SETUP\"", "");
    }

    // check I2C core
    if ((m_status = I2CCheck()) != SUCCESS) {
        CERR("ALTI: checking I2C core", "");
        ok = false;
    }
    else {
        COUT("ALTI: INFO - I2C core                        = \"SETUP\"", "");
    }

    // check PLL lock
    bool clklol, clklos, sticky_clklol, sticky_clklos;
    if ((m_status = CLKStatusReadPLL(clklol, clklos, sticky_clklol, sticky_clklos)) != SUCCESS) {
        CERR("ALTI: checking PLL lock", "");
        ok = false;
    }
    else if (!clklol) {
        COUT("ALTI: INFO - PLL                             = \"LOCKED\"", "");
    }
    else {
        COUT("ALTI: INFO - PLL                             = \"NOT LOCKED\" !!!!", "");
        ok = false;
    }

    // check jitter cleaner
    std::string designid;
    if ((m_status = CLKJitterCleanerDesignIDRead(designid)) != SUCCESS) {
        CERR("ALTI: checking jitter cleaner design ID", "");
        ok = false;
    }
    else if (designid == "ALTI_001") {
        COUT("ALTI: INFO - jitter cleaner design           = \"%s\" (DEFAULT)", designid.c_str());
    }
    else {
        COUT("ALTI: INFO - jitter cleaner design           = \"%s\" (NON-DEFAULT !!!!)", designid.c_str());
    }
    std::vector<bool> oof, los;
    bool hold, lol;
    if ((m_status = CLKJitterCleanerStatusRead(oof, los, hold, lol)) != SUCCESS) {
        CERR("ALTI: checking jitter cleaner status", "");
        ok = false;
    }
    else if (hold == true) {
        COUT("ALTI: INFO - jitter cleaner mode             = \"HOLDOVER\" (NON-DEFAULT !!!!)", "");
        ok = false;
    }

    // check decoder
    if ((m_status = ENCCheck()) != SUCCESS) {
        CERR("ALTI: checking TTC encoder", "");
        ok = false;
    }
    else {
        COUT("ALTI: INFO - TTC encoder                     = \"SETUP\"", "");
    }
  
    return(ok?SUCCESS:FAILURE);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::CFGRead(AltiConfiguration &cfg, std::ostream &os) {

    char s[STRING_LENGTH];

    // CLK
    if ((m_status = CFGReadCLK(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CLK configuration"); MERR(os, s);
        return (m_status);
    }

    // SIG
    if ((m_status = CFGReadSIG(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading SIG configuration"); MERR(os, s);
        return (m_status);
    }

    // BSY
    if ((m_status = CFGReadBSY(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading BSY configuration"); MERR(os, s);
        return (m_status);
    }

    // CRQ
    if ((m_status = CFGReadCRQ(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CRQ configuration"); MERR(os, s);
        return (m_status);
    }

    // MEM
    if ((m_status = CFGReadMEM(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading MEM configuration"); MERR(os, s);
        return (m_status);
    }

    // TTC
    if ((m_status = CFGReadTTC(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading TTC configuration"); MERR(os, s);
        return (m_status);
    }
    
    // CNT
    if ((m_status = CFGReadCNT(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CNT configuration"); MERR(os, s);
        return (m_status);
    }
    
    // PBM
    if ((m_status = CFGReadPBM(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading PBM configuration"); MERR(os, s);
        return (m_status);
    }
    
    // CTP
    if ((m_status = CFGReadCTP(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CTP configuration"); MERR(os, s);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWrite(const AltiConfiguration &cfg, std::ostream &os) {

    char s[STRING_LENGTH];

    // check the clock 
    bool clk_lol, clk_los, sticky_clk_lol, sticky_clk_los;
    if ((m_status = CLKStatusReadPLL(clk_lol, clk_los, sticky_clk_lol, sticky_clk_los)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading CLK PLL status"); MERR(os, s);
      return (m_status);
    } 
    if (clk_lol) {
      // set internal clock
      if ((m_status = CLKInputSelectWriteJitterCleaner(CLK_JC_TYPE::OSCILLATOR)) != SUCCESS) { 
	std::sprintf(s, "ALTI: setting Jitter Cleaner from the internal CLK "); MERR(os, s);
	return (m_status);
      }

      if ((m_status = CLKInputSelectWritePLL(CLK_PLL_TYPE::JITTER_CLEANER)) != SUCCESS) { 
	std::sprintf(s, "ALTI: setting PLL from the Jitter Cleaner "); MERR(os, s);
	return (m_status);
      }
      // wait for PLL to lock
      usleep(1);
    }


    // SIG
    if ((m_status = CFGWriteSIG(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing SIG configuration"); MERR(os, s);
        return (m_status);
    }

    // BSY
    if ((m_status = CFGWriteBSY(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing BSY configuration"); MERR(os, s);
        return (m_status);
    }

    // CRQ
    if ((m_status = CFGWriteCRQ(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing CRQ configuration"); MERR(os, s);
        return (m_status);
    }

    // MEM
    if ((m_status = CFGWriteMEM(cfg, os)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing MEM configuration"); MERR(os, s);
        return (m_status);
    }

    // TTC
    if ((m_status = CFGWriteTTC(cfg, os)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing TTC configuration"); MERR(os, s);
      return (m_status);
    }
    
    // CTP
    if ((m_status = CFGWriteCNT(cfg, os)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing CNT configuration"); MERR(os, s);
      return (m_status);
    }

    // PBM
    if ((m_status = CFGWritePBM(cfg, os)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing CTP configuration"); MERR(os, s);
      return (m_status);
    }

    // CTP
    if ((m_status = CFGWriteCTP(cfg, os)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing CTP configuration"); MERR(os, s);
      return (m_status);
    }

    // CLK confiuration trick (read from config file first, config at the end so PLL won't lose lock)
    
    // CLK
    if ((m_status = CFGWriteCLK(cfg, os)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing CLK configuration"); MERR(os, s);
      return (m_status);
    }
        
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGReadCLK(AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.clk_config) return (m_status);

    char s[STRING_LENGTH];

    if ((m_status = CLKInputSelectReadPLL(cfg.clk_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CLK select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = CLKInputSelectReadJitterCleaner(cfg.clk_jc_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CLK jitter cleaner select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = CLKPhasePositionReadPLL(cfg.clk_phase_shift)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading CLK output phase shift"); MERR(os, s);
      return (m_status); 
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGReadSIG(AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.sig_config) return (m_status);

    char s[STRING_LENGTH];

    if ((m_status = SIGMuxBGOOutputLEMORead(cfg.sig_fp_bgo2enable, cfg.sig_fp_bgo3enable)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading SIG:BGO configuration"); MERR(os, s);
        return (m_status);
    }

    unsigned i, j;
    for (i = 0; i < SIGNAL_NUMBER_ROUTED; i++) {
        for (j = 0; j < SIGNAL_DESTINATION_NUMBER; j++) {
            if ((m_status = SIGSwitchConfigRead(SIGNAL(i), SIGNAL_DESTINATION(j), cfg.sig_swx[i][j])) != SUCCESS) {
                std::sprintf(s, "ALTI: reading SIG:SWX configuration, signal %s, destination %s", SIGNAL_NAME[i].c_str(), SIGNAL_DESTINATION_NAME[j].c_str()); MERR(os, s);
                return (m_status);
            }
        }
    }
    cfg.readSWXIndividualToBundleTTYP();
    
    if ((m_status = SIGEqualizersConfigRead(CTP_IN, cfg.sig_eqz_ctp_in_enable, cfg.sig_eqz_ctp_in_mode)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:EQZ configuration for %s", SIGNAL_SOURCE_NAME[CTP_IN].c_str()); MERR(os, s);
      return (m_status);
    }
    if ((m_status = SIGEqualizersConfigRead(ALTI_IN, cfg.sig_eqz_alti_in_enable, cfg.sig_eqz_alti_in_mode)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:EQZ configuration for %s", SIGNAL_SOURCE_NAME[ALTI_IN].c_str()); MERR(os, s);
      return (m_status);
    }
    
    if ((m_status = SIGInputSyncEnableRead(cfg.sig_io_sync_phase)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO sync configuration"); MERR(os, s);
      return (m_status);
    }
                    
    if ((m_status = SIGSyncShapeSwitchRead(cfg.sig_io_shaping)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO shape configuration"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = SIGOutputSelectOrbitRead(cfg.sig_orb_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO orbit source"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = SIGOutputSelectL1aRead(cfg.sig_l1a_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO L1A source"); MERR(os, s);
      return (m_status);
    }
   
    for (u_int ttr = 0; ttr < ALTI_CTL_OUTPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER; ttr++) {
      if ((m_status = SIGOutputSelectTestTriggerRead(ttr, cfg.sig_ttr_source[ttr])) != SUCCESS) {
	std::sprintf(s, "ALTI: reading SIG:IO TTR source"); MERR(os, s);
	return (m_status);
      }
    } 

    for (u_int bgo = 0; bgo < ALTI_CTL_OUTPUTSELECT_BITSTRING::BGO_NUMBER; bgo++) {
      if ((m_status = SIGOutputSelectBgoRead(bgo, cfg.sig_bgo_source[bgo])) != SUCCESS) {
	std::sprintf(s, "ALTI: reading SIG:IO BGO source"); MERR(os, s);
	return (m_status);
      }
    } 

    if ((m_status = SIGOutputSelectTriggerTypeRead(cfg.sig_ttyp_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO Trigger Type source"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = SIGInputSelectOrbitRead(cfg.sig_orb_input_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO orbit input source"); MERR(os, s);
      return (m_status);
    }
    
    if ((m_status = SIGTurnCounterMaskRead(cfg.turn_cnt_mask)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO turn counter mask"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = SIGTurnCounterFreqRead(cfg.turn_cnt_max)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO turn counter maximum"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = SIGBGo2L1aDelayRead(cfg.bgo2_l1a_delay)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO BGo-2 - L1A delay"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = CTPInputL1aRead(cfg.ctp_l1a_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading CTP L1A source"); MERR(os, s);
      return (m_status);
    }
   
    if ((m_status = CTPInputBgo2Read(cfg.ctp_bgo2_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading CTP BGo-2 source"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = CTPInputBgo3Read(cfg.ctp_bgo3_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading CTP BGo-3 source"); MERR(os, s);
      return (m_status);
    }

    for (u_int ttr = 0; ttr < ALTI_CTP_INP_INPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER; ttr++) {
      if ((m_status = CTPInputTtrRead(ttr, cfg.ctp_ttr_source[ttr])) != SUCCESS) {
	std::sprintf(s, "ALTI: reading CTP TTR source"); MERR(os, s);
	return (m_status);
      }
    } 

    for (u_int crq = 0; crq < ALTI_CTP_INP_INPUTSELECT_BITSTRING::CALREQUEST_NUMBER; crq++) {
      if ((m_status = CTPInputCalreqRead(crq, cfg.ctp_crq_source[crq])) != SUCCESS) {
	std::sprintf(s, "ALTI: reading CTP CALREQ source"); MERR(os, s);
	return (m_status);
      }
    }    

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGReadBSY(AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.bsy_config) return (m_status);

    char s[STRING_LENGTH];

    if ((m_status = BSYInputSelectRead(cfg.bsy_in_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading BSY input select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = BSYOutputSelectRead(cfg.bsy_out_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading BSY output select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = BSYConstLevelRegRead(cfg.bsy_data_vme)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading BSY const level register configuration"); MERR(os, s);
        return (m_status);
    }

    bool mask;
    if ((m_status = BSYMaskingL1aEnableRead(mask)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading BSY masking of L1A configuration"); MERR(os, s);
        return (m_status);
    }
    cfg.bsy_l1a_masked = mask;

    bool mask0, mask1, mask2, mask3;
    if ((m_status = BSYMaskingTTREnableRead(mask1, mask2, mask3)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading BSY masking of TTR configuration"); MERR(os, s);
      return (m_status);
    }
    cfg.bsy_ttr_masked[0] = mask1;
    cfg.bsy_ttr_masked[1] = mask2;
    cfg.bsy_ttr_masked[2] = mask3;

    if ((m_status = BSYMaskingBGoEnableRead(mask0, mask1, mask2, mask3)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading BSY masking of BGo configuration"); MERR(os, s);
        return (m_status);
    }
    cfg.bsy_bgo_masked[0] = mask0;
    cfg.bsy_bgo_masked[1] = mask1;
    cfg.bsy_bgo_masked[2] = mask2;
    cfg.bsy_bgo_masked[3] = mask3;

    if ((m_status = BSYFrontPanelLevelRead(cfg.bsy_level)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading BSY Front-Panel level configuration"); MERR(os, s);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGReadCRQ(AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.crq_config) return (m_status);

    char s[STRING_LENGTH];

    if ((m_status = CRQInputSelectRead(cfg.crq_in_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CRQ input select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = CRQOutputSelectRead(CALREQ_TO_CTP, cfg.crq_out_select[0])) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CRQ CTP output select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = CRQOutputSelectRead(CALREQ_TO_ALTI, cfg.crq_out_select[1])) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CRQ ALTI output select configuration"); MERR(os, s);
        return (m_status);
    }

    for(u_int i=0; i<ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER; i++) {
      bool ena;
      if ((m_status = CRQConstLevelRegRead(i, ena)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading CRQ const level register configuration"); MERR(os, s);
        return (m_status);
      }
      cfg.crq_data_vme[i] = ena;
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGReadMEM(AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.mem_config) return (m_status);

    char s[STRING_LENGTH];

    // read PAT generation enable
    if ((m_status = PRMEnableRead(cfg.mem_pg_enable)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading PAT generation enable"); MERR(os, s);
        return (m_status);
    }

    // read PAT generation repeat
    if ((m_status = PRMRepeatRead(cfg.mem_pg_repeat)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading PAT generation repeat configuration"); MERR(os, s);
        return (m_status);
    }

    // read PAT generation start address
    if ((m_status = PRMStartAddressRead(cfg.mem_pg_start_addr)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading PAT generation start address"); MERR(os, s);
        return (m_status);
    }

    // read PAT generation stop address
    if ((m_status = PRMStopAddressRead(cfg.mem_pg_stop_addr)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading PAT generation stop address"); MERR(os, s);
        return (m_status);
    }

    cfg.mem_pg_file = m_mem_pg_file;

    // read signal mask
    for(u_int sig =0; sig < AltiModule::SIGNAL_NUMBER; sig++) {
      bool masked;
      if ((m_status = PRMSnapshotSignalMaskRead(AltiModule::SIGNAL(sig), masked)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading PAT signal mask"); MERR(os, s);
        return (m_status);
      }
      cfg.mem_snap_masked.at(AltiModule::SIGNAL(sig)) = masked;
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGReadTTC(AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.ttc_config) return (m_status);

    char s[STRING_LENGTH];

    std::vector<bool> enabled;
    enabled.resize(TRANSMITTER_NUMBER); 
    if ((m_status = ENCTransmitterEnableRead(enabled)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC:TX enable configuration"); MERR(os, s);
      return (m_status);
    }
    std::vector<u_int> delay;
    enabled.resize(TRANSMITTER_NUMBER); 
    if ((m_status = ENCTransmitterDelayRead(delay)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC:TX delay configuration"); MERR(os, s);
      return (m_status);
    }
    
    u_int i;
    for (i = 0; i < TRANSMITTER_NUMBER; i++) {
      cfg.ttc_tx_enable[i] = enabled[i];
      cfg.ttc_tx_delay[i] = delay[i];
    }
    
    if ((m_status = ENCInhibitParamsRead(cfg.ttc_bgo_inhibit_width, cfg.ttc_bgo_inhibit_delay)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC BGO inhibit width and delay configuration"); MERR(os, s);
      return (m_status);
    }
    if ((m_status = ENCTriggerTypeDelayRead(cfg.ttc_ttyp_delay)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC TTYP word delay configuration"); MERR(os, s);
      return (m_status);
    }
    bool ena;
    if ((m_status = ENCTriggerTypeControlRead(ena, cfg.ttc_ttyp_addr, cfg.ttc_ttyp_address_space, cfg.ttc_ttyp_subaddr)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC TTYP control configuration"); MERR(os, s);
      return (m_status);
    }
    if ((m_status = ENCBgoCommandModeRead(cfg.ttc_bgo_mode)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC BGO mode configuration"); MERR(os, s);
      return (m_status);
    }
    if ((m_status = ENCBgoFifoRetransmitRead(cfg.ttc_fifo_retransmit)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC FIFOs retransmit configuration"); MERR(os, s);
      return (m_status);
    }
    u_int word;
    for (i = 0; i < TTC_FIFO_NUMBER; i++) {
      if ((m_status = ENCFifoRead((TTC_FIFO) i, word)) != SUCCESS) {
	std::sprintf(s, "ALTI: reading TTC FIFOs word"); MERR(os, s);
	return (m_status);
      }
      cfg.ttc_fifo_word[i] = word  >> 23;
    }
    
    bool ttc_l1a_source[7];  
    if ((m_status = ENCL1ASourceRead(ttc_l1a_source[0],ttc_l1a_source[1],ttc_l1a_source[2],ttc_l1a_source[3],ttc_l1a_source[4],ttc_l1a_source[5],ttc_l1a_source[6])) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC L1A source configuration"); MERR(os, s);
      return (m_status);
    }
    for(u_int i=0; i<7; i++) cfg.ttc_l1a_source[i] = ttc_l1a_source[i];

    std::string ttc_orb_source;
    if ((m_status = ENCOrbitSourceRead(ttc_orb_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC Orbit source configuration"); MERR(os, s);
      return (m_status);
    }
    cfg.ttc_orb_source = ttc_orb_source;


    std::string ttc_bgo_source;  
    for(u_int i=0; i<TTC_FIFO_NUMBER; i++) {
      if ((m_status = ENCBgoSourceRead(i, ttc_bgo_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC BGo source configuration"); MERR(os, s);
      return (m_status);
      }
      cfg.ttc_bgo_source[i] = ttc_bgo_source;
    }

    std::string ttc_ttyp_source;
    if ((m_status = ENCTtypSourceRead(ttc_ttyp_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading TTC TTYP source configuration"); MERR(os, s);
      return (m_status);
    }
    cfg.ttc_ttyp_source = ttc_ttyp_source;    

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGReadCNT(AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.cnt_config) return (m_status);

    char s[STRING_LENGTH];

    bool status;
    if ((m_status = CNTEnableRead(cfg.cnt_enable, status)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading counters enable"); MERR(os, s);
        return (m_status);
    }

    // read counter trigger type LUT
    if(cfg.cnt_ttyp_lut_file.empty()) {
      if((m_status = CNTTtypLutRead(cfg.cnt_ttyp_lut)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CNT trigger type LUT into configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CNTTtypLutRead(cfg.cnt_ttyp_lut_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP trigger type LUT into file \"%s\"", cfg.cnt_ttyp_lut_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGReadCTP(AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.ctp_config) return (m_status);

    char s[STRING_LENGTH];

    // read ECR generation
    if ((m_status = ECRGenerationRead(cfg.ctp_ecr_generation)) != SUCCESS) {
        std::sprintf(s, "ALTI: reading ECR generation control"); MERR(os, s);
        return (m_status);
    }
    
    // read items LUT
    if(cfg.ctp_item_lut_file.empty()) {
      if((m_status = CTPTriggerLutRead(cfg.ctp_item_lut)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP items LUT into configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else { 
      if((m_status = CTPTriggerLutRead(cfg.ctp_item_lut_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP items LUT into file \"%s\"", cfg.ctp_item_lut_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }
    
    // read TTYP LUT
    if(cfg.ctp_ttyp_lut_file.empty()) {
      if((m_status = CTPTtypLutRead(cfg.ctp_ttyp_lut)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP TTYP LUT into configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CTPTtypLutRead(cfg.ctp_ttyp_lut_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP TTYP LUT into file \"%s\"", cfg.ctp_ttyp_lut_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }
    
    // read random 
    cfg.ctp_rndm_seed_valid = true;
    for(u_int rndm = 0; rndm < ALTI::CTP_RND_THRESHOLD_NUMBER; rndm++) {
      if((m_status = CTPRandomTriggerSeedRead(rndm, cfg.ctp_rndm_seed[rndm])) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP RNDM%d seet", rndm); MERR(os,s);
	return(m_status);
      }
      if((m_status = CTPRandomTriggerThresholdRead(rndm, cfg.ctp_rndm_threshold[rndm])) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP RNDM%d threshold", rndm); MERR(os,s);
	return(m_status);
      }
    }
    
    // read BGRP data
    if(cfg.ctp_bgrp_file.empty()) {
      if((m_status = CTPBunchGroupDataRead(cfg.ctp_bgrp)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP BGRP into configuration"); MERR(os,s);
	return(m_status);
        }
    }
    else {
        if((m_status = CTPBunchGroupDataRead(cfg.ctp_bgrp_file)) != SUCCESS) {
	  std::sprintf(s,"ALTI: reading CTP BGRP into file \"%s\"", cfg.ctp_bgrp_file.c_str()); MERR(os,s);
	  return(m_status);
        }
    }
    
    // read BGRP BCID offset
    if((m_status = CTPBunchGroupBcidOffsetRead(cfg. ctp_bgrp_bcid_offset)) != SUCCESS) {
        std::sprintf(s,"ALTI: reading CTP BGRP BCID offset"); MERR(os,s);
        return(m_status);
    }

    // read BGRP mask
    if(cfg.ctp_bgrp_mask_file.empty()) {
      if((m_status = CTPBunchGroupMaskRead(cfg.ctp_bgrp_mask)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP BGRP mask into configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CTPBunchGroupMaskRead(cfg.ctp_bgrp_mask_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CTP BGRP mask into file \"%s\"", cfg.ctp_bgrp_mask_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }

    // read prescale 
    cfg.ctp_prsc_seed_valid = true;
    if(cfg.ctp_prsc_seed_file.empty()) {
      if((m_status = CTPPrescalerSeedRead(cfg.ctp_prsc_seed)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading PRSC seed into configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CTPPrescalerSeedRead(cfg.ctp_prsc_seed_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading PRSC seed into file \"%s\"", cfg.ctp_prsc_seed_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }
    if(cfg.ctp_prsc_threshold_file.empty()) {
      if((m_status = CTPPrescalerThresholdRead(cfg.ctp_prsc_threshold)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading PRSC threshold into configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CTPPrescalerThresholdRead(cfg.ctp_prsc_threshold_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading PRSC threshold into file \"%s\"", cfg.ctp_prsc_threshold_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }

    // TAV enabled
    if((m_status = CTPTavEnableRead(cfg.ctp_tav_enable)) != SUCCESS) {
      std::sprintf(s,"ALTI: reading TAV enable into configuration"); MERR(os,s);
      return(m_status);
    }

    // read SMPL deadtime
    if((m_status = CTPSimpleDeadtimeWidthRead(cfg.ctp_smpl_deadtime)) != SUCCESS) {
      std::sprintf(s,"ALTI: reading SMPL deadtime width"); MERR(os,s);
      return(m_status);
    }
    
    // read CPLX leady bucket
    u_int rte, lvl;
    bool ena;
    for(u_int bck = 0; bck < LEAKY_BUCKET_NUMBER; bck++) {
      if((m_status = CTPLeakyBucketConfigRead(bck, rte, lvl)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CPLX leaky bucket %d configuration", bck); MERR(os,s);
	return(m_status);
      }
      if((m_status = CTPLeakyBucketEnableRead(bck, ena)) != SUCCESS) {
	std::sprintf(s,"ALTI: reading CPLX leaky bucket %d enable", bck); MERR(os,s);
	return(m_status);
      }
      cfg.ctp_cplx_bucket[bck].rate = rte;
      cfg.ctp_cplx_bucket[bck].level = lvl;
      cfg.ctp_cplx_bucket[bck].enable = ena;
    }
    
    // read CPLX sliding window
    u_int wdw, num;    
    if((m_status = CTPSlidingWindowConfigRead(wdw, num)) != SUCCESS) {
      std::sprintf(s,"ALTI: reading CPLX sliding window configuration"); MERR(os,s);
      return(m_status);
    }
    if((m_status = CTPSlidingWindowEnableRead(ena)) != SUCCESS) {
      std::sprintf(s,"ALTI: reading CPLX sliding window enable"); MERR(os,s);
      return(m_status);
    }
    cfg.ctp_cplx_sliding_window.window = wdw;
    cfg.ctp_cplx_sliding_window.number = num;
    cfg.ctp_cplx_sliding_window.enable = ena;
    
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGReadPBM(AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.pbm_config) return (m_status);


    if ((m_status = m_alti->PBM_BcidOffset_Read(cfg.pbm_bcid_offset)) != SUCCESS) {
      CERR("ALTI: reading PBM BCID offset register","");
      return(m_status);
    }
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWriteCLK(const AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.clk_config) return (m_status);

    char s[STRING_LENGTH];
    
    // Write CLK JC configuration only if cfg.clk_jc_file is not empty
    if(cfg.clk_jc_file == "DEFAULT") {
      if ((m_status = CLKJitterCleanerConfigWrite()) != SUCCESS) {
        std::sprintf(s, "ALTI: writing CLK jitter cleaner default configuration"); MERR(os, s);
        return (m_status);
      }
    }
    else if (cfg.clk_jc_file != "") {
      if((m_status = CLKJitterCleanerConfigWrite(cfg.clk_jc_file)) != SUCCESS) {
	std::sprintf(s, "ALTI: writing CLK jitter cleaner configuration from file %s", cfg.clk_jc_file.c_str()); MERR(os, s);
	return (m_status);
      }
    }

    // change PLL input clock only if needed 
    CLK_PLL_TYPE clk_input;
    if ((m_status = CLKInputSelectReadPLL(clk_input)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading CLK select configuration"); MERR(os, s);
      return (m_status);
    }
    if(clk_input != cfg.clk_select) {
      if ((m_status = CLKInputSelectWritePLL(cfg.clk_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing CLK select configuration"); MERR(os, s);
        return (m_status);
      }
    }

    // change Jitter Cleaner input clock only if needed 
    CLK_JC_TYPE jc_input;
    if ((m_status = CLKInputSelectReadJitterCleaner(jc_input)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading CLK jitter cleaner select configuration"); MERR(os, s);
      return (m_status);
    }
    if(jc_input != cfg.clk_jc_select) {    
      if ((m_status = CLKInputSelectWriteJitterCleaner(cfg.clk_jc_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing CLK jitter cleaner select configuration"); MERR(os, s);
        return (m_status);
      }
    }
    
    const bool increment = (cfg.clk_phase_shift > 0)? true : false;  
    const u_int phase = abs(cfg.clk_phase_shift);
    if ((m_status = CLKPhaseResetPLL()) != SUCCESS) {
      std::sprintf(s, "ALTI: resetting CLK phase shift"); MERR(os, s);
      return (m_status);
    }
    
    if ((m_status |= CLKPhaseShiftPLL(increment, phase)) !=  AltiModule::SUCCESS) {
      std::sprintf(s, "ALTI: writing CLK phase shift"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = CLKStatusClearPLL()) != SUCCESS) {
      std::sprintf(s, "ALTI: clearing CLK PLL status"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = CLKJitterCleanerStatusClear()) != SUCCESS) {
      std::sprintf(s, "ALTI: clearing CLK jitter cleaner status"); MERR(os, s);
      return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWriteSIG(const AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.sig_config) return (m_status);

    char s[STRING_LENGTH];

    if ((m_status = SIGMuxBGOOutputLEMOWrite(cfg.sig_fp_bgo2enable, cfg.sig_fp_bgo3enable)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing SIG:BGO configuration"); MERR(os, s);
        return (m_status);
    }

    unsigned i, j;
    //unsigned int voltage[EQUALIZER_VOLTAGE_NUMBER];
    for (i = 0; i < SIGNAL_NUMBER_ROUTED; i++) {
        for (j = 0; j < SIGNAL_DESTINATION_NUMBER; j++) {
            if ((SIGNAL_NAME[i]).rfind("TTYP", 0) == 0) {
                if ((m_status = SIGSwitchConfigWrite(SIGNAL(i), SIGNAL_DESTINATION(j), cfg.sig_swx_ttyp[j])) != SUCCESS) {
                    std::sprintf(s, "ALTI: writing SIG configuration, signal %s, destination %s", SIGNAL_NAME[i].c_str(), SIGNAL_DESTINATION_NAME[j].c_str()); MERR(os, s);
                    return (m_status);
                }
            }
            else {
                if ((m_status = SIGSwitchConfigWrite(SIGNAL(i), SIGNAL_DESTINATION(j), cfg.sig_swx[i][j])) != SUCCESS) {
                    std::sprintf(s, "ALTI: writing SIG configuration, signal %s, destination %s", SIGNAL_NAME[i].c_str(), SIGNAL_DESTINATION_NAME[j].c_str()); MERR(os, s);
                    return (m_status);
                }
            }
        }
    }
  
    if ((m_status = SIGEqualizersConfigWrite(CTP_IN,cfg.sig_eqz_ctp_in_enable, cfg.sig_eqz_ctp_in_mode)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:EQZ configuration for %s", SIGNAL_SOURCE_NAME[CTP_IN].c_str()); MERR(os, s);
      return (m_status);
    }
    if ((m_status = SIGEqualizersConfigWrite(ALTI_IN,cfg.sig_eqz_alti_in_enable, cfg.sig_eqz_alti_in_mode)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:EQZ configuration for %s", SIGNAL_SOURCE_NAME[ALTI_IN].c_str()); MERR(os, s);
      return (m_status);
    }

    if ((m_status = SIGInputSyncEnableWrite(cfg.sig_io_sync_phase)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:IO sync configuration"); MERR(os, s);
      return (m_status);
    }
    
    if ((m_status = SIGSyncShapeSwitchWrite(cfg.sig_io_shaping)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:IO shape configuration"); MERR(os, s);
      return (m_status);
    }
    
   if ((m_status = SIGOutputSelectOrbitWrite(cfg.sig_orb_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:IO orbit source"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = SIGOutputSelectL1aWrite(cfg.sig_l1a_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:IO L1A source"); MERR(os, s);
      return (m_status);
    }
   
    for (u_int ttr = 0; ttr < ALTI_CTL_OUTPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER; ttr++) {
      if ((m_status = SIGOutputSelectTestTriggerWrite(ttr, cfg.sig_ttr_source[ttr])) != SUCCESS) {
	std::sprintf(s, "ALTI: writing SIG:IO TTR source"); MERR(os, s);
	return (m_status);
      }
    } 

    for (u_int bgo = 0; bgo < ALTI_CTL_OUTPUTSELECT_BITSTRING::BGO_NUMBER; bgo++) {
      if ((m_status = SIGOutputSelectBgoWrite(bgo, cfg.sig_bgo_source[bgo])) != SUCCESS) {
	std::sprintf(s, "ALTI: writing SIG:IO BGO source"); MERR(os, s);
	return (m_status);
      }
    } 

    if ((m_status = SIGOutputSelectTriggerTypeWrite(cfg.sig_ttyp_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:IO Trigger Type source"); MERR(os, s);
      return (m_status);
    }
   
   if ((m_status = SIGInputSelectOrbitWrite(cfg.sig_orb_input_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:IO orbit input source"); MERR(os, s);
      return (m_status);
    }
    if ((m_status = SIGTurnCounterMaskWrite(cfg.turn_cnt_mask)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:IO turn counter mask"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = SIGTurnCounterFreqWrite(cfg.turn_cnt_max)) != SUCCESS) {
      std::sprintf(s, "ALTI: reading SIG:IO turn counter maximum"); MERR(os, s);
      return (m_status);
    }

    if ((m_status = SIGBGo2L1aDelayWrite(cfg.bgo2_l1a_delay)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing SIG:IO BGo-2 - L1A delay"); MERR(os, s);
      return (m_status);
    }

   if ((m_status = CTPInputL1aWrite(cfg.ctp_l1a_source)) != SUCCESS) {
     std::sprintf(s, "ALTI: writing CTP L1A source"); MERR(os, s);
     return (m_status);
   }
   
   if ((m_status = CTPInputBgo2Write(cfg.ctp_bgo2_source)) != SUCCESS) {
     std::sprintf(s, "ALTI: writing CTP BGo-2 source"); MERR(os, s);
     return (m_status);
   }

   if ((m_status = CTPInputBgo3Write(cfg.ctp_bgo3_source)) != SUCCESS) {
     std::sprintf(s, "ALTI: writing CTP BGo-3 source"); MERR(os, s);
     return (m_status);
   }

   for (u_int ttr = 0; ttr < ALTI_CTP_INP_INPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER; ttr++) {
     if ((m_status = CTPInputTtrWrite(ttr, cfg.ctp_ttr_source[ttr])) != SUCCESS) {
       std::sprintf(s, "ALTI: writing CTP TTR source"); MERR(os, s);
       return (m_status);
     }
   } 

   for (u_int crq = 0; crq < ALTI_CTP_INP_INPUTSELECT_BITSTRING::CALREQUEST_NUMBER; crq++) {
     if ((m_status = CTPInputCalreqWrite(crq, cfg.ctp_crq_source[crq])) != SUCCESS) {
       std::sprintf(s, "ALTI: writing CTP CALREQ source"); MERR(os, s);
       return (m_status);
     }
   } 

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWriteBSY(const AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.bsy_config) return (m_status);

    char s[STRING_LENGTH];

    if ((m_status = BSYInputSelectWrite(cfg.bsy_in_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing BSY input select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = BSYOutputSelectWrite(cfg.bsy_out_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing BSY output select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = BSYConstLevelRegWrite(cfg.bsy_data_vme)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing BSY const level register configuration"); MERR(os, s);
        return (m_status);
    }

    bool input_masking   = (cfg.sig_l1a_source == "PATTERN") ? false : cfg.bsy_l1a_masked;
    bool pattern_masking = (cfg.sig_l1a_source == "PATTERN") ? cfg.bsy_l1a_masked : false;

    if ((m_status = BSYMaskingL1aEnableWrite(input_masking)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing BSY masking of L1A configuration"); MERR(os, s);
      return (m_status);
    }
    if ((m_status = BSYMaskingPatL1aEnableWrite(pattern_masking)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing BSY masking of L1A configuration"); MERR(os, s);
      return (m_status);
    }
 
    for (u_int ttr = 0; ttr < ALTI_CTL_OUTPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER; ttr++) {
      input_masking   = (cfg.sig_ttr_source[ttr] == "PATTERN") ? false : cfg.bsy_ttr_masked[ttr];
      pattern_masking = (cfg.sig_ttr_source[ttr] == "PATTERN") ? cfg.bsy_ttr_masked[ttr] : false;
      if ((m_status = BSYMaskingTTREnableWrite(ttr, input_masking)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing BSY masking of TTR%d configuration", ttr+1); MERR(os, s);
        return (m_status);
      }
      if ((m_status = BSYMaskingPatTTREnableWrite(ttr, pattern_masking)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing BSY masking of TTR%d configuration", ttr+1); MERR(os, s);
        return (m_status);
      }
    }
    
    for (u_int bgo = 0; bgo < ALTI_CTL_OUTPUTSELECT_BITSTRING::BGO_NUMBER; bgo++) {
      input_masking   = (cfg.sig_bgo_source[bgo] == "PATTERN") ? false : cfg.bsy_bgo_masked[bgo];
      pattern_masking = (cfg.sig_bgo_source[bgo] == "PATTERN") ? cfg.bsy_bgo_masked[bgo] : false;
      if ((m_status = BSYMaskingBGoEnableWrite(bgo, input_masking)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing BSY masking of BGo-%d configuration", bgo); MERR(os, s);
        return (m_status);
      }
      
      if ((m_status = BSYMaskingPatBGoEnableWrite(bgo, pattern_masking)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing BSY masking of BGo-%d configuration", bgo); MERR(os, s);
        return (m_status);
      }
    }      

    if ((m_status = BSYFrontPanelLevelWrite(cfg.bsy_level)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing BSY Front-Panel level configuration"); MERR(os, s);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWriteCRQ(const AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.crq_config) return (m_status);

    char s[STRING_LENGTH];

    if ((m_status = CRQInputSelectWrite(cfg.crq_in_select)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing CRQ input select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = CRQOutputSelectWrite(CALREQ_TO_CTP, cfg.crq_out_select[0])) != SUCCESS) {
        std::sprintf(s, "ALTI: writing CRQ CTP output select configuration"); MERR(os, s);
        return (m_status);
    }

    if ((m_status = CRQOutputSelectWrite(CALREQ_TO_ALTI, cfg.crq_out_select[1])) != SUCCESS) {
        std::sprintf(s, "ALTI: writing CRQ ALTI output select configuration"); MERR(os, s);
        return (m_status);
    }

    for(u_int i=0; i<ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER; i++) {
      if ((m_status = CRQConstLevelRegWrite(i, cfg.crq_data_vme[i])) != SUCCESS) {
        std::sprintf(s, "ALTI: writing CRQ const level register configuration"); MERR(os, s);
        return (m_status);
      }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWriteMEM(const AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.mem_config) return (m_status);

    char s[STRING_LENGTH];

    // disable MEM snapshot / pattern
    if ((m_status = PRMEnableWrite(false)) != SUCCESS) {
        std::sprintf(s, "ALTI: disabling MEM snapshot / pattern"); MERR(os, s);
        return (m_status);
    }


    // write MEM pattern generation file into memory
    std::ifstream inf(cfg.mem_pg_file.c_str(), std::ifstream::in);
    if (inf) {
        if ((m_status = PRMWriteFile(m_segment, inf)) != SUCCESS) {
            std::sprintf(s, "ALTI: writing MEM pattern generation file into memory"); MERR(os, s);
            return (m_status);
        }
    }
    m_mem_pg_file = cfg.mem_pg_file;

    // write MEM pattern generation start address
    if(cfg.mem_pg_start_addr!=0) {
      if ((m_status = PRMStartAddressWrite(cfg.mem_pg_start_addr)) != SUCCESS) {
	std::sprintf(s, "ALTI: configuring MEM pattern generation start address"); MERR(os, s);
	return (m_status);
      }
    }

    // write MEM pattern generation stop address
    if(cfg.mem_pg_stop_addr!=0) {
	if ((m_status = PRMStopAddressWrite(cfg.mem_pg_stop_addr)) != SUCCESS) {
	  std::sprintf(s, "ALTI: configuring MEM pattern generation stop address"); MERR(os, s);
	  return (m_status);
	}
      }
    
    // write signal mask
    for(u_int sig =0; sig < AltiModule::SIGNAL_NUMBER; sig++) {
      bool masked = cfg.mem_snap_masked.at(AltiModule::SIGNAL(sig));
      if ((m_status = PRMSnapshotSignalMaskWrite(AltiModule::SIGNAL(sig), masked)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing pattern and snapshot signal mask"); MERR(os, s);
        return (m_status);
      }
    }
    
    // write PAT generation repeat
    if ((m_status = PRMRepeatWrite(cfg.mem_pg_repeat)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing PAT generation repeat configuration"); MERR(os, s);
      return (m_status);
    }

    // write MEM pattern generation enable
    if ((m_status = PRMEnableWrite(cfg.mem_pg_enable)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing MEM pattern generation enable"); MERR(os, s);
      return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWriteTTC(const AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.ttc_config) return (m_status);

    char s[STRING_LENGTH];

    unsigned int i;
    for (i = 0; i < TRANSMITTER_NUMBER; i++) {
        if ((m_status = ENCTransmitterEnableWrite(i, cfg.ttc_tx_enable[i])) != SUCCESS) {
            std::sprintf(s, "ALTI: enabling TTC transmitter TX[%02d]", i); MERR(os, s);
            return (m_status);
        }
	if ((m_status = ENCTransmitterDelayWrite(i, cfg.ttc_tx_delay[i])) != SUCCESS) {
	  std::sprintf(s, "ALTI: writing TTC transmitter delay TX[%02d]", i); MERR(os, s);
            return (m_status);
        }
    }

    // reset and write TTC FIFO
    if ((m_status = ENCFifoReset()) != SUCCESS) {
      std::sprintf(s, "ALTI: reseting TTC FIFOs"); MERR(os, s);
      return (m_status);
    }
    for (i = 0; i < TTC_FIFO_NUMBER; i++) {
      if(cfg.ttc_fifo_word[i] > 0) {
	if ((m_status = ENCBgoCommandPutShort((TTC_FIFO) i, cfg.ttc_fifo_word[i])) != SUCCESS) {
	  std::sprintf(s, "ALTI: writing TTC FIFOs word"); MERR(os, s);
	  return (m_status);
	}
      }
    }

    if ((m_status = ENCInhibitParamsWrite(cfg.ttc_bgo_inhibit_width, cfg.ttc_bgo_inhibit_delay)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing TTC BGO inhibit width and delay configuration"); MERR(os, s);
        return (m_status);
    }
    if ((m_status = ENCTriggerTypeDelayWrite(cfg.ttc_ttyp_delay)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing TTC TTYP word delay configuration"); MERR(os, s);
        return (m_status);
    }
    if ((m_status = ENCTriggerTypeControlWrite(cfg.ttc_ttyp_addr, cfg.ttc_ttyp_address_space, cfg.ttc_ttyp_subaddr)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing TTC TTYP control configuration"); MERR(os, s);
        return (m_status);
    }
    if ((m_status = ENCBgoCommandModeWrite(cfg.ttc_bgo_mode)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing TTC BGO mode configuration"); MERR(os, s);
        return (m_status);
    }
    if ((m_status = ENCBgoFifoRetransmitWrite(cfg.ttc_fifo_retransmit)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing TTC FIFOs retransmit configuration"); MERR(os, s);
        return (m_status);
    }
    if ((m_status = ENCL1ASourceWrite(cfg.ttc_l1a_source[0],cfg.ttc_l1a_source[1],cfg.ttc_l1a_source[2],cfg.ttc_l1a_source[3],cfg.ttc_l1a_source[4],cfg.ttc_l1a_source[5],cfg.ttc_l1a_source[6])) != SUCCESS) {
      std::sprintf(s, "ALTI: writing TTC L1A source configuration"); MERR(os, s);
      return (m_status);
    }
    if ((m_status = ENCOrbitSourceWrite(cfg.ttc_orb_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing TTC Orbit source configuration"); MERR(os, s);
      return (m_status);
    }
    for (i = 0; i < TTC_FIFO_NUMBER; i++) {
      if ((m_status = ENCBgoSourceWrite(i, cfg.ttc_bgo_source[i])) != SUCCESS) {
	std::sprintf(s, "ALTI: writing TTC BGo source configuration"); MERR(os, s);
	return (m_status);
      }
    }
    if ((m_status = ENCTtypSourceWrite(cfg.ttc_ttyp_source)) != SUCCESS) {
      std::sprintf(s, "ALTI: writing TTC TTYP source configuration"); MERR(os, s);
      return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWriteCNT(const AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.cnt_config) return (m_status);

    char s[STRING_LENGTH];

    if ((m_status = CNTEnableWrite(cfg.cnt_enable)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing counters enable"); MERR(os, s);
        return (m_status);
    }

    // write counter trigger type LUT
    if(cfg.cnt_ttyp_lut_file.empty()) {
      if((m_status = CNTTtypLutWrite(cfg.cnt_ttyp_lut)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CNT trigger type LUT into configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CNTTtypLutWrite(cfg.cnt_ttyp_lut_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CTP trigger type LUT into file \"%s\"", cfg.cnt_ttyp_lut_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWriteCTP(const AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.ctp_config) return (m_status);

    char s[STRING_LENGTH];

    // write ECR generation
    if ((m_status = ECRGenerationWrite(cfg.ctp_ecr_generation)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing ECR generation control"); MERR(os, s);
        return (m_status);
    }

    // write items LUT
    if(cfg.ctp_item_lut_file.empty()) {
      if((m_status = CTPTriggerLutWrite(cfg.ctp_item_lut)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CTP items LUT from configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CTPTriggerLutWrite(cfg.ctp_item_lut_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CTP items LUT from file \"%s\"", cfg.ctp_item_lut_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }
    
    // write TTYP LUT
    if(cfg.ctp_ttyp_lut_file.empty()) {
      if((m_status = CTPTtypLutWrite(cfg.ctp_ttyp_lut)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CTP TTYP LUT from configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CTPTtypLutWrite(cfg.ctp_ttyp_lut_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CTP TTYP LUT from file \"%s\"", cfg.ctp_ttyp_lut_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }
    
    // write random 
      for(u_int rndm = 0; rndm < ALTI::CTP_RND_THRESHOLD_NUMBER; rndm++) {
	if(cfg.ctp_rndm_seed_valid) {
	  if((m_status = CTPRandomTriggerSeedWrite(rndm, cfg.ctp_rndm_seed[rndm])) != SUCCESS) {
	    std::sprintf(s,"ALTI: writeing CTP RNDM%d seet", rndm); MERR(os,s);
	    return(m_status);
	  }
	}      
	if((m_status = CTPRandomTriggerThresholdWrite(rndm, cfg.ctp_rndm_threshold[rndm])) != SUCCESS) {
	  std::sprintf(s,"ALTI: writing CTP RNDM%d threshold", rndm); MERR(os,s);
	  return(m_status);
	}
    }
    
    // write BGRP data
    if(cfg.ctp_bgrp_file.empty()) {
      if((m_status = CTPBunchGroupDataWrite(cfg.ctp_bgrp)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CTP BGRP from configuration"); MERR(os,s);
	return(m_status);
        }
    }
    else {
        if((m_status = CTPBunchGroupDataWrite(cfg.ctp_bgrp_file)) != SUCCESS) {
	  std::sprintf(s,"ALTI: writing CTP BGRP from file \"%s\"", cfg.ctp_bgrp_file.c_str()); MERR(os,s);
	  return(m_status);
        }
    }
    
    // write BGRP BCID offset
    if((m_status = CTPBunchGroupBcidOffsetWrite(cfg. ctp_bgrp_bcid_offset)) != SUCCESS) {
        std::sprintf(s,"ALTI: writing CTP BGRP BCID offset from configuration"); MERR(os,s);
        return(m_status);
    }

    // write BGRP mask
    if(cfg.ctp_bgrp_mask_file.empty()) {
      if((m_status = CTPBunchGroupMaskWrite(cfg.ctp_bgrp_mask)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CTP BGRP mask from configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CTPBunchGroupMaskWrite(cfg.ctp_bgrp_mask_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CTP BGRP mask from file \"%s\"", cfg.ctp_bgrp_mask_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }

    // write prescale 
    if( cfg.ctp_prsc_seed_valid) {
      if(cfg.ctp_prsc_seed_file.empty()) {
	if((m_status = CTPPrescalerSeedWrite(cfg.ctp_prsc_seed)) != SUCCESS) {
	  std::sprintf(s,"ALTI: writing PRSC seed from configuration"); MERR(os,s);
	  return(m_status);
	}
      }
      else {
	if((m_status = CTPPrescalerSeedWrite(cfg.ctp_prsc_seed_file)) != SUCCESS) {
	  std::sprintf(s,"ALTI: writing PRSC seed from file \"%s\"", cfg.ctp_prsc_seed_file.c_str()); MERR(os,s);
	  return(m_status);
	}
      }
    }

    if(cfg.ctp_prsc_threshold_file.empty()) {
      if((m_status = CTPPrescalerThresholdWrite(cfg.ctp_prsc_threshold)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing PRSC threshold from configuration"); MERR(os,s);
	return(m_status);
      }
    }
    else {
      if((m_status = CTPPrescalerThresholdWrite(cfg.ctp_prsc_threshold_file)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing PRSC threshold from file \"%s\"", cfg.ctp_prsc_threshold_file.c_str()); MERR(os,s);
	return(m_status);
      }
    }

    // TAV enabled, also enable the item after the bunch-group masking
    if((m_status = CTPTavEnableWrite(cfg.ctp_tav_enable)) != SUCCESS) {
      std::sprintf(s,"ALTI: writing TAV enable from configuration"); MERR(os,s);
	return(m_status);
    }
    if((m_status = CTPBunchGroupItemEnabledWrite(cfg.ctp_tav_enable)) != SUCCESS) {
      std::sprintf(s,"ALTI: writing bunch-group item enable from configuration"); MERR(os,s);
      return(m_status);
    }
    
    // write SMPL deadtime
    if((m_status = CTPSimpleDeadtimeWidthWrite(cfg.ctp_smpl_deadtime)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing SMPL deadtime width from configuration"); MERR(os,s);
	return(m_status);
      }
      
      // write CPLX leady bucket
      for(u_int bck = 0; bck < LEAKY_BUCKET_NUMBER; bck++) {
	u_int rte = cfg.ctp_cplx_bucket[bck].rate;
	u_int lvl = cfg.ctp_cplx_bucket[bck].level;
	bool ena = cfg.ctp_cplx_bucket[bck].enable;
	if((m_status = CTPLeakyBucketConfigWrite(bck, rte, lvl)) != SUCCESS) {
	  std::sprintf(s,"ALTI: writing CPLX leaky bucket %d configuration", bck); MERR(os,s);
	  return(m_status);
	}
	if((m_status = CTPLeakyBucketEnableWrite(bck, ena)) != SUCCESS) {
	  std::sprintf(s,"ALTI: writing CPLX leaky bucket %d enable", bck); MERR(os,s);
	  return(m_status);
	}
      }
      
      // write CPLX sliding window
      u_int wdw = cfg.ctp_cplx_sliding_window.window;
      u_int num = cfg.ctp_cplx_sliding_window.number;
      bool ena  = cfg.ctp_cplx_sliding_window.enable;
      if((m_status = CTPSlidingWindowConfigWrite(wdw, num)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CPLX sliding window configuration"); MERR(os,s);
	return(m_status);
      }
      if((m_status = CTPSlidingWindowEnableWrite(ena)) != SUCCESS) {
	std::sprintf(s,"ALTI: writing CPLX sliding window enable from configuration"); MERR(os,s);
	return(m_status);
      }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CFGWritePBM(const AltiConfiguration &cfg, std::ostream &os) {

    if (!cfg.pbm_config) return (m_status);

    char s[STRING_LENGTH];

    if ((m_status = PBMWriteOffset(cfg.pbm_bcid_offset)) != SUCCESS) {
        std::sprintf(s, "ALTI: writing PBM BCID offset"); MERR(os, s);
        return (m_status);
    }

    return (m_status);
}

	   
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::CSRManufacturerIdRead(unsigned int &manuf) {

    // manufacturer identification
    if ((m_status = m_vme->ReadCRCSR(m_slot, MANUF_ID_BASE, &manuf)) != 0) {
        CERR("ALTI: reading MANUFACTURER ID from CR/CSR", "");
        return (m_status);
    }
    manuf &= MANUF_ID_MASK;
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CSRBoardIdRead(unsigned int &board) {

    // board identification
    if ((m_status = m_vme->ReadCRCSR(m_slot, BOARD_ID_BASE, &board)) != 0) {
        CERR("ALTI: reading BOARD ID from CR/CSR", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CSRRevisionIdRead(unsigned int &rev) {

    // revision identification
    if ((m_status = m_vme->ReadCRCSR(m_slot, REV_ID_BASE, &rev)) != 0) {
        CERR("ALTI: reading REVISION ID from CR/CSR", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CSRBARRead(unsigned int &bar) {

    // BAR
    if ((m_status = m_vme->ReadCRCSR(m_slot, BAR, &bar)) != 0) {
        CERR("ALTI: reading BAR from CR/CSR", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CSRADERRead(unsigned int &ader) {

    // ADER
    if ((m_status = m_vme->ReadCRCSR(m_slot, ADER_BASE, &ader)) != 0) {
        CERR("ALTI: reading ADER from CR/CSR", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CSRADERWrite(const unsigned int ader) {
    if (m_ader == ader) { // nothing to do
        m_status = SUCCESS;
        return (m_status);
    }

    if (m_net) {
        // delete I2C network
        delete m_net;
    }

    if (m_i2c) {
        // delete I2C
        delete m_i2c;
    }

    if (m_ds1wm) {
        // delete DS1WM
        delete m_ds1wm;
    }

    if (m_alti) {
        // delete low-level ALTI
        delete m_alti;
    }

    // ADER
    if ((m_status = m_vme->WriteCRCSR(m_slot, ADER_BASE, ader)) != 0) {
        CERR("ALTI: writing ADER to CR/CSR","");
        return (m_status);
    }
    m_ader = ader;

    // create low-level ALTI
    m_alti = new ALTI(m_vme, m_ader);
    if ((*m_alti)()) {
        CERR("ALTI: opening ALTI at vme = 0x%08x, size = 0x%08x, type = \"%s\"", m_ader, ALTI::size(), ALTI::code().c_str());
        m_status = FAILURE;
        return (m_status);
    }
    std::printf("ALTI: INFO - ALTI opened at vme = 0x%08x, size = 0x%08x, type = \"%s\"\n", m_ader, ALTI::size(), ALTI::code().c_str());

    // fetch VME master mapping
    m_vmm = m_alti->getVmeMasterMap();

    // create DS1WM, initialize and read serial number
    m_ds1wm = new DS1WM(DS1WM::ALTI, m_vmm);

    // create and initialize I2C network
    if ((m_status = I2CInit()) != SUCCESS) {
        CERR("ALTI: initializing I2C network", "");
        m_status = FAILURE;
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CSRBoardIdRead(int slot, unsigned int &board) {
    // open VME
    RCD::VME *vme = RCD::VME::Open();

    // board identification
    int status(SUCCESS);
    if ((status = vme->ReadCRCSR(slot, BOARD_ID_BASE, &board)) != 0) {
        CERR("ALTI: reading BOARD ID from CR/CSR, slot %d", slot);
        return (status);
    }

    // close VME
    RCD::VME::Close();

    return (SUCCESS);
}

//------------------------------------------------------------------------------

int AltiModule::CSRBARRead(int slot, unsigned int &bar) {
    // open VME
    RCD::VME *vme = RCD::VME::Open();

    // BAR
    int status(SUCCESS);
    if ((status = vme->ReadCRCSR(slot, BAR, &bar)) != SUCCESS) {
        CERR("ALTI: reading BAR from CR/CSR, slot %d", slot);
        return (status);
    }

    // close VME
    RCD::VME::Close();

    return (SUCCESS);
}

//------------------------------------------------------------------------------

int AltiModule::CSRADERRead(int slot, unsigned int &ader) {
    // open VME
    RCD::VME *vme = RCD::VME::Open();

    // ADER
    int status(SUCCESS);
    if ((status = vme->ReadCRCSR(slot, ADER_BASE, &ader)) != SUCCESS) {
        CERR("ALTI: reading ADER from CR/CSR, slot %d", slot);
        return (status);
    }

    // close VME
    RCD::VME::Close();

    return (SUCCESS);
}

//------------------------------------------------------------------------------

int AltiModule::CSRADERWrite(int slot, const unsigned int ader) {
    // open VME
    RCD::VME *vme = RCD::VME::Open();

    // ADER
    int status(SUCCESS);
    if ((status = vme->WriteCRCSR(slot, ADER_BASE, ader)) != 0) {
        CERR("ALTI: writing ADER 0x%08x to CR/CSR", ader);
        return (status);
    }

    // close VME
    RCD::VME::Close();

    return (SUCCESS);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchConfigRead(const SIGNAL signal, const SIGNAL_DESTINATION dst, SIGNAL_SOURCE &src) { // 1 signal, 1 output

    I2CNetwork::I2CNode &node = m_SignalSwitchConfig[signal];
    // READ
    unsigned int readout;
    I2CDataRead(node, readout);

    SWITCH_CONFIG switch_config_bs;
    std::vector<unsigned int> tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(readout));
    switch_config_bs = (RCD::BitSet) tmp;

    SIGNAL_SOURCE tmp_src = (SIGNAL_SOURCE) switch_config_bs.Output(SWX_MAP_DST[signal][dst]);
    src = SWX_MAP_SRC_INV[signal][tmp_src];

    m_status = SUCCESS;
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchConfigRead(const SIGNAL signal, std::vector<SIGNAL_SOURCE> &src) { // 1 signal, all outputs
    src.clear();

    SIGNAL_SOURCE tmp_src;
    SIGNAL_DESTINATION dst;

    unsigned int i;
    for (i = 0; i < SIGNAL_DESTINATION_NUMBER; i++) {
        dst = (SIGNAL_DESTINATION) i;
        if ((m_status = SIGSwitchConfigRead(signal, dst, tmp_src)) != SUCCESS) {
            CERR("ALTI: reading switch source for signal \"%s\", destination \"%s\"", SIGNAL_NAME[signal].c_str(), SIGNAL_DESTINATION_NAME[dst].c_str());
            return (m_status);
        }
        src.push_back(tmp_src);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchConfigWrite(const SIGNAL signal, const SIGNAL_DESTINATION dst, const SIGNAL_SOURCE src) { // 1 signal, 1 output

    I2CNetwork::I2CNode &node = m_SignalSwitchConfig[signal];
    // READ
    unsigned int readout;
    I2CDataRead(node, readout);

    SWITCH_CONFIG switch_config_bs;
    std::vector<unsigned int> tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(readout));
    switch_config_bs = (RCD::BitSet) tmp;

    // MODIFY
    switch_config_bs.Output(SWX_MAP_DST[signal][dst], SWX_MAP_SRC[signal][src]);

    // WRITE
    m_net->write(node, switch_config_bs.number()[0]);

    m_status = SUCCESS;
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchConfigWrite(const SIGNAL_DESTINATION dst, const SIGNAL_SOURCE src) { // all signals, 1 output
    SIGNAL signal;

    unsigned int i;
    for (i = 0; i < SIGNAL_NUMBER_ROUTED; i++) {
        signal = (SIGNAL) i;
        if ((m_status = SIGSwitchConfigWrite(signal, dst, src)) != SUCCESS) {
            CERR("ALTI: writing switch source \"%s\" for signal \"%s\", destination \"%s\"", SIGNAL_SOURCE_NAME[src].c_str(), SIGNAL_NAME[signal].c_str(), SIGNAL_DESTINATION_NAME[dst].c_str());
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchConfigWrite(const SIGNAL signal, const SIGNAL_SOURCE src) { // 1 signal, all outputs

    I2CNetwork::I2CNode &node = m_SignalSwitchConfig[signal];
    // READ
    unsigned int readout;
    I2CDataRead(node, readout);

    SWITCH_CONFIG switch_config_bs;
    std::vector<unsigned int> tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(readout));
    switch_config_bs = (RCD::BitSet) tmp;

    // MODIFY
    unsigned int i;
    SIGNAL_DESTINATION dst;
    for (i = 0; i < SIGNAL_DESTINATION_NUMBER; i++) {
        dst = (SIGNAL_DESTINATION) i;
        switch_config_bs.Output(SWX_MAP_DST[signal][dst], SWX_MAP_SRC[signal][src]);
    }

    // WRITE
    m_net->write(node, switch_config_bs.number()[0]);

    m_status = SUCCESS;

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchConfigWrite(const SIGNAL_SOURCE src) { // all signals, all outputs
    SIGNAL signal;
    unsigned i;
    for (i = 0; i < SIGNAL_NUMBER_ROUTED; i++) {
        signal = (SIGNAL) i;
        if ((m_status = SIGSwitchConfigWrite(signal, src)) != SUCCESS) {
            CERR("ALTI: writing switch source \"%s\" for signal \"%s\", all destinations", SIGNAL_SOURCE_NAME[src].c_str(), SIGNAL_NAME[signal].c_str());
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchConfigWrite(const ALTI_MODE mode) {
    switch (mode) {
        case MASTER:
            return SIGSwitchMasterConfigurationWrite();
            break;
        case CTP_SLAVE:
            return SIGSwitchCtpSlaveConfigurationWrite();
            break;
        case ALTI_SLAVE:
            return SIGSwitchAltiSlaveConfigurationWrite();
            break;
        case LEMO_SLAVE:
            return SIGSwitchLemoSlaveConfigurationWrite();
            break;
        default:
            break;
    }

    return (SUCCESS);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchPowerDownEnableRead(const SIGNAL signal, const SIGNAL_DESTINATION dst, bool &pwdn) { // 1 signal, 1 output

    I2CNetwork::I2CNode &node = m_SignalSwitchCtrl[signal];
    // READ
    unsigned int readout;
    I2CDataRead(node, readout);

    SWITCH_CTRL switch_ctrl_bs;
    std::vector<unsigned int> tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(readout));
    switch_ctrl_bs = (RCD::BitSet) tmp;

    std::string s = switch_ctrl_bs.PowerDown(SWX_MAP_DST[signal][dst]);
    if (s == "Enabled") {
       pwdn = true;
    }
    else if (s == "Disabled") {
       pwdn = false;
    }
    else {
        CERR("ALTI: unknown value \"%s\" for power-down enable of signal \"%s\", destination \"%s\"", s.c_str(), AltiModule::SIGNAL_NAME[signal].c_str(), AltiModule::SIGNAL_DESTINATION_NAME[dst].c_str());
        return (FAILURE);
    }

    m_status = SUCCESS;
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchPowerDownEnableRead(const SIGNAL signal, std::vector<bool> &pwdn) { // 1 signal, all outputs
    pwdn.clear();

    bool tmp_pwdn;
    SIGNAL_DESTINATION dst;

    unsigned int i;
    for (i = 0; i < SIGNAL_DESTINATION_NUMBER; i++) {
        dst = (SIGNAL_DESTINATION) i;
        if ((m_status = SIGSwitchPowerDownEnableRead(signal, dst, tmp_pwdn)) != SUCCESS) {
            CERR("ALTI: reading power-down mode for signal \"%s\", destination \"%s\"", SIGNAL_NAME[signal].c_str(), SIGNAL_DESTINATION_NAME[dst].c_str());
            return (m_status);
        }
        pwdn.push_back(tmp_pwdn);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchPowerDownEnableWrite(const SIGNAL signal, const SIGNAL_DESTINATION dst, const bool pwdn) { // 1 signal, 1 output

    I2CNetwork::I2CNode &node = m_SignalSwitchCtrl[signal];
    // READ
    unsigned int readout;
    I2CDataRead(node, readout);

    SWITCH_CTRL switch_ctrl_bs;
    std::vector<unsigned int> tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(readout));
    switch_ctrl_bs = (RCD::BitSet) tmp;

    // MODIFY
    switch_ctrl_bs.PowerDown(SWX_MAP_DST[signal][dst], pwdn ? "Enabled" : "Disabled");

    // WRITE
    m_net->write(node, switch_ctrl_bs.number()[0]);

    m_status = SUCCESS;
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchPowerDownEnableWrite(const SIGNAL_DESTINATION dst, const bool pwdn) { // all signals, 1 output
    SIGNAL signal;

    unsigned int i;
    for (i = 0; i < SIGNAL_NUMBER_ROUTED; i++) {
        signal = (SIGNAL) i;
        if ((m_status = SIGSwitchPowerDownEnableWrite(signal, dst, pwdn)) != SUCCESS) {
            CERR("ALTI: %s power-down mode for signal \"%s\", destination \"%s\"", pwdn ? "enabling" : "disabling", SIGNAL_NAME[signal].c_str(), SIGNAL_DESTINATION_NAME[dst].c_str());
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchPowerDownEnableWrite(const SIGNAL signal, const bool pwdn) { // 1 signal, all outputs

    I2CNetwork::I2CNode &node = m_SignalSwitchCtrl[signal];
    // READ
    unsigned int readout;
    I2CDataRead(node, readout);

    SWITCH_CTRL switch_ctrl_bs;
    std::vector<unsigned int> tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(readout));
    switch_ctrl_bs = (RCD::BitSet) tmp;

    // MODIFY
    unsigned int i;
    SIGNAL_DESTINATION dst;
    for (i = 0; i < SIGNAL_DESTINATION_NUMBER; i++) {
        dst = (SIGNAL_DESTINATION) i;
        switch_ctrl_bs.PowerDown(SWX_MAP_DST[signal][dst], pwdn ? "Enabled" : "Disabled");
    }

    // WRITE
    m_net->write(node, switch_ctrl_bs.number()[0]);

    m_status = SUCCESS;

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchPowerDownEnableWrite(const bool pwdn) { // all signals, all outputs
    SIGNAL signal;
    unsigned i;
    for (i = 0; i < SIGNAL_NUMBER_ROUTED; i++) {
        signal = (SIGNAL) i;
        if ((m_status = SIGSwitchPowerDownEnableWrite(signal, pwdn)) != SUCCESS) {
            CERR("ALTI: %s power-down mode for signal \"%s\", all destinations", pwdn ? "enabling" : "disabling", SIGNAL_NAME[signal].c_str());
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchMasterConfigurationWrite() {
  return SIGSwitchConfigWrite(FROM_FPGA); // everything from FPGA
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchCtpSlaveConfigurationWrite() {
    return SIGSwitchConfigWrite(CTP_IN); // everything from CTP
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchAltiSlaveConfigurationWrite() {
    return SIGSwitchConfigWrite(ALTI_IN); // everything from ALTI
}

//------------------------------------------------------------------------------

int AltiModule::SIGSwitchLemoSlaveConfigurationWrite() {
    return SIGSwitchConfigWrite(NIM_IN); // everything from NIM
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


int AltiModule::SIGEqualizersEnableRead(const SIGNAL_SOURCE src, bool &enabled) {
 if (src >= SIGNAL_SOURCE_NUMBER - 3) {
    CERR("ALTI: wrong signal source chosen", "");
    m_status = WRONGPAR;
    return (m_status);
  }
  
 ALTI_IO_EQUALIZERCONTROL_BITSTRING bs;
 // READ
 if ((m_status = m_alti->IO_EqualizerControl_Read(bs)) != 0) {
   CERR("ALTI: reading equalizer control", "");
   return (m_status);
 }

 if(src==CTP_IN) enabled = bs.EnableCtpEqualizer();
 else if(src==ALTI_IN) enabled = bs.EnableAltiEqualizer();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGEqualizersEnableWrite(const SIGNAL_SOURCE src, const bool enabled) {
 if (src >= SIGNAL_SOURCE_NUMBER - 3) {
    CERR("ALTI: wrong signal source chosen", "");
    m_status = WRONGPAR;
    return (m_status);
  }
  
 ALTI_IO_EQUALIZERCONTROL_BITSTRING bs;

 // READ 
 if ((m_status = m_alti->IO_EqualizerControl_Read(bs)) != 0) {
   CERR("ALTI: reading equalizer control", "");
   return (m_status);
 }

 // MODIFY
 if(src==CTP_IN)  bs.EnableCtpEqualizer(enabled); 
 else if(src==ALTI_IN)  bs.EnableAltiEqualizer(enabled); 

 // WRITE
 if ((m_status = m_alti->IO_EqualizerControl_Write(bs)) != 0) {
   CERR("ALTI: writing equalizer control", "");
   return (m_status);
 }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGEqualizersConfigRead(const SIGNAL_SOURCE src, EQUALIZER_VOLTAGE vlt, unsigned int &v) {
  if (src >= SIGNAL_SOURCE_NUMBER - 3) {
    CERR("ALTI: wrong signal source chosen", "");
    m_status = WRONGPAR;
    return (m_status);
  }

  // READ
  unsigned int readout;
  if ((m_status = m_net->read(m_EqualizerConfig[src][vlt], readout)) != 0) {
    CERR("ALTI: reading %s %s from AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[vlt].c_str());
    return (m_status);
  }

  EQUALIZER_DAC eqz_dac_bs;
  std::vector<unsigned int> tmp;
  tmp.clear();
  tmp.push_back(static_cast<unsigned int>(readout));
  eqz_dac_bs = (RCD::BitSet) tmp;
  // DAC 2B readout pattern
  v = eqz_dac_bs.Data();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGEqualizersConfigWrite(const SIGNAL_SOURCE src, const EQUALIZER_VOLTAGE vlt, const unsigned int v) {
  if (src >= SIGNAL_SOURCE_NUMBER - 3) {
    CERR("ALTI: wrong signal source chosen", "");
    m_status = WRONGPAR;
    return (m_status);
  }

  EQUALIZER_DAC eqz_dac_bs;
  std::vector<unsigned int> tmp;
  tmp.clear();
  tmp.push_back(static_cast<unsigned int>(0x00000000));
  eqz_dac_bs = (RCD::BitSet) tmp;
  // DAC 2B write pattern: CLR (active low) bit set to 1 for normal operation, LDAC (active low) set to 0 for latching, no PD modes used
  eqz_dac_bs.Data(v);
  eqz_dac_bs.Clear("Disabled");
  eqz_dac_bs.Load("Enabled");

  // WRITE
  if ((m_status = m_net->write(m_EqualizerConfig[src][vlt], eqz_dac_bs.number()[0])) != 0) {
    CERR("ALTI: writing %s %s to AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[vlt].c_str());
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGEqualizersConfigRead(const SIGNAL_SOURCE src, unsigned int &vpeak, unsigned int &vpole, unsigned int &vgain, unsigned int &voffset) {
  if (src >= SIGNAL_SOURCE_NUMBER - 3) {
    CERR("ALTI: wrong signal source chosen", "");
    m_status = WRONGPAR;
    return (m_status);
  }
  
  if ((m_status = SIGEqualizersConfigRead(src, V_PEAK, vpeak)) != 0) {
    CERR("ALTI: reading %s %s from AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[V_PEAK].c_str());
    return (m_status);
  }
  if ((m_status = SIGEqualizersConfigRead(src, V_POLE, vpole)) != 0) {
    CERR("ALTI: reading %s %s from AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[V_POLE].c_str());
    return (m_status);
  }
  if ((m_status = SIGEqualizersConfigRead(src, V_GAIN, vgain)) != 0) {
    CERR("ALTI: reading %s %s from AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[V_GAIN].c_str());
    return (m_status);
  }
  if ((m_status = SIGEqualizersConfigRead(src, V_OFFSET, voffset)) != 0) {
    CERR("ALTI: reading %s %s from AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[V_OFFSET].c_str());
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGEqualizersConfigWrite(const SIGNAL_SOURCE src, const unsigned int vpeak, const unsigned int vpole, const unsigned int vgain, const unsigned int voffset) {
  if (src >= SIGNAL_SOURCE_NUMBER - 3) {
    CERR("ALTI: wrong signal source chosen", "");
    m_status = WRONGPAR;
    return (m_status);
  }
  
  if ((m_status = SIGEqualizersConfigWrite(src, V_PEAK, vpeak)) != 0) {
    CERR("ALTI: writing %s %s to AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[V_PEAK].c_str());
    return (m_status);
  }
  if ((m_status = SIGEqualizersConfigWrite(src, V_POLE, vpole)) != 0) {
    CERR("ALTI: writing %s %s to AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[V_POLE].c_str());
    return (m_status);
  }
  if ((m_status = SIGEqualizersConfigWrite(src, V_GAIN, vgain)) != 0) {
    CERR("ALTI: writing %s %s to AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[V_GAIN].c_str());
    return (m_status);
  }
  if ((m_status = SIGEqualizersConfigWrite(src, V_OFFSET, voffset)) != 0) {
    CERR("ALTI: writing %s %s to AD5305", SIGNAL_SOURCE_NAME[src].c_str(), EQUALIZER_VOLTAGE_NAME[V_OFFSET].c_str());
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGEqualizersConfigWrite(const SIGNAL_SOURCE src, const bool enable, const EQUALIZER_CONFIG config) {
  if (src >= SIGNAL_SOURCE_NUMBER - 3) {
    CERR("ALTI: wrong signal source chosen", "");
    m_status = WRONGPAR;
    return (m_status);
  }

  if (config == CUSTOM_CONFIG) {
    CERR("ALTI: wrong input parameter \"%s\" for writing \"%s\" equalizer configuration", EQUALIZER_CONFIG_NAME[config].c_str(), SIGNAL_SOURCE_NAME[src].c_str());
    m_status = WRONGPAR;
    return (m_status);
  }

  if ((m_status = SIGEqualizersEnableWrite(src, enable)) != 0) {
      CERR("ALTI: writing \"%s\" equalizer enable to \"s\" cable", enable, SIGNAL_SOURCE_NAME[src].c_str());
    return (m_status);
  }

  if ((m_status = SIGEqualizersConfigWrite(src, EQUALIZER_CONFIG_VOLTAGE[config][V_PEAK], EQUALIZER_CONFIG_VOLTAGE[config][V_POLE], EQUALIZER_CONFIG_VOLTAGE[config][V_GAIN], EQUALIZER_CONFIG_VOLTAGE[config][V_OFFSET])) != 0) {
    CERR("ALTI: writing \"%s\" equalizer configuration to \"s\" cable", EQUALIZER_CONFIG_NAME[config].c_str(), SIGNAL_SOURCE_NAME[src].c_str());
    return (m_status);
  }
 
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGEqualizersConfigRead(const SIGNAL_SOURCE src, bool &enable, EQUALIZER_CONFIG &config) {
  if (src >= SIGNAL_SOURCE_NUMBER - 3) {
    CERR("ALTI: wrong signal source chosen", "");
    m_status = WRONGPAR;
    return (m_status);
  }

  if ((m_status = SIGEqualizersEnableRead(src, enable)) != 0) {
    CERR("ALTI: reading \"%s\" equalizer enable to \"s\" cable", enable, SIGNAL_SOURCE_NAME[src].c_str());
    return (m_status);
  }
  
  unsigned int voltages[EQUALIZER_VOLTAGE_NUMBER];
  if ((m_status = SIGEqualizersConfigRead(src, voltages[0], voltages[1], voltages[2], voltages[3])) != 0) {
    CERR("ALTI: reading \"%s\" equalizer configuration", SIGNAL_SOURCE_NAME[src].c_str());
    return (m_status);
  }
  
  if ((voltages[0] == EQUALIZER_CONFIG_VOLTAGE[SHORT_CABLE][0]) 
      && (voltages[1] == EQUALIZER_CONFIG_VOLTAGE[SHORT_CABLE][1]) 
      && (voltages[2] == EQUALIZER_CONFIG_VOLTAGE[SHORT_CABLE][2]) 
      && (voltages[3] == EQUALIZER_CONFIG_VOLTAGE[SHORT_CABLE][3])) {
    config = SHORT_CABLE;
  }
  else if ((voltages[0] == EQUALIZER_CONFIG_VOLTAGE[LONG_CABLE][0]) 
	   && (voltages[1] == EQUALIZER_CONFIG_VOLTAGE[LONG_CABLE][1]) 
	   && (voltages[2] == EQUALIZER_CONFIG_VOLTAGE[LONG_CABLE][2]) 
	   && (voltages[3] == EQUALIZER_CONFIG_VOLTAGE[LONG_CABLE][3])) {
    config = LONG_CABLE;
  }
  else {
    config = CUSTOM_CONFIG;
  }

  return (m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::SIGInputSyncHistogramsRead(const SYNCHRONIZER_INPUT_SIGNAL asyncIn, std::vector<unsigned int> &edge_detected){//&countPosByPhase, std::vector<unsigned int> &countNegByPhase) {
    ALTI_IO_CONTROLSTATUSREGISTER_BITSTRING bs;

    if ((m_status = m_alti->IO_ControlStatusRegister_Read(asyncIn, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
    }
    unsigned int i;
    edge_detected.clear();
    for(i = 0; i < 8; i++){
      edge_detected.push_back(bs.EdgeDetected(i));
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGInputSyncHistogramsReset() {
    ALTI_IO_CONTROLSTATUSREGISTER_BITSTRING bs;

    SYNCHRONIZER_INPUT_SIGNAL asyncIn;
    unsigned int i;
    for (i = 0; i < SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
        asyncIn = (SYNCHRONIZER_INPUT_SIGNAL) i;
        // READ
        if ((m_status = m_alti->IO_ControlStatusRegister_Read(asyncIn, bs)) != 0) {
            CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
            return (m_status);
        }
        // WRITE: reset histogram
        bs.StatusClear(1);
        if ((m_status = m_alti->IO_ControlStatusRegister_Write(asyncIn, bs)) != 0) {
            CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
            return (m_status);
        }
        // WRITE: re-enable histogram calculation
        bs.StatusClear(0);
        if ((m_status = m_alti->IO_ControlStatusRegister_Write(asyncIn, bs)) != 0) {
            CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGInputSyncEnableRead(const SYNCHRONIZER_INPUT_SIGNAL asyncIn, unsigned int &phase) {
    ALTI_IO_CONTROLSTATUSREGISTER_BITSTRING bs;

    // READ
    if ((m_status = m_alti->IO_ControlStatusRegister_Read(asyncIn, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
    }
   
    phase = bs.PhaseSelect();
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGInputSyncEnableWrite(const SYNCHRONIZER_INPUT_SIGNAL asyncIn, const unsigned int phase) {
    if (phase < 0 || phase > 7) {
        CERR("ALTI: wrong phase index \"%d\", allowed range is [0, 7]", phase);
        m_status = -1;
        return (m_status);
    }

    ALTI_IO_CONTROLSTATUSREGISTER_BITSTRING bs;

    // READ
    if ((m_status = m_alti->IO_ControlStatusRegister_Read(asyncIn, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
    }
    // WRITE: reset histogram
    bs.StatusClear(1);
    if ((m_status = m_alti->IO_ControlStatusRegister_Write(asyncIn, bs)) != 0) {
        CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
    }
    // MODIFY
    bs.PhaseSelect(phase);
    // WRITE: re-enable histogram calculation
    bs.StatusClear(0);
    if ((m_status = m_alti->IO_ControlStatusRegister_Write(asyncIn, bs)) != 0) {
        CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGInputSyncEnableRead(std::vector<unsigned int> &phase) { // all
    //enabled.clear();
    phase.clear();

    unsigned int i;
    SYNCHRONIZER_INPUT_SIGNAL asyncIn;
    //bool rd_enabled;
    unsigned int rd_phase;
    for (i = 0; i < SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
        asyncIn = (SYNCHRONIZER_INPUT_SIGNAL) i;
        //if ((m_status = SIGInputSyncEnableRead(asyncIn, rd_enabled, rd_phase)) != 0) {
        if ((m_status = SIGInputSyncEnableRead(asyncIn, rd_phase)) != 0) {
            CERR("ALTI: reading synchronization enable and phase for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
            return (m_status);
        }
        //enabled.push_back(rd_enabled);
        phase.push_back(rd_phase);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGInputSyncEnableWrite( const std::vector<unsigned int> &phase) { // all
    unsigned int i;
    SYNCHRONIZER_INPUT_SIGNAL asyncIn;
    for (i = 0; i < SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
        asyncIn = (SYNCHRONIZER_INPUT_SIGNAL) i;
        if ((m_status = SIGInputSyncEnableWrite(asyncIn, phase[i])) != 0) {
            CERR("ALTI: writing synchronization enable and phase for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGInputSyncEnableWrite(const unsigned int phase) { // all
    unsigned int i;
    SYNCHRONIZER_INPUT_SIGNAL asyncIn;
    for (i = 0; i < SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
        asyncIn = (SYNCHRONIZER_INPUT_SIGNAL) i;
        if ((m_status = SIGInputSyncEnableWrite(asyncIn, phase)) != 0) {
            CERR("ALTI: writing synchronization enable and phase for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSyncShapeSwitchRead(const SYNCHRONIZER_INPUT_SIGNAL asyncIn, SYNC_SHAPE &s) {
    ALTI_IO_CONTROLSTATUSREGISTER_BITSTRING bs;

    // READ
    if ((m_status = m_alti->IO_ControlStatusRegister_Read(asyncIn, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
    }

    int rd_val = bs.SyncOrShape();

    if (rd_val == 0){
        s = IN_SYNCHRONIZED;
      }
    else if(rd_val == 1) {
        s = IN_SHAPED_ONE;
      }
    else {
        s = IN_SHAPED_TWO;
      }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSyncShapeSwitchRead(std::vector<unsigned int> &status) { // all
    status.clear();

    unsigned int i;
    SYNCHRONIZER_INPUT_SIGNAL asyncIn;
    SYNC_SHAPE rd_val;
    for (i = 0; i < SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
        asyncIn = (SYNCHRONIZER_INPUT_SIGNAL) i;
        if ((m_status = SIGSyncShapeSwitchRead(asyncIn, rd_val)) != 0) {
            CERR("ALTI: reading shape enable for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
            return (m_status);
        }
        
        status.push_back(rd_val);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSyncShapeSwitchWrite(const SYNCHRONIZER_INPUT_SIGNAL asyncIn, const unsigned int s) {
    ALTI_IO_CONTROLSTATUSREGISTER_BITSTRING bs;

    // READ
    if ((m_status = m_alti->IO_ControlStatusRegister_Read(asyncIn, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
    }

      bs.SyncOrShape(s);
    if ((m_status = m_alti->IO_ControlStatusRegister_Write(asyncIn, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGSyncShapeSwitchWrite(const std::vector<unsigned int> s) {
    ALTI_IO_CONTROLSTATUSREGISTER_BITSTRING bs;

    unsigned int i;
    SYNCHRONIZER_INPUT_SIGNAL asyncIn;
    for (i = 0; i < SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
      asyncIn = (SYNCHRONIZER_INPUT_SIGNAL) i;
      // READ
      if ((m_status = m_alti->IO_ControlStatusRegister_Read(asyncIn, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
      }
    
      // MODIFY
      bs.SyncOrShape(s[i]);
      // WRITE
      if ((m_status = m_alti->IO_ControlStatusRegister_Write(asyncIn, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
        return (m_status);
      }
    } 
    return (m_status);
}


//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectL1aRead(std::string& output) {
  
  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  output = bs.L1a();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectL1aWrite(const std::string output) {

  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  // MODIFY
  bs.L1a(output);
  // WRITE
  if ((m_status = m_alti->CTL_OutputSelect_Write(bs)) != 0){
    CERR("ALTI: writing CTL output select Register","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectOrbitRead(std::string& output) {

  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  output = bs.Orbit();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectOrbitWrite(const std::string output) {
  
  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  // MODIFY
  bs.Orbit(output);
  // WRITE
  if ((m_status = m_alti->CTL_OutputSelect_Write(bs)) != 0){
    CERR("ALTI: writing CTL output select Register","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectBgoRead(const int bgo, std::string& output) {

  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  output = bs.Bgo(bgo);
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectBgoWrite(const int bgo, const std::string output) {

  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  // MODIFY
  bs.Bgo(bgo, output);
  // WRITE
  if ((m_status = m_alti->CTL_OutputSelect_Write(bs)) != 0){
    CERR("ALTI: writing CTL output select Register","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectTestTriggerRead(const int ttr, std::string& output) {

  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  output = bs.TestTrigger(ttr);
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectTestTriggerWrite(const int ttr, const std::string output) {

  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  // MODIFY
  bs.TestTrigger(ttr, output);
  // WRITE
  if ((m_status = m_alti->CTL_OutputSelect_Write(bs)) != 0){
    CERR("ALTI: writing CTL output select Register","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectTriggerTypeRead(std::string& output) {
  
  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  output = bs.TriggerType();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGOutputSelectTriggerTypeWrite(const std::string output) {

  ALTI_CTL_OUTPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_OutputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  // MODIFY
  bs.TriggerType(output);
  // WRITE
  if ((m_status = m_alti->CTL_OutputSelect_Write(bs)) != 0){
    CERR("ALTI: writing CTL output select Register","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGInputSelectOrbitRead(std::string& input) {

  ALTI_CTL_INPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_InputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL output select Register","");
    return(m_status);
  }
  input = bs.Orbit();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGInputSelectOrbitWrite(const std::string input) {
  
  ALTI_CTL_INPUTSELECT_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_InputSelect_Read(bs)) != 0){
    CERR("ALTI: reading CTL input select Register","");
    return(m_status);
  }
  // MODIFY
  bs.Orbit(input);
  // WRITE
  if ((m_status = m_alti->CTL_InputSelect_Write(bs)) != 0){
    CERR("ALTI: writing CTL input select Register","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGMuxBGOOutputLEMORead(bool &bgo2enabled, bool &bgo3enabled) { // both
    //ALTI_BGO_CONFIG_BITSTRING bs;
    ALTI_IO_BGOFRONTPANELSELECTION_BITSTRING bs;
  
  // READ
    if ((m_status = m_alti->IO_BgoFrontPanelSelection_Read(bs)) != SUCCESS){
      CERR("ALTI: reading BGo FP Selection Register","");
      return(m_status);
    }
    std::string s;
    s = bs.LemoOutEnableBgo0Bgo2();
    if (s == "BGO2") {
        bgo2enabled = true;
    }
    else if (s == "BGO0") {
        bgo2enabled = false;
    }
    else {
        CERR("ALTI: unknown value \"%s\" for BGO2/0 front panel output enable", s.c_str());
        return (-1);
    }
    s = bs.LemoOutEnableBgo1Bgo3();
    if (s == "BGO3") {
        bgo3enabled = true;
    }
    else if (s == "BGO1") {
        bgo3enabled = false;
    }
    else {
        CERR("ALTI: unknown value \"%s\" for BGO3/1 front panel output enable", s.c_str());
        return (-1);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGMuxBGOOutputLEMOWrite(const bool bgo2enable, const bool bgo3enable) { // both
  
 // ALTI_BGO_CONFIG_BITSTRING bs;
    ALTI_IO_BGOFRONTPANELSELECTION_BITSTRING bs;

    // READ
    if ((m_status = m_alti->IO_BgoFrontPanelSelection_Read(bs)) != 0){
      CERR("ALTI: reading BGo FP Selection Register","");
      return(m_status);
    }
    // MODIFY
    bs.LemoOutEnableBgo0Bgo2(bgo2enable ? "BGO2" : "BGO0");
    bs.LemoOutEnableBgo1Bgo3(bgo3enable ? "BGO3" : "BGO1");
    // WRITE
    if ((m_status = m_alti->IO_BgoFrontPanelSelection_Write(bs)) != 0){
      CERR("ALTI: writing BGo FP Selection Register","");
      return(m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::SIGTurnCounterOrbitSourceRead(std::string &source) {

  ALTI_CTL_TURNSIGNAL_BITSTRING bs;
  if ((m_status = m_alti->CTL_TurnSignal_Read(bs)) != 0){
    CERR("ALTI: reading turn counter","");
    return(m_status);
  }

  source = bs.OrbitSource();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGTurnCounterOrbitSourceWrite(const std::string source) {

  ALTI_CTL_TURNSIGNAL_BITSTRING bs;
  // READ
  if ((m_status = m_alti->CTL_TurnSignal_Read(bs)) != 0){
    CERR("ALTI: reading turn counter","");
    return(m_status);
  }
  // MODIFY
  bs.OrbitSource(source);
  // WRITE
  if ((m_status = m_alti->CTL_TurnSignal_Write(bs)) != 0){
    CERR("ALTI: writing turn counter","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------


int AltiModule::SIGTurnCounterMaskRead(unsigned int &mask) {

  if ((m_status = m_alti->CTL_TurnSignalMask_Read(mask)) != 0){
    CERR("ALTI: reading turn counter mask","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGTurnCounterMaskWrite(const unsigned int mask) {
  
  if ((m_status = m_alti->CTL_TurnSignalMask_Write(mask)) != 0){
    CERR("ALTI: writing turn counter mask","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGTurnCounterReset() {

  ALTI_CTL_TURNSIGNAL_BITSTRING bs;
  if ((m_status = m_alti->CTL_TurnSignal_Read(bs)) != 0){
    CERR("ALTI: reading turn counter","");
    return(m_status);
  }
  bs.CounterReset(true);
  if ((m_status = m_alti->CTL_TurnSignal_Write(bs)) != 0){
    CERR("ALTI: writing turn counter","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGTurnCounterFreqRead(unsigned int &freq) {

  ALTI_CTL_TURNSIGNAL_BITSTRING bs;
  if ((m_status = m_alti->CTL_TurnSignal_Read(bs)) != 0){
    CERR("ALTI: reading turn counter","");
    return(m_status);
  }
  freq = bs.MaximumTurn();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGTurnCounterFreqWrite(const unsigned int freq) {

  ALTI_CTL_TURNSIGNAL_BITSTRING bs;
  if ((m_status = m_alti->CTL_TurnSignal_Read(bs)) != 0){
    CERR("ALTI: reading turn counter","");
    return(m_status);
  }
  bs.MaximumTurn(freq);
  if ((m_status = m_alti->CTL_TurnSignal_Write(bs)) != 0){
    CERR("ALTI: writing turn counter","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGTurnCounterValueRead(unsigned int &cnt) {

  ALTI_CTL_TURNSIGNAL_BITSTRING bs;
  if ((m_status = m_alti->CTL_TurnSignal_Read(bs)) != 0){
    CERR("ALTI: reading turn counter","");
    return(m_status);
  }
  cnt = bs.CounterValue();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGBGo2L1aDelayRead(unsigned int &delay) {

  ALTI_CTL_L1ACALIBRATIONSIGNAL_BITSTRING bs;
  if ((m_status = m_alti->CTL_L1aCalibrationSignal_Read(bs)) != 0){
    CERR("ALTI: reading L1A calibration signal","");
    return(m_status);
  }
  delay = bs.L1aDelay();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGBGo2L1aDelayWrite(const unsigned int delay) {

  ALTI_CTL_L1ACALIBRATIONSIGNAL_BITSTRING bs;
  if ((m_status = m_alti->CTL_L1aCalibrationSignal_Read(bs)) != 0){
    CERR("ALTI: reading L1A calibration signal","");
    return(m_status);
  }
  bs.L1aDelay(delay);
  if ((m_status = m_alti->CTL_L1aCalibrationSignal_Write(bs)) != 0){
    CERR("ALTI: writing L1A calibration signal","");
    return(m_status);
  }
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::SIGBGo2L1aCounterRead(unsigned int &cnt) {

  ALTI_CTL_L1ACALIBRATIONSIGNAL_BITSTRING bs;
  if ((m_status = m_alti->CTL_L1aCalibrationSignal_Read(bs)) != 0){
    CERR("ALTI: reading L1A calibration signal","");
    return(m_status);
  }
  cnt = bs.CounterValue();

  return (m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::BSYInputSelectRead(const BUSY_INPUT in, bool &selected) { // 1 input
    ALTI_BSY_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    switch (in) {
        case BUSY_FROM_FRONT_PANEL:
            selected = bs.FrontPanelBusyEnable();
            break;
        case BUSY_FROM_CTP:
            selected = bs.CtpBusyEnable();
            break;
        case BUSY_FROM_ALTI:
            selected = bs.AltiBusyEnable();
            break;
        case BUSY_FROM_PG:
            selected = bs.PatternBusyEnable();
            break;
        case BUSY_FROM_VME:
            selected = bs.VmeBusyEnable();
            break;
        case BUSY_FROM_ECR:
            selected = bs.EcrBusyEnable();
            break;
        default:
            break;
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYInputSelectWrite(const BUSY_INPUT in, const bool select) { // 1 input
    ALTI_BSY_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    // modify
    switch (in) {
        case BUSY_FROM_FRONT_PANEL:
            bs.FrontPanelBusyEnable(select);
            break;
        case BUSY_FROM_CTP:
            bs.CtpBusyEnable(select);
            break;
        case BUSY_FROM_ALTI:
            bs.AltiBusyEnable(select);
            break;
        case BUSY_FROM_PG:
            bs.PatternBusyEnable(select);
            break;
        case BUSY_FROM_VME:
            bs.VmeBusyEnable(select);
            break;
        case BUSY_FROM_ECR:
            bs.EcrBusyEnable(select);
            break;
        default:
            break;
    }

    // write
    if ((m_status = m_alti->BSY_Control_Write(bs)) != 0) {
        CERR("ALTI: writing busy control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYInputSelectRead(std::vector<bool> &selected) { // all inputs
    ALTI_BSY_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    selected.clear();

    unsigned int i;
    for (i = 0; i < BUSY_INPUT_NUMBER; i++) {
      // 0: not selected, 1: selected
      switch ((BUSY_INPUT) i) {
      case BUSY_FROM_FRONT_PANEL:
	selected.push_back(bs.FrontPanelBusyEnable());
	break;
      case BUSY_FROM_CTP:
	selected.push_back(bs.CtpBusyEnable());
	break;
      case BUSY_FROM_ALTI:
	selected.push_back(bs.AltiBusyEnable());
	break;
      case BUSY_FROM_PG:
	selected.push_back(bs.PatternBusyEnable());
	break;
      case BUSY_FROM_VME:
	selected.push_back(bs.VmeBusyEnable());
	break;
      case BUSY_FROM_ECR:
	selected.push_back(bs.EcrBusyEnable());
	break;
      default:
	break;
      }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYInputSelectWrite(const std::vector<bool> &select) { // all inputs
    ALTI_BSY_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    // modify
    unsigned int i;
    for (i = 0; i < BUSY_INPUT_NUMBER; i++) {
        // 0: not selected, 1: selected
        switch ((BUSY_INPUT) i) {
            case BUSY_FROM_FRONT_PANEL:
                bs.FrontPanelBusyEnable(select[i]);
                break;
            case BUSY_FROM_CTP:
                bs.CtpBusyEnable(select[i]);
                break;
            case BUSY_FROM_ALTI:
                bs.AltiBusyEnable(select[i]);
                break;
            case BUSY_FROM_PG:
                bs.PatternBusyEnable(select[i]);
                break;
            case BUSY_FROM_VME:
                bs.VmeBusyEnable(select[i]);
                break;
            case BUSY_FROM_ECR:
                bs.EcrBusyEnable(select[i]);
                break;
            default:
                break;
        }
    }

    // write
    if ((m_status = m_alti->BSY_Control_Write(bs)) != 0) {
        CERR("ALTI: writing busy control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYOutputSelectRead(const BUSY_OUTPUT out, BUSY_SOURCE &src) { // 1 output
    ALTI_BSY_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    std::string s;
    switch (out) {
        case BUSY_TO_CTP:
            s = bs.CtpOutputSelect();
            break;
        case BUSY_TO_ALTI:
            s = bs.AltiOutputSelect();
            break;
        default:
            break;
    }

    if (s == "INACTIVE") {
        src = BUSY_INACTIVE;
    }
    else if (s == "LOCAL") {
        src = BUSY_LOCAL;
    }
    else if (s == "CTP") {
        src = BUSY_CTP;
    }
    else if (s == "ALTI") {
        src = BUSY_ALTI;
    }
    else {
        CERR("ALTI: unknown busy source", "");
        m_status = -1;
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYOutputSelectWrite(const BUSY_OUTPUT out, const BUSY_SOURCE src) { // 1 output
    ALTI_BSY_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    std::string s;
    switch (out) {
        case BUSY_TO_CTP:
            // modify
            switch (src) {
                case BUSY_INACTIVE:
                    bs.CtpOutputSelect("INACTIVE");
                    break;
                case BUSY_LOCAL:
                    bs.CtpOutputSelect("LOCAL");
                    break;
                case BUSY_CTP:
                    bs.CtpOutputSelect("CTP");
                    break;
                case BUSY_ALTI:
                    bs.CtpOutputSelect("ALTI");
                    break;
                default:
                    break;
            }
            break;
        case BUSY_TO_ALTI:
            // modify
            switch (src) {
                case BUSY_INACTIVE:
                    bs.AltiOutputSelect("INACTIVE");
                    break;
                case BUSY_LOCAL:
                    bs.AltiOutputSelect("LOCAL");
                    break;
                case BUSY_CTP:
                    bs.AltiOutputSelect("CTP");
                    break;
                case BUSY_ALTI:
                    bs.AltiOutputSelect("ALTI");
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

    // write
    if ((m_status = m_alti->BSY_Control_Write(bs)) != 0) {
        CERR("ALTI: writing busy control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYOutputSelectRead(std::vector<BUSY_SOURCE> &src) { // all outputs
    ALTI_BSY_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    src.clear();

    std::string s;
    unsigned int i;
    for (i = 0; i < BUSY_OUTPUT_NUMBER; i++) {
        switch ((BUSY_OUTPUT) i) {
            case BUSY_TO_CTP:
                s = bs.CtpOutputSelect();
                break;
            case BUSY_TO_ALTI:
                s = bs.AltiOutputSelect();
                break;
            default:
                break;
        }

        if (s == "INACTIVE") {
            src.push_back(BUSY_INACTIVE);
        }
        else if (s == "LOCAL") {
            src.push_back(BUSY_LOCAL);
        }
        else if (s == "CTP") {
            src.push_back(BUSY_CTP);
        }
        else if (s == "ALTI") {
            src.push_back(BUSY_ALTI);
        }
        else {
            CERR("ALTI: unknown busy source", "");
            m_status = -1;
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYOutputSelectWrite(const std::vector<BUSY_SOURCE> &src) { // all outputs
    ALTI_BSY_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    unsigned int i;
    for (i = 0; i < BUSY_OUTPUT_NUMBER; i++) {
        switch ((BUSY_OUTPUT) i) {
            case BUSY_TO_CTP:
                // modify
                switch (src[i]) {
                    case BUSY_INACTIVE:
                        bs.CtpOutputSelect("INACTIVE");
                        break;
                    case BUSY_LOCAL:
                        bs.CtpOutputSelect("LOCAL");
                        break;
                    case BUSY_CTP:
                        bs.CtpOutputSelect("CTP");
                        break;
                    case BUSY_ALTI:
                        bs.CtpOutputSelect("ALTI");
                        break;
                    default:
                        break;
                }
                break;
            case BUSY_TO_ALTI:
                // modify
                switch (src[i]) {
                    case BUSY_INACTIVE:
                        bs.AltiOutputSelect("INACTIVE");
                        break;
                    case BUSY_LOCAL:
                        bs.AltiOutputSelect("LOCAL");
                        break;
                    case BUSY_CTP:
                        bs.AltiOutputSelect("CTP");
                        break;
                    case BUSY_ALTI:
                        bs.AltiOutputSelect("ALTI");
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    // write
    if ((m_status = m_alti->BSY_Control_Write(bs)) != 0) {
        CERR("ALTI: writing busy control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYConstLevelRegRead(bool &busy) {
    ALTI_BSY_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    busy = bs.VmeBusy();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYConstLevelRegWrite(const bool busy) {
    ALTI_BSY_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    // modify
    bs.VmeBusy(busy);

    // write
    if ((m_status = m_alti->BSY_Control_Write(bs)) != 0) {
        CERR("ALTI: writing busy control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingL1aEnableRead(bool &enabled) {
    ALTI_BSY_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    enabled = bs.EnableL1aBusyMasking();
        
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingL1aEnableWrite(const bool enable) {
    ALTI_BSY_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }
    // MODIFY
    bs.EnableL1aBusyMasking(enable);
    // WRITE
    if ((m_status = m_alti->BSY_Control_Write(bs)) != 0 ) {
        CERR("ALTI: writing busy control register", "");
        return (m_status);
    }
    return (m_status);
}


//------------------------------------------------------------------------------

int AltiModule::BSYMaskingTTREnableWrite(const short ttr,const bool enable) {
    ALTI_BSY_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }
    // MODIFY
    bs.EnableTTrBusyMasking(ttr, enable);
    // WRITE
    if ((m_status = m_alti->BSY_Control_Write(bs)) != 0 ) {
        CERR("ALTI: writing busy control register", "");
        return (m_status);
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingTTREnableRead(bool &ttr1, bool &ttr2, bool &ttr3) {
    ALTI_BSY_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    ttr1 = bs.EnableTTrBusyMasking(0);
    ttr2 = bs.EnableTTrBusyMasking(1);
    ttr3 = bs.EnableTTrBusyMasking(2);
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingBGoEnableWrite(const short bgo,const bool enable) {
    ALTI_BSY_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }
    // MODIFY
    bs.EnableBGoBusyMasking(bgo, enable);
    // WRITE
    if ((m_status = m_alti->BSY_Control_Write(bs)) != 0 ) {
        CERR("ALTI: writing busy control register", "");
        return (m_status);
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingBGoEnableRead(bool &bgo0, bool &bgo1, bool &bgo2, bool &bgo3) {
    ALTI_BSY_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->BSY_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading busy control register", "");
        return (m_status);
    }

    bgo0 = bs.EnableBGoBusyMasking(0);
    bgo1 = bs.EnableBGoBusyMasking(1);
    bgo2 = bs.EnableBGoBusyMasking(2);
    bgo3 = bs.EnableBGoBusyMasking(3);
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingPatL1aEnableRead(bool &enabled) {
  ALTI_PRM_PATBUSYMASKING_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->PRM_PatBusyMasking_Read(bs)) != 0 ) {
        CERR("ALTI: reading PRM busy control register", "");
        return (m_status);
  }
  
  enabled = bs.L1a();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingPatL1aEnableWrite(const bool enable) {
    ALTI_PRM_PATBUSYMASKING_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_PatBusyMasking_Read(bs)) != 0 ) {
        CERR("ALTI: reading PRM busy control register", "");
        return (m_status);
    }
    // MODIFY
    bs.L1a(enable);
    // WRITE
    if ((m_status = m_alti->PRM_PatBusyMasking_Write(bs)) != 0 ) {
        CERR("ALTI: writing PRM busy control register", "");
        return (m_status);
    }
    return (m_status);
}


//------------------------------------------------------------------------------

int AltiModule::BSYMaskingPatTTREnableWrite(const short ttr,const bool enable) {
    ALTI_PRM_PATBUSYMASKING_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_PatBusyMasking_Read(bs)) != 0 ) {
        CERR("ALTI: reading PRM busy control register", "");
        return (m_status);
    }
    // MODIFY
    bs.TestTrigger(ttr, enable);
    // WRITE
    if ((m_status = m_alti->PRM_PatBusyMasking_Write(bs)) != 0 ) {
        CERR("ALTI: writing PRM busy control register", "");
        return (m_status);
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingPatTTREnableRead(bool &ttr1, bool &ttr2, bool &ttr3) {
    ALTI_PRM_PATBUSYMASKING_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_PatBusyMasking_Read(bs)) != 0 ) {
        CERR("ALTI: reading PRM busy control register", "");
        return (m_status);
    }

    ttr1 = bs.TestTrigger(0);
    ttr2 = bs.TestTrigger(1);
    ttr3 = bs.TestTrigger(2);
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingPatBGoEnableWrite(const short bgo,const bool enable) {
  ALTI_PRM_PATBUSYMASKING_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_PatBusyMasking_Read(bs)) != 0 ) {
        CERR("ALTI: reading PRM busy control register", "");
        return (m_status);
    }
    // MODIFY
    bs.Bgo(bgo, enable);
    // WRITE
    if ((m_status = m_alti->PRM_PatBusyMasking_Write(bs)) != 0 ) {
        CERR("ALTI: writing PRM busy control register", "");
        return (m_status);
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMaskingPatBGoEnableRead(bool &bgo0, bool &bgo1, bool &bgo2, bool &bgo3) {
    ALTI_PRM_PATBUSYMASKING_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_PatBusyMasking_Read(bs)) != 0 ) {
        CERR("ALTI: reading PRM busy control register", "");
        return (m_status);
    }

    bgo0 = bs.Bgo(0);
    bgo1 = bs.Bgo(1);
    bgo2 = bs.Bgo(2);
    bgo3 = bs.Bgo(3);
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFrontPanelLevelRead(BUSY_LEVEL &level) {
  ALTI_BSY_CONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->BSY_Control_Read(bs)) != 0 ) {
    CERR("ALTI: reading busy Front-Panel Level register", "");
    return (m_status);
  }
  
  std::string level_str = bs.FrontPanelLevel();
  
  if(level_str == "NIM") level = NIM;
  else if(level_str == "TTL") level = TTL;
  else {
    CERR("ALTI: unknown Busy Front-Panel Level", "");
    m_status = -1;
    return (m_status);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFrontPanelLevelWrite(const BUSY_LEVEL level)
{
  ALTI_BSY_CONTROL_BITSTRING bs;

 // READ
  if ((m_status = m_alti->BSY_Control_Read(bs)) != 0 ) {
    CERR("ALTI: reading busy Front-Panel Level register", "");
    return (m_status);
  }
  
  switch (level) {
  case NIM: 
    bs.FrontPanelLevel("NIM");
    break;
  case TTL:
    bs.FrontPanelLevel("TTL");
    break;
  default:
    break;
  }

  // WRITE
  if ((m_status = m_alti->BSY_Control_Write(bs)) != 0 ) {
    CERR("ALTI: writing busy Front-Panel Level register", "");
    return (m_status);
  }
  return (m_status); 
  }

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::CRQInputSyncHistogramsRead(const ASYNC_INPUT_CALREQ asyncIn, std::vector<unsigned int> &edge_detected){//&countPosByPhase, std::vector<unsigned int> &countNegByPhase) {

  if(asyncIn <= ASYNC_INPUT_CALREQ::CTP_CRQ2) { // from CTP
    ALTI_CRQ_CTPINSYNCCONTROL_BITSTRING bs;
    
    if ((m_status = m_alti->CRQ_CtpInSyncControl_Read(asyncIn, bs)) != 0) {
      CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
      return (m_status);
    }
    unsigned int i;
    edge_detected.clear();
    for(i = 0; i < 8; i++){
      edge_detected.push_back(bs.EdgeDetected(i));
    }
  }
  else if(asyncIn <= ASYNC_INPUT_CALREQ::ALTI_CRQ2){ // from ALTI
    ALTI_CRQ_ALTIINSYNCCONTROL_BITSTRING bs;
    
    if ((m_status = m_alti->CRQ_AltiInSyncControl_Read(asyncIn-3, bs)) != 0) {
      CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
      return (m_status);
    }
    unsigned int i;
    edge_detected.clear();
    for(i = 0; i < 8; i++){
      edge_detected.push_back(bs.EdgeDetected(i));
    }
  }
  else if(asyncIn <= ASYNC_INPUT_CALREQ::RJ45_CRQ2){ // from RJ45
    ALTI_CRQ_RJ45INSYNCCONTROL_BITSTRING bs;
    
    if ((m_status = m_alti->CRQ_Rj45InSyncControl_Read(asyncIn-6, bs)) != 0) {
      CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
      return (m_status);
    }
    unsigned int i;
    edge_detected.clear();
    for(i = 0; i < 8; i++){
      edge_detected.push_back(bs.EdgeDetected(i));
    }
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQInputSyncHistogramsReset() {

    ASYNC_INPUT_CALREQ asyncIn;
    unsigned int i;

    for (i = 0; i < ASYNC_INPUT_CALREQ_NUMBER; i++) {
        asyncIn = (ASYNC_INPUT_CALREQ) i;

	if(asyncIn <= ASYNC_INPUT_CALREQ::CTP_CRQ2) { // from CTP

	  ALTI_CRQ_CTPINSYNCCONTROL_BITSTRING bs; 
	  // READ
	  if ((m_status = m_alti->CRQ_CtpInSyncControl_Read(asyncIn, bs)) != 0) {
            CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            return (m_status);
	  }
	  // WRITE: reset histogram
	  bs.StatusClear(1);
	  if ((m_status = m_alti->CRQ_CtpInSyncControl_Write(asyncIn, bs)) != 0) {
            CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            return (m_status);
	  }
	  // WRITE: re-enable histogram calculation
	  bs.StatusClear(0);
	  if ((m_status = m_alti->CRQ_CtpInSyncControl_Write(asyncIn, bs)) != 0) {
            CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            return (m_status);
	  }
	}
	else if(asyncIn <= ASYNC_INPUT_CALREQ::ALTI_CRQ2){ // from ALTI

	  ALTI_CRQ_ALTIINSYNCCONTROL_BITSTRING bs;
	  // READ
	  if ((m_status = m_alti->CRQ_AltiInSyncControl_Read(asyncIn-3, bs)) != 0) {
            CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            return (m_status);
	  }
	  // WRITE: reset histogram
	  bs.StatusClear(1);
	  if ((m_status = m_alti->CRQ_AltiInSyncControl_Write(asyncIn-3, bs)) != 0) {
            CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            return (m_status);
	  }
	  // WRITE: re-enable histogram calculation
	  bs.StatusClear(0);
	  if ((m_status = m_alti->CRQ_AltiInSyncControl_Write(asyncIn-3, bs)) != 0) {
            CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            return (m_status);
	  }
	}
	else if(asyncIn <= ASYNC_INPUT_CALREQ::RJ45_CRQ2){ // from RJ45

	  ALTI_CRQ_RJ45INSYNCCONTROL_BITSTRING bs;
	  // READ
	  if ((m_status = m_alti->CRQ_Rj45InSyncControl_Read(asyncIn-6, bs)) != 0) {
            CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            return (m_status);
	  }
	  // WRITE: reset histogram
	  bs.StatusClear(1);
	  if ((m_status = m_alti->CRQ_Rj45InSyncControl_Write(asyncIn-6, bs)) != 0) {
            CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            return (m_status);
	  }
	  // WRITE: re-enable histogram calculation
	  bs.StatusClear(0);
	  if ((m_status = m_alti->CRQ_Rj45InSyncControl_Write(asyncIn-6, bs)) != 0) {
            CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            return (m_status);
	  }
	}
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQInputSyncEnableRead(const ASYNC_INPUT_CALREQ asyncIn, unsigned int &phase) {
    
  if(asyncIn <= ASYNC_INPUT_CALREQ::CTP_CRQ2) { // from CTP
    ALTI_CRQ_CTPINSYNCCONTROL_BITSTRING bs;    
    // READ
    if ((m_status = m_alti->CRQ_CtpInSyncControl_Read(asyncIn, bs)) != 0) {
      CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
      return (m_status);
    }
    phase = bs.PhaseSelect();
  }
  else if(asyncIn <= ASYNC_INPUT_CALREQ::ALTI_CRQ2) { // from ALTI 
    ALTI_CRQ_ALTIINSYNCCONTROL_BITSTRING bs;    
    // READ
    if ((m_status = m_alti->CRQ_AltiInSyncControl_Read(asyncIn-3, bs)) != 0) {
      CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
      return (m_status);
    }
    phase = bs.PhaseSelect();
  }
  else if(asyncIn <= ASYNC_INPUT_CALREQ::RJ45_CRQ2) { // from RJ45 
    ALTI_CRQ_RJ45INSYNCCONTROL_BITSTRING bs;    
    // READ
    if ((m_status = m_alti->CRQ_Rj45InSyncControl_Read(asyncIn-6, bs)) != 0) {
      CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
      return (m_status);
    }
    phase = bs.PhaseSelect();
  }
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQInputSyncEnableWrite(const ASYNC_INPUT_CALREQ asyncIn, const unsigned int phase) {
    if (phase < 0 || phase > 7) {
        CERR("ALTI: wrong phase index \"%d\", allowed range is [0, 7]", phase);
        m_status = -1;
        return (m_status);
    }

    if(asyncIn <= ASYNC_INPUT_CALREQ::CTP_CRQ2) { // from CTP
      ALTI_CRQ_CTPINSYNCCONTROL_BITSTRING bs;
      // READ
      if ((m_status = m_alti->CRQ_CtpInSyncControl_Read(asyncIn, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
        return (m_status);
      }
      // WRITE: reset histogram
      bs.StatusClear(true);
      if ((m_status = m_alti->CRQ_CtpInSyncControl_Write(asyncIn, bs)) != 0) {
        CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
        return (m_status);
      }
      bs.PhaseSelect(phase);
      // WRITE: re-enable histogram calculation
      bs.StatusClear(false);
      if ((m_status = m_alti->CRQ_CtpInSyncControl_Write(asyncIn, bs)) != 0) {
        CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
        return (m_status);
      }
    }
    else if(asyncIn <= ASYNC_INPUT_CALREQ::ALTI_CRQ2){ // from ALTI
      ALTI_CRQ_ALTIINSYNCCONTROL_BITSTRING bs;
      // READ
      if ((m_status = m_alti->CRQ_AltiInSyncControl_Read(asyncIn-3, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
        return (m_status);
      }
      // WRITE: reset histogram
      bs.StatusClear(true);
      if ((m_status = m_alti->CRQ_AltiInSyncControl_Write(asyncIn-3, bs)) != 0) {
        CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
        return (m_status);
      }
      bs.PhaseSelect(phase);
      // WRITE: re-enable histogram calculation
      bs.StatusClear(false);
      if ((m_status = m_alti->CRQ_AltiInSyncControl_Write(asyncIn-3, bs)) != 0) {
        CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
        return (m_status);
      }
    }
    else if(asyncIn <= ASYNC_INPUT_CALREQ::RJ45_CRQ2){ // from RJ45
      ALTI_CRQ_RJ45INSYNCCONTROL_BITSTRING bs;
      // READ
      if ((m_status = m_alti->CRQ_Rj45InSyncControl_Read(asyncIn-6, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
        return (m_status);
      }
      // WRITE: reset histogram
      bs.StatusClear(true);
      if ((m_status = m_alti->CRQ_Rj45InSyncControl_Write(asyncIn-6, bs)) != 0) {
        CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
        return (m_status);
      }
      bs.PhaseSelect(phase);
      // WRITE: re-enable histogram calculation
      bs.StatusClear(false);
      if ((m_status = m_alti->CRQ_Rj45InSyncControl_Write(asyncIn-6, bs)) != 0) {
        CERR("ALTI: writing configuration and histogram data register for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
        return (m_status);
      }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQInputSyncEnableRead(std::vector<unsigned int> &phase) { // all
    phase.clear();

    unsigned int i;
    ASYNC_INPUT_CALREQ asyncIn;
    unsigned int rd_phase;
    for (i = 0; i < ASYNC_INPUT_CALREQ_NUMBER; i++) {
      asyncIn = (ASYNC_INPUT_CALREQ) i;
      if ((m_status = CRQInputSyncEnableRead(asyncIn, rd_phase)) != 0) {
	CERR("ALTI: reading synchronization enable and phase for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
	return (m_status);
      }
      phase.push_back(rd_phase);
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQInputSyncEnableWrite( const std::vector<unsigned int> &phase) { // all
  unsigned int i;
  ASYNC_INPUT_CALREQ asyncIn;
  for (i = 0; i < ASYNC_INPUT_CALREQ_NUMBER; i++) {
    asyncIn = (ASYNC_INPUT_CALREQ) i;
    if ((m_status = CRQInputSyncEnableWrite(asyncIn, phase[i])) != 0) {
      CERR("ALTI: writing synchronization enable and phase for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
      return (m_status);
    }
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQInputSyncEnableWrite(const unsigned int phase) { // all
  unsigned int i;
  ASYNC_INPUT_CALREQ asyncIn;
  for (i = 0; i < ASYNC_INPUT_CALREQ_NUMBER; i++) {
    asyncIn = (ASYNC_INPUT_CALREQ) i;
    if ((m_status = CRQInputSyncEnableWrite(asyncIn, phase)) != 0) {
      CERR("ALTI: writing synchronization enable and phase for asynchronous input signal \"%s\"", ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
      return (m_status);
    }
  }
  
  return (m_status);
}
//------------------------------------------------------------------------------

int AltiModule::CRQRJ45ShapeEnableRead(const unsigned int sig, SYNC_SHAPE &s) {
    ALTI_CRQ_RJ45INSYNCCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CRQ_Rj45InSyncControl_Read(sig, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous input signal \"%u\"",sig);
        return (m_status);
    }
    int rd_val = bs.SyncOrShape();
    if (rd_val == 0){
        s = IN_SYNCHRONIZED;
      }
    else if(rd_val == 1) {
        s = IN_SHAPED_ONE;
      }
    else {
        s = IN_SHAPED_TWO;
      }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQRJ45ShapeEnableWrite(const unsigned int sig, const unsigned int s) {
    ALTI_CRQ_RJ45INSYNCCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CRQ_Rj45InSyncControl_Read(sig, bs)) != 0) {
        CERR("ALTI: reading shape length register for asynchronous RJ45 input \"%u\"", sig);
        return (m_status);
    }
      bs.SyncOrShape(s);
    if ((m_status = m_alti->CRQ_Rj45InSyncControl_Write(sig, bs)) != 0) {
        CERR("ALTI: reading configuration and histogram data register for asynchronous RJ45 input \"%u\"", sig);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQLocalSourceRead(const unsigned int bit, CALREQ_INPUT &in) {
    if (bit > 2) {
        CERR("ALTI: calibration request bit exceeds [2:0] range", "");
        return (m_status);
    }

    ALTI_CRQ_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    std::string s = bs.LocalSourceSelect(bit);
    if (s == "RJ45") {
        in = CALREQ_FROM_RJ45;
    }
    else if (s == "FRONTPANEL") {
        in = CALREQ_FROM_FRONT_PANEL;
    }
    else if (s == "CTP") {
        in = CALREQ_FROM_CTP;
    }
    else if (s == "ALTI") {
        in = CALREQ_FROM_ALTI;
    }
    else if (s == "PATTERN") {
        in = CALREQ_FROM_PG;
    }
    else if (s == "VME") {
        in = CALREQ_FROM_VME;
    }
    else {
      CERR("ALTI: unknown calibration request input %s", s.c_str());
      m_status = -1;
      return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQLocalSourceWrite(const unsigned int bit, const CALREQ_INPUT in) {
    if (bit > 2) {
        CERR("ALTI: calibration request bit exceeds [2:0] range", "");
        return (m_status);
    }

    ALTI_CRQ_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    // modify
    switch (in) {
        case CALREQ_FROM_RJ45:
            bs.LocalSourceSelect(bit, "RJ45");
            break;
        case CALREQ_FROM_FRONT_PANEL:
            bs.LocalSourceSelect(bit, "FRONTPANEL");
            break;
        case CALREQ_FROM_CTP:
            bs.LocalSourceSelect(bit, "CTP");
            break;
        case CALREQ_FROM_ALTI:
            bs.LocalSourceSelect(bit, "ALTI");
            break;
        case CALREQ_FROM_PG:
            bs.LocalSourceSelect(bit, "PATTERN");
            break;
        case CALREQ_FROM_VME:
            bs.LocalSourceSelect(bit, "VME");
            break;
        default:
            break;
    }

    // write
    if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
        CERR("ALTI: writing calibration request control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQInputSelectRead(std::vector<CALREQ_INPUT> &in) { // all 3 bits

    ALTI_CRQ_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    in.clear();
    unsigned int bit;
    for (bit = 0; bit < 3; bit++) {
        std::string s = bs.LocalSourceSelect(bit);
        if (s == "RJ45") {
            in.push_back(CALREQ_FROM_RJ45);
        }
        else if (s == "FRONTPANEL") {
            in.push_back(CALREQ_FROM_FRONT_PANEL);
        }
        else if (s == "CTP") {
            in.push_back(CALREQ_FROM_CTP);
        }
        else if (s == "ALTI") {
            in.push_back(CALREQ_FROM_ALTI);
        }
        else if (s == "PATTERN") {
            in.push_back(CALREQ_FROM_PG);
        }
        else if (s == "VME") {
            in.push_back(CALREQ_FROM_VME);
        }
        else {
	  CERR("ALTI: unknown calibration request input %s", s.c_str());
            m_status = -1;
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQInputSelectWrite(const std::vector<CALREQ_INPUT> &in) { // all 3 bits

    ALTI_CRQ_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    unsigned int bit;
    for (bit = 0; bit < 3; bit++) {
        // modify
        switch (in[bit]) {
            case CALREQ_FROM_RJ45:
                bs.LocalSourceSelect(bit, "RJ45");
                break;
            case CALREQ_FROM_FRONT_PANEL:
                bs.LocalSourceSelect(bit, "FRONTPANEL");
                break;
            case CALREQ_FROM_CTP:
                bs.LocalSourceSelect(bit, "CTP");
                break;
            case CALREQ_FROM_ALTI:
                bs.LocalSourceSelect(bit, "ALTI");
                break;
            case CALREQ_FROM_PG:
                bs.LocalSourceSelect(bit, "PATTERN");
                break;
            case CALREQ_FROM_VME:
                bs.LocalSourceSelect(bit, "VME");
                break;
            default:
                break;
        }
    }

    // write
    if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
        CERR("ALTI: writing calibration request control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQInputSelectWrite(const CALREQ_INPUT in) { // all 3 bits

    ALTI_CRQ_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    unsigned int bit;
    for (bit = 0; bit < 3; bit++) {
        // modify
        switch (in) {
            case CALREQ_FROM_RJ45:
                bs.LocalSourceSelect(bit, "RJ45");
                break;
            case CALREQ_FROM_FRONT_PANEL:
                bs.LocalSourceSelect(bit, "FRONTPANEL");
                break;
            case CALREQ_FROM_CTP:
                bs.LocalSourceSelect(bit, "CTP");
                break;
            case CALREQ_FROM_ALTI:
                bs.LocalSourceSelect(bit, "ALTI");
                break;
            case CALREQ_FROM_PG:
                bs.LocalSourceSelect(bit, "PATTERN");
                break;
            case CALREQ_FROM_VME:
                bs.LocalSourceSelect(bit, "VME");
                break;
            default:
                break;
        }
    }

    // write
    if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
        CERR("ALTI: writing calibration request control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQOutputSelectRead(const CALREQ_OUTPUT out, const unsigned int bit, CALREQ_SOURCE &src) { // 1 output, 1 bit
    if (bit > 2) {
        CERR("ALTI: calibration request bit exceeds [2:0] range", "");
        return (m_status);
    }

    ALTI_CRQ_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    std::string s;
    switch (out) {
        case CALREQ_TO_CTP:
            s = bs.CtpOutputSelect(bit);
            break;
        case CALREQ_TO_ALTI:
            s = bs.AltiOutputSelect(bit);
            break;
        default:
            break;
    }

    if (s == "INACTIVE") {
        src = CALREQ_INACTIVE;
    }
    else if (s == "LOCAL") {
        src = CALREQ_LOCAL;
    }
    else if (s == "CTP") {
        src = CALREQ_CTP;
    }
    else if (s == "ALTI") {
        src = CALREQ_ALTI;
    }
    else {
        CERR("ALTI: unknown calibration request source", "");
        m_status = -1;
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQOutputSelectWrite(const CALREQ_OUTPUT out, const unsigned int bit, const CALREQ_SOURCE src) { // 1 output, 1 bit
    if (bit > 2) {
        CERR("ALTI: calibration request bit exceeds [2:0] range", "");
        return (m_status);
    }

    ALTI_CRQ_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    std::string s;
    switch (out) {
        case CALREQ_TO_CTP:
            // modify
            switch (src) {
                case CALREQ_INACTIVE:
                    bs.CtpOutputSelect(bit, "INACTIVE");
                    break;
                case CALREQ_LOCAL:
                    bs.CtpOutputSelect(bit, "LOCAL");
                    break;
                case CALREQ_CTP:
                    bs.CtpOutputSelect(bit, "CTP");
                    break;
                case CALREQ_ALTI:
                    bs.CtpOutputSelect(bit, "ALTI");
                    break;
                default:
                    break;
            }
            break;
        case CALREQ_TO_ALTI:
            // modify
            switch (src) {
                case CALREQ_INACTIVE:
                    bs.AltiOutputSelect(bit, "INACTIVE");
                    break;
                case CALREQ_LOCAL:
                    bs.AltiOutputSelect(bit, "LOCAL");
                    break;
                case CALREQ_CTP:
                    bs.AltiOutputSelect(bit, "CTP");
                    break;
                case CALREQ_ALTI:
                    bs.AltiOutputSelect(bit, "ALTI");
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

    // write
    if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
        CERR("ALTI: writing calibration request control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQOutputSelectRead(const CALREQ_OUTPUT out, std::vector<CALREQ_SOURCE> &src) { // 1 output, all 3 bits
    ALTI_CRQ_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    src.clear();
    unsigned int bit;
    for (bit = 0; bit < 3; bit++) {
        std::string s;
        switch (out) {
            case CALREQ_TO_CTP:
                s = bs.CtpOutputSelect(bit);
                break;
            case CALREQ_TO_ALTI:
                s = bs.AltiOutputSelect(bit);
                break;
            default:
                break;
        }

        if (s == "INACTIVE") {
            src.push_back(CALREQ_INACTIVE);
        }
        else if (s == "LOCAL") {
            src.push_back(CALREQ_LOCAL);
        }
        else if (s == "CTP") {
            src.push_back(CALREQ_CTP);
        }
        else if (s == "ALTI") {
            src.push_back(CALREQ_ALTI);
        }
        else {
            CERR("ALTI: unknown calibration request source", "");
            m_status = -1;
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQOutputSelectWrite(const CALREQ_OUTPUT out, const std::vector<CALREQ_SOURCE> &src) { // 1 output, all 3 bits
    ALTI_CRQ_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    unsigned int bit;
    for (bit = 0; bit < 3; bit++) {
        switch (out) {
            case CALREQ_TO_CTP:
                // modify
                switch (src[bit]) {
                    case CALREQ_INACTIVE:
                        bs.CtpOutputSelect(bit, "INACTIVE");
                        break;
                    case CALREQ_LOCAL:
                        bs.CtpOutputSelect(bit, "LOCAL");
                        break;
                    case CALREQ_CTP:
                        bs.CtpOutputSelect(bit, "CTP");
                        break;
                    case CALREQ_ALTI:
                        bs.CtpOutputSelect(bit, "ALTI");
                        break;
                    default:
                        break;
                }
                break;
            case CALREQ_TO_ALTI:
                // modify
                switch (src[bit]) {
                    case CALREQ_INACTIVE:
                        bs.AltiOutputSelect(bit, "INACTIVE");
                        break;
                    case CALREQ_LOCAL:
                        bs.AltiOutputSelect(bit, "LOCAL");
                        break;
                    case CALREQ_CTP:
                        bs.AltiOutputSelect(bit, "CTP");
                        break;
                    case CALREQ_ALTI:
                        bs.AltiOutputSelect(bit, "ALTI");
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    // write
    if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
        CERR("ALTI: writing calibration request control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQOutputSelectRead(const unsigned int bit, std::vector<CALREQ_SOURCE> &src) { // all outputs, 1 bit
    if (bit > 2) {
        CERR("ALTI: calibration request bit exceeds [2:0] range", "");
        return (m_status);
    }

    ALTI_CRQ_CONTROL_BITSTRING bs;

    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    std::string s_ctp, s_alti;
    s_ctp = bs.CtpOutputSelect(bit);
    s_alti = bs.AltiOutputSelect(bit);

    src.clear();

    if (s_ctp == "INACTIVE") {
        src.push_back(CALREQ_INACTIVE);
    }
    else if (s_ctp == "LOCAL") {
        src.push_back(CALREQ_LOCAL);
    }
    else if (s_ctp == "CTP") {
        src.push_back(CALREQ_CTP);
    }
    else if (s_ctp == "ALTI") {
        src.push_back(CALREQ_ALTI);
    }
    else {
        CERR("ALTI: unknown calibration request source", "");
        m_status = -1;
        return (m_status);
    }

    if (s_alti == "INACTIVE") {
        src.push_back(CALREQ_INACTIVE);
    }
    else if (s_alti == "LOCAL") {
        src.push_back(CALREQ_LOCAL);
    }
    else if (s_alti == "CTP") {
        src.push_back(CALREQ_CTP);
    }
    else if (s_alti == "ALTI") {
        src.push_back(CALREQ_ALTI);
    }
    else {
        CERR("ALTI: unknown calibration request source", "");
        m_status = -1;
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQOutputSelectWrite(const unsigned int bit, const std::vector<CALREQ_SOURCE> &src) { // all outputs, 1 bit
    if (bit > 2) {
        CERR("ALTI: calibration request bit exceeds [2:0] range", "");
        return (m_status);
    }

    ALTI_CRQ_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    // modify
    switch (src[0]) {
        case CALREQ_INACTIVE:
            bs.CtpOutputSelect(bit, "INACTIVE");
            break;
        case CALREQ_LOCAL:
            bs.CtpOutputSelect(bit, "LOCAL");
            break;
       case CALREQ_CTP:
            bs.CtpOutputSelect(bit, "CTP");
            break;
        case CALREQ_ALTI:
            bs.CtpOutputSelect(bit, "ALTI");
            break;
        default:
            break;
    }
    // modify
    switch (src[1]) {
        case CALREQ_INACTIVE:
            bs.AltiOutputSelect(bit, "INACTIVE");
            break;
        case CALREQ_LOCAL:
            bs.AltiOutputSelect(bit, "LOCAL");
            break;
        case CALREQ_CTP:
            bs.AltiOutputSelect(bit, "CTP");
            break;
        case CALREQ_ALTI:
            bs.AltiOutputSelect(bit, "ALTI");
            break;
        default:
            break;
    }

    // write
    if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
        CERR("ALTI: writing calibration request control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQOutputSelectWrite(const CALREQ_OUTPUT out, const CALREQ_SOURCE src) { // 1 output, all bits
    ALTI_CRQ_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    unsigned int bit;
    for (bit = 0; bit < 3; bit++) {
        switch (out) {
            case CALREQ_TO_CTP:
                // modify
                switch (src) {
                    case CALREQ_INACTIVE:
                        bs.CtpOutputSelect(bit, "INACTIVE");
                        break;
                    case CALREQ_LOCAL:
                        bs.CtpOutputSelect(bit, "LOCAL");
                        break;
                    case CALREQ_CTP:
                        bs.CtpOutputSelect(bit, "CTP");
                        break;
                    case CALREQ_ALTI:
                        bs.CtpOutputSelect(bit, "ALTI");
                        break;
                    default:
                        break;
                }
                break;
            case CALREQ_TO_ALTI:
                // modify
                switch (src) {
                    case CALREQ_INACTIVE:
                        bs.AltiOutputSelect(bit, "INACTIVE");
                        break;
                    case CALREQ_LOCAL:
                        bs.AltiOutputSelect(bit, "LOCAL");
                        break;
                    case CALREQ_CTP:
                        bs.AltiOutputSelect(bit, "CTP");
                        break;
                    case CALREQ_ALTI:
                        bs.AltiOutputSelect(bit, "ALTI");
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    // write
    if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
        CERR("ALTI: writing calibration request control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQOutputSelectWrite(const unsigned int bit, const CALREQ_SOURCE src) { // all outputs, 1 bit
    if (bit > 2) {
        CERR("ALTI: calibration request bit exceeds [2:0] range", "");
        return (m_status);
    }

    ALTI_CRQ_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    // modify
    switch (src) {
        case CALREQ_INACTIVE:
            bs.CtpOutputSelect(bit, "INACTIVE");
            break;
        case CALREQ_LOCAL:
            bs.CtpOutputSelect(bit, "LOCAL");
            break;
       case CALREQ_CTP:
            bs.CtpOutputSelect(bit, "CTP");
            break;
        case CALREQ_ALTI:
            bs.CtpOutputSelect(bit, "ALTI");
            break;
        default:
            break;
    }
    // modify
    switch (src) {
        case CALREQ_INACTIVE:
            bs.AltiOutputSelect(bit, "INACTIVE");
            break;
        case CALREQ_LOCAL:
            bs.AltiOutputSelect(bit, "LOCAL");
            break;
        case CALREQ_CTP:
            bs.AltiOutputSelect(bit, "CTP");
            break;
        case CALREQ_ALTI:
            bs.AltiOutputSelect(bit, "ALTI");
            break;
        default:
            break;
    }

    // write
    if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
        CERR("ALTI: writing calibration request control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQOutputSelectWrite(const CALREQ_SOURCE src) { // all outputs, all bits
    ALTI_CRQ_CONTROL_BITSTRING bs;

    // read
    if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    unsigned int bit;
    for (bit = 0; bit < 3; bit++) {
        // modify
        switch (src) {
            case CALREQ_INACTIVE:
                bs.CtpOutputSelect(bit, "INACTIVE");
                break;
            case CALREQ_LOCAL:
                bs.CtpOutputSelect(bit, "LOCAL");
                break;
            case CALREQ_CTP:
                bs.CtpOutputSelect(bit, "CTP");
                break;
            case CALREQ_ALTI:
                bs.CtpOutputSelect(bit, "ALTI");
                break;
            default:
                break;
        }
        // modify
        switch (src) {
            case CALREQ_INACTIVE:
                bs.AltiOutputSelect(bit, "INACTIVE");
                break;
            case CALREQ_LOCAL:
                bs.AltiOutputSelect(bit, "LOCAL");
                break;
            case CALREQ_CTP:
                bs.AltiOutputSelect(bit, "CTP");
                break;
            case CALREQ_ALTI:
                bs.AltiOutputSelect(bit, "ALTI");
                break;
            default:
                break;
        }
    }

    // write
    if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
        CERR("ALTI: writing calibration request control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQConstLevelRegRead(const unsigned int creq, bool& ena) {

  CHECKRANGE0(creq, ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER - 1, "CRQ number");

  ALTI_CRQ_CONTROL_BITSTRING bs;
  
  if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
        CERR("ALTI: reading calibration request control register", "");
        return (m_status);
    }

    ena = bs.VmeCalRequest(creq);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CRQConstLevelRegWrite(const unsigned int creq, const bool ena) {

  CHECKRANGE0(creq, ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER - 1, "CRQ number");

  ALTI_CRQ_CONTROL_BITSTRING bs;
  
  // read
  if ((m_status = m_alti->CRQ_Control_Read(bs)) != 0) {
    CERR("ALTI: reading calibration request control register", "");
    return (m_status);
  }
  
  // modify
  bs.VmeCalRequest(creq, ena);
  
  // write
  if ((m_status = m_alti->CRQ_Control_Write(bs)) != 0) {
    CERR("ALTI: writing calibration request control register", "");
    return (m_status);
  }
  
    return (m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::PRMModeRead(std::string &mode) {
  
  ALTI_PRM_CONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->PRM_Control_Read(bs)) != 0 ) {
    CERR("ALTI: reading pattern/snapshot memory control register", "");
    return (m_status);
  }
  mode = bs.SnapPatSelect();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMModeWrite(const std::string mode) {

  ALTI_PRM_CONTROL_BITSTRING bs;
  
  if(mode != "SNAPSHOT" && mode != "PATTERN") {
    CERR("ALTI: unknow PRM mode %s", mode.c_str());
  }

  // READ
  if ((m_status = m_alti->PRM_Control_Read(bs)) != 0 ) {
    CERR("ALTI: reading pattern/snapshot memory control register", "");
    return (m_status);
  }
   
  // MODIFY
  bs.SnapPatSelect(mode);

  // WRITE
  if ((m_status = m_alti->PRM_Control_Write(bs)) != 0 ) {
    CERR("ALTI: writing pattern/snapshot memory control register", "");
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMEnableRead(bool &enabled) {
    ALTI_PRM_CONTROL_BITSTRING prm;

    // READ
    if ((m_status = m_alti->PRM_Control_Read(prm)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }

    enabled = prm.VmeMode() ? false : true;

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMEnableWrite(const bool enable) {

  
  ALTI_PRM_CONTROL_BITSTRING prm;

  // READ
  if ((m_status = m_alti->PRM_Control_Read(prm)) != 0 ) {
    CERR("ALTI: reading pattern/snapshot memory control register", "");
    return (m_status);
  }
      
  // MODIFY
  bool ena = enable ? false : true;
  prm.VmeMode(ena);
  
  // WRITE
  if ((m_status = m_alti->PRM_Control_Write(prm)) != 0 ) {
    CERR("ALTI: writing ram control register", "");
    return (m_status);
  }
 
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMReset() {

    ALTI_PRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }
    // MODIFY
    bs.VmeMode(false);
    bs.SnapshotReset(true);
    
    // WRITE
    if ((m_status = m_alti->PRM_Control_Write(bs)) != 0 ) {
        CERR("ALTI: writing pattern/snapshot memory control register", "");
        return (m_status);
    }
    
    // MODIFY
    bs.VmeMode(true);
    bs.SnapshotReset(false);
    
    // WRITE
    if ((m_status = m_alti->PRM_Control_Write(bs)) != 0 ) {
      CERR("ALTI: writing pattern/snapshot memory control register", "");
      return (m_status);
    }
    

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMStatusRead(bool &snp_full, std::string &status) {

    ALTI_PRM_STATUS_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_Status_Read(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory status register", "");
        return (m_status);
    }
  
    snp_full = bs.SnapshotFull();
    status   = bs.RunStatus();

    return (m_status);
}
//------------------------------------------------------------------------------

int AltiModule::PRMSnapshotSignalMaskRead(ALTI_PRM_SNAPSHOTMASK_BITSTRING &mask) {

    if ((m_status = m_alti->PRM_SnapshotMask_Read(mask)) != 0) {
        CERR("ALTI: reading snapshot mask register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMSnapshotSignalMaskWrite(const ALTI_PRM_SNAPSHOTMASK_BITSTRING mask) {
    
  bool enabled;
  if ((m_status = PRMEnableRead(enabled)) != SUCCESS) {
    CERR("ALTI: reading PRM enable", "");
    return (m_status);
  }
  if (enabled) {
    CERR("ALTI: writing signal mask while pattern / snapshot is enabled", "");
    m_status = ALTI_RAM_MEMORY_BUSY;
    return (m_status);
  }
  
  if ((m_status = m_alti->PRM_SnapshotMask_Write(mask)) != 0) {
    CERR("ALTI: writing signal mask register with mask 0x%08x", mask);
    return (m_status);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMSnapshotSignalMaskRead(const SIGNAL signal, bool &masked) { // 1 signal
    
  ALTI_PRM_SNAPSHOTMASK_BITSTRING mask;
  if ((m_status = m_alti->PRM_SnapshotMask_Read(mask)) != 0) {
    CERR("ALTI: reading snapshot mask register", "");
    return (m_status);
  }

  switch (signal) {
  case ORB:
    masked = mask.Orbit();
    break;
  case L1A:
    masked = mask.L1a();
    break;
  case TTR1:
    masked = mask.TestTrigger(0);
    break;
  case TTR2:
    masked = mask.TestTrigger(1);
    break;
  case TTR3:
    masked = mask.TestTrigger(2);
    break;
  case TTYP0:
    masked = (mask.TriggerType() & 0x00000001);
    break;
  case TTYP1:
    masked = (mask.TriggerType() & 0x00000002);
    break;
  case TTYP2:
    masked = (mask.TriggerType() & 0x00000004);
    break;
  case TTYP3:
    masked = (mask.TriggerType() & 0x00000008);
    break;
  case TTYP4:
    masked = (mask.TriggerType() & 0x00000010);
    break;
  case TTYP5:
    masked = (mask.TriggerType() & 0x00000020);
    break;
  case TTYP6:
    masked = (mask.TriggerType() & 0x00000040);
    break;
  case TTYP7:
    masked = (mask.TriggerType() & 0x00000080);
    break;
  case BGO0:
    masked = mask.Bgo(0);
    break;
  case BGO1:
    masked = mask.Bgo(1);
    break;
  case BGO2:
    masked = mask.Bgo(2);
    break;
  case BGO3:
    masked = mask.Bgo(3);
    break;
  case CALREQ0:
    masked = mask.CalRequest(0);
    break;
  case CALREQ1:
    masked = mask.CalRequest(1);
    break;
  case CALREQ2:
    masked = mask.CalRequest(2);
    break;      
  case BUSY:
    masked = mask.Busy();
    break;      
  default:
    break;
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMSnapshotSignalMaskWrite(const SIGNAL signal, const bool masked) { // 1 signal

  ALTI_PRM_SNAPSHOTMASK_BITSTRING mask;
  if ((m_status = m_alti->PRM_SnapshotMask_Read(mask)) != 0) {
    CERR("ALTI: reading snapshot mask register", "");
    return (m_status);
  }
 
  u_int ttyp_mask = mask.TriggerType();

  switch (signal) {
  case ORB:
    mask.Orbit(masked);
    break;
  case L1A:
    mask.L1a(masked);
    break;
  case TTR1:
    mask.TestTrigger(0, masked);
    break;
  case TTR2:
    mask.TestTrigger(1, masked);
    break;
  case TTR3:
    mask.TestTrigger(2, masked);
    break;
  case TTYP0:
    mask.TriggerType(masked ? ttyp_mask | 0x00000001 : ttyp_mask & ~0x00000001);
    break;
  case TTYP1:
    mask.TriggerType(masked ? ttyp_mask | 0x00000002 : ttyp_mask & ~0x00000002);
    break;
  case TTYP2:
    mask.TriggerType(masked ? ttyp_mask | 0x00000004 : ttyp_mask & ~0x00000004);
    break;
  case TTYP3:
    mask.TriggerType(masked ? ttyp_mask | 0x00000008 : ttyp_mask & ~0x00000008);
    break;
  case TTYP4:
    mask.TriggerType(masked ? ttyp_mask | 0x00000010 : ttyp_mask & ~0x00000010);
    break;
  case TTYP5:
    mask.TriggerType(masked ? ttyp_mask | 0x00000020 : ttyp_mask & ~0x00000020);
    break;
  case TTYP6:
    mask.TriggerType(masked ? ttyp_mask | 0x00000040 : ttyp_mask & ~0x00000040);
    break;
  case TTYP7:
    mask.TriggerType(masked ? ttyp_mask | 0x00000080 : ttyp_mask & ~0x00000080);
    break;
  case BGO0:
    mask.Bgo(0, masked);
    break;
  case BGO1:
    mask.Bgo(1, masked);
    break;
  case BGO2:
    mask.Bgo(2, masked);
    break;
  case BGO3:
    mask.Bgo(3, masked);
    break;
  case CALREQ0:
    mask.CalRequest(0, masked);
    break;
  case CALREQ1:
    mask.CalRequest(1, masked);
    break;
  case CALREQ2:
    mask.CalRequest(2, masked);
    break;      
  case BUSY:
    mask.Busy(masked);
    break;      
  default:
    break;
  }

  if ((m_status = m_alti->PRM_SnapshotMask_Write(mask)) != 0) {
    CERR("ALTI: writing snapshot mask register", "");
    return (m_status);
  }

  return (m_status);
}


//------------------------------------------------------------------------------

int AltiModule::PRMSnapshotSignalMaskRead(u_int &mask) { // all signals

  if ((m_status = m_alti->PRM_SnapshotMask_Read(mask)) != 0 ) {
    CERR("ALTI: reading snapshot mask", "");
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMSnapshotSignalMaskWrite(const u_int mask) { // all signals

  if ((m_status = m_alti->PRM_SnapshotMask_Write(mask)) != 0 ) {
    CERR("ALTI: writing snapshot mask", "");
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMSnapshotSignalMaskWrite(const bool mask) { // all signals

  for(u_int sig =0; sig < AltiModule::SIGNAL_NUMBER; sig++) {
    if ((m_status = PRMSnapshotSignalMaskWrite(AltiModule::SIGNAL(sig), mask)) != SUCCESS) {
      CERR("ALTI: writing SNP mask", ""); 
      return (m_status);
    }
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMRepeatRead(bool &repeated) {
    ALTI_PRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }
    
    repeated = bs.PatternRepeat();
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMRepeatWrite(const bool repeated) {
    ALTI_PRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }
    // MODIFY
    bs.PatternRepeat(repeated);

    // WRITE
    if ((m_status = m_alti->PRM_Control_Write(bs)) != 0 ) {
        CERR("ALTI: writing pattern/snapshot memory control register", "");
        return (m_status);
    }

    return (m_status);
}

//----------------------------------------------------------------------------------

int AltiModule::PRMCurrentAddressRead(unsigned int &cur) {

  // READ
  if ((m_status = m_alti->PRM_RamCurrentSnapAddress_Read(cur)) != 0 ) {
    CERR("ALTI: reading pattern/snapshot current address", "");
    return (m_status);
  }  
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMStartAddressRead(unsigned int &start) {

    if ((m_status = m_alti->PRM_RamStartPatAddress_Read(start)) != 0) {
        CERR("ALTI: reading pattern start address", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMStartAddressWrite(const unsigned int start) {

    bool pat_enabled;
    if ((m_status = PRMEnableRead(pat_enabled)) != SUCCESS) {
        CERR("ALTI: reading PAT generation enable", "");
        return (m_status);
    }
    if (pat_enabled) {
        CERR("ALTI: configuring pattern generation memory while it is enabled", "");
        m_status = ALTI_RAM_MEMORY_BUSY;
        return (m_status);
    }

    if ((m_status = m_alti->PRM_RamStartPatAddress_Write(start)) != 0) {
        CERR("ALTI: writing pattern start address", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMStopAddressRead(unsigned int &stop) {
    
  if ((m_status = m_alti->PRM_RamStopPatAddress_Read(stop)) != 0) {
        CERR("ALTI: reading pattern stop address", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMStopAddressWrite(const unsigned int stop) {

  bool pat_enabled;
  if ((m_status = PRMEnableRead(pat_enabled)) != SUCCESS) {
    CERR("ALTI: reading PAT generation enable", "");
    return (m_status);
  }
  if (pat_enabled) {
    CERR("ALTI: configuring pattern generation memory while it is enabled", "");
    m_status = ALTI_RAM_MEMORY_BUSY;
    return (m_status);
  }
  
  if ((m_status = m_alti->PRM_RamStopPatAddress_Write(stop)) != 0) {
    CERR("ALTI: writing pattern stop address", "");
    return (m_status);
  }
 
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMTriggerEnableRead(bool &enable) {
    ALTI_PRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }
    enable = bs.TriggerEnable();
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMTriggerEnableWrite(const bool enable) {
    ALTI_PRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }

    bs.TriggerEnable(enable);
    if ((m_status = m_alti->PRM_Control_Write(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMTriggerRepeatRead(bool &repeat) {
    ALTI_PRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }
    repeat = bs.PatternTriggerRepeat();
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMTriggerRepeatWrite(const bool repeat) {
    ALTI_PRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->PRM_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }

    bs.PatternTriggerRepeat(repeat);
    if ((m_status = m_alti->PRM_Control_Write(bs)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }
    return (m_status);
}



//------------------------------------------------------------------------------

int AltiModule::PRMTriggerSignalRead(const SIGNAL_PG signal, bool &trig) { // 1 signal
    
  ALTI_PRM_TRIGGERMASK_BITSTRING mask;
  if ((m_status = m_alti->PRM_TriggerMask_Read(mask)) != 0) {
    CERR("ALTI: reading trigger mask register", "");
    return (m_status);
  }
  
  switch (signal) {
  case PG_ORB:
    trig = mask.Orbit();
    break;;
  case PG_L1A:
    trig = mask.L1a();
    break;
  case PG_TTR1:
    trig = mask.TestTrigger(0);
    break;
  case PG_TTR2:
    trig = mask.TestTrigger(1);
    break;
  case PG_TTR3:
    trig = mask.TestTrigger(2);
    break;
  case PG_TTYP:
    trig = mask.TriggerType();
    break;
  case PG_BGO0:
    trig = mask.Bgo(0);
    break;
  case PG_BGO1:
    trig = mask.Bgo(1);
    break;
  case PG_BGO2:
    trig = mask.Bgo(2);
    break;
  case PG_BGO3:
    trig = mask.Bgo(3);
    break;
  case PG_CALREQ0:
    trig = mask.CalRequest(0);
    break;
  case PG_CALREQ1:
    trig = mask.CalRequest(1);
    break;
  case PG_CALREQ2:
    trig = mask.CalRequest(2);
    break;      
  case PG_BUSY:
    trig = mask.Busy();
    break;      
  default:
    break;
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMTriggerSignalWrite(const SIGNAL_PG signal, const bool trig) { // 1 signal

  ALTI_PRM_TRIGGERMASK_BITSTRING mask;
  if ((m_status = m_alti->PRM_TriggerMask_Read(mask)) != 0) {
    CERR("ALTI: reading snapshot mask register", "");
    return (m_status);
  }
  
  switch (signal) {
  case PG_ORB:
    mask.Orbit(trig);
    break;;
  case PG_L1A:
    mask.L1a(trig);
    break;
  case PG_TTR1:
    mask.TestTrigger(0, trig);
    break;
  case PG_TTR2:
    mask.TestTrigger(1, trig);
    break;
  case PG_TTR3:
    mask.TestTrigger(2, trig);
    break;
  case PG_TTYP:
    mask.TriggerType(trig);
    break;
  case PG_BGO0:
    mask.Bgo(0, trig);
    break;
  case PG_BGO1:
    mask.Bgo(1, trig);
    break;
  case PG_BGO2:
    mask.Bgo(2, trig);
    break;
  case PG_BGO3:
    mask.Bgo(3, trig);
    break;
  case PG_CALREQ0:
    mask.CalRequest(0, trig);
    break;
  case PG_CALREQ1:
    mask.CalRequest(1, trig);
    break;
  case PG_CALREQ2:
    mask.CalRequest(2, trig);
    break;      
  case PG_BUSY:
    mask.Busy(trig);
    break;      
  default:
    break;
  }
    
  if ((m_status = m_alti->PRM_TriggerMask_Write(mask)) != 0) {
    CERR("ALTI: writing trigger mask register", "");
    return (m_status);
  }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMTriggerTypeRead(u_int & ttyp) {

    // READ
    if ((m_status = m_alti->PRM_TriggerTypeValue_Read(ttyp)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot trigger type", "");
        return (m_status);
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMTriggerTypeWrite(const u_int ttyp) {

    // READ
    if ((m_status = m_alti->PRM_TriggerTypeValue_Write(ttyp)) != 0 ) {
        CERR("ALTI: writing pattern/snapshot trigger type", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::MEMSelectWrite(const std::string sr){

    ALTI_PRM_CONTROL_BITSTRING ps;
    ALTI_TRM_CONTROL_BITSTRING ts;
    ALTI_CTL_RAMCONTROL_BITSTRING gs;
    
    // READ
    if ((m_status = m_alti->PRM_Control_Read(ps)) != 0 ) {
        CERR("ALTI: reading pattern/snapshot memory control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_Control_Read(ts)) != 0 ) {
      CERR("ALTI: reading global memory control register", "");
      return (m_status);
 }
    if ((m_status = m_alti->CTL_RamControl_Read(gs)) != 0 ) {
        CERR("ALTI: reading global memory control register", "");
        return (m_status);
    }
    
    // MODIFY
    if (sr == "SPY") {
      ts.Mode("VME");
      ts.RamSelect("SPY");
      gs.RamSelect("SPY");
    }
    else if (sr == "REF") {
      ts.Mode("VME");
      ts.RamSelect("REF");
      gs.RamSelect("SPY");
    }
    else if (sr == "PATSNAP") {
      ps.VmeMode(true);
      gs.RamSelect("PATSNAP");
    }
    else {
      CERR("ALTI: unknown RAM memory %s", sr.c_str());
    }
 
    // WRITE
    if ((m_status = m_alti->PRM_Control_Write(ps)) != 0 ) {
      CERR("ALTI: writing pattern/snapshot memory control register", "");
      return (m_status);
    }
    if ((m_status = m_alti->TRM_Control_Write(ts)) != 0 ) {
      CERR("ALTI: writing TTC decoder memory control register", "");
      return (m_status);
    }
    if ((m_status = m_alti->CTL_RamControl_Write(gs)) != 0 ) {
      CERR("ALTI: writing global memory control register", "");
      return (m_status);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MEMDataRead(const ALTI_RAM_MEMORY mem, unsigned int &data, const unsigned int vme_off) { // number
    bool patsnap_enabled, dec_enabled;
   
    switch (mem) {
    case PATTERN_SNAPSHOT:
      if ((m_status = PRMEnableRead(patsnap_enabled)) != SUCCESS) {
	CERR("ALTI: reading PATTERN / SNAPSHOT generation enable", "");
	return (m_status);
      }
      if (patsnap_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("PATSNAP");
      if ((m_status = m_alti->MEM_SharedAltiRam_Read(vme_off, data)) != 0) {
	CERR("ALTI: reading single entry in \"%s\" memory, index 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off);
	return (m_status);
      }
      break;
    case TTC_DECODER_COMMAND:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("SPY");
      if ((m_status = m_alti->MEM_SharedAltiRam_Read(vme_off, data)) != 0) {
	CERR("ALTI: reading single entry in \"%s\" memory, index 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off);
	return (m_status);
      }
      break;
    case TTC_DECODER_TIMESTAMP:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("REF");
      if ((m_status = m_alti->MEM_SharedAltiRam_Read(vme_off, data)) != 0) {
	CERR("ALTI: reading single entry in \"%s\" memory, index 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off);
	return (m_status);
      }
      break;
    default:
      break;
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MEMDataWrite(const ALTI_RAM_MEMORY mem, const unsigned int data, const unsigned int vme_off) { // number
    bool patsnap_enabled, dec_enabled;
    
    switch (mem) {
    case PATTERN_SNAPSHOT:
      if ((m_status = PRMEnableRead(patsnap_enabled)) != SUCCESS) {
	CERR("ALTI: reading PATTERN /SNAPSHOT generation enable", "");
	return (m_status);
      }
      if (patsnap_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      } 
      MEMSelectWrite("PATSNAP");
      if ((m_status = m_alti->MEM_SharedAltiRam_Write(vme_off, data)) != 0) {
	CERR("ALTI: writing single entry in \"%s\" memory, index 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off);
	return (m_status);
      }
      break;
    case TTC_DECODER_COMMAND:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("SPY");
      if ((m_status = m_alti->MEM_SharedAltiRam_Write(vme_off, data)) != 0) {
	CERR("ALTI: writing single entry in \"%s\" memory, index 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off);
	return (m_status);
      }
      break;
    case TTC_DECODER_TIMESTAMP:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("REF");
      if ((m_status = m_alti->MEM_SharedAltiRam_Write(vme_off, data)) != 0) {
	CERR("ALTI: writing single entry in \"%s\" memory, index 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off);
	return (m_status);
      }
      break;
    default:
      break;
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MEMDataRead(const ALTI_RAM_MEMORY mem, std::vector<unsigned int> &data, const unsigned int vme_off) { // vector
    bool patsnap_enabled, dec_enabled;
    
    switch (mem) {
    case PATTERN_SNAPSHOT:
      if ((m_status = PRMEnableRead(patsnap_enabled)) != SUCCESS) {
	CERR("ALTI: reading PATTERN / SNAPSHOT generation enable", "");
	return (m_status);
      }
      if (patsnap_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("PATSNAP");
      if ((m_status = m_alti->MEM_SharedAltiRam_Read(data, vme_off)) != 0) {
	CERR("ALTI: reading \"%s\" memory in single cycle, index 0x%08x, size 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, data.size());
	return (m_status);
      }
      break;
    case TTC_DECODER_COMMAND:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("SPY");
      if ((m_status = m_alti->MEM_SharedAltiRam_Read(data, vme_off)) != 0) {
	CERR("ALTI: reading \"%s\" memory in single cycle, index 0x%08x, size 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, data.size());
	return (m_status);
      }
      break;
    case TTC_DECODER_TIMESTAMP:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("REF");
      if ((m_status = m_alti->MEM_SharedAltiRam_Read(data, vme_off)) != 0) {
	CERR("ALTI: reading \"%s\" memory in single cycle, index 0x%08x, size 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, data.size());
	return (m_status);
      }
      break;
    default:
      break;
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MEMDataWrite(const ALTI_RAM_MEMORY mem, const std::vector<unsigned int> &data, const unsigned int vme_off) { // vector
    bool patsnap_enabled, dec_enabled;
    
    switch (mem) {
    case PATTERN_SNAPSHOT:
      if ((m_status = PRMEnableRead(patsnap_enabled)) != SUCCESS) {
	CERR("ALTI: reading PATTERN / SNAPSHOT generation enable", "");
	return (m_status);
      }
      if (patsnap_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("PATSNAP");
      if ((m_status = m_alti->MEM_SharedAltiRam_Write(data, vme_off)) != 0) {
	CERR("ALTI: writing \"%s\" memory in single cycle, index 0x%08x, size 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, data.size());
	return (m_status);
      }
      break;
    case TTC_DECODER_COMMAND:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("SPY");
      if ((m_status = m_alti->MEM_SharedAltiRam_Write(data, vme_off)) != 0) {
	CERR("ALTI: writing \"%s\" memory in single cycle, index 0x%08x, size 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, data.size());
	return (m_status);
      }
      break;
    case TTC_DECODER_TIMESTAMP:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("REF");
      if ((m_status = m_alti->MEM_SharedAltiRam_Write(data, vme_off)) != 0) {
	CERR("ALTI: writing \"%s\" memory in single cycle, index 0x%08x, size 0x%08x", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, data.size());
	return (m_status);
      }
      break;
    default:
      break;
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MEMDataRead(const ALTI_RAM_MEMORY mem, RCD::CMEMSegment *seg, const unsigned int size, const unsigned int vme_off, const unsigned int seg_off) { // block
    bool patsnap_enabled, dec_enabled;

    switch (mem) {
    case PATTERN_SNAPSHOT:
      if ((m_status = PRMEnableRead(patsnap_enabled)) != SUCCESS) {
	CERR("ALTI: reading PATTERN / SNAPSHOT generation enable", "");
	return (m_status);
      }
      if (patsnap_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("PATSNAP");
      if ((m_status = m_alti->MEM_SharedAltiRam_Read(seg, size, vme_off, seg_off)) != 0) {
	CERR("ALTI: reading \"%s\" memory in block cycle, index 0x%08x, size 0x%08x, segment offset", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, size, seg_off);
	return (m_status);
      }
      break;
    case TTC_DECODER_COMMAND:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("SPY");
      if ((m_status = m_alti->MEM_SharedAltiRam_Read(seg, size, vme_off, seg_off)) != 0) {
	CERR("ALTI: reading \"%s\" memory in block cycle, index 0x%08x, size 0x%08x, segment offset", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, size, seg_off);
	return (m_status);
      }
      break;
    case TTC_DECODER_TIMESTAMP:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("REF");
      if ((m_status = m_alti->MEM_SharedAltiRam_Read(seg, size, vme_off, seg_off)) != 0) {
	CERR("ALTI: reading \"%s\" memory in block cycle, index 0x%08x, size 0x%08x, segment offset", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, size, seg_off);
	return (m_status);
      }
      break;
    default:
      break;
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MEMDataWrite(const ALTI_RAM_MEMORY mem, RCD::CMEMSegment *seg, const unsigned int size, const unsigned int vme_off, const unsigned int seg_off) { // block
    bool patsnap_enabled, dec_enabled;

    switch (mem) {
    case PATTERN_SNAPSHOT:
      if ((m_status = PRMEnableRead(patsnap_enabled)) != SUCCESS) {
	CERR("ALTI: reading PATTERN / SNAPSHOT generation enable", "");
	return (m_status);
      }
      if (patsnap_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("PATSNAP");
      if ((m_status = m_alti->MEM_SharedAltiRam_Write(seg, size, vme_off, seg_off)) != 0) {
	CERR("ALTI: writing \"%s\" memory in block cycle, index 0x%08x, size 0x%08x, segment offset", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, size, seg_off);
	return (m_status);
      }
      break;
    case TTC_DECODER_COMMAND:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("SPY");
      if ((m_status = m_alti->MEM_SharedAltiRam_Write(seg, size, vme_off, seg_off)) != 0) {
	CERR("ALTI: writing \"%s\" memory in block cycle, index 0x%08x, size 0x%08x, segment offset", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, size, seg_off);
	return (m_status);
      }
      break;
    case TTC_DECODER_TIMESTAMP:
      if ((m_status = DECEnableRead(dec_enabled)) != SUCCESS) {
	CERR("ALTI: reading DEC enable", "");
	return (m_status);
      }
      if (dec_enabled) {
	CERR("ALTI: reading \"%s\" memory while it is busy", ALTI_RAM_MEMORY_NAME[mem].c_str());
	m_status = ALTI_RAM_MEMORY_BUSY;
	return (m_status);
      }
      MEMSelectWrite("REF");
      if ((m_status = m_alti->MEM_SharedAltiRam_Write(seg, size, vme_off, seg_off)) != 0) {
	CERR("ALTI: writing \"%s\" memory in block cycle, index 0x%08x, size 0x%08x, segment offset", ALTI_RAM_MEMORY_NAME[mem].c_str(), vme_off, size, seg_off);
	return (m_status);
      }
      break;
    default:
      break;
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMRead(RCD::CMEMSegment *segment, u_int start, u_int stop) { // read pattern generation memory
  if(start == 0) {
    if ((m_status = PRMStartAddressRead(start)) != 0) {
      CERR("ALTI: reading pattern generation start address", "");
      return (m_status);
    }
  }
  if(stop == 0) {
    if ((m_status = PRMStopAddressRead(stop)) != 0) {
      CERR("ALTI: reading pattern generation stop address", "");
      return (m_status);
    }
  }  
  if (start > stop) {
    m_status = SUCCESS;
    return (m_status);
  }
  
  if ((m_status = MEMDataRead(ALTI_RAM_MEMORY::PATTERN_SNAPSHOT, segment, stop - start + 1, start, start)) != 0) {
    CERR("ALTI: reading pattern generation memory in block cycle", "");
    return (m_status);
  }  
  
  std::string readable;
  unsigned int indx; 
  unsigned int *data = (unsigned int *) segment->VirtualAddress();
  
  int format = 1;
  switch (format) {
  case 1: // human-readable (original)
    std::cout << "#---------------------------\n";
    std::cout << "#                      M\n";
    std::cout << "#                      u\n";
    std::cout << "#                      l\n";
    std::cout << "#                      t\n";
    std::cout << "#                      i\n";
    std::cout << "#                      p\n";
    std::cout << "#                      l\n";
    std::cout << "#   CCC                i\n";
    std::cout << "#  BRRR    T BBBB TTT  c\n";
    std::cout << "#O UEEE    T GGGG TTTL i\n";
    std::cout << "#R SQQQ    Y OOOO RRR1 t\n";
    std::cout << "#B Y210    P 3210 321A y\n";
    std::cout << "#---------------------------\n";
    for (indx = start; indx <= stop; indx++) {
      MEMSnapshotToString(data[indx], readable);
      std::cout << readable << "\n";
    }
    std::cout << "#---------------------------\n";
    break;
  default:
    break;
  }
  
  //outf.close();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMReadFile(RCD::CMEMSegment *segment, std::ofstream &outf, u_int start, u_int stop) { // read pattern generation memory
  if(start == 0) {
    if ((m_status = PRMStartAddressRead(start)) != 0) {
      CERR("ALTI: reading pattern generation start address", "");
      return (m_status);
    }
  }
  if(stop == 0) {
    if ((m_status = PRMStopAddressRead(stop)) != 0) {
      CERR("ALTI: reading pattern generation stop address", "");
      return (m_status);
    }
  }  
  if (start > stop) {
    m_status = SUCCESS;
    return (m_status);
  }
  
  if ((m_status = MEMDataRead(ALTI_RAM_MEMORY::PATTERN_SNAPSHOT, segment, stop - start + 1, start, start)) != 0) {
    CERR("ALTI: reading pattern generation memory in block cycle", "");
    return (m_status);
  }  
  
  std::string readable;
  unsigned int indx; 
  unsigned int *data = (unsigned int *) segment->VirtualAddress();
  
  int format = 1;
  switch (format) {
  case 1: // human-readable (original)
    outf << "#---------------------------\n";
    outf << "#                      M\n";
    outf << "#                      u\n";
    outf << "#                      l\n";
    outf << "#                      t\n";
    outf << "#                      i\n";
    outf << "#                      p\n";
    outf << "#                      l\n";
    outf << "#   CCC                i\n";
    outf << "#  BRRR    T BBBB TTT  c\n";
    outf << "#O UEEE    T GGGG TTTL i\n";
    outf << "#R SQQQ    Y OOOO RRR1 t\n";
    outf << "#B Y210    P 3210 321A y\n";
    outf << "#---------------------------\n";
    for (indx = start; indx <= stop; indx++) {
      MEMSnapshotToString(data[indx], readable);
      outf << readable << "\n";
    }
    outf << "#---------------------------\n";
    break;
  default:
    break;
  }
  
  outf.close();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMWriteFile(RCD::CMEMSegment *segment, std::ifstream &inf) { // write pattern generation memory from file
    unsigned int xdat, indx, i;
    unsigned int* data = (unsigned int*) segment->VirtualAddress();

    indx = 0;
    std::string line;
    std::vector<unsigned int> pattern;

    while (std::getline(inf, line)) {
        if (line.empty()) continue;
        if (line[0] == '#' || line[0] == '-') continue; // ignore comment lines starting with # or -

        if ((m_status = MEMStringToPattern(line, pattern, ALTI_LINE)) != 0) {
            CERR("ALTI: parsing line from the pattern file", "");
            return (m_status);
        }
        for (i = 0; (i < pattern.size()) && (indx < MAX_MEMORY); i++) {
            xdat = pattern[i];
            data[indx] = xdat & ALTI::MASK_MEM_SHAREDALTIRAM;
            indx++;
        }
    }
    if ((m_status = PRMStartAddressWrite(0)) != 0) {
        CERR("ALTI: writing pattern generation start address 0x%08x", 0);
        return (m_status);
    }
    if ((m_status = PRMStopAddressWrite(indx - 1)) != 0) {
        CERR("ALTI: writing pattern generation stop address 0x%08x", indx - 1);
        return (m_status);
    }

   
    if (indx < MAX_MEMORY) { // fill the next entry with zero
        xdat = 0;
        data[indx] = xdat & ALTI::MASK_MEM_SHAREDALTIRAM;
        indx++;
    }
    if ((m_status = MEMDataWrite(PATTERN_SNAPSHOT, segment, indx, 0, 0)) != 0) {
        CERR("ALTI: writing pattern generation memory in block cycle", "");
        return (m_status);
    }
   

    inf.close();
        
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMReadFile(std::ofstream &outf, u_int start, u_int stop) { // read pattern generation memory
  if(start == 0) {
    if ((m_status = PRMStartAddressRead(start)) != 0) {
      CERR("ALTI: reading pattern generation start address", "");
      return (m_status);
    }
  }
  if(stop == 0) {
    if ((m_status = PRMStopAddressRead(stop)) != 0) {
      CERR("ALTI: reading pattern generation stop address", "");
      return (m_status);
    }
  }  
  if (start > stop) {
    m_status = SUCCESS;
    return (m_status);
  }
  
  
  if ((m_status = MEMDataRead(ALTI_RAM_MEMORY::PATTERN_SNAPSHOT, m_segment, stop - start + 1, start, start)) != 0) {
    CERR("ALTI: reading pattern generation memory in block cycle", "");
    return (m_status);
  }  
  
  std::string readable;
  unsigned int indx; 
  unsigned int *data = (unsigned int *) m_segment->VirtualAddress();
  
  int format = 1;
  switch (format) {
  case 1: // human-readable (original)
    outf << "#---------------------------\n";
    outf << "#                      M\n";
    outf << "#                      u\n";
    outf << "#                      l\n";
    outf << "#                      t\n";
    outf << "#                      i\n";
    outf << "#                      p\n";
    outf << "#                      l\n";
    outf << "#   CCC                i\n";
    outf << "#  BRRR    T BBBB TTT  c\n";
    outf << "#O UEEE    T GGGG TTTL i\n";
    outf << "#R SQQQ    Y OOOO RRR1 t\n";
    outf << "#B Y210    P 3210 321A y\n";
    outf << "#---------------------------\n";
    for (indx = start; indx <= stop; indx++) {
      MEMSnapshotToString(data[indx], readable);
      outf << readable << "\n";
    }
    outf << "#---------------------------\n";
    break;
  default:
    break;
  }
  
  outf.close();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PRMWriteFile(std::ifstream &inf) { // write pattern generation memory from file

  unsigned int xdat, indx, i;
  unsigned int* data = (unsigned int*) m_segment->VirtualAddress();
  
  indx = 0;
  std::string line;
  std::vector<unsigned int> pattern;
  
  while (std::getline(inf, line)) {
    if (line.empty()) continue;
    if (line[0] == '#' || line[0] == '-') continue; // ignore comment lines starting with # or -
    
    if ((m_status = MEMStringToPattern(line, pattern, ALTI_LINE)) != 0) {
      CERR("ALTI: parsing line from the pattern file", "");
      return (m_status);
    }
    for (i = 0; (i < pattern.size()) && (indx < MAX_MEMORY); i++) {
      xdat = pattern[i];
      data[indx] = xdat & ALTI::MASK_MEM_SHAREDALTIRAM;
      indx++;
    }
  }
  if ((m_status = PRMStartAddressWrite(0)) != 0) {
    CERR("ALTI: writing pattern generation start address 0x%08x", 0);
    return (m_status);
  }
  if ((m_status = PRMStopAddressWrite(indx - 1)) != 0) {
    CERR("ALTI: writing pattern generation stop address 0x%08x", indx - 1);
    return (m_status);
  }
    
  if (indx < MAX_MEMORY) { // fill the next entry with zero
    xdat = 0;
    data[indx] = xdat & ALTI::MASK_MEM_SHAREDALTIRAM;
    indx++;
  }
  if ((m_status = MEMDataWrite(PATTERN_SNAPSHOT, m_segment, indx, 0, 0)) != 0) {
    CERR("ALTI: writing pattern generation memory in block cycle", "");
    return (m_status);
  }
  
  inf.close();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MEMSnapshotToString(unsigned int snapshot, std::string &readable) {
    std::ostringstream buf;

    // try to remove numbers afterwards, masks and shifts can be found in the snapshot bitstring

    buf << ' ';

    buf << ((snapshot & MEM_MASK[ORB]) ? ('1') : ('0')); // ORB

    buf << ' ';

    buf << ((snapshot & MEM_MASK[BUSY]) ? ('1') : ('0')); // BUSY
    buf << ((snapshot & MEM_MASK[CALREQ2]) ? ('1') : ('0')); // CALREQ[2]
    buf << ((snapshot & MEM_MASK[CALREQ1]) ? ('1') : ('0')); // CALREQ[1]
    buf << ((snapshot & MEM_MASK[CALREQ0]) ? ('1') : ('0')); // CALREQ[0]

    buf << ' ';

    buf << "0x" << std::setfill('0') << std::setw(2) << std::hex << ((snapshot & (MEM_MASK[TTYP0] + MEM_MASK[TTYP1] + MEM_MASK[TTYP2] + MEM_MASK[TTYP3] + MEM_MASK[TTYP4] + MEM_MASK[TTYP5] + MEM_MASK[TTYP6] + MEM_MASK[TTYP7])) >> 8); // TTYP

    buf << ' ';

    buf << ((snapshot & MEM_MASK[BGO3]) ? ('1') : ('0')); // BGO3
    buf << ((snapshot & MEM_MASK[BGO2]) ? ('1') : ('0')); // BGO2
    buf << ((snapshot & MEM_MASK[BGO1]) ? ('1') : ('0')); // BGO1
    buf << ((snapshot & MEM_MASK[BGO0]) ? ('1') : ('0')); // BGO0

    buf << ' ';

    buf << ((snapshot & MEM_MASK[TTR3]) ? ('1') : ('0')); // TTR3
    buf << ((snapshot & MEM_MASK[TTR2]) ? ('1') : ('0')); // TTR2
    buf << ((snapshot & MEM_MASK[TTR1]) ? ('1') : ('0')); // TTR1
    buf << ((snapshot & MEM_MASK[L1A])  ? ('1') : ('0')); // L1A

    buf << ' ';

    //buf << std::dec << ((snapshot & MEM_MULT_MASK) >> MEM_SIGNAL_BITS); // Multiplicity
    buf << std::dec << (((snapshot & MEM_MULT_MASK) >> MEM_SIGNAL_BITS) + 1); // Multiplicity

    readable = buf.str();

    return 0;
}

//------------------------------------------------------------------------------

int AltiModule::MEMStringToPattern(std::string &readable, std::vector<unsigned int> &pattern, const SNAPSHOT_TYPE lineType, uint8_t ttypw[4]) {
    pattern.clear();

    int i;
    std::stringstream buf;
    buf << readable;

    // read pattern
    char orb;
    char busy, creq[SNAPSHOT::CALREQ_NUMBER];
    unsigned int ttyp;
    char bgo[SNAPSHOT::BGO_NUMBER];
    char ttr[SNAPSHOT::TTR_NUMBER], l1a;
    char inhib;
    char ttypc[2];
    int multiplicity;
    switch (lineType) {
        case ALTI_LINE:

            buf >> orb;
            if (orb < '0' || orb > '1') {
                CERR("ALTI: ORB not binary in %s pattern %s", SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                return -1;
            }

            buf >> busy >> creq[2] >> creq[1] >> creq[0];
            if (busy < '0' || busy > '1') {
                CERR("ALTI: BUSY not binary in %s pattern %s", SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                return -1;
            }
            for (i = 2; i >= 0; i--) {
                if (creq[i] < '0' || creq[i] > '1') {
                    CERR("ALTI: CALREQ[%d] not binary in %s pattern %s", i, SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                    return -1;
                }
            }

            buf >> std::hex >> ttyp;

            buf >> bgo[3] >> bgo[2] >> bgo[1] >> bgo[0];
            for (i = 3; i >= 0; i--) {
                if (bgo[i] < '0' || bgo[i] > '1') {
                    CERR("ALTI: BGO[%d] not binary in %s pattern %s", i, SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                    return -1;
                }
            }

            buf >> ttr[2] >> ttr[1] >> ttr[0] >> l1a;
            for (i = 2; i >= 0; i--) {
                if (ttr[i] < '0' || ttr[i] > '1') {
                    CERR("ALTI: TTR[%d] not binary in %s pattern %s", i + 1, SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                    return -1;
                }
            }
            if (l1a < '0' || l1a > '1') {
                CERR("ALTI: L1A not binary in %s pattern %s", SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                return -1;
            }

            buf >> std::dec >> multiplicity;
            
            if (multiplicity <= 0) {
                CERR("ALTI: multiplicity not positive in %s pattern %s", SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                return -1;
            }

            break;
        case LTP_LINE:

            buf >> busy;
            if (busy < '0' || busy > '1') {
                CERR("ALTI: BUSY not binary in %s pattern %s", SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                return -1;
            }
            //std::printf("BUSY: %c\n", busy);

            buf >> inhib;
            if (inhib < '0' || inhib > '1') {
                CERR("ALTI: INHIBIT not binary in %s pattern %s", SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                return -1;
            }
            //std::printf("INHIBIT: %c\n", inhib);

            buf >> creq[2] >> creq[1] >> creq[0];
            for (i = 2; i >= 0; i--) {
                if (creq[i] < '0' || creq[i] > '1') {
                    CERR("ALTI: CALREQ[%d] not binary in %s pattern %s", i, SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                    return -1;
                }
            }
            //std::printf("CALREQ[2..0]: %c%c%c\n", creq[2], creq[1], creq[0]);

            buf >> bgo[3] >> bgo[2] >> bgo[1] >> bgo[0];
            for (i = 3; i >= 0; i--) {
                if (bgo[i] < '0' || bgo[i] > '1') {
                    CERR("ALTI: BGO[%d] not binary in %s pattern %s", i, SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                    return -1;
                }
            }
            //std::printf("BGO[3..0]: %c%c%c%c\n", bgo[3], bgo[2], bgo[1], bgo[0]);

            buf >> ttypc[1] >> ttypc[0];
            for (i = 1; i >= 0; i--) {
                if (ttypc[i] < '0' || ttypc[i] > '1') {
                    CERR("ALTI: TTYP[%d] not binary in %s pattern %s", i, SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                    return -1;
                }
            }
            //std::printf("TTYP[1..0]: %c%c\n", ttypc[1], ttypc[0]);
            ttyp = ttypw[(ttypc[1] - '0')*2 + (ttypc[0] - '0')*1]; // problem, how to translate to 32b word?

            buf >> ttr[2] >> ttr[1] >> ttr[0] >> l1a;
            for (i = 2; i >= 0; i--) {
                if (ttr[i] < '0' || ttr[i] > '1') {
                    CERR("ALTI: TTR[%d] not binary in %s pattern %s", i + 1, SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                    return -1;
                }
            }
            if (l1a < '0' || l1a > '1') {
                CERR("ALTI: L1A not binary in %s pattern %s", SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                return -1;
            }
            //std::printf("TTR[3..1]: %c%c%c\n", ttr[2], ttr[1], ttr[0]);
            //std::printf("L1A: %c\n", l1a);

            buf >> orb;
            if (orb < '0' || orb > '1') {
                CERR("ALTI: ORB not binary in %s pattern %s", SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                return -1;
            }
            //std::printf("ORB: %c\n", orb);

            buf >> std::dec >> multiplicity;
            if (multiplicity <= 0) {
                CERR("ALTI: multiplicity not positive in %s pattern %s", SNAPSHOT_TYPE_NAME[lineType].c_str(), readable.c_str());
                return -1;
            }
            //std::printf("mult: %d\n", multiplicity);

            break;
        default:
            break;
    }

    // transform into pattern generation data entry/entries

    unsigned int data = 0, duration;
    data |= ((l1a - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[L1A]));
    data |= ((ttr[0] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[TTR1]));
    data |= ((ttr[1] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[TTR2]));
    data |= ((ttr[2] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[TTR3]));
    data |= ((bgo[0] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[BGO0]));
    data |= ((bgo[1] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[BGO1]));
    data |= ((bgo[2] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[BGO2]));
    data |= ((bgo[3] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[BGO3]));
    data |= (ttyp << AltiCommon::getFirstSetBitPos(MEM_MASK[TTYP0]));
    data |= ((creq[0] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[CALREQ0]));
    data |= ((creq[1] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[CALREQ1]));
    data |= ((creq[2] - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[CALREQ2]));
    data |= ((busy - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[BUSY]));
    data |= ((orb - '0') << AltiCommon::getFirstSetBitPos(MEM_MASK[ORB]));

    switch (lineType) {
        case ALTI_LINE:

            // PG input: written to memory counting from 0, i.e. 0 means duration of 1 
            while (multiplicity > 0) {
                //std::cout << "mult: " << multiplicity << std::endl;
                //duration = min(multiplicity, 0xff);
                duration = ((((unsigned int) multiplicity) <= MEM_MULT_MAX) ? (multiplicity - 1) : (MEM_MULT_MAX));

                pattern.push_back((duration << MEM_SIGNAL_BITS) | data);
                //std::printf("pattern: 0x%08x\n", pattern[pattern.size() - 1]);

                multiplicity -= (duration + 1);
            }

            break;
        case LTP_LINE:

            // PG input: written to memory as-is, i.e. 0 means duration of 1 
            while (multiplicity > 0) {
                //std::cout << "mult: " << multiplicity << std::endl;
                //duration = min(multiplicity, 0xff);
                duration = ((((unsigned int) multiplicity) <= MEM_MULT_MAX) ? (multiplicity - 1) : (MEM_MULT_MAX));

                pattern.push_back((duration << MEM_SIGNAL_BITS) | data);
                //std::printf("pattern: 0x%08x\n", pattern[pattern.size() - 1]);

                multiplicity -= (duration + 1);
            }

            break;
        default:
            break;
    }

    return 0;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterEnableRead(const unsigned int tx, bool &enabled) {
    if (tx >= TRANSMITTER_NUMBER) {
        CERR("ALTI: wrong TTC TX number chosen", "");
        m_status = WRONGPAR;
        return (m_status);
    }

    ALTI_CTL_SFPCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CTL_SFPControl_Read(bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }

    enabled = bs.TxEnable(tx);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterEnableRead(std::vector<bool> &enabled) {
    enabled.clear();

    ALTI_CTL_SFPCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CTL_SFPControl_Read(bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }

    unsigned int i;
    std::string s;
    for (i = 0; i < TRANSMITTER_NUMBER; i++) {
      enabled.push_back(bs.TxEnable(i));
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterFaultRead(const unsigned int tx, bool &fault) {
    if (tx >= TRANSMITTER_NUMBER) {
        CERR("ALTI: wrong TTC TX number chosen", "");
        m_status = WRONGPAR;
        return (m_status);
    }

    ALTI_CTL_SFPSTATUS_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CTL_SFPStatus_Read(bs)) != 0) {
        CERR("ALTI: reading TTC status register", "");
        return (m_status);
    }

    fault = bs.TxFault(tx);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterFaultRead(std::vector<bool> &fault) { // all
    fault.clear();

    ALTI_CTL_SFPSTATUS_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CTL_SFPStatus_Read(bs)) != 0) {
        CERR("ALTI: reading TTC status register", "");
        return (m_status);
    }

    unsigned int i;
    std::string s;
    for (i = 0; i < TRANSMITTER_NUMBER; i++) {
      fault.push_back(bs.TxFault(i));
    }

    return (m_status);
}


//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterFaultClear() { // all
  
  ALTI_CTL_SFPCONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_SFPControl_Read(bs)) != 0) {
    CERR("ALTI: reading TTC control register", "");
    return (m_status);
  }
  
  bs.StatusFlagsReset(true);
  
  // WRITE
  if ((m_status = m_alti->CTL_SFPControl_Write(bs)) != 0) {
    CERR("ALTI: writing TTC control register", "");
    return (m_status);
  }
  
  return (m_status);
}



//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterEnableWrite(const unsigned int tx, const bool enable) {
    if (tx >= TRANSMITTER_NUMBER) {
        CERR("ALTI: wrong TTC TX number chosen", "");
        m_status = WRONGPAR;
        return (m_status);
    }

    ALTI_CTL_SFPCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CTL_SFPControl_Read(bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    // MODIFY
    bs.TxEnable(tx, enable);
    // WRITE
    if ((m_status = m_alti->CTL_SFPControl_Write(bs)) != 0) {
        CERR("ALTI: writing TTC control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterEnableWrite(const bool enable) { // all
    ALTI_CTL_SFPCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CTL_SFPControl_Read(bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    // MODIFY
    unsigned int i;
    for (i = 0; i < TRANSMITTER_NUMBER; i++) {
      bs.TxEnable(i, enable);
    }
    // WRITE
    if ((m_status = m_alti->CTL_SFPControl_Write(bs)) != 0) {
        CERR("ALTI: writing TTC control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterEnableWrite(const std::vector<bool> &enable) { // all
    ALTI_CTL_SFPCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CTL_SFPControl_Read(bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }

    unsigned int i;
    for (i = 0; i < TRANSMITTER_NUMBER; i++) {
        // MODIFY
        bs.TxEnable(i, enable[i]);
    }

    // WRITE
    if ((m_status = m_alti->CTL_SFPControl_Write(bs)) != 0) {
        CERR("ALTI: writing TTC control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterDelayRead(const unsigned int tx, u_int &delay) {
    if (tx >= TRANSMITTER_NUMBER) {
        CERR("ALTI: wrong TTC TX number chosen", "");
        m_status = WRONGPAR;
        return (m_status);
    }
    
    ALTI_CTL_TTCTXDELAYCONTROL_BITSTRING bs;
    if( tx <= 5) {
      // READ
      if ((m_status = m_alti->CTL_TtcTxDelayControl_Read(0, bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
      }
      delay = bs.Delay(tx);
    }
    else {
      // READ
      if ((m_status = m_alti->CTL_TtcTxDelayControl_Read(1, bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
      }
      delay = bs.Delay(tx-6);
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterDelayRead(std::vector<u_int> &delay) {
    delay.clear();
    
    ALTI_CTL_TTCTXDELAYCONTROL_BITSTRING bs_up;
    ALTI_CTL_TTCTXDELAYCONTROL_BITSTRING bs_bottom;
    
    // READ
    if ((m_status = m_alti->CTL_TtcTxDelayControl_Read(0, bs_up)) != 0) {
      CERR("ALTI: reading TTC control register", "");
      return (m_status);
    }   
    if ((m_status = m_alti->CTL_TtcTxDelayControl_Read(1, bs_bottom)) != 0) {
      CERR("ALTI: reading TTC control register", "");
      return (m_status);
    }
    
    for (u_int i = 0; i < TRANSMITTER_NUMBER; i++) {
      if(i <= 5) delay.push_back(bs_up.Delay(i));
      else       delay.push_back(bs_bottom.Delay(i-6));
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterDelayWrite(const unsigned int tx, const u_int delay) {
    if (tx >= TRANSMITTER_NUMBER) {
        CERR("ALTI: wrong TTC TX number chosen", "");
        m_status = WRONGPAR;
        return (m_status);
    }
    
    ALTI_CTL_TTCTXDELAYCONTROL_BITSTRING bs;
    if( tx <= 5) {
      // READ
      if ((m_status = m_alti->CTL_TtcTxDelayControl_Read(0, bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
      }
      // MODIFY 
      bs.Delay(tx, delay);
      // WRITE
      if ((m_status = m_alti->CTL_TtcTxDelayControl_Write(0, bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
      }
    }
    else {
      // READ
      if ((m_status = m_alti->CTL_TtcTxDelayControl_Read(1, bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
      }
      // MODIFY 
      bs.Delay(tx-6, delay);
      // WRITE
      if ((m_status = m_alti->CTL_TtcTxDelayControl_Write(1, bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
      }
    }   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTransmitterDelayWrite(std::vector<u_int> delay) {
    
    ALTI_CTL_TTCTXDELAYCONTROL_BITSTRING bs_up;
    ALTI_CTL_TTCTXDELAYCONTROL_BITSTRING bs_bottom;
    
    // READ
    if ((m_status = m_alti->CTL_TtcTxDelayControl_Read(0, bs_up)) != 0) {
      CERR("ALTI: reading TTC control register", "");
      return (m_status);
    }   
    if ((m_status = m_alti->CTL_TtcTxDelayControl_Read(1, bs_bottom)) != 0) {
      CERR("ALTI: reading TTC control register", "");
      return (m_status);
    }
    
    // modify
    for (u_int i = 0; i < TRANSMITTER_NUMBER; i++) { 
      if(i <= 5) bs_up.Delay(i, delay[i]);	  
      else       bs_bottom.Delay(i-6, delay[i]);
    }

    // WRITE
    if ((m_status = m_alti->CTL_TtcTxDelayControl_Write(0, bs_up)) != 0) {
      CERR("ALTI: reading TTC control register", "");
      return (m_status);
    }
    if ((m_status = m_alti->CTL_TtcTxDelayControl_Write(1, bs_bottom)) != 0) {
      CERR("ALTI: reading TTC control register", "");
      return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCL1ASourceRead(bool &l1a, bool &ttr1, bool &ttr2, bool &ttr3, bool &calreq, bool &pattern, bool &minictp) {

  ALTI_CTL_TTCTRIGGERSELECT_BITSTRING bs; 
  
  // READ
  if ((m_status = m_alti->CTL_TtcTriggerSelect_Read(bs)) != SUCCESS ) {
    CERR("ALTI: reading CTP TTC trigger select register", "");
    return (m_status);
  }

  l1a     = bs.L1aEnable();
  ttr1    = bs.TTrEnable(0);
  ttr2    = bs.TTrEnable(1);
  ttr3    = bs.TTrEnable(2);
  calreq  = bs.CalibSignalEnable();
  pattern = bs.PatternEnable();
  minictp = bs.MiniCTPEnable();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCL1ASourceWrite(const bool l1a, const bool ttr1, const bool ttr2, const bool ttr3, const bool calreq, const bool pattern, const bool minictp) {
  
    ALTI_CTL_TTCTRIGGERSELECT_BITSTRING bs; 
    
    // READ
    if ((m_status = m_alti->CTL_TtcTriggerSelect_Read(bs)) != SUCCESS ) {
      CERR("ALTI: reading CTP TTC trigger select register", "");
      return (m_status);
    }
	
    // MODIFY
    bs.L1aEnable(l1a); 
    bs.TTrEnable(0, ttr1);
    bs.TTrEnable(1, ttr2);
    bs.TTrEnable(2, ttr3);
    bs.CalibSignalEnable(calreq);
    bs.PatternEnable(pattern);
    bs.MiniCTPEnable(minictp);

    // WRITE
    if ((m_status = m_alti->CTL_TtcTriggerSelect_Write(bs)) != SUCCESS ) {
      CERR("ALTI: writing CTP TTC trigger select register", "");
      return (m_status);
    } 
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCOrbitSourceRead(std::string &src) {

  ALTI_CTL_TTCTRIGGERSELECT_BITSTRING bs; 
  
  // READ
  if ((m_status = m_alti->CTL_TtcTriggerSelect_Read(bs)) != SUCCESS ) {
    CERR("ALTI: reading CTP TTC trigger select register", "");
    return (m_status);
  }
  src = bs.Orbit();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCOrbitSourceWrite(const std::string src) {
  
    ALTI_CTL_TTCTRIGGERSELECT_BITSTRING bs; 
    
    // READ
    if ((m_status = m_alti->CTL_TtcTriggerSelect_Read(bs)) != SUCCESS ) {
      CERR("ALTI: reading CTP TTC trigger select register", "");
      return (m_status);
    }
	
    // MODIFY
    bs.Orbit(src);

    // WRITE
    if ((m_status = m_alti->CTL_TtcTriggerSelect_Write(bs)) != SUCCESS ) {
      CERR("ALTI: writing CTP TTC trigger select register", "");
      return (m_status);
    } 
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoSourceRead(const u_int bgo, std::string &src) {

  ALTI_CTL_TTCTRIGGERSELECT_BITSTRING bs; 
  
  // READ
  if ((m_status = m_alti->CTL_TtcTriggerSelect_Read(bs)) != SUCCESS ) {
    CERR("ALTI: reading CTP TTC trigger select register", "");
    return (m_status);
  }
  src = bs.Bgo(bgo);

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoSourceWrite(const u_int bgo, const std::string src) {
  
    ALTI_CTL_TTCTRIGGERSELECT_BITSTRING bs; 
    
    // READ
    if ((m_status = m_alti->CTL_TtcTriggerSelect_Read(bs)) != SUCCESS ) {
      CERR("ALTI: reading CTP TTC trigger select register", "");
      return (m_status);
    }
	
    // MODIFY
    bs.Bgo(bgo, src);

    // WRITE
    if ((m_status = m_alti->CTL_TtcTriggerSelect_Write(bs)) != SUCCESS ) {
      CERR("ALTI: writing CTP TTC trigger select register", "");
      return (m_status);
    } 
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTtypSourceRead(std::string &src) {

  ALTI_CTL_TTCTRIGGERSELECT_BITSTRING bs; 
  
  // READ
  if ((m_status = m_alti->CTL_TtcTriggerSelect_Read(bs)) != SUCCESS ) {
    CERR("ALTI: reading CTP TTC trigger select register", "");
    return (m_status);
  }
  src = bs.TriggerType();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTtypSourceWrite(const std::string src) {
  
    ALTI_CTL_TTCTRIGGERSELECT_BITSTRING bs; 
    
    // READ
    if ((m_status = m_alti->CTL_TtcTriggerSelect_Read(bs)) != SUCCESS ) {
      CERR("ALTI: reading CTP TTC trigger select register", "");
      return (m_status);
    }
	
    // MODIFY
    bs.TriggerType(src);

    // WRITE
    if ((m_status = m_alti->CTL_TtcTriggerSelect_Write(bs)) != SUCCESS ) {
      CERR("ALTI: writing CTP TTC trigger select register", "");
      return (m_status);
    } 
   
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECEnableRead(bool &enabled) {
    ALTI_TRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->TRM_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading TTC decoder snapshot memory control register", "");
        return (m_status);
    }
    
    enabled = (bs.Mode() == "SPY" ? true : false);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECEnableWrite(const bool enabled) {
    ALTI_TRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->TRM_Control_Read(bs)) != 0 ) {
        CERR("ALTI: reading TTC decoder snapshot memory control register", "");
        return (m_status);
    }
    // MODIFY
    bs.Mode(enabled ? "SPY" : "VME");
    // WRITE
    if ((m_status = m_alti->TRM_Control_Write(bs)) != 0 ) {
        CERR("ALTI: writing TTC decoder snapshot memory control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECCurrentAddressRead(unsigned int &cur) {
    if ((m_status = m_alti->TRM_RamCurrentSpyAddr_Read(cur)) != 0) {
        CERR("ALTI: reading TTC decoder current address", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECCurrentAddressWrite(unsigned int cur) {
    if ((m_status = m_alti->TRM_RamCurrentSpyAddr_Write(cur)) != 0) {
        CERR("ALTI: doesn't exist anymore", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------
int AltiModule::DECStatusRead(bool &MemoryLooped, bool &MultiCounterFull, bool &RamFull, bool &DecoderReady, bool &CommunicationError, bool &singleBitError, bool &doubleBitError, bool &CdrLosError, bool &CdrLolError, bool &CdrPllLock, bool &RunStatus) {
    ALTI_TRM_STATUS_BITSTRING bs;
    ALTI_CLK_STATUS_BITSTRING cs;

    // READ
    if ((m_status = m_alti->TRM_Status_Read(bs)) != 0 ) {
        CERR("ALTI: reading TTC decoder status register", "");
        return (m_status);
    }

    if ((m_status = m_alti->CLK_Status_Read(cs)) != 0 ) {
        CERR("ALTI: reading CLK status register", "");
        return (m_status);
    }

    MemoryLooped = bs.MemoryLooped();
    MultiCounterFull = bs.MultiCounterFull();
    RamFull = bs.RamFull();
    DecoderReady = bs.DecoderReady();
    CommunicationError = bs.CommunicationError();
    singleBitError = bs.SingleBitError();
    doubleBitError = bs.DoubleBitError();
    CdrLosError = cs.CdrClockLOS();
    CdrLolError = cs.CdrClockLOL();
    CdrPllLock = cs.CdrPllLock();
    RunStatus = bs.RunStatus();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECStatusClear() {
    ALTI_TRM_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->TRM_Control_Read(bs)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }

    bs.FlagsReset(true);

    // WRITE
    if ((m_status = m_alti->TRM_Control_Write(bs)) != 0) {
        CERR("ALTI: writing TTC control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECReset() {
    ALTI_TRM_CONTROL_BITSTRING cs;

    // WRITE
    if ((m_status = DECCurrentAddressWrite(0)) != SUCCESS ) {
        CERR("ALTI: writing TTC decoder current address", "");
        return (m_status);
    }
    // READ
    if ((m_status = m_alti->TRM_Control_Read(cs)) != SUCCESS ) {
        CERR("ALTI: reading TTC decoder snapshot memory control register", "");
        return (m_status);
    }
    // MODIFY
    cs.FlagsReset(true);
    cs.CounterReset(true);
    cs.SignalCounterReset(true);
    // WRITE
    if ((m_status = m_alti->TRM_Control_Write(cs)) != SUCCESS ) {
        CERR("ALTI: writing TTC decoder snapshot memory control register", "");
        return (m_status);
    }

    // Orbit counter reset
    ALTI_TRM_ORBITCOUNTERCONTROL_BITSTRING bs;
      // READ
    if ((m_status = m_alti->TRM_OrbitCounterControl_Read(bs)) != SUCCESS ) {
        CERR("ALTI: reading TTC decoder orbit counter control register", "");
        return (m_status);
    }
    // MODIFY
    bs.ResetOrbitCounter(true);
    bs.ResetShortCounter(true);
    bs.ResetLongCounter(true);
    bs.ClearMinimumLength(true);
    bs.ClearMaximumLength(true);
    // WRITE
    if ((m_status = m_alti->TRM_OrbitCounterControl_Write(bs)) != SUCCESS ) {
        CERR("ALTI: writing TTC decoder orbit counter control register", "");
        return (m_status);
    }

    // full reset 
    if( (m_status = AltiResetControl(false, false, false, true)) != SUCCESS) {
      CERR("ALTI: resetting the TTC decoder", "");
      return (m_status);
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECCounterReset() {
    ALTI_TRM_CONTROL_BITSTRING cs;

    // READ
    if ((m_status = m_alti->TRM_Control_Read(cs)) != SUCCESS ) {
        CERR("ALTI: reading TTC decoder snapshot memory control register", "");
        return (m_status);
    }
    // MODIFY
    cs.CounterReset(true);
    cs.SignalCounterReset(true);
    // WRITE
    if ((m_status = m_alti->TRM_Control_Write(cs)) != SUCCESS ) {
        CERR("ALTI: writing TTC decoder snapshot memory control register", "");
        return (m_status);
    }

    // Orbit counter reset
    ALTI_TRM_ORBITCOUNTERCONTROL_BITSTRING bs;
      // READ
    if ((m_status = m_alti->TRM_OrbitCounterControl_Read(bs)) != SUCCESS ) {
        CERR("ALTI: reading TTC decoder orbit counter control register", "");
        return (m_status);
    }
    // MODIFY
    bs.ResetOrbitCounter(true);
    bs.ResetShortCounter(true);
    bs.ResetLongCounter(true);
    bs.ClearMinimumLength(true);
    bs.ClearMaximumLength(true);
    // WRITE
    if ((m_status = m_alti->TRM_OrbitCounterControl_Write(bs)) != SUCCESS ) {
        CERR("ALTI: writing TTC decoder orbit counter control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECCountersRead(u_int &long_cmd, u_int &short_cmd, u_int &l1a) {
    
  // READ
  if ((m_status = m_alti->TRM_LongCommandCounter_Read(long_cmd)) != SUCCESS ) {
    CERR("ALTI: reading TTC decoder long command counter", "");
    return (m_status);
  }
  if ((m_status = m_alti->TRM_ShortCommandCounter_Read(short_cmd)) != SUCCESS ) {
    CERR("ALTI: reading TTC decoder short command counter", "");
        return (m_status);
  }
  if ((m_status = m_alti->TRM_L1aCounter_Read(l1a)) != SUCCESS ) {
    CERR("ALTI: reading TTC decoder L1A counter", "");
    return (m_status);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECOrbitCountersRead(u_int &orb, u_int &orb_short, u_int &orb_long, u_int &min, u_int &max) {
    
if ((m_status = m_alti->TRM_OrbitCounter_Read(orb)) != SUCCESS ) {
    CERR("ALTI: reading TTC decoder orbit counter", "");
    return (m_status);
  }

  ALTI_TRM_SHORTLONGCOUNTER_BITSTRING sl;
  if ((m_status = m_alti->TRM_ShortLongCounter_Read(sl)) != SUCCESS ) {
    CERR("ALTI: reading TTC decoder short long counter", "");
    return (m_status);
  }
  orb_short = sl.ShortOrbits();
  orb_long = sl.LongOrbits();

  ALTI_TRM_ORBITLENGTH_BITSTRING ol;
  if ((m_status = m_alti->TRM_OrbitLength_Read(ol)) != SUCCESS ) {
    CERR("ALTI: reading TTC decoder orbit length", "");
    return (m_status);
  }
  min = ol.Minimum();
  max= ol.Maximum();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECLoopRead(bool &loop) {
  
  ALTI_TRM_CONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->TRM_Control_Read(bs)) != 0) {
    CERR("ALTI: reading TRM control register","");
    return(m_status);
  }
   
  loop = bs.LoopWrite();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECLoopWrite(const bool loop) {

 
  ALTI_TRM_CONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->TRM_Control_Read(bs)) != 0) {
    CERR("ALTI: reading TRM control register","");
    return(m_status);
  }
  
  // MODIFY
  bs.LoopWrite(loop);
  
  // WRITE
  if ((m_status = m_alti->TRM_Control_Write(bs)) != 0) {
    CERR("ALTI: writing to TRM control register","");
    return(m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECTriggerWordWrite(const bool enable, const short cmd, const short cmdmask, const short addrl, const short addrlmask, const short addrs, const short addrsmask, const bool l1a, const bool ext, const bool astrb, const bool bstrb, const bool errdbl, const bool errsng) {
    ALTI_TRM_TRIGGERWORDLS_BITSTRING ls;
    ALTI_TRM_TRIGGERWORDMS_BITSTRING ms;
    ALTI_TRM_TRIGGERMASKLS_BITSTRING lsmsk;
    ALTI_TRM_TRIGGERMASKMS_BITSTRING msmsk;
    ALTI_TRM_CONTROL_BITSTRING ctrl;
    // READ
    if ((m_status = m_alti->TRM_Control_Read(ctrl)) != 0) {
      CERR("ALTI: reading SMC control register","");
      return(m_status);
    }
    if ((m_status = m_alti->TRM_TriggerWordLS_Read(ls)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_TriggerWordMS_Read(ms)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_TriggerMaskLS_Read(lsmsk)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_TriggerMaskMS_Read(msmsk)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    ctrl.EnableTrigger(enable);
    ls.Command(cmd);
    ls.AddressLocal(addrs);
    ls.AddressLong(addrl);
    lsmsk.Command(cmdmask);
    lsmsk.AddressLocal(addrsmask);
    lsmsk.AddressLong(addrlmask);
    ls.L1a(l1a);
    lsmsk.L1a(l1a);
    ls.External(ext);
    lsmsk.External(ext);
    
    ms.DoubleBitError(errdbl);
    msmsk.DoubleBitError(errdbl);
    ms.SingleBitError(errsng);
    msmsk.SingleBitError(errsng);
    ms.AdrStrobe(astrb);
    msmsk.AdrStrobe(astrb);
    ms.BrcStrobe(bstrb);
    msmsk.BrcStrobe(bstrb);

    // WRITE
    if ((m_status = m_alti->TRM_Control_Write(ctrl)) != 0) {
      CERR("ALTI: writing to SMC control register","");
      return(m_status);
    }
    if ((m_status = m_alti->TRM_TriggerWordLS_Write(ls)) != 0) {
        CERR("ALTI: writing TTC control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_TriggerWordMS_Write(ms)) != 0) {
        CERR("ALTI: writing TTC control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_TriggerMaskLS_Write(lsmsk)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_TriggerMaskMS_Write(msmsk)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECTriggerWordRead(bool &enabled, short &cmd, short &addrs, short &addrl, short &cmdmask, short &addrsmask, short &addrlmask, bool &l1a, bool &ext, bool &astrb, bool &bstrb, bool &errdbl, bool &errsng) {
    ALTI_TRM_TRIGGERWORDLS_BITSTRING ls;
    ALTI_TRM_TRIGGERWORDMS_BITSTRING ms;
    ALTI_TRM_TRIGGERMASKLS_BITSTRING lsmsk;
    ALTI_TRM_TRIGGERMASKMS_BITSTRING msmsk;
    ALTI_TRM_CONTROL_BITSTRING ctrl;
    
    // READ
    if ((m_status = m_alti->TRM_Control_Read(ctrl)) != 0) {
      CERR("ALTI: reading SMC control register","");
      return(m_status);
    }
    if ((m_status = m_alti->TRM_TriggerWordLS_Read(ls)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_TriggerWordMS_Read(ms)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_TriggerMaskLS_Read(lsmsk)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    if ((m_status = m_alti->TRM_TriggerMaskMS_Read(msmsk)) != 0) {
        CERR("ALTI: reading TTC control register", "");
        return (m_status);
    }
    enabled = ctrl.EnableTrigger();
    cmd = ls.Command();
    addrs = ls.AddressLocal();
    addrl = ls.AddressLong();
    cmdmask = lsmsk.Command();
    addrsmask = lsmsk.AddressLocal();
    addrlmask = lsmsk.AddressLong();
    l1a = ls.L1a();
    ext = ls.External();
    
    errdbl = ms.DoubleBitError();
    errsng = ms.SingleBitError();
    astrb = ms.AdrStrobe();
    bstrb = ms.BrcStrobe();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::DECRead(RCD::CMEMSegment *segment0, RCD::CMEMSegment *segment1, const unsigned int start, const unsigned int num, unsigned int bcr_word, int bcr_delay) { // read TTC decoder memory
    if (start > MAX_MEMORY) {
        CERR("ALTI: invalid start index of snapshot memory", "");
        m_status = WRONGPAR;
        return (m_status);
    }
    else if (start + num > MAX_MEMORY) {
        CERR("ALTI: invalid number of data to be read from the snapshot memory", "");
        m_status = WRONGPAR;
        return (m_status);
    }
   
    if ((m_status = MEMDataRead(ALTI_RAM_MEMORY::TTC_DECODER_COMMAND, segment0, num, start, start)) != 0) {
        CERR("ALTI: reading \"%s\" memory in block cycle", ALTI_RAM_MEMORY_NAME[TTC_DECODER_COMMAND].c_str());
        return (m_status);
    }
   
    if ((m_status = MEMDataRead(ALTI_RAM_MEMORY::TTC_DECODER_TIMESTAMP, segment1, num, start, start)) != 0) {
        CERR("ALTI: reading \"%s\" memory in block cycle", ALTI_RAM_MEMORY_NAME[TTC_DECODER_TIMESTAMP].c_str());
        return (m_status);
    }

    unsigned int indx;
    unsigned int *data[2];
    data[0] = (unsigned int *) segment0->VirtualAddress();
    data[1] = (unsigned int *) segment1->VirtualAddress();
    TTC_SPY_COMMAND command;
    TTC_SPY_TIMESTAMP timestamp;
    std::vector<unsigned int> tmp;

    std::string readable;

    bool l1a, frame_long;
    unsigned int pseudo_l1id = 0;
    bool bcr_found = false;
    unsigned int bcr_offset = 0;
    int bcid = 0;
    int ttyp_subaddr = -1;
    unsigned int ttyp_addr;
    bool ttyp_external = false;
    bool ttyp_recognized = false;
    unsigned int ttyp_word;
    unsigned int ttyp_cnt[3];
	    
    // human-readable, TTC scope
    std::cout << "|  Msg    |      TTCrx  E   SUB    DATA  |  Pseudo-L1ID  |  CHCK VAL  |  BCID  |  BC Id:fied  |   BC Count  |\n";
    std::cout << "|         |    [Type CNT-HEX (CNT-DEC)]  |               |            |        |              |             |\n";
    std::cout << "|---------|------------------------------|---------------|------------|--------|--------------|-------------|\n";
    for (indx = start; indx < start + num; indx++) {
      MEMCommandTimestampToBitstrings(data[0][indx], data[1][indx], command, timestamp);
      l1a = timestamp.L1A();
      frame_long = (command.FrameType() == "Long");
      
      // BC Identified
      if (!frame_long && (command.Data() == bcr_word)) {
	bcid = 0;
	if(!bcr_found) bcr_found = true;
      }
      else if(bcr_found){		
	bcid = (timestamp.BC_Cnt() + bcid - bcr_offset + 1) % ORBIT_LENGTH; // Increases by the counter value + 1 each time (counter value is the number of empty BCIDs between the lines)
	if(bcid != 0) {
	  if((bcid - bcr_delay) >= 0) bcid -= bcr_delay; 
	  else bcid += (1 - bcr_delay);
	}
      }

      // Channel-A command
      if(l1a){
	
	std::ostringstream buf_a;
	buf_a << "|";
	buf_a << "  " << std::setw(5) << std::left << "L1A" << "  |";
	buf_a << "                              |";
	buf_a << "   0x" << std::hex << std::setw(8) << std::setfill('0') << std::right << (pseudo_l1id++) << "  |";
	buf_a << "     " << ((timestamp.SingleBitError() || timestamp.DoubleBitError()) ? "ERR" : "OK ") << "    |";
	buf_a << "  " << std::dec << std::setw(4) << std::setfill('0') << std::right << (bcid) << "  |";
	buf_a << "     " << std::setw(3) << (bcr_found ? "YES" : "NO ") << std::left << "      |";
	buf_a << "  " << std::dec << std::setw(9) << std::setfill('0') << std::right << timestamp.BC_Cnt() << "  |";
	std::cout << buf_a.str() << "\n";
	l1a = false;
      } // end Channel-A
      
      // Channel-B command
      if(command.Data()!=0 || frame_long){
	
	std::ostringstream buf;
	//buf.clear();
	
	buf << "|";
	buf << "  " << std::setw(5) << std::left << (l1a ? "L1A" : command.FrameType()) << "  |";
	if ((!l1a) && frame_long) { // Long
	  buf << "    A 0x" << std::hex << std::setw(4) << std::setfill('0') << std::right << command.Address() << ((command.AddressSpace() == "External") ? " 1" : ((command.AddressSpace() == "Internal") ? " 0" : "   ")) << " S 0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << command.SubAddress() << " D 0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << command.Data() << "  |";
	  
	  // check the ttypes
	  if (command.SubAddress() == ((unsigned int) (ttyp_subaddr + 1))) {
	    if (ttyp_subaddr == -1) { // start
	      ttyp_subaddr = 0;
	      ttyp_addr = command.Address();
	      ttyp_external = (command.AddressSpace() == "External");
	      ttyp_word = command.Data();
	    }
	    else if ((ttyp_addr >= 0) && (ttyp_addr < 3)) {
	      if ((command.Address() == ttyp_addr) && (command.AddressSpace() == (ttyp_external ? "External" : "Internal"))) {
		ttyp_cnt[ttyp_subaddr++] = command.Data();
		if (ttyp_subaddr == 3) { // end
		  ttyp_recognized = true;
		  ttyp_subaddr = -1;
		}
	      }
	      else { // start over
		ttyp_subaddr = -1;
	      }
	    }
	    else { // start over
	      ttyp_subaddr = -1;
	    }
	  }
	  else { // start over
	    ttyp_subaddr = -1;
	  }
	  
	}
	else if (!frame_long) { // Short
	  buf << "                      D 0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << command.Data() << "  |";
	}
	else if (l1a) { // L1A
	  buf << "                              |";
	}
	
	// Pseudo-L1ID
	if (l1a) {
	  buf << "   0x" << std::hex << std::setw(8) << std::setfill('0') << std::right << (pseudo_l1id++) << "  |";
	}
	else {
	  buf << "               |";
	}
	
	buf << "     " << ((timestamp.SingleBitError() || timestamp.DoubleBitError()) ? "ERR" : "OK ") << "    |";
	
	// BCID
	buf << "  " << std::dec << std::setw(4) << std::setfill('0') << std::right << (bcid) << "  |";
	buf << "     " << std::setw(3) << (bcr_found ? "YES" : "NO ") << std::left << "      |";
	
	buf << "  " << std::dec << std::setw(9) << std::setfill('0') << std::right << timestamp.BC_Cnt() << "  |";
	
	std::cout << buf.str() << "\n";
	
	if (ttyp_recognized) {
	  std::ostringstream buf1;
	  buf1 << "|  TType  |  [0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << ttyp_word << " 0x" << std::hex << std::setw(6) << ((ttyp_cnt[0] << 16) + (ttyp_cnt[1] << 8) + ttyp_cnt[2]) << " (" << std::dec << std::setw(8) << std::setfill('0') << std::right << ((ttyp_cnt[0] << 16) + (ttyp_cnt[1] << 8) + ttyp_cnt[2]) << ")]  |";
	  buf1 << "               |"; // Pseudo-L1ID
	  buf1 << "     " << ((timestamp.SingleBitError() || timestamp.DoubleBitError()) ? "ERR" : "OK ") << "    |";
	  buf1 << "  " << std::dec << std::setw(4) << std::setfill('0') << std::right << (bcid) << "  |";
	  buf1 << "     " << std::setw(3) << (bcr_found ? "YES" : "NO ") << std::left << "      |";
	  buf1 << "  " << std::dec << std::setw(9) << std::setfill('0') << std::right << timestamp.BC_Cnt() << "  |";
	  
	  ttyp_recognized = false;
	  
	  std::cout << buf1.str() << "\n";
	}
      } // end Channel-B
    }
     
    return (m_status);
}   

//------------------------------------------------------------------------------

int AltiModule::DECReadFile(RCD::CMEMSegment *segment0, RCD::CMEMSegment *segment1, const unsigned int start, const unsigned int num, unsigned int bcr_word, int bcr_delay, const int format, std::ofstream &outf) { // read TTC decoder memory, into file
    if (start > MAX_MEMORY) {
        CERR("ALTI: invalid start index of snapshot memory", "");
        m_status = WRONGPAR;
        return (m_status);
    }
    else if (start + num > MAX_MEMORY) {
        CERR("ALTI: invalid number of data to be read from the snapshot memory", "");
        m_status = WRONGPAR;
        return (m_status);
    }

    if ((m_status = MEMDataRead(ALTI_RAM_MEMORY::TTC_DECODER_COMMAND, segment0, num, start, start)) != 0) {
        CERR("ALTI: reading \"%s\" memory in block cycle", ALTI_RAM_MEMORY_NAME[TTC_DECODER_COMMAND].c_str());
        return (m_status);
    }

    if ((m_status = MEMDataRead(ALTI_RAM_MEMORY::TTC_DECODER_TIMESTAMP, segment1, num, start, start)) != 0) {
        CERR("ALTI: reading \"%s\" memory in block cycle", ALTI_RAM_MEMORY_NAME[TTC_DECODER_TIMESTAMP].c_str());
        return (m_status);
    }
  
    unsigned int indx;
    unsigned int *data[2];
    data[0] = (unsigned int *) segment0->VirtualAddress();
    data[1] = (unsigned int *) segment1->VirtualAddress();
    TTC_SPY_COMMAND command;
    TTC_SPY_TIMESTAMP timestamp;
    std::vector<unsigned int> tmp;
    bool l1a, frame_long;

    std::string readable;

    unsigned int pseudo_l1id  = 0;
    bool bcr_found = false;
    unsigned int bcid = 0;
    int ttyp_subaddr = -1;
    unsigned int ttyp_addr;
    bool ttyp_external = false;
    bool ttyp_recognized = false;
    unsigned int ttyp_word;
    unsigned int ttyp_cnt[3];
    
    switch (format) {
        case 0: // hex
            if ((m_status = AltiCommon::writeFile(outf, num, data[0] + start, data[1] + start)) != 0) {
            //if ((m_status = AltiCommon::writeFile(outf, num, &(data[0][0]), &(data[1][0]))) != 0) {
                CERR("ALTI: reading TTC decoder memories into hex file ", "");
                return (m_status);
            }
            break;
         case 2: // human-readable, TTC scope
            outf << "|  Msg    |      TTCrx  E   SUB    DATA  |  Pseudo-L1ID  |  CHCK VAL  |  BCID  |  BC Id:fied  |   BC Count  |\n";
            outf << "|         |    [Type CNT-HEX (CNT-DEC)]  |               |            |        |              |             |\n";
            outf << "|---------|------------------------------|---------------|------------|--------|--------------|-------------|\n";
          
            for (indx = start; indx < start + num; indx++) {
                MEMCommandTimestampToBitstrings(data[0][indx], data[1][indx], command, timestamp);
                l1a = timestamp.L1A();
                frame_long = (command.FrameType() == "Long");
		
		// BC Identified
		if (!frame_long && (command.Data() == bcr_word)) {
		  bcid = 0;
		  if(!bcr_found) bcr_found = true;
		}
                else {		
		  bcid = timestamp.BC_Cnt() + bcid + 1; // Increases by the counter value each time.
                }

                // Channel-A command
                if(l1a){

                  std::ostringstream buf_a;
                  buf_a << "|";
                  buf_a << "  " << std::setw(5) << std::left << "L1A" << "  |";
                  buf_a << "                              |";
                  buf_a << "   0x" << std::hex << std::setw(8) << std::setfill('0') << std::right << (pseudo_l1id++) << "  |";
                  buf_a << "     " << ((timestamp.SingleBitError() || timestamp.DoubleBitError()) ? "ERR" : "OK ") << "    |";
                  buf_a << "  " << std::dec << std::setw(4) << std::setfill('0') << std::right << (bcid) << "  |";
                  buf_a << "     " << std::setw(3) << (bcr_found ? "YES" : "NO ") << std::left << "      |";
                  buf_a << "  " << std::dec << std::setw(9) << std::setfill('0') << std::right << timestamp.BC_Cnt() << "  |";
                  outf << buf_a.str() << "\n";
                  l1a = false;
                } // end Channel-A

                // Channel-B command
                if(command.Data()!=0 || frame_long){

                  std::ostringstream buf;
                  //buf.clear();

                  buf << "|";
                  buf << "  " << std::setw(5) << std::left << (l1a ? "L1A" : command.FrameType()) << "  |";
                  if ((!l1a) && frame_long) { // Long
                      buf << "    A 0x" << std::hex << std::setw(4) << std::setfill('0') << std::right << command.Address() << ((command.AddressSpace() == "External") ? " 1" : ((command.AddressSpace() == "Internal") ? " 0" : "   ")) << " S 0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << command.SubAddress() << " D 0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << command.Data() << "  |";

                      // check the ttypes
                      if (command.SubAddress() == ((unsigned int) (ttyp_subaddr + 1))) {
                          if (ttyp_subaddr == -1) { // start
                              ttyp_subaddr = 0;
                              ttyp_addr = command.Address();
                              ttyp_external = (command.AddressSpace() == "External");
                              ttyp_word = command.Data();
                          }
                          else if ((ttyp_addr >= 0) && (ttyp_addr < 3)) {
                              if ((command.Address() == ttyp_addr) && (command.AddressSpace() == (ttyp_external ? "External" : "Internal"))) {
                                  ttyp_cnt[ttyp_subaddr++] = command.Data();
                                  if (ttyp_subaddr == 3) { // end
                                      ttyp_recognized = true;
                                      ttyp_subaddr = -1;
                                  }
                              }
                              else { // start over
                                  ttyp_subaddr = -1;
                              }
                          }
                          else { // start over
                              ttyp_subaddr = -1;
                          }
                      }
                      else { // start over
                          ttyp_subaddr = -1;
                      }
  
                  }
                  else if (!frame_long) { // Short
                      buf << "                      D 0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << command.Data() << "  |";
                  }
                  else if (l1a) { // L1A
                      buf << "                              |";
                  }
  
                  // Pseudo-L1ID
                  if (l1a) {
                      buf << "   0x" << std::hex << std::setw(8) << std::setfill('0') << std::right << (pseudo_l1id++) << "  |";
                  }
                  else {
                      buf << "               |";
                  }
  
                  buf << "     " << ((timestamp.SingleBitError() || timestamp.DoubleBitError()) ? "ERR" : "OK ") << "    |";
		  
		  // BCID
                  buf << "  " << std::dec << std::setw(4) << std::setfill('0') << std::right << (bcid) << "  |";
		  buf << "     " << std::setw(3) << (bcr_found ? "YES" : "NO ") << std::left << "      |";

                  buf << "  " << std::dec << std::setw(9) << std::setfill('0') << std::right << timestamp.BC_Cnt() << "  |";   
                  outf << buf.str() << "\n";
  
                  if (ttyp_recognized) {
                      std::ostringstream buf1;
                      buf1 << "|  TType  |  [0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << ttyp_word << " 0x" << std::hex << std::setw(6) << ((ttyp_cnt[0] << 16) + (ttyp_cnt[1] << 8) + ttyp_cnt[2]) << " (" << std::dec << std::setw(8) << std::setfill('0') << std::right << ((ttyp_cnt[0] << 16) + (ttyp_cnt[1] << 8) + ttyp_cnt[2]) << ")]  |";
                      buf1 << "               |"; // Pseudo-L1ID
                      buf1 << "     " << ((timestamp.SingleBitError() || timestamp.DoubleBitError()) ? "ERR" : "OK ") << "    |";
                      buf1 << "  " << std::dec << std::setw(4) << std::setfill('0') << std::right << (bcid) << "  |";
                      buf1 << "     " << std::setw(3) << (bcr_found ? "YES" : "NO ") << std::left << "      |";
                      buf1 << "  " << std::dec << std::setw(9) << std::setfill('0') << std::right << timestamp.BC_Cnt() << "  |";
  
                      ttyp_recognized = false;
       
		      outf << buf1.str() << "\n";
		  }
		} // end B-channel
	    }
            break;
        default:
            break;
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MEMCommandTimestampToScopeString(const unsigned int command, const unsigned int timestamp, std::string &readable) { // TTC decoder command/timestamp memory entries -> user readable ttcscope line

    TTC_SPY_COMMAND command_bs;
    TTC_SPY_TIMESTAMP timestamp_bs;
    std::vector<unsigned int> tmp;

    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(command));
    command_bs = (RCD::BitSet) tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(timestamp));
    timestamp_bs = (RCD::BitSet) tmp;

    bool l1a, frame_long;
    l1a = (timestamp & TTC_SPY_TIMESTAMP::STAT_MASK_L1A);
    frame_long = (command_bs.FrameType() == "Long");
    
    std::ostringstream buf;

    buf << "|";
    buf << "  " << std::setw(5) << std::left << (l1a ? "L1A" : command_bs.FrameType()) << "  |";
    if ((!l1a) && frame_long) { // Long
        buf << "  A 0x" << std::hex << std::setw(4) << std::setfill('0') << std::right << command_bs.Address() << ((command_bs.AddressSpace() == "External") ? " 1" : ((command_bs.AddressSpace() == "Internal") ? " 0" : "   ")) << " S 0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << command_bs.SubAddress() << " D 0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << command_bs.Data() << "  |";
    }
    else if ((!l1a) && (!frame_long)) { // Short
        buf << "                    D 0x" << std::hex << std::setw(2) << std::setfill('0') << std::right << command_bs.Data() << "  |";
    }
    else if (l1a) { // L1A
        buf << "                            |";
    }
    buf << "     " << ((timestamp_bs.SingleBitError() || timestamp_bs.DoubleBitError()) ? "ERR" : "OK ") << "    |";
    buf << "  " << std::dec << std::setw(9) << std::setfill('0') << std::right << timestamp_bs.BC_Cnt() << "  |";

    readable = buf.str();

    return (0);
}

//------------------------------------------------------------------------------

int AltiModule::MEMScopeStringToCommandTimestamp(const std::string &readable, unsigned int &command, TTC_SPY_COMMAND &command_bs, unsigned int &timestamp, TTC_SPY_TIMESTAMP &timestamp_bs) { // user readable ttcscope line -> TTC decoder command/timestamp memory entries

    std::stringstream ss;
    std::vector<std::string> arg;
    int num;
    if ((num  = AltiCommon::splitString(readable, "|", arg)) != 9) {
        CERR("ALTI: wrong number of fields in ttcscope line\n%s", readable.c_str());
        return (-1);
    }

    std::vector<unsigned int> tmp;

    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(0));
    command_bs = (RCD::BitSet) tmp;
    timestamp_bs = (RCD::BitSet) tmp;

    // BC_Cnt field
    arg[7] = AltiCommon::trimString(arg[7]);
    ss << arg[7];
    unsigned int bc_count;
    ss >> bc_count;
    bc_count &= TTC_SPY_TIMESTAMP::STAT_MASK_BC_CNT; // keep only 28 bits
    //std::printf("bc_count: 0x%07x\n", bc_count);
    timestamp_bs.BC_Cnt(bc_count);

    // SingleOrDoubleBitError field
    arg[4] = AltiCommon::trimString(arg[4]);
    ss << arg[4];
    bool error = (arg[4] == "OK" || arg[4].empty()) ? false : true;
    //std::printf("error: %s\n", error ? "ER" : "OK");
    timestamp_bs.SingleBitError(error);
    timestamp_bs.DoubleBitError(error);

    ss.clear();
    ss << arg[2];

    arg[1] = AltiCommon::trimString(arg[1]);
    if (arg[1] == "L1A") {
        timestamp_bs.L1A(1);
        //command_bs.FrameType("Long"); // this bit is set like this in the FW, check this...
    }
    else if (arg[1] == "Short") {
        timestamp_bs.L1A(0);
        command_bs.FrameType("Short");
        char c;
        unsigned int data, checksum;
        ss >> c >> std::hex >> data >> std::hex >> checksum;
        data &= TTC_SPY_COMMAND::STAT_MASK_DATA; // keep only 8 bits
        checksum &= TTC_SPY_COMMAND::STAT_MASK_DATA; // keep only 8 bits
       
        if (c != 'D') {
            CERR("ALTI: wrong ttcscope Short command line, expected \"D\" field, got \"%c\" instead", c);
            return (-1);
        }
        command_bs.Data(data);
    }
    else if (arg[1] == "Long") {
        timestamp_bs.L1A(0);
        command_bs.FrameType("Long");
        char c1, c2, c3;
        unsigned int addr, subaddr, data, checksum;
        bool external;
        ss >> c1 >> std::hex >> addr >> external >> c2 >> std::hex >> subaddr >> c3 >> std::hex >> data >> std::hex >> checksum;
        addr &= (TTC_SPY_COMMAND::STAT_MASK_ADDRESS >> AltiCommon::getFirstSetBitPos(TTC_SPY_COMMAND::STAT_MASK_ADDRESS)); // keep only 14 bits
        subaddr &= (TTC_SPY_COMMAND::STAT_MASK_SUBADDRESS >> AltiCommon::getFirstSetBitPos(TTC_SPY_COMMAND::STAT_MASK_SUBADDRESS)); // keep only 8 bits
        data &= (TTC_SPY_COMMAND::STAT_MASK_DATA >> AltiCommon::getFirstSetBitPos(TTC_SPY_COMMAND::STAT_MASK_DATA)); // keep only 8 bits
        checksum &= (TTC_SPY_COMMAND::STAT_MASK_DATA >> AltiCommon::getFirstSetBitPos(TTC_SPY_COMMAND::STAT_MASK_DATA)); // keep only 8 bits
      
        if (c1 != 'A') {
            CERR("ALTI: wrong ttcscope Long command line, expected \"A\" field, got \"%c\" instead", c1);
            return (-1);
        }
        if (c2 != 'S') {
            CERR("ALTI: wrong ttcscope Long command line, expected \"S\" field, got \"%c\" instead", c2);
            return (-1);
        }
        if (c3 != 'D') {
            CERR("ALTI: wrong ttcscope Long command line, expected \"D\" field, got \"%c\" instead", c3);
            return (-1);
        }
        command_bs.Address(addr);
        command_bs.SubAddress(subaddr);
        command_bs.Data(data);
    }
    else {
        CERR("ALTI: wrong ttcscope line type \"%s\"", arg[1].c_str());
        return (-1);
    }
   
    command = command_bs.number()[0];
    timestamp = timestamp_bs.number()[0];

    return (0);
}

//------------------------------------------------------------------------------

int AltiModule::MEMCommandTimestampToBitstrings(const unsigned int command, const unsigned int timestamp, TTC_SPY_COMMAND &command_bs, TTC_SPY_TIMESTAMP &timestamp_bs) { // TTC decoder command/timestamp memory entries -> corresponding bitstrings

    std::vector<unsigned int> tmp;

    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(command));
    command_bs = (RCD::BitSet) tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(timestamp));
    timestamp_bs = (RCD::BitSet) tmp;

    //command_bs.print();

    return (0);
}

//------------------------------------------------------------------------------

int AltiModule::ENCInhibitParamsRead(const unsigned int bgo, unsigned int &width, unsigned int &delay) {
    if (bgo < 0 || bgo > 3) {
        CERR("ALTI: wrong BGO index \"%d\"", bgo);
        m_status = -1;
        return (m_status);
    }

    ALTI_ENC_BGOCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != 0) {
        CERR("ALTI: reading BGO%d inhibit parameter register", bgo);
        return (m_status);
    }
   
    width = bs.InhibitWidth();
    delay = bs.InhibitDelay();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCInhibitParamsWrite(const unsigned int bgo, const unsigned int width, const unsigned int delay) { // all
    if (bgo < 0 || bgo > 3) {
        CERR("ALTI: wrong BGO index \"%d\"", bgo);
        m_status = -1;
        return (m_status);
    }

    ALTI_ENC_BGOCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != 0) {
        CERR("ALTI: reading BGO%d inhibit parameter register", bgo);
        return (m_status);
    }
    // MODIFY
    bs.InhibitWidth(width);
    bs.InhibitDelay(delay);
    // WRITE
    if ((m_status = m_alti->ENC_BgoControl_Write(bgo, bs)) != 0) {
        CERR("ALTI: writing width %d and delay %d to BGO%d inhibit parameter register", width, delay, bgo);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCInhibitParamsRead(std::vector<unsigned int> &width, std::vector<unsigned int> &delay) { // all
    width.clear();
    delay.clear();

    ALTI_ENC_BGOCONTROL_BITSTRING bs;

    unsigned int bgo;
    for (bgo = 0; bgo <= 3; bgo++) {
        // READ
        if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != 0) {
            CERR("ALTI: reading BGo-%d inhibit parameter register", bgo);
            return (m_status);
        }
        width.push_back(bs.InhibitWidth());
        delay.push_back(bs.InhibitDelay());
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCInhibitParamsWrite(const std::vector<unsigned int> &width, const std::vector<unsigned int> &delay) {
    ALTI_ENC_BGOCONTROL_BITSTRING bs;
  
    unsigned int bgo;
    for (bgo = 0; bgo <= 3; bgo++) {
      // READ
      if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != 0) {
        CERR("ALTI: reading BGO%d inhibit parameter register", bgo);
        return (m_status);
      }
      // MODIFY
      bs.InhibitWidth(width[bgo]);
      bs.InhibitDelay(delay[bgo]);
      // WRITE
      if ((m_status = m_alti->ENC_BgoControl_Write(bgo, bs)) != 0) {
	CERR("ALTI: writing width %d and delay %d to BGO%d inhibit parameter register", width[bgo], delay[bgo], bgo);
	return (m_status);
      }
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeDelayRead(unsigned int &delay) {
  ALTI_ENC_TTYPECONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->ENC_TtypeControl_Read(bs)) != 0) {
    CERR("ALTI: reading TTC encoder TTYP control register", "");
    return (m_status);
  }
  
  delay = bs.TtypeDelay();
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeDelayWrite(const unsigned int delay) {
  ALTI_ENC_TTYPECONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->ENC_TtypeControl_Read(bs)) != 0) {
    CERR("ALTI: reading TTC encoder TTYP control register", "");
    return (m_status);
  }
  
  // MODIFY
  bs.TtypeDelay(delay);
  
  // WRITE
  if ((m_status = m_alti->ENC_TtypeControl_Write(bs)) != 0) {
    CERR("ALTI: writing TTC encoder TTYP control register", "");
    return (m_status);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeControlRead(bool &ena, unsigned int &addr, TTC_ADDRESS_SPACE &addressSpace, unsigned int &subAddr) {
  ALTI_ENC_TTYPECONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti-> ENC_TtypeControl_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading TTC encoder TTYP word control register", "");
    return (m_status);
  }

  ena  = bs.EnableTtype(); 
  addr = bs.TtypeAddress();  
  if (bs.TtypeExternal())  addressSpace = TTC_ADDRESS_SPACE::EXTERNAL;
  else                     addressSpace = TTC_ADDRESS_SPACE::INTERNAL;  
  subAddr = bs.TtypeSubAddress(); 
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeControlWrite(const unsigned int addr, const TTC_ADDRESS_SPACE addressSpace, const unsigned int subAddr) {
  ALTI_ENC_TTYPECONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti-> ENC_TtypeControl_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading TTC encoder TTYP word control register", "");
    return (m_status);
  }
  
  // MODIFY
  bs.TtypeAddress(addr);
  bs.TtypeExternal((addressSpace == EXTERNAL) ? true : false );
  bs.TtypeSubAddress(subAddr); 
 
  // WRITE
  if ((m_status = m_alti-> ENC_TtypeControl_Write(bs)) != SUCCESS) {
    CERR("ALTI: writing TTC encoder TTYP word control register", "");
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeEnable(const bool ena) {
  ALTI_ENC_TTYPECONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti-> ENC_TtypeControl_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading TTC encoder TTYP word control register", "");
    return (m_status);
  }
 
  // MODIFY
  bs.EnableTtype(ena);

  // WRITE
  if ((m_status = m_alti-> ENC_TtypeControl_Write(bs)) != SUCCESS) {
    CERR("ALTI: writing TTC encoder TTYP word control register", "");
    return (m_status);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeFifoStatusRead(bool &full, bool &empty, u_int &level) {

    ALTI_ENC_TTYPESTATUS_BITSTRING bs;

    // READ
    if ((m_status = m_alti->ENC_TtypeStatus_Read(bs)) != 0) {
        CERR("ALTI: reading TTYP FIFOs status register", "");
        return (m_status);
    }
   
    full  = bs.FifoFull();
    empty = bs.FifoEmpty();
    level = bs.FifoLevel();

    return m_status;
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeCounterReset(const bool evt, const bool orb) {

  ALTI_ENC_TTYPECONTROL_BITSTRING bs;

  // READ
  if ((m_status = m_alti-> ENC_TtypeControl_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading TTC encoder TTYP word control register", "");
    return (m_status);
  }
 
  // MODIFY
  if(evt) bs.ResetEventCounter(true);
  if(orb) bs.ResetOrbitCounter(true);
  if ((m_status = m_alti-> ENC_TtypeControl_Write(bs)) != SUCCESS) {
    CERR("ALTI: writing TTC encoder TTYP word control register", "");
    return (m_status);
  }

  // REVERT
  bs.ResetEventCounter(false);
  bs.ResetOrbitCounter(false);
  if ((m_status = m_alti-> ENC_TtypeControl_Write(bs)) != SUCCESS) {
    CERR("ALTI: writing TTC encoder TTYP word control register", "");
    return (m_status);
  }
  
  return m_status;
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeCounterSourceRead(TTYP_COUNTER_SOURCE &src) {

  ALTI_ENC_TTYPECONTROL_BITSTRING bs;

  // READ
  if ((m_status = m_alti-> ENC_TtypeControl_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading TTC encoder TTYP word control register", "");
    return (m_status);
  }
  
  if(bs.SelectCounterSource()) src = TTYP_COUNTER_SOURCE::ORBIT;
  else                         src = TTYP_COUNTER_SOURCE::EVENT;

  return m_status;
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeCounterSourceWrite(const TTYP_COUNTER_SOURCE src) {

  ALTI_ENC_TTYPECONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti-> ENC_TtypeControl_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading TTC encoder TTYP word control register", "");
    return (m_status);
  }
  
  // MODIFY
  if(src == TTYP_COUNTER_SOURCE::ORBIT) bs.SelectCounterSource(true);
  else                                  bs.SelectCounterSource(false);
  
  // WRITE
  if ((m_status = m_alti-> ENC_TtypeControl_Write(bs)) != SUCCESS) {
    CERR("ALTI: writing TTC encoder TTYP word control register", "");
    return (m_status);
  }

  return m_status;
}

//------------------------------------------------------------------------------

int AltiModule::ENCTriggerTypeCounterRead(u_int &evt, u_int &orb) {
 ALTI_ENC_TTYPECONTROL_BITSTRING bs;
  
 // READ
 if ((m_status = m_alti-> ENC_EventCounter_Read(evt)) != SUCCESS) {
   CERR("ALTI: reading TTC Event counter register", "");
   return (m_status);
 }
 
 if ((m_status = m_alti-> ENC_OrbitCounter_Read(orb)) != SUCCESS) {
   CERR("ALTI: reading TTC Orbit counter register", "");
   return (m_status);
 }
 
 return m_status;
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoGenerate(const unsigned int bgo) {
    if (bgo < 0 || bgo > 3) {
        CERR("ALTI: wrong BGO index \"%d\"", bgo);
        m_status = -1;
        return (m_status);
    }
    ALTI_ENC_BGOCONTROL_BITSTRING bs;
    
    // READ
    if ((m_status = m_alti->ENC_BgoControl_Read(bgo,bs)) != SUCCESS) { 
        CERR("ALTI: writing %d to VME short command register", bgo);
        return (m_status);
    }
    // SEND
    bs.BgoTrigger(true);
    if ((m_status = m_alti->ENC_BgoControl_Write(bgo,bs)) != SUCCESS) { 
        CERR("ALTI: writing %d to VME short command register", bgo);
        return (m_status);
    }
    // REVERT
    bs.BgoTrigger(false);
    if ((m_status = m_alti->ENC_BgoControl_Write(bgo,bs)) != SUCCESS) { 
        CERR("ALTI: writing %d to VME short command register", bgo);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandModeRead(const unsigned int bgo, TTC_BGO_MODE_TRIGGER &mode_trigger, TTC_BGO_MODE_COMMAND &mode_command, TTC_BGO_MODE_REPEAT &mode_repeat, TTC_BGO_MODE_FIFO &mode_fifo, TTC_BGO_MODE_INHIBIT &mode_inh) {
    if (bgo < 0 || bgo > 3) {
        CERR("ALTI: wrong BGO index \"%d\"", bgo);
        m_status = -1;
        return (m_status);
    }

    ALTI_ENC_BGOCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != 0) {
        CERR("ALTI: reading BGO%d mode selection register", bgo);
        return (m_status);
    }

    bool b;
    b = bs.BgoSource();
    if (b == ALTI_ENC_BGOCONTROL_BITSTRING::BGOSOURCE_TYPE::BGOSOURCE_EXTERNAL) {
      mode_trigger = TTC_BGO_MODE_TRIGGER::BGO_SIGNAL;
    }
    else if (b == ALTI_ENC_BGOCONTROL_BITSTRING::BGOSOURCE_TYPE::BGOSOURCE_VME) {
      mode_trigger = TTC_BGO_MODE_TRIGGER::VME;
    }
   
    b = bs.CommandMode();
    if (b == ALTI_ENC_BGOCONTROL_BITSTRING::COMMANDMODE_TYPE::COMMANDMODE_SYNCHRONOUS) {
        mode_command = TTC_BGO_MODE_COMMAND::SYNCHRONOUS;
    }
    else if (b == ALTI_ENC_BGOCONTROL_BITSTRING::COMMANDMODE_TYPE::COMMANDMODE_ASYNCHRONOUS) {
        mode_command = TTC_BGO_MODE_COMMAND::ASYNCHRONOUS;
    }

    b = bs.FifoRetransmitMode();
    if (b == ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_SINGLE) {
        mode_repeat = TTC_BGO_MODE_REPEAT::SINGLE;
    }
    else if (b == ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_REPETITIVE) {
        mode_repeat = TTC_BGO_MODE_REPEAT::REPETITIVE;
    }

    b = bs.FifoMode();
    if (b) {
        mode_fifo = TTC_BGO_MODE_FIFO::WHEN_NOT_EMPTY;
    }
    else {
        mode_fifo = TTC_BGO_MODE_FIFO::ON_TRIGGER;
    }

    b = bs.InhibitMode();
    if (b) {
        mode_inh = TTC_BGO_MODE_INHIBIT::ON_INHIBIT;
    }
    else {
        mode_inh = TTC_BGO_MODE_INHIBIT::OTHER_MODES;
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandModeWrite(const unsigned int bgo, const TTC_BGO_MODE_TRIGGER mode_trigger, const TTC_BGO_MODE_COMMAND mode_command, const TTC_BGO_MODE_REPEAT mode_repeat, const TTC_BGO_MODE_FIFO mode_fifo, const TTC_BGO_MODE_INHIBIT mode_inh) {
    if (bgo < 0 || bgo > 3) {
        CERR("ALTI: wrong BGO index \"%d\"", bgo);
        m_status = -1;
        return (m_status);
    }
    ALTI_ENC_BGOCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != SUCCESS) {
        CERR("ALTI: reading BGO%d mode selection register", bgo);
        return (m_status);
    }

    // MODIFY
    bs.BgoSource((mode_trigger == TTC_BGO_MODE_TRIGGER::BGO_SIGNAL) ? ALTI_ENC_BGOCONTROL_BITSTRING::BGOSOURCE_TYPE::BGOSOURCE_EXTERNAL : ALTI_ENC_BGOCONTROL_BITSTRING::BGOSOURCE_TYPE::BGOSOURCE_VME);
    bs.CommandMode((mode_command == TTC_BGO_MODE_COMMAND::SYNCHRONOUS) ? ALTI_ENC_BGOCONTROL_BITSTRING::COMMANDMODE_TYPE::COMMANDMODE_SYNCHRONOUS : ALTI_ENC_BGOCONTROL_BITSTRING::COMMANDMODE_TYPE::COMMANDMODE_ASYNCHRONOUS);
    bs.FifoRetransmitMode((mode_repeat == TTC_BGO_MODE_REPEAT::SINGLE) ? ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_SINGLE : ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_REPETITIVE);
    bs.FifoMode((mode_fifo == TTC_BGO_MODE_FIFO::WHEN_NOT_EMPTY) ? true : false );
    bs.InhibitMode((mode_inh == TTC_BGO_MODE_INHIBIT::ON_INHIBIT) ? true : false );

    // WRITE
    if ((m_status = m_alti->ENC_BgoControl_Write(bgo, bs)) != SUCCESS) {
        CERR("ALTI: writing BGO%d mode selection register", bgo);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandModeRead(std::vector<TTC_BGO_MODE_TRIGGER> &mode_trigger, std::vector<TTC_BGO_MODE_COMMAND> &mode_command, std::vector<TTC_BGO_MODE_REPEAT> &mode_repeat, std::vector<TTC_BGO_MODE_FIFO> &mode_fifo, std::vector<TTC_BGO_MODE_INHIBIT> &mode_inh) {
    mode_trigger.clear();
    mode_command.clear();
    mode_repeat.clear();
    mode_fifo.clear();
    mode_inh.clear();

    ALTI_ENC_BGOCONTROL_BITSTRING bs;

    unsigned int bgo;
    bool b;
    for (bgo = 0; bgo <= 3; bgo++) {
        // READ
        if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != SUCCESS) {
            CERR("ALTI: reading BGO%d mode selection register", bgo);
            return (m_status);
        }
     
	b = bs.BgoSource();
	if (b == ALTI_ENC_BGOCONTROL_BITSTRING::BGOSOURCE_TYPE::BGOSOURCE_EXTERNAL) {
	  mode_trigger.push_back(TTC_BGO_MODE_TRIGGER::BGO_SIGNAL);
	}
	else if (b == ALTI_ENC_BGOCONTROL_BITSTRING::BGOSOURCE_TYPE::BGOSOURCE_VME) {
	  mode_trigger.push_back(TTC_BGO_MODE_TRIGGER::VME);
	}
	
	b = bs.CommandMode();
	if (b == ALTI_ENC_BGOCONTROL_BITSTRING::COMMANDMODE_TYPE::COMMANDMODE_SYNCHRONOUS) {
	  mode_command.push_back(TTC_BGO_MODE_COMMAND::SYNCHRONOUS);
	}
	else if (b == ALTI_ENC_BGOCONTROL_BITSTRING::COMMANDMODE_TYPE::COMMANDMODE_ASYNCHRONOUS) {
	  mode_command.push_back(TTC_BGO_MODE_COMMAND::ASYNCHRONOUS);
	}
	
	b = bs.FifoRetransmitMode();
	if (b == ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_SINGLE) {
	  mode_repeat.push_back(TTC_BGO_MODE_REPEAT::SINGLE);
	}
	else if (b == ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_REPETITIVE) {
	  mode_repeat.push_back(TTC_BGO_MODE_REPEAT::REPETITIVE);
	}  

	b = bs.FifoMode();
        if (b) {
            mode_fifo.push_back(TTC_BGO_MODE_FIFO::WHEN_NOT_EMPTY);
        }
        else {
	  mode_fifo.push_back(TTC_BGO_MODE_FIFO::ON_TRIGGER);
        }
       
        b = bs.InhibitMode();
        if (b) {
            mode_inh.push_back(TTC_BGO_MODE_INHIBIT::ON_INHIBIT);
        }
        else {
            mode_inh.push_back(TTC_BGO_MODE_INHIBIT::OTHER_MODES);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandModeWrite(const std::vector<TTC_BGO_MODE_TRIGGER> &mode_trigger, const std::vector<TTC_BGO_MODE_COMMAND> &mode_command, const std::vector<TTC_BGO_MODE_REPEAT> &mode_repeat, const std::vector<TTC_BGO_MODE_FIFO> &mode_fifo, const std::vector<TTC_BGO_MODE_INHIBIT> &mode_inh) {
    ALTI_ENC_BGOCONTROL_BITSTRING bs;

    unsigned int bgo;
    for (bgo = 0; bgo <= 3; bgo++) {
        // READ
        if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != SUCCESS) {
            CERR("ALTI: reading BGO%d mode selection register", bgo);
            return (m_status);
        }
    
	// MODIFY
	bs.BgoSource((mode_trigger[bgo] == TTC_BGO_MODE_TRIGGER::BGO_SIGNAL) ? ALTI_ENC_BGOCONTROL_BITSTRING::BGOSOURCE_TYPE::BGOSOURCE_EXTERNAL : ALTI_ENC_BGOCONTROL_BITSTRING::BGOSOURCE_TYPE::BGOSOURCE_VME);
	bs.CommandMode((mode_command[bgo] == TTC_BGO_MODE_COMMAND::SYNCHRONOUS) ? ALTI_ENC_BGOCONTROL_BITSTRING::COMMANDMODE_TYPE::COMMANDMODE_SYNCHRONOUS : ALTI_ENC_BGOCONTROL_BITSTRING::COMMANDMODE_TYPE::COMMANDMODE_ASYNCHRONOUS);
	bs.FifoRetransmitMode((mode_repeat[bgo] == TTC_BGO_MODE_REPEAT::SINGLE) ? ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_SINGLE : ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_REPETITIVE);
        bs.FifoMode((mode_fifo[bgo] == TTC_BGO_MODE_FIFO::WHEN_NOT_EMPTY) ? true : false);
        bs.InhibitMode((mode_inh[bgo] == TTC_BGO_MODE_INHIBIT::ON_INHIBIT) ? true : false);
        
	// WRITE
        if ((m_status = m_alti->ENC_BgoControl_Write(bgo, bs)) != SUCCESS) {
            CERR("ALTI: writing BGO%d mode selection register", bgo);
            return (m_status);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandModeRead(const unsigned int bgo, TTC_BGO_MODE &mode) {
  
  TTC_BGO_MODE_TRIGGER mode_trigger;
    TTC_BGO_MODE_COMMAND mode_command;
    TTC_BGO_MODE_REPEAT mode_repeat;
    TTC_BGO_MODE_FIFO mode_fifo;
    TTC_BGO_MODE_INHIBIT mode_inh;
  
    if ((m_status = ENCBgoCommandModeRead(bgo, mode_trigger, mode_command, mode_repeat, mode_fifo, mode_inh)) != SUCCESS) {
        CERR("ALTI: reading BGO%d mode", bgo);
        return (m_status);
    }
    mode = ENCLowLevelToHighLevelBgoMode(mode_trigger, mode_command, mode_repeat, mode_fifo, mode_inh);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandModeWrite(const unsigned int bgo, const TTC_BGO_MODE mode) {
    TTC_BGO_MODE current_mode;
    
    if ((m_status = ENCBgoCommandModeRead(bgo, current_mode)) != SUCCESS) {
        CERR("ALTI: reading TTC BGO%d FIFO mode", bgo);
        return (m_status);
    }
  
    if ((m_status = ENCBgoCommandModeWrite(bgo, TTC_BGO_MODE_HL[mode].mode_trigger, TTC_BGO_MODE_HL[mode].mode_command, TTC_BGO_MODE_HL[mode].mode_repeat, TTC_BGO_MODE_HL[mode].mode_fifo, TTC_BGO_MODE_HL[mode].mode_inh)) != 0) {
        CERR("ALTI: writing \"%s\" mode for the BGO%d TTC FIFO", TTC_BGO_MODE_NAME[mode].c_str(), bgo);
        return (m_status);
    }
    if ((current_mode == ASYNCHRONOUS_BGO_SIGNAL) && (mode == ASYNCHRONOUS_VME_ON_TRIGGER)) { // specific transition
      // reset the pipeline
      
      if ((m_status = ENCBgoFifoRetransmitWrite((TTC_FIFO) bgo, false)) != SUCCESS) {
	CERR("ALTI: putting TTC BGO%d FIFO out of retransmit mode", bgo);
	return (m_status);
      }
      
      if ((m_status = ENCFifoReset((TTC_FIFO) bgo)) != SUCCESS) {
	CERR("ALTI: resetting read/write pointers for the BGO%d TTC FIFO", bgo);
            return (m_status);
      }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandModeRead(std::vector<TTC_BGO_MODE> &mode) {
    unsigned int bgo;
    
    for (bgo = 0; bgo <= 3; bgo++) {
        m_status = ENCBgoCommandModeRead(bgo, mode[bgo]);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandModeWrite(const std::vector<TTC_BGO_MODE> &mode) {
    unsigned int bgo;
    
    for (bgo = 0; bgo <= 3; bgo++) {
        m_status = ENCBgoCommandModeWrite(bgo, mode[bgo]);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

AltiModule::TTC_BGO_MODE AltiModule::ENCLowLevelToHighLevelBgoMode(const TTC_BGO_MODE_TRIGGER mode_trigger, const TTC_BGO_MODE_COMMAND mode_command, const TTC_BGO_MODE_REPEAT mode_repeat, const TTC_BGO_MODE_FIFO mode_fifo, const TTC_BGO_MODE_INHIBIT mode_inh) {
    
  if (mode_inh == ON_INHIBIT) {
    return SYNCHRONOUS_REPETITIVE;
  }
  else if ((mode_command == SYNCHRONOUS) && (mode_repeat == SINGLE) && (mode_trigger == BGO_SIGNAL) && (mode_fifo == ON_TRIGGER)) {
    return SYNCHRONOUS_SINGLE_BGO_SIGNAL;
  }
  else if ((mode_command == ASYNCHRONOUS) && (mode_trigger == BGO_SIGNAL)) {
    return ASYNCHRONOUS_BGO_SIGNAL;
  }
  else if ((mode_command == ASYNCHRONOUS) && (mode_repeat == SINGLE) && (mode_trigger == VME) && (mode_fifo == ON_TRIGGER)) {
    return ASYNCHRONOUS_VME_ON_TRIGGER;
  }
  else if ((mode_command == ASYNCHRONOUS) && (mode_fifo == WHEN_NOT_EMPTY)) {
    return ASYNCHRONOUS_FIFO_MODE;
  }
  else {
    return INVALID_MODE;
  }
}



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandPut(const unsigned int bgo, const TTC_FRAME_TYPE frameType, const uint16_t addr, const TTC_ADDRESS_SPACE addrSpace, const uint8_t subAddr, const uint8_t data) {
    if (bgo < 0 || bgo > 3) {
        CERR("ALTI: wrong BGO index \"%d\"", bgo);
        m_status = -1;
        return (m_status);
    }

    int short_command = 0;
    short_command = data << 23;

    TTC_SPY_COMMAND command;
    std::vector<unsigned int> tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(0x00000000));
    command = (RCD::BitSet) tmp;
    
    command.FrameType((frameType == TTC_FRAME_TYPE::SHORT) ? "Short" : ((frameType == TTC_FRAME_TYPE::LONG) ? "Long" : ""));
    command.Address(addr);
    command.AddressSpace((addrSpace == TTC_ADDRESS_SPACE::INTERNAL) ? "Internal" : ((addrSpace == TTC_ADDRESS_SPACE::EXTERNAL) ? "External" : ""));
    command.SubAddress(subAddr);
    command.Data(data);

    if (frameType == TTC_FRAME_TYPE::LONG){    
      if ((m_status = m_alti->ENC_BgoFifo_Write(bgo, command.number()[0])) != 0) {
	CERR("ALTI: putting command 0x%08x (\"%s\", \"%s\", addr = 0x%04x, subAddr = 0x%02x, data = 0x%02x) to BGO%d FIFO", command.number()[0], TTC_FRAME_TYPE_NAME[frameType], TTC_ADDRESS_SPACE_NAME[addrSpace], addr, subAddr, data, bgo);
        return (m_status);
      }
    }
    else {
      if ((m_status = m_alti->ENC_BgoFifo_Write(bgo, short_command)) != 0) {
	CERR("ALTI: putting command 0x%08x (\"%s\", \"%s\", addr = 0x%04x, subAddr = 0x%02x, data = 0x%02x) to BGO%d FIFO", command.number()[0], TTC_FRAME_TYPE_NAME[frameType], TTC_ADDRESS_SPACE_NAME[addrSpace], addr, subAddr, data, bgo);
        return (m_status);
      }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandPutShort(const unsigned int bgo, const uint8_t data) {
    return ENCBgoCommandPut(bgo, TTC_FRAME_TYPE::SHORT, 0x0000, TTC_ADDRESS_SPACE::INTERNAL, 0x00, data);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCommandPutLong(const unsigned int bgo, const uint16_t addr, const TTC_ADDRESS_SPACE addrSpace, const uint8_t subAddr, const uint8_t data) {
    return ENCBgoCommandPut(bgo, TTC_FRAME_TYPE::LONG, addr, addrSpace, subAddr, data);
}

//------------------------------------------------------------------------------

int AltiModule::ENCFifoStatusRead(const TTC_FIFO ttcFifo, bool &busy, bool &full, bool &empty, u_int &level) {

    if (ttcFifo < 0 || ttcFifo > 3) {
      CERR("ALTI: wrong BGO index \"%d\"", ttcFifo);
      m_status = -1;
      return (m_status);
    }

    ALTI_ENC_BGOSTATUS_BITSTRING bs;

    // READ
    if ((m_status = m_alti->ENC_BgoStatus_Read(ttcFifo, bs)) != 0) {
        CERR("ALTI: reading BGO/TTYP FIFOs status register", "");
        return (m_status);
    }

    busy  = bs.Busy();
    full  = bs.FifoFull();
    empty = bs.FifoEmpty();
    level = bs.FifoLevel();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCFifoStatusRead(std::vector<bool> &busy, std::vector<bool> &full, std::vector<bool> &empty, std::vector<u_int> &level) {
    busy.clear();
    full.clear();
    empty.clear();
    level.clear();
    
    for(u_int bgo = 0; bgo < 4; bgo++) {
      ALTI_ENC_BGOSTATUS_BITSTRING bs;
      
      // READ
      if ((m_status = m_alti->ENC_BgoStatus_Read(bgo, bs)) != 0) {
        CERR("ALTI: reading BGO%d FIFOs status register", bgo);
        return (m_status);
      }
      
      busy.push_back(bs.Busy());
      full.push_back(bs.FifoFull());
      empty.push_back(bs.FifoEmpty());
      level.push_back(bs.FifoLevel());
    }

    return (m_status);
}


//------------------------------------------------------------------------------

int AltiModule::ENCFifoReset(const TTC_FIFO ttcFifo) {

    //ALTI_BGO_PIPELINEFIFOSRESETS_BITSTRING bs;
    ALTI_ENC_BGOCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->ENC_BgoControl_Read(ttcFifo, bs)) != 0) {
        CERR("ALTI: reading BGO/TTYP FIFOs reset register", "");
        return (m_status);
    }
    // MODIFY
    bs.FifoReset(true);
    // WRITE
    if ((m_status = m_alti->ENC_BgoControl_Write(ttcFifo, bs)) != 0) {
        CERR("ALTI: writing BGO/TTYP FIFOs reset register", "");
        return (m_status);
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCFifoReset() { // all
    
    // MODIFY
    for (u_int bgo = 0; bgo < 4; bgo++) {
      ALTI_ENC_BGOCONTROL_BITSTRING bs;
      // READ
      if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != 0) {
          CERR("ALTI: reading BGO/TTYP FIFOs reset register", "");
          return (m_status);
      }
      // MODIFY
      bs.FifoReset(true);
      // WRITE
      if ((m_status = m_alti->ENC_BgoControl_Write(bgo, bs)) != 0) {
        CERR("ALTI: writing BGO/TTYP FIFOs reset register", "");
        return (m_status);
      }
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCFifoRead(const TTC_FIFO bgo, unsigned int &data) {
  // check FIFO
  CHECKRANGE0(bgo, TTC_FIFO_NUMBER - 1, "TTC FIFO number");  
  
  if ((m_status = m_alti->ENC_BgoFifo_Read((u_int) bgo, data)) != 0) {
    CERR("ALTI: reading BGo-%d FIFO", (u_int) bgo);
    return (m_status);
  }
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCFifoRead(const TTC_FIFO bgo, std::vector<unsigned int> &data) {
  if ((m_status = m_alti->ENC_BgoFifo_Read((u_int) bgo, data)) != 0) {
    CERR("ALTI: reading BGo-%d FIFO", (u_int) bgo);
    return (m_status);
  }
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoFifoRetransmitRead(const TTC_FIFO ttcFifo, bool &retransmit) {
  
  ALTI_ENC_BGOCONTROL_BITSTRING bs;
  
  unsigned int bgo = ttcFifo;
  // READ
  if ((m_status = m_alti->ENC_BgoControl_Read(bgo, bs)) != 0) {
    CERR("ALTI: reading BGO%d mode selection register", bgo);
    return (m_status);
  }
  bool b;
  b = bs.FifoRetransmitMode();
  if (b == ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_REPETITIVE) {
    retransmit = true;
  }
  else if (b == ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_SINGLE) {
    retransmit = false;
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoFifoRetransmitWrite(const TTC_FIFO ttcFifo, const bool retransmit) {
  
  ALTI_ENC_BGOCONTROL_BITSTRING bs;
  
  unsigned int bgo = ttcFifo;
  // READ
  if ((m_status = m_alti->ENC_BgoControl_Read(bgo,bs)) != 0) {
    CERR("ALTI: reading BGO%d mode selection register", bgo);
    return (m_status);
  }
  // MODIFY
  bs.FifoRetransmitMode(retransmit ? ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_REPETITIVE : ALTI_ENC_BGOCONTROL_BITSTRING::FIFORETRANSMITMODE_TYPE::FIFORETRANSMITMODE_SINGLE);
  // WRITE
  if ((m_status = m_alti->ENC_BgoControl_Write(bgo,bs)) != 0) {
    CERR("ALTI: writing BGO%d mode selection register", bgo);
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoFifoRetransmitRead(std::vector<bool> &retransmit) { // all
   
    retransmit.resize(TTC_FIFO_NUMBER);

    unsigned int i;
    TTC_FIFO ttcFifo;
    bool rd;
    for (i = 0; i < TTC_FIFO_NUMBER; i++) {
        ttcFifo = (TTC_FIFO) i;
        ENCBgoFifoRetransmitRead(ttcFifo, rd);
        retransmit[i] = rd;
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoFifoRetransmitWrite(const std::vector<bool> &retransmit) { // all

    unsigned int i;
    TTC_FIFO ttcFifo;
    for (i = 0; i < TTC_FIFO_NUMBER; i++) {
        ttcFifo = (TTC_FIFO) i;
        ENCBgoFifoRetransmitWrite(ttcFifo, retransmit[i]);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoFifoRetransmitWrite(const bool retransmit) { // all
   
    unsigned int i;
    TTC_FIFO ttcFifo;
    for (i = 0; i < TTC_FIFO_NUMBER; i++) {
        ttcFifo = (TTC_FIFO) i;
        ENCBgoFifoRetransmitWrite(ttcFifo, retransmit);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCAsyncCommandPutShort(const uint8_t data) {
    if ((m_status = m_alti->ENC_VmeShortCommand_Write(data)) != 0){
      CERR("Sending short command from VME","");
      return(m_status);
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCAsyncCommandPutLong(const uint16_t addr, const TTC_ADDRESS_SPACE addrSpace, const uint8_t subAddr, const uint8_t data) {
    TTC_SPY_COMMAND command;
    std::vector<unsigned int> tmp;
    tmp.clear();
    tmp.push_back(static_cast<unsigned int>(0x00000000));
    command = (RCD::BitSet) tmp;
    
    command.FrameType("Long");
    command.Address(addr);
    command.AddressSpace((addrSpace == TTC_ADDRESS_SPACE::INTERNAL) ? "Internal" : ((addrSpace == TTC_ADDRESS_SPACE::EXTERNAL) ? "External" : ""));
    command.SubAddress(subAddr);
    command.Data(data);
    
    if ((m_status = m_alti->ENC_VmeLongCommand_Write(command.number()[0])) != 0){
      CERR("Sending long command from VME","");
      return(m_status);
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCAsyncCommandPendingShort(bool &pending) {

    ALTI_ENC_VMESHORTCOMMAND_BITSTRING bs;

    // READ
    if ((m_status = m_alti->ENC_VmeShortCommand_Read(bs)) != 0) {
        CERR("ALTI: reading VME short command status register", "");
        return (m_status);
    }
      
    if (bs.Pending()) {
      pending = true;
    }
    else {
        pending = false;
    }
    
    return (m_status);
}
      
//------------------------------------------------------------------------------

int AltiModule::ENCAsyncCommandPendingLong(bool &pending) {

    ALTI_ENC_VMELONGCOMMAND_BITSTRING bs;

    // READ
    if ((m_status = m_alti->ENC_VmeLongCommand_Read(bs)) != 0) {
        CERR("ALTI: reading VME long command status register", "");
        return (m_status);
    }
      
    if (bs.Pending()) {
      pending = true;
    }
    else {
        pending = false;
    }
    
    return (m_status);
}
  
//------------------------------------------------------------------------------

int AltiModule::ENCBgoRequestStatusRead(bool &vme, bool &ttyp, bool &bgo0, bool &bgo1, bool &bgo2, bool &bgo3) {
  ALTI_ENC_REQUESTSTATUS_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->ENC_RequestStatus_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading ENC request control register", "");
    return (m_status);
  }
  
  vme = bs.VmeRequest();
  ttyp = bs.TtypeRequest();
  bgo0 = bs.BgoRequest(0);
  bgo1 = bs.BgoRequest(1);
  bgo2 = bs.BgoRequest(2);
  bgo3 = bs.BgoRequest(3);

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoRequestReset(const TTC_FIFO bgo) {
 ALTI_ENC_REQUESTCONTROL_BITSTRING bs;
  
 // READ
 if ((m_status = m_alti->ENC_RequestControl_Read(bs)) != SUCCESS) {
   CERR("ALTI: reading ENC request control register", "");
   return (m_status);
 }
 
 // MODIFY
 bs.ResetBgoRequest(bgo, true);
 if ((m_status = m_alti->ENC_RequestControl_Write(bs)) != SUCCESS) {
   CERR("ALTI: writng ENC request control register", "");
   return (m_status);
 }

 // REVERT
 bs.ResetBgoRequest(bgo, false);
 if ((m_status = m_alti->ENC_RequestControl_Write(bs)) != SUCCESS) {
   CERR("ALTI: writng ENC request control register", "");
   return (m_status);
 }
 
 return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoRequestReset() {
 ALTI_ENC_REQUESTCONTROL_BITSTRING bs;
  
 // READ
  if ((m_status = m_alti->ENC_RequestControl_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading ENC request control register", "");
    return (m_status);
  }
  
  // MODIFY
  bs.ResetAllRequests(true);
  if ((m_status = m_alti->ENC_RequestControl_Write(bs)) != SUCCESS) {
    CERR("ALTI: writng ENC request control register", "");
    return (m_status);
  }   

  // REVERT
  bs.ResetAllRequests(false);
  if ((m_status = m_alti->ENC_RequestControl_Write(bs)) != SUCCESS) {
    CERR("ALTI: writng ENC request control register", "");
    return (m_status);
  }   

  return (m_status);
}
    
//------------------------------------------------------------------------------

int AltiModule::ENCBgoCounterReset(const TTC_FIFO bgo) {
  ALTI_ENC_BGOCONTROL_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->ENC_BgoControl_Read((u_int) bgo, bs)) != SUCCESS) {
    CERR("ALTI: reading ENC BGo control register", "");
    return (m_status);
  }

  // MODIFY
  bs.ResetGrantCounter(true);
  if ((m_status = m_alti->ENC_BgoControl_Write((u_int) bgo, bs)) != SUCCESS) {
    CERR("ALTI: writing ENC BGo control register", "");
    return (m_status);
  }
  
  // REVERT
  bs.ResetGrantCounter(false);
  if ((m_status = m_alti->ENC_BgoControl_Write((u_int) bgo, bs)) != SUCCESS) {
    CERR("ALTI: writing ENC BGo control register", "");
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCounterReset() {
  ALTI_ENC_BGOCONTROL_BITSTRING bs;
  
  for(u_int bgo = 0; bgo < ALTI::ENC_BGOGRANTCOUNTER_NUMBER; bgo++) {
    const TTC_FIFO fifo = TTC_FIFO(bgo);
    m_status |= AltiModule::ENCBgoCounterReset(fifo);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCounterRead(const TTC_FIFO bgo, u_int &data) {

  if ((m_status = m_alti->ENC_BgoGrantCounter_Read((u_int) bgo, data)) != SUCCESS) {
    CERR("ALTI: reading ENC BGo-%d counter", bgo);
    return (m_status);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCBgoCounterRead(std::vector<u_int> &data) {
  u_int value;

  for (u_int bgo = 0; bgo < ALTI::ENC_BGOGRANTCOUNTER_NUMBER; bgo++) {

    if ((m_status = m_alti->ENC_BgoGrantCounter_Read((u_int) bgo, value)) != SUCCESS) {
    CERR("ALTI: reading ENC BGo-%d counter", bgo);
    return (m_status);
    }
    data.push_back(value);
  }
  
  return (m_status);
}

//------------------------------------------------------------------------------
//----------- Clock control and status  -------------
//------------------------------------------------------------------------------

int AltiModule::CLKStatusReadPLL(bool &lol, bool &los, bool &sticky_lol, bool &sticky_los) {
    ALTI_CLK_STATUS_BITSTRING bs0;
    ALTI_CLK_STATUSSTICKY_BITSTRING bs1;

    // READ
    if ((m_status = m_alti->CLK_Status_Read(bs0)) != 0) {
      CERR("ALTI: reading clock status sticky register", "");
      return (m_status);
    }
    if ((m_status = m_alti->CLK_StatusSticky_Read(bs1)) != 0) {
      CERR("ALTI: reading clock status sticky register", "");
      return (m_status);
    }
    
    std::string s;

    lol = bs0.PLLOutputClocksLOL();
    los = bs0.PLLInputClockStopped();

    sticky_lol = bs1.PLLOutputClocksLOL();
    sticky_los = bs1.PLLInputClockStopped();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKStatusClearPLL() { 
    unsigned int data;
    // 
    // READ
    if ((m_status = m_alti->CLK_Status_Read(data)) != 0) {
        CERR("ALTI: reading clock status register", "");
        return (m_status); 
    }

    // MODIFY
    data &= ~(ALTI_CLK_STATUS_BITSTRING::STAT_MASK_PLLINPUTCLOCKSTOPPED + ALTI_CLK_STATUS_BITSTRING::STAT_MASK_PLLOUTPUTCLOCKSLOL); // clear
    data |= (ALTI_CLK_STATUS_BITSTRING::STAT_MASK_JITTERCLEANERLOS + ALTI_CLK_STATUS_BITSTRING::STAT_MASK_JITTERCLEANERLOL + ALTI_CLK_STATUS_BITSTRING::STAT_MASK_JITTERCLEANERINTRRUPT); // don't clear
  
    unsigned int i, mask;
    for (i = 0; i < ALTI_CLK_STATUS_BITSTRING::CLOCKSTOPPED_NUMBER; i++) {
        mask = (ALTI_CLK_STATUS_BITSTRING::MASK_CLOCKSTOPPED[i]).number()[0];
        data |= mask; // don't clear
    }
    for (i = 0; i < ALTI_CLK_STATUS_BITSTRING::CLOCKFREQOUTOFRANGE_NUMBER; i++) {
        mask = (ALTI_CLK_STATUS_BITSTRING::MASK_CLOCKFREQOUTOFRANGE[i]).number()[0];
        data |= mask; // don't clear
    }
    for (i = 0; i < ALTI_CLK_STATUS_BITSTRING::CLOCKGLITCH_NUMBER; i++) {
        mask = (ALTI_CLK_STATUS_BITSTRING::MASK_CLOCKGLITCH[i]).number()[0];
        data |= mask; // don't clear
    }

    // WRITE
    if ((m_status = m_alti->CLK_Status_Write(data)) != 0) {
        CERR("ALTI: writing status register", "");
        return (m_status);
    }

    return (m_status);
}


//------------------------------------------------------------------------------

int AltiModule::CLKMonitorReadPLL(const EXT_CLK_SOURCE src, bool &stopped, bool &oor, bool &glitch) {
    ALTI_CLK_STATUSSTICKY_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_StatusSticky_Read(bs)) != 0) {
        CERR("ALTI: reading clock control sticky register", "");
        return (m_status);
    }

    stopped = bs.ClockStopped(src);
    oor = bs.ClockFreqOutOfRange(src);
    glitch = bs.ClockGlitch(src);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKMonitorStatusClearPLL() {
    unsigned int data;

    // READ
    if ((m_status = m_alti->CLK_Status_Read(data)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }

    // MODIFY
    data |= (ALTI_CLK_STATUS_BITSTRING::STAT_MASK_PLLINPUTCLOCKSTOPPED + ALTI_CLK_STATUS_BITSTRING::STAT_MASK_PLLOUTPUTCLOCKSLOL); // don't clear
    data |= (ALTI_CLK_STATUS_BITSTRING::STAT_MASK_JITTERCLEANERLOS + ALTI_CLK_STATUS_BITSTRING::STAT_MASK_JITTERCLEANERLOL + ALTI_CLK_STATUS_BITSTRING::STAT_MASK_JITTERCLEANERINTRRUPT); // don't clear
  
    unsigned int i, mask;
    for (i = 0; i < ALTI_CLK_STATUS_BITSTRING::CLOCKSTOPPED_NUMBER; i++) {
        mask = (ALTI_CLK_STATUS_BITSTRING::MASK_CLOCKSTOPPED[i]).number()[0];
        data &= ~mask; // clear
    }
    for (i = 0; i < ALTI_CLK_STATUS_BITSTRING::CLOCKFREQOUTOFRANGE_NUMBER; i++) {
        mask = (ALTI_CLK_STATUS_BITSTRING::MASK_CLOCKFREQOUTOFRANGE[i]).number()[0];
        data &= ~mask; // clear
    }
    for (i = 0; i < ALTI_CLK_STATUS_BITSTRING::CLOCKGLITCH_NUMBER; i++) {
        mask = (ALTI_CLK_STATUS_BITSTRING::MASK_CLOCKGLITCH[i]).number()[0];
        data &= ~mask; // clear
    }

    // WRITE
    if ((m_status = m_alti->CLK_Status_Write(data)) != 0) {
        CERR("ALTI: writing clock control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKResetPLL() {
    ALTI_CLK_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Control_Read(bs)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }
    // MODIFY
    bs.PLLReset(true);
    // WRITE
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: writing clock control register", "");

        return (m_status);
    }
    // MODIFY
    bs.PLLReset(false);
    // WRITE
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: writing clock control register", "");

        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKStickyBitsReset() {
    ALTI_CLK_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Control_Read(bs)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }

    // MODIFY
    bs.ResetStickyFlags(true);
    // WRITE
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: writing to Sticky_Reset register", "");
        return (m_status);
    }

    // MODIFY
    bs.ResetStickyFlags(false);
    // WRITE
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: writing to Sticky_Reset register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKInputSelectReadPLL(CLK_PLL_TYPE &clk) {
    ALTI_CLK_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Control_Read(bs)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }
    std::string s = bs.PLLSourceSelect();
    if (s == "FROMSWITCH") {
        clk = FROM_SWITCH;
    }
    else if (s == "FROMJITCLEANER") {
        clk = JITTER_CLEANER;
    }
    else {
        CERR("ALTI: unknown value \"%s\" for CLK type", s.c_str());
        return (-1);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKInputSelectWritePLL(const CLK_PLL_TYPE clk) {
    ALTI_CLK_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Control_Read(bs)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }

    // WRITE: assert PLL reset
    bs.PLLReset(true);
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: asserting PLL reset", "");
        return (m_status);
    }

    // WRITE: clock selection    
    bs.PLLSourceSelect((clk == FROM_SWITCH) ? "FROMSWITCH" : ((clk == JITTER_CLEANER) ? "FROMJITCLEANER" : ""));
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: selecting \"%s\" clock", CLK_PLL_NAME[clk].c_str());
        return (m_status);
    }

    // WRITE: resset PLL reset
    bs.PLLReset(false);
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: deasserting PLL reset", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKInputSelectTogglePLL() {
    ALTI_CLK_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Control_Read(bs)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }

    CLK_PLL_TYPE clk_toggle;
    std::string s = bs.PLLSourceSelect();
    if (s == "FROMSWITCH") {
      clk_toggle = CLK_PLL_TYPE::JITTER_CLEANER;
    }
    else if (s == "FROMJITCLEANER") {
      clk_toggle = CLK_PLL_TYPE::FROM_SWITCH;
    }
    else {
        CERR("ALTI: unknown value \"%s\" for CLK type", s.c_str());
        return (-1);
    }

    return CLKInputSelectWritePLL(clk_toggle);
}

//------------------------------------------------------------------------------

int AltiModule::CLKPhaseShiftPLL(const bool inc, const unsigned int steps) {
    ALTI_CLK_PHASESHIFTCONTROLSTATUS_BITSTRING bs;

    while (true) {
      // READ
      if ((m_status = m_alti->CLK_PhaseShiftControlStatus_Read(bs)) != 0) {
	CERR("ALTI: reading clock phase shift control and status register", "");
	return (m_status);
      }
      if(bs.PhaseShiftBusy()) continue;
      else break;
    }

    // MODIFY
    bs.IncDecSelect(inc);
    bs.Steps(steps);

    // WRITE
    if ((m_status = m_alti->CLK_PhaseShiftControlStatus_Write(bs)) != 0) {
        CERR("ALTI: writing clock phase shift control and status register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKPhasePositionReadPLL(int &steps) {
    ALTI_CLK_PHASESHIFTTOTALIZATOR_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_PhaseShiftTotalizator_Read(bs)) != 0) {
        CERR("ALTI: reading clock phase shift totalizator register", "");
        return (m_status);
    }

    steps = bs.ActualPhasePosition();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKPhaseShiftBusy(bool &busy) {
    ALTI_CLK_PHASESHIFTCONTROLSTATUS_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_PhaseShiftControlStatus_Read(bs)) != 0) {
        CERR("ALTI: reading clock phase shift control status register", "");
        return (m_status);
    }

    busy = bs.PhaseShiftBusy();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKPhaseResetPLL() {
    int steps = 1;
    bool busy = false;

    while(steps>0) {
      if ((m_status = CLKPhasePositionReadPLL(steps)) != SUCCESS) {
        CERR("ALTI: reading PLL clock phase", "");
        return (m_status);
      }
      if(steps>2000) steps = 2000;
      if ((m_status = CLKPhaseShiftPLL(0, steps)) != SUCCESS) {
        CERR("ALTI: %srementing PLL clock phase by %d steps", ((steps < 0) ? "inc" : "dec"), std::abs(steps));
        return (m_status);
      }
      
      // wait for the shift to be done
      busy = true;
      while(busy) {
	if ((m_status = CLKPhaseShiftBusy(busy)) != SUCCESS) {
	  CERR("ALTI: reading  PLL clock phase shift busy", "");
	  return (m_status);
	}
	usleep(10);
      }
      
      if ((m_status = CLKPhasePositionReadPLL(steps)) != SUCCESS) {
	CERR("ALTI: reading PLL clock phase", "");
	return (m_status);
      }
    }
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKStatusReadJitterCleaner(bool &intr, bool &lol, bool &los, bool &intr_sticky, bool &lol_sticky, bool &los_sticky) {
    ALTI_CLK_STATUS_BITSTRING bs0;
    ALTI_CLK_STATUSSTICKY_BITSTRING bs1;

    // READ
    if ((m_status = m_alti->CLK_Status_Read(bs0)) != 0) {
      CERR("ALTI: reading clock status register", "");
        return (m_status);
    }
    if ((m_status = m_alti->CLK_StatusSticky_Read(bs1)) != 0) {
        CERR("ALTI: reading clock status sticky register", "");
        return (m_status);
    }

    intr = bs0.JitterCleanerIntrrupt();
    los  = bs0.JitterCleanerLOS();
    lol  = bs0.JitterCleanerLOL();

    intr_sticky = bs1.JitterCleanerIntrrupt();
    los_sticky  = bs1.JitterCleanerLOS();
    lol_sticky  = bs1.JitterCleanerLOL();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKStatusClearJitterCleaner() {
    unsigned int data;

    // READ
    if ((m_status = m_alti->CLK_Status_Read(data)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }

    // MODIFY
    data |= (ALTI_CLK_STATUS_BITSTRING::STAT_MASK_PLLINPUTCLOCKSTOPPED + ALTI_CLK_STATUS_BITSTRING::STAT_MASK_PLLOUTPUTCLOCKSLOL); // don't clear
    data &= ~(ALTI_CLK_STATUS_BITSTRING::STAT_MASK_JITTERCLEANERLOS + ALTI_CLK_STATUS_BITSTRING::STAT_MASK_JITTERCLEANERLOL + ALTI_CLK_STATUS_BITSTRING::STAT_MASK_JITTERCLEANERINTRRUPT); // clear
  
    unsigned int i, mask;
    for (i = 0; i < ALTI_CLK_STATUS_BITSTRING::CLOCKSTOPPED_NUMBER; i++) {
        mask = (ALTI_CLK_STATUS_BITSTRING::MASK_CLOCKSTOPPED[i]).number()[0];
        data |= mask; // don't clear
    }
    for (i = 0; i < ALTI_CLK_STATUS_BITSTRING::CLOCKFREQOUTOFRANGE_NUMBER; i++) {
        mask = (ALTI_CLK_STATUS_BITSTRING::MASK_CLOCKFREQOUTOFRANGE[i]).number()[0];
        data |= mask; // don't clear
    }
    for (i = 0; i < ALTI_CLK_STATUS_BITSTRING::CLOCKGLITCH_NUMBER; i++) {
        mask = (ALTI_CLK_STATUS_BITSTRING::MASK_CLOCKGLITCH[i]).number()[0];
        data |= mask; // don't clear
    }

    // WRITE
    if ((m_status = m_alti->CLK_Status_Write(data)) != 0) {
        CERR("ALTI: writing clock control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKResetJitterCleaner() {
    ALTI_CLK_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Control_Read(bs)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }
    // MODIFY
    bs.JitterCleanerReset(true);
    // WRITE
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: writing clock control register", "");

        return (m_status);
    }
    // MODIFY
    bs.JitterCleanerReset(false);
    // WRITE
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: writing clock control register", "");

        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKInputSelectReadJitterCleaner(CLK_JC_TYPE &insel) {
    ALTI_CLK_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Control_Read(bs)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }

    std::string s = bs.JitterCleanerInputSel();
    if (s == "FROMSWITCH") {
      insel = CLK_JC_TYPE::SWITCH;
    }
    else if (s == "FROMOSCILLATOR") {
      insel = CLK_JC_TYPE::OSCILLATOR;
    }
    else {
      CERR("ALTI: unknown value \"%s\" for CLK type", s.c_str());
      return (-1);
    }
    
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKInputSelectWriteJitterCleaner(const CLK_JC_TYPE insel) {
    ALTI_CLK_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Control_Read(bs)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }

    // MODIFY: clock selection    
    bs.JitterCleanerInputSel((insel == CLK_JC_TYPE::OSCILLATOR) ? "FROMOSCILLATOR" : ((insel == CLK_JC_TYPE::SWITCH) ? "FROMSWITCH" : ""));
  
    // WRITE
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: writing clock control register", "");

        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerResync() {
    if ((m_status = m_net->write(m_SI5344BPdnHardRstSync, (unsigned int) 0x04)) != 0) { // SYNC
        CERR("ALTI: writing 0x04 to \"%s\"", m_SI5344BPdnHardRstSync.name().c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerIntFlagsRead(std::vector<bool> &los_flg, std::vector<bool> &oof_flg, bool &lol_flg, bool &hold_flg, bool &losxaxb_flg, bool &smbus_timeout_flg, bool &cal_flg_pll, bool &sysincal_flg) {
    unsigned int data, i;
    los_flg.clear();
    oof_flg.clear();

    if ((m_status = m_net->read(m_SI5344BOofLosStickyFlg, data)) != 0) { // OOF_LOS_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BOofLosStickyFlg.name().c_str());
        return (m_status);
    }
    for (i = 0; i < 4; i++) { // 4 inputs
        los_flg.push_back(data & (0x01 << i)); // LOS_FLG[i]
        oof_flg.push_back(data & (0x10 << i)); // OOF_FLG[i]
    }
    if ((m_status = m_net->read(m_SI5344BHoldLolStickyFlg, data)) != 0) { // HOLD_LOL_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BHoldLolStickyFlg.name().c_str());
        return (m_status);
    }
    lol_flg = (data & 0x02); // LOL_FLG
    hold_flg = (data & 0x20); // HOLD_FLG
    if ((m_status = m_net->read(m_SI5344BStatusStickyFlg, data)) != 0) { // STATUS_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BStatusStickyFlg.name().c_str());
        return (m_status);
    }
    losxaxb_flg = (data & 0x02); // LOSXAXB_FLG
    smbus_timeout_flg = (data & 0x20); // SMBUS_TIMEOUT_FLG
    sysincal_flg = (data & 0x01); // SYSINCAL_FLG
    if ((m_status = m_net->read(m_SI5344BPllCalStickyFlg, data)) != 0) { // PLL_CAL_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BPllCalStickyFlg.name().c_str());
        return (m_status);
    }
    cal_flg_pll = (data & 0x20); // CAL_FLG_PLL

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerIntMasksRead(std::vector<bool> &los_intr_msk, std::vector<bool> &oof_intr_msk, bool &lol_intr_msk, bool &hold_intr_msk, bool &losxaxb_intr_msk, bool &smbus_timeout_intr_msk, bool &cal_intr_msk, bool &sysincal_intr_msk) {
    unsigned int data, i;
    los_intr_msk.clear();
    oof_intr_msk.clear();

    if ((m_status = m_net->read(m_SI5344BOofLosIntrMsk, data)) != 0) { // OOF_LOS_INTR_MSK
        CERR("ALTI: reading \"%s\"", m_SI5344BOofLosIntrMsk.name().c_str());
        return (m_status);
    }
    for (i = 0; i < 4; i++) { // 4 inputs
        los_intr_msk.push_back(data & (0x01 << i)); // LOS_INTR_MSK[i]
        oof_intr_msk.push_back(data & (0x10 << i)); // OOF_INTR_MSK[i]
    }
    if ((m_status = m_net->read(m_SI5344BHoldLolIntrMsk, data)) != 0) { // HOLD_LOL_INTR_MSK
        CERR("ALTI: reading \"%s\"", m_SI5344BHoldLolIntrMsk.name().c_str());
        return (m_status);
    }
    lol_intr_msk = (data & 0x02); // LOL_INTR_MSK
    hold_intr_msk = (data & 0x20); // HOLD_INTR_MSK
    if ((m_status = m_net->read(m_SI5344BStatusIntrMsk, data)) != 0) { // STATUS_INTR_MSK
        CERR("ALTI: reading \"%s\"", m_SI5344BStatusIntrMsk.name().c_str());
        return (m_status);
    }
    losxaxb_intr_msk = (data & 0x02); // LOSXAXB_INTR_MSK
    smbus_timeout_intr_msk = (data & 0x20); // SMBUS_TIMEOUT_INTR_MSK
    sysincal_intr_msk = (data & 0x01); // SYSINCAL_INTR_MSK
    if ((m_status = m_net->read(m_SI5344BPllCalIntrMsk, data)) != 0) { // PLL_CAL_INTR_MSK
        CERR("ALTI: reading \"%s\"", m_SI5344BPllCalIntrMsk.name().c_str());
        return (m_status);
    }
    cal_intr_msk = (data & 0x20); // CAL_INTR_MSK

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerStatusClear() {
    unsigned int flg, intr_msk, data;

    if ((m_status = m_net->read(m_SI5344BOofLosStickyFlg, flg)) != 0) { // OOF_LOS_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BOofLosStickyFlg.name().c_str());
        return (m_status);
    }
    if ((m_status = m_net->read(m_SI5344BOofLosIntrMsk, intr_msk)) != 0) { // OOF_LOS_INTR_MSK
        CERR("ALTI: reading \"%s\"", m_SI5344BOofLosIntrMsk.name().c_str());
        return (m_status);
    }
    //data = uint8_t(~(flg & (~intr_msk) & 0xff))); // clear bits 0..7, if needed: LOS_FLG[3..0], OOF_FLG[3..0]
    data = uint8_t(0x00); // clear bits 0..7 (Rev B issues)
    if ((m_status = m_net->write(m_SI5344BOofLosStickyFlg, data)) != 0) { // OOF_LOS_STICKY_FLG
        CERR("ALTI: writing 0x%02x to \"%s\"", data, m_SI5344BOofLosStickyFlg.name().c_str());
        return (m_status);
    }

    if ((m_status = m_net->read(m_SI5344BHoldLolStickyFlg, flg)) != 0) { // HOLD_LOL_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BHoldLolStickyFlg.name().c_str());
        return (m_status);
    }
    if ((m_status = m_net->read(m_SI5344BHoldLolIntrMsk, intr_msk)) != 0) { // HOLD_LOL_INTR_MSK
        CERR("ALTI: reading \"%s\"", m_SI5344BHoldLolIntrMsk.name().c_str());
        return (m_status);
    }
    data = uint8_t(~(flg & (~intr_msk) & 0x22)); // clear bits 1 and 5, if needed: LOL_FLG, HOLD_FLG
    if ((m_status = m_net->write(m_SI5344BHoldLolStickyFlg, data)) != 0) { // HOLD_LOL_STICKY_FLG
        CERR("ALTI: writing 0x%02x to \"%s\"", data, m_SI5344BHoldLolStickyFlg.name().c_str());
        return (m_status);
    }

    if ((m_status = m_net->read(m_SI5344BStatusStickyFlg, flg)) != 0) { // STATUS_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BStatusStickyFlg.name().c_str());
        return (m_status);
    }
    if ((m_status = m_net->read(m_SI5344BStatusIntrMsk, intr_msk)) != 0) { // STATUS_INTR_MSK
        CERR("ALTI: reading \"%s\"", m_SI5344BStatusIntrMsk.name().c_str());
        return (m_status);
    }
    data = uint8_t(~(flg & (~intr_msk) & 0x23)); // clear bits 0, 1 and 5, if needed: SYSINCAL_FLG, LOSXAXB_FLG, SMBUS_TIMEOUT_FLG
    if ((m_status = m_net->write(m_SI5344BStatusStickyFlg, data)) != 0) { // STATUS_STICKY_FLG
        CERR("ALTI: writing 0x%02x to \"%s\"", data, m_SI5344BStatusStickyFlg.name().c_str());
        return (m_status);
    }

    if ((m_status = m_net->read(m_SI5344BPllCalStickyFlg, flg)) != 0) { // PLL_CAL_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BPllCalStickyFlg.name().c_str());
        return (m_status);
    }
    if ((m_status = m_net->read(m_SI5344BPllCalIntrMsk, intr_msk)) != 0) { // PLL_CAL_INTR_MSK
        CERR("ALTI: reading \"%s\"", m_SI5344BPllCalIntrMsk.name().c_str());
        return (m_status);
    }
    data = uint8_t(~(flg & (~intr_msk) & 0x20)); // clear bit 5, if needed: CAL_FLG_PLL
    if ((m_status = m_net->write(m_SI5344BPllCalStickyFlg, data)) != 0) { // PLL_CAL_STICKY_FLG
        CERR("ALTI: writing 0x%02x to \"%s\"", data, m_SI5344BPllCalStickyFlg.name().c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::pagedI2CDeviceRead(const I2CNetwork::I2CNode &node, const uint8_t page, const uint8_t offs, unsigned int &data) {
    int rtnv;
    I2CInterface *ii2c = m_net->getI2CBus(node.bus());
    unsigned int iswc, isel, pageAddr, devAddr;
    iswc = m_net->getSwitchAddressI2CDevice(node.bus(), node.dev());
    isel = m_net->getSwitchSelectI2CDevice(node.bus(), node.dev());
    pageAddr = m_net->getPageOffsetI2CDevice(node.bus(), node.dev());
    devAddr = m_net->getAddressI2CDevice(node.bus(), node.dev());
    uint8_t datc[I2CInterface::MAX_BLOCK];

    // write I2C switch
    unsigned int numb(1);
    CDBG("write switch: address = 0x%02x, select = 0x%02x", iswc, isel);
    if ((rtnv = ii2c->DataWrite(iswc, isel, numb, datc, I2CInterface::BYTE)) != SUCCESS) {
        CERR("writing I2C bus/device %d/%d, switch 0x%02x, select 0x%02x", node.bus(), node.dev(), iswc, isel);
        return (rtnv);
    }

    // write I2C page select
    datc[0] = page;
    numb = 1;
    CDBG("write page: address = 0x%02x, select = 0x%02x", devAddr, addr_hi);
    if ((rtnv = ii2c->DataWrite(devAddr, pageAddr, numb, datc, I2CInterface::DATA)) != SUCCESS) {
        CERR("writing I2C bus/device %d/%d, offset 0x%02x, select 0x%02x", node.bus(), node.dev(), pageAddr, page);
        return (rtnv);
    }

    // read I2C data
    numb = 1;
    if ((rtnv = ii2c->DataRead(devAddr, offs, numb, datc, I2CInterface::DATA)) != SUCCESS) {
        CERR("reading I2C bus/device %d/%d, page 0x%02x, offset 0x%02x", node.bus(), node.dev(), page, offs);
        return (rtnv);
    }
    data = datc[0];

    //return (m_status);
    return 0;
}

//------------------------------------------------------------------------------

int AltiModule::pagedI2CDeviceWrite(const I2CNetwork::I2CNode &node, const uint8_t page, const uint8_t offs, const unsigned int data) {
    int rtnv;
    I2CInterface *ii2c = m_net->getI2CBus(node.bus());
    unsigned int iswc, isel, pageAddr, devAddr;
    iswc = m_net->getSwitchAddressI2CDevice(node.bus(), node.dev());
    isel = m_net->getSwitchSelectI2CDevice(node.bus(), node.dev());
    pageAddr = m_net->getPageOffsetI2CDevice(node.bus(), node.dev());
    devAddr = m_net->getAddressI2CDevice(node.bus(), node.dev());
    uint8_t datc[I2CInterface::MAX_BLOCK];

    // write I2C switch
    unsigned int numb(1);
    CDBG("write switch: address = 0x%02x, select = 0x%02x", iswc, isel);
    if ((rtnv = ii2c->DataWrite(iswc, isel, numb, datc, I2CInterface::BYTE)) != SUCCESS) {
        CERR("writing I2C bus/device %d/%d, switch 0x%02x, select 0x%02x", node.bus(), node.dev(), iswc, isel);
        return (rtnv);
    }

    // write I2C page select
    datc[0] = page;
    numb = 1;
    CDBG("write page: address = 0x%02x, select = 0x%02x", devAddr, addr_hi);
    if ((rtnv = ii2c->DataWrite(devAddr, pageAddr, numb, datc, I2CInterface::DATA)) != SUCCESS) {
        CERR("writing I2C bus/device %d/%d, offset 0x%02x, select 0x%02x", node.bus(), node.dev(), pageAddr, page);
        return (rtnv);
    }

    // write I2C data
    datc[0] = data;
    numb = 1;
    if ((rtnv = ii2c->DataWrite(devAddr, offs, numb, datc, I2CInterface::DATA)) != SUCCESS) {
        CERR("writing I2C bus/device %d/%d, page 0x%02x, offset 0x%02x", node.bus(), node.dev(), page, offs);
        return (rtnv);
    }

    //return (m_status);
    return 0;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerConfigRead(const std::string &fn_in, const std::string &fn_out) { // read configuration (addresses given in input file) into output file

    // open input file
    std::ifstream fs_in(fn_in);
    if (!fs_in) {
        CERR("cannot open input file \"%s\"\n", fn_in.c_str());
        return (m_status = WRONGPAR);
    }

    // open output file
    std::ofstream fs_out(fn_out);
    if (!fs_out) {
        CERR("cannot open output file \"%s\"\n", fn_out.c_str());
        return (m_status = WRONGPAR);
    }

    unsigned int line(0), lnum(0), addr, addr_hi, addr_lo;
    unsigned int data;
    std::string buf;
    std::vector<std::string> arg;
    char xbuf[1024];

    I2CNetwork::I2CNode node;
    const std::string nodeName = "BUS[0]/CLK_GEN_CLEANER_P2/DESIGN_ID";
    if ((m_status = m_net->getI2CNode(nodeName, node)) != SUCCESS) {
        CERR("Cannot find I2C node \"%s\"", nodeName.c_str());
    }

    // read address from input file
    while (getline(fs_in, buf)) {
        if ((buf[0] == '#') or (buf == "Address,Data")) {
            if (buf.find("# Delay") != std::string::npos) {
                if (runDelay(line, buf) != SUCCESS) {
                    CERR("running delay command from line %d \"%s\"", line, buf.c_str());
                    return (m_status);
                }
            }
            line++;
            continue;
        }
        if (AltiCommon::splitString(buf, ",", arg) < 1) {
            CERR("reading arguments from line %d \"%s\" (expected \"addr ...\")", line, buf.c_str());
            return (m_status = WRONGPAR);
        }
        if (AltiCommon::readNumber(arg[0], addr)) {
            CERR("reading address from line %d \"%s\"", line, buf.c_str());
            return (m_status = WRONGPAR);
        }
        addr_hi = (addr >> PAGE_ADDR_SHFT) & PAGE_ADDR_MASK;
        addr_lo = addr & ADDR_ADDR_MASK;

        // instead:
        pagedI2CDeviceRead(node, addr_hi, addr_lo, data);

        // write address and data to output file
        std::sprintf(xbuf, "0x%04X,0x%02X", addr, data);
        fs_out << xbuf << std::endl;
        line++;
        lnum++;
    }
    COUT("read %d (addr) line%s from input file \"%s\" and wrote data to output file \"%s\"", lnum, (lnum > 1) ? "s" : "" , fn_in.c_str(), fn_out.c_str());
    std::printf("read from CLK jitter cleaner input file \"%s\" and output file \"%s\"\n", fn_in.c_str(), fn_in.c_str());

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerConfigWrite(const std::string &fn_in) { // write configuration from input file

    if (fn_in.empty() or (fn_in == "<DEFAULT>")) return (CLKJitterCleanerConfigWrite());

    // open input file
    std::ifstream fs_in(fn_in);
    if (!fs_in) {
        CERR("cannot open input file \"%s\"\n", fn_in.c_str());
        return (m_status = WRONGPAR);
    }

    unsigned int line(0), lnum(0), addr, addr_hi, addr_lo;
    unsigned int data;
    std::string buf;
    std::vector<std::string> arg;

    I2CNetwork::I2CNode node;
    const std::string nodeName = "BUS[0]/CLK_GEN_CLEANER_P2/DESIGN_ID";
    if ((m_status = m_net->getI2CNode(nodeName, node)) != SUCCESS) {
        CERR("Cannot find I2C node \"%s\"", nodeName.c_str());
    }

    // read address from input file
    while (getline(fs_in, buf)) {
        if ((buf[0] == '#') or (buf == "Address,Data")) {
            if (buf.find("# Delay") != std::string::npos) {
                if (runDelay(line, buf) != SUCCESS) {
                    CERR("running delay command from line %d \"%s\"", line, buf.c_str());
                    return (m_status);
                }
            }
            line++;
            continue;
        }
        if (AltiCommon::splitString(buf, ",", arg) < 2) {
            CERR("reading arguments from line %d \"%s\" (expected \"addr, data\")", line, buf.c_str());
            return (m_status = WRONGPAR);
        }
        if (AltiCommon::readNumber(arg[0], addr)) {
            CERR("reading address from line %d \"%s\"", line, buf.c_str());
            return (m_status = WRONGPAR);
        }
        if (AltiCommon::readNumber(arg[1], data)) {
            CERR("reading data from line %d \"%s\"", line, buf.c_str());
            return (m_status = WRONGPAR);
        }
        addr_hi = (addr >> PAGE_ADDR_SHFT) & PAGE_ADDR_MASK;
        addr_lo = addr & ADDR_ADDR_MASK;

        // instead:
        pagedI2CDeviceWrite(node, addr_hi, addr_lo, data);

        line++;
        lnum++;
    }
    COUT("read %d (addr) line%s from input file \"%s\"", lnum, (lnum > 1) ? "s" : "" , fn_in.c_str());
    std::printf("wrote input file \"%s\" to CLK jitter cleaner\n", fn_in.c_str());

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerConfigWrite() { // write default configuration

    I2CNetwork::I2CNode node;
    const std::string nodeName = "BUS[0]/CLK_GEN_CLEANER_P2/DESIGN_ID";
    if ((m_status = m_net->getI2CNode(nodeName, node)) != SUCCESS) {
        CERR("Cannot find I2C node \"%s\"", nodeName.c_str());
    }

    unsigned int addr, addr_hi, addr_lo, data, line(0);
    static struct timespec tdel; tdel.tv_sec = 0L; tdel.tv_nsec = 300000000L; // 300 msec

    for (unsigned int i = 0; i < JITTER.NUMBER; i++) {

      
        addr = JITTER.si5344_revb_registers[i].address;
        addr_lo = addr & 0xff;
        addr_hi = (addr >> 8) & 0xff;
        data = JITTER.si5344_revb_registers[i].value;

        pagedI2CDeviceWrite(node, addr_hi, addr_lo, data);
        line++;

        // run delay if end of configuration preamble
        if ((addr == 0x0540) and (data == 0x01)) nanosleep(&tdel, nullptr);
    }

    std::printf("wrote %d (addr,data) line%s to CLK jitter cleaner\n", line, (line > 1) ? "s" : "");

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerStatusRead(std::vector<bool> &oof, std::vector<bool> &los, bool &hold, bool &lol) {
    unsigned int data, i;
    oof.clear();
    los.clear();

    if ((m_status = m_net->read(m_SI5344BOofLosFlg, data)) != 0) { // OOF_LOS_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BOofLosFlg.name().c_str());
        return (m_status);
    }
    for (i = 0; i < 4; i++) { // 4 inputs
        oof.push_back(data & (0x10 << i)); // OOF[i]
        los.push_back(data & (0x01 << i)); // LOS[i]
    }
    if ((m_status = m_net->read(m_SI5344BHoldLolFlg, data)) != 0) { // HOLD_LOL_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BHoldLolFlg.name().c_str());
        return (m_status);
    }
    hold = (data & 0x20); // HOLD
    lol = (data & 0x02); // LOL

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerStickyStatusRead(std::vector<bool> &oof, std::vector<bool> &los, bool &hold, bool &lol) {
    unsigned int data, i;
    oof.clear();
    los.clear();

    if ((m_status = m_net->read(m_SI5344BOofLosStickyFlg, data)) != 0) { // OOF_LOS_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BOofLosStickyFlg.name().c_str());
        return (m_status);
    }
    for (i = 0; i < 4; i++) { // 4 inputs
        oof.push_back(data & (0x10 << i)); // OOF[i]
        los.push_back(data & (0x01 << i)); // LOS[i]
    }
    if ((m_status = m_net->read(m_SI5344BHoldLolStickyFlg, data)) != 0) { // HOLD_LOL_STICKY_FLG
        CERR("ALTI: reading \"%s\"", m_SI5344BHoldLolStickyFlg.name().c_str());
        return (m_status);
    }
    hold = (data & 0x20); // HOLD
    lol = (data & 0x02); // LOL

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKJitterCleanerDesignIDRead(std::string &designid) {
    //designid.clear();
    designid.resize(8);

    if ((m_status = m_net->read(m_SI5344BDesignId, designid)) != 0) { // DESIGN_ID
        CERR("ALTI: reading \"%s\"", m_SI5344BDesignId.name().c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::runDelay(const unsigned int line, const std::string &buf) {

    std::vector<std::string> arg;
    int num;
    unsigned int tim;

    if ((num = AltiCommon::splitString(buf, " ", arg)) < 1) {
        CERR("reading arguments from line %d \"%s\" (expected \"# Delay xxx msec\")", line, buf.c_str());
        return (m_status = WRONGPAR);
    }
    if (num != 4) {
        CERR("wrong number arguments from line %d \"%s\" (expected %d\")", line, buf.c_str());
        return (m_status = WRONGPAR);
    }
    if (AltiCommon::readNumber(arg[2], tim)) {
        CERR("reading delay time from line %d \"%s\"", line, buf.c_str());
        return (m_status = WRONGPAR);
    }
    if (arg[3] != "msec") {
        CERR("reading delay unit from line %d \"%s\" (expected \"msec\")", line, buf.c_str());
        return (m_status = WRONGPAR);
    }

    struct timespec req, rem;
    if (tim < THOUSAND) {
        req.tv_sec = 0;
        req.tv_nsec = tim*THOUSAND*THOUSAND;
    }
    else {
        req.tv_sec = tim/THOUSAND;
        req.tv_nsec = (tim%THOUSAND)*THOUSAND*THOUSAND;
    }
    if (nanosleep(&req, &rem) < 0) {
        CERR("nanosleep(%d sec, %d nsec) returns \"%s\"", req.tv_sec, req.tv_nsec, strerror(errno));
        return (m_status = FAILURE);
    }

    return (m_status);
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::CNTBcidMaskRead(const unsigned int bcid, bool &mask) {

    // check BCID
    CHECKRANGE0(bcid, BCID_NUMBER - 1, "CNT BCID number");

    unsigned int wrd(bcid/WORD_SIZE), off(bcid % WORD_SIZE), data;

    // read BCID mask
    if ((m_status = m_alti->CNT_BcidMask_Read(wrd, data)) != SUCCESS) {
        CERR("ALTI: reading CNT BCID mask %4d", bcid);
        return (m_status);
    }
    mask = data & (0x00000001 << off);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTBcidMaskWrite(const unsigned int bcid, const bool mask) {

    // check BCID
    CHECKRANGE0(bcid, BCID_NUMBER - 1, "CNT BCID number");

    unsigned int wrd(bcid/WORD_SIZE), off(bcid % WORD_SIZE), data;

    // read BCID mask
    if ((m_status = m_alti->CNT_BcidMask_Read(wrd, data)) != SUCCESS) {
        CERR("ALTI: reading CNT BCID mask %4d", bcid);
        return (m_status);
    }

    // modify BGRP data
    data = mask ? (data | (0x00000001 << off)) : (data & (~(0x00000001 << off)));

    // write BCID mask
    if ((m_status = m_alti->CNT_BcidMask_Write(wrd, data)) != SUCCESS) {
        CERR("ALTI: writing CNT BCID mask %4d", bcid);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTBcidMaskRead(std::vector<bool> &mask) {

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // read BCID mask using block transfer
    if ((m_status = m_alti->CNT_BcidMask_Read(dseg.segment())) != SUCCESS) {
        CERR("ALTI: reading CNT BCID mask using block transfer", "");
        return (m_status);
    }

    unsigned int wrd, bit, off, num(0);

    // reset BCID mask vector
    mask.clear();
    mask.resize(BCID_NUMBER);

    // copy BCID mask from CMEM segment to vector
    for (wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
        off = 0x00000001;
        for (bit = 0; bit < WORD_SIZE; bit++) {
            mask[num] = (dseg[wrd] & off);
            off <<= 1;
            num++;
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTBcidMaskWrite(const std::vector<bool> &mask) {

    unsigned int wrd, bit, off, num(0);

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // copy BCID mask from vector to CMEM segment
    for (wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
        off = 0x00000001;
        dseg[wrd] = 0x00000000;
        for (bit = 0; bit < WORD_SIZE; bit++) {
            if (mask[num]) dseg[wrd] |= off;
            off <<= 1;
            num++;
        }
    }

    // write BCID mask using block transfer
    if ((m_status = m_alti->CNT_BcidMask_Write(dseg.segment())) != SUCCESS) {
        CERR("ALTI: writing CNT BCID mask using block transfer", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTBcidMaskRead(std::vector<unsigned int> &mask) {

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_WORD_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // read BCID mask using block transfer
    if ((m_status = m_alti->CNT_BcidMask_Read(dseg.segment())) != SUCCESS) {
        CERR("ALTI: reading CNT BCID mask using block transfer", "");
        return (m_status);
    }

    unsigned int wrd, bit, off, num(0);

    // reset BCID mask vector
    mask.clear();
    mask.resize(BCID_NUMBER);

    // copy BCID mask from CMEM segment to vector
    for (wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
        off = 0x00000001;
        for (bit = 0; bit < WORD_SIZE; bit++) {
            mask[num] = (dseg[wrd] & off) ? 0x00000001 : 0x00000000;
            off <<= 1;
            num++;
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTBcidMaskWrite(const std::vector<unsigned int> &mask) {

    unsigned int wrd, bit, off, num(0);

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_WORD_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
        return (FAILURE);
    }

    // copy BCID mask from vector to CMEM segment
    for (wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
        off = 0x00000001;
        dseg[wrd] = 0x00000000;
        for (bit = 0; bit < WORD_SIZE; bit++) {
            if (mask[num] != 0x00000000) dseg[wrd] |= off;
            off <<= 1;
            num++;
        }
    }

    // write BCID mask using block transfer
    if ((m_status = m_alti->CNT_BcidMask_Write(dseg.segment())) != SUCCESS) {
        CERR("ALTI: writing CNT BCID mask using block transfer", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTBcidMaskRead(const std::string &fn) {

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_WORD_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // read BCID mask using block transfer
    if ((m_status = m_alti->CNT_BcidMask_Read(dseg.segment())) != SUCCESS) {
        CERR("ALTI: reading CNT BCID mask using block transfer", "");
        return (m_status);
    }

    // copy data from CMEM segment into file
    if ((m_status = AltiCommon::writeFile(fn, BCID_WORD_NUMBER, dseg.data())) != AltiCommon::SUCCESS) {
        CERR("ALTI: writing data from CNT BCID mask to file \"%s\"", fn.c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTBcidMaskWrite(const std::string &fn) {

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_WORD_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // copy data from file into CMEM segment
    if ((m_status = AltiCommon::readFile(fn, BCID_WORD_NUMBER, dseg.data())) != AltiCommon::SUCCESS) {
        CERR("ALTI: reading data from CNT BCID mask from file \"%s\"", fn.c_str());
        return (m_status);
    }

    // write BCID mask using block transfer
    if ((m_status = m_alti->CNT_BcidMask_Write(dseg.segment())) != SUCCESS) {
        CERR("ALTI: writing CNT BCID mask using block transfer", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTBcidOffsetRead(unsigned int &off) {

    // read BCID offset
    if ((m_status = m_alti->CNT_BcidOffset_Read(off)) != SUCCESS) {
        CERR("ALTI: reading CNT BCID offset", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTBcidOffsetWrite(const unsigned int off) {

    // write BCID offset
    if ((m_status = m_alti->CNT_BcidOffset_Write(off)) != SUCCESS) {
        CERR("ALTI: writing CNT BCID offset", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTControlPrint() {

    // print control
    ALTI_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CNT enable", "");
        return (m_status);
    }
    ctl.print();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTEnableRead(bool &ena, bool &sta) {

    // read CNT enable
    ALTI_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CNT enable", "");
        return (m_status);
    }
    ena = ctl.EnableCounters();
    sta = ctl.EnableStatus();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTEnableWrite(const bool ena) {

    // read-modify-write CNT enable
    ALTI_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CNT enable", "");
        return (m_status);
    }
    ctl.EnableCounters(ena);
    if ((m_status = m_alti->CNT_Control_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CNT enable", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTEnableWait(const bool ena) {

    unsigned int itim(0);

    while (true) {
        ALTI_CNT_CONTROL_BITSTRING ctl;
        if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
            CERR("ALTI: reading CNT enable", "");
            return (m_status);
        }
        if (ena != ctl.EnableCounters()) {
            CERR("ALTI: waiting for CNT enable to be \"%s\" while it is \"%s\"", ena ? "ENABLED" : "DISABLED", ctl.EnableCounters() ? "ENABLED" : "DISABLED");
            ctl.print();
            return (WRONGPAR);
        }
        if(ctl.EnableStatus() == ena) return (m_status);
        if ((++itim) == CNT_CONTROL_TIMEOUT) {
            CERR("ALTI: timeout waiting for CNT enable status to be \"%s\":", ena ? "ACTIVE" : "NOT_ACTIVE");
            return (TIMEOUT);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTClearWrite() {

    // read-modify-write CNT clear
    ALTI_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CNT clear", "");
        return (m_status);
    }
    ctl.ClearCounters(true);
    if ((m_status = m_alti->CNT_Control_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CNT clear", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTClearWait() {

    unsigned int itim(0);

    while (true) {
        ALTI_CNT_CONTROL_BITSTRING ctl;
        if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
            CERR("ALTI: reading CNT clear", "");
            return (m_status);
        }
        if (!ctl.ClearBusy()) return (m_status);
        if ((++itim) == CNT_CONTROL_TIMEOUT) {
            CERR("ALTI: timeout waiting for CNT clear to finish", "");
            return (TIMEOUT);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTCopyWrite() {

    // read-modify-write CNT copy
    ALTI_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CNT copy", "");
        return (m_status);
    }
    ctl.CopyCounters(true);
    if ((m_status = m_alti->CNT_Control_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CNT copy", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTCopyWait() {

    unsigned int itim(0);

    while (true) {
        ALTI_CNT_CONTROL_BITSTRING ctl;
        if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
            CERR("ALTI: reading CNT copy", "");
            return (m_status);
        }
        if (!ctl.CopyBusy()) return (m_status);
        if ((++itim) == CNT_CONTROL_TIMEOUT) {
            CERR("ALTI: timeout waiting for CNT copy to finish", "");
            return (TIMEOUT);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTCopyClearWrite() {

    // read-modify-write CNT copy
    ALTI_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CNT copy/clear", "");
        return (m_status);
    }
    ctl.CopyCounters(true);
    ctl.ClearCounters(true);
    if ((m_status = m_alti->CNT_Control_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CNT copy/clear", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTCopyClearWait() {

    unsigned int itim(0);

    while (true) {
        ALTI_CNT_CONTROL_BITSTRING ctl;
        if ((m_status = m_alti->CNT_Control_Read(ctl)) != SUCCESS) {
            CERR("ALTI: reading CNT copy/clear", "");
            return (m_status);
        }
        if (!ctl.CopyBusy() && !ctl.ClearBusy()) return (m_status);
        if ((++itim) == CNT_CONTROL_TIMEOUT) {
            CERR("ALTI: timeout waiting for CNT copy/clear to finish", "");
            return (TIMEOUT);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTRequestPrint() {

    // print request
    ALTI_CNT_REQUEST_BITSTRING ctl;
    if ((m_status = m_alti->CNT_Request_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CNT request", "");
        return (m_status);
    }
    ctl.print();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTRequestWrite(const bool cnt, const bool trn, const bool tst) {

    // print request
    ALTI_CNT_REQUEST_BITSTRING ctl;
    ctl.EnableCountOverflow(cnt ? "ENABLED" : "DISABLED");
    ctl.EnableTurnOverflow(trn ? "ENABLED" : "DISABLED");
    ctl.AssertRequest(tst ? "ENABLED" : "DISABLED");
    if ((m_status = m_alti->CNT_Request_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CNT request", "");
        return (m_status);
    }

    return (m_status);
}

int AltiModule::CNTEdgeDetectionRead(const CNT_TYPE signal, bool &ena) {

  ALTI_CNT_ENABLEEDGEDETECTION_BITSTRING bs;

  if ((m_status = m_alti->CNT_EnableEdgeDetection_Read(bs)) != 0) {
    CERR("ALTI: reading counter edge detection register", "");
    return (m_status);
  }
  
  switch (signal) {
  case CNT_L1A:
    ena = bs.L1a();
    break;
  case CNT_TTR1:
    ena = bs.TestTrig(0);
    break;
  case CNT_TTR2:
    ena = bs.TestTrig(1);
    break;
  case CNT_TTR3:
    ena = bs.TestTrig(2);
    break;
  case CNT_BGO0:
    ena = bs.Bgo(0);
    break;
  case CNT_BGO1:
    ena = bs.Bgo(1);
    break;
  case CNT_BGO2:
    ena = bs.Bgo(2);
    break;
  case CNT_BGO3:
    ena = bs.Bgo(3);
    break;
  default:
    break;
  }
  
  return (m_status);
}
//------------------------------------------------------------------------------

int AltiModule::CNTEdgeDetectionWrite(const CNT_TYPE signal, const bool ena) {

  ALTI_CNT_ENABLEEDGEDETECTION_BITSTRING bs;

  if ((m_status = m_alti->CNT_EnableEdgeDetection_Read(bs)) != 0) {
    CERR("ALTI: reading counter edge detection register", "");
    return (m_status);
  }
  
  switch (signal) {
  case CNT_L1A:
    bs.L1a(ena);
    break;
  case CNT_TTR1:
    bs.TestTrig(0, ena);
    break;
  case CNT_TTR2:
    bs.TestTrig(1, ena);
    break;
  case CNT_TTR3:
    bs.TestTrig(2, ena);
    break;
  case CNT_BGO0:
    bs.Bgo(0, ena);
    break;
  case CNT_BGO1:
    bs.Bgo(1, ena);
    break;
  case CNT_BGO2:
    bs.Bgo(2, ena);
    break;
  case CNT_BGO3:
    bs.Bgo(3, ena);
    break;
  default:
    break;
  }
  
  if ((m_status = m_alti->CNT_EnableEdgeDetection_Write(bs)) != 0) {
    CERR("ALTI: writing counter edge detection register", "");
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTEdgeDetectionWrite(const bool ena) {

  ALTI_CNT_ENABLEEDGEDETECTION_BITSTRING bs;

  if ((m_status = m_alti->CNT_EnableEdgeDetection_Read(bs)) != 0) {
    CERR("ALTI: reading counter edge detection register", "");
    return (m_status);
  }
   
  bs.L1a(ena);
  bs.TestTrig(0, ena);
  bs.TestTrig(1, ena);
  bs.TestTrig(2, ena);
  bs.Bgo(0, ena);
  bs.Bgo(1, ena);
  bs.Bgo(2, ena);
  bs.Bgo(3, ena);
  
  if ((m_status = m_alti->CNT_EnableEdgeDetection_Write(bs)) != 0) {
    CERR("ALTI: writing counter edge detection register", "");
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTOrbitReset() {

  ALTI_CNT_ORBITCOUNTERCONTROL_BITSTRING bs;

  if ((m_status = m_alti->CNT_OrbitCounterControl_Read(bs)) != 0) {
    CERR("ALTI: reading counter control register", "");
    return (m_status);
  }
  
  bs.ResetOrbitCounter(true);
  bs.ResetShortCounter(true);
  bs.ResetLongCounter(true);
  bs.ClearMinimumLength(true);
  bs.ClearMaximumLength(true);

  if ((m_status = m_alti->CNT_OrbitCounterControl_Write(bs)) != 0) {
    CERR("ALTI: writing counter control register", "");
    return (m_status);
  }

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTOrbitRead(unsigned int &cnt, unsigned int &cnt_short, unsigned int &cnt_long) {

  // orbit counter
  if ((m_status = m_alti->CNT_OrbitCounter_Read(cnt)) != 0) {
    CERR("ALTI: reading orbit counter", "");
    return (m_status);
  }
  
  // orbit length counter
  ALTI_CNT_SHORTLONGCOUNTER_BITSTRING bs;  
  if ((m_status = m_alti->CNT_ShortLongCounter_Read(bs)) != 0) {
    CERR("ALTI: reading orbit length counter", "");
    return (m_status);
  }
  cnt_short = bs.ShortOrbits();
  cnt_long = bs.LongOrbits();

  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTOrbitLengthRead(unsigned int &length_short, unsigned int &length_long) {

  ALTI_CNT_ORBITLENGTH_BITSTRING bs;
  if ((m_status = m_alti->CNT_OrbitLength_Read(bs)) != 0) {
    CERR("ALTI: reading orbit length", "");
    return (m_status);
  }
  length_short = bs.Minimum()+1;
  length_long = bs.Maximum()+1;
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTTtypLutRead(std::vector<unsigned int> &lut) {


    // get data segment
    DataSegment dseg("AltiModule[CNT_TTYP_LUT]", ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // read TTYP counter LUT using block transfer
    if ((m_status = m_alti->CNT_TriggerTypeLut_Read(dseg.segment())) != SUCCESS) {
        CERR("ALTI: reading CNT TTYP LUT using block transfer", "");
        return (m_status);
    }

    unsigned int wrd;

    // reset TTYP counter LUT
    lut.clear();
    lut.resize(ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER);

    // copy TTYP counter LUT from CMEM segment to vector
    for (wrd = 0; wrd < ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER; wrd++) {
        lut[wrd] = dseg[wrd];
	}
 
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTTtypLutWrite(const std::vector<unsigned int> lut) {
  
    unsigned int wrd;

    // get data segment
    DataSegment dseg("AltiModule[CNT_TTYP_LUT]", CNT_TTYP_LUT_WORD_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // copy TTYP counter LUT from vector to CMEM segment
    for (wrd = 0; wrd < ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER; wrd++) {
        dseg[wrd] = lut[wrd];
    }

    // write TTYP counter LUT using block transfer
    if ((m_status = m_alti->CNT_TriggerTypeLut_Write(dseg.segment())) != SUCCESS) {
        CERR("ALTI: writing CNT TTYP LUT using block transfer", "");
        return (m_status);
	}
  
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTTtypLutRead(const std::string fn) {
 
    // get data segment
  DataSegment dseg("AltiModule[CNT_TTYP_LUT]", ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // read TTYP counter LUT using block transfer
    if ((m_status = m_alti->CNT_TriggerTypeLut_Read(dseg.segment())) != SUCCESS) {
        CERR("ALTI: reading CNT TTYP LUT using block transfer", "");
        return (m_status);
    }

    // copy data from CMEM segment into file
    if ((m_status = AltiCommon::writeFile(fn, ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER, dseg.data())) != AltiCommon::SUCCESS) {
        CERR("ALTI: writing data from CNT TTYP LUT to file \"%s\"", fn.c_str());
        return (m_status);
    }
  
    return (m_status);
  
}

//------------------------------------------------------------------------------

int AltiModule::CNTTtypLutWrite(const std::string fn) {
  
    // get data segment
    DataSegment dseg("AltiModule[CNT_TTYP_LUT]", ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // copy data from file into CMEM segment
    if ((m_status = AltiCommon::readFile(fn, ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER, dseg.data())) != AltiCommon::SUCCESS) {
        CERR("ALTI: reading data from CNT TTYP LUT from file \"%s\"", fn.c_str());
        return (m_status);
    }

    // write BCID mask using block transfer
    if ((m_status = m_alti->CNT_TriggerTypeLut_Write(dseg.segment())) != SUCCESS) {
        CERR("ALTI: writing CNT TTYP LUT using block transfer", "");
        return (m_status);
    }
  
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTTtypLutPrint() {
   
   std::vector<unsigned int> lut;
    if ((m_status = CNTTtypLutRead(lut)) != SUCCESS) {
        CERR("ALTI: reading CNT TTYP LUT", "");
    }

    unsigned int i, mask, num_enabled, ttyp;
    for (i = 0; i < CNT_TTYP_NUMBER; i++) {
        mask = (0x00000001 << i);
        num_enabled = 0;
        std::printf("TTYP counter bucket [%d]: ", i);
        for (ttyp = 0x00; ttyp <= 0xff; ttyp++) {
            if (mask & lut[ttyp]) {
                std::printf("%s0x%02x", (num_enabled == 0) ? "" : ", ", ttyp);
                num_enabled++;
            }
        }
        std::printf("\n");
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTTurnCountRead(unsigned int &trn) {

    // read TURN counter
    if ((m_status = m_alti->CNT_TurnCounter_Read(trn)) != SUCCESS) {
        CERR("ALTI: reading CNT TURN counter", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CNTCounterRead(AltiCounter *cnt) {

    if (cnt->getCmemSegment()) {
        if ((m_status = m_alti->CNT_CounterValue_Read(cnt->getCmemSegment(), cnt->dsize())) != SUCCESS) {
            CERR("ALTI: reading CNT using block transfer", "");
            return (m_status);
        }
    }

    // read TURN counter
    unsigned int turn;
    if ((m_status = CNTTurnCountRead(turn)) != SUCCESS) {
        CERR("ALTI: reading CNT TURN counter", "");
        return (m_status);
    }
    cnt->turn(turn);

    return (m_status);
}
  
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::FMWControlEnableBoot(bool checkIDOnly, bool verifyOnly) {
    ALTI_FMW_QUICKBOOTCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->FMW_QuickBootControl_Read(bs)) != SUCCESS) {
        CERR("ALTI: reading SPI FLASH QuickBoot control register", "");
        return (m_status);
    }
    // MODIFY
    bs.CheckIdOnly(checkIDOnly);
    bs.VerifyOnly(verifyOnly);
    bs.EnableBoot(true);
    // WRITE
    if ((m_status = m_alti->FMW_QuickBootControl_Write(bs)) != SUCCESS) {
        CERR("ALTI: writing SPI FLASH QuickBoot control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::FMWControlReset() {
    ALTI_FMW_QUICKBOOTCONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->FMW_QuickBootControl_Read(bs)) != SUCCESS) {
        CERR("ALTI: reading SPI FLASH QuickBoot control register", "");
        return (m_status);
    }
    // MODIFY
    bs.FifoReset(true);
    bs.EnableBoot(false);
    // WRITE
    if ((m_status = m_alti->FMW_QuickBootControl_Write(bs)) != SUCCESS) {
        CERR("ALTI: writing SPI FLASH QuickBoot control register", "");
        return (m_status);
    }
    // MODIFY
    bs.FifoReset(false);
    // WRITE
    if ((m_status = m_alti->FMW_QuickBootControl_Write(bs)) != SUCCESS) {
        CERR("ALTI: writing SPI FLASH QuickBoot control register", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::FMWFIFOStatusRead(bool &full, bool &almostFull, bool &empty, bool &almostEmpty, u_int &level, unsigned int &anyError, unsigned int &debug) {
    ALTI_FMW_FIFOSTATUS_BITSTRING bs;
    // READ
    if ((m_status = m_alti->FMW_FifoStatus_Read(bs)) != SUCCESS) {
        CERR("ALTI: reading SPI FIFO status register", "");
        return (m_status);
    }
    full = bs.Full();
    almostFull = bs.AlmostFull();
    empty = bs.Empty();
    almostEmpty = bs.AlmostEmpty();
    level = bs.Level();

    ALTI_FMW_QUICKBOOTSTATUS_BITSTRING bs2;
    // READ
    if ((m_status = m_alti->FMW_QuickBootStatus_Read(bs2)) != SUCCESS) {
        CERR("ALTI: reading SPI FIFO status register", "");
        return (m_status);
    }
    anyError = bs2.IdCodeError() * 1000000 + bs2.DoneOrError() * 100000 + bs2.Error() * 10000 + bs2.EraseError() * 1000 + bs2.ProgramError() * 100 + bs2.TimeOutError() * 10 + bs2.CRCError();
    debug = bs2.EraseDone() * 10000000 + bs2.CheckIdDone() * 1000000 + bs2.ProgramSwitchWordDone() * 10000 + bs2.ProgramDone() * 1000 + bs2.EraseSwitchWordDone() * 100 + bs2.InitializationDone() * 10 + bs2.Started();
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::FMWQuickBootStatusRead(bool &Error, bool &IdCodeError, bool &EraseError, bool &ProgramError, bool &TimeOutError, bool &CRCError, bool &Started, bool &InitializationDone, bool &CheckIdDone, bool &EraseSwitchWordDone, bool &EraseDone, bool &ProgramDone, bool &VerifyDone, bool &ProgramSwitchWordDone) {

    ALTI_FMW_QUICKBOOTSTATUS_BITSTRING bs;
    // READ
    if ((m_status = m_alti->FMW_QuickBootStatus_Read(bs)) != SUCCESS) {
        CERR("ALTI: reading SPI FIFO status register", "");
        return (m_status);
    }

    Error = bs.Error();
    IdCodeError = bs.IdCodeError();
    EraseError = bs.EraseError();
    ProgramError = bs.ProgramError();
    TimeOutError = bs.TimeOutError();
    CRCError = bs.CRCError();
    Started = bs.Started();
    InitializationDone = bs.InitializationDone();
    CheckIdDone = bs.CheckIdDone();
    EraseSwitchWordDone = bs.EraseSwitchWordDone();
    EraseDone = bs.EraseDone();
    ProgramDone = bs.ProgramDone();
    VerifyDone = bs.VerifyDone();
    ProgramSwitchWordDone = bs.ProgramSwitchWordDone();

    return (m_status);
}
//------------------------------------------------------------------------------

int AltiModule::FMWWaitUntilDone(bool &error) {
    ALTI_FMW_QUICKBOOTSTATUS_BITSTRING bs;

    // POLL
    while (true) {
        // READ
        if ((m_status = m_alti->FMW_QuickBootStatus_Read(bs)) != SUCCESS) {
            CERR("ALTI: reading SPI FLASH QuickBoot status register", "");
            return (m_status);
        }
        if (bs.DoneOrError()) break;
        usleep(1000);
    }
    error = bs.Error();

    return (m_status);
}


//------------------------------------------------------------------------------

int AltiModule::FMWBootSafetyEnable(bool ena) {
  
  u_int value = (ena ? 0x88888888 : 0x0);
  if ((m_status = m_alti->FMW_BootSafetyEnable_Write(value)) != SUCCESS) {
    CERR("ALTI: writing SPI FLASH QuickBoot saftey register", "");
    return (m_status);
  }
  
  return (m_status); 
}

//------------------------------------------------------------------------------

int AltiModule::FMWReceivedDataRead(unsigned int &data) {
    ALTI_FMW_QUICKBOOTSTATUS_BITSTRING bs;

    // READ
    if ((m_status = m_alti->FMW_QuickBootStatus_Read(bs)) != SUCCESS) {
        CERR("ALTI: reading SPI FLASH QuickBoot status register", "");
        return (m_status);
    }
    data = bs.ReceivedData();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::FMWIcapCommandRead(u_int &command) {

   // READ
   if ((m_status = m_alti->FMW_ICAP_Command_Read(command)) != SUCCESS) {
     CERR("ALTI: reading ICAP command", "");
     return (m_status);
   }

   return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::FMWIcapCommandWrite(const u_int command) {

   // WRITE
   if ((m_status = m_alti->FMW_ICAP_Command_Write(command)) != SUCCESS) {
     CERR("ALTI: writing ICAP command", "");
     return (m_status);
   }

   return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::FMWIcapWarmBootStartAddressRead(u_int &address) {

   // READ
   if ((m_status = m_alti->FMW_ICAP_WarmBootStartAddress_Read(address)) != SUCCESS) {
     CERR("ALTI: reading ICAP warm boot start address", "");
     return (m_status);
   }

    return (m_status);
}
 
//------------------------------------------------------------------------------

int AltiModule::FMWIcapWarmBootStartAddressWrite(const u_int address) {

   // WRITE
   if ((m_status = m_alti->FMW_ICAP_WarmBootStartAddress_Write(address)) != SUCCESS) {
     CERR("ALTI: writing ICAP warm boot start address", "");
     return (m_status);
   }

   return (m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::MONPowerAndTemperatureAlarm(bool &any, bool &board_temp, bool &fpga_temp, bool &power_supply, bool &vcc_bram, bool &vcc_int, bool &vcc_aux) {

  ALTI_CTL_POWERANDTEMPERATURESTATUS_BITSTRING bs;
  
  // READ
  if ((m_status = m_alti->CTL_PowerAndTemperatureStatus_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading Power and Temperature status register", "");
    return (m_status);
  }
  any          = bs.AnyAlarm();
  board_temp   = bs.BoardTemperatureAlarm(); 
  fpga_temp    = bs.FpgaDieTemperatureAlarm(); 
  power_supply = bs.AnyPowerSupplyAlarm(); 
  vcc_bram     = bs.VccBram1V0PowerSupplyAlarm(); 
  vcc_int      = bs.VccInt1V0PowerSupplyAlarm(); 
  vcc_aux      = bs.VccAux1V8PowerSupplyAlarm(); 
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MONVoltageReadLTC2991(std::vector<float> &voltages, std::vector<float> &nominal) {
  // trigger the measurement 
  // wait for the measurement to finish
  I2CNetwork::I2CNode node;
  unsigned int status;
  m_status = m_net->getI2CNode("BUS[0]/LTC2991/STATUS_HI", node);
  while (1) {
    if ((m_status = I2CDataRead(node, status)) != 0 ) {
      CERR("ALTI: reading voltage monitor status", "");
      return (m_status);
    }
    if (!(status & 0x00000004)) { // BUSY = 0, conversion done
      break;
    }
  }
  
  // read measurement results;
  for (u_int i = 0; i < 9; i++) {
    switch (i) {
    case 0:
      m_status |= m_net->getI2CNode("BUS[0]/LTC2991/V1", node);
      m_status |= I2CDataRead(node, voltages[i]);
      nominal[i] = 1.0;
      break;
    case 1:
      m_status |= m_net->getI2CNode("BUS[0]/LTC2991/V2", node);
      m_status |= I2CDataRead(node, voltages[i]);
      nominal[i] = 1.8;
      break;
    case 2:
      m_status |= m_net->getI2CNode("BUS[0]/LTC2991/V3", node);
      m_status |= I2CDataRead(node, voltages[i]);
      nominal[i] = 2.5;
      break;
    case 3:
      m_status |= m_net->getI2CNode("BUS[0]/LTC2991/V4", node);
      m_status |= I2CDataRead(node, voltages[i]);
      nominal[i] = 3.3;
      voltages[i] *= 2;
      break;
    case 4:
      m_status |= m_net->getI2CNode("BUS[0]/LTC2991/V5", node);
      m_status |= I2CDataRead(node, voltages[i]);
      nominal[i] = 5.0;
      voltages[i] *= 2;
      break;
    case 5:
      m_status |= m_net->getI2CNode("BUS[0]/LTC2991/V6", node);
      m_status |= I2CDataRead(node, voltages[i]);
      nominal[i] = 12.0;
      voltages[i] *= 4;
      break;
    case 6:
      m_status |= m_net->getI2CNode("BUS[0]/LTC2991/V7", node);
      m_status |= I2CDataRead(node, voltages[i]);
      nominal[i] = -12.0;
      voltages[i] *= -4;
      break;
    case 7:
      m_status |= m_net->getI2CNode("BUS[0]/LTC2991/V8", node);
      m_status |= I2CDataRead(node, voltages[i]);
      nominal[i] = -5.0;
      voltages[i] *= -2;
      break;
    case 8:
      m_status |= m_net->getI2CNode("BUS[0]/LTC2991/VCC", node);
      m_status |= I2CDataRead(node, voltages[i]);
      nominal[i] = -5.0;
      voltages[i] *= -2;
      break;
    }
  }

      return (m_status);
    }

//------------------------------------------------------------------------------

int AltiModule::MONInternalTempReadLTC2991(float &temperature) {
    // trigger the measurement
  I2CNetwork::I2CNode node;
  unsigned int status;
  m_status = m_net->getI2CNode("BUS[0]/LTC2991/STATUS_HI", node);
  if ((m_status = m_net->write(node, (unsigned int) 0x08)) != 0) {
    CERR("ALTI: triggering LTC2991 internal temperature measurement", "");
    return (m_status);
  }

    // wait for the measurement to finish
    while (1) {
      if ((m_status = I2CDataRead(node, status)) != 0 ) {
	CERR("ALTI: reading LTC2991 internal temperature status", "");
	return (m_status);
      }
      //printf("status: 0x%08x\n", status);
      if (status & 0x00000002) { // T_INTERNAL = 1, register contains new data
	break;
      }
    }
    // read measurement results;
    m_status = m_net->getI2CNode("BUS[0]/LTC2991/TINT", node);
    if ((m_status = I2CDataRead(node, temperature)) != 0) {
      CERR("ALTI: reading internal temperature from the LTC2991 monitor", "");
      return (m_status);
    }
    
    return (m_status);
}
//------------------------------------------------------------------------------

int AltiModule::MONLocalTempReadMAX1617(float &temperature) {
   
  // read measurement results;
  I2CNetwork::I2CNode node;
  m_status |= m_net->getI2CNode("BUS[0]/MAX1617/RLTS_LOCAL_TEMP", node);
  if ((m_status = I2CDataRead(node, temperature)) != 0) {
    CERR("ALTI: reading local temperature from the MAX1617 monitor", "");
    return (m_status);
    }
  
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MONLocalTempReadMAX6695(float &temperature) {
  
  // read measurement results;
  I2CNetwork::I2CNode node;
  m_status |= m_net->getI2CNode("BUS[0]/MAX6695/RLTS_LOCAL_TEMP", node);
  if ((m_status = I2CDataRead(node, temperature)) != 0) {
    CERR("ALTI: reading local temperature from the MAX6695 monitor", "");
    return (m_status);
  }
  
    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::MONLocalTempRead(float &temperature) {
    return MONLocalTempReadMAX6695(temperature);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::CLKSetup() {
    ALTI_CLK_CONTROL_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Control_Read(bs)) != 0) {
        CERR("ALTI: reading clock control register", "");
        return (m_status);
    }
    // MODIFY
    bs.JitterCleanerInputSel("FROMOSCILLATOR"); 
    // WRITE
    if ((m_status = m_alti->CLK_Control_Write(bs)) != 0) {
        CERR("ALTI: writing clock control register", "");

        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CLKCheck() {
    ALTI_CLK_STATUS_BITSTRING bs;

    // READ
    if ((m_status = m_alti->CLK_Status_Read(bs)) != 0) {
        CERR("ALTI: reading clock status register", "");
        return (m_status);
    }
    return (bs.JitterCleanerLOL() ? FAILURE : SUCCESS); 
}
       
//------------------------------------------------------------------------------

int AltiModule::I2CInit() {

    // create I2C
    m_i2c = new I2C(I2C::ALTI, m_vmm);

    unsigned int b;
    m_net = new I2CNetwork();
    m_net->addI2CBus("BUS[0]", m_i2c, b);
    I2CNetwork::BUS_TYPE bus_type = I2CNetwork::BUS_ALTI2;
    m_net->initI2CBus(b, bus_type);

    unsigned int i, j;
    std::ostringstream name;

    // fetch switches
    SIGNAL signal;
    for (i = 0; i < SIGNAL_NUMBER_ROUTED; i++) {
        signal = SIGNAL(i);

        name.str("");
        name.clear();
        name << "BUS[0]/" << SIGNAL_NAME[signal] << "_SWITCH/CONFIG";
        m_net->getI2CNode(name.str(), m_SignalSwitchConfig[signal]);

        name.str("");
        name.clear();
        name << "BUS[0]/" << SIGNAL_NAME[signal] << "_SWITCH/CTRL";
        m_net->getI2CNode(name.str(), m_SignalSwitchCtrl[signal]);
    }

    // fetch equalizers
    SIGNAL_SOURCE src;
    EQUALIZER_VOLTAGE vlt;
    for (i = 0; i < SIGNAL_SOURCE_NUMBER - 3; i++) {
        src = SIGNAL_SOURCE(i);
        for (j = 0; j < EQUALIZER_VOLTAGE_NUMBER; j++) {
           vlt = EQUALIZER_VOLTAGE(j);

           name.str("");
           name.clear();
           name << "BUS[0]/" << SIGNAL_SOURCE_NAME[src] << "/" << ((vlt == V_PEAK) ? ("DACA") : ((vlt == V_POLE) ? ("DACB") : ((vlt == V_GAIN) ? ("DACC") : ("DACD"))));
           m_net->getI2CNode(name.str(), m_EqualizerConfig[src][vlt]);
        }
    }

    // fetch SI5344B
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/STATUS_FLG", m_SI5344BStatusFlg);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/OOF_LOS_FLG", m_SI5344BOofLosFlg);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/HOLD_LOL_FLG", m_SI5344BHoldLolFlg);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/PLL_CAL_FLG", m_SI5344BPllCalFlg);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/STATUS_STICKY_FLG", m_SI5344BStatusStickyFlg);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/OOF_LOS_STICKY_FLG", m_SI5344BOofLosStickyFlg);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/HOLD_LOL_STICKY_FLG", m_SI5344BHoldLolStickyFlg);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/PLL_CAL_STICKY_FLG", m_SI5344BPllCalStickyFlg);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/STATUS_INTR_MSK", m_SI5344BStatusIntrMsk);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/OOF_LOS_INTR_MSK", m_SI5344BOofLosIntrMsk);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/HOLD_LOL_INTR_MSK", m_SI5344BHoldLolIntrMsk);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/PLL_CAL_INTR_MSK", m_SI5344BPllCalIntrMsk);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P0/PDN_HARD_RST_SYNC", m_SI5344BPdnHardRstSync);
    m_net->getI2CNode("BUS[0]/CLK_GEN_CLEANER_P2/DESIGN_ID", m_SI5344BDesignId);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CCheck() {
    unsigned int prescale;
    bool enabled;

    // check proper prescaler value for the I2C core
    if ((m_status = m_i2c->PrescaleRead(prescale)) != SUCCESS) {
        CERR("ALTI: reading prescale value", "");
        return(m_status);
    }
    if (prescale != I2C_PRESCALE_VALUE) {
        CERR("ALTI: wrong I2C prescale value", "");
        return(FAILURE);
    }

    // check that I2C core is enabled
    if ((m_status = m_i2c->EnableRead(enabled)) != SUCCESS) {
        CERR("ALTI: reading I2C enable value", "");
        return(m_status);
    }
    if (!enabled) {
        CERR("ALTI: I2C not enabled", "");
        return(FAILURE);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCCheck() {
    unsigned int ttyp_delay;

    // check the TTYP delay with respect to the L1A
    if ((m_status = ENCTriggerTypeDelayRead(ttyp_delay)) != SUCCESS) {
        CERR("ALTI: reading the TTYP word delay", "");
        return(m_status);
    }
    if (ttyp_delay != TTYP_DELAY_FOR_LATCHING) {
        CERR("ALTI: wrong TTYP delay value", "");
        return(FAILURE);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::JCSetup() {

    // set up jitter cleaner: default config
    if ((m_status = CLKJitterCleanerConfigWrite()) != SUCCESS) {
        CERR("ALTI: setting up jitter cleaner, default config", "");
        return (m_status);
    }
    usleep(1000000); // sleep 1s
    // clear status flags
    if ((m_status = CLKJitterCleanerStatusClear()) != SUCCESS) {
        CERR("ALTI: clearing jitter cleaner status flags", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ENCSetup() {
    // set TTYP delay with respect to the L1A to 2BCs
    if ((m_status = ENCTriggerTypeDelayWrite(TTYP_DELAY_FOR_LATCHING)) != SUCCESS) {
        CERR("ALTI: setting the TTYP word delay to %d BCs", TTYP_DELAY_FOR_LATCHING);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

unsigned int AltiModule::pollSlots(std::vector<unsigned int> &slots) {
    unsigned int n = 0, i, boardid;
    slots.clear();

    // open VME
    RCD::VME *vme = RCD::VME::Open();

    for (i = 1; i <= 21; i++) {
        // read BOARD ID
        if (vme->ReadCRCSR(i, BOARD_ID_BASE, &boardid) == 0) {
            if (boardid == BOARD_ID) { // found ALTI
                slots.push_back(i);
                n++;
            }
        }
    }

    // close VME
    RCD::VME::Close();

    return n;
}

//------------------------------------------------------------------------------

int AltiModule::ECRGenerationRead(ECR_GENERATION &ecr) {

    ALTI_ECR_CONTROL_BITSTRING ctl;

    // read ECR type and length
    if ((m_status = m_alti->ECR_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading ECR control", "");
        return (m_status);
    }
    ecr.type = (ctl.Source() == "VME") ? ECR_VME : ECR_INTERNAL;
    ecr.length = ctl.Length() + 1;

    // read ECR frequency
    if ((m_status = m_alti->ECR_Frequency_Read(ecr.frequency)) != SUCCESS) {
        CERR("ALTI: reading ECR frequency", "");
        return (m_status);
    }

    // read ECR BUSY before
    if ((m_status = m_alti->ECR_BusyBefore_Read(ecr.busy_before)) != SUCCESS) {
        CERR("ALTI: reading ECR BUSY BEFORE length", "");
        return (m_status);
    }

    // read ECR BUSY after
    if ((m_status = m_alti->ECR_BusyAfter_Read(ecr.busy_after)) != SUCCESS) {
        CERR("ALTI: reading ECR BUSY AFTER length", "");
        return (m_status);
    }

    // read ECR OBRIT offset
    if ((m_status = m_alti->ECR_OrbitOffset_Read(ecr.orbit_offset)) != SUCCESS) {
        CERR("ALTI: reading ECR ORBIT offset", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ECRGenerationWrite(const ECR_GENERATION &ecr) {

    ALTI_ECR_CONTROL_BITSTRING ctl;

    // write ECR type and length
    ctl.Source(ecr.type == ECR_VME ? "VME" : "INTERNAL");
    ctl.Length(ecr.length - 1);
    if ((m_status = m_alti->ECR_Control_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing ECR control", "");
        return (m_status);
    }

    // write ECR frequency
    if ((m_status = m_alti->ECR_Frequency_Write(ecr.frequency)) != SUCCESS) {
        CERR("ALTI: writing ECR frequency", "");
        return (m_status);
    }

    // write ECR BUSY before
    if ((m_status = m_alti->ECR_BusyBefore_Write(ecr.busy_before)) != SUCCESS) {
        CERR("ALTI: writing ECR BUSY BEFORE length", "");
        return (m_status);
    }

    // write ECR BUSY after
    if ((m_status = m_alti->ECR_BusyAfter_Write(ecr.busy_after)) != SUCCESS) {
        CERR("ALTI: writing ECR BUSY AFETR length", "");
        return (m_status);
    }

    // write ECR OBRIT offset
    if ((m_status = m_alti->ECR_OrbitOffset_Write(ecr.orbit_offset)) != SUCCESS) {
        CERR("ALTI: writing ECR ORBIT offset", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ECRGenerate() {

    unsigned int data(0x00000001);

    // write ECR generate
    if ((m_status = m_alti->ECR_GenerateEcr_Write(data)) != SUCCESS) {
        CERR("ALTI: writing ECR generate", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::ECRReadyWait() {

    ALTI_ECR_BUSYCONTROL_BITSTRING ctl;

    // read busy control
    if ((m_status = m_alti->ECR_BusyControl_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading BSY control", "");
        return (m_status);
    }
    if (ctl.TestEcrBusy()) {
        CERR("ALTI: waiting for ECR BUSY while in \"TEST\"", "");
        return (WRONGPAR);
    }
     
    if (!ctl.EcrBusyStatus()) return (m_status);

    unsigned int itim(0);

    while (true) {

        // read secondary partition busy control
        if ((m_status = m_alti->ECR_BusyControl_Read(ctl)) != SUCCESS) {
            CERR("ALTI: reading BSY control", "");
            return (m_status);
        }

        // check if not empty
        if (!ctl.EcrBusyStatus()) break;

        // check if timeout
        if (++itim >= BSY_CONTROL_TIMEOUT) {
            CERR("ALTI: timeout waiting for ECR BUSY", "");
            return (TIMEOUT);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::T2LControlReset(const bool l1a, const bool ecr, const bool fifo) {

    ALTI_T2L_CONTROL_BITSTRING bs;

    bs.L1aReset(l1a);
    bs.EcrReset(ecr);
    bs.FifoReset(fifo);

    // reset T2L control
    if ((m_status = m_alti->T2L_Control_Write(bs)) != SUCCESS) {
        CERR("ALTI: resetting T2L control", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::T2LEventIdentifierRead(EventIdentifier &evid) {

    unsigned int data;

    // read T2L event identifier
    if ((m_status = m_alti->T2L_L1idValue_Read(data)) != SUCCESS) {
        CERR("ALTI: reading T2L event identifier", "");
        return (m_status);
    }
    evid.data(data);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::T2LFifoStatusRead(bool &emp, bool &ful, unsigned int &lvl) {

    ALTI_T2L_FIFOSTATUS_BITSTRING bs;

    // read T2L FIFO status
    if ((m_status = m_alti->T2L_FifoStatus_Read(bs)) != SUCCESS) {
        CERR("ALTI: reading T2L FIFO status", "");
        return (m_status);
    }
    emp = bs.Empty();
    ful = bs.Full();
    lvl = (ful) ? T2L_FIFO_SIZE : bs.Level();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::T2LFifoWrite() {

    const unsigned int data(0x00000001);

    // write T2L FIFO
    if ((m_status = m_alti->T2L_FifoWrite_Write(data)) != SUCCESS) {
        CERR("ALTI: writing T2L FIFO", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::T2LFifoRead(unsigned int &l1id) {

    // read T2L FIFO
    if ((m_status = m_alti->T2L_L1idFifo_Read(l1id)) != SUCCESS) {
        CERR("ALTI: reading T2L FIFO", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::T2LFifoRead(std::vector<unsigned int> &l1id) {

    unsigned int lvl;
    bool emp, ful;

    // reset L1ID vector
    l1id.clear();

    // read T2L FIFO status
    if ((m_status = T2LFifoStatusRead(emp, ful, lvl)) != SUCCESS) {
        CERR("ALTI: reading T2L FIFO status", "");
        return (m_status);
    }
    if (lvl == 0) return (m_status);

    unsigned int data;

    // read T2L FIFO
    for (unsigned int i = 0; i < lvl; i++) {
        if ((m_status = T2LFifoRead(data)) != SUCCESS) {
            CERR("ALTI: reading T2L FIFO", "");
            return (m_status);
        }
        l1id.push_back(data);
    }

    return (m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::I2CSetup() {

    // set up I2C bus

    // write I2C prescale value
    if ((m_status = m_i2c->PrescaleWrite(I2C_PRESCALE_VALUE)) != I2C::SUCCESS) {
        CERR("ALTI: writing I2C prescale value", "");
        return(m_status);
    }

    // write I2C enable
    if ((m_status = m_i2c->EnableWrite(true)) != I2C::SUCCESS) {
        CERR("ALTI: writing enable", "");
        return(m_status);
    }
    //COUT("ALTI: INFO - I2C = \"SETUP\"", "");

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDump() {

    // dump I2C network
    m_net->dump();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CTransferTypeRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, I2CInterface::TRANSFER_TYPE &ttyp) {

    // read I2C quantity's transfer type
    if ((m_status = m_net->transferType(bus, dev, qty, ttyp)) != I2C::SUCCESS) {
        CERR("ALTI: reading transfer type of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDisplayTypeRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, I2CNetwork::DISPLAY_TYPE &dtyp) {

    // read I2C quantity's display type
    if ((m_status = m_net->displayType(bus, dev, qty, dtyp)) != I2C::SUCCESS) {
        CERR("ALTI: reading display type of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CNameRead(const unsigned int bus, std::string &name) {

    // read I2C bus's name
    if ((m_status = m_net->name(bus, name)) != I2C::SUCCESS) {
        CERR("ALTI: reading name of I2C bus %d", bus);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CNameRead(const unsigned int bus, const unsigned int dev, std::string &name) {

    // read I2C device's name
    if ((m_status = m_net->name(bus, dev, name)) != I2C::SUCCESS) {
        CERR("ALTI: reading name of I2C bus/device %d/%d", bus, dev);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CNameRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, std::string &name) {

    // read I2C quantity's name
    if ((m_status = m_net->name(bus, dev, qty, name)) != I2C::SUCCESS) {
        CERR("ALTI: reading name of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, std::vector<unsigned int> &data) {

    // read I2C quantity's data as vector
    if ((m_status = m_net->read(bus, dev, qty, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, unsigned int &data) {

    // read I2C quantity's data as word
    if ((m_status = m_net->read(bus, dev, qty, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, float &data) {

    // read I2C quantity's data as float
    if ((m_status = m_net->read(bus, dev, qty, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, std::string &data) {

    // read I2C quantity's data as string
    if ((m_status = m_net->read(bus, dev, qty, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, const unsigned int off, const unsigned int len, std::vector<unsigned int> &data) {

    // read I2C quantity's data as vector
    if ((m_status = m_net->read(bus, dev, qty, off, len, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const I2CNetwork::I2CNode &node, std::vector<unsigned int> &data) {

    // read I2C quantity's data as vector
    if ((m_status = m_net->read(node, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C node \"%s\"", node.name().c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const I2CNetwork::I2CNode &node, unsigned int &data) {

    // read I2C quantity's data as word
    if ((m_status = m_net->read(node, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C node \"%s\"", node.name().c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const I2CNetwork::I2CNode &node, float &data) {

    // read I2C quantity's data as float
    if ((m_status = m_net->read(node, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C node \"%s\"", node.name().c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const I2CNetwork::I2CNode &node, std::string &data) {

    // read I2C quantity's data as string
    if ((m_status = m_net->read(node, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C node \"%s\"", node.name().c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataRead(const I2CNetwork::I2CNode &node, const unsigned int off, const unsigned int len, std::vector<unsigned int> &data) {

    // read I2C quantity's data as vector
    if ((m_status = m_net->read(node, off, len, data)) != I2C::SUCCESS) {
        CERR("ALTI: reading data of I2C node \"%s\"", node.name().c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataWrite(const unsigned int bus, const unsigned int dev, const unsigned int qty, const unsigned int data) {

    // write I2C quantity's data as word
    if((m_status = m_net->write(bus, dev, qty, data)) != I2C::SUCCESS) {
        CERR("ALTI: writing data of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataWrite(const unsigned int bus, const unsigned int dev, const unsigned int qty, const float data) {

    // write I2C quantity's data as float
    if ((m_status = m_net->write(bus, dev, qty, data)) != I2C::SUCCESS) {
        CERR("ALTI: writing data of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::I2CDataWrite(const unsigned int bus, const unsigned int dev, const unsigned int qty, const std::string &data) {

    // write I2C quantity's data as string
    if ((m_status = m_net->write(bus, dev, qty, data)) != I2C::SUCCESS) {
        CERR("ALTI: writing data of I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
        return (m_status);
    }

    return (m_status);
}
//------------------------------------------------------------------------------

int AltiModule::BSYStatusRead(AltiBusyCounter& cnt) {

    unsigned int data;

    // read BSY status
    if((m_status = m_alti->BSY_Status_Read(data)) != SUCCESS) {
        CERR("ALTI: reading BSY status","");
        return(m_status);
    }

    // set counter mask
    cnt.setStatus(data);

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMonitoringControlRead(bool& wmod, bool& rmod) {

    ALTI_BSY_MONITORINGCONTROL_BITSTRING ctl;

    // read BSY control
    if((m_status = m_alti->BSY_MonitoringControl_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading BSY control","");
        return(m_status);
    }
    wmod = ctl.WriteMode() == "AUTOMATIC";
    rmod = ctl.ReadMode() == "LAST";

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYMonitoringControlWrite(const bool wmod, const bool rmod) {

    ALTI_BSY_MONITORINGCONTROL_BITSTRING ctl;

    // write BSY control
    ctl.WriteMode(wmod?"AUTOMATIC":"MANUAL");
    ctl.ReadMode(rmod?"LAST":"FIRST");
    if((m_status = m_alti->BSY_MonitoringControl_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing BSY control","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoReset() {

    ALTI_BSY_MONITORINGCONTROL_BITSTRING ctl;

    // read BSY control
    if((m_status = m_alti->BSY_MonitoringControl_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading BSY control","");
        return(m_status);
    }

    // modify BSY control
    ctl.ClearFifo(true);

    // write BSY control
    if((m_status = m_alti->BSY_MonitoringControl_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing BSY control","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoWrite() {

    ALTI_BSY_MONITORINGCONTROL_BITSTRING ctl;

    // read BSY control
    if((m_status = m_alti->BSY_MonitoringControl_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading BSY control","");
        return(m_status);
    }

    // modify BSY control
    ctl.WriteStrobe(true);

    // write BSY control
    if((m_status = m_alti->BSY_MonitoringControl_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing BSY control","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYSelectRead(AltiBusyCounter& cnt) {

    unsigned int data, mask, b;

    // read BSY write counter
    if((m_status = m_alti->BSY_WriteCounter_Read(data)) != SUCCESS) {
        CERR("ALTI: reading BSY write counter","");
        return(m_status);
    }

    // set counter mask
    mask = 0x00000001;
    for(b=0; b<AltiBusyCounter::BUSY_NUMBER; b++, mask=mask<<1) {
        cnt.select(static_cast<AltiBusyCounter::BUSY>(b),(data&mask)==mask);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYSelectWrite(const AltiBusyCounter& cnt) {

    unsigned int data, mask, b;

    // set counter mask
    data = 0x00000000;
    mask = 0x00000001;
    for(b=0; b<AltiBusyCounter::BUSY_NUMBER; b++, mask=mask<<1) {
        if(cnt.select(static_cast<AltiBusyCounter::BUSY>(b))) data|=mask;
    }

    // write BSY write counter
    if((m_status = m_alti->BSY_WriteCounter_Write(data)) != SUCCESS) {
        CERR("ALTI: writing BSY write counter","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYCounterReset() {

    unsigned int data;

    // set counter mask
    data = AltiBusyCounter::BSY_SEL_MASK;

    // write BSY counter clear
    if((m_status = m_alti->BSY_ClearCounter_Write(data)) != SUCCESS) {
        CERR("ALTI: writing BSY clear counter","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYCounterReset(const AltiBusyCounter& cnt) {

    unsigned int data, mask, b;

    // set counter mask
    data = 0x00000000;
    mask = 0x00000001;
    for(b=0; b<AltiBusyCounter::BUSY_NUMBER; b++, mask=mask<<1) {
        if(cnt.select(static_cast<AltiBusyCounter::BUSY>(b))) data|=mask;
    }

    // write BSY counter clear
    if((m_status = m_alti->BSY_ClearCounter_Write(data)) != SUCCESS) {
        CERR("ALTI: writing BSY clear counter","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYIntervalRead(unsigned int& itvl) {

    // read BSY interval
    if((m_status = m_alti->BSY_WriteInterval_Read(itvl)) != SUCCESS) {
        CERR("ALTI: reading BSY interval","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYIntervalWrite(const unsigned int itvl) {

    // write BSY interval
    if((m_status = m_alti->BSY_WriteInterval_Write(itvl)) != SUCCESS) {
        CERR("ALTI: writing BSY interval","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoWatermarkRead(unsigned int& wmrk) {

    // read BSY FIFO watermark
    if((m_status = m_alti->BSY_FifoWatermark_Read(wmrk)) != SUCCESS) {
        CERR("ALTI: reading BSY FIFO watermark","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoWatermarkWrite(const unsigned int wmrk) {

    // write BSY FIFO watermark
    if((m_status = m_alti->BSY_FifoWatermark_Write(wmrk)) != SUCCESS) {
        CERR("ALTI: writing BSY FIFO watermark","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoStatusRead(unsigned int& lvl, bool& ful) {

    ALTI_BSY_FIFOSTATUS_BITSTRING sta;

    // read BSY FIFO status
    if((m_status = m_alti->BSY_FifoStatus_Read(sta)) != SUCCESS) {
        CERR("ALTI: reading BSY FIFO status","");
        return(m_status);
    }
    lvl = (sta.Full()) ? BSY_FIFO_SIZE : sta.Level();

    // read BSY FIFO watermark
    unsigned int wmrk;
    if((m_status = m_alti->BSY_FifoWatermark_Read(wmrk)) != SUCCESS) {
        CERR("ALTI: reading BSY FIFO watermark","");
        return(m_status);
    }
    ful = (lvl >= wmrk);

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoLevel(unsigned int& lvl) {

    ALTI_BSY_FIFOSTATUS_BITSTRING sta;

    // read BSY FIFO status
    if((m_status = m_alti->BSY_FifoStatus_Read(sta)) != SUCCESS) {
        CERR("ALTI: reading BSY FIFO status","");
        return(m_status);
    }
    lvl = (sta.Full()) ? BSY_FIFO_SIZE : sta.Level();

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoWait(unsigned int& lvl, const unsigned int siz) {

    unsigned int itim = 0;

    while(true) {

        // read BSY FIFO status
        if((m_status = BSYFifoLevel(lvl)) != SUCCESS) {
            CERR("ALTI: reading BSY FIFO level","");
            return(m_status);
        }

        // check if not empty
        if(lvl >= siz) break;

        // check if timeout
        if(++itim >= BSY_CNT_TIMEOUT) {
            CERR("ALTI: timeout waiting for BSY FIFO","");
            return(TIMEOUT);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoReady(bool& rdy) {

    ALTI_BSY_FIFOSTATUS_BITSTRING sta;

    // read BSY FIFO status
    if((m_status = m_alti->BSY_FifoStatus_Read(sta)) != SUCCESS) {
        CERR("ALTI: reading BSY FIFO status","");
        return(m_status);
    }
    rdy = (sta.Level() > 0) || sta.Full();

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoFull(bool& ful) {

    ALTI_BSY_FIFOSTATUS_BITSTRING sta;

    // read BSY FIFO status
    if((m_status = m_alti->BSY_FifoStatus_Read(sta)) != SUCCESS) {
        CERR("ALTI: reading BSY FIFO status","");
        return(m_status);
    }
    ful = sta.Full();

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoRead(unsigned int& data) {

    // read BSY FIFO
    if((m_status = m_alti->BSY_BusyFifo_Read(data)) != SUCCESS) {
        CERR("ALTI: reading BSY FIFO","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYCounterConfigRead(AltiBusyCounter& cnt) {

    ALTI_BSY_MONITORINGCONTROL_BITSTRING ctl;
    unsigned int itv;

    // read BSY control
    if((m_status = m_alti->BSY_MonitoringControl_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading BSY control","");
        return(m_status);
    }
    cnt.m_wmod = (ctl.WriteMode()=="AUTOMATIC");
    cnt.m_rmod = (ctl.ReadMode()=="FIRST");
    CDBG("ALTI: W/mode = %s, R/mode = %s",(cnt.autoMode()?"AUTOMATIC":"MANUAL"),(cnt.firstMode()?"FIRST":"LAST"));

    // read BSY select
    if((m_status = BSYSelectRead(cnt)) != SUCCESS) {
        CERR("ALTI: reading BSY select","");
        return(m_status);
    }
    CDBG("ALTI: select = %s",cnt.selectPrint().c_str());

    // read interval time if necessary
    if(!cnt.select(AltiBusyCounter::INTERVAL)) {
        if((m_status = m_alti->BSY_WriteInterval_Read(itv)) != SUCCESS) {
            CERR("ALTI: reading BSY interval","");
            return(m_status);
        }
        cnt.m_interval = itv;
        CDBG("ALTI: interval = %d",cnt.interval());
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoRead(AltiBusyCounter& cnt, const int max, const int alm) {

    unsigned int msk(cnt.selectSize()), lvl, num, wtm;

    // check if anything selected
    if(msk == 0) {
        CERR("ALTI: nothing selected in BSY counter","");
        return(WRONGPAR);
    }

    // reset counter data
    cnt.resetDATA();

    // read BSY FIFO watermark if necessary
    if(alm < 0) {
        if((m_status = BSYFifoWatermarkRead(wtm)) != SUCCESS) {
            CERR("ALTI: reading BSY FIFO watermark","");
            return(m_status);
        }
    }
    else {
        wtm = alm;
    }

    // wait for BSY FIFO and read BSY FIFO level
    if((m_status = BSYFifoWait(lvl,msk)) != SUCCESS) {
        CERR("ALTI: waiting for BSY FIFO to have at least %d words",msk);
        return(m_status);
    }
    cnt.m_overflow = (lvl >= wtm);
    num = (max<0) ? (lvl/msk)*msk : std::min(static_cast<int>(max),static_cast<int>(lvl/msk))*msk;
    CDBG("ALTI: max = %d, msk = %d, lvl = %d, num = %d, slc = %d",max,msk,lvl,num,lvl/msk);

    // read from BSY FIFO using block transfer
    if(num>1) {
        if((m_status = m_alti->BSY_BusyFifo_Read(cnt.segment(),num&(~0x00000001))) != SUCCESS) {
            CERR("ALTI: reading BSY FIFO using block transfer","");
            return(m_status);
        }
        CDBG("ALTI: read block, size = 0x%08x, addr = 0x%08x",num&(~0x00000001),cnt.segment()->VirtualAddress());
    }

    // read the last word when transfer size is not a multiple of two
    if(num&0x00000001) {
        if((m_status = BSYFifoRead(cnt.m_data[num-1])) != SUCCESS) {
            CERR("ALTI: reading last word from BSY FIFO","");
            return(m_status);
        }
        CDBG("ALTI: read last word separately, addr = 0x%08x, data = 0x%08x",&cnt.m_data[num-1],cnt.m_data[num-1]);
    }
    cnt.m_values = num/msk;

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::BSYFifoRead(AltiBusyCounterRate& avg, AltiBusyCounter& cnt, const int max, const int alm) {

    bool rdy;
    unsigned int lvl, wtm;
    ALTI_BSY_FIFOSTATUS_BITSTRING sta;

    CDBG("ALTI: max = %d, alm = %d",max,alm);

    // reset counter
    avg.reset();

    // read BSY FIFO watermark if necessary
    if(alm < 0) {
        if((m_status = BSYFifoWatermarkRead(wtm)) != SUCCESS) {
            CERR("ALTI: reading BSY FIFO watermark","");
            return(m_status);
        }
    }
    else {
        wtm = alm;
    }
    // main loop
    while(true) {

        // check if something to read
        if((m_status = m_alti->BSY_FifoStatus_Read(sta)) != SUCCESS) {
            CERR("ALTI: reading BSY FIFO status","");
            return(m_status);
        }
        rdy = (sta.Level() > 0) || sta.Full();
        CDBG("ALTI: BSY FIFO = %s",rdy?"READY":"NOT_READY");
        if(!rdy) break;

        // check if overflow
        lvl = sta.Full() ? BSY_FIFO_SIZE : sta.Level();
        if(lvl >= wtm) avg.m_overflow = true;
        CDBG("ALTI: BSY FIFO overflow = %s",avg.m_overflow?"OVERFLOW":"NO_OVERFLOW");

        // read counter
        if((m_status = BSYFifoRead(cnt,max-avg.m_values,wtm)) != SUCCESS) {
            CERR("ALTI: reading CTPCORE BSY monitoring FIFO","");
            return(m_status);
        }
#ifdef DEBUG
        cnt.dump();
#endif  // DEBUG

        // average counter
        avg += cnt;
#ifdef DEBUG
        avg.dump();
#endif  // DEBUG

        // check maximum reads
        if((avg.m_values >= static_cast<unsigned int>(max)) || avg.m_overflow) break;
    }

    // reset FIFO if necessary
    if(avg.m_overflow) {
        if((m_status = BSYFifoReset() != SUCCESS)) {
            CERR("ALTI: resetting BSY monitoring FIFO","");
            return(m_status);
        }
        CDBG("ALTI: BSY FIFO reset","");
    }

    return(m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::PBMStatusRead(bool &ena,bool &act,unsigned int &bcid_offset,unsigned int &turn){
    ALTI_PBM_COUNTERSCONTROL_BITSTRING bs;
    if ((m_status = m_alti->PBM_CountersControl_Read(bs)) != SUCCESS) {
        CERR("ALTI: reading PBM Control register","");
        return(m_status);
    }
    ena = (bool) bs.Enable();
    act = (bool) bs.Active();
    if ((m_status = m_alti->PBM_BcidOffset_Read(bcid_offset)) != SUCCESS) {
        CERR("ALTI: reading PBM BCID offset register","");
        return(m_status);
    }
    if ((m_status = m_alti->PBM_TurnCount_Read(turn)) != SUCCESS) {
        CERR("ALTI: reading PBM Turn counter register","");
        return(m_status);
    }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PBMEnableCounters(const bool en){
    ALTI_PBM_COUNTERSCONTROL_BITSTRING bs;
    if ((m_status = m_alti->PBM_CountersControl_Read(bs)) != SUCCESS) {
        CERR("ALTI: reading PBM Control register","");
        return(m_status);
    }
    bs.Enable(en ? 1:0);
    if ((m_status = m_alti->PBM_CountersControl_Write(bs)) != SUCCESS) {
        CERR("ALTI: writing PBM Control register","");
        return(m_status);
    }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PBMClearCounters(){
    ALTI_PBM_COUNTERSCONTROL_BITSTRING bs;
    if ((m_status = m_alti->PBM_CountersControl_Read(bs)) != SUCCESS) {
        CERR("ALTI: reading PBM Control register","");
        return(m_status);
    }
    bs.Clear(1);
    if ((m_status = m_alti->PBM_CountersControl_Write(bs)) != SUCCESS) {
        CERR("ALTI: writing PBM Control register","");
        return(m_status);
    }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PBMWriteOffset(const unsigned int offset){
    
    if ((m_status = m_alti->PBM_BcidOffset_Write(offset)) != SUCCESS) {
        CERR("ALTI: reading PBM BCID offset register","");
        return(m_status);
    }
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::PBMReadTurnCounter(u_int &turn) {

  if ((m_status = m_alti->PBM_TurnCount_Read(turn)) != AltiModule::SUCCESS) {
    CERR("ALTI: reading PBM Turn counter register","");
    return(m_status);
  }

  return m_status;
}

//------------------------------------------------------------------------------

int AltiModule::PBMReadMemory(const PBM_SIGNAL signal, RCD::CMEMSegment *segment){
    ALTI_PBM_ITEMSELECT_BITSTRING bs;
    PBMEnableCounters(false); // Disable the counters in order to read the memory
    
    bs.Item(PBM_SIGNAL_NAME[signal]);
    if ((m_status = m_alti->PBM_ItemSelect_Write(bs))!= SUCCESS){
      CERR("ALTI: writing to item select register","");
      return(m_status);
    }
    if ((m_status = m_alti->PBM_CountersRam_Read(segment,3564,0,0)) != SUCCESS){
      CERR("ALTI: reading PBM memory","");
      return(m_status);
    }
  
  return(m_status);
}
					 
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int AltiModule::CTPInputL1aRead(std::string &input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  input = bs.L1a();

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPInputL1aWrite(const std::string input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  bs.L1a(input);
  if ((m_status = m_alti->CTP_INP_InputSelect_Write(bs))!= SUCCESS){
    CERR("ALTI: writing CTP input select register","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPInputBgo2Read(std::string &input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  input = bs.Bgo2();

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPInputBgo2Write(const std::string input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  bs.Bgo2(input);
  if ((m_status = m_alti->CTP_INP_InputSelect_Write(bs))!= SUCCESS){
    CERR("ALTI: writing CTP input select register","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPInputBgo3Read(std::string &input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  input = bs.Bgo3();

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPInputBgo3Write(const std::string input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  bs.Bgo3(input);
  if ((m_status = m_alti->CTP_INP_InputSelect_Write(bs))!= SUCCESS){
    CERR("ALTI: writing CTP input select register","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPInputTtrRead(const u_int ttr, std::string &input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  input = bs.TestTrigger(ttr);

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPInputTtrWrite(const u_int ttr, const std::string input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  bs.TestTrigger(ttr, input);
  if ((m_status = m_alti->CTP_INP_InputSelect_Write(bs))!= SUCCESS){
    CERR("ALTI: writing CTP input select register","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPInputCalreqRead(const u_int crq, std::string &input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  input = bs.CalRequest(crq);

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPInputCalreqWrite(const u_int crq, const std::string input) {
  ALTI_CTP_INP_INPUTSELECT_BITSTRING bs;

  if ((m_status = m_alti->CTP_INP_InputSelect_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP input select register","");
    return(m_status);
  }
  bs.CalRequest(crq, input);
  if ((m_status = m_alti->CTP_INP_InputSelect_Write(bs))!= SUCCESS){
    CERR("ALTI: writing CTP input select register","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupMaskRead(const u_int item, u_int &mask) {
  // check item range
  CHECKRANGE0(item, ITEM_NUMBER, "items number");
  
  ALTI_CTP_BGM_BUNCHGROUP_BITSTRING bs;
  if ((m_status = m_alti->CTP_BGM_BunchGroup_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP BunchGroup register","");
    return(m_status);
  }

  mask = bs.Mask(item);

  return(m_status);
}
//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupMaskWrite(const u_int item, const u_int mask) {
  // check item range
  CHECKRANGE0(item, ITEM_NUMBER, "items number");

  ALTI_CTP_BGM_BUNCHGROUP_BITSTRING bs;
  if ((m_status = m_alti->CTP_BGM_BunchGroup_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP BunchGroup register","");
    return(m_status);
  }

  bs.Mask(item, mask);
  
  if ((m_status = m_alti->CTP_BGM_BunchGroup_Write(bs))!= SUCCESS){
    CERR("ALTI: writing CTP BunchGroup register","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupMaskRead(const u_int item, bool &bg0, bool &bg1) {
  // check item range
  CHECKRANGE0(item, ITEM_NUMBER, "items number");

  ALTI_CTP_BGM_BUNCHGROUP_BITSTRING bs;
  if ((m_status = m_alti->CTP_BGM_BunchGroup_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP BunchGroup register","");
    return(m_status);
  }
  
  u_int mask = bs.Mask(item);
  bg0 = (mask & 0x1);
  bg1 = (mask & 0x2);

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupMaskWrite(const u_int item, const bool bg0, const bool bg1) {
  // check item range
  CHECKRANGE0(item, ITEM_NUMBER, "items number");
  
  ALTI_CTP_BGM_BUNCHGROUP_BITSTRING bs;
  if ((m_status = m_alti->CTP_BGM_BunchGroup_Read(bs))!= SUCCESS){
    CERR("ALTI: reading CTP BunchGroup register","");
    return(m_status);
  }
  
  u_int mask = bg0;
  mask |= (0x2 & bg1);
  bs.Mask(item, mask);
  
  if ((m_status = m_alti->CTP_BGM_BunchGroup_Write(bs))!= SUCCESS){
    CERR("ALTI: writing CTP BunchGroup register","");
    return(m_status);
  }
  
  return(m_status);
}


//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupMaskRead(std::vector<unsigned int>& msk) {
    
    // reset BGRP mask vector
    msk.clear();
    msk.resize(ITEM_NUMBER);
    
    for(u_int itm = 0; itm < ITEM_NUMBER; itm++) {
      if((m_status = CTPBunchGroupMaskRead(itm, msk[itm])) != SUCCESS) {
	CERR("ALTI: reading BGRP mask for item %d", itm);
	return(m_status);
      }
    }
    
    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupMaskWrite(const std::vector<unsigned int> msk) {

    // check vector size
    CHECKRANGE(msk.size(),ITEM_NUMBER,ITEM_NUMBER,"BGRP mask vector size");

    // write TRG BGRP mask
    for(u_int itm = 0; itm < ITEM_NUMBER; itm++) {
      if((m_status = CTPBunchGroupMaskWrite(itm, msk[itm])) != SUCCESS) {
	CERR("ALTI: writing TRG BGRP mask for item %d",itm);
	return(m_status);
      }
    }
    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupMaskRead(const std::string& fn) {

  std::vector<u_int> dat;
  if((m_status = CTPBunchGroupMaskRead(dat)) != SUCCESS) {
    CERR("ALTI:  reading mini-CTP bunch-group mask","");
    return(m_status);
  }
  
  // copy data into file
  if ((m_status = AltiCommon::writeFile(fn, ITEM_NUMBER, dat)) != AltiCommon::SUCCESS) {
    CERR("ALTI: writing mini-CTP bunch-group mask to file \"%s\"", fn.c_str());
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupMaskWrite(const std::string& fn) {
  
  std::vector<u_int> dat;
  // get data from file
  if ((m_status = AltiCommon::readFile(fn, dat)) != AltiCommon::SUCCESS) {
    CERR("ALTI: reading mini-CTP bunch-group mask data from file \"%s\"", fn.c_str());
  }
  
  if((m_status = CTPBunchGroupMaskWrite(dat)) != SUCCESS) {
    CERR("ALTI:  writinging mini-CTP bunch-group mask","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupDataRead(const u_int bgrp, const u_int bcid, bool &ena) {

  // check BCID number
  CHECKRANGE0(bcid, BCID_NUMBER-1, "BGRP BCID number");
  
  u_int wrd = bcid/WORD_SIZE;
  u_int off = bcid%WORD_SIZE;
  
  // read BGRP data
  u_int data;
  if((m_status = m_alti->CTP_BGM_BcidLut_Read(bgrp, wrd, data)) != SUCCESS) {
    CERR("ALTI:  reading BGRP%02d of word %d", bgrp, wrd);
    return(m_status);
  }
  
  ena = data & (0x00000001 << off);
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupDataWrite(const u_int bgrp, const u_int bcid, const bool ena) {
  
  // check BCID number
  CHECKRANGE0(bcid, BCID_NUMBER-1, "BGRP BCID number");
  
  u_int wrd = bcid/WORD_SIZE;
  u_int off = bcid%WORD_SIZE;
  
  // read BGRP data 
  u_int data;
  if((m_status = m_alti->CTP_BGM_BcidLut_Read(bgrp, wrd, data)) != SUCCESS) {
    CERR("ALTI:  reading BGRP%02d of word %d", bgrp, wrd);
    return(m_status);
  }
 
  // modify BGRP data
  data = ena ? (data | (0x00000001 << off)) : (data & (~(0x00000001 << off)));
  
  // write BGRP data 
  if((m_status = m_alti->CTP_BGM_BcidLut_Write(bgrp, wrd, data)) != SUCCESS) {
    CERR("ALTI:  writing BGRP%02d of word %d", bgrp, wrd);
    return(m_status);
  }
 
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupDataRead(const u_int bgrp, std::vector<bool> &ena) {

  // get data segment
  DataSegment dseg("AltiModule[BGRP]", BGRP_WORD_TOTAL);
  if(dseg() != SUCCESS) {
    CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
    return(FAILURE);
  }
    
  // read BGRP data using block transfer
  if((m_status = m_alti->CTP_BGM_BcidLut_Read(bgrp, dseg.segment())) != SUCCESS) {
    CERR("ALTI:  getting CMEM segment \"%s\"",dseg.name().c_str());
    return(m_status);
  }

  // reset BGRP data vector
  ena.clear();
  ena.resize(BCID_NUMBER);
    
  // copy BGRP data from CMEM segment to vector
  u_int idx = 0;
  for(u_int wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
    u_int off = 0x00000001;
    for(u_int bit = 0; bit < WORD_SIZE; bit++) {
      ena[idx] = (dseg[wrd] & off);
      off <<= 1;
      idx++;
    }
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupDataWrite(const u_int bgrp, const std::vector<bool> dat) {
  
  // check BGRP vector size
  CHECKRANGE(dat.size(), BCID_NUMBER, BCID_NUMBER, "BGRP vector size");
  
  // get data segment
  DataSegment dseg("AltiModule[BGRP]", BGRP_WORD_TOTAL);
  if(dseg() != SUCCESS) {
    CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
    return(FAILURE);
  }

  // copy BGRP data from vector to CMEM segment
  u_int idx = 0;
  for(u_int wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
    u_int off = 0x00000001;
    dseg[wrd] = 0x00000000;
    for(u_int bit = 0; bit < WORD_SIZE; bit++) {
      if(dat[idx]) dseg[wrd] |= off;
      off <<= 1;
      idx++;
    }
  }
  
  // read BGRP data using block transfer
  if((m_status = m_alti->CTP_BGM_BcidLut_Write(bgrp, dseg.segment())) != SUCCESS) {
    CERR("ALTI:  writing CMEM segment \"%s\"",dseg.name().c_str());
    return(m_status);
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupDataRead(std::vector<u_int> &dat) {

  // reset BGRP data vector
  dat.clear();
  dat.resize(BGRP_WORD_TOTAL);
  
  // get data segment
  DataSegment dseg("AltiModule[BGRP]", BGRP_WORD_TOTAL);
  if(dseg() != SUCCESS) {
    CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
    return(FAILURE);
  }
    
  u_int offset = 0;
  for(u_int bgrp = 0; bgrp < BGRP_NUMBER; bgrp++) {
    // read BGRP data using block transfer
    if((m_status = m_alti->CTP_BGM_BcidLut_Read(bgrp, dseg.segment(), BCID_WORD_NUMBER, 0, offset)) != SUCCESS) {
      CERR("ALTI:  getting CMEM segment \"%s\"",dseg.name().c_str());
      return(m_status);
    }
    offset += BCID_WORD_NUMBER;
  }
  
  // copy data from CMEM segment into vector
  for(u_int i=0; i < BGRP_WORD_TOTAL; i++) dat[i] = dseg[i];
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupDataWrite(const std::vector<u_int> dat) {
  
  // check BGRP vector size
  CHECKRANGE(dat.size(), BGRP_WORD_TOTAL, BGRP_WORD_TOTAL, "BGRP vector size");
  
  // get data segment
  DataSegment dseg("AltiModule[BGRP]", BGRP_WORD_TOTAL);
  if(dseg() != SUCCESS) {
    CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
    return(FAILURE);
  }
  
  // copy data from vector into CMEM segment
  for(u_int i=0; i < BGRP_WORD_TOTAL; i++) dseg[i] = dat[i];

  
  u_int offset = 0;;  
  for(u_int bgrp = 0; bgrp < ALTI::CTP_BGM_BCIDLUT_NUMBER; bgrp++) {
    // read BGRP data using block transfer
    if((m_status = m_alti->CTP_BGM_BcidLut_Write(bgrp, dseg.segment(), BCID_WORD_NUMBER, 0, offset)) != SUCCESS) {
      CERR("ALTI:  writing CMEM segment \"%s\"",dseg.name().c_str());
      return(m_status);
    }
    offset += BCID_WORD_NUMBER;
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupDataRead(const std::string& fn) {

  std::vector<u_int> dat;
  if((m_status = CTPBunchGroupDataRead(dat)) != SUCCESS) {
    CERR("ALTI:  reading mini-CTP bunch-group","");
    return(m_status);
  }
  
  // copy data into file
  if ((m_status = AltiCommon::writeFile(fn, BGRP_WORD_TOTAL, dat)) != AltiCommon::SUCCESS) {
    CERR("ALTI: writing mini-CTP bunch-group LUT to file \"%s\"", fn.c_str());
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupDataWrite(const std::string& fn) {
  
  std::vector<u_int> dat;
  // get data from file
  if ((m_status = AltiCommon::readFile(fn, dat)) != AltiCommon::SUCCESS) {
    CERR("ALTI: reading mini-CTP bunch-group LUT data from file \"%s\"", fn.c_str());
  }
  
  if((m_status = CTPBunchGroupDataWrite(dat)) != SUCCESS) {
    CERR("ALTI:  writinging mini-CTP bunch-group","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupBcidOffsetRead(unsigned int& offset) {
  
  if((m_status = m_alti->CTP_BGM_BcidOffset_Read(offset)) != SUCCESS) {
    CERR("ALTI: reading mini-CTP BCID offset", offset);
    return(m_status);
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupBcidOffsetWrite(const unsigned int offset) {
 
  if((m_status = m_alti->CTP_BGM_BcidOffset_Write(offset)) != SUCCESS) {
    CERR("ALTI: writing mini-CTP BCID offset", offset);
    return(m_status);
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupItemEnabledRead(const u_int item, bool& ena) {

  // check item range
  CHECKRANGE0(item, ITEM_NUMBER, "items number");

  u_int data;
  if((m_status = m_alti->CTP_BGM_ItemEnable_Read(data)) != SUCCESS) {
    CERR("ALTI: reading mini-CTP items enabled", "");
    return(m_status);
  }

  ena = (data >> item) & 1;

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupItemEnabledWrite(const unsigned int item, const bool ena) {

  // check item range
  CHECKRANGE0(item, ITEM_NUMBER, "items number");
  
  u_int data;
  if((m_status = m_alti->CTP_BGM_ItemEnable_Read(data)) != SUCCESS) {
    CERR("ALTI: reading mini-CTP items enabled", "");
    return(m_status);
  }
  data = ena ? (data | (0x1 << item)) : (data & ~(0x1 << item));
  if((m_status = m_alti->CTP_BGM_ItemEnable_Write(data)) != SUCCESS) {
    CERR("ALTI: writing mini-CTP items enabled", "");
    return(m_status);
  }
  
  return(m_status);  
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupItemEnabledRead(u_int &wrd) {

  wrd = 0;
  for(u_int item = 0; item < ITEM_NUMBER; item++) {
    bool ena;
    if((m_status = CTPBunchGroupItemEnabledRead(item, ena)) != SUCCESS) {
      CERR("ALTI: reading TAV enable for item $d", item);
      return(m_status);
    }
    wrd |= (0x1 << item);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPBunchGroupItemEnabledWrite(const unsigned int word) {

  for(u_int item = 0; item < ITEM_NUMBER; item++) {
    bool ena = ((word >> item) & 0x1);
    if((m_status = CTPBunchGroupItemEnabledWrite(item, ena)) != SUCCESS) {
      CERR("ALTI: writing TAV enable for item $d", item);
      return(m_status);
    }
  }

  return(m_status);  
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerSeedRead(const unsigned int item, unsigned int &seed) {

    if((m_status = m_alti->CTP_PSC_Seed_Read(item, seed)) != SUCCESS) {
        CERR("ALTI: reading PRSC seed for item %d",item);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerSeedWrite(const unsigned int item, const unsigned int seed) {

    if((m_status = m_alti->CTP_PSC_Seed_Write(item,seed)) != SUCCESS) {
        CERR("ALTI: writing PRSC seed for item %d",item);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerSeedRead(std::vector<unsigned int>& seed) {

  seed.clear();
  seed.resize(ITEM_NUMBER);

  // write PRSC seed
  for(u_int itm = 0; itm < ITEM_NUMBER; itm++) {
    if((m_status = m_alti->CTP_PSC_Seed_Read(itm, seed[itm])) != SUCCESS) {
      CERR("ALTI: reading PRSC seed for item %d",itm);
      return(m_status);
    }
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerSeedWrite(const std::vector<unsigned int> seed) {

    // check vector size
    CHECKRANGE0(seed.size(),ITEM_NUMBER,"PRSC seed vector size");

    unsigned int itm;

    // write PRSC seed
    for(itm=0; itm<ITEM_NUMBER; itm++) {
        if((m_status = m_alti->CTP_PSC_Seed_Write(itm,seed[itm])) != SUCCESS) {
            CERR("ALTI: writing PRSC seed for item %d",itm);
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerSeedWrite(const unsigned int seed) {

    std::vector<unsigned int> psc;
    psc.resize(ITEM_NUMBER,seed);

    if((m_status = CTPPrescalerSeedWrite(psc)) != SUCCESS) {
        CERR("ALTI: writing PRSC seed for all items","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerSeedRead(const std::string fn) {
  
  std::vector<unsigned int> seed;
  
  // read PRSC threshold
  if((m_status = CTPPrescalerSeedRead(seed)) != SUCCESS) {
    CERR("ALTI: reading PRSC seed","");
    return(m_status);
  }
  
  // copy data from vector into file
  if((m_status = AltiCommon::writeFile(fn, ITEM_NUMBER, seed)) != AltiCommon::SUCCESS) {
    CERR("ALTI: writing data from PRSC threshold to file \"%s\"",fn.c_str());
    return(m_status);
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerSeedWrite(const std::string fn) {

    std::vector<unsigned int> seed(ITEM_NUMBER);

    // copy data from file into vector
    if((m_status = AltiCommon::readFile(fn, ITEM_NUMBER, seed)) != AltiCommon::SUCCESS) {
      CERR("ALTI: reading data for PRSC seed from file \"%s\"",fn.c_str());
        return(m_status);
    }
    
    // write PRSC seed
    if((m_status = CTPPrescalerSeedWrite(seed)) != SUCCESS) {
      CERR("ALTI: writing PRSC seed", "");
      return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerThresholdRead(const unsigned int itm, unsigned int& thr) {

    // read PRSC threshold
    if((m_status = m_alti->CTP_PSC_Threshold_Read(itm,thr)) != SUCCESS) {
        CERR("ALTI: reading PRSC threshold for item %d",itm);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerThresholdWrite(const unsigned int itm, const unsigned int thr) {

    // write PRSC threshold
    if((m_status = m_alti->CTP_PSC_Threshold_Write(itm,thr)) != SUCCESS) {
        CERR("ALTI: writing PRSC threshold for item %d",itm);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerThresholdRead(std::vector<unsigned int>& thr) {

    unsigned int itm;

    // reset PRSC threshold vector
    thr.clear();
    thr.resize(ITEM_NUMBER);

    // read PRSC threshold
    for(itm=0; itm<ITEM_NUMBER; itm++) {
        if((m_status = m_alti->CTP_PSC_Threshold_Read(itm,thr[itm])) != SUCCESS) {
            CERR("ALTI: reading PRSC threshold for item %d",itm);
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerThresholdWrite(const std::vector<unsigned int> thr) {

    // check vector size
    CHECKRANGE0(thr.size(),ITEM_NUMBER,"PRSC threshold vector size");

    unsigned int itm;

    // write PRSC threshold
    for(itm=0; itm<ITEM_NUMBER; itm++) {
        if((m_status = m_alti->CTP_PSC_Threshold_Write(itm,thr[itm])) != SUCCESS) {
            CERR("ALTI: writing PRSC threshold for item %d",itm);
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerThresholdWrite(const unsigned int thr) {

    std::vector<unsigned int> psc;
    psc.resize(ITEM_NUMBER,thr);

    // write PRSC threshold
    if((m_status = CTPPrescalerThresholdWrite(psc)) != SUCCESS) {
        CERR("ALTI: writing PRSC threshold for all items","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerThresholdRead(const std::string fn) {

    std::vector<unsigned int> thr;

    // read PRSC threshold
    if((m_status = CTPPrescalerThresholdRead(thr)) != SUCCESS) {
        CERR("ALTI: reading PRSC threshold","");
        return(m_status);
    }

    // copy data from vector into file
    if((m_status = AltiCommon::writeFile(fn,ITEM_NUMBER,thr)) != AltiCommon::SUCCESS) {
        CERR("ALTI: writing data from PRSC threshold to file \"%s\"",fn.c_str());
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerThresholdWrite(const std::string fn) {

    std::vector<unsigned int> thr(ITEM_NUMBER);
    unsigned int itm;

    // copy data from file into vector
    if((m_status = AltiCommon::readFile(fn,ITEM_NUMBER,thr)) != AltiCommon::SUCCESS) {
        CERR("ALTI: reading data for PRSC threshold from file \"%s\"",fn.c_str());
        return(m_status);
    }

    // write PRSC threshold
    for(itm=0; itm<ITEM_NUMBER; itm++) {
        if((m_status = m_alti->CTP_PSC_Threshold_Write(itm,thr[itm])) != SUCCESS) {
            CERR("ALTI: writing PRSC threshold for item %d",itm);
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPThresholdToPrescaler(const unsigned int thr, double& psc) {

    // check if threshold in allowed range
    if(thr > PRSC_THRESHOLD_MASK) {
        psc = 0.0;
        return(WRONGPAR);
    }

    // threshold is zero
    if(thr == 0) {
        psc = 1.0;
    }

    // threshold is non-zero
    else {
        psc = 1.0 - static_cast<double>(PRSC_RANDOM_FACTOR*thr - 1)/static_cast<double>(PRSC_SEED_MASK);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiModule::CTPPrescalerToThreshold(const double psc, unsigned int& thr) {
  
  // check if prescaler in allowed range
  if((psc < static_cast<double>(PRSC_RANDOM_FACTOR)/static_cast<double>(PRSC_SEED_MASK)) || (psc > 1.0)) {
    thr = 0;
    return(WRONGPAR);
  }
  thr = static_cast<unsigned int>(round(((1.0 - psc)*static_cast<double>(PRSC_SEED_MASK) + 1.0)/static_cast<double>(PRSC_RANDOM_FACTOR)));
  
  return(SUCCESS);
}

//------------------------------------------------------------------------------

int AltiModule::CTPRandomTriggerThresholdRead(const unsigned int rnd, unsigned int& thr) {

    if((m_status = m_alti->CTP_RND_Threshold_Read(rnd,thr)) != SUCCESS) {
        CERR("ALTI: reading RNDM%d threshold",rnd);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPRandomTriggerThresholdWrite(const unsigned int rnd, const unsigned int thr) {

    if((m_status = m_alti->CTP_RND_Threshold_Write(rnd,thr)) != SUCCESS) {
        CERR("ALTI: writing RNDM%d threshold",rnd);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPRandomTriggerSeedRead(const unsigned int rnd, unsigned int& thr) {

    if((m_status = m_alti->CTP_RND_Seed_Read(rnd,thr)) != SUCCESS) {
        CERR("ALTI: reading RNDM%d seed",rnd);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPRandomTriggerSeedWrite(const unsigned int rnd, const unsigned int thr) {

    if((m_status = m_alti->CTP_RND_Seed_Write(rnd,thr)) != SUCCESS) {
        CERR("ALTI: writing RNDM%d seed",rnd);
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPLeakyBucketConfigRead(const unsigned int bck, unsigned int& rate, unsigned int& level) {
  ALTI_CTP_DTM_LEAKYBUCKETCONFIG_BITSTRING bs;
  
  if((m_status = m_alti->CTP_DTM_LeakyBucketConfig_Read(bck, bs)) != SUCCESS) {
    CERR("ALTI: reading leacky bucket %d config", bck);
    return(m_status);
  }
  
  rate  = bs.LeakRate();
  level = bs.BucketLevel();
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPLeakyBucketConfigWrite(const unsigned int bck, const unsigned int rate, const unsigned int level) {
  ALTI_CTP_DTM_LEAKYBUCKETCONFIG_BITSTRING bs;
  
  if((m_status = m_alti->CTP_DTM_LeakyBucketConfig_Read(bck, bs)) != SUCCESS) {
    CERR("ALTI: reading leacky bucket %d config", bck);
    return(m_status);
  }
  
  bs.LeakRate(rate);
  bs.BucketLevel(level);

  if((m_status = m_alti->CTP_DTM_LeakyBucketConfig_Write(bck, bs)) != SUCCESS) {
    CERR("ALTI: writing leacky bucket %d config", bck);
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPLeakyBucketEnableRead(const unsigned int bck, bool& ena) {
  ALTI_CTP_DTM_LEAKYBUCKETCONFIG_BITSTRING bs;
  
  if((m_status = m_alti->CTP_DTM_LeakyBucketConfig_Read(bck, bs)) != SUCCESS) {
    CERR("ALTI: reading leacky bucket %d config", bck);
    return(m_status);
  }
  
  ena  = bs.Enable();

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPLeakyBucketEnableWrite(const unsigned int bck, const bool ena) {
  ALTI_CTP_DTM_LEAKYBUCKETCONFIG_BITSTRING bs_lb;
  if((m_status = m_alti->CTP_DTM_LeakyBucketConfig_Read(bck, bs_lb)) != SUCCESS) {
    CERR("ALTI: reading leacky bucket %d config", bck);
    return(m_status);
  } 
  bs_lb.Enable(ena);
  if((m_status = m_alti->CTP_DTM_LeakyBucketConfig_Write(bck, bs_lb)) != SUCCESS) {
    CERR("ALTI: writing leacky bucket %d config", bck);
    return(m_status);
  }

  ALTI_CTP_DTM_DEADTIMECONFIG_BITSTRING bs_dt;
  if((m_status = m_alti->CTP_DTM_DeadtimeConfig_Read(bs_dt)) != SUCCESS) {
    CERR("ALTI: reading deadtime config", "");
    return(m_status);
  }
  bs_dt.LeakyBucketEnable(bck,ena);
  if((m_status = m_alti->CTP_DTM_DeadtimeConfig_Write(bs_dt)) != SUCCESS) {
    CERR("ALTI: writing deadtime config", "");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPSlidingWindowConfigRead(unsigned int& size, unsigned int& number) {
  ALTI_CTP_DTM_SLIDINGWINDOWCONFIG_BITSTRING bs;
  
  if((m_status = m_alti->CTP_DTM_SlidingWindowConfig_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading sliding window config", "");
    return(m_status);
  }
  
  size   = bs.WindowSize();
  number = bs.L1aNumber();

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPSlidingWindowConfigWrite(const unsigned int size, const unsigned int number) {
  ALTI_CTP_DTM_SLIDINGWINDOWCONFIG_BITSTRING bs;
  
  if((m_status = m_alti->CTP_DTM_SlidingWindowConfig_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading sliding window config", "");
    return(m_status);
  }
  
  bs.WindowSize(size);
  bs.L1aNumber(number);

  if((m_status = m_alti->CTP_DTM_SlidingWindowConfig_Write(bs)) != SUCCESS) {
    CERR("ALTI: writing sliding window config", "");
    return(m_status);
  }  

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPSlidingWindowEnableRead(bool &ena) {
  ALTI_CTP_DTM_SLIDINGWINDOWCONFIG_BITSTRING bs;
  
  if((m_status = m_alti->CTP_DTM_SlidingWindowConfig_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading sliding window config", "");
    return(m_status);
  }
  
  ena  = bs.Enable();

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPSlidingWindowEnableWrite(const bool ena) {
  ALTI_CTP_DTM_SLIDINGWINDOWCONFIG_BITSTRING bs_sl;  
  if((m_status = m_alti->CTP_DTM_SlidingWindowConfig_Read(bs_sl)) != SUCCESS) {
    CERR("ALTI: reading sliding window config", "");
    return(m_status);
  }
  bs_sl.Enable(ena);
  if((m_status = m_alti->CTP_DTM_SlidingWindowConfig_Write(bs_sl)) != SUCCESS) {
    CERR("ALTI: writing sliding window config", "");
    return(m_status);
  }

  ALTI_CTP_DTM_DEADTIMECONFIG_BITSTRING bs_dt;
  if((m_status = m_alti->CTP_DTM_DeadtimeConfig_Read(bs_dt)) != SUCCESS) {
    CERR("ALTI: reading deadtime config", "");
    return(m_status);
  }
  bs_dt.SlidingWindowEnable(ena);
  if((m_status = m_alti->CTP_DTM_DeadtimeConfig_Write(bs_dt)) != SUCCESS) {
    CERR("ALTI: writing deadtime config", "");
    return(m_status);
  }


  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPSimpleDeadtimeWidthRead(unsigned int &width) {
  ALTI_CTP_DTM_DEADTIMECONFIG_BITSTRING bs;
  
  if((m_status = m_alti->CTP_DTM_DeadtimeConfig_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading deadtime config", "");
    return(m_status);
  }
  
  width = bs.SimpleDeadtimeWidth();

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPSimpleDeadtimeWidthWrite(const unsigned int width) {
  ALTI_CTP_DTM_DEADTIMECONFIG_BITSTRING bs;
  
  if((m_status = m_alti->CTP_DTM_DeadtimeConfig_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading deadtime config", "");
    return(m_status);
  }
  
  bs.SimpleDeadtimeWidth(width);

  if((m_status = m_alti->CTP_DTM_DeadtimeConfig_Write(bs)) != SUCCESS) {
    CERR("ALTI: writing deadtime config", "");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTavEnableRead(const unsigned int item, bool &ena) {
  ALTI_CTP_DTM_TAVENABLE_BITSTRING bs;

  if((m_status = m_alti->CTP_DTM_TavEnable_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading TAV enable config", "");
    return(m_status);
  }

  ena = bs.Enable(item);

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTavEnableWrite(const unsigned int item, const bool ena) {
  ALTI_CTP_DTM_TAVENABLE_BITSTRING bs;
  
  if((m_status = m_alti->CTP_DTM_TavEnable_Read(bs)) != SUCCESS) {
    CERR("ALTI: reading TAV enable config", "");
    return(m_status);
  }
  
  bs.Enable(item, ena);

  if((m_status = m_alti->CTP_DTM_TavEnable_Write(bs)) != SUCCESS) {
    CERR("ALTI: writing TAV enable config", "");
    return(m_status);
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTavEnableRead(unsigned int& wrd) {
 
  wrd = 0;
  for(u_int item = 0; item < ITEM_NUMBER; item++) {
    bool ena;
    if((m_status = CTPTavEnableRead(item, ena)) != SUCCESS) {
      CERR("ALTI: reading TAV enable for item $d", item);
      return(m_status);
    }
    wrd |= (0x1 << item);
  }
 
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTavEnableWrite(const unsigned int wrd) {

  for(u_int item = 0; item < ITEM_NUMBER; item++) {
    bool ena = ((wrd >> item) & 0x1);
    if((m_status = CTPTavEnableWrite(item, ena)) != SUCCESS) {
      CERR("ALTI: writing TAV enable for item $d", item);
      return(m_status);
    }
  }
 
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTriggerLutRead(std::vector<unsigned int> &dat) {

  // get data segment
  DataSegment dseg("AltiModule[ITEM]", ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER);
  if(dseg() != SUCCESS) {
    CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
    return(FAILURE);
  }
    
  // read trigger items data using block transfer
  if((m_status = m_alti->CTP_LUT_TriggerLut_Read(dseg.segment())) != SUCCESS) {
    CERR("ALTI:  getting CMEM segment \"%s\"",dseg.name().c_str());
    return(m_status);
  }

  // reset trigger items LUT data vector
  dat.clear();
  dat.resize(ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER);
    
  // copy data from CMEM segment to vector
  for(u_int wrd = 0; wrd < ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER; wrd++) {
      dat[wrd] = dseg[wrd];
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTriggerLutWrite(const std::vector<unsigned int> dat) {
  // check LUT vector size
  CHECKRANGE(dat.size(), ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER, ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER, "Trigger items LUT vector size");
  
  // get data segment
  DataSegment dseg("AltiModule[ITEM]", ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER);
  if(dseg() != SUCCESS) {
    CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
    return(FAILURE);
  }
  
  // copy trigger items LUT from vector to CMEM segment
  for(u_int wrd = 0; wrd < ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER; wrd++) {
    dseg[wrd] = dat[wrd];
  }
  
  // read trigger items LUT using block transfer
  if((m_status = m_alti->CTP_LUT_TriggerLut_Write(dseg.segment())) != SUCCESS) {
    CERR("ALTI:  writing CMEM segment \"%s\"",dseg.name().c_str());
    return(m_status);
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTriggerLutRead(const std::string fn) {

  std::vector<u_int> dat;
  if((m_status = CTPTriggerLutRead(dat)) != SUCCESS) {
    CERR("ALTI:  reading mini-CTP trigger items LUT","");
    return(m_status);
  }

  // copy data into file
  if ((m_status = AltiCommon::writeFile(fn, ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER, dat)) != AltiCommon::SUCCESS) {
    CERR("ALTI: writing mini-CTP trigger items LUT to file \"%s\"", fn.c_str());
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTriggerLutWrite(const std::string fn) {

  std::vector<u_int> dat;  
  // read data from file
  if ((m_status = AltiCommon::readFile(fn, dat)) != AltiCommon::SUCCESS) {
    CERR("ALTI: reading mini-CTP trigger items LUT from file \"%s\"", fn.c_str());
  }

  if((m_status = CTPTriggerLutWrite(dat)) != SUCCESS) {
    CERR("ALTI:  writing mini-CTP trigger type","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTtypLutRead(std::vector<unsigned int> &dat) {
   
  // get data segment
  DataSegment dseg("AltiModule[ITEM]", ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER);
  if(dseg() != SUCCESS) {
    CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
    return(FAILURE);
  }
    
  // read TTYP LUT data using block transfer
  if((m_status = m_alti->CTP_DTM_TriggerTypeLut_Read(dseg.segment())) != SUCCESS) {
    CERR("ALTI:  getting CMEM segment \"%s\"",dseg.name().c_str());
    return(m_status);
  }
  
  // reset TTYP LUT data vector
  dat.clear();
  dat.resize(ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER);
 
  // copy data from CMEM segment to vector
  for(u_int wrd = 0; wrd < ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER; wrd++) {
      dat[wrd] = dseg[wrd];
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTtypLutWrite(const std::vector<unsigned int> dat) {
  // check LUT vector size
  CHECKRANGE(dat.size(), ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER, ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER, "Trigger items LUT vector size");
  
  // get data segment
  DataSegment dseg("AltiModule[ITEM]", ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER);
  if(dseg() != SUCCESS) {
    CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
    return(FAILURE);
  }
  
  // copy trigger type LUT from vector to CMEM segment
  for(u_int wrd = 0; wrd < ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER; wrd++) {
    dseg[wrd] = dat[wrd];
  }
  
  // read trigger type LUT using block transfer
  if((m_status = m_alti->CTP_DTM_TriggerTypeLut_Write(dseg.segment())) != SUCCESS) {
    CERR("ALTI:  writing CMEM segment \"%s\"",dseg.name().c_str());
    return(m_status);
  }
  
  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPTtypLutRead(const std::string fn) {
  std::vector<u_int> dat;
  if((m_status = CTPTtypLutRead(dat)) != SUCCESS) {
    CERR("ALTI:  reading mini-CTP trigger type LUT","");
    return(m_status);
  }
  
  // copy data into file
  if ((m_status = AltiCommon::writeFile(fn, ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER, dat)) != AltiCommon::SUCCESS) {
    CERR("ALTI: writing mini-CTP trigger type LUT to file \"%s\"", fn.c_str());
  }

  return(m_status);
}
    
//------------------------------------------------------------------------------

int AltiModule::CTPTtypLutWrite(const std::string fn) {
  std::vector<u_int> dat;  
  // read data from file
  if ((m_status = AltiCommon::readFile(fn, dat)) != AltiCommon::SUCCESS) {
    CERR("ALTI: reading mini-CTP trigger items LUT from file \"%s\"", fn.c_str());
  }
  
  if((m_status = CTPTtypLutWrite(dat)) != SUCCESS) {
    CERR("ALTI:  writing mini-CTP trigger type LUT","");
    return(m_status);
  }

  return(m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidMaskRead(const unsigned int bcid, bool &mask) {

    // check BCID
    CHECKRANGE0(bcid, BCID_NUMBER - 1, "CNT BCID number");

    unsigned int wrd(bcid/WORD_SIZE), off(bcid % WORD_SIZE), data;

    // read BCID mask
    if ((m_status = m_alti->CTP_CNT_BcidMask_Read(wrd, data)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT BCID mask %4d", bcid);
        return (m_status);
    }
    mask = data & (0x00000001 << off);

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidMaskWrite(const unsigned int bcid, const bool mask) {

    // check BCID
    CHECKRANGE0(bcid, BCID_NUMBER - 1, "CNT BCID number");

    unsigned int wrd(bcid/WORD_SIZE), off(bcid % WORD_SIZE), data;

    // read BCID mask
    if ((m_status = m_alti->CTP_CNT_BcidMask_Read(wrd, data)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT BCID mask %4d", bcid);
        return (m_status);
    }

    // modify BGRP data
    data = mask ? (data | (0x00000001 << off)) : (data & (~(0x00000001 << off)));

    // write BCID mask
    if ((m_status = m_alti->CTP_CNT_BcidMask_Write(wrd, data)) != SUCCESS) {
        CERR("ALTI: writing CTP CNT BCID mask %4d", bcid);
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidMaskRead(std::vector<bool> &mask) {

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // read BCID mask using block transfer
    if ((m_status = m_alti->CTP_CNT_BcidMask_Read(dseg.segment())) != SUCCESS) {
        CERR("ALTI: reading CTP CNT BCID mask using block transfer", "");
        return (m_status);
    }

    unsigned int wrd, bit, off, num(0);

    // reset BCID mask vector
    mask.clear();
    mask.resize(BCID_NUMBER);

    // copy BCID mask from CMEM segment to vector
    for (wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
        off = 0x00000001;
        for (bit = 0; bit < WORD_SIZE; bit++) {
            mask[num] = (dseg[wrd] & off);
            off <<= 1;
            num++;
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidMaskWrite(const std::vector<bool> &mask) {

    unsigned int wrd, bit, off, num(0);

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // copy BCID mask from vector to CMEM segment
    for (wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
        off = 0x00000001;
        dseg[wrd] = 0x00000000;
        for (bit = 0; bit < WORD_SIZE; bit++) {
            if (mask[num]) dseg[wrd] |= off;
            off <<= 1;
            num++;
        }
    }

    // write BCID mask using block transfer
    if ((m_status = m_alti->CTP_CNT_BcidMask_Write(dseg.segment())) != SUCCESS) {
        CERR("ALTI: writing CTP CNT BCID mask using block transfer", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidMaskRead(std::vector<unsigned int> &mask) {

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_WORD_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // read BCID mask using block transfer
    if ((m_status = m_alti->CTP_CNT_BcidMask_Read(dseg.segment())) != SUCCESS) {
        CERR("ALTI: reading CTP CNT BCID mask using block transfer", "");
        return (m_status);
    }

    unsigned int wrd, bit, off, num(0);

    // reset BCID mask vector
    mask.clear();
    mask.resize(BCID_NUMBER);

    // copy BCID mask from CMEM segment to vector
    for (wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
        off = 0x00000001;
        for (bit = 0; bit < WORD_SIZE; bit++) {
            mask[num] = (dseg[wrd] & off) ? 0x00000001 : 0x00000000;
            off <<= 1;
            num++;
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidMaskWrite(const std::vector<unsigned int> &mask) {

    unsigned int wrd, bit, off, num(0);

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_WORD_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"",dseg.name().c_str());
        return (FAILURE);
    }

    // copy BCID mask from vector to CMEM segment
    for (wrd = 0; wrd < BCID_WORD_NUMBER; wrd++) {
        off = 0x00000001;
        dseg[wrd] = 0x00000000;
        for (bit = 0; bit < WORD_SIZE; bit++) {
            if (mask[num] != 0x00000000) dseg[wrd] |= off;
            off <<= 1;
            num++;
        }
    }

    // write BCID mask using block transfer
    if ((m_status = m_alti->CTP_CNT_BcidMask_Write(dseg.segment())) != SUCCESS) {
        CERR("ALTI: writing CTP CNT BCID mask using block transfer", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidMaskRead(const std::string &fn) {

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_WORD_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // read BCID mask using block transfer
    if ((m_status = m_alti->CTP_CNT_BcidMask_Read(dseg.segment())) != SUCCESS) {
        CERR("ALTI: reading CTP CNT BCID mask using block transfer", "");
        return (m_status);
    }

    // copy data from CMEM segment into file
    if ((m_status = AltiCommon::writeFile(fn, BCID_WORD_NUMBER, dseg.data())) != AltiCommon::SUCCESS) {
        CERR("ALTI: writing data from CNT BCID mask to file \"%s\"", fn.c_str());
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidMaskWrite(const std::string &fn) {

    // get data segment
    DataSegment dseg("AltiModule[BCID]", BCID_WORD_NUMBER);
    if (dseg() != SUCCESS) {
        CERR("ALTI: getting CMEM segment \"%s\"", dseg.name().c_str());
        return (FAILURE);
    }

    // copy data from file into CMEM segment
    if ((m_status = AltiCommon::readFile(fn, BCID_WORD_NUMBER, dseg.data())) != AltiCommon::SUCCESS) {
        CERR("ALTI: reading data from CNT BCID mask from file \"%s\"", fn.c_str());
        return (m_status);
    }

    // write BCID mask using block transfer
    if ((m_status = m_alti->CTP_CNT_BcidMask_Write(dseg.segment())) != SUCCESS) {
        CERR("ALTI: writing CTP CNT BCID mask using block transfer", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidOffsetRead(unsigned int &off) {

    // read BCID offset
    if ((m_status = m_alti->CTP_CNT_BcidOffset_Read(off)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT BCID offset", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntBcidOffsetWrite(const unsigned int off) {

    // write BCID offset
    if ((m_status = m_alti->CTP_CNT_BcidOffset_Write(off)) != SUCCESS) {
        CERR("ALTI: writing CTP CNT BCID offset", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntControlPrint() {

    // print control
    ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT enable", "");
        return (m_status);
    }
    ctl.print();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntEnableRead(bool &ena, bool &sta) {

    // read CNT enable
    ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CNT enable", "");
        return (m_status);
    }
    ena = ctl.EnableCounters();
    sta = ctl.EnableStatus();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntEnableWrite(const bool ena) {

    // read-modify-write CNT enable
    ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT enable", "");
        return (m_status);
    }
    ctl.EnableCounters(ena);
    if ((m_status = m_alti->CTP_CNT_Control_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CTP CNT enable", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntEnableWait(const bool ena) {

    unsigned int itim(0);

    while (true) {
        ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
        if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
            CERR("ALTI: reading CTP CNT enable", "");
            return (m_status);
        }
        if (ena != ctl.EnableCounters()) {
            CERR("ALTI: waiting for CTP CNT enable to be \"%s\" while it is \"%s\"", ena ? "ENABLED" : "DISABLED", ctl.EnableCounters() ? "ENABLED" : "DISABLED");
            ctl.print();
            return (WRONGPAR);
        }
        if(ctl.EnableStatus() == ena) return (m_status);
        if ((++itim) == CNT_CONTROL_TIMEOUT) {
            CERR("ALTI: timeout waiting for CTP CNT enable status to be \"%s\":", ena ? "ACTIVE" : "NOT_ACTIVE");
            return (TIMEOUT);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntClearWrite() {

    // read-modify-write CNT clear
    ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT clear", "");
        return (m_status);
    }
    ctl.ClearCounters(true);
    if ((m_status = m_alti->CTP_CNT_Control_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CTP CNT clear", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntClearWait() {

    unsigned int itim(0);

    while (true) {
        ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
        if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
            CERR("ALTI: reading CNT clear", "");
            return (m_status);
        }
        if (!ctl.ClearBusy()) return (m_status);
        if ((++itim) == CNT_CONTROL_TIMEOUT) {
            CERR("ALTI: timeout waiting for CNT clear to finish", "");
            return (TIMEOUT);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntCopyWrite() {

    // read-modify-write CTP CNT copy
    ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT copy", "");
        return (m_status);
    }
    ctl.CopyCounters(true);
    if ((m_status = m_alti->CTP_CNT_Control_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CTP CNT copy", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntCopyWait() {

    unsigned int itim(0);

    while (true) {
        ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
        if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
            CERR("ALTI: reading CTP CNT copy", "");
            return (m_status);
        }
        if (!ctl.CopyBusy()) return (m_status);
        if ((++itim) == CNT_CONTROL_TIMEOUT) {
            CERR("ALTI: timeout waiting for CTP CNT copy to finish", "");
            return (TIMEOUT);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntCopyClearWrite() {

    // read-modify-write CNT copy
    ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
    if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT copy/clear", "");
        return (m_status);
    }
    ctl.CopyCounters(true);
    ctl.ClearCounters(true);
    if ((m_status = m_alti->CTP_CNT_Control_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CTP CNT copy/clear", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntCopyClearWait() {

    unsigned int itim(0);

    while (true) {
        ALTI_CTP_CNT_CONTROL_BITSTRING ctl;
        if ((m_status = m_alti->CTP_CNT_Control_Read(ctl)) != SUCCESS) {
            CERR("ALTI: reading CTP CNT copy/clear", "");
            return (m_status);
        }
        if (!ctl.CopyBusy() && !ctl.ClearBusy()) return (m_status);
        if ((++itim) == CNT_CONTROL_TIMEOUT) {
            CERR("ALTI: timeout waiting for CNT copy/clear to finish", "");
            return (TIMEOUT);
        }
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntTurnCountRead(unsigned int &trn) {

    // read TURN counter
    if ((m_status = m_alti->CTP_CNT_TurnCounter_Read(trn)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT TURN counter", "");
        return (m_status);
    }

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntCounterRead(AltiMiniCtpCounter *cnt) {

    if (cnt->getCmemSegment()) {
        if ((m_status = m_alti->CTP_CNT_CounterValue_Read(cnt->getCmemSegment(), cnt->dsize())) != SUCCESS) {
            CERR("ALTI: reading CTP CNT using block transfer", "");
            return (m_status);
        }
	if(cnt->type() == MINICTP_TAV) {
	  if((m_status = m_alti->CTP_CNT_L1aCounter_Read((*cnt)[cnt->dsize()])) != SUCCESS) {
	    CERR("ALTI: reading CTP CNT \"L1A\"", "");
	    return(m_status);
	  }
	}
    }

    // read TURN counter
    unsigned int turn;
    if ((m_status = CTPCntTurnCountRead(turn)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT TURN counter", "");
        return (m_status);
    }
    cnt->turn(turn);

    return (m_status);
}
  
//------------------------------------------------------------------------------

int AltiModule::CTPCntTbpCounterRead(const unsigned int item, unsigned int &cnt) {

  if ((m_status = m_alti->CTP_CNT_TbpCounter_Read(item, cnt)) != SUCCESS) {
    CERR("ALTI: reading CTP TAP counters", "");
    return (m_status);
    }
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntTapCounterRead(const unsigned int item, unsigned int &cnt) {
 
  if ((m_status = m_alti->CTP_CNT_TapCounter_Read(item, cnt)) != SUCCESS) {
    CERR("ALTI: reading CTP TAP counters", "");
    return (m_status);
  }
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntTavCounterRead(const unsigned int item, unsigned int &cnt) {

  if ((m_status = m_alti->CTP_CNT_TavCounter_Read(item, cnt)) != SUCCESS) {
    CERR("ALTI: reading CTP TAV counters", "");
    return (m_status);
  }
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntL1aCounterRead(unsigned int &cnt) {

  if ((m_status = m_alti->CTP_CNT_L1aCounter_Read(cnt)) != SUCCESS) {
    CERR("ALTI: reading CTP L1A counter", "");
    return (m_status);
  }
  return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntRequestPrint() {

    // print request
    ALTI_CTP_CNT_REQUEST_BITSTRING ctl;
    if ((m_status = m_alti->CTP_CNT_Request_Read(ctl)) != SUCCESS) {
        CERR("ALTI: reading CTP CNT request", "");
        return (m_status);
    }
    ctl.print();

    return (m_status);
}

//------------------------------------------------------------------------------

int AltiModule::CTPCntRequestWrite(const bool cnt, const bool trn, const bool tst) {

    // print request
    ALTI_CTP_CNT_REQUEST_BITSTRING ctl;
    ctl.EnableCountOverflow(cnt ? "ENABLED" : "DISABLED");
    ctl.EnableTurnOverflow(trn ? "ENABLED" : "DISABLED");
    ctl.AssertRequest(tst ? "ENABLED" : "DISABLED");
    if ((m_status = m_alti->CTP_CNT_Request_Write(ctl)) != SUCCESS) {
        CERR("ALTI: writing CTP CNT request", "");
        return (m_status);
    }

    return (m_status);
}
