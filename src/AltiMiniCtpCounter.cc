//******************************************************************************
// file: AltiMiniCtpCounter.cc
// desc: library for ALTI counter (scalars)
//******************************************************************************

#include "ALTI/AltiCommon.h"
#include "ALTI/AltiMiniCtpCounter.h"
#include "RCDUtilities/RCDUtilities.h"

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>

using namespace LVL1;


//------------------------------------------------------------------------------

const std::string AltiMiniCtpCounter::Error::TYPE_NAME[TYPE_NUMBER] = {
    "TBP", "TAP", "TAV", "L1A"
};

const unsigned int AltiMiniCtpCounter::Error::TYPE_MASK[TYPE_NUMBER] = {
    0x00000001, 0x00000002, 0x00000004, 0x00000008
};

//------------------------------------------------------------------------------

AltiMiniCtpCounter::Error::Error(const int num) {

    // reset to num
    reset(num);
}

//------------------------------------------------------------------------------

AltiMiniCtpCounter::Error::~Error() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void AltiMiniCtpCounter::Error::reset(const int num) {

  for (unsigned int typ = 0; typ < TYPE_NUMBER; typ++) {
    m_error[typ] = num;
    m_bit_error[typ] = num;
  }
}

//------------------------------------------------------------------------------

int64_t AltiMiniCtpCounter::Error::error() const {

    int64_t sum(0);

    for (unsigned int typ = 0; typ < TYPE_NUMBER; typ++) sum += m_error[typ];
   
    return (sum);
}

//------------------------------------------------------------------------------

void AltiMiniCtpCounter::Error::print(const Error &msk, const int num) const {

    double perc, ebit;

    for (unsigned int t = 0; t < TYPE_NUMBER; t++) {
      if (msk[(TYPE) t] != 0) {
	perc = (num) ? (((double) m_error[t])/((double) num)/double(DSIZE[t])*100.0) : (0.0);
	ebit = (m_error[t]) ? (((double) m_bit_error[t])/((double) m_error[t])) : (0.0);
	std::printf("   number of %-20s : %16" PRId64 " => %11.3f%% and %7.3f bit/err\n", (TYPE_NAME[t] + " errors").c_str(), m_error[t], perc, ebit);
      }
    }
}

//------------------------------------------------------------------------------

std::string AltiMiniCtpCounter::Error::printTypeMask(const unsigned int mask) {

    std::ostringstream buf;
    bool flag(false);

    for (unsigned int i = 0; i < TYPE_NUMBER; i++) {
        if (mask & TYPE_MASK[i]) {
            buf << (flag ? "|" : "") << TYPE_NAME[i];
            flag = true;
        }
    }

    return (buf.str());
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

const unsigned int AltiMiniCtpCounter::DSIZE[AltiModule::MINICTP_CNT_NUMBER] = {
  AltiModule::ITEM_NUMBER,
  AltiModule::ITEM_NUMBER,
  AltiModule::ITEM_NUMBER
};

const unsigned int AltiMiniCtpCounter::LSIZE[AltiModule::MINICTP_CNT_NUMBER] = {
  AltiModule::ITEM_NUMBER,
  AltiModule::ITEM_NUMBER,
  AltiModule::ITEM_NUMBER + 1 // add L1A counter in TAV
};

//------------------------------------------------------------------------------

AltiMiniCtpCounter::AltiMiniCtpCounter(const AltiModule::MINICTP_CNT_TYPE typ) : m_segment(0), m_type(typ), m_turn(0) {

    // create data array
    m_data = new unsigned int[size()];
}

//------------------------------------------------------------------------------

AltiMiniCtpCounter::AltiMiniCtpCounter(RCD::CMEMSegment *seg, const AltiModule::MINICTP_CNT_TYPE typ) : m_segment(seg), m_type(typ), m_turn(0) {

    // get virtual address
    m_data = (unsigned int *) m_segment->VirtualAddress();
}

//------------------------------------------------------------------------------

AltiMiniCtpCounter::~AltiMiniCtpCounter() {

    // delete data array
    if (!m_segment) delete [] m_data;
}

//------------------------------------------------------------------------------

unsigned int AltiMiniCtpCounter::overflow() const {

    unsigned int ovf(0), i;

    for (i = 0; i < size(); i++) if (overflow(i)) ovf++;

    return (ovf);
}

//------------------------------------------------------------------------------

void AltiMiniCtpCounter::dump() const {

    unsigned int i;
   
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI: counters" << std::endl;

    for (i = 0; i < DSIZE[m_type]; i++) {
      std::printf("item-%d: %s0x%08x", i, overflow(i) ? "O" : " ", counter(i));
      std::cout << std::endl;
    }
    // L1A
    if(m_type == AltiModule::MINICTP_TAV) {
      std::cout << "                       ----------------------------------------------------------------------------------------------" << std::endl;
      std::printf("Counter [\"L1A\"]    : ");
      std::printf(" %s0x%08x \n", overflow(DSIZE[m_type]) ? "O" : " ", counter(DSIZE[m_type]));
      std::cout << std::endl;
    }

    std::printf("TURN          :  %s0x%08x (= %10d)\n", turnOverflow() ? "O" : " ", m_turn, m_turn);
    std::printf("              :   overflow    = %10d\n", overflow());
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

void AltiMiniCtpCounter::dump(const AltiMiniCtpCounter &cnt, const Error &msk) const {

    bool err(msk[static_cast<AltiMiniCtpCounter::Error::TYPE>(m_type)]);
    unsigned int i;

    std::cout << ">> ------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI:" << std::endl;

    for (i = 0; i < DSIZE[m_type]; i++) {
      std::printf("item-%d: %s%s0x%08x \n", i, (err && (counter(i) != cnt.counter(i))) ? "E" : " ", overflow(i) ? "O" : " ", counter(i));
    }
    // L1A
    if(m_type == AltiModule::MINICTP_TAV) {
      err = (msk[AltiMiniCtpCounter::Error::L1A]);
      std::cout << "                          -------------------------------------------------------------------------------------------------------" << std::endl;
      std::printf("CounterRate [\"L1A\"]    : ");
      std::printf(" %s%s0x%08x",err && counter(DSIZE[m_type]) != cnt.counter(DSIZE[m_type]) ? "E" : " ", overflow(DSIZE[m_type]) ? "O" : " ", counter(DSIZE[m_type]));
    }

    std::printf("TURN        : %s%s0x%08x (= %s%10d)\n", (err && ((turn() != cnt.turn()))) ? "E" : " ", turnOverflow() ? "O" : " ", m_turn, (err && ((turn() != cnt.turn()))) ? "E" : " ", m_turn);
    std::printf("            :   overflow    = %s%10d\n", (overflow() != cnt.overflow()) ? "E" : " ", overflow());
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

// double has at least 15 decimal digits precision: 4 before decimal point + 11 after decimal point
const double AltiMiniCtpCounterRate::EPSILON = 0.00000000001;

//------------------------------------------------------------------------------

AltiMiniCtpCounterRate::AltiMiniCtpCounterRate(const AltiModule::MINICTP_CNT_TYPE t) : m_type(t) {

    m_name = AltiModule::MINICTP_CNT_NAME[m_type];
    m_size = AltiMiniCtpCounter::LSIZE[m_type];
    m_data = new double[m_size];
    m_turn = 0.0;

    // reset state
    resetState();

    // reset data
    for (unsigned int i = 0; i < m_size; i++) m_data[i] = 0.0;
}

//------------------------------------------------------------------------------

AltiMiniCtpCounterRate::AltiMiniCtpCounterRate(const AltiMiniCtpCounter &cnt) {

    m_name = AltiModule::MINICTP_CNT_NAME[static_cast<int>(cnt.type())];
    m_type = cnt.type();
    m_size = cnt.size();
    m_data = new double[m_size];
    m_turn = static_cast<double>(cnt.turn());

    // reset state
    resetState();

    // set data and update state
    for (unsigned int i = 0; i < m_size; i++) {
        m_data[i] = (m_turn <= 0.0) ? 0.0 : (static_cast<double>(cnt.counter(i))/m_turn);
        if (std::abs(m_data[i] - round(m_data[i])) > EPSILON) m_fraction++;
    }
}

//------------------------------------------------------------------------------

AltiMiniCtpCounterRate::AltiMiniCtpCounterRate(const AltiMiniCtpCounterRate &cnt) {

    m_name = cnt.name();
    //m_type = cnt.type();
    m_size = cnt.size();
    m_data = new double[m_size];
    m_turn = cnt.turn();

    // reset state with state from counter
    m_fraction = cnt.fraction();
    m_error = -1;
    m_infinity = cnt.infinity();

    // set data
    for (unsigned int i = 0; i < m_size; i++) m_data[i] = cnt.counter(i);
}

//------------------------------------------------------------------------------

AltiMiniCtpCounterRate::~AltiMiniCtpCounterRate() {

    if (m_data) delete [] m_data;
}

//------------------------------------------------------------------------------

AltiMiniCtpCounterRate &AltiMiniCtpCounterRate::operator=(const AltiMiniCtpCounter &cnt) {

    m_name = AltiModule::MINICTP_CNT_NAME[static_cast<int>(cnt.type())];
    if (m_size != cnt.size()) {
        m_size = cnt.size();
        if (m_data) delete [] m_data;
        m_data = new double[m_size];
    }
    m_turn = static_cast<double>(cnt.turn());

    // reset state
    resetState();

    // copy data and update state
    for (unsigned int i = 0; i < m_size; i++) {
        m_data[i] = (m_turn <= 0.0) ? 0.0 : (static_cast<double>(cnt.counter(i))/m_turn);
        if (std::abs(m_data[i] - round(m_data[i])) > EPSILON) m_fraction++;
    }

    return (*this);
}

//------------------------------------------------------------------------------

AltiMiniCtpCounterRate &AltiMiniCtpCounterRate::operator=(const AltiMiniCtpCounterRate &cnt) {

    m_name = cnt.name();
    if (m_size != cnt.size()) {
        m_size = cnt.size();
        if (m_data) delete [] m_data;
        m_data = new double[m_size];
    }
    m_turn = cnt.turn();

    // reset state with state from counter
    m_fraction = cnt.fraction();
    m_error = -1;
    m_infinity = cnt.infinity();

    // copy data
    for (unsigned int i = 0; i < m_size; i++) {
        m_data[i] = cnt.counter(i);
    }

    return (*this);
}

//------------------------------------------------------------------------------

AltiMiniCtpCounterRate AltiMiniCtpCounterRate::operator/(const AltiMiniCtpCounterRate &cnt) const {

    if (m_size != cnt.m_size) {
        CERR("wrong size for division %d != %d expected", cnt.m_size, m_size);
        return (*this);
    }

    AltiMiniCtpCounterRate frq(*this);

    // reset state
    frq.resetState();

    // copy data and update state
    for (unsigned int i = 0; i < m_size; i++) {
        if (m_data[i] <= 0.0) {
            frq.m_data[i] = 0.0;
            if (cnt.m_data[i] <= 0.0) frq.m_infinity++;
        }
        else if (cnt.m_data[i] <= 0) {
            frq.m_data[i] = 0.0;
            frq.m_infinity++;
        }
        else {
            frq.m_data[i] = (m_data[i]/cnt.m_data[i]);
            if (std::abs(frq.m_data[i] - round(frq.m_data[i])) > EPSILON) frq.m_fraction++;
        }
    }

    return (frq);
}

//------------------------------------------------------------------------------

int AltiMiniCtpCounterRate::setFraction() {

    // reset state
    resetState();

    // fill data and update state
    for (unsigned int i = 0; i < AltiMiniCtpCounter::DSIZE[m_type]; i++) {
        if (std::abs(m_data[i] - round(m_data[i])) > EPSILON) m_fraction++;
    }

    return (AltiModule::SUCCESS);
}

//------------------------------------------------------------------------------

int AltiMiniCtpCounterRate::scale(const double scl) {

    // reset state
    resetState();

    for (unsigned int i = 0; i < m_size; i++) {
        m_data[i] = m_data[i]*scl;
        if (std::abs(m_data[i] - round(m_data[i])) > EPSILON) m_fraction++;
    }

    return (AltiModule::SUCCESS);
}

//------------------------------------------------------------------------------

unsigned int AltiMiniCtpCounterRate::size(const unsigned int n)  {

    m_size = n;

    if (m_data) delete [] m_data;
    m_data = new double[m_size];
    for (unsigned int i = 0; i < m_size; i++) m_data[i] = 0.0;

    m_turn = 0.0;
    m_fraction = -1;
    m_error = -1;
    m_infinity = -1;

    return (m_size);
}


//------------------------------------------------------------------------------

void AltiMiniCtpCounterRate::dump() const {

    unsigned int i;
    
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI: rates in kHz" << std::endl;
 
    for (i = 0; i < AltiMiniCtpCounter::DSIZE[m_type]; i++) {
      std::printf("item-%d rate: %12.6f \n",i, m_data[i]);
    }
    // L1A
    if(m_type == AltiModule::MINICTP_TAV) {
      std::cout << "                       ----------------------------------------------------------------------------------------------" << std::endl;
      std::printf("Counter [\"L1A\"] rate:  %12.6f \n", m_data[AltiMiniCtpCounter::DSIZE[m_type]]);
    }
 
    std::printf("TURN          :  %12.3f\n", m_turn);
    if ((m_fraction > -1) || (m_infinity > -1)) std::cout << "            " << "  : ";
    if (m_fraction > -1) std::cout << ((m_fraction) ? " FRACTION = " : " fraction = ") << m_fraction;
    if (m_infinity > -1) std::cout << ((m_infinity) ? ", INFINITY = " : ", infinity = ") << m_infinity;
    if ((m_fraction > -1) || (m_infinity > -1)) std::cout << std::endl;
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

void AltiMiniCtpCounterRate::dump(const AltiMiniCtpCounterRate &cnt) const {

    unsigned int i;

    std::cout << ">> --------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI: rate in kHz" << std::endl;

    for (i = 0; i < AltiMiniCtpCounter::DSIZE[m_type]; i++) {
      std::printf("item-%d rate: %s%12.6f \n",i, std::abs(m_data[i] - cnt[i])>EPSILON?"E":" ", m_data[i]);
    }
    // L1A
    if(m_type == AltiModule::MINICTP_TAV) {
      std::cout << "                       ----------------------------------------------------------------------------------------------" << std::endl;
      std::printf("Counter [\"L1A\"] rate:  %s%12.6f \n", std::abs(m_data[AltiMiniCtpCounter::DSIZE[m_type]] - cnt[AltiMiniCtpCounter::DSIZE[m_type]])>EPSILON?"E":" ", m_data[AltiMiniCtpCounter::DSIZE[m_type]]);
    }

    std::cout << "                          -------------------------------------------------------------------------------------------------------" << std::endl;
    std::printf("TURN          %12.3f\n", m_turn);
    if ((m_fraction > -1) || (m_error > -1) || (m_infinity > -1)) std::printf("                : ");
    if (m_fraction > -1) std::cout << ((m_fraction) ? " FRACTION = "  : " fraction = ") << m_fraction;
    if (m_error > -1)    std::cout << ((m_error)    ? ", ERROR = "    : ", error = ") << m_error;
    if (m_infinity > -1) std::cout << ((m_infinity) ? ", INFINITY = " : ", infinity = ") << m_infinity;
    if ((m_fraction > -1) || (m_error > -1) || (m_infinity > -1)) std::cout << std::endl;
    std::cout << ">> --------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

void AltiMiniCtpCounterRate::dump(const AltiMiniCtpCounterRate &cnt, const AltiMiniCtpCounter::Error &msk) const {

    unsigned int i;
    bool err;

    std::cout << ">> --------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI: rate in kHz" << std::endl;

    err = (msk[static_cast<AltiMiniCtpCounter::Error::TYPE>(0)]);

    for (i = 0; i < AltiMiniCtpCounter::DSIZE[m_type]; i++) {
      std::printf("item-%d rate: %s%12.6f \n",i, err && (std::abs(m_data[i] - cnt[i])>EPSILON) ? "E" : " ",m_data[i]);
    }
    // L1A
    if(m_type == AltiModule::MINICTP_TAV) {
      std::cout << "                       ----------------------------------------------------------------------------------------------" << std::endl;
      std::printf("Counter [\"L1A\"] rate:  %s%12.6f \n", err && (std::abs(m_data[AltiMiniCtpCounter::DSIZE[m_type]] - cnt[AltiMiniCtpCounter::DSIZE[m_type]])>EPSILON) ? "E" : " ", m_data[AltiMiniCtpCounter::DSIZE[m_type]]);
    }
    
    std::cout << "                          -------------------------------------------------------------------------------------------------------" << std::endl;
    std::printf("TURN          %12.3f\n", m_turn);
    if ((m_fraction > -1) || (m_error > -1) || (m_infinity > -1)) std::printf("                : ");
    if (m_fraction > -1) std::cout << ((m_fraction) ? " FRACTION = "  : " fraction = ") << m_fraction;
    if (m_error > -1)    std::cout << ((m_error)    ? ", ERROR = "    : ", error = ") << m_error;
    if (m_infinity > -1) std::cout << ((m_infinity) ? ", INFINITY = " : ", infinity = ") << m_infinity;
    if ((m_fraction > -1) || (m_error > -1) || (m_infinity > -1)) std::cout << std::endl;
    std::cout << ">> --------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

void AltiMiniCtpCounterRate::dump(const AltiMiniCtpCounterRate &cnt, const AltiMiniCtpCounterRate &sig, const AltiMiniCtpCounter::Error &msk) const {

    unsigned int i/*, j*/;
    bool err;

    std::cout << ">> --------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI: rate in kHz" << std::endl;

    err = (msk[static_cast<AltiMiniCtpCounter::Error::TYPE>(0)]);

    for (i = 0; i < AltiMiniCtpCounter::DSIZE[m_type]; i++) {
      std::printf("item-%d rate: %s%12.6f \n", i, err && (std::abs(m_data[i] - cnt[i])>std::sqrt(sig[i]*sig[i]+EPSILON*EPSILON)) ? "E" : " ", m_data[i]);
		  }
    // L1A
    if(m_type == AltiModule::MINICTP_TAV) {
      std::cout << "                       ----------------------------------------------------------------------------------------------" << std::endl;
      std::printf("Counter [\"L1A\"] rate:  %s%12.6f \n", err && (std::abs(m_data[AltiMiniCtpCounter::DSIZE[m_type]] - cnt[AltiMiniCtpCounter::DSIZE[m_type]])>std::sqrt(sig[AltiMiniCtpCounter::DSIZE[m_type]]*sig[AltiMiniCtpCounter::DSIZE[m_type]]+EPSILON*EPSILON)) ? "E" : " ", m_data[AltiMiniCtpCounter::DSIZE[m_type]]);
    }

    std::cout << "                          -------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "TURN        " << "    : ";
    std::printf("  %12.3f\n", m_turn);
    if ((m_fraction > -1) || (m_error > -1) || (m_infinity > -1)) std::cout << "            " << "    : ";
    if (m_fraction > -1) std::cout << ((m_fraction) ? " FRACTION = "  : " fraction = ") << m_fraction;
    if (m_error > -1)    std::cout << ((m_error)    ? ", ERROR = "    : ", error = ") << m_error;
    if (m_infinity > -1) std::cout << ((m_infinity) ? ", INFINITY = " : ", infinity = ") << m_infinity;
    if ((m_fraction > -1) || (m_error > -1) || (m_infinity > -1)) std::cout << std::endl;
    std::cout << ">> --------------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

void AltiMiniCtpCounterRate::dumpSigma(const AltiMiniCtpCounterRate &sig) const {

    unsigned int i;

    std::cout << ">> ------------------------------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << ">> ALTI:" << std::endl;

    // DATA: TBP, TAP, or TAV
    for (i = 0; i < AltiMiniCtpCounter::DSIZE[m_type]; i++) {
        std::printf("item-%d rate:  %12.6f +- %.6f \n", i, m_data[i], sig[i]);
    }
    std::cout << "                          -------------------------------------------------------------------------------------------------------" << std::endl;

    // L1A
    if(m_type == AltiModule::MINICTP_TAV) {
      std::printf("Counter [\"L1A\"] rate:  %12.6f +- %.6f \n", m_data[AltiMiniCtpCounter::DSIZE[m_type]], sig[AltiMiniCtpCounter::DSIZE[m_type]]);
    }

    std::printf("TURN            :  %12.3f\n", m_turn);
    if ((m_fraction > -1) || (m_infinity > -1)) std::cout << "            " << "    : ";
    if (m_fraction > -1) std::cout << ((m_fraction) ? " FRACTION = " : " fraction = ") << m_fraction;
    if (m_infinity > -1) std::cout << ((m_infinity) ? ", INFINITY = " : ", infinity = ") << m_infinity;
    if ((m_fraction > -1) || (m_infinity > -1)) std::cout << std::endl;
    std::cout << ">> ------------------------------------------------------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

int AltiMiniCtpCounterRate::check(const AltiMiniCtpCounterRate &cnt) {

    // reset error
    m_error = 0;

    // for all DATA
    for (unsigned int i = 0; i < m_size; i++) {
        if (std::abs(m_data[i] - cnt[i]) > EPSILON) m_error++;
    }

    return (m_error);
}

//------------------------------------------------------------------------------

int AltiMiniCtpCounterRate::check(const AltiMiniCtpCounterRate &cnt, const AltiMiniCtpCounter::Error &msk, AltiMiniCtpCounter::Error &err) {

    // reset error
    m_error = 0;

    // DATA: TBP, TAP, or TAV
    if (msk[static_cast<AltiMiniCtpCounter::Error::TYPE>(m_type)]) {
        for (unsigned int i = 0; i < AltiMiniCtpCounter::DSIZE[m_type]; i++) {
            if (std::abs(m_data[i] - cnt[i]) > EPSILON) {
                err.m_error[m_type]++;
                err.m_bit_error[m_type] += checkWord(m_data[i], cnt[i]);
                m_error++;
            }
        }
    }

    // L1A
    if ((m_type == AltiModule::MINICTP_TAV) && (msk[AltiMiniCtpCounter::Error::L1A])) {
      if (std::abs(m_data[AltiMiniCtpCounter::DSIZE[m_type]] - cnt[AltiMiniCtpCounter::DSIZE[m_type]]) > EPSILON) {
	err.m_error[AltiMiniCtpCounter::Error::L1A]++;
	err.m_bit_error[AltiMiniCtpCounter::Error::L1A] += checkWord(m_data[AltiMiniCtpCounter::DSIZE[m_type]], cnt[AltiMiniCtpCounter::DSIZE[m_type]]);
	m_error++;
      }
    }

    return (m_error);
}

//------------------------------------------------------------------------------

int AltiMiniCtpCounterRate::check(const AltiMiniCtpCounterRate &cnt, const AltiMiniCtpCounterRate &sig, const AltiMiniCtpCounter::Error &msk, AltiMiniCtpCounter::Error &err) {

    // reset error
    m_error = 0;

    // DATA: TBP, TAP, or TAV
    if (msk[static_cast<AltiMiniCtpCounter::Error::TYPE>(m_type)]) {
        for (unsigned int i = 0; i < AltiMiniCtpCounter::DSIZE[m_type]; i++) {
            CDBG("CNT[%s%03d]: data = %15.8f, cnt = %12.8f, tol = %12.8f, dev = %12.8f%%", (AltiMiniCtpCounter::Error::TYPE_NAME[m_type].c_str()) "CNT", i, m_data[i], cnt[i], sig[i], std::abs(m_data[i] - cnt[i])/std::sqrt(sig[i]*sig[i] + EPSILON*EPSILON)*100.0);
            if (std::abs(m_data[i] - cnt[i]) > std::sqrt(sig[i]*sig[i] + EPSILON*EPSILON)) {
                err.m_error[m_type]++;
                err.m_bit_error[m_type] += checkWord(m_data[i], cnt[i]);
                m_error++;
            }
        }
    }

 // L1A
    if ((m_type == AltiModule::MINICTP_TAV) && (msk[AltiMiniCtpCounter::Error::L1A])) {
      if (std::abs(m_data[AltiMiniCtpCounter::DSIZE[m_type]] - cnt[AltiMiniCtpCounter::DSIZE[m_type]]) >  std::sqrt(sig[AltiMiniCtpCounter::DSIZE[m_type]]*sig[AltiMiniCtpCounter::DSIZE[m_type]] + EPSILON*EPSILON)) {
	err.m_error[AltiMiniCtpCounter::Error::L1A]++;
	err.m_bit_error[AltiMiniCtpCounter::Error::L1A] += checkWord(m_data[AltiMiniCtpCounter::DSIZE[m_type]], cnt[AltiMiniCtpCounter::DSIZE[m_type]]);
	m_error++;
      }
    }

    return (m_error);
}

//------------------------------------------------------------------------------

int AltiMiniCtpCounterRate::checkWord(const unsigned int data, const unsigned int datb) const {

    unsigned int i;
    int nbit(0);

    // count number of bit errors
    for (i = 0; i < WORD_SIZE; i++) {
        if ((data & (1U << i)) != (datb & (1U << i))) nbit++;
    }

    // return bit error
    return (nbit);
}

//------------------------------------------------------------------------------

void AltiMiniCtpCounterRate::resetState() {

    // reset state
    m_fraction = 0;
    m_error = -1;
    m_infinity = -1;
}
