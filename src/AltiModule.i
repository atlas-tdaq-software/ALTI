%module(directors="1") AltiModule
%{
#include <fstream>

#include "ALTI/AltiModule.h"

using namespace LVL1;
%}

%include <typemaps.i>
//%include "stdint.i"
%include "std_string.i"
%include "std_vector.i"

%template(BoolVector) std::vector<bool>;
%template(uIntVector) std::vector<unsigned int>;

//%apply std::string& INPUT  { const std::string & };

//typedef u_int unsigned int;
%apply unsigned int &OUTPUT { unsigned int& };
//%apply u_int &OUTPUT { int &, int & };

%feature("director") AltiModule;

%extend LVL1::AltiModule {
  int PATWriteFile(RCD::CMEMSegment *segment, const std::string fn) {
    std::ifstream ifs;
    ifs.open(fn.c_str(), std::ifstream::in);
    int rtnv = $self->PRMModeWrite("PATTERN");
    rtnv |= $self->PRMWriteFile(segment, ifs);
    ifs.close();
    return rtnv;
  }
};

%extend LVL1::AltiModule {
  int PATWriteFile(const std::string fn) {
    std::ifstream ifs;
    ifs.open(fn.c_str(), std::ifstream::in);
    int rtnv = $self->PRMModeWrite("PATTERN");
    rtnv |= $self->PRMWriteFile(ifs);
    ifs.close();
    return rtnv;
  }
};

%extend LVL1::AltiModule {
  int DECReadFile(RCD::CMEMSegment *segment0, RCD::CMEMSegment *segment1, const unsigned int start, const unsigned int num, unsigned int bcr_word, int bcr_delay, const int format, const std::string fn) {
    std::ofstream ofs;
    ofs.open(fn.c_str(), std::ofstream::out);
    int rtnv = $self->DECReadFile(segment0, segment1, start, num, bcr_word, bcr_delay, format, ofs);
    ofs.close();
    return rtnv;
  }
};

%extend LVL1::AltiModule {
  int SNPReadFile(RCD::CMEMSegment *segment0, const unsigned int start, const unsigned int num, const int format, const std::string fn) {
    std::ofstream ofs;
    ofs.open(fn.c_str(), std::ofstream::out);
    int rtnv = $self->PRMModeWrite("SNAPSHOT");
    rtnv |= $self->PRMReadFile(segment0, ofs, start, start + num);
    ofs.close();
    return rtnv;
  }
};

%extend LVL1::AltiModule {
  int SNPReadFile(const unsigned int start, const unsigned int num, const int format, const std::string fn) {
    std::ofstream ofs;
    ofs.open(fn.c_str(), std::ofstream::out);
    int rtnv = $self->PRMModeWrite("SNAPSHOT");
    rtnv |= $self->PRMReadFile(ofs, start, start + num);
    ofs.close();
    return rtnv;
  }
};

%include "RCDBitString/BitSet.h"
%include "ALTI/ALTI_BITSTRING.h"
%include "ALTI/ALTI.h"

%apply bool &OUTPUT { bool& };

%include "ALTI/AltiBusyCounter.h"
%include "ALTI/AltiModule.h"
