//******************************************************************************
// file: AltiConfiguration.cc
// desc: configuration for the ALTI module
// auth: 28-NOV-2017 P. Kuzmanovic
//*****************************************************************************

#include "ALTI/AltiConfiguration.h"
#include "ALTI/AltiCommon.h"
#include "RCDUtilities/RCDUtilities.h"

#include <set>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <filesystem>
#include <iostream>
#include <iomanip>

using namespace LVL1;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

std::string AltiConfiguration::BucketConfiguration::print() const {
  
  char os[STRING_LENGTH];
  
  std::sprintf(os,"rate   =  %4d, level  = %3d, %s", rate, level, enable? " ENABLED" : "DISABLED");
  
  return(os);
}

//------------------------------------------------------------------------------

std::string AltiConfiguration::SlidingWindowConfiguration::print() const {

    char os[STRING_LENGTH];

    std::sprintf(os,"window = %5d, number = %3d, %s", window, number, enable ? " ENABLED" : "DISABLED");

    return(os);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

AltiConfiguration::AltiConfiguration() {

    // default values

    clk_config                   = true;
    clk_select                   = AltiModule::JITTER_CLEANER;
    clk_jc_select                = AltiModule::OSCILLATOR;
    clk_jc_file                  = "";
    clk_phase_shift              = 0;

    sig_config                   = true;
    sig_fp_bgo2enable            = true;
    sig_fp_bgo3enable            = true;
    unsigned int i, j;
    for (j = 0; j < AltiModule::SIGNAL_DESTINATION_NUMBER; j++) {
        for (i = 0; i < AltiModule::SIGNAL_NUMBER_ROUTED; i++) {
            sig_swx[i][j] = AltiModule::SIGNAL_SOURCE::CTP_IN;
        }
        sig_swx_ttyp[j] = AltiModule::SIGNAL_SOURCE::CTP_IN;
    }
   
    sig_eqz_ctp_in_enable = false;
    sig_eqz_ctp_in_mode = AltiModule::SHORT_CABLE;
    sig_eqz_alti_in_enable = false;
    sig_eqz_alti_in_mode = AltiModule::SHORT_CABLE;
    sig_io_sync_phase.resize(AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR3 + 1);
    sig_io_shaping.resize(AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR3 + 1);
    for (i = 0; i <= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR3; i++) {
        sig_io_sync_phase[i]     = 0; 
        sig_io_shaping[i]        = 0; //no shaping
    }

    sig_orb_source = "INACTIVE";
    sig_l1a_source = "INACTIVE";
    for(i = 0; i < 3; i++) {
      sig_ttr_source[i] = "INACTIVE";
    }
    for(i = 0; i < 4; i++) {
      sig_bgo_source[i] = "INACTIVE";
    }
    sig_ttyp_source = "INACTIVE";

    sig_orb_input_source = "EXTERNAL";

    turn_cnt_mask = 0xffffffff;
    turn_cnt_max = 16;

    bgo2_l1a_delay = 100;

    bsy_config                   = true;
    bsy_in_select.resize(AltiModule::BUSY_INPUT_NUMBER);
    for (i = 0; i < AltiModule::BUSY_INPUT_NUMBER; i++) {
        bsy_in_select[i]         = false;
    }
    bsy_out_select.resize(AltiModule::BUSY_OUTPUT_NUMBER);
    for (i = 0; i < AltiModule::BUSY_OUTPUT_NUMBER; i++) {
        bsy_out_select[i]        = AltiModule::BUSY_INACTIVE;
    }
    bsy_data_vme                 = false;
    bsy_l1a_masked = false;
    bsy_ttr_masked.resize(3);
    for (i = 0; i < 3; i++) {
      bsy_ttr_masked[0]       = false;
    }
    bsy_bgo_masked.resize(4);
    for (i = 0; i < 4; i++) {
      bsy_bgo_masked[0]       = false;
    }
    bsy_level                    = AltiModule::NIM;

    crq_config                   = true;
    crq_in_select.resize(3);
    for (i = 0; i < 3; i++) {
        crq_in_select[i]         = AltiModule::CALREQ_FROM_FRONT_PANEL;
    }
    crq_out_select[0].resize(3);
    crq_out_select[1].resize(3);
    for (i = 0; i < 3; i++) {
        crq_out_select[0][i]     = AltiModule::CALREQ_INACTIVE;
        crq_out_select[1][i]     = AltiModule::CALREQ_INACTIVE;
    }
    crq_data_vme.resize(ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER);
    for(i=0; i<ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER; i++) {;
      crq_data_vme[i]                = false;
    }
    
    mem_config                   = true;
    mem_pg_enable                = false;
    mem_pg_repeat                = false;
    mem_pg_start_addr            = 0x00000000;
    mem_pg_stop_addr             = 0x00000000;
    mem_pg_file                  = "";
    mem_snap_masked.resize(AltiModule::SIGNAL_NUMBER);
    for (i = 0; i < AltiModule::SIGNAL_NUMBER; i++) {
        mem_snap_masked[i]       = false;
    }

    ttc_config                   = true;
    for (i = 0; i < AltiModule::TRANSMITTER_NUMBER; i++) {
        ttc_tx_enable[i]         = false;
    };
    for (i = 0; i < AltiModule::TRANSMITTER_NUMBER; i++) {
        ttc_tx_delay[i]         = 0;
    };
    ttc_l1a_source.resize(7);
    for (i = 0; i < 7; i++) ttc_l1a_source[i] = false; // external L1A, TTR1-3, CALREQ, pattern, mini-CTP
    ttc_orb_source              = ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::ORBIT_TYPE_NAME[0];
    ttc_bgo_source.resize(AltiModule::TTC_FIFO_NUMBER);
    for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) ttc_bgo_source[i] = ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::BGO_TYPE_NAME[0];
    ttc_ttyp_source             = ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::ORBIT_TYPE_NAME[0];
    ttc_bgo_inhibit_width.resize(AltiModule::TTC_FIFO_NUMBER);
    ttc_bgo_inhibit_delay.resize(AltiModule::TTC_FIFO_NUMBER);
    for (i = 0; i <= 3; i++) {
        ttc_bgo_inhibit_width[i] = 0;
        ttc_bgo_inhibit_delay[i] = 0;
    }
    ttc_ttyp_delay               = 2;
    ttc_ttyp_addr                = 0x000;
    ttc_ttyp_subaddr             = 0x00;
    ttc_ttyp_address_space       = AltiModule::TTC_ADDRESS_SPACE::EXTERNAL;
    ttc_bgo_mode.resize(AltiModule::TTC_FIFO_NUMBER);
    for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
        ttc_bgo_mode[i] = AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_BGO_SIGNAL;
    }
    ttc_fifo_retransmit.resize(AltiModule::TTC_FIFO_NUMBER);
    for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
        ttc_fifo_retransmit[i] = false;
    }
    ttc_fifo_word.resize(AltiModule::TTC_FIFO_NUMBER);
    for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
        ttc_fifo_word[i] = 0x0;
    }

    cnt_config                  = true;
    cnt_enable                  = true;
    cnt_ttyp_lut.resize(ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER);    

    pbm_config                  = true;
    pbm_bcid_offset             = 0;
    
    // mini-CTP
    ctp_config                  = true;
    ctp_l1a_source  = "PATTERN";
    ctp_bgo2_source = "PATTERN";
    ctp_bgo3_source = "PATTERN";
    for(i = 0; i < 3; i++) {
      ctp_ttr_source[i] = "PATTERN";
    }
    for(i = 0; i < 3; i++) {
      ctp_crq_source[i] = "PATTERN";
    }

    ctp_item_lut.resize(AltiModule::ITEM_LUT_SIZE);
    ctp_ttyp_lut.resize(AltiModule::TTYP_LUT_SIZE);

    ctp_rndm_seed.resize(ALTI::CTP_RND_THRESHOLD_NUMBER, 0x1234567d);
    ctp_rndm_seed_valid          = true;
    ctp_rndm_threshold.resize(ALTI::CTP_RND_THRESHOLD_NUMBER);
    
    ctp_bgrp.resize(AltiModule::BGRP_WORD_TOTAL);
    ctp_bgrp_bcid_offset         = AltiModule::ORBIT_LENGTH - 11;
    ctp_bgrp_mask.resize(AltiModule::ITEM_NUMBER);

    ctp_prsc_seed.resize(AltiModule::ITEM_NUMBER);
    ctp_prsc_seed_valid             = true;
    ctp_prsc_threshold.resize(AltiModule::ITEM_NUMBER);

    ctp_tav_enable = 0;

    ctp_smpl_deadtime = 4;    
    for(u_int bck = 0; bck < AltiModule::LEAKY_BUCKET_NUMBER; bck++) {
      ctp_cplx_bucket[bck].rate   = 0x00000000;
      ctp_cplx_bucket[bck].level  = 0x00000000;
      ctp_cplx_bucket[bck].enable = false;
    }
    ctp_cplx_sliding_window.window = 0x00000000;
    ctp_cplx_sliding_window.number = 0x00000000;
    ctp_cplx_sliding_window.enable = false;
	
    setDefaultFileNames();
}

//------------------------------------------------------------------------------

AltiConfiguration::AltiConfiguration(const std::string &pfx) : AltiConfiguration() {

    // add file prefix
    addFileNamePrefix(pfx);
}

//------------------------------------------------------------------------------

AltiConfiguration::~AltiConfiguration() {

    // nothing to be done
}


//------------------------------------------------------------------------------

void AltiConfiguration::setDefaultFileNames() {

    mem_pg_file                  = "";
    cnt_ttyp_lut_file            = "";

    ctp_item_lut_file            = "";
    ctp_ttyp_lut_file            = "";   

    ctp_bgrp_file                = "";
    ctp_bgrp_mask_file           = "";

    ctp_prsc_seed_file           = "";
    ctp_prsc_threshold_file      = "";
    
}

//------------------------------------------------------------------------------

void AltiConfiguration::clearFileNames() {

    mem_pg_file                 = "";
    cnt_ttyp_lut_file           = "";
    
    ctp_item_lut_file           = "";
    ctp_ttyp_lut_file           = "";   

    ctp_bgrp_file               = "";
    ctp_bgrp_mask_file          = "";

    ctp_prsc_seed_file          = "";
    ctp_prsc_threshold_file     = "";
    
}

//------------------------------------------------------------------------------

void AltiConfiguration::addFileNamePrefix(const std::string &pfx) {

  
  if(!mem_pg_file.empty())             mem_pg_file             = pfx + mem_pg_file;
  if(!cnt_ttyp_lut_file.empty())       cnt_ttyp_lut_file       = pfx + cnt_ttyp_lut_file;

  if(!ctp_item_lut_file.empty())       ctp_item_lut_file       = pfx + ctp_item_lut_file;
  if(!ctp_ttyp_lut_file.empty())       ctp_ttyp_lut_file       = pfx + ctp_ttyp_lut_file;   

  if(!ctp_bgrp_file.empty())           ctp_bgrp_file           = pfx + ctp_bgrp_file;
  if(!ctp_bgrp_mask_file.empty())      ctp_bgrp_mask_file      = pfx + ctp_bgrp_mask_file;

  if(!ctp_prsc_seed_file.empty())      ctp_prsc_seed_file      = pfx + ctp_prsc_seed_file;
  if(!ctp_prsc_threshold_file.empty()) ctp_prsc_threshold_file = pfx + ctp_prsc_threshold_file;
    
}

//------------------------------------------------------------------------------

void AltiConfiguration::config(const bool cfg) {

    clk_config  = cfg;
    sig_config  = cfg;
    mem_config  = cfg;
    bsy_config  = cfg;
    crq_config  = cfg;
    ttc_config  = cfg;
    cnt_config  = cfg;
    ctp_config  = cfg;
}

//------------------------------------------------------------------------------

void AltiConfiguration::print(std::ostream &s) const {

    unsigned int i;
    char os[STRING_LENGTH];

    std::vector<std::string> arg;
    //int num;

    s << std::endl;
    std::sprintf(os, "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"); s << os;
    std::sprintf(os, "AltiConfiguration:\n"); s << os;
    std::sprintf(os, "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"); s << os;

    if (clk_config) {
        std::sprintf(os, "CLK source                           = %s\n", AltiModule::CLK_PLL_NAME[clk_select].c_str()); s << os;
        std::sprintf(os, "CLK jitter cleaner source            = %s\n", AltiModule::CLK_JC_NAME[clk_jc_select].c_str()); s << os;
	std::sprintf(os, "CLK jitter cleaner configuration     = %s\n", clk_jc_file == "" ? "DEFAULT" : clk_jc_file.c_str()); s << os;
	std::sprintf(os, "CLK output phase shift               = %f ns\n", clk_phase_shift*0.015); s << os;
        s << "------------------------------------------------------------------------------------------------------" << std::endl;
    }

    if (sig_config) {
        unsigned int i, ttyp = 0;
        AltiModule::SIGNAL signal;
        std::sprintf(os, "SIG BGO front panel output selection = %s, %s\n", sig_fp_bgo2enable ? "BGO2" : "BGO0", sig_fp_bgo3enable ? "BGO3" : "BGO1"); s << os;
        for (i = 0; i < AltiModule::SIGNAL_NUMBER_ROUTED; i++) {
            signal = (AltiModule::SIGNAL) i;
            if ((AltiModule::SIGNAL_NAME[signal]).rfind("TTYP", 0) == 0) {
                if (ttyp++ == 0) {
                    std::sprintf(os, "SIG %-5s   switch    configuration  = %s, %s, %s, %s\n", "TTYP", AltiModule::SIGNAL_SOURCE_NAME[sig_swx_ttyp[0]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx_ttyp[1]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx_ttyp[2]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx_ttyp[3]].c_str()); s << os;
                }
            }
            else {
                std::sprintf(os, "SIG %-5s   switch    configuration  = %s, %s, %s, %s\n", AltiModule::SIGNAL_NAME[signal].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx[i][0]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx[i][1]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx[i][2]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx[i][3]].c_str()); s << os;
            }
        }
      
	std::sprintf(os, "SIG %-7s equalizer configuration  = %s, %s\n", AltiModule::SIGNAL_SOURCE_NAME[AltiModule::CTP_IN].c_str(), AltiModule::EQUALIZER_CONFIG_NAME[sig_eqz_ctp_in_mode].c_str(), sig_eqz_ctp_in_enable ? "ENABLED" : "DISABLED"); s << os;
	std::sprintf(os, "SIG %-7s equalizer configuration  = %s, %s\n", AltiModule::SIGNAL_SOURCE_NAME[AltiModule::ALTI_IN].c_str(), AltiModule::EQUALIZER_CONFIG_NAME[sig_eqz_alti_in_mode].c_str(), sig_eqz_alti_in_enable ? "ENABLED" : "DISABLED"); s << os;

        unsigned int phase[8];
        unsigned int shape_enable[8];

        phase[0] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_ORB];
        shape_enable[0] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_ORB];
        std::sprintf(os, "SIG IO sync  ORB      from switch    = %d\n", phase[0]); s << os;
        std::sprintf(os, "SIG IO shape ORB      from switch    = %s\n", AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str()); s << os;
        phase[0] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_L1A];
        shape_enable[0] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_L1A];
        std::sprintf(os, "SIG IO sync  L1A      from switch    = %d\n", phase[0]); s << os;
        std::sprintf(os, "SIG IO shape L1A      from switch    = %s\n", AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str()); s << os;
        phase[0] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO0];
        phase[1] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO1];
        phase[2] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO2];
        phase[3] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO3];
        shape_enable[0] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO0];
        shape_enable[1] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO1];
        shape_enable[2] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO2];
        shape_enable[3] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO3];
        std::sprintf(os, "SIG IO sync  BGO0..3  from switch    = %d, %d, %d, %d\n", phase[0], phase[1], phase[2], phase[3]); s << os;
        std::sprintf(os, "SIG IO shape BGO0..3  from switch    = %s, %s, %s, %s\n", AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[1]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[2]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[3]].c_str()); s << os;
	phase[0] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP0];
        phase[1] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP1];
        phase[2] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP2];
        phase[3] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP3];
        phase[4] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP4];
        phase[5] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP5];
        phase[6] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP6];
        phase[7] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP7];
        shape_enable[0] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP0];
        shape_enable[1] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP1];
        shape_enable[2] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP2];
        shape_enable[3] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP3];
        shape_enable[4] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP4];
        shape_enable[5] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP5];
        shape_enable[6] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP6];
        shape_enable[7] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP7];
        std::sprintf(os, "SIG IO sync  TTYP0..3 from switch    = %d, %d, %d, %d\n", phase[0], phase[1], phase[2], phase[3]); s << os;
        std::sprintf(os, "SIG IO shape TTYP0..3 from switch    = %s, %s, %s, %s\n", AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[1]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[2]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[3]].c_str()); s << os;
        std::sprintf(os, "SIG IO sync  TTYP4..7 from switch    = %d, %d, %d, %d\n", phase[4], phase[5], phase[6], phase[7]); s << os;
        std::sprintf(os, "SIG IO shape TTYP4..7 from switch    = %s, %s, %s, %s\n", AltiModule::SYNC_SHAPE_NAME[shape_enable[4]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[5]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[6]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[7]].c_str()); s << os;
        phase[0] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR1];
        phase[1] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR2];
        phase[2] = sig_io_sync_phase[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR3];
        shape_enable[0] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR1];
        shape_enable[1] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR2];
        shape_enable[2] = sig_io_shaping[AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR3];
        std::sprintf(os, "SIG IO sync  TTR1..3  from switch    = %d, %d, %d\n", phase[0], phase[1], phase[2]); s << os;
        std::sprintf(os, "SIG IO shape TTR1..3  from switch    = %s, %s, %s\n", AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[1]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[2]].c_str()); s << os;
        
        s << "------------------------------------------------------------------------------------------------------" << std::endl;

        std::sprintf(os, "SIG source ORB                       = %s\n", sig_orb_source.c_str()); s << os;
	std::sprintf(os, "SIG source L1A                       = %s\n", sig_l1a_source.c_str()); s << os;
	std::sprintf(os, "SIG source TTR1..3                   = %s, %s, %s\n", sig_ttr_source[0].c_str(), sig_ttr_source[1].c_str(), sig_ttr_source[2].c_str()); s << os;
	std::sprintf(os, "SIG source BGO0..3                   = %s, %s, %s, %s\n", sig_bgo_source[0].c_str(), sig_bgo_source[1].c_str(), sig_bgo_source[2].c_str(), sig_bgo_source[3].c_str()); s << os;
	std::sprintf(os, "SIG source TTYP                      = %s\n", sig_ttyp_source.c_str()); s << os;

	s << "------------------------------------------------------------------------------------------------------" << std::endl;
	
	std::sprintf(os, "SIG input source ORB                 = %s\n", sig_orb_input_source.c_str()); s << os;

	s << "------------------------------------------------------------------------------------------------------" << std::endl;

	std::sprintf(os, "Turn counter mask                    = 0x%08x\n", turn_cnt_mask); s << os;
	std::sprintf(os, "Turn counter frequency               = %d\n", turn_cnt_max); s << os;

	s << "------------------------------------------------------------------------------------------------------" << std::endl;

	std::sprintf(os, "BGo-2 - L1A delay                    = %d\n", bgo2_l1a_delay); s << os;

	s << "------------------------------------------------------------------------------------------------------" << std::endl;

    }

    if (bsy_config) {
        std::sprintf(os, "BSY from front panel                 = %s\n", bsy_in_select[0] ? "ENABLED" : "DISABLED"); s << os;
        std::sprintf(os, "BSY from CTP                         = %s\n", bsy_in_select[1] ? "ENABLED" : "DISABLED"); s << os;
        std::sprintf(os, "BSY from ALTI                        = %s\n", bsy_in_select[2] ? "ENABLED" : "DISABLED"); s << os;
        std::sprintf(os, "BSY from pattern generation          = %s\n", bsy_in_select[3] ? "ENABLED" : "DISABLED"); s << os;
        std::sprintf(os, "BSY from VME register                = %s\n", bsy_in_select[4] ? "ENABLED" : "DISABLED"); s << os;
        std::sprintf(os, "BSY from ECR                         = %s\n", bsy_in_select[5] ? "ENABLED" : "DISABLED"); s << os;
        /*num = */AltiCommon::splitString(AltiModule::BUSY_SOURCE_NAME[bsy_out_select[0]], "_", arg);
        std::sprintf(os, "BSY to CTP                           = %s\n", arg[1].c_str()); s << os;
        /*num = */AltiCommon::splitString(AltiModule::BUSY_SOURCE_NAME[bsy_out_select[1]], "_", arg);
        std::sprintf(os, "BSY to ALTI                          = %s\n", arg[1].c_str()); s << os;
        std::sprintf(os, "BSY const level VME register         = %d\n", bsy_data_vme); s << os;
        std::sprintf(os, "BSY mask of L1A                      = %s\n", bsy_l1a_masked ? "ENABLED" : "DISABLED"); s << os;
        std::sprintf(os, "BSY mask of TTR1..3                  = %s,%s,%s\n", bsy_ttr_masked[0] ? "ENABLED" : "DISABLED", bsy_ttr_masked[1] ? "ENABLED" : "DISABLED", bsy_ttr_masked[2] ? "ENABLED" : "DISABLED"); s << os;
        std::sprintf(os, "BSY mask of BGO0..3                  = %s,%s,%s,%s\n", bsy_bgo_masked[0] ? "ENABLED" : "DISABLED", bsy_bgo_masked[1] ? "ENABLED" : "DISABLED", bsy_bgo_masked[2] ? "ENABLED" : "DISABLED", bsy_bgo_masked[3] ? "ENABLED" : "DISABLED"); s << os;
	std::sprintf(os, "BSY Front-Panel level                = %s\n", AltiModule::BUSY_LEVEL_NAME[bsy_level].c_str()); s << os;
        s << "------------------------------------------------------------------------------------------------------" << std::endl;
    }

    if (crq_config) {
        std::sprintf(os, "CRQ source                           = %s,%s,%s\n", AltiModule::CALREQ_INPUT_NAME[crq_in_select[2]].c_str(), AltiModule::CALREQ_INPUT_NAME[crq_in_select[1]].c_str(), AltiModule::CALREQ_INPUT_NAME[crq_in_select[0]].c_str()); s << os;
        std::sprintf(os, "CRQ to CTP                           = %s,%s,%s\n", AltiModule::CALREQ_SOURCE_NAME[crq_out_select[0][2]].c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[0][1]].c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[0][0]].c_str()); s << os;
        std::sprintf(os, "CRQ to ALTI                          = %s,%s,%s\n", AltiModule::CALREQ_SOURCE_NAME[crq_out_select[1][2]].c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[1][1]].c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[1][0]].c_str()); s << os;
        std::sprintf(os, "CRQ const level VME register         = %s, %s, %s\n", crq_data_vme[0]? "ENABLED" : "DISABLED", crq_data_vme[0]? "ENABLED" : "DISABLED", crq_data_vme[0]? "ENABLED" : "DISABLED" ); s << os;
        s << "------------------------------------------------------------------------------------------------------" << std::endl;
    }

    if (mem_config) {
      
        std::sprintf(os, "MEM pattern generation start address = 0x%08x\n", mem_pg_start_addr); s << os;
        std::sprintf(os, "MEM pattern generation stop  address = 0x%08x\n", mem_pg_stop_addr); s << os;
	if(std::filesystem::is_symlink(mem_pg_file.c_str())) {
	  std::sprintf(os, "MEM pattern generation file          = %s\n", std::filesystem::read_symlink(mem_pg_file.c_str()).c_str()); s << os;
	}
	else {
	  std::sprintf(os, "MEM pattern generation file          = %s\n", mem_pg_file.c_str()); s << os;
	}
        std::sprintf(os, "MEM pattern generation               = %s\n", mem_pg_enable ? "ENABLED" : "DISABLED"); s << os;
        std::sprintf(os, "MEM pattern generation repeat        = %s\n", mem_pg_repeat ? "REPEATED" : "ONESHOT"); s << os;
        std::sprintf(os, "MEM snapshot mask ORB                = %s\n", mem_snap_masked[AltiModule::ORB] ? "MASKED" : "NOT_MASKED"); s << os;
        std::sprintf(os, "MEM snapshot mask L1A                = %s\n", mem_snap_masked[AltiModule::L1A] ? "MASKED" : "NOT_MASKED"); s << os;
        std::sprintf(os, "MEM snapshot mask TTR1..3            = %s,%s,%s\n", mem_snap_masked[AltiModule::TTR1] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::TTR2] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::TTR3] ? "MASKED" : "NOT_MASKED"); s << os;
        std::sprintf(os, "MEM snapshot mask TTYP0..3           = %s,%s,%s,%s\n", mem_snap_masked[AltiModule::TTYP0] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::TTYP1] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::TTYP2] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::TTYP3] ? "MASKED" : "NOT_MASKED"); s << os;
        std::sprintf(os, "MEM snapshot mask TTYP4..7           = %s,%s,%s,%s\n", mem_snap_masked[AltiModule::TTYP4] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::TTYP5] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::TTYP6] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::TTYP7] ? "MASKED" : "NOT_MASKED"); s << os;
        std::sprintf(os, "MEM snapshot mask BGO0..3            = %s,%s,%s,%s\n", mem_snap_masked[AltiModule::BGO0] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::BGO1] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::BGO2] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::BGO3] ? "MASKED" : "NOT_MASKED"); s << os;
        std::sprintf(os, "MEM snapshot mask BUSY               = %s\n", mem_snap_masked[AltiModule::BUSY] ? "MASKED" : "NOT_MASKED"); s << os;
        std::sprintf(os, "MEM snapshot mask CALREQ0..2         = %s,%s,%s\n", mem_snap_masked[AltiModule::CALREQ0] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::CALREQ1] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::CALREQ2] ? "MASKED" : "NOT_MASKED"); s << os;
        s << "------------------------------------------------------------------------------------------------------" << std::endl;
    }

    if (ttc_config) {
        for (i = 0; i < AltiModule::TRANSMITTER_NUMBER; i++) {
	  std::sprintf(os, "TTC TX[%02d]                           = %s, delay = %d (3.12ns steps)\n", i, ttc_tx_enable[i] ? "ENABLED" : "DISABLED", ttc_tx_delay[i]); s << os;
        } 
	std::sprintf(os, "TTC encoder L1 source from L1A       = %s\n", ttc_l1a_source[0] ? "ENABLED" : "DISABLED"); s << os;
	std::sprintf(os, "TTC encoder L1 source from TTR1..3   = %s,%s,%s\n", ttc_l1a_source[1] ? "ENABLED" : "DISABLED", ttc_l1a_source[2] ? "ENABLED" : "DISABLED", ttc_l1a_source[3] ? "ENABLED" : "DISABLED"); s << os;
	std::sprintf(os, "TTC encoder L1 source from CALREQ    = %s\n", ttc_l1a_source[4] ? "ENABLED" : "DISABLED"); s << os;
	std::sprintf(os, "TTC encoder L1 source from pattern   = %s\n", ttc_l1a_source[5] ? "ENABLED" : "DISABLED"); s << os;
	std::sprintf(os, "TTC encoder L1 source from mini-CTP  = %s\n", ttc_l1a_source[6] ? "ENABLED" : "DISABLED"); s << os;
	std::sprintf(os, "TTC encoder ORB source               = %s\n", ttc_orb_source.c_str()); s << os;
	for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
	  std::sprintf(os, "TTC encoder BGO%d source              = %s\n", i, ttc_bgo_source[i].c_str()); s << os;
	}
	std::sprintf(os, "TTC encoder TTYP source              = %s\n", ttc_ttyp_source.c_str()); s << os;
	for (i = 0; i <= 3; i++) {
            std::sprintf(os, "TTC encoder BGO%d inhibit parameters  = width %4d, delay %4d\n", i, ttc_bgo_inhibit_width[i], ttc_bgo_inhibit_delay[i]); s << os;
        }
        std::sprintf(os, "TTC encoder TTYP delay               = %d\n", ttc_ttyp_delay); s << os;
        std::sprintf(os, "TTC encoder TTYP word settings       = address 0x%04x, subaddress 0x%02x, %s\n", ttc_ttyp_addr, ttc_ttyp_subaddr, AltiModule::TTC_ADDRESS_SPACE_NAME[ttc_ttyp_address_space].c_str()); s << os;
        for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
            std::sprintf(os, "TTC encoder BGO%d mode                = %s\n", i, AltiModule::TTC_BGO_MODE_NAME[ttc_bgo_mode[i]].c_str()); s << os;
        }
        for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
            std::string fifoName = AltiModule::TTC_FIFO_NAME[(AltiModule::TTC_FIFO) i];
            fifoName.replace(fifoName.find("_"), 1, " ");
            std::sprintf(os, "TTC encoder %-9s retransmit     = %s\n", fifoName.c_str(), ttc_fifo_retransmit[i] ? "ENABLED" : "DISABLED"); s << os;
        }
	for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
	  std::string fifoName = AltiModule::TTC_FIFO_NAME[(AltiModule::TTC_FIFO) i];
	  fifoName.replace(fifoName.find("_"), 1, " ");
	  std::sprintf(os, "TTC encoder %-9s word           = 0x%02x\n", fifoName.c_str(), ttc_fifo_word[i]); s << os;
        }
        s << "------------------------------------------------------------------------------------------------------" << std::endl;
    }

    if (cnt_config) {
        std::sprintf(os, "counters                             = %s\n", cnt_enable ? "ENABLED" : "DISABLED"); s << os;
	if(cnt_ttyp_lut_file.empty()) {
	  std::sprintf(os,"CNT trigger type LUT                  = CONFIGURATION {0x%08x, 0x%08x .. 0x%08x, 0x%08x}\n",cnt_ttyp_lut[0], cnt_ttyp_lut[1], cnt_ttyp_lut[ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER-2], cnt_ttyp_lut[ALTI::CNT_TRIGGERTYPELUT_INDEX_NUMBER-1]); s << os;
	}
	else {
	  std::sprintf(os, "CNT trigger type LUT file            = %s\n", cnt_ttyp_lut_file.c_str()); s << os;
	}
        s << "------------------------------------------------------------------------------------------------------" << std::endl;
    }

    s << "------------------------------------------------------------------------------------------------------" << std::endl;

    if(pbm_config) {
      std::sprintf(os, "PBM BCID offset                      = %4d\n", pbm_bcid_offset); s << os;
    }
    
    s << "------------------------------------------------------------------------------------------------------" << std::endl;
    
    if (ctp_config) {
      std::sprintf(os, "CTP ECR generation                    = %s\n", ctp_ecr_generation.print().c_str()); s << os;
      
      std::sprintf(os, "CTP source L1A                        = %s\n", ctp_l1a_source.c_str()); s << os;
      std::sprintf(os, "CTP source BGo-2                      = %s\n", ctp_bgo2_source.c_str()); s << os;
      std::sprintf(os, "CTP source BGo-3                      = %s\n", ctp_bgo3_source.c_str()); s << os;
      std::sprintf(os, "CTP source TTR1..3                    = %s, %s, %s\n", ctp_ttr_source[0].c_str(), ctp_ttr_source[1].c_str(), ctp_ttr_source[2].c_str()); s << os;
      std::sprintf(os, "CTP source CALREQ0..2                 = %s, %s, %s\n", ctp_crq_source[0].c_str(), ctp_crq_source[1].c_str(), ctp_crq_source[2].c_str()); s << os;
      
      if(ctp_item_lut_file.empty()) {
	std::sprintf(os,"CTP item LUT                          = CONFIGURATION {0x%08x, 0x%08x .. 0x%08x, 0x%08x}\n",ctp_item_lut[0], ctp_item_lut[1], ctp_item_lut[ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER-2], ctp_item_lut[ALTI::CTP_LUT_TRIGGERLUT_INDEX_NUMBER-1]); s << os;
      }
      else {
	std::sprintf(os,"CTP item LUT file                     = %s\n", ctp_item_lut_file.c_str()); s << os;
      }
    
      if(ctp_ttyp_lut_file.empty()) {
	std::sprintf(os,"CTP TTYP LUT                          = CONFIGURATION {0x%08x, 0x%08x .. 0x%08x, 0x%08x}\n",ctp_ttyp_lut[0], ctp_ttyp_lut[1], ctp_ttyp_lut[ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER-2], ctp_ttyp_lut[ALTI::CTP_DTM_TRIGGERTYPELUT_INDEX_NUMBER-1]); s << os;
      }
      else {
	std::sprintf(os,"CTP TTYP LUT file                     = %s\n", ctp_ttyp_lut_file.c_str()); s << os;
      }

      if(ctp_rndm_seed_valid) {
	s << "CTP RNDM seed                         = ";
	bool flag = false;
	for(u_int i =0; i < ALTI::CTP_RND_THRESHOLD_NUMBER; i++) {
	  std::sprintf(os,"%s0x%08x",flag?", ":"", ctp_rndm_seed[i]); s << os;
	  flag = true;
	}
	s << std::endl;
      }
      s << "CTP RNDM threshold                    = ";
      bool flag = false;
      for(u_int i=0; i < ALTI::CTP_RND_THRESHOLD_NUMBER; i++) {
	std::sprintf(os,"%s0x%08x",flag?", ":"", ctp_rndm_threshold[i]); s << os;
	flag = true;
      }
      s << std::endl;

      if(ctp_bgrp_file.empty()) {
	std::sprintf(os,"CTP BGRP                              = CONFIGURATION {0x%08x, 0x%08x .. 0x%08x, 0x%08x}\n", ctp_bgrp[0], ctp_bgrp[1], ctp_bgrp[AltiModule::BGRP_WORD_TOTAL-2], ctp_bgrp[AltiModule::BGRP_WORD_TOTAL-1]); s << os;
      }
      else {
	std::sprintf(os,"CTP BGRP file                         = %s\n", ctp_bgrp_file.c_str()); s << os;
      }
      std::sprintf(os,"CTP BGRP BCID offset                  = %d\n", ctp_bgrp_bcid_offset); s << os;
      if(ctp_bgrp_mask_file.empty()) {
	std::sprintf(os,"CTP BGRP mask                         = CONFIGURATION {0x%08x, 0x%08x .. 0x%08x, 0x%08x}\n", ctp_bgrp_mask[0], ctp_bgrp_mask[1], ctp_bgrp_mask[AltiModule::ITEM_NUMBER-2], ctp_bgrp_mask[AltiModule::ITEM_NUMBER-1]); s << os;
      }
      else {
	std::sprintf(os,"CTP BGRP mask file                    = %s\n", ctp_bgrp_mask_file.c_str()); s << os;
      }

      if(ctp_prsc_seed_valid) {
	if(ctp_prsc_seed_file.empty()) {
	  std::sprintf(os,"CTP PRSC seed                         = CONFIGURATION {0x%08x, 0x%08x .. 0x%08x, 0x%08x}\n", ctp_prsc_seed[0], ctp_prsc_seed[1], ctp_prsc_seed[AltiModule::ITEM_NUMBER-2], ctp_prsc_seed[AltiModule::ITEM_NUMBER-1]); s << os;
	}
	else {
	  std::sprintf(os,"CTP PRSC seed file                    = %s\n", ctp_prsc_seed_file.c_str()); s << os;
	}
      }
      if(ctp_prsc_threshold_file.empty()) {
	std::sprintf(os,"CTP PRSC threshold                    = CONFIGURATION {0x%08x, 0x%08x .. 0x%08x, 0x%08x}\n", ctp_prsc_threshold[0], ctp_prsc_threshold[1], ctp_prsc_threshold[AltiModule::ITEM_NUMBER-2], ctp_prsc_threshold[AltiModule::ITEM_NUMBER-1]); s << os;
      }
      else {
	std::sprintf(os,"CTP PRSC threshold file               = %s\n", ctp_prsc_threshold_file.c_str()); s << os;
      }
      
      std::sprintf(os,"CTP TAV enable                        = 0x%02x\n", ctp_tav_enable); s << os;
      

      for(u_int i = 0; i < AltiModule::LEAKY_BUCKET_NUMBER; i++) {
	std::sprintf(os,"CTP leaky bucket %d                    = %s\n", i, ctp_cplx_bucket[i].print().c_str()); s << os;
      }
      std::sprintf(os,"CTP sliding window                    = %s\n", ctp_cplx_sliding_window.print().c_str()); s << os;
      std::sprintf(os,"CTP simple deadtime                   = %d\n", ctp_smpl_deadtime); s << os;
    }
    
}

//------------------------------------------------------------------------------

int AltiConfiguration::read(const std::string &fn) {

    std::ostringstream buf;
    std::string key, key1, val;
    std::vector<std::string> arg;
    unsigned int i, j;
    int num;

    // open input file
    std::ifstream s(fn.c_str());
    if (!s) {
        CERR("cannot open input file \"%s\"", fn.c_str());
        return (AltiModule::FAILURE);
    }

    key = "CLK_CONFIG";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "CONFIG") clk_config = true;
    else if (val == "NOCONFIG") clk_config = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    key = "CLK_SOURCE";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == AltiModule::CLK_PLL_NAME[AltiModule::JITTER_CLEANER]) clk_select = AltiModule::JITTER_CLEANER;
    else if (val == AltiModule::CLK_PLL_NAME[AltiModule::FROM_SWITCH]) clk_select = AltiModule::FROM_SWITCH;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    key = "CLK_JC_SOURCE";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == AltiModule::CLK_JC_NAME[AltiModule::OSCILLATOR]) clk_jc_select = AltiModule::OSCILLATOR;
    else if (val == AltiModule::CLK_JC_NAME[AltiModule::SWITCH]) clk_jc_select = AltiModule::SWITCH;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    key = "CLK_JC_FILE"; 
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    clk_jc_file = val;
    key = "CLK_PHASE_SHIFT"; 
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (abs(std::stoi(val)) <= 1073741823) clk_phase_shift = std::stoi(val);
    else { 
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }

    key = "SIG_CONFIG";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "CONFIG") sig_config = true;
    else if (val == "NOCONFIG") sig_config = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    key = "SIG_BGO_FP";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != 2) {
        CERR("error in fixed format: for key \"%s\" expected 2 values, got %d value%s", key.c_str(), num, (num > 1) ? "s" : "");
        return (AltiModule::FAILURE);
    }
    for (i = 0; i < ((unsigned int) num); i++) arg[i] = AltiCommon::trimString(arg[i]);
    if (arg[0] == "BGO2") sig_fp_bgo2enable = true;
    else if (arg[0] == "BGO0") sig_fp_bgo2enable = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    if (arg[1] == "BGO3") sig_fp_bgo3enable = true;
    else if (arg[1] == "BGO1") sig_fp_bgo3enable = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    for (i = 0; i < AltiModule::SIGNAL_NUMBER_ROUTED; i++) {
        // needs to be changed, key must be ostringstream?
        switch (i) {
            case 0:
                key = "SIG_SWX_BC";
                break;
            case 1:
                key = "SIG_SWX_ORB";
                break;
            case 2:
                key = "SIG_SWX_L1A";
                break;
            case 3:
                key = "SIG_SWX_TTR1";
                break;
            case 4:
                key = "SIG_SWX_TTR2";
                break;
            case 5:
                key = "SIG_SWX_TTR3";
                break;
            case 6:
                key = "SIG_SWX_TTYP";
                break;
            case 7:
                //key = "SIG_SWX_TTYP1";
                //break;
            case 8:
                //key = "SIG_SWX_TTYP2";
                //break;
            case 9:
                //key = "SIG_SWX_TTYP3";
                //break;
            case 10:
                //key = "SIG_SWX_TTYP4";
                //break;
            case 11:
                //key = "SIG_SWX_TTYP5";
                //break;
            case 12:
                //key = "SIG_SWX_TTYP6";
                //break;
            case 13:
                //key = "SIG_SWX_TTYP7";
                //break;
                continue;
            case 14:
                key = "SIG_SWX_BGO0";
                break;
            case 15:
                key = "SIG_SWX_BGO1";
                break;
            case 16:
                key = "SIG_SWX_BGO2";
                break;
            case 17:
                key = "SIG_SWX_BGO3";
                break;
            default:
                break;
        }
        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
        if ((num = AltiCommon::splitString(val, ",", arg)) != 4) {
            CERR("error in fixed format: for key \"%s\" expected 4 values, got %d value%s", key.c_str(), num, (num > 1) ? "s" : "");
            return (AltiModule::FAILURE);
        }
        for (j = 0; j < 4; j++) {
            arg[j] = AltiCommon::trimString(arg[j]);
            if (key == "SIG_SWX_TTYP") {
                if (arg[j] == AltiModule::SIGNAL_SOURCE_NAME[AltiModule::CTP_IN]) sig_swx_ttyp[j] = sig_swx[i][j] = sig_swx[i + 1][j] =  sig_swx[i + 2][j] = sig_swx[i + 3][j] = sig_swx[i + 4][j] = sig_swx[i + 5][j] = sig_swx[i + 6][j] = sig_swx[i + 7][j] = AltiModule::CTP_IN;
                else if (arg[j] == AltiModule::SIGNAL_SOURCE_NAME[AltiModule::ALTI_IN]) sig_swx_ttyp[j] = sig_swx[i][j] = sig_swx[i + 1][j] =  sig_swx[i + 2][j] = sig_swx[i + 3][j] = sig_swx[i + 4][j] = sig_swx[i + 5][j] = sig_swx[i + 6][j] = sig_swx[i + 7][j] = AltiModule::ALTI_IN;
                else if (arg[j] == AltiModule::SIGNAL_SOURCE_NAME[AltiModule::NIM_IN]) sig_swx_ttyp[j] = sig_swx[i][j] = sig_swx[i + 1][j] =  sig_swx[i + 2][j] = sig_swx[i + 3][j] = sig_swx[i + 4][j] = sig_swx[i + 5][j] = sig_swx[i + 6][j] = sig_swx[i + 7][j] = AltiModule::NIM_IN;
                else if (arg[j] == AltiModule::SIGNAL_SOURCE_NAME[AltiModule::FROM_FPGA]) sig_swx_ttyp[j] = sig_swx[i][j] = sig_swx[i + 1][j] =  sig_swx[i + 2][j] = sig_swx[i + 3][j] = sig_swx[i + 4][j] = sig_swx[i + 5][j] = sig_swx[i + 6][j] = sig_swx[i + 7][j] = AltiModule::FROM_FPGA;
                else {
                    CERR("value \"%s\" for the key \"%s\":%s not allowed", arg[j].c_str(), key.c_str(), AltiModule::SIGNAL_DESTINATION_NAME[j].c_str());
                   return (AltiModule::FAILURE);
                }
            }
            else {
                if (arg[j] == AltiModule::SIGNAL_SOURCE_NAME[AltiModule::CTP_IN]) sig_swx[i][j] = AltiModule::CTP_IN;
                else if (arg[j] == AltiModule::SIGNAL_SOURCE_NAME[AltiModule::ALTI_IN]) sig_swx[i][j] = AltiModule::ALTI_IN;
                else if (arg[j] == AltiModule::SIGNAL_SOURCE_NAME[AltiModule::NIM_IN]) sig_swx[i][j] = AltiModule::NIM_IN;
                else if (arg[j] == AltiModule::SIGNAL_SOURCE_NAME[AltiModule::FROM_FPGA]) sig_swx[i][j] = AltiModule::FROM_FPGA;
                else {
                    CERR("value \"%s\" for the key \"%s\":%s not allowed", arg[j].c_str(), key.c_str(), AltiModule::SIGNAL_DESTINATION_NAME[j].c_str());
                   return (AltiModule::FAILURE);
                }
            }
        }
    }
    
    key = "SIG_EQZ_CTP_IN";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != 2) {
      CERR("error in fixed format: for key \"%s\" expected 2 values, got %d value%s", key.c_str(), num, (num > 1) ? "s" : "");
      return (AltiModule::FAILURE);
    }
    arg[0] =  AltiCommon::trimString(arg[0]);
    arg[1] =  AltiCommon::trimString(arg[1]);
    if (arg[0] == "ENABLED") sig_eqz_ctp_in_enable = true;
    else if (arg[0] == "DISABLED") sig_eqz_ctp_in_enable = false;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    if (arg[1] == AltiModule::EQUALIZER_CONFIG_NAME[AltiModule::SHORT_CABLE]) sig_eqz_ctp_in_mode = AltiModule::SHORT_CABLE;
    else if (arg[1] == AltiModule::EQUALIZER_CONFIG_NAME[AltiModule::LONG_CABLE]) sig_eqz_ctp_in_mode = AltiModule::LONG_CABLE;
    else if (arg[1] == AltiModule::EQUALIZER_CONFIG_NAME[AltiModule::CUSTOM_CONFIG]) sig_eqz_ctp_in_mode = AltiModule::CUSTOM_CONFIG;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }

    key = "SIG_EQZ_ALTI_IN";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != 2) {
      CERR("error in fixed format: for key \"%s\" expected 2 values, got %d value%s", key.c_str(), num, (num > 1) ? "s" : "");
      return (AltiModule::FAILURE);
    }
    arg[0] =  AltiCommon::trimString(arg[0]);
    arg[1] =  AltiCommon::trimString(arg[1]);
    if (arg[0] == "ENABLED") sig_eqz_alti_in_enable = true;
    else if (arg[0] == "DISABLED") sig_eqz_alti_in_enable = false;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    if (arg[1] == AltiModule::EQUALIZER_CONFIG_NAME[AltiModule::SHORT_CABLE]) sig_eqz_alti_in_mode = AltiModule::SHORT_CABLE;
    else if (arg[1] == AltiModule::EQUALIZER_CONFIG_NAME[AltiModule::LONG_CABLE]) sig_eqz_alti_in_mode = AltiModule::LONG_CABLE;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    
    AltiModule::ASYNC_INPUT_SIGNAL asyncIn;
    unsigned int num_signals = 0;
    for (i = 0; i < AltiModule::ASYNC_INPUT_SIGNAL_NUMBER; i++) {
        asyncIn = (AltiModule::ASYNC_INPUT_SIGNAL) i;
        if (asyncIn > AltiModule::SWX_TTR3) continue;
        // merging
        if ((asyncIn >= AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO1) && (asyncIn <= AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO3)) continue;
        if ((asyncIn >= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP1) && (asyncIn <= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP3)) continue;
        if ((asyncIn >= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP5) && (asyncIn <= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP7)) continue;
        if ((asyncIn >= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR2) && (asyncIn <= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR3)) continue;
        
        if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO0) {
            key = "SIG_IO_SYNC_SWX_BGO";
            key1 = "SIG_IO_SHAPE_SWX_BGO";
            num_signals = 4;
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP0) {
            key = "SIG_IO_SYNC_SWX_TTYP0..3";
            key1 = "SIG_IO_SHAPE_SWX_TTYP0..3";
            num_signals = 4;
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP4) {
            key = "SIG_IO_SYNC_SWX_TTYP4..7";
            key1 = "SIG_IO_SHAPE_SWX_TTYP4..7";
            num_signals = 4;
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR1) {
            key = "SIG_IO_SYNC_SWX_TTR";
            key1 = "SIG_IO_SHAPE_SWX_TTR";
            num_signals = 3;
        }
	else {
	  key = std::string("SIG_IO_SYNC_") + AltiModule::ASYNC_INPUT_SIGNAL_NAME[asyncIn].c_str();
	  key1 = std::string("SIG_IO_SHAPE_") + AltiModule::ASYNC_INPUT_SIGNAL_NAME[asyncIn].c_str();
	  num_signals = 1;
        }

        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
	if ((num = AltiCommon::splitString(val, ",", arg)) != ((int) num_signals)) {
	  CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key1.c_str(), num_signals, num, (num > 1) ? "s" : "");
	  return (AltiModule::FAILURE);
        }
	
        for (j = 0; j < num_signals; j++) {
            arg[j] = AltiCommon::trimString(arg[j]);           
	    sig_io_sync_phase[i + j] = std::stoi(arg[j]);
        }

        if (readNextRecord(s, key1, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key1.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
        if ((num = AltiCommon::splitString(val, ",", arg)) != ((int) num_signals)) {
            CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key1.c_str(), num_signals, num, (num > 1) ? "s" : "");
            return (AltiModule::FAILURE);
        }
        for (j = 0; j < num_signals; j++) {
            arg[j] = AltiCommon::trimString(arg[j]);
	    if(arg[j] == AltiModule::SYNC_SHAPE_NAME[AltiModule::SYNC_SHAPE::IN_SYNCHRONIZED])   sig_io_shaping[i + j] = AltiModule::SYNC_SHAPE::IN_SYNCHRONIZED;
	    else if(arg[j] == AltiModule::SYNC_SHAPE_NAME[AltiModule::SYNC_SHAPE::IN_SHAPED_ONE]) sig_io_shaping[i + j] = AltiModule::SYNC_SHAPE::IN_SHAPED_ONE;
	    else if(arg[j] == AltiModule::SYNC_SHAPE_NAME[AltiModule::SYNC_SHAPE::IN_SHAPED_TWO]) sig_io_shaping[i + j] = AltiModule::SYNC_SHAPE::IN_SHAPED_TWO;
        }
    }
    
    std::set<std::string> sources {"INACTIVE", "EXTERNAL", "PATTERN", "MINICTP"};
    std::set<std::string> sources_l1a {"INACTIVE", "EXTERNAL", "PATTERN", "MINICTP", "CALIBSIGNAL"};
    std::set<std::string> sources_ttr {"INACTIVE", "EXTERNAL", "PATTERN", "CALIBREQUEST", "TURNSIGNAL", "ORBIT", "MINICTP"};
    std::set<std::string> sources_orb {"EXTERNAL", "PATTERN", "MINICTP"};
    std::set<std::string> sources_ctp {"EXTERNAL", "PATTERN"};

    key = "SIG_SOURCE_ORB";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if(sources.find(val) != sources.end()) sig_orb_source = val;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    key = "SIG_SOURCE_L1A";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if(sources_l1a.find(val) != sources_l1a.end()) sig_l1a_source = val;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
   key = "SIG_SOURCE_TTR";
   if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
     CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
     return (AltiModule::FAILURE);
   }
   if ((num = AltiCommon::splitString(val, ",", arg)) != 3) {
     CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), 3, num, (num > 1) ? "s" : "");
     return (AltiModule::FAILURE);
   }
   for (i = 0; i < 3; i++) {
     arg[i] = AltiCommon::trimString(arg[i]);
     if(sources_ttr.find(arg[i]) != sources_ttr.end()) sig_ttr_source[i] = arg[i];
     else {
       CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
       return (AltiModule::FAILURE);
     } 
   }
   key = "SIG_SOURCE_BGO";
   if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
     CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
   }
   if ((num = AltiCommon::splitString(val, ",", arg)) != AltiModule::TTC_FIFO_NUMBER) {
     CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), AltiModule::TTC_FIFO_NUMBER, num, (num > 1) ? "s" : "");
     return (AltiModule::FAILURE);
   }
   for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
     arg[i] = AltiCommon::trimString(arg[i]);
     if(sources.find(arg[i]) != sources.end()) sig_bgo_source[i] = arg[i];
      else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
      } 
   }
   key = "SIG_SOURCE_TTYP";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if(sources.find(val) != sources.end()) sig_ttyp_source = val;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }

    key = "SIG_SOURCE_INPUT_ORB";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if(sources_orb.find(val) != sources_orb.end()) sig_orb_input_source = val;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }

    key = "SIG_TURN_COUNTER";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != 2) {
      CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), 2, num, (num > 1) ? "s" : "");
      return (AltiModule::FAILURE);
    }
    turn_cnt_mask =  std::stoi(arg[0]);
    turn_cnt_max =  std::stoi(arg[1]);     
     
     key = "SIG_BGO2_L1A_DELAY";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    bgo2_l1a_delay =  std::stoi(val);

    key = "BSY_CONFIG";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "CONFIG") bsy_config = true;
    else if (val == "NOCONFIG") bsy_config = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    for (i = 0; i < AltiModule::BUSY_INPUT_NUMBER; i++) {
        switch (i) {
            case 0:
                key = "BSY_FROM_FP";
                break;
            case 1:
                key = "BSY_FROM_CTP";
                break;
            case 2:
                key = "BSY_FROM_ALTI";
                break;
            case 3:
                key = "BSY_FROM_PG";
                break;
            case 4:
                key = "BSY_FROM_VME";
                break;
            case 5:
		key = "BSY_FROM_ECR";
		break;
            default:
                break;
        }
        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
        if (val == "ENABLED") bsy_in_select[i] = true;
        else if (val == "DISABLED") bsy_in_select[i] = false;
        else {
            CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
            return (AltiModule::FAILURE);
        }
    }
    for (i = 0; i < AltiModule::BUSY_OUTPUT_NUMBER; i++) {
        switch (i) {
            case 0:
                key = "BSY_TO_CTP";
                break;
            case 1:
                key = "BSY_TO_ALTI";
                break;
            default:
                break;
        }
        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
        if (val == "INACTIVE") bsy_out_select[i] = AltiModule::BUSY_INACTIVE;
        else if (val == "LOCAL") bsy_out_select[i] = AltiModule::BUSY_LOCAL;
        else if (val == "CTP") bsy_out_select[i] = AltiModule::BUSY_CTP;
        else if (val == "ALTI") bsy_out_select[i] = AltiModule::BUSY_ALTI;
        else {
            CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
            return (AltiModule::FAILURE);
        }
    }
    key = "BSY_DATA_VME";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "0") bsy_data_vme = false;
    else if (val == "1") bsy_data_vme = true;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }

    AltiModule::SIGNAL_PG signal_bsy;
    for (i = 0; i < AltiModule::SIGNAL_PG_NUMBER; i++) {
        signal_bsy = (AltiModule::SIGNAL_PG) i;
	// merging of TTR and BGO
        if ((signal_bsy >= AltiModule::PG_TTR2) && (signal_bsy <= AltiModule::PG_TTR3)) continue;
        if ((signal_bsy >= AltiModule::PG_BGO1) && (signal_bsy <= AltiModule::PG_BGO3)) continue;

        if (signal_bsy == AltiModule::PG_TTR1) {
	  key = "BSY_GATING_TTR";
	  num_signals = 3;
        }
        else if (signal_bsy == AltiModule::PG_BGO0) {
            key = "BSY_GATING_BGO";
            num_signals = 4;
        }
        else if (signal_bsy == AltiModule::PG_L1A) {
	  key = "BSY_GATING_L1A";
	  num_signals = 1;
        }
	else continue;

        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
	  CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
	  return (AltiModule::FAILURE);
        }
        if ((num = AltiCommon::splitString(val, ",", arg)) != ((int) num_signals)) {
	  CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), num_signals, num, (num > 1) ? "s" : "");
	  return (AltiModule::FAILURE);
        }
	if(num_signals == 1) {
	  if (val == "ENABLED") bsy_l1a_masked = true;
	  else if (val == "DISABLED") bsy_l1a_masked = false;
	  else {
	    CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
	    return (AltiModule::FAILURE);
	  }
	} 
	else if(num_signals == 3) {
	  for (j = 0; j < num_signals; j++) {
	    arg[j] = AltiCommon::trimString(arg[j]);
	    if (arg[j] == "ENABLED") bsy_ttr_masked[j] = true;
	    else if (arg[j] == "DISABLED") bsy_ttr_masked[j] = false;
	    else {
	      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
	      return (AltiModule::FAILURE);
	    }
	  }
	}
	else if(num_signals == 4) {
	  for (j = 0; j < num_signals; j++) {
	    arg[j] = AltiCommon::trimString(arg[j]);
	    if (arg[j] == "ENABLED") bsy_bgo_masked[j] = true;
	    else if (arg[j] == "DISABLED") bsy_bgo_masked[j] = false;
	    else {
	      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
	      return (AltiModule::FAILURE);
	    }
	  }
	}    
    }
    key = "BSY_LEVEL";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if (val == "NIM") bsy_level = AltiModule::NIM;
    else if (val == "TTL") bsy_level = AltiModule::TTL;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }

    key = "CRQ_CONFIG";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "CONFIG") crq_config = true;
    else if (val == "NOCONFIG") crq_config = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    key = "CRQ_SOURCE";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != 3) {
        CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), 3, num, (num > 1) ? "s" : "");
        return (AltiModule::FAILURE);
    }
    for (i = 0; i < 3; i++) {
        arg[i] = AltiCommon::trimString(arg[i]);
        if (arg[i] == AltiModule::CALREQ_INPUT_NAME[AltiModule::CALREQ_FROM_RJ45]) crq_in_select[2 - i] = AltiModule::CALREQ_FROM_RJ45;
        else if (arg[i] == AltiModule::CALREQ_INPUT_NAME[AltiModule::CALREQ_FROM_FRONT_PANEL]) crq_in_select[2 - i] = AltiModule::CALREQ_FROM_FRONT_PANEL;
        else if (arg[i] == AltiModule::CALREQ_INPUT_NAME[AltiModule::CALREQ_FROM_CTP]) crq_in_select[2 - i] = AltiModule::CALREQ_FROM_CTP;
        else if (arg[i] == AltiModule::CALREQ_INPUT_NAME[AltiModule::CALREQ_FROM_ALTI]) crq_in_select[2 - i] = AltiModule::CALREQ_FROM_ALTI;
        else if (arg[i] == AltiModule::CALREQ_INPUT_NAME[AltiModule::CALREQ_FROM_PG]) crq_in_select[2 - i] = AltiModule::CALREQ_FROM_PG;
        else if (arg[i] == AltiModule::CALREQ_INPUT_NAME[AltiModule::CALREQ_FROM_VME]) crq_in_select[2 - i] = AltiModule::CALREQ_FROM_VME;
        else {
            CERR("value \"%s\" for the key \"%s\" not allowed", arg[i].c_str(), key.c_str());
            return (AltiModule::FAILURE);
        }
    }
    for (i = 0; i < AltiModule::CALREQ_OUTPUT_NUMBER; i++) {
        switch (i) {
            case 0:
                key = "CRQ_TO_CTP";
                break;
            case 1:
                key = "CRQ_TO_ALTI";
                break;
            default:
                break;
        }
        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
        if ((num = AltiCommon::splitString(val, ",", arg)) != 3) {
            CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), 3, num, (num > 1) ? "s" : "");
            return (AltiModule::FAILURE);
        }
        for (j = 0; j < 3; j++) {
            arg[j] = AltiCommon::trimString(arg[j]);
            if (arg[j] == AltiModule::CALREQ_SOURCE_NAME[AltiModule::CALREQ_INACTIVE]) crq_out_select[i][2 - j] = AltiModule::CALREQ_INACTIVE;
            else if (arg[j] == AltiModule::CALREQ_SOURCE_NAME[AltiModule::CALREQ_LOCAL]) crq_out_select[i][2 - j] = AltiModule::CALREQ_LOCAL;
            else if (arg[j] == AltiModule::CALREQ_SOURCE_NAME[AltiModule::CALREQ_CTP]) crq_out_select[i][2 - j] = AltiModule::CALREQ_CTP;
            else if (arg[j] == AltiModule::CALREQ_SOURCE_NAME[AltiModule::CALREQ_ALTI]) crq_out_select[i][2 - j] = AltiModule::CALREQ_ALTI;
            else {
                CERR("value \"%s\" for the key \"%s\" not allowed", arg[j].c_str(), key.c_str());
                return (AltiModule::FAILURE);
            }
        }
    }
    key = "CRQ_DATA_VME";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER) {
      CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER, num, (num > 1) ? "s" : "");
      return (AltiModule::FAILURE);
    }
    for (i = 0; i < ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER; i++) {
        arg[i] = AltiCommon::trimString(arg[i]);
	crq_data_vme[i] = (arg[i] == "ENABLED") ? true : false;
    }
    
    key = "MEM_CONFIG";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "CONFIG") mem_config = true;
    else if (val == "NOCONFIG") mem_config = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
   
    key = "MEM_PG_START";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    mem_pg_start_addr = std::stoi(val, nullptr, 0);
    key = "MEM_PG_STOP";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    mem_pg_stop_addr = std::stoi(val, nullptr, 0);
    key = "MEM_PG_FILE";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    mem_pg_file = val;
    key = "MEM_PG_ENABLE";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "ENABLED") mem_pg_enable = true;
    else if (val == "DISABLED") mem_pg_enable = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
   
    key = "MEM_PG_REPEAT";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "REPEATED") mem_pg_repeat = true;
    else if (val == "ONESHOT") mem_pg_repeat = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    AltiModule::SIGNAL signal;
    for (i = 1; i < AltiModule::SIGNAL_NUMBER; i++) {
        signal = (AltiModule::SIGNAL) i;
        // merging
        if ((signal >= AltiModule::TTR2) && (signal <= AltiModule::TTR3)) continue;
        if ((signal >= AltiModule::TTYP1) && (signal <= AltiModule::TTYP3)) continue;
        if ((signal >= AltiModule::TTYP5) && (signal <= AltiModule::TTYP7)) continue;
        if ((signal >= AltiModule::BGO1) && (signal <= AltiModule::BGO3)) continue;
        if ((signal >= AltiModule::CALREQ1) && (signal <= AltiModule::CALREQ2)) continue;

        if (signal == AltiModule::TTR1) {
            key = "MEM_SNAP_MASK_TTR";
            num_signals = 3;
        }
        else if (signal == AltiModule::TTYP0) {
            key = "MEM_SNAP_MASK_TTYP0..3";
            num_signals = 4;
        }
        else if (signal == AltiModule::TTYP4) {
            key = "MEM_SNAP_MASK_TTYP4..7";
            num_signals = 4;
        }
        else if (signal == AltiModule::BGO0) {
            key = "MEM_SNAP_MASK_BGO";
            num_signals = 4;
        }
        else if (signal == AltiModule::CALREQ0) {
            key = "MEM_SNAP_MASK_CALREQ";
            num_signals = 3;
        }
        else {
            key = "MEM_SNAP_MASK_" + AltiModule::SIGNAL_NAME[signal];
            num_signals = 1;
        }

        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
        if ((num = AltiCommon::splitString(val, ",", arg)) != ((int) num_signals)) {
            CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), num_signals, num, (num > 1) ? "s" : "");
            return (AltiModule::FAILURE);
        }
        for (j = 0; j < num_signals; j++) {
            arg[j] = AltiCommon::trimString(arg[j]);
            if (arg[j] == "MASKED") mem_snap_masked[i + j] = false;
            else if (arg[j] == "NOT_MASKED") mem_snap_masked[i + j] = true;
            else {
                CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
                return (AltiModule::FAILURE);
            }
        }
    }

    key = "TTC_CONFIG";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "CONFIG") ttc_config = true;
    else if (val == "NOCONFIG") ttc_config = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    key = "TTC_TX_ENABLE";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != AltiModule::TRANSMITTER_NUMBER) {
        CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), AltiModule::TRANSMITTER_NUMBER, num, (num > 1) ? "s" : "");
        return (AltiModule::FAILURE);
    }
    for (i = 0; i < ((unsigned int) num); i++) {
        arg[i] = AltiCommon::trimString(arg[i]);
        if (arg[i] == "ENABLED") ttc_tx_enable[i] = true;
        else if (arg[i] == "DISABLED") ttc_tx_enable[i] = false;
        else {
            CERR("value \"%s\" for the key \"%s\" not allowed", arg[i].c_str(), key.c_str());
            return (AltiModule::FAILURE);
        }
    }
    key = "TTC_TX_DELAY";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != AltiModule::TRANSMITTER_NUMBER) {
      CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), AltiModule::TRANSMITTER_NUMBER, num, (num > 1) ? "s" : "");
      return (AltiModule::FAILURE);
    }
    for (i = 0; i < ((unsigned int) num); i++) {
      arg[i] = AltiCommon::trimString(arg[i]);
      int delay = std::stoi(arg[i], nullptr, 0);
      if (delay < 0x00 || delay > 0x1f) {
	CERR("transmitter delay must be in range 0x00 to 0x1f (5 bits)", "");
	return (AltiModule::FAILURE);
      }
        ttc_tx_delay[i] = delay;
    }
    key = "TTC_L1A_SOURCE_FROM_L1A"; 
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if (val == "ENABLED") ttc_l1a_source[0] = true;
    else if (val == "DISABLED") ttc_l1a_source[0] = false;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    key = "TTC_L1A_SOURCE_FROM_TTR1..3";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != 3) {
      CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), 3, num, (num > 1) ? "s" : "");
      return (AltiModule::FAILURE);
    }
    for (i = 0; i < ((unsigned int) num); i++) {
      arg[i] = AltiCommon::trimString(arg[i]);
      if (arg[i] == "ENABLED") ttc_l1a_source[i+1] = true;
      else if (arg[i] == "DISABLED") ttc_l1a_source[i+1] = false;
      else {
	CERR("value \"%s\" for the key \"%s\" not allowed", arg[i].c_str(), key.c_str());
	return (AltiModule::FAILURE);
      }
    }
    key = "TTC_L1A_SOURCE_FROM_CALREQ"; 
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if (val == "ENABLED") ttc_l1a_source[4] = true;
    else if (val == "DISABLED") ttc_l1a_source[4] = false;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    key = "TTC_L1A_SOURCE_FROM_PATTERN"; 
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if (val == "ENABLED") ttc_l1a_source[5] = true;
    else if (val == "DISABLED") ttc_l1a_source[5] = false;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    key = "TTC_L1A_SOURCE_FROM_MINICTP"; 
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if (val == "ENABLED") ttc_l1a_source[6] = true;
    else if (val == "DISABLED") ttc_l1a_source[6] = false;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    
    key = "TTC_ORB_SOURCE"; 
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    auto found = std::find(std::begin(ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::ORBIT_TYPE_NAME), std::end(ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::ORBIT_TYPE_NAME), val);
    if(found !=  std::end(ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::ORBIT_TYPE_NAME)) ttc_orb_source = val;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    
    key = "TTC_BGO_SOURCE";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != AltiModule::TTC_FIFO_NUMBER) {
      CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), AltiModule::TTC_FIFO_NUMBER, num, (num > 1) ? "s" : "");
      return (AltiModule::FAILURE);
    }
    for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
      arg[i] = AltiCommon::trimString(arg[i]);
      if(sources.find(arg[i]) != sources.end()) ttc_bgo_source[i] = arg[i];
      else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
      } 
    }
    
    key = "TTC_TTYP_SOURCE"; 
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    found = std::find(std::begin(ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::TRIGGERTYPE_TYPE_NAME), std::end(ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::TRIGGERTYPE_TYPE_NAME), val);
    if(found !=  std::end(ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::TRIGGERTYPE_TYPE_NAME)) ttc_ttyp_source = val;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }

    for (i = 0; i <= 3; i++) {
        switch (i) {
            case 0:
                key = "TTC_BGO0_INHIBIT";
                break;
            case 1:
                key = "TTC_BGO1_INHIBIT";
                break;
            case 2:
                key = "TTC_BGO2_INHIBIT";
                break;
            case 3:
                key = "TTC_BGO3_INHIBIT";
                break;
            default:
                break;
        }
        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
        if ((num = AltiCommon::splitString(val, ",", arg)) != 2) {
            CERR("error in fixed format: for key \"%s\" expected 2 values, got %d value%s", key.c_str(), num, (num > 1) ? "s" : "");
            return (AltiModule::FAILURE);
        }
        for (j = 0; j < ((unsigned int) num); j++) arg[j] = AltiCommon::trimString(arg[j]);

        int width = std::stoi(arg[0]);
        //std::printf("width: %d\n", width);
        if ((width < 0) || (width > ((int) ALTI_ENC_BGOCONTROL_BITSTRING::MASK_INHIBITWIDTH.number()[0]))) {
            CERR("Inhibit width must be in range 0 to %d (12 bits)", ALTI_ENC_BGOCONTROL_BITSTRING::MASK_INHIBITWIDTH.number()[0]);
            return (AltiModule::FAILURE);
        }
        ttc_bgo_inhibit_width[i] = width;

        int delay = std::stoi(arg[1]);
        //std::printf("delay: %d\n", delay);
        if ((delay < 0) || (delay > ((int) ALTI_ENC_BGOCONTROL_BITSTRING::MASK_INHIBITDELAY.number()[0]))) {
            CERR("Inhibit delay must be in range 0 to %d (12 bits)", ALTI_ENC_BGOCONTROL_BITSTRING::MASK_INHIBITDELAY.number()[0]);
            return (AltiModule::FAILURE);
        }
        ttc_bgo_inhibit_delay[i] = delay;
    }
  
    key = "TTC_TTYP_DELAY";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    int ttyp_delay = std::stoi(val);
    //std::printf("ttyp_delay: %d\n", ttyp_delay);
    /*if ((ttyp_delay < 0) || (ttyp_delay > ((int) ALTI_ENC_TRIGGERTYPEWORDPARAMS_BITSTRING::MASK_DELAYINBCS.number()[0]))) {
        CERR("Trigger type word delay must be in range 0 to %d (12 bits)", ALTI_ENC_TRIGGERTYPEWORDPARAMS_BITSTRING::MASK_DELAYINBCS.number()[0]);
        return (AltiModule::FAILURE);
    }*/
    ttc_ttyp_delay = ttyp_delay;
    key = "TTC_TTYP_ADDR";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    int ttyp_addr = std::stoi(val, nullptr, 0);
    //std::printf("ttyp_addr: 0x%04x\n", ttyp_addr);
    if (ttyp_addr < 0x0000 || ttyp_addr > 0x3fff) {
        CERR("Trigger type address must be in range 0x0000 to 0x3fff (14 bits)", "");
        return (AltiModule::FAILURE);
    }
    ttc_ttyp_addr = ttyp_addr;
    key = "TTC_TTYP_SUBADDR";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    int ttyp_subaddr = std::stoi(val, nullptr, 0);
    //std::printf("ttyp_subaddr: 0x%02x\n", ttyp_subaddr);
    if (ttyp_subaddr < 0x00 || ttyp_subaddr > 0xff) {
        CERR("Trigger type subadress must be in range 0x00 to 0xff (8 bits)", "");
        return (AltiModule::FAILURE);
    }
    ttc_ttyp_subaddr = ttyp_subaddr;
    key = "TTC_TTYP_SPACE";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == AltiModule::TTC_ADDRESS_SPACE_NAME[AltiModule::TTC_ADDRESS_SPACE::INTERNAL].c_str()) ttc_ttyp_address_space = AltiModule::TTC_ADDRESS_SPACE::INTERNAL;
    else if (val == AltiModule::TTC_ADDRESS_SPACE_NAME[AltiModule::TTC_ADDRESS_SPACE::EXTERNAL].c_str()) ttc_ttyp_address_space = AltiModule::TTC_ADDRESS_SPACE::EXTERNAL;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    for (i = 0; i <= 3; i++) {
        switch (i) {
            case 0:
                key = "TTC_BGO0_MODE";
                break;
            case 1:
                key = "TTC_BGO1_MODE";
                break;
            case 2:
                key = "TTC_BGO2_MODE";
                break;
            case 3:
                key = "TTC_BGO3_MODE";
                break;
            default:
                break;
        }
        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
        if (val == AltiModule::TTC_BGO_MODE_NAME[AltiModule::TTC_BGO_MODE::SYNCHRONOUS_SINGLE_BGO_SIGNAL]) ttc_bgo_mode[i] = AltiModule::TTC_BGO_MODE::SYNCHRONOUS_SINGLE_BGO_SIGNAL;
        else if (val == AltiModule::TTC_BGO_MODE_NAME[AltiModule::TTC_BGO_MODE::SYNCHRONOUS_REPETITIVE]) ttc_bgo_mode[i] = AltiModule::TTC_BGO_MODE::SYNCHRONOUS_REPETITIVE;
        else if (val == AltiModule::TTC_BGO_MODE_NAME[AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_BGO_SIGNAL]) ttc_bgo_mode[i] = AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_BGO_SIGNAL;
        else if (val == AltiModule::TTC_BGO_MODE_NAME[AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_VME_ON_TRIGGER]) ttc_bgo_mode[i] = AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_VME_ON_TRIGGER;
        else if (val == AltiModule::TTC_BGO_MODE_NAME[AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_FIFO_MODE]) ttc_bgo_mode[i] = AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_FIFO_MODE;
    }
    for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
        key = "TTC_" + AltiModule::TTC_FIFO_NAME[(AltiModule::TTC_FIFO) i] + "_RETRANS";
        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
        if (val == "ENABLED") ttc_fifo_retransmit[i] = true;
        else if (val == "DISABLED") ttc_fifo_retransmit[i] = false;
        else {
            CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
            return (AltiModule::FAILURE);
        }
    }
    for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
        key = "TTC_" + AltiModule::TTC_FIFO_NAME[(AltiModule::TTC_FIFO) i] + "_WORD";
        if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
            CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
            return (AltiModule::FAILURE);
        }
	int word = std::stoi(val, nullptr, 0);
	if (word < 0x00 || word > 0xff) {
        CERR("BGo FIFO word must be in range 0x00 to 0xff (8 bits)", "");
        return (AltiModule::FAILURE);
	}
        ttc_fifo_word[i] = word;
    }

    key = "CNT_CONFIG";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "CONFIG") cnt_config = true;
    else if (val == "NOCONFIG") cnt_config = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }

    key = "CNT_COUNTERS";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if (val == "ENABLED") cnt_enable = true;
    else if (val == "DISABLED") cnt_enable = false;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
 
    key = "CNT_TTYP_LUT_FILE";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    cnt_ttyp_lut_file = val;

    key = "PBM_CONFIG";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if (val == "CONFIG") ctp_config = true;
    else if (val == "NOCONFIG") ctp_config = false;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    key = "PBM_BCID_OFFSET";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    pbm_bcid_offset = std::stoi(val);

 key = "CTP_CONFIG";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return (AltiModule::FAILURE);
    }
    if (val == "CONFIG") ctp_config = true;
    else if (val == "NOCONFIG") ctp_config = false;
    else {
        CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
        return (AltiModule::FAILURE);
    }
    key = "CTP_ECR_GENERATION";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    } 
    if (ctp_ecr_generation.read(val) != AltiModule::SUCCESS) {
      CERR("error in fixed format: for key \"%s\", cannot read ECR generation (\"%s\")", key.c_str(), val.c_str());
      return (AltiModule::FAILURE);
    }
    key = "CTP_SOURCE_L1A";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if(sources_ctp.find(val) != sources_ctp.end()) ctp_l1a_source = val;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    key = "CTP_SOURCE_BGO2";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if(sources_ctp.find(val) != sources_ctp.end()) ctp_bgo2_source = val;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    key = "CTP_SOURCE_BGO3";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if(sources_ctp.find(val) != sources_ctp.end()) ctp_bgo3_source = val;
    else {
      CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
      return (AltiModule::FAILURE);
    }
    key = "CTP_SOURCE_TTR";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != 3) {
      CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), 3, num, (num > 1) ? "s" : "");
      return (AltiModule::FAILURE);
    }
    for (i = 0; i < 3; i++) {
      arg[i] = AltiCommon::trimString(arg[i]);
      if(sources_ctp.find(arg[i]) != sources_ctp.end()) ctp_ttr_source[i] = arg[i];
      else {
	CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
	return (AltiModule::FAILURE);
      } 
    }
    key = "CTP_SOURCE_CALREQ";
    if (readNextRecord(s, key, val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return (AltiModule::FAILURE);
    }
    if ((num = AltiCommon::splitString(val, ",", arg)) != 3) {
      CERR("error in fixed format: for key \"%s\" expected %d values, got %d value%s", key.c_str(), 3, num, (num > 1) ? "s" : "");
      return (AltiModule::FAILURE);
    }
    for (i = 0; i < 3; i++) {
      arg[i] = AltiCommon::trimString(arg[i]);
      if(sources_ctp.find(arg[i]) != sources_ctp.end()) ctp_crq_source[i] = arg[i];
      else {
	CERR("value \"%s\" for the key \"%s\" not allowed", val.c_str(), key.c_str());
	return (AltiModule::FAILURE);
      } 
    }

    key = "CTP_ITEM_LUT_FILE";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"",key.c_str(),fn.c_str());
      return(AltiModule::FAILURE);
    }
    ctp_item_lut_file = val;
    
    key = "CTP_TTYP_LUT_FILE";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"",key.c_str(),fn.c_str());
      return(AltiModule::FAILURE);
    }
    ctp_ttyp_lut_file = val;
    
    key = "CTP_RNDM_SEED";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"",key.c_str(),fn.c_str());
      return(AltiModule::FAILURE);
    }
    if(!val.empty()) {
      if((num = AltiCommon::splitString(val,",",arg)) != (int) ALTI::CTP_RND_THRESHOLD_NUMBER) {
	CERR("error in fixed format: for key \"%s\" expected %d value%s, got %d value%s", key.c_str(), ALTI::CTP_RND_THRESHOLD_NUMBER, ALTI::CTP_RND_THRESHOLD_NUMBER > 1 ? "s" : "", num, num > 1 ? "s" : "");
            return(AltiModule::FAILURE);
      }
      for(u_int i = 0; i < ALTI::CTP_RND_THRESHOLD_NUMBER; i++) {
	if(AltiCommon::readNumber(arg[i], ctp_rndm_seed[i]) != AltiModule::SUCCESS) {
	  CERR("error in fixed format: for key \"%s\", cannot read number value %d (\"%s\")", key.c_str(), i, arg[i].c_str());
	  return(AltiModule::FAILURE);
	}
      }
      ctp_rndm_seed_valid = true;
    }
    else {
      // invalidate random seed
      ctp_rndm_seed_valid = false;
    }
    key = "CTP_RNDM_THRESHOLD";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return(AltiModule::FAILURE);
    }
    if(!val.empty()) {
      if((num = AltiCommon::splitString(val,",",arg)) != (int) ALTI::CTP_RND_THRESHOLD_NUMBER) {
	CERR("error in fixed format: for key \"%s\" expected %d value%s, got %d value%s",key.c_str(), ALTI::CTP_RND_THRESHOLD_NUMBER, ALTI::CTP_RND_THRESHOLD_NUMBER > 1 ? "s" : "", num, num > 1 ? "s" : "");
	return(AltiModule::FAILURE);
      }
      for(u_int i = 0; i < ALTI::CTP_RND_THRESHOLD_NUMBER; i++) {
	if(AltiCommon::readNumber(arg[i], ctp_rndm_threshold[i]) != AltiModule::SUCCESS) {
	  CERR("error in fixed format: for key \"%s\", cannot read number value %d (\"%s\")", key.c_str(), i, arg[i].c_str());
	  return(AltiModule::FAILURE);
	}
      }
    }   
    
    key = "CTP_BGRP_FILE";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"",key.c_str(),fn.c_str());
      return(AltiModule::FAILURE);
    }
    ctp_bgrp_file = val;
    key = "CTP_BGRP_BCID";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return(AltiModule::FAILURE);
    }
    if(AltiCommon::readNumber(val, ctp_bgrp_bcid_offset) != AltiModule::SUCCESS) {
        CERR("error in fixed format: for key \"%s\", cannot read number value (\"%s\")", key.c_str(), val.c_str());
        return(AltiModule::FAILURE);
    }
    key = "CTP_BGRP_MASK";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return(AltiModule::FAILURE);
    }
    ctp_bgrp_mask_file = val;
    
    key = "CTP_PRSC_SEED";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
        CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
        return(AltiModule::FAILURE);
    }
    if(!val.empty()) {
      ctp_prsc_seed_file = val;
      ctp_prsc_seed_valid = true;
    }
    else {
      // invalidate prescaler seed
      ctp_prsc_seed_valid = false;
    }
    key = "CTP_PRSC_THRESHOLD";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return(AltiModule::FAILURE);
    }
    ctp_prsc_threshold_file = val;
    
    key = "CTP_TAV_ENABLE";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return(AltiModule::FAILURE);
    }
    if(AltiCommon::readNumber(val,ctp_tav_enable) != AltiModule::SUCCESS) {
      CERR("error in fixed format: for key \"%s\", cannot read number value (\"%s\")", key.c_str(), val.c_str());
      return(AltiModule::FAILURE);
    }

    
    key = "CTP_SIMPLE";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"",key.c_str(),fn.c_str());
      return(AltiModule::FAILURE);
    }
    if(AltiCommon::readNumber(val,ctp_smpl_deadtime) != AltiModule::SUCCESS) {
      CERR("error in fixed format: for key \"%s\", cannot read number value (\"%s\")", key.c_str(), val.c_str());
      return(AltiModule::FAILURE);
    }

    for(u_int i = 0 ; i < AltiModule::LEAKY_BUCKET_NUMBER; i++) {
      buf.str(""); buf << "CTP_BUCKET" << i; key = buf.str();
      if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
	CERR("error reading key \"%s\" from file \"%s\"",key.c_str(),fn.c_str());
	return(AltiModule::FAILURE);
      }
      if((num = AltiCommon::splitString(val,",",arg)) != (int)BucketConfiguration::VALUE_NUMBER) {
	CERR("error in fixed format: for key \"%s\" expected %d value%s, got %d value%s", key.c_str(), BucketConfiguration::VALUE_NUMBER, BucketConfiguration::VALUE_NUMBER > 1 ? "s" : "", num, num > 1 ? "s" : "");
	return(AltiModule::FAILURE);
      }
      if(AltiCommon::readNumber(arg[0], ctp_cplx_bucket[i].rate) != AltiModule::SUCCESS) {
	CERR("error in fixed format: for key \"%s\", cannot read rate value (\"%s\")", key.c_str(), arg[0].c_str());
	return(AltiModule::FAILURE);
      }
      if(AltiCommon::readNumber(arg[1], ctp_cplx_bucket[i].level) != AltiModule::SUCCESS) {
	CERR("error in fixed format: for key \"%s\", cannot read level value (\"%s\")",key.c_str(),arg[1].c_str());
	return(AltiModule::FAILURE);
      }
      ctp_cplx_bucket[i].enable = (AltiCommon::trimString(arg[2]) == "ENABLED");
    }
    
    key = "CTP_SLIDING";
    if(readNextRecord(s,key,val) != AltiModule::SUCCESS) {
      CERR("error reading key \"%s\" from file \"%s\"", key.c_str(), fn.c_str());
      return(AltiModule::FAILURE);
    }
    if((num = AltiCommon::splitString(val,",",arg)) != (int)SlidingWindowConfiguration::VALUE_NUMBER) {
      CERR("error in fixed format: for key \"%s\" expected %d value%s, got %d value%s", key.c_str(), SlidingWindowConfiguration::VALUE_NUMBER, SlidingWindowConfiguration::VALUE_NUMBER > 1 ? "s" : "", num,num > 1? "s" : "");
      return(AltiModule::FAILURE);
    }
    if(AltiCommon::readNumber(arg[0], ctp_cplx_sliding_window.window) != AltiModule::SUCCESS) {
      CERR("error in fixed format: for key \"%s\", cannot read window value (\"%s\")", key.c_str(), arg[0].c_str());
      return(AltiModule::FAILURE);
    }
    if(AltiCommon::readNumber(arg[1], ctp_cplx_sliding_window.number) != AltiModule::SUCCESS) {
      CERR("error in fixed format: for key \"%s\", cannot read number value (\"%s\")", key.c_str(), arg[1].c_str());
      return(AltiModule::FAILURE);
    }
    ctp_cplx_sliding_window.enable = (AltiCommon::trimString(arg[2]) == "ENABLED");
      
    // close input file
    s.close();

    // print
    COUT("read AltiConfiguration from file \"%s\"", fn.c_str());
  
    return (AltiModule::SUCCESS);
}

//------------------------------------------------------------------------------

int AltiConfiguration::write(const std::string& fn) const {
  
    std::ostringstream buf;
    std::string key, key1;
    char os[STRING_LENGTH];
    unsigned int i, j;
  
    std::vector<std::string> arg;
  
    // open output file
    std::ofstream s(fn.c_str());
    if (!s) {
        CERR("cannot open output file \"%s\"", fn.c_str());
        return (AltiModule::FAILURE);
    }

    std::sprintf(os, "###############################################################################################################################\n"); s << os;

    std::sprintf(os, "# CONFIG|NOCONFIG\n"); s << os;
    key = "CLK_CONFIG";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), clk_config ? "CONFIG" : "NOCONFIG"); s << os;
    std::sprintf(os, "# JITTER_CLEANER | SWITCH\n"); s << os;
    key = "CLK_SOURCE";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), AltiModule::CLK_PLL_NAME[clk_select].c_str()); s << os;
    std::sprintf(os, "# OSCILLATOR | SWITCH\n"); s << os;
    key = "CLK_JC_SOURCE";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), AltiModule::CLK_JC_NAME[clk_jc_select].c_str()); s << os;
    key = "CLK_JC_FILE";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), clk_jc_file.c_str()); s << os;
    key = "CLK_PHASE_SHIFT";
    std::sprintf(os, "# -1073741823 .. 1073741823 [step: 15 ps]\n"); s << os;
    std::sprintf(os, "%-27s= %d\n", key.c_str(), clk_phase_shift); s << os;

    std::sprintf(os, "###############################################################################################################################\n"); s << os;

    std::sprintf(os, "# CONFIG|NOCONFIG\n"); s << os;
    key = "SIG_CONFIG";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), sig_config ? "CONFIG" : "NOCONFIG"); s << os;
    std::sprintf(os, "# BGO0|BGO2, BGO1|BGO3\n"); s << os;
    key = "SIG_BGO_FP";
    std::sprintf(os, "%-27s= %s, %s\n", key.c_str(), sig_fp_bgo2enable ? "BGO2" : "BGO0", sig_fp_bgo3enable ? "BGO3" : "BGO1"); s << os;
    std::sprintf(os, "# <CTP_OUT>, <ALTI_OUT>, <NIM_OUT>, <TO_FPGA>\n"); s << os;
    std::sprintf(os, "# <CTP_OUT>  = CTP_IN|ALTI_IN|NIM_IN|FROM_FPGA\n"); s << os;
    std::sprintf(os, "# <ALTI_OUT> = CTP_IN|ALTI_IN|NIM_IN|FROM_FPGA\n"); s << os;
    std::sprintf(os, "# <NIM_OUT>  = CTP_IN|ALTI_IN|NIM_IN|FROM_FPGA\n"); s << os;
    std::sprintf(os, "# <TO_FPGA>  = CTP_IN|ALTI_IN|NIM_IN|FROM_FPGA\n"); s << os;
    for (i = 0; i < AltiModule::SIGNAL_NUMBER_ROUTED; i++) {
        // needs to be changed, key must be ostringstream?
        switch (i) {
            case 0:
                key = "SIG_SWX_BC";
                break;
            case 1:
                key = "SIG_SWX_ORB";
                break;
            case 2:
                key = "SIG_SWX_L1A";
                break;
            case 3:
                key = "SIG_SWX_TTR1";
                break;
            case 4:
                key = "SIG_SWX_TTR2";
                break;
            case 5:
                key = "SIG_SWX_TTR3";
                break;
            case 6:
                key = "SIG_SWX_TTYP";
                break;
            case 7:
                //key = "SIG_SWX_TTYP1";
                //break;
            case 8:
                //key = "SIG_SWX_TTYP2";
                //break;
            case 9:
                //key = "SIG_SWX_TTYP3";
                //break;
            case 10:
                //key = "SIG_SWX_TTYP4";
                //break;
            case 11:
                //key = "SIG_SWX_TTYP5";
                //break;
            case 12:
                //key = "SIG_SWX_TTYP6";
                //break;
            case 13:
                //key = "SIG_SWX_TTYP7";
                //break;
                continue;
            case 14:
                key = "SIG_SWX_BGO0";
                break;
            case 15:
                key = "SIG_SWX_BGO1";
                break;
            case 16:
                key = "SIG_SWX_BGO2";
                break;
            case 17:
                key = "SIG_SWX_BGO3";
                break;
            default:
                break;
        }
        if (key == "SIG_SWX_TTYP") {
            std::sprintf(os, "%-27s= %s, %s, %s, %s\n", key.c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx_ttyp[0]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx_ttyp[1]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx_ttyp[2]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx_ttyp[3]].c_str()); s << os;
        }
        else {
            std::sprintf(os, "%-27s= %s, %s, %s, %s\n", key.c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx[i][0]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx[i][1]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx[i][2]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[sig_swx[i][3]].c_str()); s << os;
        }
    }
   
    std::sprintf(os, "# SHORT_CABLE|LONG_CABLE\n"); s << os;
    key = "SIG_EQZ_CTP_IN";
    std::sprintf(os, "%-27s= %s, %s\n", key.c_str(), sig_eqz_ctp_in_enable ? "ENABLED":"DISABLED", AltiModule::EQUALIZER_CONFIG_NAME[sig_eqz_ctp_in_mode].c_str()); s << os;
    key = "SIG_EQZ_ALTI_IN";
    std::sprintf(os, "%-27s= %s, %s\n", key.c_str(), sig_eqz_alti_in_enable ? "ENABLED":"DISABLED", AltiModule::EQUALIZER_CONFIG_NAME[sig_eqz_alti_in_mode].c_str()); s << os;
    
    std::sprintf(os, "# SYNC:  0 .. 7\n"); s << os;
    std::sprintf(os, "# SHAPE: 0 (disable) 1 or 2 [BC]\n"); s << os;
    AltiModule::ASYNC_INPUT_SIGNAL asyncIn;
    unsigned int phase[4];
    bool shape_enable[4];
    for (i = 0; i < AltiModule::ASYNC_INPUT_SIGNAL_NUMBER; i++) {
        asyncIn = (AltiModule::ASYNC_INPUT_SIGNAL) i;
       
        if (asyncIn > AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR3) continue;
        // merging
        if ((asyncIn >= AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO1) && (asyncIn <= AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO3)) continue;
        if ((asyncIn >= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP1) && (asyncIn <= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP3)) continue;
        if ((asyncIn >= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP5) && (asyncIn <= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP7)) continue;
        if ((asyncIn >= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR2) && (asyncIn <= AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR3)) continue;
        

        if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO0) {
            std::sprintf(os, "# BGO0..3\n"); s << os;
            key = "SIG_IO_SYNC_SWX_BGO";
            key1 = "SIG_IO_SHAPE_SWX_BGO";
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP0) {
            std::sprintf(os, "# TTYP0..3\n"); s << os;
            key = "SIG_IO_SYNC_SWX_TTYP0..3";
            key1 = "SIG_IO_SHAPE_SWX_TTYP0..3";
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP4) {
            std::sprintf(os, "# TTYP4..7\n"); s << os;
            key = "SIG_IO_SYNC_SWX_TTYP4..7";
            key1 = "SIG_IO_SHAPE_SWX_TTYP4..7";
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR1) {
            std::sprintf(os, "# TTR1..3\n"); s << os;
            key = "SIG_IO_SYNC_SWX_TTR";
            key1 = "SIG_IO_SHAPE_SWX_TTR";
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::LEMO_TTR1) {
            std::sprintf(os, "# TTR1..3\n"); s << os;
            key = "SIG_IO_SYNC_LEMO_TTR";
            key1 = "SIG_IO_SHAPE_LEMO_TTR";
        }
	else {
	  key = std::string("SIG_IO_SYNC_") + AltiModule::ASYNC_INPUT_SIGNAL_NAME[asyncIn].c_str();
	  key1 = std::string("SIG_IO_SHAPE_") + AltiModule::ASYNC_INPUT_SIGNAL_NAME[asyncIn].c_str();
        }
        

        if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_BGO0) {
            for (j = 0; j < 4; j++) {
                phase[j] = sig_io_sync_phase[asyncIn + j];
                shape_enable[j] = sig_io_shaping[asyncIn + j];
            }
            std::sprintf(os, "%-27s= %d, %d, %d, %d\n", key.c_str(), phase[0], phase[1], phase[2], phase[3]); s << os;
            std::sprintf(os, "%-27s= %s, %s, %s, %s\n", key1.c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[1]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[2]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[3]].c_str()); s << os;
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP0) {
            for (j = 0; j < 4; j++) {
                phase[j] = sig_io_sync_phase[asyncIn + j];
                shape_enable[j] = sig_io_shaping[asyncIn + j];
            }
            std::sprintf(os, "%-27s= %d, %d, %d, %d\n", key.c_str(), phase[0], phase[1], phase[2], phase[3]); s << os;
            std::sprintf(os, "%-27s= %s, %s, %s, %s\n", key1.c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[1]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[2]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[3]].c_str()); s << os;
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTYP4) {
            for (j = 0; j < 4; j++) {
                phase[j] = sig_io_sync_phase[asyncIn + j];
                shape_enable[j] = sig_io_shaping[asyncIn + j];
            }
            std::sprintf(os, "%-27s= %d, %d, %d, %d\n", key.c_str(), phase[0], phase[1], phase[2], phase[3]); s << os;
            std::sprintf(os, "%-27s= %s, %s, %s, %s\n", key1.c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[1]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[2]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[3]].c_str()); s << os;
        }
        else if (asyncIn == AltiModule::ASYNC_INPUT_SIGNAL::SWX_TTR1) {
            for (j = 0; j < 3; j++) {
                phase[j] = sig_io_sync_phase[asyncIn + j];
                shape_enable[j] = sig_io_shaping[asyncIn + j];
            }
            std::sprintf(os, "%-27s= %d, %d, %d\n", key.c_str(), phase[0], phase[1], phase[2]); s << os;
            std::sprintf(os, "%-27s= %s, %s, %s\n", key1.c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[1]].c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[2]].c_str()); s << os;
        }
	else {
	  phase[0] = sig_io_sync_phase[asyncIn];
	  shape_enable[0] = sig_io_shaping[asyncIn];
	  std::sprintf(os, "%-27s= %d\n", key.c_str(), phase[0]); s << os;
	  std::sprintf(os, "%-27s= %s\n", key1.c_str(), AltiModule::SYNC_SHAPE_NAME[shape_enable[0]].c_str()); s << os;
        }	
    }
    
    std::sprintf(os, "# signals source: INACTIVE, EXTERNAL, PATTERN, MINICTP, CALIBSIGNAL (L1A only) or CALIBREQUEST, TURNSIGNAL, ORBIT (TTR only) \n"); s << os;
    key = "SIG_SOURCE_ORB";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), sig_orb_source.c_str()); s << os;
    key = "SIG_SOURCE_L1A";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), sig_l1a_source.c_str()); s << os;
    key = "SIG_SOURCE_TTR";
    std::sprintf(os, "%-27s= %s, %s, %s\n", key.c_str(), sig_ttr_source[0].c_str(), sig_ttr_source[1].c_str(), sig_ttr_source[2].c_str()); s << os;
    key = "SIG_SOURCE_BGO";
    std::sprintf(os, "%-27s= %s, %s, %s, %s\n", key.c_str(), sig_bgo_source[0].c_str(), sig_bgo_source[1].c_str(), sig_bgo_source[2].c_str(), sig_bgo_source[3].c_str()); s << os;
    key = "SIG_SOURCE_TTYP";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), sig_ttyp_source.c_str()); s << os;

    key = "SIG_SOURCE_INPUT_ORB";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), sig_orb_input_source.c_str()); s << os;

    std::sprintf(os, "# TURN COUNTER MASK, MAXIMUM \n"); s << os;
    key = "SIG_TURN_COUNTER";
    std::sprintf(os, "%-27s= 0x%08x, %d\n", key.c_str(), turn_cnt_mask, turn_cnt_max); s << os;
    key = "SIG_BGO2_L1A_DELAY";
    std::sprintf(os, "%-27s= %d\n", key.c_str(), bgo2_l1a_delay); s << os;

    std::sprintf(os, "###############################################################################################################################\n"); s << os;

    std::sprintf(os, "# CONFIG|NOCONFIG\n"); s << os;
    key = "BSY_CONFIG";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), bsy_config ? "CONFIG" : "NOCONFIG"); s << os;
    std::sprintf(os, "# ENABLED|DISABLED\n"); s << os;
    key = "BSY_FROM_FP"; 
    std::sprintf(os, "%-27s= %s\n", key.c_str(), bsy_in_select[0] ? "ENABLED" : "DISABLED"); s << os;
    key = "BSY_FROM_CTP"; 
    std::sprintf(os, "%-27s= %s\n", key.c_str(), bsy_in_select[1] ? "ENABLED" : "DISABLED"); s << os;
    key = "BSY_FROM_ALTI"; 
    std::sprintf(os, "%-27s= %s\n", key.c_str(), bsy_in_select[2] ? "ENABLED" : "DISABLED"); s << os;
    key = "BSY_FROM_PG"; 
    std::sprintf(os, "%-27s= %s\n", key.c_str(), bsy_in_select[3] ? "ENABLED" : "DISABLED"); s << os;
    key = "BSY_FROM_VME";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), bsy_in_select[4] ? "ENABLED" : "DISABLED"); s << os;
    key = "BSY_FROM_ECR";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), bsy_in_select[5] ? "ENABLED" : "DISABLED"); s << os;
    std::sprintf(os, "# INACTIVE|LOCAL|CTP|ALTI\n"); s << os;
    key = "BSY_TO_CTP"; 
    AltiCommon::splitString(AltiModule::BUSY_SOURCE_NAME[bsy_out_select[0]], "_", arg);
    std::sprintf(os, "%-27s= %s\n", key.c_str(), arg[1].c_str()); s << os;
    key = "BSY_TO_ALTI"; 
    AltiCommon::splitString(AltiModule::BUSY_SOURCE_NAME[bsy_out_select[1]], "_", arg);
    std::sprintf(os, "%-27s= %s\n", key.c_str(), arg[1].c_str()); s << os;
    std::sprintf(os, "# 0|1\n"); s << os;
    key = "BSY_DATA_VME"; 
    std::sprintf(os, "%-27s= %s\n", key.c_str(), bsy_data_vme ? "1" : "0"); s << os;
    std::sprintf(os, "# ENABLED|DISABLED\n"); s << os;
    key = "BSY_GATING_L1A";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), bsy_l1a_masked ? "ENABLED" : "DISABLED"); s << os;
    std::sprintf(os, "# TTR1..3\n"); s << os;
    key = "BSY_GATING_TTR";
    std::sprintf(os, "%-27s= %s,%s,%s\n", key.c_str(), bsy_ttr_masked[0] ? "ENABLED" : "DISABLED", bsy_ttr_masked[1] ? "ENABLED" : "DISABLED", bsy_ttr_masked[2] ? "ENABLED" : "DISABLED"); s << os;
    std::sprintf(os, "# BGO0..3\n"); s << os;
    key = "BSY_GATING_BGO";
    std::sprintf(os, "%-27s= %s,%s,%s,%s\n", key.c_str(), bsy_bgo_masked[0] ? "ENABLED" : "DISABLED", bsy_bgo_masked[1] ? "ENABLED" : "DISABLED", bsy_bgo_masked[2] ? "ENABLED" : "DISABLED", bsy_bgo_masked[3] ? "ENABLED" : "DISABLED"); s << os;
    key = "BSY_LEVEL"; 
    std::sprintf(os, "%-27s= %s\n", key.c_str(), AltiModule::BUSY_LEVEL_NAME[bsy_level].c_str()); s << os;

    std::sprintf(os, "###############################################################################################################################\n"); s << os;

    std::sprintf(os, "# CONFIG|NOCONFIG\n"); s << os;
    key = "CRQ_CONFIG";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), crq_config ? "CONFIG" : "NOCONFIG"); s << os;
    std::sprintf(os, "# CALREQ2..0\n"); s << os;
    std::sprintf(os, "# CALREQ_FROM_RJ45|CALREQ_FROM_FRONT_PANEL|CALREQ_FROM_CTP|CALREQ_FROM_ALTI|CALREQ_FROM_PG|CALREQ_FROM_VME\n"); s << os;
    key = "CRQ_SOURCE"; 
    std::sprintf(os, "%-27s= %s,%s,%s\n", key.c_str(), AltiModule::CALREQ_INPUT_NAME[crq_in_select[2]].c_str(), AltiModule::CALREQ_INPUT_NAME[crq_in_select[1]].c_str(), AltiModule::CALREQ_INPUT_NAME[crq_in_select[0]].c_str()); s << os;
    std::sprintf(os, "# CALREQ2..0\n"); s << os;
    std::sprintf(os, "# CALREQ_INACTIVE|CALREQ_LOCAL|CALREQ_CTP|CALREQ_ALTI\n"); s << os;
    key = "CRQ_TO_CTP"; 
    std::sprintf(os, "%-27s= %s,%s,%s\n", key.c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[0][2]].c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[0][1]].c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[0][0]].c_str()); s << os;
    key = "CRQ_TO_ALTI"; 
    std::sprintf(os, "%-27s= %s,%s,%s\n", key.c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[1][2]].c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[1][1]].c_str(), AltiModule::CALREQ_SOURCE_NAME[crq_out_select[1][0]].c_str()); s << os;
    std::sprintf(os, "# 0..7\n"); s << os;
    key = "CRQ_DATA_VME"; 
    std::sprintf(os, "%-27s= %s,%s,%s\n", key.c_str(), crq_data_vme[0] ? "ENABLED" : "DISABLED", crq_data_vme[1] ? "ENABLED" : "DISABLED", crq_data_vme[2] ? "ENABLED" : "DISABLED"); s << os;

    std::sprintf(os, "###############################################################################################################################\n"); s << os;

    std::sprintf(os, "# CONFIG|NOCONFIG\n"); s << os;
    key = "MEM_CONFIG";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), mem_config ? "CONFIG" : "NOCONFIG"); s << os;
    std::sprintf(os, "# 0x00000000..0x000fffff\n"); s << os;
    key = "MEM_PG_START";
    std::sprintf(os, "%-27s= 0x%08x\n", key.c_str(), mem_pg_start_addr); s << os;
    key = "MEM_PG_STOP";
    std::sprintf(os, "%-27s= 0x%08x\n", key.c_str(), mem_pg_stop_addr); s << os;
    std::sprintf(os, "# ALTI/data/pg_alti.dat\n"); s << os;
    key = "MEM_PG_FILE";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), mem_pg_file.c_str()); s << os;
    std::sprintf(os, "# ENABLED|DISABLED\n"); s << os;
    key = "MEM_PG_ENABLE";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), mem_pg_enable ? "ENABLED" : "DISABLED"); s << os;
    std::sprintf(os, "# ONESHOT|REPEATED\n"); s << os;
    key = "MEM_PG_REPEAT";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), mem_pg_repeat ? "REPEATED" : "ONESHOT"); s << os;
    std::sprintf(os, "# MASKED|NOT_MASKED\n"); s << os;
    key = "MEM_SNAP_MASK_ORB";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), mem_snap_masked[AltiModule::ORB] ? "NOT_MASKED" : "MASKED"); s << os;
    key = "MEM_SNAP_MASK_L1A";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), mem_snap_masked[AltiModule::L1A] ? "NOT_MASKED" : "MASKED"); s << os;
    std::sprintf(os, "# TTR1..3\n"); s << os;
    key = "MEM_SNAP_MASK_TTR";
    std::sprintf(os, "%-27s= %s,%s,%s\n", key.c_str(), mem_snap_masked[AltiModule::TTR1] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::TTR2] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::TTR3] ? "NOT_MASKED" : "MASKED"); s << os;
    std::sprintf(os, "# TTYP0..3\n"); s << os;
    key = "MEM_SNAP_MASK_TTYP0..3";
    std::sprintf(os, "%-27s= %s,%s,%s,%s\n", key.c_str(), mem_snap_masked[AltiModule::TTYP0] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::TTYP1] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::TTYP2] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::TTYP3] ? "NOT_MASKED" : "MASKED"); s << os;
    std::sprintf(os, "# TTYP4..7\n"); s << os;
    key = "MEM_SNAP_MASK_TTYP4..7";
    std::sprintf(os, "%-27s= %s,%s,%s,%s\n", key.c_str(), mem_snap_masked[AltiModule::TTYP4] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::TTYP5] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::TTYP6] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::TTYP7] ? "NOT_MASKED" : "MASKED"); s << os;
    std::sprintf(os, "# BGO0..3\n"); s << os;
    key = "MEM_SNAP_MASK_BGO";
    std::sprintf(os, "%-27s= %s,%s,%s,%s\n", key.c_str(), mem_snap_masked[AltiModule::BGO0] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::BGO1] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::BGO2] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::BGO3] ? "NOT_MASKED" : "MASKED"); s << os;
    key = "MEM_SNAP_MASK_BUSY";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), mem_snap_masked[AltiModule::BUSY] ? "NOT_MASKED" : "MASKED"); s << os;
    std::sprintf(os, "# CALREQ0..2\n"); s << os;
    key = "MEM_SNAP_MASK_CALREQ";
    std::sprintf(os, "%-27s= %s,%s,%s\n", key.c_str(), mem_snap_masked[AltiModule::CALREQ0] ? "NOT_MASKED" : "MASKED", mem_snap_masked[AltiModule::CALREQ1] ? "MASKED" : "NOT_MASKED", mem_snap_masked[AltiModule::CALREQ2] ? "NOT_MASKED" : "MASKED"); s << os;

    std::sprintf(os, "###############################################################################################################################\n"); s << os;

    std::sprintf(os, "# CONFIG|NOCONFIG\n"); s << os;
    key = "TTC_CONFIG";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ttc_config ? "CONFIG" : "NOCONFIG"); s << os;
    std::sprintf(os, "# TX0..11\n"); s << os;
    std::sprintf(os, "# ENABLED|DISABLED\n"); s << os;
    key = "TTC_TX_ENABLE";
    std::sprintf(os, "%-27s= ", key.c_str()); s << os;
    for (i = 0; i < AltiModule::TRANSMITTER_NUMBER; i++) {
        if (i < AltiModule::TRANSMITTER_NUMBER - 1) {
            std::sprintf(os, "%s", ttc_tx_enable[i] ? "ENABLED," : "DISABLED,"); s << os;
        }
        else {
            std::sprintf(os, "%s\n", ttc_tx_enable[i] ? "ENABLED" : "DISABLED"); s << os;
        }
    }
    key = "TTC_TX_DELAY";
    std::sprintf(os, "%-27s= ", key.c_str()); s << os;
    for (i = 0; i < AltiModule::TRANSMITTER_NUMBER; i++) {
        if (i < AltiModule::TRANSMITTER_NUMBER - 1) {
            std::sprintf(os, "%d,", ttc_tx_delay[i]); s << os;
        }
        else {
            std::sprintf(os, "%d\n", ttc_tx_delay[i]); s << os;
        }
    }
    key = "TTC_L1A_SOURCE_FROM_L1A";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ttc_l1a_source[0] ? "ENABLED" : "DISABLED"); s << os;
    key = "TTC_L1A_SOURCE_FROM_TTR1..3";
    std::sprintf(os, "%-27s= %s,%s,%s\n", key.c_str(), ttc_l1a_source[1] ? "ENABLED" : "DISABLED", ttc_l1a_source[2] ? "ENABLED" : "DISABLED", ttc_l1a_source[3] ? "ENABLED" : "DISABLED"); s << os;
    key = "TTC_L1A_SOURCE_FROM_CALREQ";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ttc_l1a_source[4] ? "ENABLED" : "DISABLED"); s << os;
 key = "TTC_L1A_SOURCE_FROM_PATTERN";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ttc_l1a_source[5] ? "ENABLED" : "DISABLED"); s << os;
    key = "TTC_L1A_SOURCE_FROM_MINICTP";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ttc_l1a_source[6] ? "ENABLED" : "DISABLED"); s << os;
    key = "TTC_ORB_SOURCE";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ttc_orb_source.c_str()); s << os;
    key = "TTC_BGO_SOURCE";
    std::sprintf(os, "%-27s= %s, %s, %s, %s\n", key.c_str(), ttc_bgo_source[0].c_str(), ttc_bgo_source[1].c_str(), ttc_bgo_source[2].c_str(), ttc_bgo_source[3].c_str()); s << os;key = "TTC_BGO_SOURCE";
    key = "TTC_TTYP_SOURCE";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ttc_ttyp_source.c_str()); s << os;
    std::sprintf(os, "# <WIDTH>,<DELAY>\n"); s << os;
    std::sprintf(os, "# <WIDTH> = 0..4095 [step: BC]\n"); s << os;
    std::sprintf(os, "# <DELAY> = 0..4095 [step: BC]\n"); s << os;
    for (i = 0; i <= 3; i++) {
        switch (i) {
            case 0:
                key = "TTC_BGO0_INHIBIT";
                break;
            case 1:
                key = "TTC_BGO1_INHIBIT";
                break;
            case 2:
                key = "TTC_BGO2_INHIBIT";
                break;
            case 3:
                key = "TTC_BGO3_INHIBIT";
                break;
            default:
                break;
        }
        std::sprintf(os, "%-27s= ", key.c_str()); s << os;
        std::sprintf(os, "%04d,%04d\n", ttc_bgo_inhibit_width[i], ttc_bgo_inhibit_delay[i]); s << os;
    }
  
    std::sprintf(os, "# 0..4095 [step: BC]\n"); s << os;
    key = "TTC_TTYP_DELAY";
    std::sprintf(os, "%-27s= ", key.c_str()); s << os;
    std::sprintf(os, "%04d\n", ttc_ttyp_delay); s << os;
    std::sprintf(os, "# 0x0000..0x3fff\n"); s << os;
    key = "TTC_TTYP_ADDR";
    std::sprintf(os, "%-27s= ", key.c_str()); s << os;
    std::sprintf(os, "0x%04x\n", ttc_ttyp_addr); s << os;
    std::sprintf(os, "# 0x00..0xff\n"); s << os;
    key = "TTC_TTYP_SUBADDR";
    std::sprintf(os, "%-27s= ", key.c_str()); s << os;
    std::sprintf(os, "0x%02x\n", ttc_ttyp_subaddr); s << os;
    std::sprintf(os, "# INTERNAL|EXTERNAL\n"); s << os;
    key = "TTC_TTYP_SPACE";
    std::sprintf(os, "%-27s= ", key.c_str()); s << os;
    std::sprintf(os, "%s\n", AltiModule::TTC_ADDRESS_SPACE_NAME[ttc_ttyp_address_space].c_str()); s << os;
    std::sprintf(os, "# SYNCHRONOUS_SINGLE_BGO_SIGNAL|SYNCHRONOUS_REPETITIVE|ASYNCHRONOUS_BGO_SIGNAL|ASYNCHRONOUS_VME_ON_TRIGGER|ASYNCHRONOUS_FIFO_MODE\n"); s << os;
    for (i = 0; i <= 3; i++) {
        switch (i) {
            case 0:
                key = "TTC_BGO0_MODE";
                break;
            case 1:
                key = "TTC_BGO1_MODE";
                break;
            case 2:
                key = "TTC_BGO2_MODE";
                break;
            case 3:
                key = "TTC_BGO3_MODE";
                break;
            default:
                break;
        }
        std::sprintf(os, "%-27s= ", key.c_str()); s << os;
        std::sprintf(os, "%s\n", AltiModule::TTC_BGO_MODE_NAME[ttc_bgo_mode[i]].c_str()); s << os;
    }
    std::sprintf(os, "# ENABLED|DISABLED\n"); s << os;
    for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
        key = "TTC_" + AltiModule::TTC_FIFO_NAME[(AltiModule::TTC_FIFO) i] + "_RETRANS";
        std::sprintf(os, "%-27s= ", key.c_str()); s << os;
        std::sprintf(os, "%s\n", ttc_fifo_retransmit[i] ? "ENABLED" : "DISABLED"); s << os;
    }
    std::sprintf(os, "# FIFO word\n"); s << os;
    for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
        key = "TTC_" + AltiModule::TTC_FIFO_NAME[(AltiModule::TTC_FIFO) i] + "_WORD";
        std::sprintf(os, "%-27s= ", key.c_str()); s << os;
        std::sprintf(os, "0x%02x\n", ttc_fifo_word[i]); s << os;
    }


    std::sprintf(os, "###############################################################################################################################\n"); s << os;

    std::sprintf(os, "# CONFIG|NOCONFIG\n"); s << os;
    key = "CNT_CONFIG";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), cnt_config ? "CONFIG" : "NOCONFIG"); s << os;

    key = "CNT_COUNTERS";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), cnt_enable ? "ENABLED" : "DISABLED"); s << os;

    std::sprintf(os, "# ALTI/data/AltiModule_cnt_ttyp_lut.dat\n"); s << os;
    key = "CNT_TTYP_LUT_FILE";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), cnt_ttyp_lut_file.c_str()); s << os;

    std::sprintf(os, "###############################################################################################################################\n"); s << os;


     std::sprintf(os, "# CONFIG|NOCONFIG\n"); s << os;
    key = "PBM_CONFIG";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ctp_config ? "CONFIG" : "NOCONFIG"); s << os;
    std::sprintf(os, "# 0..4095 [step: BC]\n"); s << os;
    key = "PBM_BCID_OFFSET";
    std::sprintf(os, "%-27s= ", key.c_str()); s << os;
    std::sprintf(os, "%04d\n", pbm_bcid_offset); s << os;
    std::sprintf(os, "# 0x0000..0x3fff\n"); s << os;

    std::sprintf(os, "###############################################################################################################################\n"); s << os;
    
    std::sprintf(os, "# CONFIG|NOCONFIG\n"); s << os;
    key = "CTP_CONFIG";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ctp_config ? "CONFIG" : "NOCONFIG"); s << os;
    std::sprintf(os, "# VME|INTERNAL,LEN=<LEN>,FRQ=<FRQ>,BEF=<BEF>,AFT=<AFT>,OFF=<OFF>\n"); s << os;
    std::sprintf(os, "# <LEN> = 0..3          [step: BC, pulse length           = <LEN> + 1]\n"); s << os;
    std::sprintf(os, "# <FRQ> = 0..4294967295 [step: BC, frequency rate divider = <FRQ> + 1]\n"); s << os;
    std::sprintf(os, "# <BEF> = 0..16777215   [step: BC, length of the busy before ECR = <BEF>]\n"); s << os;
    std::sprintf(os, "# <AFT> = 0..16777215   [step: BC, length of the busy after  ECR = <AFT>]\n"); s << os;
    std::sprintf(os, "# <OFF> = 0..4095       [step: BC, orbit offset in order to remove the ECR busy = <OFF>]\n"); s << os;
    key = "CTP_ECR_GENERATION";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ctp_ecr_generation.print().c_str()); s << os;
    std::sprintf(os, "# signals source for mini-CTP: EXTERNAL, PATTERN \n"); s << os;
    key = "CTP_SOURCE_L1A";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ctp_l1a_source.c_str()); s << os;
    key = "CTP_SOURCE_BGO2";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ctp_bgo2_source.c_str()); s << os;
    key = "CTP_SOURCE_BGO3";
    std::sprintf(os, "%-27s= %s\n", key.c_str(), ctp_bgo3_source.c_str()); s << os;
    key = "CTP_SOURCE_TTR";
    std::sprintf(os, "%-27s= %s, %s, %s\n", key.c_str(), ctp_ttr_source[0].c_str(), ctp_ttr_source[1].c_str(), ctp_ttr_source[2].c_str()); s << os;
    key = "CTP_SOURCE_CALREQ";
    std::sprintf(os, "%-27s= %s, %s, %s\n", key.c_str(), ctp_crq_source[0].c_str(), ctp_crq_source[1].c_str(), ctp_crq_source[2].c_str()); s << os;

    key = "CTP_ITEM_LUT_FILE";
    std::sprintf(os,"%-27s= %s\n", key.c_str(), ctp_item_lut_file.c_str()); s << os;
    key = "CTP_TTYP_LUT_FILE";
    std::sprintf(os,"%-27s= %s\n", key.c_str(), ctp_ttyp_lut_file.c_str()); s << os;

    key = "CTP_RNDM_SEED"; buf.str("");
    if(ctp_rndm_seed_valid) {
      bool flag = false;
      for(u_int i = 0; i < ALTI::CTP_RND_THRESHOLD_NUMBER; i++) {
	std::sprintf(os, "%s0x%x", flag ? ", " : "", ctp_rndm_seed[i]); buf << os;
	flag = true;
      }
    }
    std::sprintf(os,"%-27s= %s\n", key.c_str(), buf.str().c_str()); s << os;
    key = "CTP_RNDM_THRESHOLD"; buf.str("");
    bool flag = false;
    for(u_int i = 0; i < ALTI::CTP_RND_THRESHOLD_NUMBER; i++) {
      std::sprintf(os,"%s0x%x", flag ? ", " : "", ctp_rndm_threshold[i]); buf << os;
      flag = true;
    }
    std::sprintf(os,"%-27s= %s\n", key.c_str(), buf.str().c_str()); s << os;
    
    key = "CTP_BGRP_FILE";
    std::sprintf(os,"%-27s= %s\n", key.c_str(), ctp_bgrp_file.c_str()); s << os;
    key = "CTP_BGRP_BCID";
    std::sprintf(os,"%-27s= %d\n", key.c_str(), ctp_bgrp_bcid_offset); s << os;
    key = "CTP_BGRP_MASK";
    std::sprintf(os,"%-27s= %s\n", key.c_str(), ctp_bgrp_mask_file.c_str()); s << os;

    key = "CTP_PRSC_SEED";
    if(ctp_prsc_seed_valid) {
      std::sprintf(os,"%-27s= %s\n", key.c_str(), ctp_prsc_seed_file.c_str()); s << os;
    }
    else {
      std::sprintf(os,"%-27s=\n", key.c_str()); s << os;
    }
    key = "CTP_PRSC_THRESHOLD";
    std::sprintf(os,"%-27s= %s\n", key.c_str(), ctp_prsc_threshold_file.c_str()); s << os;
    
    key = "CTP_TAV_ENABLE";
    std::sprintf(os,"%-27s= 0x%x\n", key.c_str(), ctp_tav_enable); s << os;

    key = "CTP_SIMPLE";
    std::sprintf(os,"%-27s= %d\n", key.c_str(), ctp_smpl_deadtime); s << os;
    
    for(u_int i = 0; i < AltiModule::LEAKY_BUCKET_NUMBER; i++) {
      buf.str(""); buf << "CTP_BUCKET" << i; key = buf.str();
      std::sprintf(os,"%-27s= %d, %d, %s\n", key.c_str(), ctp_cplx_bucket[i].rate, ctp_cplx_bucket[i].level, ctp_cplx_bucket[i].enable ? "ENABLED" : "DISABLED"); s << os;
    }
    
    key = "CTP_SLIDING";
    std::sprintf(os,"%-27s= %d, %d, %s\n", key.c_str(), ctp_cplx_sliding_window.window, ctp_cplx_sliding_window.number, ctp_cplx_sliding_window.enable ? "ENABLED" : "DISABLED"); s << os;
    
    std::sprintf(os, "###############################################################################################################################\n"); s << os;

    // close output file
    s.close();

    // print
    COUT("wrote AltiConfiguration to file \"%s\"", fn.c_str());
  
    return (AltiModule::SUCCESS);
}


//------------------------------------------------------------------------------

void AltiConfiguration::readSWXIndividualToBundleTTYP() {
    unsigned int i, j, ttyp;
    AltiModule::SIGNAL_SOURCE src = AltiModule::SIGNAL_SOURCE::INVALID_CONFIG;
    bool mismatch;
    for (j = 0; j < AltiModule::SIGNAL_DESTINATION_NUMBER; j++) {
        ttyp = 0;
        mismatch = false;
        for (i = 0; i < AltiModule::SIGNAL_NUMBER_ROUTED; i++) {
            if ((AltiModule::SIGNAL_NAME[i]).rfind("TTYP", 0) == 0) {
                if (ttyp++ == 0) {
                    src = sig_swx[i][j];
                }
                else {
                    if (sig_swx[i][j] != src) {
                        mismatch = true;
                        break;
                    }
                }
            }
        }
        if (mismatch) {
            sig_swx_ttyp[j] = AltiModule::SIGNAL_SOURCE::INVALID_CONFIG;
        }
        else {
            sig_swx_ttyp[j] = src;
        }
    }
}

//------------------------------------------------------------------------------

int AltiConfiguration::readNextRecord(std::istream &s, std::string &key, std::string &val) {

    char os[STRING_LENGTH];
    std::string line, nkey;

    val.clear();

    while (true) {
        if (!s.getline(os, STRING_LENGTH)) return (AltiModule::FAILURE);
        line = os;
        if (!line.empty() && !(line.substr(0, 1) == "#") && !(line.substr(0, 2) == "//")) break;
    }

    std::string::size_type pos = line.find_first_of("=", 0);
    nkey = AltiCommon::trimString(line.substr(0, pos));

    if (nkey != key) {
        CERR("error in fixed format: expected key \"%s\" got key \"%s\"", key.c_str(), nkey.c_str());
        return (AltiModule::FAILURE);
    }

    val = AltiCommon::trimString(line.substr(pos + 1));

    return (AltiModule::SUCCESS);
}

//------------------------------------------------------------------------------

std::string AltiConfiguration::bool2string(const bool enabled) {
    return (enabled ? "ENABLED" : "DISABLED");
}

