%module ALTI
%{
#include "ALTI/ALTI.h"
using namespace LVL1;
%}

%include <typemaps.i>
%apply unsigned int &OUTPUT { unsigned int& };

%include "RCDBitString/BitSet.h"
%include "ALTI/ALTI_BITSTRING.h"
%include "ALTI/ALTI.h"
