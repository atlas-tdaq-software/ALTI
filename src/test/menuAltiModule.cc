//******************************************************************************
// file: menuAltiModule.cc
// desc: menu program for ALTI Module
// auth: 02/11/17 P. Kuzmanovic
//*****************************************************************************

#include "RCDUtilities/RCDUtilities.h"
#include "RCDMenu/RCDMenu.h"
#include "RCDVme/RCDCmemSegment.h"
#include "ALTI/AltiModule.h"

#include "ALTI/AltiConfiguration.h"
#include "ALTI/AltiCommon.h"
#include "ALTI/AltiCounter.h"
#include "ALTI/AltiMiniCtpCounter.h"
#include "ALTI/EventCounter.h"
#include "ALTI/AltiBusyCounter.h"

#include <fstream>
#include <iomanip>
#include <cmath>
#include <unistd.h>
#include <filesystem>
		
using namespace std;
using namespace RCD;
using namespace LVL1;

AltiModule *alti = 0;
CMEMSegment *segment[2] = {0, 0};

// BCR word used in TTC decoder
unsigned int m_bcr_word = 0x01;
int m_bcr_delay = 0;
bool m_bcr_bitwise = true;

//------------------------------------------------------------------------------

class AltiModuleOpen : public MenuItem {
  public:
    AltiModuleOpen() { setName("open AltiModule"); }
    int action() {

        std::vector<unsigned int> slots;
        unsigned int n = AltiModule::pollSlots(slots);
        std::printf("%d ALTI module%s found", n, (n == 1) ? "" : "s");
        if (n > 0) {
            std::printf(" in slot%s ", (n > 1) ? "s" : "");
            unsigned int i;
            for (i = 0; i < n; i++) {
                std::printf("%d%s", slots[i], (i < n - 1) ? ", " : ".\n");
            }
        }
        else {
            std::printf("\n");
            return AltiModule::FAILURE;
        }

        int s;
        if (n == 1) {
            s = slots[0];
        }
        else {
            while (true) {
                s  = enterInt("Slot number", AltiModule::ALTI_SLOT_MIN, AltiModule::ALTI_SLOT_MAX);
                if (std::find(slots.begin(), slots.end(), s) != slots.end()) break;
            }
        }
        std::printf("ALTI module in slot %d selected\n", s);

        // create AltiModule
        alti = new AltiModule(s);

        // create CMEM segements
        std::string snam = "menuAltiModule";
        // sometimes we'll read both TTC decoder memories at once
        segment[0] = new RCD::CMEMSegment(snam + '0', sizeof(u_int)*AltiModule::MAX_MEMORY, true);
        if ((*segment[0])() != CMEM_RCC_SUCCESS) {
            CERR("error opening CMEM segment \"%s\" of size 0x%08x\n", (snam + '0').c_str(), segment[0]->Size());
            return AltiModule::FAILURE;
        }
        COUT("opened CMEM segment \"%s\" of size 0x%08x, phys 0ix%08x, virt 0x%08x", (snam + '0').c_str(), segment[0]->Size(), segment[0]->PhysicalAddress(), segment[0]->VirtualAddress());
        segment[1] = new RCD::CMEMSegment(snam + '1', sizeof(u_int)*AltiModule::MAX_MEMORY, true);
        if ((*segment[1])() != CMEM_RCC_SUCCESS) {
            CERR("error opening CMEM segment \"%s\" of size 0x%08x\n", (snam + '1').c_str(), segment[1]->Size());
            return AltiModule::FAILURE;
        }
        COUT("opened CMEM segment \"%s\" of size 0x%08x, phys 0ix%08x, virt 0x%08x", (snam + '1').c_str(), segment[1]->Size(), segment[1]->PhysicalAddress(), segment[1]->VirtualAddress());

        return ((*alti)());
    }
};

//------------------------------------------------------------------------------

class AltiModuleClose : public MenuItem {
  public:
    AltiModuleClose() { setName("close AltiModule"); }
    int action() {

        // delete CMEM segments
        if (segment[0]) delete segment[0];
        if (segment[1]) delete segment[1];

        // delete AltiModule
        if (alti) delete alti;

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiCSRManufacturerIdRead : public MenuItem {
  public:
    AltiCSRManufacturerIdRead() { setName("read  manufacturer ID"); }
    int action() {

        int rtnv(0);
        unsigned int manuf;

        if ((rtnv = alti->CSRManufacturerIdRead(manuf)) != AltiModule::SUCCESS) {
            CERR("reading manufacturer id", "");
            return (rtnv);
        }
        std::printf("manufacturer id = 0x%08x <-> %s\n", manuf, ((manuf == AltiModule::MANUF_ID)? "CERN" : "ERROR: NOT IDENTIFIED"));

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCSRBoardIdRead : public MenuItem {
  public:
    AltiCSRBoardIdRead() { setName("read  board        ID"); }
    int action() {

        int rtnv(0);
        unsigned int board;

        if ((rtnv = alti->CSRBoardIdRead(board)) != AltiModule::SUCCESS) {
            CERR("reading board id", "");
            return (rtnv);
        }
        std::printf("board id = 0x%08x <-> %s\n", board, ((board == AltiModule::BOARD_ID)? "ALTI" : "ERROR: NOT IDENTIFIED"));

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCSRRevisionIdRead : public MenuItem {
  public:
    AltiCSRRevisionIdRead() { setName("read  revision     ID"); }
    int action() {

        int rtnv(0);
        unsigned int rev, dd, mm, yy, revnum;

        if ((rtnv = alti->CSRRevisionIdRead(rev)) != AltiModule::SUCCESS) {
            CERR("reading revision id", "");
            return (rtnv);
        }
        AltiCommon::parseRevision(rev, dd, mm, yy, revnum);
        std::printf("revision id = 0x%08x (#%d, %02d.%02d.%d)\n", rev, revnum, dd, mm, yy);

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCSRBARRead : public MenuItem {
  public:
    AltiCSRBARRead() { setName("read  BAR"); }
    int action() {

        int rtnv(0);
        unsigned int bar;

        if ((rtnv = alti->CSRBARRead(bar)) != AltiModule::SUCCESS) {
            CERR("reading BAR", "");
            return (rtnv);
        }
        std::printf("BAR = 0x%08x <-> slot %d\n", bar, bar  >> 3);

        return (rtnv);
    }
};

//------------------------------------------------------------------------------


class AltiCSRADERRead : public MenuItem {
  public:
    AltiCSRADERRead() { setName("read  ADER"); }
    int action() {

        int rtnv(0);
        unsigned int ader;

        if ((rtnv = alti->CSRADERRead(ader)) != AltiModule::SUCCESS) {
            CERR("reading ADER", "");
            return (rtnv);
        }
        std::printf("ADER = 0x%08x\n", ader);

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCSRADERWrite : public MenuItem {
  public:
    AltiCSRADERWrite() { setName("write ADER"); }
    int action() {

        int rtnv(0);
        unsigned int ader = enterHex("ADER (8 msbs)", 0, 0xff);
        ader <<= 24;
        if ((rtnv = alti->CSRADERWrite(ader)) != AltiModule::SUCCESS) {
            CERR("writing ADER", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSerialNumber : public MenuItem {
  public: AltiSerialNumber() { setName("read  serial number"); }
    int action() {

        int rtnv(0);
        std::string ser, id;

        if ((rtnv = alti->OWSerialNumber(ser)) != AltiModule::SUCCESS) {
            cout << "ALTI does not seem to have DS1WM => PRE_PRODUCTION_MODULE" << std::endl;
            return (rtnv);
        }
        DS1WM::getIdentifier(ser, id);
        cout << "Serial number = \"" << ser << "\" - identifier = \"" << id << "\"" << std::endl;

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiReset : public MenuItem {
  public:
    AltiReset() { setName("reset ALTI"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->AltiReset()) != AltiModule::SUCCESS) {
            CERR("resetting ALTI","");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------
class AltiSetup : public MenuItem {
  public:
    AltiSetup() { setName("setup ALTI"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->AltiSetup()) != AltiModule::SUCCESS) {
            CERR("setting up ALTI","");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCheck : public MenuItem {
  public:
    AltiCheck() { setName("check ALTI"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->AltiCheck()) != AltiModule::SUCCESS) {
            CERR("checking ALTI","");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiConfigRead : public MenuItem {
  public:
    AltiConfigRead() { setName("read  ALTI configuration"); }
    int action() {

        int rtnv(0);
        AltiConfiguration cfg;
        if ((rtnv = alti->AltiConfigRead(cfg)) != AltiModule::SUCCESS) {
            CERR("reading ALTI config", "");
            return (rtnv);
        }
        cfg.print();

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiConfigReadFile : public MenuItem {
  public:
    AltiConfigReadFile() { setName("read  ALTI configuration [into file]"); }
    int action() {

        int rtnv(0);
        std::string pfx = enterString("File PREFIX (to \"cfg.dat\")");
        std::string fn = pfx + "cfg.dat";
        AltiConfiguration cfg(pfx);

        if ((rtnv = alti->AltiConfigRead(cfg)) != AltiModule::SUCCESS) {
            CERR("reading ALTI config", "");
            return (rtnv);
        }
        if ((rtnv = cfg.write(fn)) != AltiModule::SUCCESS) {
            CERR("writing ALTI config into file \"%s\"", fn.c_str());
            return (rtnv);
        }
	 
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiConfigWriteFile : public MenuItem {
  public:
    AltiConfigWriteFile() { setName("write ALTI configuration [from file]"); }
    int action() {

        int rtnv(0);
        AltiConfiguration cfg; 
        std::string fn = enterString("File name (empty=cfg.dat)");

        if (fn.empty()) fn = "cfg.dat"; 
      
	if ((rtnv = cfg.read(fn)) != AltiModule::SUCCESS) {
	  CERR("reading ALTI config from file \"%s\"", fn.c_str());
	  return (rtnv);
	}	
	
        if ((rtnv = alti->AltiConfigWrite(cfg)) != AltiModule::SUCCESS) {
	  CERR("writing ALTI config","");
	  return (rtnv);
        }
	
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiConfigWrite : public MenuItem {
  public:
    AltiConfigWrite() { setName("write ALTI configuration [default]"); }
    int action() {

        int rtnv(0);
        bool flag = (bool) enterInt("Write default configuration (0=\"NO\", 1=\"YES\")", 0, 1);
        if (!flag) return (rtnv);

        AltiConfiguration cfg;
        if ((rtnv = alti->AltiConfigWrite(cfg)) != AltiModule::SUCCESS) {
            CERR("writing ALTI config", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiConfigWritePredefined : public MenuItem {
  public:
    AltiConfigWritePredefined() { setName("write ALTI configuration [predefined modes]"); }
    int action() {

        int rtnv(0);
        AltiConfiguration cfg;

        unsigned int mode_choice = enterInt("Predefined mode (0=\"CTP_SLAVE\", 1=\"ALTI_SLAVE\", 2=\"LEMO_SLAVE\", 3=\"PATTERN_GENERATOR\")", 0, 3);

	string prefix = ".";
	bool local_path = false;
	// first check local directory
	char* path = getenv("PWD");
	if(path) {     
	  std::filesystem::path full_path = std::string(path) + "/installed/share/data/ALTI/"; 
	  if(std::filesystem::is_directory(full_path)) { 
	    prefix = std::string(path) + "/installed";
	    local_path = true;
	  }	
	} 
	if(!local_path) { // check L1CT release 
	  path = getenv("L1CT_INST_PATH");
	  if(path) prefix = path;
	}
	if(!path) { // check TDAQ release 
	  path = getenv("TDAQ_INST_PATH");
	  if(path) prefix = path;
	}
	prefix += "/share/data/ALTI/";
	std::string cfg_file;
        switch (mode_choice) {
	case 0: // CTP_SLAVE:
	  cfg_file = "AltiModule_CTP_Slave_cfg.dat";
	  break;
	case 1: // ALTI_SLAVE:
	  cfg_file = "AltiModule_ALTI_Slave_cfg.dat";
	  break;
	case 2: // LEMO_SLAVE:
	  cfg_file = "AltiModule_LEMO_Slave_cfg.dat";
	  break;
	case 3: // PATTERN_GENERATOR:
	  cfg_file = "AltiModule_Pattern_Generator_cfg.dat";
	  break;
	default:
	  break;
        }

        if (cfg_file.empty()) cfg.clearFileNames();
        else {
            if ((rtnv = cfg.read(prefix+cfg_file)) != AltiModule::SUCCESS) {
	      CERR("reading ALTI config from file \"%s%s\"", prefix.c_str(),cfg_file.c_str());
                return (rtnv);
            }
	    // set prefix to configuration files 
	    cfg.addFileNamePrefix(prefix);
	}

        if ((rtnv = alti->AltiConfigWrite(cfg)) != AltiModule::SUCCESS) {
            CERR("writing ALTI config","");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiSIGSwitchConfigRead : public MenuItem {
  public:
    AltiSIGSwitchConfigRead() { setName("read  switch    configuration"); }
    int action() {

        int rtnv(0);

        AltiModule::SIGNAL signal;
        AltiModule::SIGNAL_SOURCE src[AltiModule::SIGNAL_DESTINATION_NUMBER];
        AltiModule::SIGNAL_DESTINATION dst;

        //table
        printf("\n");
        printf("+-----------------------------------------------------------+\n");
        printf("| Signal    | CTP out   | ALTI out  | NIM out   | to FPGA   |\n");
        printf("+-----------------------------------------------------------+\n");

        unsigned int i, j;
        for (i = 0; i < AltiModule::SIGNAL_NUMBER_ROUTED; i++) {
            signal = AltiModule::SIGNAL(i);
            for (j = 0; j < AltiModule::SIGNAL_DESTINATION_NUMBER; j++) {
                dst = AltiModule::SIGNAL_DESTINATION(j);
                if ((rtnv = alti->SIGSwitchConfigRead(signal, dst, src[j])) != AltiModule::SUCCESS) {
                    CERR("reading signal switch configuration", "");
                    return (rtnv);
                }
            }
            printf("| [%02d]%-5s | %-9s | %-9s | %-9s | %-9s |\n", i, AltiModule::SIGNAL_NAME[signal].c_str(), AltiModule::SIGNAL_SOURCE_NAME[src[0]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[src[1]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[src[2]].c_str(), AltiModule::SIGNAL_SOURCE_NAME[src[3]].c_str());
            printf("+-----------------------------------------------------------+\n");
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGSwitchConfigWrite : public MenuItem {
  public:
    AltiSIGSwitchConfigWrite() { setName("write switch    configuration"); }
    int action() {

        int rtnv(0);

        AltiModule::SIGNAL signal;
        AltiModule::SIGNAL_DESTINATION dst;
        AltiModule::SIGNAL_SOURCE src;
        unsigned int signal_choice = enterInt("Signal (0=\"BC\", 1=\"ORB\", ..., 17=\"BGO3\", 18=all)                       ", 0, AltiModule::SIGNAL_NUMBER_ROUTED - 1 + 1);
        unsigned int dst_choice = enterInt("Destination (0=\"CTP_OUT\", 1=\"ALTI_OUT\", 2=\"NIM_OUT\", 3=\"TO_FPGA\", 4=all)", 0, AltiModule::SIGNAL_DESTINATION_NUMBER - 1 + 1);
        src = (AltiModule::SIGNAL_SOURCE) enterInt("Source (0=\"CTP_IN\", 1=\"ALTI_IN\", 2=\"NIM_IN\", 3=\"FROM_FPGA\")             ", 0, AltiModule::SIGNAL_SOURCE_NUMBER - 1 - 1);
        if (signal_choice < AltiModule::SIGNAL_NUMBER_ROUTED) signal = (AltiModule::SIGNAL) signal_choice;
        if (dst_choice < AltiModule::SIGNAL_DESTINATION_NUMBER) dst = (AltiModule::SIGNAL_DESTINATION) dst_choice;

        if ((signal_choice < AltiModule::SIGNAL_NUMBER_ROUTED) && (dst_choice < AltiModule::SIGNAL_DESTINATION_NUMBER)) { // 1 signal, 1 output
            if ((rtnv = alti->SIGSwitchConfigWrite(signal, dst, src)) != AltiModule::SUCCESS) {
                CERR("writing source %s to switch %s destination %s", AltiModule::SIGNAL_SOURCE_NAME[src].c_str(), AltiModule::SIGNAL_NAME[signal].c_str(), AltiModule::SIGNAL_DESTINATION_NAME[dst].c_str());
                return (rtnv);
            }
        }
        else if (signal_choice < AltiModule::SIGNAL_NUMBER_ROUTED) { // 1 signal, all outputs
            if ((rtnv = alti->SIGSwitchConfigWrite(signal, src)) != AltiModule::SUCCESS) {
                CERR("writing source %s to switch %s", AltiModule::SIGNAL_SOURCE_NAME[src].c_str(), AltiModule::SIGNAL_NAME[signal].c_str());
                return (rtnv);
            }
        }
        else if (dst_choice < AltiModule::SIGNAL_DESTINATION_NUMBER) { // all signals, 1 output
            if ((rtnv = alti->SIGSwitchConfigWrite(dst, src)) != AltiModule::SUCCESS) {
                CERR("writing source %s to destination %s", AltiModule::SIGNAL_SOURCE_NAME[src].c_str(), AltiModule::SIGNAL_DESTINATION_NAME[dst].c_str());
                return (rtnv);
            }
        }
        else { // all signals, all outputs
            if ((rtnv = alti->SIGSwitchConfigWrite(src)) != AltiModule::SUCCESS) {
                CERR("writing source %s", AltiModule::SIGNAL_SOURCE_NAME[src].c_str());
                return (rtnv);
            }
        }


        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGSwitchMasterConfigurationWrite : public MenuItem {
  public:
    AltiSIGSwitchMasterConfigurationWrite() { setName("write switch    configuration [Master mode - from FPGA]"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->SIGSwitchConfigWrite(AltiModule::MASTER)) != AltiModule::SUCCESS) {
            CERR("configuring switches to master mode", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGSwitchCtpSlaveConfigurationWrite : public MenuItem {
  public:
    AltiSIGSwitchCtpSlaveConfigurationWrite() { setName("write switch    configuration [Slave  mode - from CTP]"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->SIGSwitchConfigWrite(AltiModule::CTP_SLAVE)) != AltiModule::SUCCESS) {
            CERR("configuring switches to CTP-slave mode", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGSwitchAltiSlaveConfigurationWrite : public MenuItem {
  public:
    AltiSIGSwitchAltiSlaveConfigurationWrite() { setName("write switch    configuration [Slave  mode - from ALTI]"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->SIGSwitchConfigWrite(AltiModule::ALTI_SLAVE)) != AltiModule::SUCCESS) {
            CERR("configuring switches to ALTI-slave mode", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGSwitchLemoSlaveConfigurationWrite : public MenuItem {
  public:
    AltiSIGSwitchLemoSlaveConfigurationWrite() { setName("write switch    configuration [Lemo  mode - from NIM]"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->SIGSwitchConfigWrite(AltiModule::LEMO_SLAVE)) != AltiModule::SUCCESS) {
            CERR("configuring switches to LEMO-slave mode", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiSIGEqualizersConfigRead : public MenuItem {
  public:
    AltiSIGEqualizersConfigRead() { setName("read  equalizer configuration"); }
    int action() {

        int rtnv(0);

        AltiModule::SIGNAL_SOURCE src;
        //AltiModule::SIGNAL_DESTINATION dst;

        printf("\n");
        printf("+---------------------------------------------------------------------------------------------------------+\n");
        printf("| Cable      | Status     | Vpeak[V]   | Vpole[V]   | Vgain[V]   | Voffset[V] || High-level configuration |\n");
        printf("+---------------------------------------------------------------------------------------------------------+\n");

	bool enabled;
        AltiModule::EQUALIZER_CONFIG eqz_config;

        unsigned int vpeak, vpole, vgain, voffset;
        float v_vpeak, v_vpole, v_vgain, v_voffset;
        unsigned int i;
        for (i = 0; i < AltiModule::SIGNAL_SOURCE_NUMBER - 3; i++) {
            src = AltiModule::SIGNAL_SOURCE(i);

	    if ((rtnv = alti->SIGEqualizersConfigRead(src, enabled, eqz_config)) != AltiModule::SUCCESS) {
	      CERR("reading high-level %s equalizer configuration", AltiModule::SIGNAL_SOURCE_NAME[src].c_str());
	      return (rtnv);
	    }

            if ((rtnv = alti->SIGEqualizersConfigRead(src, vpeak, vpole, vgain, voffset)) != AltiModule::SUCCESS) {
                CERR("reading cable %s equalizer configuration", AltiModule::SIGNAL_SOURCE_NAME[src].c_str());
                return (rtnv);
            }
            v_vpeak = ((float) vpeak)/256.0*2.5;
            v_vpole = ((float) vpole)/256.0*2.5;
            v_vgain = ((float) vgain)/256.0*2.5;
            v_voffset = ((float) voffset)/256.0*2.5;
            std::printf("| [%01d]%-7s | %-10s | %01.5f    | %01.5f    | %01.5f    | %01.5f    || %-24s |\n", src,  AltiModule::SIGNAL_SOURCE_NAME[src].c_str(), enabled ? "ENABLED " : "DISABLED", v_vpeak, v_vpole, v_vgain, v_voffset, (AltiModule::EQUALIZER_CONFIG_NAME[eqz_config] + ((eqz_config == AltiModule::EQUALIZER_CONFIG::SHORT_CABLE) ? " (< 10m)" : ((eqz_config == AltiModule::EQUALIZER_CONFIG::LONG_CABLE) ? " (> 10m)" : ""))).c_str());
            printf("+---------------------------------------------------------------------------------------------------------+\n");
        }

        return (rtnv);
    }
};


//------------------------------------------------------------------------------

class AltiSIGEqualizersPredefinedModeWrite : public MenuItem {
  public:
    AltiSIGEqualizersPredefinedModeWrite() { setName("write equalizer configuration"); }
    int action() {

        int rtnv(0);
	
	AltiModule::SIGNAL_SOURCE src(AltiModule::CTP_IN);
        unsigned int src_choice = enterInt("Cable (0=\"CTP_IN\", 1=\"ALTI_IN\")                                  ", 0, AltiModule::SIGNAL_SOURCE_NUMBER - 3 - 1);
	if (src_choice < AltiModule::SIGNAL_SOURCE_NUMBER - 3) src = (AltiModule::SIGNAL_SOURCE) src_choice;
	
	std::string command = "Enable " + AltiModule::SIGNAL_SOURCE_NAME[src] + " equalizer (0=disable, 1=enable)";	  
	bool enable = (bool) enterInt(command.c_str(), 0, 1);
	if (enable) { 
	  command = AltiModule::SIGNAL_SOURCE_NAME[src] + " mode (0=\"SHORT_CABLE (< 10m)\", 1=\"LONG_CABLE (> 10m)\")";
	  AltiModule::EQUALIZER_CONFIG mode = (AltiModule::EQUALIZER_CONFIG) enterInt(command.c_str(), 0, 1);   
	  if ((rtnv = alti->SIGEqualizersConfigWrite(src, enable, mode)) != AltiModule::SUCCESS) {
	    CERR("writing equalizer configuration %s: %s", AltiModule::SIGNAL_SOURCE_NAME[src].c_str(), AltiModule::EQUALIZER_CONFIG_NAME[mode].c_str());
            return (rtnv);
	  }
	}
	else {
	  if ((rtnv = alti->SIGEqualizersEnableWrite(src, false)) != AltiModule::SUCCESS) {
	    CERR("disabling equalizer for %s", AltiModule::SIGNAL_SOURCE_NAME[src].c_str());
	    return (rtnv);
	  }
	}
	
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGEqualizersConfigWrite : public MenuItem {
  public:
    AltiSIGEqualizersConfigWrite() { setName("write equalizer configuration [expert user]"); }
    int action() {

        int rtnv(0);

        AltiModule::SIGNAL_SOURCE src(AltiModule::CTP_IN);
        AltiModule::EQUALIZER_VOLTAGE vlt(AltiModule::V_PEAK);
        unsigned int i, v, v_vector[AltiModule::EQUALIZER_VOLTAGE_NUMBER];
        std::ostringstream msg;
        unsigned int src_choice = enterInt("Cable (0=\"CTP_IN\", 1=\"ALTI_IN\")                                  ", 0, AltiModule::SIGNAL_SOURCE_NUMBER - 3 - 1);
	if (src_choice < AltiModule::SIGNAL_SOURCE_NUMBER - 3) src = (AltiModule::SIGNAL_SOURCE) src_choice;

	bool enable = (bool) enterInt("Enable  (0=disable, 1=enable)", 0, 1);
	if(enable) { 
	  if ((rtnv = alti->SIGInputSyncEnableWrite(src)) != AltiModule::SUCCESS) {
	    CERR("enabling equalizer for %s", AltiModule::SIGNAL_SOURCE_NAME[src].c_str());
	    return (rtnv);
	  }

	  unsigned int vlt_choice = enterInt("Voltage (0=\"V_PEAK\", 1=\"V_POLE\", 2=\"V_GAIN\", 3=\"V_OFFSET\", 4=all)", 0, AltiModule::EQUALIZER_VOLTAGE_NUMBER - 1 + 1);
	  if (vlt_choice < AltiModule::EQUALIZER_VOLTAGE_NUMBER) vlt = (AltiModule::EQUALIZER_VOLTAGE) vlt_choice;
	  
	  if (vlt_choice < AltiModule::EQUALIZER_VOLTAGE_NUMBER) { // 1 cable, 1 voltage
            msg.str("");
            msg.clear();
            msg << ("\"" + AltiModule::SIGNAL_SOURCE_NAME[src] + "\" ") << (AltiModule::EQUALIZER_VOLTAGE_NAME[vlt]);
            v = (unsigned int) (AltiCommon::enterFloat(msg.str(), 0, 2.5)/2.5*255.0);
            if (alti->SIGEqualizersConfigWrite(src, vlt, v) != AltiModule::SUCCESS) {
	      CERR("writing %s equalizer %s voltage", AltiModule::SIGNAL_SOURCE_NAME[src].c_str(), AltiModule::EQUALIZER_VOLTAGE_NAME[vlt].c_str());
	      return (rtnv);
            }
	  }
	  else if (vlt_choice == AltiModule::EQUALIZER_VOLTAGE_NUMBER) { // 1 cable, all voltage
            for (i = 0; i < AltiModule::EQUALIZER_VOLTAGE_NUMBER; i++) {
	      vlt = (AltiModule::EQUALIZER_VOLTAGE) i;
	      
	      msg.str("");
	      msg.clear();
	      msg << ("\"" + AltiModule::SIGNAL_SOURCE_NAME[src] + "\" ") << std::setw(8) << std::left << (AltiModule::EQUALIZER_VOLTAGE_NAME[vlt]);
	      v_vector[i] = (unsigned int) (AltiCommon::enterFloat(msg.str(), 0, 2.5)/2.5*255.0);
            }
            if (alti->SIGEqualizersConfigWrite(src, v_vector[0], v_vector[1], v_vector[2], v_vector[3]) != AltiModule::SUCCESS) {
	      CERR("writing %s equalizer voltages", AltiModule::SIGNAL_SOURCE_NAME[src].c_str());
	      return (rtnv);
            }
	  }
	}
	else {
	  if ((rtnv = alti->SIGEqualizersEnableWrite(src, false)) != AltiModule::SUCCESS) {
	    CERR("disabling equalizer for %s", AltiModule::SIGNAL_SOURCE_NAME[src].c_str());
	    return (rtnv);
	  }
	}

        return (rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiSIGInputSyncHistogramsRead : public MenuItem {
  public:
    AltiSIGInputSyncHistogramsRead() { setName("read  IO histogram statistics"); }
    int action() {

        int rtnv(0);

        AltiModule::SYNCHRONIZER_INPUT_SIGNAL asyncIn;
        std::vector<unsigned int> edge_detected;

        printf("\n");
        printf("Histogram statistics:\n");
        printf("+-----------------------------------------------------------+\n");
        printf("| Asynchronous input signal |                               |\n");
        printf("+-----------------------------------------------------------+\n");
        printf("|                           | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |\n");
        printf("+-----------------------------------------------------------+\n");

        unsigned int i,j;
        for (i = 0; i < AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
            asyncIn = (AltiModule::SYNCHRONIZER_INPUT_SIGNAL) i;
            rtnv = (alti->SIGInputSyncHistogramsRead(asyncIn, edge_detected));//countPosByPhase, countNegByPhase));
            if (rtnv != AltiModule::SUCCESS) {
                CERR("reading sample histogram statistics for asynchronous input signal \"%s\"", AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
                return (rtnv);
            }
            else {
                printf("| [%02d]%-21s | ", asyncIn, AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
                for(j = 0; j < 8; j++){
                  printf("%d | ", edge_detected[j]);
                }
                printf("\n");
            }
            if ((asyncIn == AltiModule::SYNC_IN_TTR3) || (asyncIn == AltiModule::SYNC_IN_TTYP7) || (asyncIn == AltiModule::SYNC_IN_TTYP3) || (asyncIn == AltiModule::SYNC_IN_BGO3) || (asyncIn == AltiModule::SYNC_IN_ORB)) printf("+-----------------------------------------------------------+\n");
        }
        printf("+-----------------------------------------------------------+\n");

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGInputSyncHistogramsReset : public MenuItem {
  public:
    AltiSIGInputSyncHistogramsReset() { setName("reset IO histograms"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->SIGInputSyncHistogramsReset()) != AltiModule::SUCCESS) {
            CERR("resetting all input synchronization histograms", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGInputSyncStatusRead : public MenuItem {
  public:
    AltiSIGInputSyncStatusRead() { setName("read  IO sync   status"); }
    int action() {

        int rtnv(0);

        //table
        printf("\n");
        printf("+-----------------------------------+\n");//--------------------------+\n");
        printf("| Asynchronous input signal | Phase |\n");//     | Clock phase selected |\n");
        printf("+-----------------------------------+\n");//--------------------------+\n");

        //std::vector<bool> enabled;
        std::vector<unsigned int> phase;
        if ((rtnv = alti->SIGInputSyncEnableRead(phase)) != AltiModule::SUCCESS) {
            CERR("reading synchronization status for all asynchronous input signals", "");
            return (rtnv);
        }

        unsigned int i;
        AltiModule::SYNCHRONIZER_INPUT_SIGNAL asyncIn;
        for (i = 0; i < AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
            asyncIn = AltiModule::SYNCHRONIZER_INPUT_SIGNAL(i);
            printf("| [%02d]%-21s |     %-1d |\n", i, AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str(), phase[i]);
            if ((asyncIn == AltiModule::SYNC_IN_TTR3)) printf("+-----------------------------------+\n");
        }
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGInputSyncEnableWrite : public MenuItem {
  public:
    AltiSIGInputSyncEnableWrite() { setName("write IO sync   enable"); }
    int action() {

        int rtnv(0);

        bool all = (bool) enterInt("signal or all signals (0=single, 1=all) ", 0, 1);
        AltiModule::SYNCHRONIZER_INPUT_SIGNAL asyncIn;
        unsigned int phase;

	if(all) {
	  phase = enterInt("Clock phase: ", 0, 7);	  
	  if ((rtnv = alti->SIGInputSyncEnableWrite(phase)) != AltiModule::SUCCESS) {
	    CERR("writing clock phase %d for all input signals", phase);
	    return (rtnv);
	  }
	}
	else {
	  printf("-------------\n");
	  for (unsigned int i = 0; i < AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
            asyncIn = AltiModule::SYNCHRONIZER_INPUT_SIGNAL(i);
            std::printf("%02d-%s\n", i, AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
            if ((asyncIn == AltiModule::SYNC_IN_TTR3)) printf("-------------\n");
	  }
	  
	  asyncIn = (AltiModule::SYNCHRONIZER_INPUT_SIGNAL) enterInt("Single signal ", 0, AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NUMBER - 1);
	  phase = enterInt("Clock phase: ", 0, 7);
	  
	  if ((rtnv = alti->SIGInputSyncEnableWrite(asyncIn, phase)) != AltiModule::SUCCESS) {
	    CERR("writing clock phase %d for asynchronous input signal \"%s\"", phase, AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
	    return (rtnv);
	  }
        }
    
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGSyncShapeStatusRead : public MenuItem {
  public:
    AltiSIGSyncShapeStatusRead() { setName("read  IO shape  status"); }
    int action() {

        int rtnv(0);

        //table
        printf("\n");
        printf("+----------------------------------------------+\n");
        printf("| Asynchronous input signal |                  |\n");
        printf("+----------------------------------------------+\n");

        std::vector<unsigned int> status;
        if ((rtnv = alti->SIGSyncShapeSwitchRead(status)) != AltiModule::SUCCESS) {
            CERR("reading shaping status for all asynchronous input signals", "");
            return (rtnv);
        }

        unsigned int i;
        AltiModule::SYNCHRONIZER_INPUT_SIGNAL asyncIn;
        for (i = 0; i < AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
            asyncIn = AltiModule::SYNCHRONIZER_INPUT_SIGNAL(i);
            printf("| [%02d]%-21s | %-16s |\n", i, AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str(), AltiModule::SYNC_SHAPE_NAME[status[i]].c_str());
            if (asyncIn == AltiModule::SYNC_IN_TTR3) printf("+----------------------------------------------+\n");
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGSyncShapeEnableWrite : public MenuItem {
  public:
    AltiSIGSyncShapeEnableWrite() { setName("write IO shape  enable"); }
    int action() {

        int rtnv(0);
        int shape;
	bool enable;
        bool all = (bool) enterInt("Single signal or all signals (0=single, 1=all)", 0, 1);
        AltiModule::SYNCHRONIZER_INPUT_SIGNAL asyncIn;
     
	if(all) {
	  enable = (bool) enterInt("Enable shape (0-Sync, 1=Shape)                   ", 0, 1);
	  if (enable){
	    shape = enterInt("Shaped pulse length: \n",1,2);
	  } else {
	    shape = 0;
	  }
	  std::vector<unsigned int> v_shape;
	  for (unsigned i = 0; i < AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
	    v_shape.push_back(shape);
	  }	  
	  if ((rtnv = alti->SIGSyncShapeSwitchWrite(v_shape)) != AltiModule::SUCCESS) {
	    CERR("writing shaping value %d for all asynchronous input signal", shape);
	    return (rtnv);
	  }
	}
	else {
	  printf("-------------\n");
	  for (unsigned i = 0; i < AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NUMBER; i++) {
            asyncIn = AltiModule::SYNCHRONIZER_INPUT_SIGNAL(i);
            std::printf("%02d-%s\n", i, AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
            if (asyncIn == AltiModule::SYNC_IN_TTR3) printf("-------------\n");
	  }

	  asyncIn = (AltiModule::SYNCHRONIZER_INPUT_SIGNAL) enterInt("Single signal ", 0, AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NUMBER - 1);
            enable = (bool) enterInt("Enable shape (0-Sync, 1=Shape) ", 0, 1);
            if (enable){
              shape = enterInt("Shaped pulse length ",1,2);
            } else {
	      shape = 0;
	    }
            if (alti->SIGSyncShapeSwitchWrite(asyncIn, shape) != AltiModule::SUCCESS) {
	      CERR("writing shaping value %d for asynchronous input signal \"%s\"", shape, AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NAME[asyncIn].c_str());
	      return AltiModule::FAILURE;
            }
        }
       
        return (rtnv);
    }
};


//------------------------------------------------------------------------------

class AltiSIGMuxBGOOutputLEMORead : public MenuItem {
  public:
    AltiSIGMuxBGOOutputLEMORead() { setName("read  front panel BGO selection"); }
    int action() {

        int rtnv(0);

        bool bgo2enabled, bgo3enabled;
        if ((rtnv = alti->SIGMuxBGOOutputLEMORead(bgo2enabled, bgo3enabled)) != AltiModule::SUCCESS) {
            CERR("reading front panel output BGO selection", "");
            return (rtnv);
        }

        //table
        printf("\n");
        printf("+----------------------+\n");
        printf("| BGO2 FP output: BGO%d |\n", bgo2enabled ? 2 : 0);
        printf("| BGO3 FP output: BGO%d |\n", bgo3enabled ? 3 : 1);
        printf("+----------------------+\n");

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGMuxBGOOutputLEMOWrite : public MenuItem {
  public:
    AltiSIGMuxBGOOutputLEMOWrite() { setName("write front panel BGO selection"); }
    int action() {

        int rtnv(0);

        unsigned int bgo_fp_sel = enterInt("BGO2/3 FP output selection (0=\"BGO0/1\", 1=\"BGO0/3\", 2=\"BGO2/1\", 3=\"BGO2/3\")", 0, 3);
        bool bgo2enable = bgo_fp_sel & 0x2;
        bool bgo3enable = bgo_fp_sel & 0x1;
        if ((rtnv = alti->SIGMuxBGOOutputLEMOWrite(bgo2enable, bgo3enable)) != AltiModule::SUCCESS) {
            CERR("writing front panel output BGO selection: \"BGO%d\" and \"BGO%d\"", bgo2enable ? 2 : 0, bgo3enable ? 3 : 1);
            return (rtnv);
        }


        return (rtnv);
    }
};


//------------------------------------------------------------------------------

class AltiSIGMuxTTRInputLEMORead : public MenuItem {
  public:
    AltiSIGMuxTTRInputLEMORead() { setName("read  front panel TTR input selection"); }
    int action() {

        int rtnv(0);

	printf("\n");
	for (u_int bit = 0; bit < 3; bit++) {
	  AltiModule::CALREQ_INPUT input;
	  if ((rtnv = alti->CRQLocalSourceRead(bit, input)) != AltiModule::SUCCESS) {
            CERR("reading TTR lemo input selection", "");
            return (rtnv);
	  }
	  printf("TTR%d: %s\n", bit+1, (input == AltiModule::CALREQ_FROM_FRONT_PANEL) ? "CALREQ" : "TRT");
        }
        printf("\n");
	
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGOutputSelectRead : public MenuItem {
  public:
    AltiSIGOutputSelectRead() { setName("read  signals output selection"); }
    int action() {

        int rtnv(0);
	string output;
	printf("\n");
	if ((rtnv = alti->SIGOutputSelectL1aRead(output)) != AltiModule::SUCCESS) {
	  CERR("reading L1a output selection", "");
	  return (rtnv);
	}
	printf(" %11s : %10s  \n", "L1A", output.c_str());

	if ((rtnv = alti->SIGOutputSelectOrbitRead(output)) != AltiModule::SUCCESS) {
	  CERR("reading Orbit output selection", "");
	  return (rtnv);
	}
	printf(" %11s : %10s  \n", "Orbit", output.c_str());

	for (u_int bgo = 0; bgo < ALTI_CTL_OUTPUTSELECT_BITSTRING::BGO_NUMBER; bgo++) {
	  if ((rtnv = alti->SIGOutputSelectBgoRead(bgo, output)) != AltiModule::SUCCESS) {
	    CERR("reading Bgo-%d output selection", bgo);
	    return (rtnv);
	  }
	  printf(" %10s%d : %10s  \n", "BGo-", bgo, output.c_str());
	} 
	
	for (u_int ttr = 0; ttr < ALTI_CTL_OUTPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER; ttr++) {
	  if ((rtnv = alti->SIGOutputSelectTestTriggerRead(ttr, output)) != AltiModule::SUCCESS) {
	    CERR("reading TTR-%d output selection", ttr+1);
	    return (rtnv);
	  }
	  printf(" %10s%d : %10s  \n", "TTR-", ttr+1, output.c_str());
	} 

	if ((rtnv = alti->SIGOutputSelectTriggerTypeRead(output)) != AltiModule::SUCCESS) {
	  CERR("reading Trigger Type output selection", "");
	  return (rtnv);
	}
	printf(" %11s : %10s  \n", "TriggerType", output.c_str());
	
        printf("\n");

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGOutputSelectWrite : public MenuItem {
  public:
    AltiSIGOutputSelectWrite() { setName("write signals output selection"); }
    int action() {

        int rtnv(0);
	u_int output = 0;

	u_int signal = enterInt("Signal: 0=L1A, 1=Orbit, 2=BGo, 3=TTR, 4=TriggerType, 5=ALL",0 , 5);  

	if(signal == 0 || signal == 5) {
	  output = enterInt("L1A output selection (0=\"INACTIVE\", 1=\"EXTERNAL\", 2=\"PATTERN\", 3=\"MINICTP\", 4=\"CALIBSIGNAL\")", 0, 4);
	  if ((rtnv = alti->SIGOutputSelectL1aWrite(ALTI_CTL_OUTPUTSELECT_BITSTRING::L1A_TYPE_NAME[output])) != AltiModule::SUCCESS) {
	    CERR("writing L1a output selection", "");
	    return (rtnv);
	  }
	}

	if(signal == 1 || signal == 5) {
	  output = enterInt("Orbit output selection (0=\"INACTIVE\", 1=\"EXTERNAL\", 2=\"PATTERN\", 3=\"MINICTP\")", 0, 3);
	  if ((rtnv = alti->SIGOutputSelectOrbitWrite(ALTI_CTL_OUTPUTSELECT_BITSTRING::ORBIT_TYPE_NAME[output])) != AltiModule::SUCCESS) {
	    CERR("writing Orbit output selection", "");
	    return (rtnv);
	  }
	}

	if(signal == 2 || signal == 5) {
	  u_int bgo_choice = enterInt("BGo signal: 0=BGo-0, 1=BGo-1, 2=BGo-2, 3=BGo-3, 4=all)", 0, ALTI_CTL_OUTPUTSELECT_BITSTRING::BGO_NUMBER);
	  if(bgo_choice == ALTI_CTL_OUTPUTSELECT_BITSTRING::BGO_NUMBER)  {
	    output = enterInt("BGo output selection (0=\"INACTIVE\", 1=\"EXTERNAL\", 2=\"PATTERN\", 3=\"MINICTP\")", 0, 3);
	  }
	  for (u_int bgo = 0; bgo < ALTI_CTL_OUTPUTSELECT_BITSTRING::BGO_NUMBER; bgo++) {
	    if(bgo == bgo_choice || bgo_choice == ALTI_CTL_OUTPUTSELECT_BITSTRING::BGO_NUMBER)  {
	      string command = "BGo-" + to_string(bgo) + " output selection (0=\"INACTIVE\", 1=\"EXTERNAL\", 2=\"PATTERN\", 3=\"MINICTP\")";
	      output = enterInt(command.c_str(), 0, 3);	      
	      if ((rtnv = alti->SIGOutputSelectBgoWrite(bgo, ALTI_CTL_OUTPUTSELECT_BITSTRING::BGO_TYPE_NAME[output])) != AltiModule::SUCCESS) {
		CERR("writing BGo-%d output selection", bgo);
		return (rtnv);
	      }
	    }
	  }
	}

	if(signal == 3 || signal == 5) {	
	  u_int ttr_choice = enterInt("TTR signal: 0=TTR-1, 1=TTR-2, 2=TTR-3, 3=all)", 0, ALTI_CTL_OUTPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER);
	  if(ttr_choice == ALTI_CTL_OUTPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER)  {
	    output = enterInt("TTR output selection (0=\"INACTIVE\", 1=\"EXTERNAL\", 2=\"PATTERN\", 3=\"CALIBREQUEST\", 4=\"TURNSIGNAL\", 5=\"ORBIT\", 6=\"MINICTP\"", 0, 6);
	  }
	  for (u_int ttr = 0; ttr < ALTI_CTL_OUTPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER; ttr++) {
	    if((ttr == ttr_choice) || (ttr_choice == ALTI_CTL_OUTPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER))  {
	      string command = "TTR-" + to_string(ttr+1) + " output selection (0=\"INACTIVE\", 1=\"EXTERNAL\", 2=\"PATTERN\", 3=\"CALIBREQUEST\", 4=\"TURNSIGNAL\", 5=\"ORBIT\", 6=\"MINICTP\"";
	      output = enterInt(command.c_str(), 0, 6);	      
	      if ((rtnv = alti->SIGOutputSelectTestTriggerWrite(ttr, ALTI_CTL_OUTPUTSELECT_BITSTRING::TESTTRIGGER_TYPE_NAME[output])) != AltiModule::SUCCESS) {
		CERR("writing TTR-%d output selection", ttr);
		return (rtnv);
	      }
	    }
	  }
	}

	if(signal == 4 || signal == 5) {
	  output = enterInt("TriggerType output selection (0=\"INACTIVE\", 1=\"EXTERNAL\", 2=\"PATTERN\", 3=\"MINICTP\")", 0, 3);
	  if ((rtnv = alti->SIGOutputSelectTriggerTypeWrite(ALTI_CTL_OUTPUTSELECT_BITSTRING::TRIGGERTYPE_TYPE_NAME[output])) != AltiModule::SUCCESS) {
	    CERR("writing TriggerType output selection", "");
	    return (rtnv);
	  }
	}

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGOrbInputSelectRead : public MenuItem {
  public:
    AltiSIGOrbInputSelectRead() { setName("read  orbit input multiplexer selection"); }
    int action() {

        int rtnv(0);
	string input;
	printf("\n");
	if ((rtnv = alti->SIGInputSelectOrbitRead(input)) != AltiModule::SUCCESS) {
	  CERR("reading Orbit input multiplexer selection", "");
	  return (rtnv);
	}
	printf(" Orbit input multiplexer : %10s  \n", input.c_str());

        printf("\n");

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGOrbInputSelectWrite : public MenuItem {
  public:
    AltiSIGOrbInputSelectWrite() { setName("write orbit input multiplexer selection"); }
    int action() {

      int rtnv(0);
      u_int input = enterInt("ORB input multiplexer selection (0=\"EXTERNAL\", 1=\"PATTERN\", 2=\"MINICTP\")", 0, 2);
      if ((rtnv = alti->SIGInputSelectOrbitWrite(ALTI_CTL_INPUTSELECT_BITSTRING::ORBIT_TYPE_NAME[input])) != AltiModule::SUCCESS) {
	CERR("writing L1a output selection", "");
	return (rtnv);
      }
      
      return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiSIGTurnCounterOrbitSourceRead : public MenuItem {
  public:
  AltiSIGTurnCounterOrbitSourceRead() { setName("read  turn counter orbit source"); }
  int action() {
    
    int rtnv(0);
    
    std::string source;
    if ((rtnv = alti->SIGTurnCounterOrbitSourceRead(source)) != AltiModule::SUCCESS) {
      CERR("reading turn counter orbit source", "");
      return (rtnv);
    }
    printf("turn counter orbit source = %s\n", source.c_str());
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGTurnCounterOrbitSourceWrite : public MenuItem {
  public:
  AltiSIGTurnCounterOrbitSourceWrite() { setName("write turn counter orbit source"); }
  int action() {
    
    int rtnv(0);
    
    u_int source = enterInt("Turn counter orbit source (0=\"EXTERNAL\", 1=\"MINICTP\")", 0, 1);
    if ((rtnv = alti->SIGTurnCounterOrbitSourceWrite(ALTI_CTL_TURNSIGNAL_BITSTRING::ORBITSOURCE_TYPE_NAME[source])) != AltiModule::SUCCESS) {
      CERR("writing turn counter orbit source", "");
      return (rtnv);
    }
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGTurnCounterMaskRead : public MenuItem {
  public:
  AltiSIGTurnCounterMaskRead() { setName("read  turn counter mask"); }
  int action() {
    
    int rtnv(0);
    
    u_int mask;
    if ((rtnv = alti->SIGTurnCounterMaskRead(mask)) != AltiModule::SUCCESS) {
      CERR("reading turn counter mask", "");
      return (rtnv);
    }
    printf("turn counter mask = 0x%08x (= %10u)\n", mask, mask);
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGTurnCounterMaskWrite : public MenuItem {
public:
  AltiSIGTurnCounterMaskWrite() { setName("write turn counter mask"); }
  int action() {
    
    int rtnv(0);
    
    u_int mask = enterHex("Turn counter mask   ", 0, 0xffffffff); 
    if ((rtnv = alti->SIGTurnCounterMaskWrite(mask)) != AltiModule::SUCCESS) {
      CERR("writing turn counter mask", "");
      return (rtnv);
    }
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGTurnCounterFreqRead : public MenuItem {
public:
  AltiSIGTurnCounterFreqRead() { setName("read  turn counter frequency"); }
  int action() {
    
    int rtnv(0);
    
    u_int freq;
    if ((rtnv = alti->SIGTurnCounterFreqRead(freq)) != AltiModule::SUCCESS) {
      CERR("reading turn counter frequency", "");
      return (rtnv);
    }
    printf("turn counter frequency = %d orbits\n", freq);
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGTurnCounterFreqWrite : public MenuItem {
public:
  AltiSIGTurnCounterFreqWrite() { setName("write turn counter frequency"); }
  int action() {
    
    int rtnv(0);
    
    u_int freq = enterInt("Turn counter frequency [orbit]   ", 0, 31); 
    if ((rtnv = alti->SIGTurnCounterFreqWrite(freq)) != AltiModule::SUCCESS) {
      CERR("writing turn counter frequency", "");
      return (rtnv);
    }
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGTurnCounterReset : public MenuItem {
  public:
  AltiSIGTurnCounterReset() { setName("reset turn counter"); }
  int action() {
    
    int rtnv(0);
    
    if ((rtnv = alti->SIGTurnCounterReset()) != AltiModule::SUCCESS) {
      CERR("reseting turn counter value", "");
      return (rtnv);
    }
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGTurnCounterValueRead : public MenuItem {
public:
  AltiSIGTurnCounterValueRead() { setName("read  turn counter value"); }
  int action() {
    
    int rtnv(0);
    
    u_int cnt;
    if ((rtnv = alti->SIGTurnCounterValueRead(cnt)) != AltiModule::SUCCESS) {
      CERR("reading turn counter value", "");
      return (rtnv);
    }
    printf("turn counter value = %d\n", cnt);
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGBGo2L1aDelayRead : public MenuItem {
public:
  AltiSIGBGo2L1aDelayRead() { setName("read  BGo-2 L1A delay"); }
  int action() {
    
    int rtnv(0);
    
    u_int delay;
    if ((rtnv = alti->SIGBGo2L1aDelayRead(delay)) != AltiModule::SUCCESS) {
      CERR("reading BGo-2 L1A delay", "");
      return (rtnv);
    }
    printf("BGo-2 L1A delay = %d BCIDs\n", delay);
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGBGo2L1aDelayWrite : public MenuItem {
public:
  AltiSIGBGo2L1aDelayWrite() { setName("write BGo-2 L1A delay"); }
  int action() {
    
    int rtnv(0);
    
    u_int delay = enterInt("BGo-2 L1A delay [BCs]   ", 0, 1024); 
    if ((rtnv = alti->SIGBGo2L1aDelayWrite(delay)) != AltiModule::SUCCESS) {
      CERR("writing BGo-2 L1A delay", "");
      return (rtnv);
    }
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------

class AltiSIGBGo2L1aCounterRead : public MenuItem {
public:
  AltiSIGBGo2L1aCounterRead() { setName("read  BGo-2 L1A counter"); }
  int action() {
    
    int rtnv(0);
    
    u_int cnt;
    if ((rtnv = alti->SIGBGo2L1aCounterRead(cnt)) != AltiModule::SUCCESS) {
      CERR("reading BGo-2 L1A counter", "");
      return (rtnv);
    }
    printf("BGo-2 L1A counter = %d\n", cnt);
    
    return (rtnv);
  }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiBSYStatusRead : public MenuItem {
  public:
    AltiBSYStatusRead() { setName("status"); }
    int action() {

        int rtnv(0);
	printf("\n");
	std::cout << "Busy input selection:" << endl;
	std::vector<bool> selected;
        selected.resize(AltiModule::BUSY_INPUT_NUMBER);
        if ((rtnv = alti->BSYInputSelectRead(selected)) != AltiModule::SUCCESS) {
            CERR("reading busy input selection", "");
            return (rtnv);
        }
        unsigned int i;
        int total = 0;
        AltiModule::BUSY_INPUT input;
        for (i = 0; i < AltiModule::BUSY_INPUT_NUMBER; i++) {
            input = AltiModule::BUSY_INPUT(i);
            printf("[%02d]%-21s: %s\n", i, AltiModule::BUSY_INPUT_NAME[input].c_str(), selected[i] ? "Enabled" : "Disabled");
            total += selected[i];
        }
        printf("\n");
        printf("Local busy: ");
        if (total == 0) {
            printf("Inactive");
        }
        else {
            for (i = 0; i < AltiModule::BUSY_INPUT_NUMBER; i++) {
                input = AltiModule::BUSY_INPUT(i);
                printf("%s", selected[i] ? AltiModule::BUSY_INPUT_NAME[input].c_str() : "");
                total -= selected[i];
                printf("%s", (selected[i] && (total > 0)) ? " + " : "");
            }
        }
        printf("\n \n");

	std::cout << "Busy output selection:" << endl;
        std::vector<AltiModule::BUSY_SOURCE> src;
        src.resize(AltiModule::BUSY_OUTPUT_NUMBER);
        if ((rtnv = alti->BSYOutputSelectRead(src)) != AltiModule::SUCCESS) {
            CERR("reading busy output selection", "");
            return (rtnv);
        }

        AltiModule::BUSY_OUTPUT output;
        for (i = 0; i < AltiModule::BUSY_OUTPUT_NUMBER; i++) {
            output = AltiModule::BUSY_OUTPUT(i);
            printf(" [%02d]%-12s: %s\n", i, AltiModule::BUSY_OUTPUT_NAME[output].c_str(), AltiModule::BUSY_SOURCE_NAME[src[i]].c_str());
        }
        printf("\n");

	
	std::cout << "Busy gating of input signals:" << endl;
	bool l1a_en;
        bool ttr1,ttr2,ttr3;
        bool bgo0,bgo1,bgo2,bgo3;
        if ((rtnv = alti->BSYMaskingL1aEnableRead(l1a_en)) != AltiModule::SUCCESS) {
            CERR("reading busy masking of L1A", "");
            return (rtnv);
        }
        if ((rtnv = alti->BSYMaskingTTREnableRead(ttr1,ttr2,ttr3)) != AltiModule::SUCCESS) {
          CERR("reading busy masking of TTriggers", "");
          return (rtnv);
        }  
        if ((rtnv = alti->BSYMaskingBGoEnableRead(bgo0,bgo1,bgo2,bgo3)) != AltiModule::SUCCESS) {
          CERR("reading busy masking of BGos", "");
          return (rtnv);
        }
        printf("  L1A   : %s\n  TTR1  : %s\n  TTR2  : %s\n  TTR3  : %s\n  BGo0  : %s\n  BGo1  : %s\n  BGo2  : %s\n  BGo3  : %s",
        l1a_en ? "Enabled" : "Disabled",ttr1 ? "Enabled" : "Disabled",ttr2 ? "Enabled" : "Disabled",ttr3 ? "Enabled" : "Disabled",bgo0 ? "Enabled" : "Disabled",bgo1 ? "Enabled" : "Disabled",bgo2 ? "Enabled" : "Disabled",bgo3 ? "Enabled" : "Disabled");
        
	printf("\n");
	std::cout << "Busy gating of output PG signals:" << endl;
        if ((rtnv = alti->BSYMaskingPatL1aEnableRead(l1a_en)) != AltiModule::SUCCESS) {
            CERR("reading PRM busy masking of L1A", "");
            return (rtnv);
        }
        if ((rtnv = alti->BSYMaskingPatTTREnableRead(ttr1,ttr2,ttr3)) != AltiModule::SUCCESS) {
          CERR("reading PRM busy masking of TTriggers", "");
          return (rtnv);
        }  
        if ((rtnv = alti->BSYMaskingPatBGoEnableRead(bgo0,bgo1,bgo2,bgo3)) != AltiModule::SUCCESS) {
          CERR("reading PRM busy masking of BGos", "");
          return (rtnv);
        }
        printf("  L1A   : %s\n  TTR1  : %s\n  TTR2  : %s\n  TTR3  : %s\n  BGo0  : %s\n  BGo1  : %s\n  BGo2  : %s\n  BGo3  : %s",
        l1a_en ? "Enabled" : "Disabled",ttr1 ? "Enabled" : "Disabled",ttr2 ? "Enabled" : "Disabled",ttr3 ? "Enabled" : "Disabled",bgo0 ? "Enabled" : "Disabled",bgo1 ? "Enabled" : "Disabled",bgo2 ? "Enabled" : "Disabled",bgo3 ? "Enabled" : "Disabled");
        
	printf("\n");
        AltiBusyCounter cnt;	
        // read primary partition BUSY status
        if((rtnv = alti->BSYStatusRead(cnt)) != AltiModule::SUCCESS) {
	  CERR("reading BSY status","");
            return(rtnv);
        }        
        printf("\n");        
        cnt.dump();

	printf("\n\n");
	bool busy;
        if ((rtnv = alti->BSYConstLevelRegRead(busy)) != AltiModule::SUCCESS) {
	  CERR("reading constant level busy register", "");
	  return (rtnv);
        }	
        printf("Constant level busy register: %s\n", busy ? "Active" : "Inactive");

	printf("\n");
	AltiModule::BUSY_LEVEL level;
	if ((rtnv = alti->BSYFrontPanelLevelRead(level)) != AltiModule::SUCCESS) {
	  CERR("reading busy Front-Panel level", "");
	  return (rtnv);
        }
        printf("Level of Front-Panel: %s\n", AltiModule::BUSY_LEVEL_NAME[level].c_str());

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYInputSelectWrite : public MenuItem {
  public:
   
    AltiBSYInputSelectWrite() { setName("write Input selection"); }
    int action() {

        int rtnv(0);

        AltiModule::BUSY_INPUT input;
        unsigned int input_choice = enterInt("Input (0=\"FRONT_PANEL\", 1=\"CTP\", 2=\"ALTI\", 3=\"PG\", 4=\"VME\", 5=\"ECR\", 6=all)", 0, AltiModule::BUSY_INPUT_NUMBER - 1 + 1);
        if (input_choice < AltiModule::BUSY_INPUT_NUMBER) input = (AltiModule::BUSY_INPUT) input_choice;

        if (input_choice < AltiModule::BUSY_INPUT_NUMBER) { // 1 input
            std::printf("%-21s ", AltiModule::BUSY_INPUT_NAME[input].c_str());
            bool select = (bool) enterInt("enable (0=disable, 1=enable)                ", 0, 1);
            if ((rtnv = alti->BSYInputSelectWrite(input, select)) != AltiModule::SUCCESS) {
                CERR("%s busy input %s", select ? "enabling" : "disabling", AltiModule::BUSY_INPUT_NAME[input].c_str());
                return (rtnv);
            }
        }
        else { // all inputs
            std::vector<bool> select;
            select.clear();
            unsigned int i;
            for (i = 0; i < AltiModule::BUSY_INPUT_NUMBER; i++) {
                input = (AltiModule::BUSY_INPUT) i;
                std::printf("%-21s ", AltiModule::BUSY_INPUT_NAME[input].c_str());
                select.push_back((bool) enterInt("enable (0=disable, 1=enable)                ", 0, 1));
            }
            if ((rtnv = alti->BSYInputSelectWrite(select)) != AltiModule::SUCCESS) {
                CERR("writing selection for all busy inputs","");
                return (rtnv);
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYOutputSelectWrite : public MenuItem {
  public:
  
    AltiBSYOutputSelectWrite() { setName("write Output selection"); }
    int action() {

        int rtnv(0);

        AltiModule::BUSY_OUTPUT output;
        unsigned int output_choice = enterInt("Output (0=\"CTP\", 1=\"ALTI\", 2=all)                               ", 0, AltiModule::BUSY_OUTPUT_NUMBER - 1 + 1);
        if (output_choice < AltiModule::BUSY_OUTPUT_NUMBER) output = (AltiModule::BUSY_OUTPUT) output_choice;

        if (output_choice < AltiModule::BUSY_OUTPUT_NUMBER) { // 1 output
            std::printf("%-12s ", AltiModule::BUSY_OUTPUT_NAME[output].c_str());
            AltiModule::BUSY_SOURCE src = (AltiModule::BUSY_SOURCE) enterInt("source (0=\"Inactive\", 1=\"Local\", 2=\"CTP\", 3=\"ALTI\")", 0, AltiModule::BUSY_SOURCE_NUMBER - 1);
            if ((rtnv = alti->BSYOutputSelectWrite(output, src)) != AltiModule::SUCCESS) {
                CERR("selecting source %s for busy output %s", AltiModule::BUSY_SOURCE_NAME[src].c_str(), AltiModule::BUSY_OUTPUT_NAME[output].c_str());
                return (rtnv);
            }
        }
        else { // all outputs
            std::vector<AltiModule::BUSY_SOURCE> src;
            src.clear();
            unsigned int i;
            for (i = 0; i < AltiModule::BUSY_OUTPUT_NUMBER; i++) {
                output = (AltiModule::BUSY_OUTPUT) i;
                std::printf("%-12s ", AltiModule::BUSY_OUTPUT_NAME[output].c_str());
                src.push_back((AltiModule::BUSY_SOURCE) enterInt("source (0=\"Inactive\", 1=\"Local\", 2=\"CTP\", 3=\"ALTI\")", 0, AltiModule::BUSY_SOURCE_NUMBER - 1));
            }
            if ((rtnv = alti->BSYOutputSelectWrite(src)) != AltiModule::SUCCESS) {
                CERR("selecting sources for all busy ouputs","");
                return (rtnv);
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYMaskingWrite : public MenuItem {
  public:
    AltiBSYMaskingWrite() { setName("write input signals Busy gating"); }
    int action() {
      int rtnv = 0;

      unsigned int signal_pg_choice = enterInt("(0=\"L1A\", 1..3=\"TTR1..3\",  4..7=\"BGO0..3\", 9=ALL)", 0, 8);
      bool enable = (bool) enterInt("Enable (0=disable, 1=enable)                                         ", 0, 1);

      if (signal_pg_choice == 0 || signal_pg_choice == 8){
	if ((rtnv = alti->BSYMaskingL1aEnableWrite(enable)) != AltiModule::SUCCESS) {
	  CERR("%s busy gating of L1A", enable ? "enabling" : "disabling");
	  return (rtnv);
	}
      } 
      if ((signal_pg_choice >=1 && signal_pg_choice <= 3) || signal_pg_choice == 8)
	{
	  for(u_int i = 0; i < 3; i++) {
	    if(signal_pg_choice != 10 && i != (signal_pg_choice - 1)) continue;
	    if ((rtnv = alti->BSYMaskingTTREnableWrite(i, enable)) != AltiModule::SUCCESS) {
	      CERR("%s busy gating of \"%s\"", enable ? "enabling" : "disabling", AltiModule::SIGNAL_PG_NAME[(AltiModule::SIGNAL_PG) signal_pg_choice].c_str());
	      return (rtnv);
	    }
	  }
	}
      if(signal_pg_choice >= 4) 
        {
	  for(u_int i = 0; i < 4; i++) {
	    if(signal_pg_choice != 8 && i != (signal_pg_choice - 4)) continue;
	    if ((rtnv = alti->BSYMaskingBGoEnableWrite(i, enable)) != AltiModule::SUCCESS) {
	      CERR("%s busy gating of \"%s\"", enable ? "enabling" : "disabling", AltiModule::SIGNAL_PG_NAME[(AltiModule::SIGNAL_PG) signal_pg_choice].c_str());
	      return (rtnv);
	    }
	  }
	}
      
      return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYMaskingPatWrite : public MenuItem {
  public:
    AltiBSYMaskingPatWrite() { setName("write output PG signals Busy gating"); }
    int action() {
      int rtnv = 0;

      unsigned int signal_pg_choice = enterInt("(0=\"L1A\", 1..3=\"TTR1..3\",  4..7=\"BGO0..3\", 9=ALL)", 0, 8);
      bool enable = (bool) enterInt("Enable (0=disable, 1=enable)                                         ", 0, 1);

      if (signal_pg_choice == 0 || signal_pg_choice == 8){
	if ((rtnv = alti->BSYMaskingPatL1aEnableWrite(enable)) != AltiModule::SUCCESS) {
	  CERR("%s busy gating of L1A", enable ? "enabling" : "disabling");
	  return (rtnv);
	}
      } 
      if ((signal_pg_choice >=1 && signal_pg_choice <= 3) || signal_pg_choice == 8)
	{
	  for(u_int i = 0; i < 3; i++) {
	    if(signal_pg_choice != 10 && i != (signal_pg_choice - 1)) continue;
	    if ((rtnv = alti->BSYMaskingPatTTREnableWrite(i, enable)) != AltiModule::SUCCESS) {
	      CERR("%s busy gating of \"%s\"", enable ? "enabling" : "disabling", AltiModule::SIGNAL_PG_NAME[(AltiModule::SIGNAL_PG) signal_pg_choice].c_str());
	      return (rtnv);
	    }
	  }
	}
      if(signal_pg_choice >= 4) 
        {
	  for(u_int i = 0; i < 4; i++) {
	    if(signal_pg_choice != 8 && i != (signal_pg_choice - 4)) continue;
	    if ((rtnv = alti->BSYMaskingPatBGoEnableWrite(i, enable)) != AltiModule::SUCCESS) {
	      CERR("%s busy gating of \"%s\"", enable ? "enabling" : "disabling", AltiModule::SIGNAL_PG_NAME[(AltiModule::SIGNAL_PG) signal_pg_choice].c_str());
	      return (rtnv);
	    }
	  }
	}
      
      return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYConstLevelRegWrite : public MenuItem {
  public:

    AltiBSYConstLevelRegWrite() { setName("write Constant level register"); }
    int action() {

        int rtnv(0);

        bool busy = (bool) enterInt("Busy (0=inactive, 1=active)", 0, 1);
        if ((rtnv = alti->BSYConstLevelRegWrite(busy)) != AltiModule::SUCCESS) {
            CERR("writing %s to constant level busy register", busy ? "Active(1)" : "Inactive(0)");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYFrontPanelLevelWrite : public MenuItem {
  public:
   
    AltiBSYFrontPanelLevelWrite() { setName("write Front-Panel level"); }
    int action() {
        int rtnv(0);
	AltiModule::BUSY_LEVEL level = AltiModule::NIM;
        u_int level_choice = enterInt("Level (0=NIM, 1=TTL)", 0, 1);
	if (level_choice < AltiModule::BUSY_LEVEL_NUMBER) level = (AltiModule::BUSY_LEVEL) level_choice; 
        if ((rtnv = alti->BSYFrontPanelLevelWrite(level)) != AltiModule::SUCCESS) {
	  CERR("writing Busy Front-Panel level to %s", AltiModule::BUSY_LEVEL_NAME[level].c_str());
	  return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiCRQInputSyncHistogramsRead : public MenuItem {
  public:
    AltiCRQInputSyncHistogramsRead() { setName("read  IO histogram statistics"); }
    int action() {

        int rtnv(0);

        AltiModule::ASYNC_INPUT_CALREQ asyncIn;
        std::vector<unsigned int> edge_detected;

        printf("\n");
        printf("Histogram statistics:\n");
        printf("+-----------------------------------------------------------+\n");
        printf("| Asynchronous input signal |                               |\n");
        printf("+-----------------------------------------------------------+\n");
        printf("|                           | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |\n");
        printf("+-----------------------------------------------------------+\n");

        unsigned int i,j;
        for (i = 0; i < AltiModule::ASYNC_INPUT_CALREQ_NUMBER; i++) {
            asyncIn = (AltiModule::ASYNC_INPUT_CALREQ) i;
            rtnv = (alti->CRQInputSyncHistogramsRead(asyncIn, edge_detected));
            if (rtnv != AltiModule::SUCCESS) {
                CERR("reading sample histogram statistics for asynchronous input signal \"%s\"", AltiModule::ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
                return (rtnv);
            }
            else {
                printf("| [%02d]%-21s | ", asyncIn, AltiModule::ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
                for(j = 0; j < 8; j++){
                  printf("%d | ", edge_detected[j]);
                }
                printf("\n");
            }
            if ((asyncIn == AltiModule::CTP_CRQ2)) printf("+-----------------------------------------------------------+\n");
	    if ((asyncIn == AltiModule::ALTI_CRQ2)) printf("+-----------------------------------------------------------+\n");
        }
        printf("+-----------------------------------------------------------+\n");

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQInputSyncHistogramsReset : public MenuItem {
  public:
    AltiCRQInputSyncHistogramsReset() { setName("reset IO histograms"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->CRQInputSyncHistogramsReset()) != AltiModule::SUCCESS) {
            CERR("resetting all input synchronization histograms", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQInputSyncStatusRead : public MenuItem {
  public:
    AltiCRQInputSyncStatusRead() { setName("read  CALREQ sync   status"); }
    int action() {

        int rtnv(0);

        //table
        printf("\n");
        printf("+-----------------------------------+\n");
        printf("| Asynchronous input signal | Phase |\n");
        printf("+-----------------------------------+\n");

        std::vector<unsigned int> phase;
        if ((rtnv = alti->CRQInputSyncEnableRead(phase)) != AltiModule::SUCCESS) {
            CERR("reading synchronization status for all asynchronous input signals", "");
            return (rtnv);
        }

        unsigned int i;
        AltiModule::ASYNC_INPUT_CALREQ asyncIn;
        for (i = 0; i < AltiModule::ASYNC_INPUT_CALREQ_NUMBER; i++) {
            asyncIn = AltiModule::ASYNC_INPUT_CALREQ(i);
            printf("| [%02d]%-21s |     %-1d |\n", i, AltiModule::ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str(), phase[i]);
            if ((asyncIn == AltiModule::CTP_CRQ2)) printf("+-----------------------------------+\n");
	    if ((asyncIn == AltiModule::ALTI_CRQ2)) printf("+-----------------------------------+\n");
	    if ((asyncIn == AltiModule::RJ45_CRQ2)) printf("+-----------------------------------+\n");
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQInputSyncEnableWrite : public MenuItem {
  public:
    AltiCRQInputSyncEnableWrite() { setName("write CALREQ sync   enable"); }
    int action() {

        int rtnv(0);

        bool all = (bool) enterInt("Single signal or group (0=single, 1=all) ", 0, 1);
        AltiModule::ASYNC_INPUT_CALREQ asyncIn;
        bool enable = true;
        unsigned int phase;

	if (all) { // all signals
          phase = enterInt("Clock phase ", 0, 7);
          if ((rtnv = alti->CRQInputSyncEnableWrite(phase)) != AltiModule::SUCCESS) {
	    CERR("%s sync and selecting %01d clock phase for all asynchronous input signals", phase);
	    return (rtnv);
          }
	}
	else {
	  printf("-------------\n");
	  for (unsigned int i = 0; i < AltiModule::ASYNC_INPUT_CALREQ_NUMBER; i++) {
            asyncIn = AltiModule::ASYNC_INPUT_CALREQ(i);
            std::printf("%02d-%s\n", i, AltiModule::ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
            if ((asyncIn == AltiModule::ALTI_CRQ2)) printf("-------------\n");
	  }
	  asyncIn = (AltiModule::ASYNC_INPUT_CALREQ) enterInt("Single signal ", 0, AltiModule::ASYNC_INPUT_CALREQ_NUMBER - 1);
	  phase = enterInt("Clock phase: ", 0, 7);
	  if ((rtnv = alti->CRQInputSyncEnableWrite(asyncIn, phase)) != AltiModule::SUCCESS) {
	    CERR("%s sync and selecting %01d clock phase for asynchronous input signal \"%s\"", enable ? "enabling" : "disabling", phase, AltiModule::ASYNC_INPUT_CALREQ_NAME[asyncIn].c_str());
	    return (rtnv);
	  }
        }
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQRJ45InputShapeRead : public MenuItem {
  public:
    AltiCRQRJ45InputShapeRead() { setName("read  CALREQ RJ45 synchronisation / shape "); }
    int action() {
        AltiModule::SYNC_SHAPE val;
        int rtnv(0);
        unsigned int i;
        for (i=0; i<3; i++) {
          if ((rtnv = alti->CRQRJ45ShapeEnableRead(i,val)) != AltiModule::SUCCESS){
            CERR("reading CALREQ RJ45 shape register", "");
            return (rtnv);
          }
          printf(" RJ45(%d) = \"%s\"",i,AltiModule::SYNC_SHAPE_NAME[val].c_str());
          printf("\n");
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQRJ45InputShapeWrite : public MenuItem {
  public:
    AltiCRQRJ45InputShapeWrite() { setName("write CALREQ RJ45 synchronisation / shape "); }
    int action() {
        AltiModule::SYNC_SHAPE val;
        int rtnv(0);
        unsigned int i;
        for (i=0; i<3; i++) {
          printf(" RJ45(%d) = ",i);
          val = (AltiModule::SYNC_SHAPE) enterInt("(0-Disabled, 1..2-Valid pulse lengths)",0, 2);
          if ((rtnv = alti->CRQRJ45ShapeEnableWrite(i,val)) != AltiModule::SUCCESS){
            CERR("reading CALREQ RJ45 shape register", "");
            return (rtnv);
          }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQLocalSourceRead : public MenuItem {
  public:
    AltiCRQLocalSourceRead() { setName("read  CALREQ local source"); }
    int action() {

        int rtnv(0);

	printf("\n");
        AltiModule::CALREQ_INPUT input;
        for(u_int creq=0; creq < 3; creq++) { 
	  
	  if ((rtnv = alti->CRQLocalSourceRead(creq, input)) != AltiModule::SUCCESS) {
            CERR("reading CALREQ-%d local source", creq);
            return (rtnv);
	  }
	  
	  printf("CALREQ-%d: %s\n", creq, AltiModule::CALREQ_INPUT_NAME[input].c_str());
        }
        printf("\n");
	
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQLocalSourceWrite : public MenuItem {
  public:
    AltiCRQLocalSourceWrite() { setName("write CALREQ local source"); }
    int action() {

        int rtnv(0);

        unsigned int bit_choice = enterInt("CALREQ bit (0=\"CALREQ-0\", 1=\"CALREQ-1\", 2=\"CALREQ-2\", 3-all)      ", 0, 3);

        AltiModule::CALREQ_INPUT input = (AltiModule::CALREQ_INPUT) enterInt("Input (0=\"RJ45\", 1=\"FRONT_PANEL\", 2=\"CTP\", 3=\"ALTI\", 4=\"PG\", 5=\"VME\")", 0, AltiModule::CALREQ_INPUT_NUMBER - 1);

        if (bit_choice == 3) { // all bits
	  for(u_int creq=0; creq < 3; creq++) { 
	    if ((rtnv = alti->CRQLocalSourceWrite(creq, input)) != AltiModule::SUCCESS) {
	      CERR("writing CALREQ-%d local source", creq);
	      return (rtnv);
            }
	  }
	}
	else { // 1 bit
	  if ((rtnv = alti->CRQLocalSourceWrite(bit_choice, input)) != AltiModule::SUCCESS) {
	    CERR("writing CALREQ-%d local source", bit_choice);
	    return (rtnv);
	  }
        }
	
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQOutputSelectRead : public MenuItem {
  public:
    AltiCRQOutputSelectRead() { setName("read  CALREQ output selection"); }
    int action() {

        int rtnv(0);

        std::vector<AltiModule::CALREQ_SOURCE> src;
        src.resize(3);

        printf("\n");
        printf("+-------------------------------------------------------------------------+\n");
        printf("| Output            | CALREQ-2        | CALREQ-1        | CALREQ-0        |\n");
        printf("+-------------------------------------------------------------------------+\n");

        unsigned int i;
        AltiModule::CALREQ_OUTPUT output;
        for (i = 0; i < AltiModule::CALREQ_OUTPUT_NUMBER; i++) {
            output = AltiModule::CALREQ_OUTPUT(i);
            if ((rtnv = alti->CRQOutputSelectRead(output, src)) != AltiModule::SUCCESS) {
                CERR("reading CALREQ output selection for %s", AltiModule::CALREQ_OUTPUT_NAME[output].c_str());
                return (rtnv);
            }
            printf("| [%d]%-14s | %-15s | %-15s | %-15s |\n", i, AltiModule::CALREQ_OUTPUT_NAME[output].c_str(), AltiModule::CALREQ_SOURCE_NAME[src[2]].c_str(),  AltiModule::CALREQ_SOURCE_NAME[src[1]].c_str(),  AltiModule::CALREQ_SOURCE_NAME[src[0]].c_str());
            printf("+-------------------------------------------------------------------------+\n");
        }
        printf("\n");

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQOutputSelectWrite : public MenuItem {
  public:
    AltiCRQOutputSelectWrite() { setName("write CALREQ output selection"); }
    int action() {

        int rtnv(0);

        unsigned int bit;
        unsigned int bit_choice = enterInt("CALREQ bit (0=\"CALREQ[0]\", 1=\"CALREQ[1]\", 2=\"CALREQ[2]\", 3-all)", 0, 3);
        if (bit_choice < 3) bit = bit_choice;

        AltiModule::CALREQ_OUTPUT output;
        unsigned int output_choice = enterInt("Output (0=\"CTP\", 1=\"ALTI\", 2=all)                              ", 0, AltiModule::CALREQ_OUTPUT_NUMBER - 1 + 1);
        if (output_choice < AltiModule::CALREQ_OUTPUT_NUMBER) output = (AltiModule::CALREQ_OUTPUT) output_choice;

        AltiModule::CALREQ_SOURCE src = (AltiModule::CALREQ_SOURCE) enterInt("Source (0=\"Inactive\", 1=\"Local\", 2=\"CTP\", 3=\"ALTI\")            ", 0, AltiModule::CALREQ_SOURCE_NUMBER - 1);

        if ((bit_choice < 3) && output_choice < AltiModule::CALREQ_OUTPUT_NUMBER) { // 1 output, 1 bit
            if ((rtnv = alti->CRQOutputSelectWrite(output, bit, src)) != AltiModule::SUCCESS) {
                CERR("selecting source %s for creq[%d] output %s", AltiModule::CALREQ_SOURCE_NAME[src].c_str(), bit, AltiModule::CALREQ_OUTPUT_NAME[output].c_str());
                return (rtnv);
            }
        }
        else if (bit_choice < 3) { // all outputs, 1 bit
            if ((rtnv = alti->CRQOutputSelectWrite(bit, src)) != AltiModule::SUCCESS) {
                CERR("selecting sources for all creq[%d] outputs", bit);
                return (rtnv);
            }
        }
        else if (output_choice < AltiModule::CALREQ_OUTPUT_NUMBER) { // 1 output, all bits
            if ((rtnv = alti->CRQOutputSelectWrite(output, src)) != AltiModule::SUCCESS) {
                CERR("selecting sources for creq[2:0] output %s", AltiModule::CALREQ_OUTPUT_NAME[output].c_str());
                return (rtnv);
            }
        }
        else { // all outputs, all bits
            if ((rtnv = alti->CRQOutputSelectWrite(src)) != AltiModule::SUCCESS) {
                CERR("selecting sources for all creq[2:0] outputs", "");
                return (rtnv);
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQConstLevelRegRead : public MenuItem {
  public:
    AltiCRQConstLevelRegRead() { setName("read  CALREQ constant level register"); }
    int action() {

        int rtnv(0);

        bool creq;
        printf("\n");
	for(u_int i=0; i<ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER; i++) {
	  if ((rtnv = alti->CRQConstLevelRegRead(i, creq)) != AltiModule::SUCCESS) {
	    CERR("reading CALREQ-%d const level register configuration", i); 
	    return (rtnv);
	  }
	  printf("CRQ-%d: %7s \n", i, creq ? "ACTIVE" : "INACTIVE" );
	}
        printf("\n");

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCRQConstLevelRegWrite : public MenuItem {
  public:
    AltiCRQConstLevelRegWrite() { setName("write CALREQ constant level register"); }
    int action() {

        int rtnv(0);

	for(u_int i=0; i<ALTI_CRQ_CONTROL_BITSTRING::VMECALREQUEST_NUMBER; i++) {
	  std::string command = "Constant level register for CALREQ-" + to_string(i) + " (0=inactive, 1=active)";
	  bool creq = (bool) enterInt(command.c_str(), 0, 1);
	  if ((rtnv = alti->CRQConstLevelRegWrite(i, creq)) != AltiModule::SUCCESS) {
            CERR("writing CALREQ-%d constant level register", i);
            return (rtnv);
	  }
	}
        return (rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiPATStatusRead : public MenuItem {
  public:
    AltiPATStatusRead() { setName("status"); }
    int action() {

      std::string mode;
       if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	 CERR("reading PRM mode", "");
	 return AltiModule::FAILURE;
       }
       if(mode == "SNAPSHOT") {
	 printf("Shared memory mode: SNAPSHOT");
	 return AltiModule::SUCCESS;
       }
       else {
	 std::printf("\n");
	 std::printf("Shared memory mode: PATTERN");
       }

       bool generationEnabled;
       if (alti->PRMEnableRead(generationEnabled) != AltiModule::SUCCESS) {
	 CERR("reading pattern generation enable", "");
	 return AltiModule::FAILURE;
       }
       
       bool generationRepeated;
       if (alti->PRMRepeatRead(generationRepeated) != AltiModule::SUCCESS) {
	 CERR("reading pattern generation repeat", "");
	 return AltiModule::FAILURE;
       }
        
       std::printf("\n");
       std::printf("Pattern generation: %s, %s\n", generationEnabled ? ("Enabled") : ("Disabled"), generationRepeated ? ("Repeated") : ("OneShot"));
       
       bool mem_full;
       std::string status;
       if (alti->PRMStatusRead(mem_full, status) != AltiModule::SUCCESS) {
	 CERR("reading pattern generation status", "");
	 return AltiModule::FAILURE;
       }
       std::printf("Memory: %s\n", mem_full ? "full" : "not full");
       if(status == "VME") std::printf("Status: disabled \n");
       else if(status == "ARMED") std::printf("Status: armed - waiting for trigger \n");
       else if(status == "PATTERN") std::printf("Status: pattern generator running \n");
       else if(status == "SNAPSHOT") std::printf("Status: snapshot running \n");
       else if(status == "DONE") std::printf("Status: done \n");

       unsigned int start;
       if (alti->PRMStartAddressRead(start) != AltiModule::SUCCESS) {
	 CERR("reading pattern generation start address", "");
	 return AltiModule::FAILURE;
       }
       unsigned int stop;
       if (alti->PRMStopAddressRead(stop) != AltiModule::SUCCESS) {
	 CERR("reading pattern generation stop address", "");
	 return AltiModule::FAILURE;
       }
       std::printf("\n");
       std::printf("Start address = 0x%08x (= %10u)\n", start, start);
       std::printf("Stop  address = 0x%08x (= %10u)\n", stop, stop);
       std::printf("\n");
       
       return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATSetModePattern : public MenuItem {
  public:
    AltiPATSetModePattern() { setName("set       shared memory in mode PATTERN"); }
    int action() {

       if (alti->PRMModeWrite("PATTERN") != AltiModule::SUCCESS) {
	 CERR("writing PRM mode", "");
	 return AltiModule::FAILURE;
       }
      
    return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------


class AltiPATGenerationEnable : public MenuItem {
  public:
    AltiPATGenerationEnable() { setName("enable    pattern generation"); }
    int action() {
        
      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }
      
      if (alti->PRMEnableWrite(true) != AltiModule::SUCCESS) {
	CERR("enabling pattern generation", "");
	return AltiModule::FAILURE;
      }
      
        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATGenerationDisable : public MenuItem {
  public:
    AltiPATGenerationDisable() { setName("disable   pattern generation"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }
      
      if (alti->PRMEnableWrite(false) != AltiModule::SUCCESS) {
	CERR("disabling pattern generation", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATGenerationRepeatWrite : public MenuItem {
  public:
    AltiPATGenerationRepeatWrite() { setName("repeat    pattern generation"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }
      
      bool repeat = (bool) enterInt("Enable repeated generation (0=\"OneShot\", 1=\"Repeated\")", 0, 1);
      
      if (alti->PRMRepeatWrite(repeat) != AltiModule::SUCCESS) {
	CERR("%s repeated generation from pattern memory", repeat ? "enabling" : "disabling");
	return AltiModule::FAILURE;
      }
      
        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATGenerationStartAddressWrite : public MenuItem {
  public:
    AltiPATGenerationStartAddressWrite() { setName("write     pattern generation start address"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }
      
      unsigned int start;
      start = enterHex("start address", 0, AltiModule::MAX_MEMORY - 1);
      if (alti->PRMStartAddressWrite(start) != AltiModule::SUCCESS) {
	CERR("writing pattern generation start address", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATGenerationStopAddressWrite : public MenuItem {
  public:
    AltiPATGenerationStopAddressWrite() { setName("write     pattern generation stop  address"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }
      
      unsigned int stop;
      stop = enterHex("stop address", 0, AltiModule::MAX_MEMORY - 1);
      if (alti->PRMStopAddressWrite(stop) != AltiModule::SUCCESS) {
	CERR("writing pattern generation stop address", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATGenerationTrigCondRead : public MenuItem {
  public:
    AltiPATGenerationTrigCondRead() { setName("read      pattern generation trigger condition"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }   
      
      unsigned int ttype;
      unsigned int i;
      bool enable, repeat;
      AltiModule::SIGNAL_PG signal;
      if (alti->PRMTriggerEnableRead(enable) != AltiModule::SUCCESS) {
	CERR("reading trigger condition enable", "");
	return AltiModule::FAILURE;
      }
      if (enable){
	if (alti->PRMTriggerRepeatRead(repeat) != AltiModule::SUCCESS) {
	  CERR("reading trigger condition repeat", "");
	  return AltiModule::FAILURE;
	}
      	printf(" Trigger Mode: Enabled - %s \n", repeat ? "repeat" : "one-shot");
	for (i = 0; i < AltiModule::SIGNAL_PG_NUMBER; i++){
	  signal = AltiModule::SIGNAL_PG(i);
	  if (alti->PRMTriggerSignalRead(signal, enable) != AltiModule::SUCCESS) {
	    CERR("reading snapshot trigger condition for signal %s", AltiModule::SIGNAL_PG_NAME[i].c_str());
	    return AltiModule::FAILURE;
	  }
	  printf("%-7s : %s \n", AltiModule::SIGNAL_PG_NAME[i].c_str(), enable ? "ENABLED" : "DISABLED");
	}
	
	if (alti->PRMTriggerTypeRead(ttype) != AltiModule::SUCCESS) {
	  CERR("reading snapshot trigger condition for ttyp", "");
	  return AltiModule::FAILURE;
	}
	
	printf("TTYPE   : 0x%02x\n",ttype);
      }
      else {          
	printf("Trigger Mode: Disabled \n");
      }
   
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATGenerationTrigCondWrite : public MenuItem {
  public:
    AltiPATGenerationTrigCondWrite() { setName("write     pattern generation trigger condition"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }
      
      unsigned int ttype;
      unsigned int i;
      bool enable, repeat;
      AltiModule::SIGNAL_PG signal;
      string command;
      
      enable = (bool) enterInt("Enable trigger condition for snapshot: 0=NO, 1=YES", 0, 1);
      
      if (alti->PRMTriggerEnableWrite(enable) != AltiModule::SUCCESS) {
	CERR("writing trigger condition enable", "");
	return AltiModule::FAILURE;
      }
      if (enable){
	printf(" Trigger Mode: Enabled \n");
	repeat = (bool) enterInt("Repeat trigger: 0=NO, 1=YES", 0, 1);
      
	if (alti->PRMTriggerRepeatWrite(repeat) != AltiModule::SUCCESS) {
	  CERR("writing trigger condition repeat", "");
	  return AltiModule::FAILURE;
	}
	for (i = 0; i < AltiModule::SIGNAL_PG_NUMBER; i++){
	  signal = AltiModule::SIGNAL_PG(i);
	  command = "Enable trigger condition for " + AltiModule::SIGNAL_PG_NAME[i] + ": 0=NO, 1=YES";
	  enable = (bool) enterInt(command.c_str(), 0, 1);
	  if (alti->PRMTriggerSignalWrite(signal, enable) != AltiModule::SUCCESS) {
	    CERR("writing snapshot trigger condition for signal %s", AltiModule::SIGNAL_PG_NAME[i].c_str());
	    return AltiModule::FAILURE;
	  }
	}
	
	ttype =  enterInt("TTYP trigger condition for snapshot: 0=NO, 1=YES", 0, 0xff);
	if (alti->PRMTriggerTypeWrite(ttype) != AltiModule::SUCCESS) {
	  CERR("writing snapshot trigger condition for ttyp", "");
	  return AltiModule::FAILURE;
	}
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATRead : public MenuItem {
  public:
    AltiPATRead() { setName("read      pattern generation memory"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }

      if (alti->PRMRead(segment[0]) != AltiModule::SUCCESS) {
	CERR("reading pattern generation memory. If the pattern generator is still enabled, disable it before reading the memory", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATReadFile : public MenuItem {
  public:
    AltiPATReadFile() { setName("read      pattern generation memory [into file]"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }

      std::string fn = enterString("File name");
      std::ofstream outf(fn.c_str(), std::ofstream::out);
      if (!outf) {
	CERR("cannot open output file \"%s\"", fn.c_str());
	return AltiModule::FAILURE;
      }
      
      if (alti->PRMReadFile(segment[0], outf) != AltiModule::SUCCESS) {
	CERR("reading pattern generation memory. If the pattern generator is still enabled, disable it before reading the memory", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiPATWriteFile : public MenuItem {
  public:
    AltiPATWriteFile() { setName("write     pattern generation memory [from file]"); }
    int action() {
      
      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "SNAPSHOT") {
	printf("Shared memory in SNAPSHOT mode: do nothing");
	return AltiModule::SUCCESS;
      }
      
      std::string fn = enterString("File name");
      std::ifstream inf(fn.c_str(), std::ifstream::in);
      if(!inf) {
	CERR("cannot open input file \"%s\"", fn.c_str());
	return AltiModule::FAILURE;
      }
      
      if (alti->PRMWriteFile(segment[0], inf) != AltiModule::SUCCESS) {
	CERR("writing pattern generation memory from file. If the pattern generator is still enabled, disable it before writing to the memory", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------


class AltiMEMDataPATSNPReadNumber : public MenuItem {
  public:
    AltiMEMDataPATSNPReadNumber() { setName("read    memory [number]"); }
    int action() {
      
      unsigned int indx, data;
      indx = enterHex("address index", 0, AltiModule::MAX_MEMORY - 1);
      if (alti->MEMDataRead(AltiModule::ALTI_RAM_MEMORY::PATTERN_SNAPSHOT, data, indx) != AltiModule::SUCCESS) {
	CERR("reading snapshot/pattern memory", "");
	return AltiModule::FAILURE;
      }
      
      std::printf("\n");
      std::printf("data[%6d] = 0x%08x (= %10u)\n", indx, data, data);
      std::printf("\n");
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMEMDataPATSNPWriteNumber : public MenuItem {
  public:
    AltiMEMDataPATSNPWriteNumber() { setName("write   memory [number]"); }
    int action() {
      
      unsigned int indx, data;
      indx = enterHex("address index", 0, AltiModule::MAX_MEMORY - 1);
      data = enterHex("data         ", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
      if (alti->MEMDataWrite(AltiModule::ALTI_RAM_MEMORY::PATTERN_SNAPSHOT, data, indx) != AltiModule::SUCCESS) {
	CERR("writing snapshot/pattern memory", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMEMDataPATSNPReadVector : public MenuItem {
public:
  AltiMEMDataPATSNPReadVector() { setName("read    memory [vector]"); }
  int action() {
   
    std::vector<unsigned int> data;
    
    unsigned int vme_off = enterHex("starting index", 0, AltiModule::MAX_MEMORY - 1);
    unsigned int size = enterHex("size          ", 1, AltiModule::MAX_MEMORY - vme_off);
    data.resize(size);
    
    if (alti->MEMDataRead(AltiModule::ALTI_RAM_MEMORY::PATTERN_SNAPSHOT, data, vme_off) != AltiModule::SUCCESS) {
      CERR("reading snapshot/pattern memory", "");
      return AltiModule::FAILURE;
    }
    
    unsigned int indx;
    for (indx = 0; indx < size; indx++) {
      std::printf("data[%6d] = 0x%08x (= %10u)\n", vme_off + indx, data[indx], data[indx]);
    }
    
    return AltiModule::SUCCESS;
  }
};

//------------------------------------------------------------------------------

class AltiMEMDataPATSNPWriteVector : public MenuItem {
  public:
    AltiMEMDataPATSNPWriteVector() { setName("write   memory [vector]"); }
    int action() {
      
      std::vector<unsigned int> data;
      
      unsigned int vme_off = enterHex("starting index ", 0, AltiModule::MAX_MEMORY - 1);
      unsigned int size = enterHex("size           ", 1, AltiModule::MAX_MEMORY - vme_off);
      unsigned int sdat = enterHex("starting value ", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
      unsigned int idat = enterHex("increment value", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
      unsigned int xdat(sdat), indx;
      
      for (indx = 0; indx < size; indx++) {
	data.push_back(xdat & ALTI::MASK_MEM_SHAREDALTIRAM);
	xdat += idat;
      }
      
      if (alti->MEMDataWrite(AltiModule::ALTI_RAM_MEMORY::PATTERN_SNAPSHOT, data, vme_off) != AltiModule::SUCCESS) {
	CERR("writing snapshot/pattern memory", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMEMDataPATSNPReadBlock : public MenuItem {
  public:
    AltiMEMDataPATSNPReadBlock() { setName("read    memory [block]"); }
    int action() {
      
      if (alti->MEMDataRead(AltiModule::ALTI_RAM_MEMORY::PATTERN_SNAPSHOT, segment[0], AltiModule::MAX_MEMORY, 0, 0) != AltiModule::SUCCESS) {
	CERR("reading snapshot/pattern memory in block transfer", "");
	return AltiModule::FAILURE;
      }
      
      unsigned int indx;
      unsigned int *data = (unsigned int *) segment[0]->VirtualAddress();
      for (indx = 0; indx < AltiModule::MAX_MEMORY; indx++) {
	std::printf("data[%6d] = 0x%08x (= %10u)\n", indx, data[indx], data[indx]);
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMEMDataPATSNPWriteBlock : public MenuItem {
  public:
    AltiMEMDataPATSNPWriteBlock() { setName("write   memory [block]"); }
    int action() {
      
      unsigned int sdat = enterHex("starting  value", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
      unsigned int idat = enterHex("increment value", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
      unsigned int xdat(sdat), indx;
      unsigned int* data = (unsigned int*) segment[0]->VirtualAddress();
      
      for (indx = 0; indx < AltiModule::MAX_MEMORY; indx++) {
	data[indx] = xdat & ALTI::MASK_MEM_SHAREDALTIRAM;
	xdat += idat;
      }
      
      if (alti->MEMDataWrite(AltiModule::ALTI_RAM_MEMORY::PATTERN_SNAPSHOT, segment[0], AltiModule::MAX_MEMORY, 0, 0) != AltiModule::SUCCESS) {
	CERR("writing snapshot/pattern memory in block transfer", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiSNPStatusRead : public MenuItem {
  public:
    AltiSNPStatusRead() { setName("status"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "PATTERN") {
	printf("Shared memory mode: PATTERN");
	 return AltiModule::SUCCESS;
      }
      else {
	std::printf("\n");
	std::printf("Shared memory mode: SNAPSHOT");
      }

        //table
        printf("\n");
        printf("+--------------------------+\n");
        printf("| Signal      | Enabled    |\n");
        printf("+--------------------------+\n");

        unsigned int i;
        AltiModule::SIGNAL signal;
	bool masked;
        for (i = 1; i < AltiModule::SIGNAL_NUMBER; i++) {
	  signal = AltiModule::SIGNAL(i);
	  if (alti->PRMSnapshotSignalMaskRead(signal, masked) != AltiModule::SUCCESS) {
	    CERR("reading snapshot signal mask", "");
	    return AltiModule::FAILURE;
	  }
	
	  printf("| [%02d]%-7s | %-10s |\n", i, AltiModule::SIGNAL_NAME[signal].c_str(), masked ? "enabled" : "disabled");
	}
	printf("+--------------------------+\n");
	
        bool snapshotEnabled;
	if (alti->PRMEnableRead(snapshotEnabled) != AltiModule::SUCCESS) {
	  CERR("reading snapshot enable", "");
	  return AltiModule::FAILURE;
	}	
        std::printf("\n");
        std::printf("Snapshot: %s\n", snapshotEnabled ? ("Enabled") : ("Disabled"));
	
	bool mem_full;
	std::string status;
	if (alti->PRMStatusRead(mem_full, status) != AltiModule::SUCCESS) {
	  CERR("reading snapshot status", "");
	  return AltiModule::FAILURE;
	}
	std::printf("Memory: %s\n", mem_full ? "full" : "not full");
	if(status == "VME") std::printf("Status: disabled \n");
	else if(status == "ARMED") std::printf("Status: armed - waiting for trigger \n");
	else if(status == "PATTERN") std::printf("Status: pattern generator running \n");
	else if(status == "SNAPSHOT") std::printf("Status: snapshot running \n");
	else if(status == "DONE") std::printf("Status: done \n");
	
        unsigned int cur;
        if (alti->PRMCurrentAddressRead(cur) != AltiModule::SUCCESS) {
	  CERR("reading snapshot current address", "");
	  return AltiModule::FAILURE;
        }
        std::printf("Current address = 0x%08x (= %10u)\n", cur, cur);
        std::printf("\n");
	
        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiSNPSetModeSnapshot : public MenuItem {
  public:
    AltiSNPSetModeSnapshot() { setName("set     shared memory in mode SNAPSHOT"); }
    int action() {

      std::string mode;
       if (alti->PRMModeWrite("SNAPSHOT") != AltiModule::SUCCESS) {
	 CERR("writing PRM mode", "");
	 return AltiModule::FAILURE;
       }
      
    return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiSNPEnable : public MenuItem {
  public:
    AltiSNPEnable() { setName("enable  snapshot"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "PATTERN") {
	printf("Shared memory in PATTERN mode: do nothing");
	return AltiModule::SUCCESS;
      }
      
      if (alti->PRMStartAddressWrite(0) != AltiModule::SUCCESS) {
	CERR("writing pattern generation start address", "");
	return AltiModule::FAILURE;
      }
      if (alti->PRMStopAddressWrite(0xfffff) != AltiModule::SUCCESS) {
	CERR("writing pattern generation start address", "");
	return AltiModule::FAILURE;
      }
      
      if(alti->PRMReset() != AltiModule::SUCCESS){
	CERR("resetting the snapshot memory","");
	return AltiModule::FAILURE;
      }
      
      if (alti->PRMEnableWrite(true) != AltiModule::SUCCESS) {
	CERR("enabling snapshot memory", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
      
    }
};

//------------------------------------------------------------------------------

class AltiSNPDisable : public MenuItem {
public:
  AltiSNPDisable() { setName("disable snapshot"); }
  int action() {
    
    std::string mode;
    if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
      CERR("reading PRM mode", "");
      return AltiModule::FAILURE;
    }
    if(mode == "PATTERN") {
      printf("Shared memory in PATTERN mode: do nothing");
      return AltiModule::SUCCESS;
    }
    
    if (alti->PRMEnableWrite(false) != AltiModule::SUCCESS) {
      CERR("disabling snapshot memory", "");
      return AltiModule::FAILURE;
    }
    
    return AltiModule::SUCCESS;
  }
};

//------------------------------------------------------------------------------

class AltiSNPMaskWrite : public MenuItem {
public:
  AltiSNPMaskWrite() { setName("write   snapshot mask"); }
  int action() {
    
    std::string mode;
    if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
      CERR("reading PRM mode", "");
      return AltiModule::FAILURE;
    }
    if(mode == "PATTERN") {
      printf("Shared memory in PATTERN mode: do nothing");
      return AltiModule::SUCCESS;
    }
    
    AltiModule::SIGNAL signal;
    unsigned int signal_choice = enterInt("Signal (1=\"ORB\", 2=\"L1A\", ..., 18=\"BUSY\", 19..21=\"CALREQ0..2\", 22=all)", 1, AltiModule::SIGNAL_NUMBER - 1 + 1);
    if (signal_choice < AltiModule::SIGNAL_NUMBER) signal = (AltiModule::SIGNAL) signal_choice;
    
    bool mask = (bool) enterInt("Enable (0=no, 1=yes)                                                     ", 0, 1);
    
    if (signal_choice < AltiModule::SIGNAL_NUMBER) { // 1 signal
      if (alti->PRMSnapshotSignalMaskWrite(signal, mask) != AltiModule::SUCCESS) {
	CERR("%s signal %s from the snapshot", mask ? "masking" : "unmasking", AltiModule::SIGNAL_NAME[signal].c_str());
      }
    }
    else { // all signals
      if (alti->PRMSnapshotSignalMaskWrite(mask) != AltiModule::SUCCESS) {
	CERR("%s all signals from the snapshot", mask ? "masking" : "unmasking");
      }
    }
    
    return AltiModule::SUCCESS;
  }
};

//------------------------------------------------------------------------------

class AltiSNPRead : public MenuItem {
public:
  AltiSNPRead() { setName("read    snapshot memory"); }
  int action() {
    
    std::string mode;
    if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
      CERR("reading PRM mode", "");
      return AltiModule::FAILURE;
    }
    if(mode == "PATTERN") {
      printf("Shared memory in PATTERN mode: do nothing");
      return AltiModule::SUCCESS;
    }
    
    unsigned int vme_off = enterHex("Starting index", 0, AltiModule::MAX_MEMORY - 1);
    unsigned int size = enterHex("Size          ", 1, AltiModule::MAX_MEMORY - vme_off);
    
    if (alti->PRMRead(segment[0], vme_off, vme_off+size) != AltiModule::SUCCESS) {
      CERR("reading snapshot memory. If the snapshot is still enabled, disable it before reading the memory", "");
      return AltiModule::FAILURE;
    }
    
    return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiSNPReadFile : public MenuItem {
public:
  AltiSNPReadFile() { setName("read    snapshot memory [into file]"); }
  int action() {
    
    std::string mode;
    if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
      CERR("reading PRM mode", "");
      return AltiModule::FAILURE;
    }
    if(mode == "PATTERN") {
      printf("Shared memory in PATTERN mode: do nothing");
      return AltiModule::SUCCESS;
    }
    
    unsigned int start = enterHex("Starting index                                          ", 0, AltiModule::MAX_MEMORY - 1);
    unsigned int size = enterHex("Size                                                    ", 1, AltiModule::MAX_MEMORY - start);
      
    std::string fn = enterString("File name                                                                    ");
    std::ofstream outf(fn.c_str(), std::ofstream::out);
    if (!outf) {
      CERR("cannot open output file \"%s\"", fn.c_str());
      return AltiModule::FAILURE;
    }
    
    if (alti->PRMReadFile(segment[0], outf, start, start + size) != AltiModule::SUCCESS) {
      CERR("reading snapshot memory into file. If the snapshot is still enabled, disable it before reading the memory", "");
      return AltiModule::FAILURE;
    }
    
    return AltiModule::SUCCESS;
  }
};

//------------------------------------------------------------------------------

class AltiSNPTrigCondRead : public MenuItem {
  public:
    AltiSNPTrigCondRead() { setName("read    snapshot trigger condition"); }
    int action() {

      std::string mode;
      if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
	CERR("reading PRM mode", "");
	return AltiModule::FAILURE;
      }
      if(mode == "PATTERN") {
	printf("Shared memory in PATTERN mode: do nothing");
	return AltiModule::SUCCESS;
      }
      
      unsigned int ttype;
      unsigned int i;
      bool enable;
      AltiModule::SIGNAL_PG signal;
      if (alti->PRMTriggerEnableRead(enable) != AltiModule::SUCCESS) {
	CERR("reading trigger condition enable", "");
	return AltiModule::FAILURE;
      }
      if (enable){
	printf(" Trigger Mode: Enabled \n");
	for (i = 0; i < AltiModule::SIGNAL_PG_NUMBER; i++){
	  signal = AltiModule::SIGNAL_PG(i);
	  if (alti->PRMTriggerSignalRead(signal, enable) != AltiModule::SUCCESS) {
	    CERR("reading snapshot trigger condition for signal %s", AltiModule::SIGNAL_PG_NAME[i].c_str());
	    return AltiModule::FAILURE;
	  }
	  printf("%-7s : %s \n", AltiModule::SIGNAL_PG_NAME[i].c_str(), enable ? "ENABLED" : "DISABLED");
	}
	
	if (alti->PRMTriggerTypeRead(ttype) != AltiModule::SUCCESS) {
	  CERR("reading snapshot trigger condition for ttyp", "");
	  return AltiModule::FAILURE;
	}
	
	printf("TTYPE   : 0x%02x\n",ttype);
      }
      else {          
	printf("Trigger Mode: Disabled \n");
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiSNPTrigCondWrite : public MenuItem {
public:
  AltiSNPTrigCondWrite() { setName("write   snapshot trigger condition"); }
  int action() {
    
    std::string mode;
    if (alti->PRMModeRead(mode) != AltiModule::SUCCESS) {
      CERR("reading PRM mode", "");
      return AltiModule::FAILURE;
    }
    if(mode == "PATTERN") {
      printf("Shared memory in PATTERN mode: do nothing");
      return AltiModule::SUCCESS;
    }
    
    unsigned int ttype;
    unsigned int i;
    bool enable;
    AltiModule::SIGNAL_PG signal;
    string command;
    
    enable = (bool) enterInt("Enable trigger condition for snapshot: 0=NO, 1=YES", 0, 1);
    
    if (alti->PRMTriggerEnableWrite(enable) != AltiModule::SUCCESS) {
      CERR("writing trigger condition enable", "");
      return AltiModule::FAILURE;
    }
    if (enable){
      printf(" Trigger Mode: Enabled \n");
      for (i = 0; i < AltiModule::SIGNAL_PG_NUMBER; i++){
        signal = AltiModule::SIGNAL_PG(i);
	command = "Enable trigger condition for " + AltiModule::SIGNAL_PG_NAME[i] + ": 0=NO, 1=YES";
	enable = (bool) enterInt(command.c_str(), 0, 1);
	if (alti->PRMTriggerSignalWrite(signal, enable) != AltiModule::SUCCESS) {
	  CERR("writing snapshot trigger condition for signal %s", AltiModule::SIGNAL_PG_NAME[i].c_str());
	  return AltiModule::FAILURE;
	}
      }
      
      ttype =  enterInt("TTYP trigger condition for snapshot: 0=NO, 1=YES", 0, 0xff);
      if (alti->PRMTriggerTypeWrite(ttype) != AltiModule::SUCCESS) {
	CERR("writing snapshot trigger condition for ttyp", "");
	return AltiModule::FAILURE;
      }
    }

    return AltiModule::SUCCESS;
  }
};
    

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiENCTransmitterStatusRead : public RCD::MenuItem {
  public:
    AltiENCTransmitterStatusRead() { setName("read    transmitters status"); }
    int action() {

        std::printf("\n");
        std::printf("+---------------------------------------------+\n");
        std::printf("| Transmitter | Delay [ns] |      Status      |\n");
        std::printf("+---------------------------------------------+\n");

        std::vector<unsigned int> delay;
        delay.resize(AltiModule::TRANSMITTER_NUMBER);
        std::vector<bool> enabled;
        enabled.resize(AltiModule::TRANSMITTER_NUMBER);
        std::vector<bool> fault;
        fault.resize(AltiModule::TRANSMITTER_NUMBER);
       
        if (alti->ENCTransmitterEnableRead(enabled) != AltiModule::SUCCESS) {
            CERR("reading enable status of all TXs", "");
            return AltiModule::FAILURE;
        }
	if (alti->ENCTransmitterDelayRead(delay) != AltiModule::SUCCESS) {
            CERR("reading delays of all TXs", "");
            return AltiModule::FAILURE;
        }
        if (alti->ENCTransmitterFaultRead(fault) != AltiModule::SUCCESS) {
            CERR("reading fault status of all TXs", "");
            return AltiModule::FAILURE;
        }
        unsigned int i;
        for (i = 0; i < enabled.size(); i++) {
	  std::printf("| TX[%02d]      |     %-2d     | %s %s |\n", i, delay[i], enabled[i] ? "Enabled " : "Disabled", fault[i] ? "/ Fault" : "       ");
	    std::printf("+---------------------------------------------+\n");
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCTransmitterEnable : public RCD::MenuItem {
  public:
    AltiENCTransmitterEnable() { setName("write   transmitters enable"); }
    int action() {


        unsigned int tx = enterInt("Transmitter (0=\"TX0\", 1=\"TX1\", ..., \"10=TX10\", 11=all)", 0, AltiModule::TRANSMITTER_NUMBER - 1 + 1);
        unsigned int enable = enterInt("Enable (0=disable, 1=enable)                           ", 0, 1);

        if (tx < AltiModule::TRANSMITTER_NUMBER) {
            if (alti->ENCTransmitterEnableWrite(tx, enable) != AltiModule::SUCCESS) {
                CERR("%s TX%d", enable ? "enabling" : "disabling", tx);
                return AltiModule::FAILURE;
            }
        }
        else { // all
            if (alti->ENCTransmitterEnableWrite(enable) != AltiModule::SUCCESS) {
                CERR("%s all TXs", enable ? "enabling" : "disabling");
                return AltiModule::FAILURE;
            }
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCTransmitterDelayWrite : public RCD::MenuItem {
  public:
    AltiENCTransmitterDelayWrite() { setName("write   transmitters delay"); }
    int action() {


        unsigned int tx = enterInt("Transmitter (0=\"TX0\", 1=\"TX1\", ..., \"10=TX10\", 11=all)", 0, AltiModule::TRANSMITTER_NUMBER - 1 + 1);
        unsigned int delay = enterInt("Delay steps (~3.12ns each)", 0, 31);

        if (tx < AltiModule::TRANSMITTER_NUMBER) {
	  if (alti->ENCTransmitterDelayWrite(tx, delay) != AltiModule::SUCCESS) {
	    CERR("setting TX-%d delay to %d", tx, delay);	     
	    return AltiModule::FAILURE;
	  }
        }
        else { // all
	  std::vector<u_int> delays(AltiModule::TRANSMITTER_NUMBER, delay);
	  if (alti->ENCTransmitterDelayWrite(delays) != AltiModule::SUCCESS) {
	    CERR("setting all TXs delays to %d", delay);
	    return AltiModule::FAILURE;
	  }
        }
	
        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCTransmitterFaultClear : public RCD::MenuItem {
  public:
    AltiENCTransmitterFaultClear() { setName("clear   transmitters fault"); }
    int action() {

      if (alti->ENCTransmitterFaultClear() != AltiModule::SUCCESS) {
	CERR("clearing fault status for all TXs", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCSourceRead : public RCD::MenuItem {
  public:
    AltiENCSourceRead() { setName("read    TTC source selection"); }
    int action() {
      bool l1a, ttr1, ttr2, ttr3, calreq, pattern, minictp;
      if (alti->ENCL1ASourceRead(l1a, ttr1, ttr2, ttr3, calreq, pattern, minictp) != 0) {
        CERR("reading TTC encoder L1A source", "");
        return AltiModule::FAILURE;     
      }      
      printf(" L1A from EXT     : %s\n", l1a ? "enabled" : "disabled");
      printf(" L1A from TTR1    : %s\n", ttr1 ? "enabled" : "disabled");
      printf(" L1A from TTR2    : %s\n", ttr2 ? "enabled" : "disabled");
      printf(" L1A from TTR3    : %s\n", ttr3 ? "enabled" : "disabled");
      printf(" L1A from CALREQ  : %s\n", calreq ? "enabled" : "disabled");
      printf(" L1A from PATTERN : %s\n", pattern ? "enabled" : "disabled");
      printf(" L1A from MINICTP : %s\n", minictp ? "enabled" : "disabled");

      string src;
      if (alti->ENCOrbitSourceRead(src) != 0) {
	CERR("reading TTC encoder Orbit source", "");
	return AltiModule::FAILURE;     
      }
      printf(" Orbit source    : %s\n", src.c_str());

      for (u_int i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
	if (alti->ENCBgoSourceRead(i, src) != 0) {
	  CERR("reading TTC encoder BGo%d source", i);
	  return AltiModule::FAILURE;     
	}
	printf(" BGo%d source  : %s\n", i, src.c_str());
      }

      if (alti->ENCTtypSourceRead(src) != 0) {
	CERR("reading TTC encoder TTYP source", "");
	return AltiModule::FAILURE;     
      }
      printf(" TTYP source     : %s\n", src.c_str());

      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCL1aSourceWrite : public RCD::MenuItem {
  public:
    AltiENCL1aSourceWrite() { setName("write   L1A source selection"); }
    int action() {
      
      bool l1a     = (bool) enterInt("L1A      (0 = disabled, 1 = enabled)", 0, 1);
      bool ttr1    = (bool) enterInt("TTR1     (0 = disabled, 1 = enabled)", 0, 1);
      bool ttr2    = (bool) enterInt("TTR2     (0 = disabled, 1 = enabled)", 0, 1);
      bool ttr3    = (bool) enterInt("TTR3     (0 = disabled, 1 = enabled)", 0, 1);
      bool calreq  = (bool) enterInt("CALREQ   (0 = disabled, 1 = enabled)", 0, 1);
      bool pattern = (bool) enterInt("PATTERN  (0 = disabled, 1 = enabled)", 0, 1);
      bool minictp = (bool) enterInt("MINICTP  (0 = disabled, 1 = enabled)", 0, 1);

      if (alti->ENCL1ASourceWrite(l1a, ttr1, ttr2, ttr3, calreq, pattern, minictp) != 0) 
      {
        CERR("writing TTC encoder L1A source", "");
        return AltiModule::FAILURE;     
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCOrbitSourceWrite : public RCD::MenuItem {
  public:
    AltiENCOrbitSourceWrite() { setName("write   Orbit source selection"); }
    int action() {
      
      int src     = enterInt("Source      (0 = EXTERNAL, 1 = PATTERN, 2 = MINICTP)", 0, 2);

      if (alti->ENCOrbitSourceWrite(ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::ORBIT_TYPE_NAME[src]) != 0) 
      {
        CERR("writing TTC encoder Orbit source", "");
        return AltiModule::FAILURE;     
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCBgoSourceWrite : public RCD::MenuItem {
  public:
    AltiENCBgoSourceWrite() { setName("write   Bgo source selection"); }
    int action() {
      u_int bgo = enterInt("BGO index (0-3, 4=all)                   ", 0, AltiModule::TTC_FIFO_NUMBER);
      u_int src     = enterInt("ORB      (0 = EXTERNAL, 1 = PATTERN, 2 = MINICTP)", 0, 2);
      
      if(bgo == AltiModule::TTC_FIFO_NUMBER) {
	for(u_int i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) { 
	  if (alti->ENCBgoSourceWrite(i, ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::BGO_TYPE_NAME[src]) != 0) 
	    {
	      CERR("writing TTC encoder BGo source", "");
	      return AltiModule::FAILURE;     
	    }
	}
      }
      else {
	if (alti->ENCBgoSourceWrite(bgo, ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::BGO_TYPE_NAME[src]) != 0) 
	  {
	    CERR("writing TTC encoder BGo source", "");
	    return AltiModule::FAILURE;     
	  }
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCTtypSourceWrite : public RCD::MenuItem {
  public:
    AltiENCTtypSourceWrite() { setName("write   TTYP source selection"); }
    int action() {
      
      int src     = enterInt("Source      (0 = EXTERNAL, 1 = PATTERN, 2 = MINICTP)", 0, 2);

      if (alti->ENCTtypSourceWrite(ALTI_CTL_TTCTRIGGERSELECT_BITSTRING::TRIGGERTYPE_TYPE_NAME[src]) != 0) 
      {
        CERR("writing TTC encoder TTYP source", "");
        return AltiModule::FAILURE;     
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCInhibitParamsRead : public RCD::MenuItem {
  public:
    AltiENCInhibitParamsRead() { setName("read    BGO  inhibit parameters"); }
    int action() {

        std::vector<unsigned int> width, delay;
        if (alti->ENCInhibitParamsRead(width, delay) != AltiModule::SUCCESS) {
            CERR("reading TTC encoder BGO inhibit parameters", "");
            return AltiModule::FAILURE;
        }

        //table
        printf("\n");
        printf("+------------------------------+\n");
        printf("|      | Width[BC] | Delay[BC] |\n");
        printf("+------------------------------+\n");

        unsigned int i;
        for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
            printf("| BGO%d | %4d      | %4d      |\n", i, width[i], delay[i]);
            printf("+------------------------------+\n");
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCInhibitParamsWrite : public RCD::MenuItem {
  public:
    AltiENCInhibitParamsWrite() { setName("write   BGO  inhibit parameters"); }
    int action() {

        unsigned int bgo;
        unsigned int bgo_choice = enterInt("BGO index (0-3, 4=all)                   ", 0,  AltiModule::TTC_FIFO_NUMBER);
        if (bgo_choice < 4) bgo = bgo_choice;

        if (bgo_choice < 4) { // 1 BGO
            std::printf("BGO%d width [BCs]                      ", bgo);
            unsigned int width = enterInt("", 0, ALTI_ENC_BGOCONTROL_BITSTRING::MASK_INHIBITWIDTH.number()[0]); 
            std::printf("BGO%d delay [BCs]                      ", bgo);
            unsigned int delay = enterInt("", 0, 3564);
            if (alti->ENCInhibitParamsWrite(bgo, width, delay) != AltiModule::SUCCESS) {
                CERR("writing TTC encoder BGO%d inhibit width %d, delay", bgo, width, delay);
                return AltiModule::FAILURE;
            }
        }
        else { // all BGOs
            std::vector<unsigned int> width, delay;
            width.clear();
            delay.clear();
            unsigned int i;
            for (i = 0; i <= 3; i++) {
                std::printf("BGO%d width [BCs]                      ", i);
                width.push_back(enterInt("", 0, ALTI_ENC_BGOCONTROL_BITSTRING::MASK_INHIBITWIDTH.number()[0])); 
                std::printf("BGO%d delay [BCs]                      ", i);
                delay.push_back(enterInt("", 0, 3564));

                std::printf("\n");

                if (alti->ENCInhibitParamsWrite(width, delay) != AltiModule::SUCCESS) {
                    CERR("writing TTC encoder inhibit width and delay for all BGOs", "");
                    return AltiModule::FAILURE;
                }
            }
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCTriggerWordStatusRead : public RCD::MenuItem {
  public:
    AltiENCTriggerWordStatusRead() { setName("read    TTYP word status"); }
    int action() {
      
      bool ena; 
      u_int addr, sub_addr;
      AltiModule::TTC_ADDRESS_SPACE addr_space;
      
      if (alti->ENCTriggerTypeControlRead(ena, addr, addr_space, sub_addr) != AltiModule::SUCCESS) {
	CERR("reading TTC encoder TTYP control", "");
	return AltiModule::FAILURE;
      }
      
      u_int delay;
      if (alti->ENCTriggerTypeDelayRead(delay) != AltiModule::SUCCESS) {
	CERR("reading TTC encoder TTYP delay", "");
	return AltiModule::FAILURE;
      }

      AltiModule::TTYP_COUNTER_SOURCE src;
      if (alti->ENCTriggerTypeCounterSourceRead(src) != AltiModule::SUCCESS) {
	CERR("reading TTC encoder TTYP counter source", "");
	return AltiModule::FAILURE;
      }

      u_int cnt_evt, cnt_orb;
      if (alti->ENCTriggerTypeCounterRead(cnt_evt, cnt_orb) != AltiModule::SUCCESS) {
	CERR("reading TTC encoder TTYP counters", "");
	return AltiModule::FAILURE;
      }
      
      printf("+-----------------------------+\n");
      printf("|      TRIGGER TYPE cycle     |\n");
      printf("+-----------------------------+\n");
      printf("| Enabled        |   %5s    |\n", ena ? "True" : "False");
      printf("| Delay          |     %-2d     |\n", delay);
      printf("| Address        | 0x%08x |\n", addr);
      printf("| Address space  |  %8s  |\n", AltiModule::TTC_ADDRESS_SPACE_NAME[addr_space].c_str());
      printf("| Sub-address    | 0x%08x |\n", sub_addr);
      printf("| Counter source |   %5s    |\n", AltiModule::TTYP_COUNTER_SOURCE_NAME[src].c_str());
      printf("+----------------------------+\n");
      printf("|       counter values        |\n");
      printf("+----------------------------+\n");
      printf("| Event counter: %010u   |\n", cnt_evt);
      printf("| Orbit counter: %010u   |\n", cnt_orb);
      printf("+----------------------------+\n");

      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCTriggerWordDelayWrite : public RCD::MenuItem {
  public:
    AltiENCTriggerWordDelayWrite() { setName("write   TTYP word delay"); }
    int action() {

      unsigned int delay = enterInt("Delay [BCs]", 0, 7);
      
        if (alti->ENCTriggerTypeDelayWrite(delay) != AltiModule::SUCCESS) {
            CERR("writing TTC encoder TTYP delay %d", delay);
            return AltiModule::FAILURE;
        }
        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCTriggerWordControlWrite : public RCD::MenuItem {
public:
  AltiENCTriggerWordControlWrite() { setName("write   TTYP control"); }
  int action() {
    
    unsigned int addr = enterHex("TTCrx address           ", 0, 0x3fff); // 14 bits
    AltiModule::TTC_ADDRESS_SPACE addressSpace = (AltiModule::TTC_ADDRESS_SPACE) enterInt("Address space (0=internal, 1=external)", 0, AltiModule::TTC_ADDRESS_SPACE_NUMBER - 1);
    unsigned int subAddr = enterHex("Subaddress              ", 0, 0xff); // 8 bits, 2 LSb get trimmed, 6 MSb get stored
    
    if (alti->ENCTriggerTypeControlWrite(addr, addressSpace, subAddr) != AltiModule::SUCCESS) {
      CERR("writing TTC encoder TTYP cycles control", "");
      return AltiModule::FAILURE;
    }
    
    return AltiModule::SUCCESS;
  }
};

//------------------------------------------------------------------------------

class AltiENCTriggerWordCounterSourceWrite : public RCD::MenuItem {
public:
  AltiENCTriggerWordCounterSourceWrite() { setName("write   TTYP counter source"); }
  int action() {
    
    AltiModule::TTYP_COUNTER_SOURCE src = (AltiModule::TTYP_COUNTER_SOURCE) enterInt("counter source (0=event, 1=orbit)", 0, AltiModule::TTYP_COUNTER_SOURCE_NUMBER - 1);
    
    if (alti->ENCTriggerTypeCounterSourceWrite(src) != AltiModule::SUCCESS) {
      CERR("writing TTC encoder TTYP cycles counter source", "");
      return AltiModule::FAILURE;
    }
    
    return AltiModule::SUCCESS;
  }
};

//------------------------------------------------------------------------------

class AltiENCTriggerWordCounterReset : public RCD::MenuItem {
public:
  AltiENCTriggerWordCounterReset() { setName("reset   TTYP counters"); }
  int action() {
    
    u_int reset = (AltiModule::TTYP_COUNTER_SOURCE) enterInt("counter reset (0=event, 1=orbit, 2=all)", 0, AltiModule::TTYP_COUNTER_SOURCE_NUMBER);
    
    bool evt = true;
    bool orb = true;
    if(reset == 0)      orb = false;
    else if(reset == 1) evt = false;

    if (alti->ENCTriggerTypeCounterReset(evt, orb) != AltiModule::SUCCESS) {
      CERR("doing reset of TTC encoder TTYP cycles counter", "");
      return AltiModule::FAILURE;
    }
    
    return AltiModule::SUCCESS;
  }
};

//------------------------------------------------------------------------------

class AltiENCTriggerWordCycleEnable : public RCD::MenuItem {
public:
  AltiENCTriggerWordCycleEnable() { setName("enable  TTYP cycles"); }
  int action() {
  
    if (alti->ENCTriggerTypeEnable(true) != AltiModule::SUCCESS) {
      CERR("enabling TTC encoder TTYP cycles", "");
      return AltiModule::FAILURE;
    }
    
    return AltiModule::SUCCESS;
  }
};

//------------------------------------------------------------------------------

class AltiENCTriggerWordCycleDisable : public RCD::MenuItem {
  public:
    AltiENCTriggerWordCycleDisable() { setName("disable TTYP cycles"); }
    int action() {

      if (alti->ENCTriggerTypeEnable(false) != AltiModule::SUCCESS) {
	CERR("disabling TTC encoder TTYP cycles", "");
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCBgoGenerate : public RCD::MenuItem {
  public:
    AltiENCBgoGenerate() { setName("trigger BGo command from the VME"); }
    int action() {

        unsigned int bgo = enterInt("BGO index", 0, 3);
        if (alti->ENCBgoGenerate(bgo) != AltiModule::SUCCESS) {
            CERR("triggering one-shot BGO%d command from VME", bgo);
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCBgoCommandModeRead : public RCD::MenuItem {
  public:
    AltiENCBgoCommandModeRead() { setName("read    BGO  FIFO mode"); }
    int action() {

        std::vector<AltiModule::TTC_BGO_MODE_TRIGGER> mode_trigger;
        std::vector<AltiModule::TTC_BGO_MODE_COMMAND> mode_command;
        std::vector<AltiModule::TTC_BGO_MODE_REPEAT> mode_repeat;
        std::vector<AltiModule::TTC_BGO_MODE_FIFO> mode_fifo;
        std::vector<AltiModule::TTC_BGO_MODE_INHIBIT> mode_inh;
        if (alti->ENCBgoCommandModeRead(mode_trigger, mode_command, mode_repeat, mode_fifo, mode_inh) != AltiModule::SUCCESS) {
            CERR("reading TTC encoder BGO mode", "");
            return AltiModule::FAILURE;
        }

        //table
        printf("\n");
        printf("+--------------------------------------------------------------------------------------------------------------------+\n");
        printf("|      | Trigger    | Command      | Repeat     | FIFO           | INHIBIT        || High-level description          |\n");
        printf("+--------------------------------------------------------------------------------------------------------------------+\n");

        unsigned int i;
        for (i = 0; i <= 3; i++) {
            printf("| BGO%d | %-10s | %-12s | %-10s | %-14s | %-14s || %-31s |\n", i, AltiModule::TTC_BGO_MODE_TRIGGER_NAME[mode_trigger[i]].c_str(), AltiModule::TTC_BGO_MODE_COMMAND_NAME[mode_command[i]].c_str(), AltiModule::TTC_BGO_MODE_REPEAT_NAME[mode_repeat[i]].c_str(), AltiModule::TTC_BGO_MODE_FIFO_NAME[mode_fifo[i]].c_str(), AltiModule::TTC_BGO_MODE_INHIBIT_NAME[mode_inh[i]].c_str(), AltiModule::TTC_BGO_MODE_NAME[AltiModule::ENCLowLevelToHighLevelBgoMode(mode_trigger[i], mode_command[i], mode_repeat[i], mode_fifo[i], mode_inh[i])].c_str());
            printf("+--------------------------------------------------------------------------------------------------------------------+\n");
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCBgoCommandModeWrite : public RCD::MenuItem {
  public:
    AltiENCBgoCommandModeWrite() { setName("write   BGO  FIFO mode [expert user]"); }
    int action() {

        unsigned int bgo_choice = enterInt("BGO index (0-3, 4=all)                                ", 0, 4);

        AltiModule::TTC_BGO_MODE_TRIGGER mode_trigger;
        AltiModule::TTC_BGO_MODE_COMMAND mode_command;
        AltiModule::TTC_BGO_MODE_REPEAT mode_repeat;
        AltiModule::TTC_BGO_MODE_FIFO mode_fifo;
        AltiModule::TTC_BGO_MODE_INHIBIT mode_inh;
        std::vector<AltiModule::TTC_BGO_MODE_TRIGGER> mode_trigger_v;
        std::vector<AltiModule::TTC_BGO_MODE_COMMAND> mode_command_v;
        std::vector<AltiModule::TTC_BGO_MODE_REPEAT> mode_repeat_v;
        std::vector<AltiModule::TTC_BGO_MODE_FIFO> mode_fifo_v;
        std::vector<AltiModule::TTC_BGO_MODE_INHIBIT> mode_inh_v;
        if (bgo_choice < 4) { // 1 BGO
            unsigned int bgo = bgo_choice;
            std::printf("BGO%d trigger mode (0=\"BGO_SIGNAL\", 1=\"VME\")           ", bgo);
            mode_trigger = (AltiModule::TTC_BGO_MODE_TRIGGER) enterInt("", 0, AltiModule::TTC_BGO_MODE_TRIGGER_NUMBER - 1);
            std::printf("BGO%d command mode (0=\"SYNCHRONOUS\", 1=\"ASYNCHRONOUS\") ", bgo);
            mode_command = (AltiModule::TTC_BGO_MODE_COMMAND) enterInt("", 0, AltiModule::TTC_BGO_MODE_COMMAND_NUMBER - 1);
            std::printf("BGO%d repeat  mode (0=\"SINGLE\", 1=\"REPETITIVE\")        ", bgo);
            mode_repeat = (AltiModule::TTC_BGO_MODE_REPEAT) enterInt("", 0, AltiModule::TTC_BGO_MODE_REPEAT_NUMBER - 1);
            std::printf("BGO%d FIFO    mode (0=\"WHEN_NOT_EMPTY\", 1=\"ON_TRIGGER\")", bgo);
            mode_fifo = (AltiModule::TTC_BGO_MODE_FIFO) enterInt("", 0, AltiModule::TTC_BGO_MODE_FIFO_NUMBER - 1);
            std::printf("BGO%d Inhibit mode (0=\"ON_INHIBIT\", 1=\"OTHER_MODES\")", bgo);
            mode_inh = (AltiModule::TTC_BGO_MODE_INHIBIT) enterInt("", 0, AltiModule::TTC_BGO_MODE_INHIBIT_NUMBER - 1);
            if (alti->ENCBgoCommandModeWrite(bgo, mode_trigger, mode_command, mode_repeat, mode_fifo, mode_inh) != AltiModule::SUCCESS) {
                CERR("writing TTC encoder BGO%d FIFO mode \"%s\", \"%s\", \"%s\", \"%s\", \"%s\"", bgo, AltiModule::TTC_BGO_MODE_TRIGGER_NAME[mode_trigger].c_str(), AltiModule::TTC_BGO_MODE_COMMAND_NAME[mode_command].c_str(), AltiModule::TTC_BGO_MODE_REPEAT_NAME[mode_repeat].c_str(), AltiModule::TTC_BGO_MODE_FIFO_NAME[mode_fifo].c_str(), AltiModule::TTC_BGO_MODE_INHIBIT_NAME[mode_inh].c_str());
                return AltiModule::FAILURE;
            }
        }
        else { // all BGOs
            mode_trigger_v.clear();
            mode_command_v.clear();
            mode_repeat_v.clear();
            mode_fifo_v.clear();
            mode_inh_v.clear();
            unsigned int i;
            for (i = 0; i <= 3; i++) {
                std::printf("BGO%d trigger mode (0=\"BGO_SIGNAL\", 1=\"VME\")           ", i);
                mode_trigger_v.push_back((AltiModule::TTC_BGO_MODE_TRIGGER) enterInt("", 0, AltiModule::TTC_BGO_MODE_TRIGGER_NUMBER - 1));
                std::printf("BGO%d command mode (0=\"SYNCHRONOUS\", 1=\"ASYNCHRONOUS\") ", i);
                mode_command_v.push_back((AltiModule::TTC_BGO_MODE_COMMAND) enterInt("", 0, AltiModule::TTC_BGO_MODE_COMMAND_NUMBER - 1));
                std::printf("BGO%d repeat  mode (0=\"SINGLE\", 1=\"REPETITIVE\")        ", i);
                mode_repeat_v.push_back((AltiModule::TTC_BGO_MODE_REPEAT) enterInt("", 0, AltiModule::TTC_BGO_MODE_REPEAT_NUMBER - 1));
                std::printf("BGO%d FIFO    mode (0=\"WHEN_NOT_EMPTY\", 1=\"ON_TRIGGER\")", i);
                mode_fifo_v.push_back((AltiModule::TTC_BGO_MODE_FIFO) enterInt("", 0, AltiModule::TTC_BGO_MODE_FIFO_NUMBER - 1));
                std::printf("BGO%d FIFO    mode (0=\"OTHER_MODES\", 1=\"ON_INHIBIT\")", i);
                mode_inh_v.push_back((AltiModule::TTC_BGO_MODE_INHIBIT) enterInt("", 0, AltiModule::TTC_BGO_MODE_INHIBIT_NUMBER - 1));

                std::printf("\n");
            }
            if (alti->ENCBgoCommandModeWrite(mode_trigger_v, mode_command_v, mode_repeat_v, mode_fifo_v, mode_inh_v) != AltiModule::SUCCESS) {
                CERR("writing TTC encoder all modes for all BGOs", "");
                return AltiModule::FAILURE;
            }
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCBgoCommandModeHighLevelWrite : public RCD::MenuItem {
  public:
    AltiENCBgoCommandModeHighLevelWrite() { setName("write   BGO  FIFO mode"); }
    int action() {

        unsigned int bgo;
        unsigned int bgo_choice = enterInt("BGO index (0-3, 4=all)", 0, 4);
        if (bgo_choice < 4) bgo = bgo_choice;

        AltiModule::TTC_BGO_MODE mode;
        std::vector<AltiModule::TTC_BGO_MODE> mode_v;
        if (bgo_choice < 4) { // 1
            mode = (AltiModule::TTC_BGO_MODE) enterInt("Mode (0=\"SYNCHRONOUS_SINGLE_BGO_SIGNAL\", 1=\"SYNCHRONOUS_REPETITIVE\", 2=\"ASYNCHRONOUS_BGO_SIGNAL\", 3=\"ASYNCHRONOUS_VME_ON_TRIGGER\", 4=\"ASYNCHRONOUS_FIFO_MODE\")", 0, AltiModule::TTC_BGO_MODE_NUMBER - 1 - 1);
            if (alti->ENCBgoCommandModeWrite(bgo, mode) != AltiModule::SUCCESS) {
                CERR("writing TTC encoder BGO%d high-level mode \"%s\"", bgo, AltiModule::TTC_BGO_MODE_NAME[mode].c_str());
                return AltiModule::FAILURE;
            }
        }
        else if (bgo_choice == 4) { // all
            mode_v.clear();
            unsigned int i;
            for (i = 0; i <= 3; i++) {
                std::printf("BGO%d mode (0=\"SYNCHRONOUS_SINGLE_BGO_SIGNAL\", 1=\"SYNCHRONOUS_REPETITIVE\", 2=\"ASYNCHRONOUS_BGO_SIGNAL\", 3=\"ASYNCHRONOUS_VME_ON_TRIGGER\", 4=\"ASYNCHRONOUS_FIFO_MODE\")", i);
                mode_v.push_back((AltiModule::TTC_BGO_MODE) enterInt("", 0, 4));
            }
            if (alti->ENCBgoCommandModeWrite(mode_v) != AltiModule::SUCCESS) {
                CERR("writing TTC encoder high-level modes for all BGOs", "");
                return AltiModule::FAILURE;
            }
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCFifoStatusRead : public RCD::MenuItem {
  public:
    AltiENCFifoStatusRead() { setName("read    FIFOs status"); }
    int action() {

        bool busy, full, empty, retransmit;
        u_int level;

        std::printf("\n");
        unsigned int i;
        AltiModule::TTC_FIFO ttcFifo;
        for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
            ttcFifo = (AltiModule::TTC_FIFO) i;

	    if (alti->ENCFifoStatusRead(ttcFifo, busy, full, empty, level) != AltiModule::SUCCESS) {
	      CERR("reading BGO FIFOs status", "");
	      return AltiModule::FAILURE;
	    }

	    if (alti->ENCBgoFifoRetransmitRead(ttcFifo, retransmit) != AltiModule::SUCCESS) {
	      CERR("reading BGO FIFOs retransmit mode", "");
	      return AltiModule::FAILURE;
	    }

            std::printf("\"%-9s\" status: ", AltiModule::TTC_FIFO_NAME[ttcFifo].c_str());
            std::printf("%-14s,", busy ? "Pipeline FULL" : "Pipeline EMPTY");
	    std::printf("%-11s", full ? " FIFO FULL" : ((empty ? " FIFO EMPTY" : "")));
	    std::printf(" LEVEL: %d%%", level);
            std::printf("%-12s\n", retransmit ? ", retransmit" : "");
        }

	bool ttyp_full, ttyp_empty;
        u_int ttyp_level;

	if (alti->ENCTriggerTypeFifoStatusRead(ttyp_full, ttyp_empty, ttyp_level) != AltiModule::SUCCESS) {
	  CERR("reading TTYP FIFOs statuses", "");
	  return AltiModule::FAILURE;
	}

	std::printf("\"%-9s\" status: ", "TTYP_FIFO");
	std::printf("%-11s", ttyp_full ? " FIFO FULL" : ((ttyp_empty ? " FIFO EMPTY" : "")));
	std::printf(" LEVEL: %d%%", ttyp_level);
	
        std::printf("\n");

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCFifoRead : public RCD::MenuItem {
  public:
    AltiENCFifoRead() { setName("read    FIFOs"); }
    int action() {
      unsigned int fifo_choice = enterInt("FIFO (0=\"BGO0\", 1=\"BGO1\", 2=\"BGO2\", 3=\"BGO3\")", 0, AltiModule::TTC_FIFO_NUMBER-1);
      AltiModule::TTC_FIFO ttc_fifo = (AltiModule::TTC_FIFO) fifo_choice;
      
      unsigned int nb = enterInt("Number of words", 0, ALTI::ENC_BGOFIFO_INDEX_NUMBER-1);
      
      std::vector<u_int> data;
      data.resize(nb);
      if (alti->ENCFifoRead(ttc_fifo, data) != AltiModule::SUCCESS) {
	CERR("reading FIFO for \"%s\"", AltiModule::TTC_FIFO_NAME[ttc_fifo].c_str());
	return (AltiModule::FAILURE);
      }
      
      TTC_SPY_COMMAND cmd;
      std::vector<unsigned int> tmp;
 
      for(u_int i=0; i<data.size(); i++) { 
	tmp.clear();
	tmp.push_back(data[i]);
	cmd = (RCD::BitSet) tmp;
	
	//cmd.print();
	if(cmd.FrameType() == "Long") { 
	  printf("word %d = 0x%08x : long command: address = 0x%04x, address space = %s, sub-address = 0x%02x, data=0x%02x \n", i, data[i], cmd.Address(), cmd.AddressSpace().c_str(), cmd.SubAddress(), cmd.Data());
	}
	else {
	  printf("word %d = 0x%08x : short command: data=0x%02x \n", i, data[i], data[i] >> 23);
	}
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCFifoReset : public RCD::MenuItem {
  public:
    AltiENCFifoReset() { setName("reset   FIFOs"); }
    int action() {
        unsigned int fifo_choice = enterInt("FIFO (0=\"BGO0\", 1=\"BGO1\", 2=\"BGO2\", 3=\"BGO3\", 4=\"TTYP\", 5=all)", 0, 5);
        AltiModule::TTC_FIFO ttcFifo;
        if (fifo_choice <= 4) ttcFifo = (AltiModule::TTC_FIFO) fifo_choice;

        if (fifo_choice <= 4) { // ttcFifo
	  if (alti->ENCFifoReset(ttcFifo) != AltiModule::SUCCESS) {
	    CERR("resetting read and write address for \"%s\"", AltiModule::TTC_FIFO_NAME[ttcFifo].c_str());
	    return (AltiModule::FAILURE);
	  }
        }
        else if (fifo_choice == 5) { // all
	  if (alti->ENCFifoReset() != AltiModule::SUCCESS) {
	    CERR("resetting read and write address for all FIFOs", "");
	    return AltiModule::FAILURE;
	  }
	}

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCBgoFifoRetransmitSelect : public RCD::MenuItem {
  public:
    AltiENCBgoFifoRetransmitSelect() { setName("write   FIFOs retransmit"); }
    int action() {
      unsigned int fifo_choice = enterInt("FIFO (0=\"BGO0\", 1=\"BGO1\", 2=\"BGO2\", 3=\"BGO3\", 4=all)", 0, AltiModule::TTC_FIFO_NUMBER);
      bool retransmit  = (bool) enterInt("Retransmit                               (0=disable, 1=enable)", 0, 1);
      
      AltiModule::TTC_FIFO ttcFifo;
      if (fifo_choice < AltiModule::TTC_FIFO_NUMBER) {
	ttcFifo = (AltiModule::TTC_FIFO) fifo_choice;
	
	if (alti->ENCBgoFifoRetransmitWrite(ttcFifo, retransmit) != AltiModule::SUCCESS) {
	  CERR("%s retransmit mode for \"%s\"", (retransmit ? "Enabling" : "Disabling"), AltiModule::TTC_FIFO_NAME[ttcFifo].c_str());
	  return AltiModule::FAILURE;
	}
      }
      else { // all
	if (alti->ENCBgoFifoRetransmitWrite(retransmit) != AltiModule::SUCCESS) {
	  CERR("%s retransmit mode for all FIFOs", (retransmit ? "Enabling" : "Disabling"));
	  return AltiModule::FAILURE;
	}
      }
      
      return AltiModule::SUCCESS;
    }
};


//------------------------------------------------------------------------------

class AltiENCBgoCommandPut : public RCD::MenuItem {
  public:
    AltiENCBgoCommandPut() { setName("put     command [FIFO]"); }
    int action() {

        unsigned int bgo = enterInt("BGO index                             ", 0, 3);
        AltiModule::TTC_FRAME_TYPE frameType = (AltiModule::TTC_FRAME_TYPE) enterInt("Frame type (0=short, 1=long)          ", 0, AltiModule::TTC_FRAME_TYPE_NUMBER - 1);
        unsigned int data;
        switch (frameType) {
            case AltiModule::TTC_FRAME_TYPE::SHORT:
                data = enterHex("Data                    ", 0, 0xff); // 8 bits
                if (alti->ENCBgoCommandPutShort(bgo, data) != AltiModule::SUCCESS) {
                    CERR("ALTI: putting short command (data = 0x%02x) to BGo-%d FIFO", data, bgo);
                    return AltiModule::FAILURE;
                }
		printf("put short command (data = 0x%02x) to BGo-%d FIFO", data, bgo);
                break;
            case AltiModule::TTC_FRAME_TYPE::LONG:
                unsigned int addr = enterHex("TTCrx address           ", 0, 0x3fff); // 14 bits
                AltiModule::TTC_ADDRESS_SPACE addressSpace = (AltiModule::TTC_ADDRESS_SPACE) enterInt("Address space (0=internal, 1=external)", 0, AltiModule::TTC_ADDRESS_SPACE_NUMBER - 1);
                unsigned int subAddr = enterHex("Subaddress              ", 0, 0xff); // 8 bits
                data = enterHex("Data                    ", 0, 0xff); // 8 bits
                if (alti->ENCBgoCommandPutLong(bgo, addr, addressSpace, subAddr, data) != AltiModule::SUCCESS ) {
                    CERR("ALTI: putting long command (\"%s\", addr = 0x%04x, subAddr = 0x%02x, data = 0x%02x) to BGo-%d FIFO", AltiModule::TTC_ADDRESS_SPACE_NAME[addressSpace].c_str(), addr, subAddr, data, bgo);
                    return AltiModule::FAILURE;
                }
		printf("put long command (\"%s\", addr = 0x%04x, subAddr = 0x%02x, data = 0x%02x) to BG0-%d FIFO", AltiModule::TTC_ADDRESS_SPACE_NAME[addressSpace].c_str(), addr, subAddr, data, bgo);
                break;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCAsyncCommandPut : public RCD::MenuItem {
  public:
    AltiENCAsyncCommandPut() { setName("send    command [async]"); }
    int action() {

        AltiModule::TTC_FRAME_TYPE frameType = (AltiModule::TTC_FRAME_TYPE) enterInt("Frame type (0=short, 1=long)          ", 0, AltiModule::TTC_FRAME_TYPE_NUMBER - 1);
        unsigned int data;
        switch (frameType) {
            case AltiModule::TTC_FRAME_TYPE::SHORT:
                data = enterHex("Data                    ", 0, 0xff); // 8 bits
                if (alti->ENCAsyncCommandPutShort(data) != AltiModule::SUCCESS) {
                    CERR("ALTI: sending short command (data = 0x%02x)", data);
                    return AltiModule::FAILURE;
                }
                break;
            case AltiModule::TTC_FRAME_TYPE::LONG:
                unsigned int addr = enterHex("TTCrx address           ", 0, 0x3fff); // 14 bits
                AltiModule::TTC_ADDRESS_SPACE addressSpace = (AltiModule::TTC_ADDRESS_SPACE) enterInt("Address space (0=internal, 1=external)", 0, AltiModule::TTC_ADDRESS_SPACE_NUMBER - 1);
                unsigned int subAddr = enterHex("Subaddress              ", 0, 0xff); // 8 bits
                data = enterHex("Data                    ", 0, 0xff); // 8 bits
                if (alti->ENCAsyncCommandPutLong(addr, addressSpace, subAddr, data) != AltiModule::SUCCESS ) {
                    CERR("ALTI: sending long command (\"%s\", addr = 0x%04x, subAddr = 0x%02x, data = 0x%02x)", AltiModule::TTC_ADDRESS_SPACE_NAME[addressSpace].c_str(), addr, subAddr, data);
                    return AltiModule::FAILURE;
                }
                break;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCAsyncCommandPending : public RCD::MenuItem {
  public:
    AltiENCAsyncCommandPending() { setName("read    BGo command pending status"); }
    int action() {

        int rtnv(0);

        bool pending_short, pending_long;
        if ((rtnv = alti->ENCAsyncCommandPendingShort(pending_short)) != AltiModule::SUCCESS) {
            CERR("reading async short command pending status", "");
            return (rtnv);
        }
	if ((rtnv = alti->ENCAsyncCommandPendingLong(pending_long)) != AltiModule::SUCCESS) {
            CERR("reading async long command pending status", "");
            return (rtnv);
        }
	if(pending_short && pending_long) {
	  CERR("Both short and long commands pending", "");
	  return AltiModule::FAILURE;
	}
        std::printf("\n");
	if(pending_short) std::printf("Pending status: sending of SHORT command in progress...");
	else if(pending_long) std::printf("Pending status: sending of LONG command in progress...");
	else std::printf("Pending status: last command sent, ready for the next one");

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCRequestStatus : public RCD::MenuItem {
  public:
    AltiENCRequestStatus() { setName("read    BGo command requests status"); }
    int action() {

	bool vme, ttyp, bgo0, bgo1, bgo2, bgo3;

	if (alti->ENCBgoRequestStatusRead(vme, ttyp, bgo0, bgo1, bgo2, bgo3) != AltiModule::SUCCESS) {
	  CERR("reading BGo requests status", "");
	  return AltiModule::FAILURE;
	}
    
	std::printf("\n");
	std::printf("+----------------------------+\n");
	std::printf("| Command waiting to be sent |\n");
	std::printf("+----------------------------+\n");
	std::printf("| from VME   :     %-5s     |\n", vme ? "TRUE" : "FALSE");
	std::printf("| from TTYP  :     %-5s     |\n", ttyp ? "TRUE" : "FALSE");
	std::printf("| from BGo-0 :     %-5s     |\n", bgo0 ? "TRUE" : "FALSE");
	std::printf("| from BGo-1 :     %-5s     |\n", bgo1 ? "TRUE" : "FALSE");
	std::printf("| from BGo-2 :     %-5s     |\n", bgo2 ? "TRUE" : "FALSE");
	std::printf("| from BGo-3 :     %-5s     |\n", bgo3 ? "TRUE" : "FALSE");
	std::printf("+----------------------------+\n");
	std::printf("\n");

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiENCRequestReset : public RCD::MenuItem {
  public:
    AltiENCRequestReset() { setName("reset   BGo command requests"); }
    int action() {

	unsigned int fifo_choice = enterInt("FIFO (0=\"BGO0\", 1=\"BGO1\", 2=\"BGO2\", 3=\"BGO3\", 4=all)", 0, AltiModule::TTC_FIFO_NUMBER);

        AltiModule::TTC_FIFO ttcFifo;
        if (fifo_choice < AltiModule::TTC_FIFO_NUMBER) {
	  ttcFifo = (AltiModule::TTC_FIFO) fifo_choice;

            if (alti->ENCBgoRequestReset(ttcFifo) != AltiModule::SUCCESS) {
                CERR("reseting BGo request for \"%s\"", AltiModule::TTC_FIFO_NAME[ttcFifo].c_str());
                return AltiModule::FAILURE;
	    }
	}
	else {
	  if (alti->ENCBgoRequestReset() != AltiModule::SUCCESS) {
	    CERR("reseting all BGo requests", "");
	    return AltiModule::FAILURE;
	  }
	}	
        return AltiModule::SUCCESS;	
    }
};

//------------------------------------------------------------------------------

class AltiENCRequestCounterRead : public RCD::MenuItem {
  public:
    AltiENCRequestCounterRead() { setName("read    BGo command counter"); }
    int action() {

	unsigned int i, value;
        AltiModule::TTC_FIFO ttcFifo;

	 std::printf("\n");
        std::printf("+-------------------------------------------+\n");
        std::printf("| Counter     | Data (hex) | Data (decimal) |\n");
        std::printf("+-------------------------------------------+\n");

        for (i = 0; i < AltiModule::TTC_FIFO_NUMBER; i++) {
            ttcFifo = (AltiModule::TTC_FIFO) i;

	    if (alti->ENCBgoCounterRead(ttcFifo, value) != AltiModule::SUCCESS) {
	      CERR("reading BGo counter for \"%s\"", AltiModule::TTC_FIFO_NAME[ttcFifo].c_str());
	      return AltiModule::FAILURE;
	    }
	    std::printf("| BGo-%d      | | 0x%08x | %010u     |\n", i, value, value);
	}
	std::printf("+-------------------------------------------+\n");
	return AltiModule::SUCCESS;	
    }
};

//------------------------------------------------------------------------------

class AltiENCRequestCounterReset : public RCD::MenuItem {
  public:
    AltiENCRequestCounterReset() { setName("reset   BGo command requests counter"); }
    int action() {

	unsigned int fifo_choice = enterInt("FIFO (0=\"BGO0\", 1=\"BGO1\", 2=\"BGO2\", 3=\"BGO3\", 4=all)", 0, AltiModule::TTC_FIFO_NUMBER);
	
        AltiModule::TTC_FIFO ttcFifo;
        if (fifo_choice < AltiModule::TTC_FIFO_NUMBER) {
	  ttcFifo = (AltiModule::TTC_FIFO) fifo_choice;
	  
	  if (alti->ENCBgoCounterReset(ttcFifo) != AltiModule::SUCCESS) {
	    CERR("reseting BGo counter for \"%s\"", AltiModule::TTC_FIFO_NAME[ttcFifo].c_str());
	    return AltiModule::FAILURE;
	  }
	}
	else {
	  if (alti->ENCBgoCounterReset() != AltiModule::SUCCESS) {
	    CERR("reseting all BGo counterss", "");
	    return AltiModule::FAILURE;
	  }
	}	
        return AltiModule::SUCCESS;	
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiDECStatusRead : public RCD::MenuItem {
  public:
    AltiDECStatusRead() { setName("status"); }
    int action() {

      // enable status
      bool enabled;
      if (alti->DECEnableRead(enabled) != AltiModule::SUCCESS) {
	CERR("reading TTC decoder enable", "");
            return AltiModule::FAILURE;
      }

      // status flags
      bool MemoryLooped, MultiCounterFull, RamFull, DecoderReady, CommunicationError, singleBitError, doubleBitError, CdrLosError, CdrLolError, CdrPllLock, RunStatus;
      if (alti->DECStatusRead(MemoryLooped, MultiCounterFull, RamFull, DecoderReady, CommunicationError, singleBitError, doubleBitError, CdrLosError, CdrLolError, CdrPllLock, RunStatus) != AltiModule::SUCCESS) {
	CERR("reading TTC decoder status", "");
	return AltiModule::FAILURE;
      }
      
      // current address
      unsigned int cur;
      if (alti->DECCurrentAddressRead(cur) != AltiModule::SUCCESS) {
	CERR("reading TTC decoder snapshot current address", "");
	return AltiModule::FAILURE;
      }
      
      // loop
      bool loop;
      if (alti->DECLoopRead(loop) != AltiModule::SUCCESS) {
	CERR("reading TTC decoder loop mode", "");
	return AltiModule::FAILURE;
      }

      // trigger condition;
      bool enable;
      short cmd, addr, sub_addr;
      short cmd_mask, addr_mask, sub_addr_mask;
      bool l1a, ext, adr_strb, bcr_strb, single_errs, double_err;
      if (alti->DECTriggerWordRead(enable, cmd, addr, sub_addr, cmd_mask, addr_mask, sub_addr_mask, l1a, ext, adr_strb, bcr_strb, single_errs, double_err) != AltiModule::SUCCESS) {
	CERR("reading TTC decoder snapshot current address", "");
	return AltiModule::FAILURE;
      }
      
      // counters
      u_int cnt_long_cmd, cnt_short_cmd, cnt_l1a;
      if (alti->DECCountersRead(cnt_long_cmd, cnt_short_cmd, cnt_l1a) != AltiModule::SUCCESS) {
	CERR("reading TTC decoder counters", "");
	return AltiModule::FAILURE;
      }

      // orbit counters
      u_int orb, orb_short, orb_long, orb_min, orb_max;
      if (alti->DECOrbitCountersRead(orb, orb_short, orb_long, orb_min, orb_max) != AltiModule::SUCCESS) {
	CERR("reading TTC decoder orbit counters", "");
	return AltiModule::FAILURE;
      }
      
      printf("\n");
      printf("Status :%s", RunStatus ? "running" : "stopped");
      printf("\n");
      printf("Receiver : %s\n", enabled ? "enabled" : "disabled");

      printf("\n");
      printf("Current address = 0x%08x (= %10u)\n", cur, cur);
      printf("Loop mode : %s \n", loop ? "enabled" : "disabled");
      printf("Memory looped : %s \n", MemoryLooped ? "yes": "no");
      printf("\n");
      
      printf("Starting condition: %s\n", enable ? "Enabled" : "Disabled");
      printf("     command       = 0x%02x, mask = 0x%02x \n", cmd, cmd_mask); 
      printf("     address       = 0x%04x, mask = 0x%04x \n", addr, addr_mask);
      printf("     sub-address   = 0x%04x, mask = 0x%04x \n", sub_addr, sub_addr_mask);
      printf("     address space = %s \n", ext ? "external" : "internal");
      printf("     any address   = %s \n", adr_strb ? "yes" : "no");
      printf("     any broadcast = %s \n", bcr_strb ? "yes" : "no");
      printf("     L1A           = %s \n", l1a ? "yes" : "no");
      printf("     single error  = %s \n", single_errs ? "yes" : "no");
      printf("     double error  = %s \n", double_err ? "yes" : "no");

      printf("\n");

      printf("BCR word = 0x%02x \n", m_bcr_word);
      std::printf("BCR delay = %d BCID%s \n", m_bcr_delay, fabs(m_bcr_delay) <= 1 ? "" : "s");
      printf("BCR bit-wise decoding scheme = %s \n", m_bcr_bitwise ? ("enabled") : ("disabled"));
      printf("\n");
            
      printf("+-----------------------------------------+\n");
      printf("|              status flags               |\n");
      printf("+-----------------------------------------+\n");
      printf("| Decoder             : %-9s (sticky) |\n", DecoderReady ? "ready" : "not ready");
      printf("| Communication       : %-9s (sticky) |\n", CommunicationError ? "error" : "OK");
      printf("| Single bit error    : %-9s (sticky) |\n", singleBitError ? "error" : "OK");
      printf("| Double bit error    : %-9s (sticky) |\n", doubleBitError ? "error" : "OK");
      printf("| CDR loss of signal  : %-9s (sticky) |\n", CdrLosError ? "LOS" : "OK");
      printf("| CDR loss of lock    : %-9s (sticky) |\n", CdrLolError ? "LOL" : "OK");
      printf("| CDR pll lock signal : %-9s (sticky) |\n", CdrPllLock ? "OK" : "error");
      printf("| counters            : %-9s (sticky) |\n", MultiCounterFull ? "full" : "not full");
      printf("| RAM                 : %-9s (sticky) |\n", RamFull ? "full" : "not full");
      printf("+-----------------------------------------+\n");
      
      printf("\n");
      
      printf("+-------------------------------+\n");
      printf("|       counters values         |\n");
      printf("+-------------------------------+\n");
      printf("| L1A            : %010u   |\n", cnt_l1a);
      printf("| Short commands : %010u   |\n", cnt_short_cmd);
      printf("| Long commands  : %010u   |\n", cnt_long_cmd);
      printf("+-------------------------------+\n");
      
      printf("\n");
      
      printf("+-------------------------------+\n");
      printf("|    Orbit counters values      |\n");
      printf("+-------------------------------+\n");
      printf("| Orbits         : %010u   |\n", orb);
      printf("| Short orbits   : %010u   |\n", orb_short);
      printf("| Long orbits    : %010u   |\n", orb_long);
      printf("| min length     : %010u   |\n", orb_min);
      printf("| max lenght     : %010u   |\n", orb_max);
      printf("+-------------------------------+\n");
      

      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiDECEnable : public RCD::MenuItem {
  public:
    AltiDECEnable() { setName("enable  TTC decoder"); }
    int action() {

        if (alti->DECEnableWrite(true) != AltiModule::SUCCESS) {
            CERR("enabling TTC decoder snapshot memory", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiDECDisable : public RCD::MenuItem {
  public:
    AltiDECDisable() { setName("disable TTC decoder"); }
    int action() {

        if (alti->DECEnableWrite(false) != AltiModule::SUCCESS) {
            CERR("disabling TTC decoder snapshot memory", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------


class AltiDECStatusClear : public RCD::MenuItem {
  public:
    AltiDECStatusClear() { setName("clear   status flags"); }
    int action() {

        if (alti->DECStatusClear() != AltiModule::SUCCESS) {
            CERR("clearing all TTC decoder status bits", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiDECCounterReset : public RCD::MenuItem {
  public:
    AltiDECCounterReset() { setName("reset   counters"); }
    int action() {

        if (alti->DECCounterReset() != AltiModule::SUCCESS) {
            CERR("resetting TTC decoder counters","");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiDECReset : public RCD::MenuItem {
  public:
    AltiDECReset() { setName("reset   TTC decoder"); }
    int action() {

        if (alti->DECReset() != AltiModule::SUCCESS) {
            CERR("resetting TTC decoder","");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiDECSetBcrWord : public RCD::MenuItem {
  public:
  AltiDECSetBcrWord() { setName("set     BCR scheme"); }
  int action() {
    m_bcr_word = enterHex("BCR bit (default = 0x1)", 0, 0xff);
    m_bcr_delay = enterInt("BCR delay applied after the ALTI(default = 0)", -3563, 3563);
    m_bcr_bitwise = enterInt("bit-wise decoding scheme 0=disable, 1=enable (defaul). If enabled, any word containing the BCR bit will be interpreted as BCR", 0, 1);;
    return AltiModule::SUCCESS;
  }
};

//------------------------------------------------------------------------------

class AltiDECSetLoop : public RCD::MenuItem {
  public:
  AltiDECSetLoop() { setName("set     Loop mode"); }
  int action() {
    bool loop= (bool) enterInt("Set loop mode: 0 = disabled, 1 = enabled", 0, 1);;

    if (alti->DECLoopWrite(loop) != AltiModule::SUCCESS) {
      CERR("writing TTC decoder loop mode", "");
      return AltiModule::FAILURE;
    }   

    return AltiModule::SUCCESS;
  }
};


//------------------------------------------------------------------------------

class AltiDECSetTrigCond : public RCD::MenuItem {
  public:
  AltiDECSetTrigCond() { setName("set     Trigger Condition"); }
  int action() {
    bool enabled = (bool) enterInt("0 : Free run, 1 : On trigger",0,1);
    if (enabled){
      int trig_word = enterHex("Command word: ", 0, 0xff);
      int mask_word = enterHex("Command mask: ", 0, 0xff);
      int trig_laddr = enterHex("External Address: ", 0, 0x3fff);
      int mask_laddr = enterHex("External Address mask: ", 0, 0x3fff);
      int trig_saddr = enterHex("Internal Address: ", 0, 0xff);
      int mask_saddr = enterHex("Iinternal Address mask: ", 0, 0xff);
      int trig_IE = enterInt("Address space: 0 = internal, 1 = external",0,1);
      int trig_astrb = enterInt("Any addressed command: ",0,1);
      int trig_sstrb = enterInt("Any broadcast command: ",0,1);
      int trig_l1a = enterInt("L1A: ",0,1);
      int trig_dbl = enterInt("Double bit error: ",0,1);
      int trig_sngl = enterInt("Single bit error: ",0,1);
    
      if (alti->DECTriggerWordWrite(enabled,trig_word,mask_word,trig_laddr,mask_laddr,trig_saddr,mask_saddr,trig_l1a,trig_IE,trig_astrb,trig_sstrb,trig_dbl,trig_sngl) != AltiModule::SUCCESS) {
              CERR("clearing all TTC decoder status bits", "");
              return AltiModule::FAILURE;
          }   
    }
    else {
      if (alti->DECTriggerWordWrite(enabled,0,0,0,0,0,0,0,0,0,0,0,0) != AltiModule::SUCCESS) {
              CERR("clearing all TTC decoder status bits", "");
              return AltiModule::FAILURE;
          }   
    }
    return AltiModule::SUCCESS;
  }
};


//------------------------------------------------------------------------------

class AltiMEMDataDECReadNumber : public MenuItem {
  public:
    AltiMEMDataDECReadNumber() { setName("read    memory [number]"); }
    int action() {

        if (alti->DECEnableWrite(false) != AltiModule::SUCCESS) {
            CERR("disabling TTC decoder", "");
            return AltiModule::FAILURE;
        }

        unsigned int indx, data0, data1;
        indx = enterHex("address index", 0, AltiModule::MAX_MEMORY - 1);
        if (alti->MEMDataRead(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_COMMAND, data0, indx) != AltiModule::SUCCESS) {
            CERR("reading TTC decoder command memory", "");
            return AltiModule::FAILURE;
        }
        if (alti->MEMDataRead(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_TIMESTAMP, data1, indx) != AltiModule::SUCCESS) {
            CERR("reading TTC decoder timestamp memory", "");
            return AltiModule::FAILURE;
        }

        std::printf("\n");
        std::printf("data[%6d] = 0x%08x, 0x%08x (= %10u, %10u)\n", indx, data0, data1, data0, data1);
        std::printf("\n");

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMEMDataDECWriteNumber : public MenuItem {
  public:
    AltiMEMDataDECWriteNumber() { setName("write   memory [number]"); }
    int action() {

        if (alti->DECEnableWrite(false) != AltiModule::SUCCESS) {
            CERR("disabling TTC decoder", "");
            return AltiModule::FAILURE;
        }

        unsigned int indx, data0, data1;
        indx = enterHex("address index                ", 0, AltiModule::MAX_MEMORY - 1);
        data0 = enterHex("data (most  significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        data1 = enterHex("data (least significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        if (alti->MEMDataWrite(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_COMMAND, data0, indx) != AltiModule::SUCCESS) {
            CERR("writing TTC decoder command memory", "");
            return AltiModule::FAILURE;
        }
        if (alti->MEMDataWrite(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_TIMESTAMP, data1, indx) != AltiModule::SUCCESS) {
            CERR("writing TTC decoder timestamp memory", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMEMDataDECReadVector : public MenuItem {
  public:
    AltiMEMDataDECReadVector() { setName("read    memory [vector]"); }
    int action() {

        if (alti->DECEnableWrite(false) != AltiModule::SUCCESS) {
            CERR("disabling TTC decoder", "");
            return AltiModule::FAILURE;
        }

        std::vector<unsigned int> data0, data1;

        unsigned int vme_off = enterHex("starting index", 0, AltiModule::MAX_MEMORY - 1);
        unsigned int size = enterHex("size          ", 1, AltiModule::MAX_MEMORY - vme_off);
        data0.resize(size);
        data1.resize(size);

        if (alti->MEMDataRead(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_COMMAND, data0, vme_off) != AltiModule::SUCCESS) {
            CERR("reading TTC decoder command memory", "");
            return AltiModule::FAILURE;
        }
        if (alti->MEMDataRead(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_TIMESTAMP, data1, vme_off) != AltiModule::SUCCESS) {
            CERR("reading TTC decoder timestamp memory", "");
            return AltiModule::FAILURE;
        }

        unsigned int indx;
        for (indx = 0; indx < size; indx++) {
            std::printf("data[%6d] = 0x%08x, 0x%08x (= %10u, %10u)\n", vme_off + indx, data0[indx], data1[indx], data0[indx], data1[indx]);
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMEMDataDECWriteVector : public MenuItem {
  public:
    AltiMEMDataDECWriteVector() { setName("write   memory [vector]"); }
    int action() {

        if (alti->DECEnableWrite(false) != AltiModule::SUCCESS) {
            CERR("disabling TTC decoder", "");
            return AltiModule::FAILURE;
        }

        std::vector<unsigned int> data0, data1;

        unsigned int vme_off = enterHex("starting index                          ", 0, AltiModule::MAX_MEMORY - 1);
        unsigned int size = enterHex("size                                    ", 1, AltiModule::MAX_MEMORY - vme_off);
        unsigned int sdat0 = enterHex("starting  value (most  significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        unsigned int idat0 = enterHex("increment value (most  significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        unsigned int sdat1 = enterHex("starting  value (least significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        unsigned int idat1 = enterHex("increment value (least significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        unsigned int xdat0(sdat0), xdat1(sdat1), indx;

        for (indx = 0; indx < size; indx++) {
            data0.push_back(xdat0 & ALTI::MASK_MEM_SHAREDALTIRAM);
            xdat0 += idat0;
            data1.push_back(xdat1 & ALTI::MASK_MEM_SHAREDALTIRAM);
            xdat1 += idat1;
        }

        if (alti->MEMDataWrite(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_COMMAND, data0, vme_off) != AltiModule::SUCCESS) {
            CERR("writing TTC decoder command memory", "");
            return AltiModule::FAILURE;
        }
        if (alti->MEMDataWrite(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_TIMESTAMP, data1, vme_off) != AltiModule::SUCCESS) {
            CERR("writing TTC decoder timestamp memory", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMEMDataDECReadBlock : public MenuItem {
  public:
    AltiMEMDataDECReadBlock() { setName("read    memory [block]"); }
    int action() {

        if (alti->DECEnableWrite(false) != AltiModule::SUCCESS) {
            CERR("disabling TTC decoder", "");
            return AltiModule::FAILURE;
        }

        if (alti->MEMDataRead(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_COMMAND, segment[0], AltiModule::MAX_MEMORY, 0, 0) != AltiModule::SUCCESS) {
            CERR("reading TTC decoder command memory in block transfer", "");
            return AltiModule::FAILURE;
        }
        if (alti->MEMDataRead(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_TIMESTAMP, segment[1], AltiModule::MAX_MEMORY, 0, 0) != AltiModule::SUCCESS) {
            CERR("reading TTC decoder timestamp memory in block transfer", "");
            return AltiModule::FAILURE;
        }

        unsigned int indx;
        unsigned int *data0 = (unsigned int *) segment[0]->VirtualAddress();
        unsigned int *data1 = (unsigned int *) segment[1]->VirtualAddress();
        for (indx = 0; indx < AltiModule::MAX_MEMORY; indx++) {
            std::printf("data[%6d] = 0x%08x, 0x%08x (= %10u, %10u)\n", indx, data0[indx], data1[indx], data0[indx], data1[indx]);
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMEMDataDECWriteBlock : public MenuItem {
  public:
    AltiMEMDataDECWriteBlock() { setName("write   memory [block]"); }
    int action() {

        if (alti->DECEnableWrite(false) != AltiModule::SUCCESS) {
            CERR("disabling TTC decoder", "");
            return AltiModule::FAILURE;
        }

        unsigned int sdat0 = enterHex("starting  value (most  significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        unsigned int idat0 = enterHex("increment value (most  significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        unsigned int sdat1 = enterHex("starting  value (least significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        unsigned int idat1 = enterHex("increment value (least significant word)", 0, ALTI::MASK_MEM_SHAREDALTIRAM);
        unsigned int xdat0(sdat0), xdat1(sdat1), indx;
        unsigned int* data0 = (unsigned int*) segment[0]->VirtualAddress();
        unsigned int* data1 = (unsigned int*) segment[1]->VirtualAddress();

        for (indx = 0; indx < AltiModule::MAX_MEMORY; indx++) {
            data0[indx] = xdat0 & ALTI::MASK_MEM_SHAREDALTIRAM;
            xdat0 += idat0;
            data1[indx] = xdat1 & ALTI::MASK_MEM_SHAREDALTIRAM;
            xdat1 += idat1;
        }

        if (alti->MEMDataWrite(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_COMMAND, segment[0], AltiModule::MAX_MEMORY, 0, 0) != AltiModule::SUCCESS) {
            CERR("writing TTC decoder command memory in block transfer", "");
            return AltiModule::FAILURE;
        }
        if (alti->MEMDataWrite(AltiModule::ALTI_RAM_MEMORY::TTC_DECODER_TIMESTAMP, segment[1], AltiModule::MAX_MEMORY, 0, 0) != AltiModule::SUCCESS) {
            CERR("writing TTC decoder timestamp memory in block transfer", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiDECRead : public MenuItem {
  public:
    AltiDECRead() { setName("read    TTC decoder memory"); }
    int action() {

        unsigned int vme_off = enterInt("Starting index", 0, AltiModule::MAX_MEMORY - 1);
        unsigned int size = enterInt("Size          ", 1, AltiModule::MAX_MEMORY - vme_off);

        if (alti->DECRead(segment[0], segment[1], vme_off, size, m_bcr_word, m_bcr_delay) != AltiModule::SUCCESS) {
            CERR("reading TTC decoder memory. If the TTC decoder monitoring is still enabled, disable it before reading the memory", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiDECReadFile : public RCD::MenuItem {
  public:
    AltiDECReadFile() { setName("read    TTC decoder memory [into file]"); }
    int action() {

        unsigned int vme_off = enterInt("Starting index                   ", 0, AltiModule::MAX_MEMORY - 1);
        unsigned int size = enterInt("Size                             ", 1, AltiModule::MAX_MEMORY - vme_off);

        int format = enterInt("File format [0-Hex/1-Human-readable/2-TTCScope]", 0, 2);

        std::string fn = enterString("File name                                             ");
        std::ofstream outf(fn.c_str(), std::ofstream::out);
        if (!outf) {
            CERR("cannot open output file \"%s\"", fn.c_str());
           return AltiModule::FAILURE;
        }

        if (alti->DECReadFile(segment[0], segment[1], vme_off, size, m_bcr_word, m_bcr_delay, format, outf) != AltiModule::SUCCESS) {
            CERR("reading TTC decoder memory into file. If the TTC decoder monitoring is still enabled, disable it before reading the memory", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiCLKStatusRead : public RCD::MenuItem {
  public:
    AltiCLKStatusRead() { setName("status"); }
    int action() {

        int rtnv(0);

        AltiModule::CLK_PLL_TYPE pll_selection;
        if ((rtnv = alti->CLKInputSelectReadPLL(pll_selection)) != AltiModule::SUCCESS) {
            CERR("reading CLK PLL input selection", "");
            return (rtnv);
        }

        bool lol, los, sticky_lol, sticky_los;
        if ((rtnv = alti->CLKStatusReadPLL(lol, los, sticky_lol, sticky_los)) != AltiModule::SUCCESS) {
            CERR("reading CLK PLL status", "");
            return (rtnv);
        }

	bool jc_intr, jc_lol, jc_los, jc_sticky_intr, jc_sticky_lol, jc_sticky_los;
        if ((rtnv = alti->CLKStatusReadJitterCleaner(jc_intr, jc_lol, jc_los, jc_sticky_intr, jc_sticky_lol, jc_sticky_los)) != AltiModule::SUCCESS) {
	  CERR("reading CLK jitter cleaner status", "");
	  return (rtnv);
        }

	u_int i;
	AltiModule::EXT_CLK_SOURCE src;
	bool stopped[AltiModule::EXT_CLK_SOURCE_NUMBER], oor[AltiModule::EXT_CLK_SOURCE_NUMBER], glitch[AltiModule::EXT_CLK_SOURCE_NUMBER];
	for(i = 0; i < AltiModule::EXT_CLK_SOURCE_NUMBER; i++) {
	  src = AltiModule::EXT_CLK_SOURCE(i);
	  if ((rtnv = alti->CLKMonitorReadPLL(src, stopped[i], oor[i], glitch[i])) != AltiModule::SUCCESS) {
            CERR("reading CLK PLL monitor status", "");
            return (rtnv);
	  }
	}

        std::string designid;
        if ((rtnv = alti->CLKJitterCleanerDesignIDRead(designid)) != AltiModule::SUCCESS) {
            CERR("reading design ID from I2C JitterCleaner", "");
            return (rtnv);
        }
       
        AltiModule::CLK_JC_TYPE jc_selection;
        if ((rtnv = alti->CLKInputSelectReadJitterCleaner(jc_selection)) != AltiModule::SUCCESS) {
            CERR("reading JC input selection", "");
            return (rtnv);
        }

	// from I2C
	std::vector<bool> v_jc_oof, v_jc_los;
        bool jc_hold, jc_lol2;
        if ((rtnv = alti->CLKJitterCleanerStatusRead(v_jc_oof, v_jc_los, jc_hold, jc_lol2)) != AltiModule::SUCCESS) {
	  CERR("reading statuses from I2C JitterCleaner", "");
	  return (rtnv);
        }
        std::vector<bool> v_jc_sticky_oof, v_jc_sticky_los;
        bool jc_sticky_hold, jc_sticky_lol2;
        if ((rtnv = alti->CLKJitterCleanerStickyStatusRead(v_jc_sticky_oof, v_jc_sticky_los, jc_sticky_hold, jc_sticky_lol2)) != AltiModule::SUCCESS) {
	  CERR("reading sticky statuses from I2C JitterCleaner", "");
	  return (rtnv);
        }

        signed int steps;
        if ((rtnv = alti->CLKPhasePositionReadPLL(steps)) != AltiModule::SUCCESS) {
            CERR("reading PLL output clock phase position", "");
            return (rtnv);
        }

        std::printf("\n");
        std::printf("PLL  input  clock: %s, %s (sticky) \n", ((pll_selection == AltiModule::FROM_SWITCH) ? "from switch" : ((pll_selection == AltiModule::JITTER_CLEANER) ? "from jitter cleaner" : "")), sticky_los ? "loss of signal" : "OK");
        std::printf("PLL  output clock: %-3s(current)%s \n", lol ? "ERR" : "OK", sticky_lol ? ", loss of lock (sticky)" : "");

        std::printf("\n");
	std::printf("PLL  output phase shift: %d steps ~ %5.2f ns \n", steps, steps*0.015);

        std::printf("\n");
        std::printf("Jitter cleaner input  clock: %s, %s (sticky)  \n", ((jc_selection == AltiModule::OSCILLATOR) ? "from oscillator" : ((jc_selection == AltiModule::SWITCH) ? "from switch" : "")), jc_sticky_los ? "loss of signal" : "OK");
	std::printf("Jitter cleaner  output clock: %-3s(current)%s \n", jc_lol ? "ERR" : "OK", jc_sticky_lol ? ", loss of lock (sticky)" : "");
        std::printf("Jitter cleaner design ID: \"%s\"\n", designid.c_str());
        std::printf("Jitter cleaner interrupt: %s (sticky)\n", jc_intr ? "active" : "inactive");

        std::printf("%s", jc_hold ? "\nWarning: jitter cleaner is in holdover mode!\n" : "");

        std::printf("\n");
        std::printf("(read from the VME)\n");
        std::printf("+-----------------------------------------------------------+\n");
        std::printf("| %-20s || %-7s | %-12s | %-8s |\n", "PLL input monitoring", "Signal", "Frequency", "Glitches");
        std::printf("+-----------------------------------------------------------+\n");
        for (i = 0; i < AltiModule::EXT_CLK_SOURCE_NUMBER; i++) {
            src = (AltiModule::EXT_CLK_SOURCE) i;
            std::printf("| %-20s || %-7s | %-12s | %-8s | (sticky)\n", AltiModule::EXT_CLK_SOURCE_NAME[src].c_str(), stopped[i] ? "stopped" : "OK", oor[i] ? "out of range" : "OK", glitch[i] ? "glitch" : "OK");
            std::printf("+-----------------------------------------------------------+\n");
        }

        std::string jc_input_name;
        std::printf("\n");
        std::printf("(read from the I2C)\n");
        std::printf("+---------------------------------------------------------------------------------------+\n");
        std::printf("| %-31s || %-21s | %-26s |\n", "Jitter cleaner input monitoring", "Signal", "Frequency");
        std::printf("+---------------------------------------------------------------------------------------+\n");
        for (i = 0; i < 2; i++) {
            switch (i) {
                case 0:
                    jc_input_name = "from oscillator";
                    break;
                case 1:
                    jc_input_name = "from switch";
                    break;
                default:
                    break;
            }
            std::printf("| %-31s || %-3s%-18s | %-3s%-23s | \n", jc_input_name.c_str(), v_jc_los[i] ? "ERR" : "OK", v_jc_sticky_los[i] ? ", stopped (sticky)" : "", v_jc_oof[i] ? "ERR" : "OK", v_jc_sticky_oof[i] ? ", out of range (sticky)" : "");
            std::printf("+---------------------------------------------------------------------------------------+\n");
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCLKSelectWrite : public RCD::MenuItem {
  public:
    AltiCLKSelectWrite() { setName("select PLL input  clock"); }
    int action() {

        AltiModule::CLK_PLL_TYPE clk = (AltiModule::CLK_PLL_TYPE) enterInt("Input clock (0=\"from the jitter cleaner\", 1=\"from the switch\")", 0, 1);
        if (alti->CLKInputSelectWritePLL(clk) != AltiModule::SUCCESS) {
            CERR("writing %s clock selection", AltiModule::CLK_PLL_NAME[clk].c_str());
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiCLKSelectToggle : public RCD::MenuItem {
  public:
    AltiCLKSelectToggle() { setName("toggle PLL input  clock selection"); }
    int action() {

        if (alti->CLKInputSelectTogglePLL() != AltiModule::SUCCESS) {
            CERR("toggling clock selection", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiCLKPhaseShift : public RCD::MenuItem {
  public:
    AltiCLKPhaseShift() { setName("shift  PLL output clock phase"); }
    int action() {
      short max = 2000;
      short min = -2000;
      bool inc  = (bool) enterInt("Shift direction (0=decrement, 1=increment)", 0, 1);
      short steps = enterInt("Number of steps (~15ps each)     ", 0, 0x000007ff);
      int current;
      if (alti->CLKPhasePositionReadPLL(current) != AltiModule::SUCCESS){
	CERR("reading PLL output clock phase position", "");
	return AltiModule::FAILURE;
      } 
      
      if ((current >= 0) and (30000 > current) and inc) { 
	if ((current + steps) > max) {
	  CERR("Phase shift cannot exceed %d", max);
	  return AltiModule::FAILURE;
	}
      }
      else if (current > 30000 and not inc){
	if ((current - 65535 - steps) < min) { 
	  CERR("Phase shift cannot exceed %d", min);
	  return AltiModule::FAILURE;
	}
      }
      if (alti->CLKPhaseShiftPLL(inc, steps) != AltiModule::SUCCESS) {
	CERR("%s PLL output clock phase by %d steps", inc ? "incrementing" : "decrementing", steps);
	return AltiModule::FAILURE;
      }
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiCLKPhaseReset : public RCD::MenuItem {
  public:
    AltiCLKPhaseReset() { setName("reset  PLL output clock phase"); }
    int action() {

        if (alti->CLKPhaseResetPLL() != AltiModule::SUCCESS) {
            CERR("resetting PLL output clock phase", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiCLKReset : public RCD::MenuItem {
  public:
    AltiCLKReset() { setName("reset  PLL"); }
    int action() {

        if (alti->CLKResetPLL() != AltiModule::SUCCESS) {
            CERR("resetting PLL", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiCLKStatusClear : public RCD::MenuItem {
  public:
    AltiCLKStatusClear() { setName("clear  status flags"); }
    int action() {
        int rtnv(0);
    
	unsigned int clear_choice = enterInt("Flags to clear (0=PLL, 1=jitter cleaner, 2=all)", 0, 2);

        switch (clear_choice) {
            case 0:
                if ((rtnv = alti->CLKStickyBitsReset()) != AltiModule::SUCCESS){
		  CERR("clearing PLL Status Flags","");
		  return (rtnv);
		}
                break;
	case 1:
	  if ((rtnv = alti->CLKJitterCleanerStatusClear()) != AltiModule::SUCCESS) {
	    CERR("clearing jitter cleaner status flags (I2C)", "");
	    return (rtnv);
	  }
	  if ((rtnv = alti->CLKStatusClearJitterCleaner()) != AltiModule::SUCCESS) {
	    CERR("clearing jitter cleaner status flags (VME)", "");
	    return (rtnv);
	  }
	  break;
	case 2: // all
	  if ((rtnv = alti->CLKJitterCleanerStatusClear()) != AltiModule::SUCCESS) {
	    CERR("clearing jitter cleaner status flags (I2C)", "");
	    return (rtnv);
	  }
	  if ((rtnv = alti->CLKStatusClearJitterCleaner()) != AltiModule::SUCCESS) {
	    CERR("clearing jitter cleaner status flags (VME)", "");
	    return (rtnv);
	  }
	  if ((rtnv = alti->CLKStickyBitsReset()) != AltiModule::SUCCESS){
	    CERR("clearing PLL Status Flags","");
	    return (rtnv);
	  }
	  break;
	default:
	  break;
        }
    return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCLKJitterCleanerInSelWrite : public RCD::MenuItem {
  public:
    AltiCLKJitterCleanerInSelWrite() { setName("select jitter cleaner input clock"); }
    int action() {
        int rtnv(0);

        AltiModule::CLK_JC_TYPE clk = (AltiModule::CLK_JC_TYPE) enterInt("Input clock (0=\"oscillator\", 1=\"from the switch\")", 0, 1);
        if ((rtnv = alti->CLKInputSelectWriteJitterCleaner(clk)) != AltiModule::SUCCESS) {
            CERR("writing %s jitter cleaner input selection", AltiModule::CLK_JC_NAME[clk].c_str());
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCLKJitterCleanerConfigRead : public RCD::MenuItem {
  public:
    AltiCLKJitterCleanerConfigRead() { setName("read   jitter cleaner configuration [into file]"); }
    int action() {

        int rtnv(0);
        std::string fn_in = enterString("Input  file name");
        std::string fn_out = enterString("Output file name");

        // read CLK jitter cleaner config into file
        if ((rtnv = alti->CLKJitterCleanerConfigRead(fn_in, fn_out)) != AltiModule::SUCCESS) {
            CERR("reading CLK jitter cleaner config with input file \"%s\" into output file \"%s\"", fn_in.c_str(), fn_out.c_str());
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCLKJitterCleanerConfigWrite : public RCD::MenuItem {
  public:
    AltiCLKJitterCleanerConfigWrite() { setName("write  jitter cleaner configuration [from file]"); }
    int action() {

        int rtnv(0);
        std::string fn_in = enterString("Input file name");
        //if (fn_in == "") fn_in = "ALTI/data/Si5344-RevB-ALTI_001-Registers.txt"; // default filename

        // write CLK jitter cleaner config from file
        if ((rtnv = alti->CLKJitterCleanerConfigWrite(fn_in)) != AltiModule::SUCCESS) {
            CERR("writing CLK jitter cleaner config from input file \"%s\"", fn_in.c_str());
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCLKJitterCleanerConfigWriteDefault : public RCD::MenuItem {
  public:
    AltiCLKJitterCleanerConfigWriteDefault() { setName("write  jitter cleaner configuration [default]"); }
    int action() {

        int rtnv(0);
        bool flag = (bool) enterInt("Write default configuration (0=\"NO\", 1=\"YES\")", 0, 1);
        if (!flag) return (rtnv);

        //int rtnv(0);
        // write DEFAULT CLK jitter cleaner config
        if ((rtnv = alti->CLKJitterCleanerConfigWrite()) != AltiModule::SUCCESS) {
            CERR("writing DEFAULT CLK jitter cleaner config", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiCLKJitterCleanerResync : public RCD::MenuItem {
  public:
    AltiCLKJitterCleanerResync() { setName("resync jitter cleaner"); }
    int action() {

        if (alti->CLKJitterCleanerResync() != AltiModule::SUCCESS ) {
            CERR("resyncing jitter cleaner", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiCLKJitterCleanerReset : public RCD::MenuItem {
  public:
    AltiCLKJitterCleanerReset() { setName("reset  jitter cleaner"); }
    int action() {

        if (alti->CLKResetJitterCleaner() != AltiModule::SUCCESS ) {
            CERR("resetting jitter cleaner", "");
            return AltiModule::FAILURE;
        }

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class ECRGenerationRead : public MenuItem {
  public:
    ECRGenerationRead() { setName("read  ECR generation"); }
    int action() {

        int rtnv(0);
        AltiModule::ECR_GENERATION ecr;

        if ((rtnv = alti->ECRGenerationRead(ecr)) != AltiModule::SUCCESS) {
            CERR("reading ECR control", "");
        }
        std::cout << "--------------------------------------------" << std::endl;
        ecr.dump();

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class ECRGenerationWrite : public MenuItem {
  public:
    ECRGenerationWrite() { setName("write ECR generation"); }
    int action() {

        int rtnv(0);
        AltiModule::ECR_GENERATION ecr;

        ecr.type = (AltiModule::ECR_TYPE) enterInt("ECR type (0=\"VME\", 1=\"INTERNAL\")", 0, 1);
        ecr.length = enterInt("ECR length", 1, AltiModule::ECR_LENGTH);
        if (ecr.type == AltiModule::ECR_INTERNAL) {
            ecr.frequency = enterNumber("ECR frequency", 0, AltiModule::ECR_FREQUENCY_MASK);
        }
        ecr.busy_before = enterNumber("ECR BUSY BEFORE length", 0, AltiModule::ECR_BUSY_BEFORE_MASK);
        ecr.busy_after = enterNumber("ECR BUSY AFTER length", 0, AltiModule::ECR_BUSY_AFTER_MASK);
        ecr.orbit_offset = enterNumber("ECR ORBIT offset", 0, AltiModule::ECR_ORBIT_OFFSET_MASK);

        if ((rtnv = alti->ECRGenerationWrite(ecr)) != AltiModule::SUCCESS) {
            CERR("writing ECR control", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class ECRGenerate : public MenuItem {
  public:
    ECRGenerate() { setName("write ECR generate"); }
    int action() {

        int rtnv(0);

        unsigned int loop = enterInt("How many times", 0, 100000);

        for (unsigned int i = 0; i < loop; i++) {
            if ((rtnv = alti->ECRGenerate()) != AltiModule::SUCCESS) {
                CERR("writing ECR generate", "");
                return (rtnv);
            }
            if ((rtnv = alti->ECRReadyWait()) != AltiModule::SUCCESS) {
                CERR("waiting for ECR generation to be ready again", "");
                return (rtnv);
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class T2LControlReset : public MenuItem {
  public:
    T2LControlReset() { setName("reset T2L control"); }
    int action() {

        int rtnv(0);

	bool l1id = static_cast<bool>(enterInt("Reset L1ID (0=no, 1=yes)", 0, 1));
        bool ecrn = static_cast<bool>(enterInt("Reset ECR  (0=no, 1=yes)", 0, 1));
        bool fifo = static_cast<bool>(enterInt("Reset FIFO (0=no, 1=yes)", 0, 1));

        // reset T2L control
        if ((rtnv = alti->T2LControlReset(l1id, ecrn, fifo)) != AltiModule::SUCCESS) {
            CERR("resetting T2L control", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class T2LEventIdentifierRead : public MenuItem {
  public:
    T2LEventIdentifierRead() { setName("read  T2L event identifier [sample]"); }
    int action() {

        int rtnv(0);
        unsigned int sec, i;
        EventIdentifier evid;
        EventCounter ecr, l1a;
        ecr.mask(EventIdentifier::ECRC_MAX);
        l1a.mask(EventIdentifier::L1ID_MAX);

        unsigned int loop = enterInt("How many times", 0, 100000);

        if (loop == 1) {
            if ((rtnv = alti->T2LEventIdentifierRead(evid)) != AltiModule::SUCCESS) {
                CERR("reading T2L event identifier", "");
                return (rtnv);
            }
            std::printf("T2L EVID = 0x%08x (ecr = %3d, l1a = %8d)\n", evid.data(), evid.ECRC(), evid.L1ID());
        }
        else {
            sec = enterInt("How many seconds", 0, 10);
            for (i = 0; i < loop; i++) {
                if (i > 0) sleep(sec);
                if ((rtnv = alti->T2LEventIdentifierRead(evid)) != AltiModule::SUCCESS) {
                    CERR("reading T2L event identifier", "");
                    return (rtnv);
                }
                ecr.data(evid.ECRC());
                l1a.data(evid.L1ID());
                std::printf("T2L EVID[%5d sec]: 0x%08x (ecr = %s, l1a = %s)\n", i*sec, evid.data(), ecr.string().c_str(), l1a.string().c_str());
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class T2LFifoStatusRead : public MenuItem {
  public:
    T2LFifoStatusRead() { setName("read  T2L FIFO status"); }
    int action() {

        int rtnv(0);
        unsigned int lvl;
        bool emp, ful;

        // read T2L FIFO status
        if((rtnv = alti->T2LFifoStatusRead(emp, ful, lvl)) != AltiModule::SUCCESS) {
            CERR("reading T2L FIFO status", "");
            return (rtnv);
        }
        std::printf("T2L FIFO status: %s, %s, level = %2d\n", emp ? "    EMPTY" : "not empty", ful ? "    FULL" : "not full", lvl);

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class T2LFifoWrite : public MenuItem {
  public:
    T2LFifoWrite() { setName("write T2L FIFO"); }
    int action() {

        int rtnv(0);

        // write T2L FIFO
        if ((rtnv = alti->T2LFifoWrite()) != AltiModule::SUCCESS) {
            CERR("writing T2L FIFO", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class T2LFifoRead : public MenuItem {
  public:
    T2LFifoRead() { setName("read  T2L FIFO"); }
    int action() {

        int rtnv(0);
        std::vector<unsigned int> data;
        EventIdentifier evid;

        // read T2L FIFO
        if ((rtnv = alti->T2LFifoRead(data)) != AltiModule::SUCCESS) {
            CERR("reading T2L FIFO", "");
            return (rtnv);
        }
        if (data.empty()) {
           std::printf("T2L FIFO is EMPTY\n");
            return (rtnv);
        }
        for (unsigned int i = 0; i < data.size(); i++) {
            evid.data(data[i]);
            std::printf("T2L FIFO WORD[%2d]: 0x%08x (ecr = %3d, l1a = %8d)\n", i, data[i], evid.ECRC(), evid.L1ID());
        }

        return (rtnv);
    }
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class CNTBcidMaskRead : public MenuItem {
  public:
    CNTBcidMaskRead() { setName("read  CNT BCID mask"); }
    int action() {

        int rtnv(0);
        //unsigned int type = enterInt("CNT type (0=LUT, 1=TBP, 2=TAP, 3=TAV)",0,CtpcorePlusModule::CNT_NUMBER-1);
        unsigned int bcid = enterInt("BCID number (4096=all)", 0, AltiModule::BCID_NUMBER);

        if (bcid == AltiModule::BCID_NUMBER) {
            std::vector<bool> ena;
            if ((rtnv = alti->CNTBcidMaskRead(ena)) != AltiModule::SUCCESS) {
                CERR("reading CNT BCID mask", "");
                return (rtnv);
            }
            std::printf("CNT:\n");
            for (unsigned int i = 0; i < AltiModule::BCID_NUMBER; i++) {
                std::printf("BCID %4d = %s\n", i, ena[i] ? " ENABLED" : "DISABLED");
            }
        }

        else {
            unsigned int num = enterInt("BCID range", 0, AltiModule::BCID_NUMBER - bcid);
            bool ena;
            std::printf("CNT:\n");
            for (unsigned int i = bcid; i < (bcid + num); i++) {
                if ((rtnv = alti->CNTBcidMaskRead(i, ena)) != AltiModule::SUCCESS) {
                    CERR("reading CNT BCID mask %d", i);
                    return (rtnv);
                }
                std::printf("BCID %4d = %s\n", i, ena ? " ENABLED" : "DISABLED");
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTBcidMaskWrite : public MenuItem {
  public:
    CNTBcidMaskWrite() { setName("write CNT BCID mask"); }
    int action() {

        int rtnv(0);
        //unsigned int type = enterInt("CNT type (0=LUT, 1=TBP, 2=TAP, 3=TAV)",0,CtpcorePlusModule::CNT_NUMBER-1);
        unsigned int bcid = enterInt("BCID number (4096=all)", 0, AltiModule::BCID_NUMBER);

        if (bcid == AltiModule::BCID_NUMBER) {
            bool ena = (bool) enterInt("Enable (0=no, 1=yes)", 0, 1);
            std::vector<bool> fna(AltiModule::BCID_NUMBER);
            for (unsigned int i = 0; i < AltiModule::BCID_NUMBER; i++) fna[i] = ena;
            if ((rtnv = alti->CNTBcidMaskWrite(fna)) != AltiModule::SUCCESS) {
                CERR("writing CNT BCID mask", "");
                return (rtnv);
            }
        }

        else {
            unsigned int num = enterInt("BCID range", 0, AltiModule::BCID_NUMBER - bcid);
            bool ena = (bool) enterInt("Enable (0=no, 1=yes)", 0, 1);
            for (unsigned int i = bcid; i < (bcid + num); i++) {
                if ((rtnv = alti->CNTBcidMaskWrite(i, ena)) != AltiModule::SUCCESS) {
                    CERR("writing CNT BCID mask %d", i);
                    return (rtnv);
                }
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTBcidMaskReadFile : public MenuItem {
  public:
    CNTBcidMaskReadFile() { setName("read  CNT BCID mask [into file]"); }
    int action() {

        int rtnv(0);
        //unsigned int type = enterInt("CNT type (0=LUT, 1=TBP, 2=TAP, 3=TAV)",0,CtpcorePlusModule::CNT_NUMBER-1);
        std::string fn = enterString("File name");

        if ((rtnv = alti->CNTBcidMaskRead(fn)) != AltiModule::SUCCESS) {
            CERR("reading CNT BCID mask into file \"%s\"", fn.c_str());
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTBcidMaskWriteFile : public MenuItem {
  public:
    CNTBcidMaskWriteFile() { setName("write CNT BCID mask [from file]"); }
    int action() {

        int rtnv(0);
        //unsigned int type = enterInt("CNT type (0=LUT, 1=TBP, 2=TAP, 3=TAV)",0,CtpcorePlusModule::CNT_NUMBER-1);
        std::string fn = enterString("File name");

        if ((rtnv = alti->CNTBcidMaskWrite(fn)) != AltiModule::SUCCESS) {
            CERR("writing CNT BCID mask from file \"%s\"", fn.c_str());
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTBcidOffsetRead : public MenuItem {
  public:
    CNTBcidOffsetRead() { setName("read  CNT BCID offset"); }
    int action() {

        int rtnv(0);
        unsigned int off;

        if ((rtnv = alti->CNTBcidOffsetRead(off)) != AltiModule::SUCCESS) {
            CERR("reading CNT BCID offset", "");
            return (rtnv);
        }
        std::printf("CNT BCID offset = %4d (= 0x%03x)\n", off, off);

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTBcidOffsetWrite : public MenuItem {
  public:
    CNTBcidOffsetWrite() { setName("write CNT BCID offset"); }
    int action() {

        int rtnv(0);
        
        unsigned int off = enterInt("CNT BCID offset", 0, AltiModule::BCID_NUMBER);

        if ((rtnv = alti->CNTBcidOffsetWrite(off)) != AltiModule::SUCCESS) {
             CERR("writing CNT BCID offset", "");
             return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTEdgeEnableRead : public MenuItem {
  public:
    CNTEdgeEnableRead() { setName("read  CNT Edge enable"); }
    int action() {

        int rtnv(0);
	
	AltiModule::CNT_TYPE cnt;
	bool ena;
	for(u_int sig = 2; sig < AltiModule::CNT_NUMBER; sig++) {
	  cnt = static_cast<AltiModule::CNT_TYPE>(sig);
	  if ((rtnv = alti->CNTEdgeDetectionRead(cnt, ena)) != AltiModule::SUCCESS) {
	    CERR("reading CNT edge detection", "");
	    return (rtnv);
	  }
	  std::printf("CNT mode for %-8s = %s \n", AltiModule::CNT_NAME[sig].c_str(), ena ? "edge" : "level");
	}
	
        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTEdgeEnableWrite : public MenuItem {
  public:
    CNTEdgeEnableWrite() { setName("write CNT Edge enable"); }
    int action() {

      int rtnv(0);
	
      AltiModule::CNT_TYPE cnt;
      bool ena;
      for(u_int sig = 2; sig < AltiModule::CNT_NUMBER; sig++) {
	cnt = static_cast<AltiModule::CNT_TYPE>(sig);
	char text[100];
	std::sprintf(text, "%-8s mode: 0 = edge , 1 = level", AltiModule::CNT_NAME[sig].c_str());
	ena = (bool) enterInt(text, 0, 1);
	if ((rtnv = alti->CNTEdgeDetectionWrite(cnt, ena)) != AltiModule::SUCCESS) {
	  CERR("writing CNT edge detection", "");
	  return (rtnv);
	}
      }
	
      return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTOrbitCounterReset : public MenuItem {
  public:
    CNTOrbitCounterReset() { setName("reset CNT Orbit counters"); }
    int action() {

      int rtnv(0);
	
      if ((rtnv = alti->CNTOrbitReset()) != AltiModule::SUCCESS) {
	  CERR("reseting CNT orbit", "");
	  return (rtnv);
      }
      
      return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTOrbitCounterRead : public MenuItem {
  public:
    CNTOrbitCounterRead() { setName("read  CNT orbit counters"); }
    int action() {

        int rtnv(0);
	
	u_int cnt, cnt_short, cnt_long;
	if ((rtnv = alti->CNTOrbitRead(cnt, cnt_short, cnt_long)) != AltiModule::SUCCESS) {
	  CERR("reading CNT orbit counters", "");
	  return (rtnv);
	}

	u_int min, max;
	if ((rtnv = alti->CNTOrbitLengthRead(min, max)) != AltiModule::SUCCESS) {
	  CERR("reading CNT orbit lengths", "");
	  return (rtnv);
	}

	std::printf("Number of orbits with length = 3564 BCIDs = %d \n", cnt);
	std::printf("Number of orbits with length < 3564 BCIDs = %d \n", cnt_short);
	std::printf("Number of orbits with lenght > 3564 BCIDs = %d \n", cnt_long);
	std::printf("Minimum orbit length                      = %d BCIDs \n", min);
	std::printf("Maximum orbit length                      = %d BCIDs \n", max);
	
        return (rtnv);
    }
};


//------------------------------------------------------------------------------

class CNTControlPrint : public MenuItem {
  public:
    CNTControlPrint() { setName("print CNT control"); }
    int action() {

        int rtnv(0);

        std::printf("\nCNT:\n");
        if ((rtnv = alti->CNTControlPrint()) != AltiModule::SUCCESS) {
            CERR("printing CNT control", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTEnableRead : public MenuItem {
  public:
    CNTEnableRead() { setName("read  CNT enable"); }
    int action() {

        int rtnv(0);
        bool ena, sta;

        if ((rtnv = alti->CNTEnableRead(ena, sta)) != AltiModule::SUCCESS) {
            CERR("reading CNT control", "");
            return (rtnv);
        }
        std::printf("CNT: %s, status = %s\n", ena ? " ENABLED" : "DISABLED", sta ? " ENABLED" : "DISABLED");

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTEnableWrite : public MenuItem {
  public:
    CNTEnableWrite() { setName("write CNT enable"); }
    int action() {

        int rtnv(0);
        bool ena = (bool) enterInt("Enable (0=no, 1=yes)", 0, 1);

        if ((rtnv = alti->CNTEnableWrite(ena)) != AltiModule::SUCCESS) {
            CERR("writing CNT enable", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTCopyClearWrite : public MenuItem {
  public:
    CNTCopyClearWrite() { setName("write CNT copy/clear"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->CNTCopyClearWrite()) != AltiModule::SUCCESS) {
            CERR("writing CNT copy/clear", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTCopyClearWait : public MenuItem {
  public:
    CNTCopyClearWait() { setName("wait  CNT copy/clear"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->CNTCopyClearWait()) != AltiModule::SUCCESS) {
            CERR("waiting for CNT copy/clear", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTTurnCountRead : public MenuItem {
  public:
    CNTTurnCountRead() { setName("read  CNT TURN count"); }
    int action() {

        int rtnv(0);
        unsigned int turn;
        if ((rtnv = alti->CNTTurnCountRead(turn)) != AltiModule::SUCCESS) {
            CERR("reading CNT TURN count", "");
            return (rtnv);
        }
        std::printf("CNT TURN count = 0x%08x (= %10d)\n", turn, turn);

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTCounterRead : public MenuItem {
  public:
    CNTCounterRead() { setName("read  CNT values"); }
    int action() {

        int rtnv(0);
       
        // create CMEM segment
        std::string sn = "menuAltiModule[CNT]";
        RCD::CMEMSegment *sg = new RCD::CMEMSegment(sn, AltiModule::CNT_SIZE *sizeof(uint32_t), true);
        if ((*sg)() != CMEM_RCC_SUCCESS) {
            CERR("error opening CMEM segment \"%s\" of size 0x%08x\n", sn.c_str(), sg->Size());
            return AltiModule::FAILURE;
        }
        std::printf("opened CMEM segment \"%s\" of size 0x%08x, phys 0x%08x, virt 0x%08x\n", sn.c_str(), sg->Size(), (unsigned int) sg->PhysicalAddress(), (unsigned int) sg->VirtualAddress());

        AltiCounter cnt(sg);

        if ((rtnv = alti->CNTCounterRead(&cnt)) != AltiModule::SUCCESS) {
            CERR("reading CNT counter", "");
            return (rtnv);
        }
        cnt.dump();

        bool rflg = (bool) enterInt("Print rate (0=no, 1=yes)", 0, 1);
        if (rflg) {
            AltiCounterRate *rcnt = new AltiCounterRate(cnt);
            rcnt->dump();
            delete rcnt;
        }

        // delete CMEM segment
        delete sg;
        std::printf("\nclosed CMEM segment \"%s\"\n", sn.c_str());

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTRequestPrint : public MenuItem {
  public:
    CNTRequestPrint() { setName("print CNT request"); }
    int action() {

        int rtnv(0);

        std::printf("\nCNT:\n");
        if ((rtnv = alti->CNTRequestPrint()) != AltiModule::SUCCESS) {
            CERR("printing CNT request", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTTtypLutReadFile : public MenuItem {
  public:
    CNTTtypLutReadFile() { setName("read  CNT TTYP LUT [into file]"); }
    int action() {

        int rtnv(0);
        std::string fn = enterString("File name");

        if ((rtnv = alti->CNTTtypLutRead(fn)) != AltiModule::SUCCESS) {
            CERR("reading CNT TTYP LUT into file \"%s\"", fn.c_str());
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTTtypLutWriteFile : public MenuItem {
  public:
    CNTTtypLutWriteFile() { setName("write CNT TTYP LUT [from file]"); }
    int action() {

        int rtnv(0);
        std::string fn = enterString("File name");

        if ((rtnv = alti->CNTTtypLutWrite(fn)) != AltiModule::SUCCESS) {
            CERR("writing CNT TTYP LUT from file \"%s\"", fn.c_str());
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CNTTtypLutPrint : public MenuItem {
  public:
    CNTTtypLutPrint() { setName("print CNT TTYP LUT"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->CNTTtypLutPrint()) != AltiModule::SUCCESS) {
            CERR("printing CNT TTYP LUT", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiMONAlarmsRead : public MenuItem {
  public:
    AltiMONAlarmsRead() { setName("read power and temperature alarms"); }
    int action() {

      bool any, board_temp, fpga_temp, power_supply, vcc_bram, vcc_int, vcc_aux;
      if (alti->MONPowerAndTemperatureAlarm(any, board_temp, fpga_temp, power_supply, vcc_bram, vcc_int, vcc_aux) != AltiModule::SUCCESS) {
	CERR("reading power and temperature alarms", "");
	return AltiModule::FAILURE;
      }
      
      printf("+---------------------------------------------------------------+\n");
      printf(" - Any alarm                         : %s \n", any ? "on" : "off");
      printf(" - Board Temperature alarm           : %s \n", board_temp ? "on" : "off");
      printf(" - Fpga Die Temperature alarm        : %s \n", fpga_temp ? "on" : "off");
      printf(" - Any Power Supply alarm            : %s \n", power_supply ? "on" : "off");
      printf(" - Vcc Bram1V0 Power Supply alarm    : %s \n", vcc_bram ? "on" : "off");
      printf(" - Vcc Int1V0 Power Supply alarm     : %s \n", vcc_int ? "on" : "off");
      printf(" - Vcc Aux1V8 Power Supply Any alarm : %s \n", vcc_aux ? "on" : "off");
      printf("+---------------------------------------------------------------+\n");
      
      return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------


class AltiMONVoltageRead : public MenuItem {
  public:
    AltiMONVoltageRead() { setName("read voltages"); }
    int action() {

      std::vector<float> voltages(9), nominal(9);
      if (alti->MONVoltageReadLTC2991(voltages, nominal) != AltiModule::SUCCESS) {
            CERR("reading voltages", "");
            return AltiModule::FAILURE;
        }


        //table
        printf("\n");
        printf("+----------------------------------------------------+\n");
        printf("| Voltage | Nominal[V] | Measured[V] | Difference[%%] |\n");
        printf("+----------------------------------------------------+\n");
        unsigned int i;
        float difference;
        for (i = 0; i < 9; i++) {
          
	  difference = (voltages[i]/nominal[i] - 1)*100;
	  char sign_nominal = (nominal[i] >= 0) ? '+' : '-';
	  char sign_measured = (voltages[i] >= 0) ? '+' : '-';
	  char sign_difference = ((difference >= 0 && nominal[i] >= 0) || (difference <= 0 && nominal[i] <= 0 ) ) ? '+' : '-';
	  
	  if(i<8) printf("| V%d      | %c%09.6f | %c%09.6f  | %c%09.6f    |\n", i + 1, sign_nominal, fabs(nominal[i]), sign_measured, fabs(voltages[i]), sign_difference, fabs(difference));
	  else    printf("| VCC     | %c%09.6f | %c%09.6f  | %c%09.6f    |\n",  sign_nominal, fabs(nominal[i]), sign_measured, fabs(voltages[i]), sign_difference, fabs(difference));
        }
	printf("+----------------------------------------------------+\n");

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------

class AltiMONTempRead : public MenuItem {
  public:
    AltiMONTempRead() { setName("read temperatures"); }
    int action() {

        float internalTemp, localTemp;
        if (alti->MONInternalTempReadLTC2991(internalTemp) != AltiModule::SUCCESS) {
            CERR("reading LTC2991 internal temperature", "");
            return AltiModule::FAILURE;
        }
	
        if (alti->MONLocalTempRead(localTemp) != AltiModule::SUCCESS) {
            CERR("reading local temperature", "");
            return AltiModule::FAILURE;
        }
	
        //table
        printf("\n");
        printf("+---------------------------------------------------------+\n");
        printf("LTC2991     , internal: %.4f degrees Celsius\n", internalTemp);
        printf("MAX1617/6695, local   : %.4f degrees Celsius\n", localTemp);
        printf("+---------------------------------------------------------+\n");

        return AltiModule::SUCCESS;
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class AltiI2CSetup : public MenuItem {
  public:
    AltiI2CSetup() { setName("setup I2C"); }
    int action() {

        int rtnv(0);
        if((rtnv = alti->I2CSetup()) != AltiModule::SUCCESS) {
            CERR("setting up I2C","");
            return(rtnv);
        }
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiI2CDump : public MenuItem {
  public:
    AltiI2CDump() { setName("dump  I2C"); }
    int action() {

        int rtnv(0);
        rtnv = alti->I2CDump();

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiI2CRead : public MenuItem {
  public:
    AltiI2CRead() { setName("read  I2C"); }
    int action() {

        int rtnv(0);
        unsigned int bus, ndv, dev, qty, wdat;
        I2CInterface::TRANSFER_TYPE ttyp;
        I2CNetwork::DISPLAY_TYPE dtyp;
        std::string snam, sdat;
        std::vector<unsigned int> vdat;
        float fdat;

        bus = 0;
        ndv = alti->I2CNumDevices(bus);
        dev = (ndv == 1) ? 0 : ((unsigned int) enterInt("I2C device  ", 0, ndv - 1));
        qty = (unsigned int) enterInt("I2C quantity", 0, alti->I2CNumQuantities(bus, dev) - 1);

        // read I2C transfer type
        if ((rtnv = alti->I2CTransferTypeRead(bus, dev, qty, ttyp)) != I2CNetwork::SUCCESS) {
            CERR("reading transfer type from I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
            return (rtnv);
        }

        // read I2C display type
        if ((rtnv = alti->I2CDisplayTypeRead(bus, dev, qty, dtyp)) != I2CNetwork::SUCCESS) {
            CERR("reading display type from I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
            return (rtnv);
        }

        // read I2C name
        if ((rtnv = alti->I2CNameRead(bus, dev, qty, snam)) != I2CNetwork::SUCCESS) {
            CERR("reading name from I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
            return (rtnv);
        }

        // check I2C readability
        bool rdb;
        alti->I2CReadableQuantity(bus, dev, qty, rdb);
        if (!rdb) {
            CERR("I2C bus/device/quantity %d/%d/%d is NOT READABLE", bus, dev, qty);
            rtnv = -1;
            return (rtnv);
        }

        // read I2C data
        if (dtyp == I2CNetwork::WORD) {
            // read I2C data as word
            if ((rtnv = alti->I2CDataRead(bus, dev, qty, wdat)) != I2CNetwork::SUCCESS) {
                CERR("reading data from I2C bus/device/quantity %d/%d/%d - WORD", bus, dev, qty);
                return (rtnv);
            }
            unsigned int num = alti->I2CQuantityGetNumber(bus, dev, qty);
            std::printf("\"%s\" = 0x%0*x\n", snam.c_str(), num*2, wdat);
        }
        else if (dtyp == I2CNetwork::FLOAT) {

            // read I2C data as float
            if ((rtnv = alti->I2CDataRead(bus, dev, qty, fdat)) != I2CNetwork::SUCCESS) {
                CERR("reading data from I2C bus/device/quantity %d/%d/%d - FLOAT", bus, dev, qty);
                return (rtnv);
            }
            std::printf("\"%s\" = %f\n", snam.c_str(), fdat);
        }
        else if (dtyp == I2CNetwork::STRING) {

            // read I2C data as string
            if ((rtnv = alti->I2CDataRead(bus, dev, qty, sdat)) != I2CNetwork::SUCCESS) {
                CERR("reading data from I2C bus/device/quantity %d/%d/%d - STRING", bus, dev, qty);
                return (rtnv);
            }
            std::printf("\"%s\" = \"%s\"\n", snam.c_str(), sdat.c_str());
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiI2CReadDevice : public MenuItem {
  public:
    AltiI2CReadDevice() { setName("read  I2C [device]"); }
    int action() {

        int rtnv (0);
        unsigned int bus, dev, qty;
        std::string snam;

        bus = 0;
        alti->I2CNameRead(bus, snam);
        std::cout << "----------------------------------------" << std::endl;
        std::printf("I2C bus %d: \"%s\":\n", bus, snam.c_str());
        for (dev = 0; dev < alti->I2CNumDevices(bus); dev++) {
            alti->I2CNameRead(bus, dev, snam);
            std::printf("I2C bus/device %d/%2d: \"%s\"\n", bus, dev, snam.c_str());
        }
        std::cout << "----------------------------------------" << std::endl;

        dev = (unsigned int) enterInt("I2C device", 0, alti->I2CNumDevices(bus) - 1);

        for (qty = 0; qty < alti->I2CNumQuantities(bus, dev); qty++) {
            if ((rtnv = alti->I2CDataPrint(bus, dev, qty)) != I2CNetwork::SUCCESS) {
                CERR("printing I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
                continue;
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiI2CReadAll : public MenuItem {
  public:
    AltiI2CReadAll() { setName("read  I2C [all]"); }
    int action() {

        int rtnv(0);
        unsigned int bus, dev, qty;

        bus = 0;
        std::cout << "================================================================================" << std::endl;
        for (dev = 0; dev < alti->I2CNumDevices(bus); dev++) {
            std::cout << "--------------------------------------------------------------------------------" << std::endl;
            for (qty = 0; qty < alti->I2CNumQuantities(bus, dev); qty++) {
                if ((rtnv = alti->I2CDataPrint(bus, dev, qty)) != I2CNetwork::SUCCESS) {
                    CERR("printing I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
                    continue;
                }
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiI2CWrite : public MenuItem {
  public:
    AltiI2CWrite() { setName("write I2C"); }
    int action() {

        int rtnv (0);
        unsigned int bus, ndv, dev, qty, wdat(0);
        I2CInterface::TRANSFER_TYPE ttyp;
        I2CNetwork::DISPLAY_TYPE dtyp;
        std::string snam, sdat;
        std::ostringstream buf;

        bus = 0;
        ndv = alti->I2CNumDevices(bus);
        dev = (ndv == 1) ? 0 : ((unsigned int) enterInt("I2C device  ", 0, ndv - 1));
        qty = (unsigned int) enterInt("I2C quantity", 0, alti->I2CNumQuantities(bus, dev) - 1);

        // read I2C name
        if ((rtnv = alti->I2CNameRead(bus, dev, qty, snam)) != I2CNetwork::SUCCESS) {
            CERR("reading name from I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
            return (rtnv);
        }

        // read I2C transfer type
        if ((rtnv = alti->I2CTransferTypeRead(bus, dev, qty, ttyp)) != I2CNetwork::SUCCESS) {
            CERR("reading transfer type from I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
            return (rtnv);
        }

        // read I2C display type
        if ((rtnv = alti->I2CDisplayTypeRead(bus, dev, qty, dtyp)) != I2CNetwork::SUCCESS) {
            CERR("reading display type from I2C bus/device/quantity %d/%d/%d", bus, dev, qty);
            return (rtnv);
        }

        // check I2C writability
        bool wtb;
        alti->I2CWritableQuantity(bus, dev, qty, wtb);
        if (!wtb) {
            CERR("I2C bus/device/quantity %d/%d/%d is NOT WRITABLE", bus, dev, qty);
            rtnv = -1;
            return (rtnv);
        }

        // check transfer type
        if ((ttyp != I2CInterface::BYTE) && (ttyp != I2CInterface::DATA) && (ttyp != I2CInterface::WORD)) {
            CERR("writing to I2C bus/device/quantity %d/%d/%d of transfer type \"%s\" NOT IMPLEMENTED", bus, dev, qty, I2CInterface::TRANSFER_TYPE_NAME[ttyp].c_str());
            rtnv = -1;
            return (rtnv);
        }

        // write I2C as word
        if (dtyp == I2CNetwork::WORD) {
            if (alti->I2CQuantityGetNumber(bus, dev, qty)) {
                wdat = (unsigned int) enterNumber("DATA", 0, 0xffff);
            }
            if ((rtnv = alti->I2CDataWrite(bus, dev, qty, wdat)) != I2CNetwork::SUCCESS) {
                CERR("writing data from I2C bus/device/quantity %d/%d/%d - WORD", bus, dev, qty);
                return (rtnv);
            }
        }

        // write I2C as float
        else if (dtyp == I2CNetwork::FLOAT) {
            float fdat(0.0);
            if (alti->I2CQuantityGetNumber(bus, dev, qty)) {
                char buf[20];
                while (true) {
                    std::printf("DATA[FLOAT]:");
                    buf[0] = '\0';
                    if (!std::cin) std::cin.clear();
                    std::cin >> buf;
                    if (buf[0] == '\0') {
                        continue;
                    }
                    else {
                        fdat = strtof(buf, nullptr);
                        break;
                    }
                }
                cin.getline(buf, 1);
            }
            if ((rtnv = alti->I2CDataWrite(bus, dev, qty, fdat)) != I2CNetwork::SUCCESS) {
                CERR("writing data to I2C bus/device/quantity %d/%d/%d - FLOAT", bus, dev, qty);
                return (rtnv);
            }
        }

        // write I2C as string
        else if (dtyp == I2CNetwork::STRING) {
            std::string sdat;
            if (alti->I2CQuantityGetNumber(bus, dev, qty)) {
                sdat = enterString("DATA[STRING]:");
            }
            if ((rtnv = alti->I2CDataWrite(bus, dev, qty, sdat)) != I2CNetwork::SUCCESS) {
                CERR("writing data to I2C bus/device/quantity %d/%d/%d - STRING", bus, dev, qty);
                return (rtnv);
            }
        }

        else {
            CERR("writing to I2C bus/device/quantity %d/%d/%d of display type \"%s\" NOT IMPLEMENTED", bus, dev, qty, I2CNetwork::DISPLAY_TYPE_NAME[dtyp].c_str());
            rtnv = -1;
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------
// From here BSY
//------------------------------------------------------------------------------
class AltiBSYFifoReset : public MenuItem {
  public:
    AltiBSYFifoReset() { setName("reset BSY counter FIFO"); }
    int action() {

        int rtnv(0);

        if((rtnv = alti->BSYFifoReset()) != AltiModule::SUCCESS) {
            CERR("resetting BSY FIFO","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYMonitoringControlRead : public MenuItem {
  public:
    AltiBSYMonitoringControlRead() { setName("read  BSY counter control"); }
    int action() {

        int rtnv(0);
        bool wmod, rmod;

        if((rtnv = alti->BSYMonitoringControlRead(wmod,rmod)) != AltiModule::SUCCESS) {
            CERR("reading BSY control","");
            return(rtnv);
        }
        std::printf("BSY control: WRITE = %s, READ = %s\n",wmod?"AUTOMATIC":"   MANUAL",rmod?" LAST":"FIRST");

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYMonitoringControlWrite : public MenuItem {
  public:
    AltiBSYMonitoringControlWrite() { setName("write BSY counter control"); }
    int action() {

        int rtnv(0);
        bool wmod = (bool)enterInt("Write mode (0=man, 1=auto)",0,1);
        bool rmod = (bool)enterInt("Read mode (0=first, 1=last)",0,1);

        if((rtnv = alti->BSYMonitoringControlWrite(wmod,rmod)) != AltiModule::SUCCESS) {
            CERR("writing BSY control","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYSelectRead : public MenuItem {
  public:
    AltiBSYSelectRead() { setName("read  BSY counter select"); }
    int action() {

        int rtnv(0);
        AltiBusyCounter sel;

        if((rtnv = alti->BSYSelectRead(sel)) != AltiModule::SUCCESS) {
            CERR("reading BSY select","");
            return(rtnv);
        }
        sel.selectDump();

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYSelectWrite : public MenuItem {
  public:
    AltiBSYSelectWrite() { setName("write BSY counter select"); }
    int action() {

        int rtnv(0);
        AltiBusyCounter sel;
        std::string nam;
        std::ostringstream buf;

        bool all = (bool)enterInt("All (0=no, 1=yes)",0,1);
        if(all) {
            sel.selectAll(true);
        }
        else {
            for(unsigned int b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) {
                nam = "BSY[\"" + AltiBusyCounter::BUSY_NAME[b] + "\"]";
                buf.str(""); buf << "Enable " << std::setw(16) << std::left << nam << " (0=no, 1=yes)";
                sel.select(static_cast<AltiBusyCounter::BUSY>(b),(bool)enterInt(buf.str().c_str(),0,1));
            }
        }

        if((rtnv = alti->BSYSelectWrite(sel)) != AltiModule::SUCCESS) {
            CERR("writing BSY select","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYCounterReset : public MenuItem {
  public:
    AltiBSYCounterReset() { setName("reset BSY counter"); }
    int action() {

        int rtnv(0);
        bool all = (bool)enterInt("All (0=no, 1=yes)",0,1);

        if(all) {
            if((rtnv = alti->BSYCounterReset()) != AltiModule::SUCCESS) {
                CERR("writing BSY clear counter","");
                return(rtnv);
            }
        }
        else {
            std::string nam;
            std::ostringstream buf;
            AltiBusyCounter clr;
            for(unsigned int b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) {
                nam = "BSY[\"" + AltiBusyCounter::BUSY_NAME[b] + "\"]";
                buf.str(""); buf << "Reset " << std::setw(16) << std::left << nam << " (0=no, 1=yes)";
                clr.select(static_cast<AltiBusyCounter::BUSY>(b),(bool)enterInt(buf.str().c_str(),0,1));
            }
            if((rtnv = alti->BSYCounterReset(clr)) != AltiModule::SUCCESS) {
                CERR("writing BSY clear counter","");
                return(rtnv);
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYIntervalRead : public MenuItem {
  public:
    AltiBSYIntervalRead() { setName("read  BSY counter interval"); }
    int action() {

        int rtnv(0);
        unsigned int itvl;

        if((rtnv = alti->BSYIntervalRead(itvl)) != AltiModule::SUCCESS) {
            CERR("reading BSY interval","");
            return(rtnv);
        }
        std::printf("BSY interval = %d, (0x%06x)",itvl,itvl);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYIntervalWrite : public MenuItem {
  public:
    AltiBSYIntervalWrite() { setName("write BSY counter interval"); }
    int action() {

        int rtnv(0);
        unsigned int itvl = enterHex("BSY interval",0,AltiBusyCounter::BSY_CNT_MASK);

        if((rtnv = alti->BSYIntervalWrite(itvl)) != AltiModule::SUCCESS) {
            CERR("writing BSY interval","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYFifoWatermarkRead : public MenuItem {
  public:
    AltiBSYFifoWatermarkRead() { setName("read  BSY counter FIFO watermark"); }
    int action() {

        int rtnv(0);
        unsigned int fmrk;

        if((rtnv = alti->BSYFifoWatermarkRead(fmrk)) != AltiModule::SUCCESS) {
            CERR("reading BSY FIFO watermark","");
            return(rtnv);
        }
        std::printf("BSY FIFO watermark = %4d (0x%03x)\n",fmrk,fmrk);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYFifoWatermarkWrite : public MenuItem {
  public:
    AltiBSYFifoWatermarkWrite() { setName("write BSY counter FIFO watermark"); }
    int action() {

        int rtnv(0);
        unsigned int fmrk = (unsigned int)enterInt("BSY FIFO watermark",0,AltiModule::BSY_FIFO_MASK);

        if((rtnv = alti->BSYFifoWatermarkWrite(fmrk)) != AltiModule::SUCCESS) {
            CERR("writing BSY FIFO watermark","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYFifoWrite : public MenuItem {
  public:
    AltiBSYFifoWrite() { setName("write BSY counter FIFO [manually]"); }
    int action() {

        int rtnv(0);

        if((rtnv = alti->BSYFifoWrite()) != AltiModule::SUCCESS) {
            CERR("writing BSY FIFO","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYFifoStatus : public MenuItem {
  public:
    AltiBSYFifoStatus() { setName("read  BSY counter FIFO status"); }
    int action() {

        int rtnv(0);
        unsigned int lvl;
        bool ful;

        if((rtnv = alti->BSYFifoStatusRead(lvl,ful)) != AltiModule::SUCCESS) {
            CERR("reading BSY FIFO status","");
            return(rtnv);
        }
        std::printf("BSY FIFO status: level = %4d  - %s\n",lvl,ful?"FULL":"not full");

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYFifoRead : public MenuItem {
  public:
    AltiBSYFifoRead() { setName("read  BSY counter FIFO"); }
    int action() {

        int rtnv(0);
        unsigned int data, i;
        unsigned int loop = enterInt("How many words",0,AltiModule::BSY_FIFO_SIZE);

        for(i=0; i<loop; i++) {
            if((rtnv = alti->BSYFifoRead(data)) != AltiModule::SUCCESS) {
                CERR("reading BSY FIFO at word %d",i);
                return(rtnv);
            }
            std::printf("BSY FIFO word %4d: 0x%08x\n",i,data);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYFifoReadCounter : public MenuItem {
  public:
    AltiBSYFifoReadCounter() { setName("read  BSY counter FIFO [counter]"); }
    int action() {

        int rtnv(0);

        // create CMEM segment
        std::string sn = "menuAltiModule[BSY]";
        RCD::CMEMSegment* sg = new RCD::CMEMSegment(sn,AltiModule::BSY_FIFO_SIZE*sizeof(uint32_t),true);
        if((*sg)() != CMEM_RCC_SUCCESS) {
            CERR("error opening CMEM segment \"%s\" of size 0x%08x\n",sn.c_str(),sg->Size());
            return AltiModule::FAILURE;
        }
        std::printf("opened CMEM segment \"%s\" of size 0x%08x, phys 0x%08x, virt 0x%08x\n",sn.c_str(),sg->Size(),(unsigned int)sg->PhysicalAddress(),(unsigned int)sg->VirtualAddress());
        AltiBusyCounter BSY(sg);

        // read counter config
        if((rtnv = alti->BSYCounterConfigRead(BSY)) != AltiModule::SUCCESS) {
            CERR("reading BSY counter configuration","");
            return(rtnv);
        }
        if(!BSY.check()) {
            CERR("BSY counter/control config inconsistent","");
            return AltiModule::FAILURE;
        }

        // read counter
        if((rtnv = alti->BSYFifoRead(BSY)) != AltiModule::SUCCESS) {
            CERR("reading BSY FIFO counter","");
            return(rtnv);
        }
        BSY.dump();

        // calculate rate
        AltiBusyCounterRate rBSY;
        rBSY = BSY;
        std::cout << std::endl; rBSY.dump();

        // delete CMEM segment
        delete sg;
        std::printf("\nclosed CMEM segment \"%s\"\n",sn.c_str());

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class AltiBSYFifoReadAverage : public MenuItem {
  public:
    AltiBSYFifoReadAverage() { setName("read  BSY counter FIFO [average]"); }
    int action() {

        int rtnv(0);
        int num = enterInt("Maximum number of reads",-1,AltiModule::BSY_FIFO_SIZE);

        // create CMEM segment
        std::string sn = "menuAltiModule[BSY]";
        RCD::CMEMSegment* sg = new RCD::CMEMSegment(sn,AltiModule::BSY_FIFO_SIZE*sizeof(uint32_t),true);
        if((*sg)() != CMEM_RCC_SUCCESS) {
            CERR("error opening CMEM segment \"%s\" of size 0x%08x\n",sn.c_str(),sg->Size());
            return AltiModule::FAILURE;
        }
        std::printf("opened CMEM segment \"%s\" of size 0x%08x, phys 0x%08x, virt 0x%08x\n",sn.c_str(),sg->Size(),(unsigned int)sg->PhysicalAddress(),(unsigned int)sg->VirtualAddress());
        AltiBusyCounter BSY(sg);
        AltiBusyCounterRate avg;

        // read counter configuration
        if((rtnv = alti->BSYCounterConfigRead(BSY)) != AltiModule::SUCCESS) {
            CERR("reading BSY FIFO counter configuration","");
            return(rtnv);
        }
        if(!BSY.check()) {
            CERR("counter control inconsistent","");
            return AltiModule::FAILURE;
        }

        // read average counter
        if((rtnv = alti->BSYFifoRead(avg,BSY,num)) != AltiModule::SUCCESS) {
            CERR("reading BSY FIFO counter","");
            return(rtnv);
        }
        avg.dump();

        // delete CMEM segment
        delete sg;
        std::printf("\nclosed CMEM segment \"%s\"\n",sn.c_str());

        return(rtnv);
    }
};


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class PBMCountersStatus : public MenuItem {
  public:
    PBMCountersStatus() { setName("read  Status"); }
    int action() {
      int rtnv(0);
      bool en,clr;
      unsigned int bcid_offset,turn;
      if((rtnv = alti->PBMStatusRead(en,clr,bcid_offset,turn)) != AltiModule::SUCCESS) {
              CERR("reading BSY status","");
              return(rtnv);
          }
      printf(" Counters:     %8s\n Clear:        %8s\n BCID offset:      %4d\n Turn Counter: %8d\n",en?"Enabled":"Disabled",clr?"Active":"Inactive",bcid_offset,turn);
      return(rtnv);
    }
};

//------------------------------------------------------------------------------

class PBMCountersEnable : public MenuItem {
  public:
    PBMCountersEnable() { setName("write Counters Enable"); }
    int action() {
      int rtnv(0);
      bool en = (bool)enterInt("Enable per-bunch monitoring counters (0=disable, 1=enable) ", 0, 1);
      if((rtnv = alti->PBMEnableCounters(en)) != AltiModule::SUCCESS) {
              CERR("reading BSY status","");
              return(rtnv);
          }
      return(rtnv);
    }
};

//------------------------------------------------------------------------------

class PBMCountersClear : public MenuItem {
  public:
    PBMCountersClear() { setName("clear Counters"); }
    int action() {
      int rtnv(0);
      if((rtnv = alti->PBMClearCounters()) != AltiModule::SUCCESS) {
              CERR("clearing Counters","");
              return(rtnv);
          }
      return(rtnv);
    }
};

//------------------------------------------------------------------------------

class PBMWriteOffset : public MenuItem {
  public:
    PBMWriteOffset() { setName("write BCID offset"); }
    int action() {
      int rtnv(0);
      signed int input = enterInt("Offset:",-3563,3563);
      unsigned int offset;
      if (input < 0) {offset = 3563+input;} else offset = input;
      if((rtnv = alti->PBMWriteOffset(offset)) != AltiModule::SUCCESS) {
              CERR("clearing Counters","");
              return(rtnv);
          }
      return(rtnv);
    }
};

//------------------------------------------------------------------------------

class PBMReadMemory : public MenuItem {
  public:
    PBMReadMemory() { setName("read  Counters Memory"); }
    int action() {
      int rtnv(0);
      AltiModule::PBM_SIGNAL signal;
      unsigned int i = enterInt("L1A: 0, TTR1..3: 1..3, BGO0..3: 4..7",0,7);
      signal = AltiModule::PBM_SIGNAL(i);
      if((rtnv = alti->PBMReadMemory(signal,segment[0])) != AltiModule::SUCCESS) {
	CERR("reading PBM memory","");
	return(rtnv);
      }
      
      unsigned int *data = (unsigned int *) segment[0]->VirtualAddress();
      unsigned int turn;
      if ((rtnv = alti->PBMReadTurnCounter(turn)) != AltiModule::SUCCESS) {
        CERR("reading PBM Turn counter register","");
        return(rtnv);
      }
      printf("Turn Counter: %8d \n", turn);
      printf("BCID  |   %s\n",AltiModule::PBM_SIGNAL_NAME[signal].c_str());
      for (u_int bcid = 0; bcid < 3563; bcid++){
	double rate = 0;
	if(turn > 0) rate = ((float) data[bcid] / (float) turn) * 11.2455;
	if(data[bcid] != AltiModule::SUCCESS) printf("%4d  | %8d = %.3f kHz \n", bcid, data[bcid], rate);
      }

      return(rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class CTPInputRead : public MenuItem {
  public:
    CTPInputRead() { setName("read  Input signals"); }
    int action() {

      int rtnv(0);
      string src;
      if ((rtnv = alti->CTPInputL1aRead(src)) != AltiModule::SUCCESS) {
	CERR("reading mini-CTP L1A source", "");
	return AltiModule::FAILURE;     
      }
      printf(" L1A   source  : %s\n", src.c_str());
      if ((rtnv = alti->CTPInputBgo2Read(src)) != AltiModule::SUCCESS) {
	CERR("reading mini-CTP BGo2 source", "");
	return AltiModule::FAILURE;     
      }
      printf(" BGo-2 source  : %s\n", src.c_str());
      if ((rtnv = alti->CTPInputBgo3Read(src)) != AltiModule::SUCCESS) {
	CERR("reading mini-CTP BGo3 source", "");
	return AltiModule::FAILURE;     
      }
      printf(" BGo-3 source  : %s\n", src.c_str());

      for (u_int i = 0; i < ALTI_CTP_INP_INPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER; i++) {
	if ((rtnv = alti->CTPInputTtrRead(i, src)) != AltiModule::SUCCESS) {
	  CERR("reading mini-CTP TTR%d source", i+1);
	  return AltiModule::FAILURE;     
	}
	printf(" TTR-%d source  : %s\n", i+1, src.c_str());
      }
      
      for (u_int i = 0; i < ALTI_CTP_INP_INPUTSELECT_BITSTRING::CALREQUEST_NUMBER; i++) {
	if ((rtnv = alti->CTPInputCalreqRead(i, src)) != AltiModule::SUCCESS) {
	  CERR("reading mini-CTP CalReq%d source", i);
	  return AltiModule::FAILURE;     
	}
	printf(" CRQ-%d source  : %s\n", i, src.c_str());
      }
      
      return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPInputWrite : public MenuItem {
public:
  CTPInputWrite() { setName("write Input signals"); }
  int action() {
    
    int rtnv(0);
    
    int src;
    src     = enterInt("L1A   source    (0 = EXTERNAL, 1 = PATTERN)", 0, 1);
    if ((rtnv = alti->CTPInputL1aWrite(ALTI_CTP_INP_INPUTSELECT_BITSTRING::L1A_TYPE_NAME[src])) != AltiModule::SUCCESS) {
      CERR("writing mini-CTP L1A source", "");
      return AltiModule::FAILURE;     
    }
    src     = enterInt("BGo-2 source    (0 = EXTERNAL, 1 = PATTERN)", 0, 1);
    if ((rtnv = alti->CTPInputBgo2Write(ALTI_CTP_INP_INPUTSELECT_BITSTRING::BGO2_TYPE_NAME[src])) != AltiModule::SUCCESS) {
      CERR("writing mini-CTP BGo-2 source", "");
      return AltiModule::FAILURE;     
    }
    src     = enterInt("BGo-3 source    (0 = EXTERNAL, 1 = PATTERN)", 0, 1);
    if ((rtnv = alti->CTPInputBgo3Write(ALTI_CTP_INP_INPUTSELECT_BITSTRING::BGO3_TYPE_NAME[src])) != AltiModule::SUCCESS) {
      CERR("writing mini-CTP BGo-3 source", "");
      return AltiModule::FAILURE;     
    }	
    for (u_int i = 0; i < ALTI_CTP_INP_INPUTSELECT_BITSTRING::TESTTRIGGER_NUMBER; i++) {
      char text[100];
      std::sprintf(text, "TTR-%d source    (0 = EXTERNAL, 1 = PATTERN)", i+1);
      src     = enterInt(text, 0, 1);
      if ((rtnv = alti->CTPInputTtrWrite(i, ALTI_CTP_INP_INPUTSELECT_BITSTRING::TESTTRIGGER_TYPE_NAME[src])) != AltiModule::SUCCESS) {
	CERR("writing mini-CTP TTR%d source", i+1);
	return AltiModule::FAILURE;     
      }
    }
    for (u_int i = 0; i < ALTI_CTP_INP_INPUTSELECT_BITSTRING::CALREQUEST_NUMBER; i++) {
      char text[100];
      std::sprintf(text, "CRQ-%d source    (0 = EXTERNAL, 1 = PATTERN)", i);
      src     = enterInt(text, 0, 1);
      if ((rtnv = alti->CTPInputCalreqWrite(i, ALTI_CTP_INP_INPUTSELECT_BITSTRING::CALREQUEST_TYPE_NAME[src])) != AltiModule::SUCCESS) {
	CERR("reading mini-CTP CalReq%d source", i);
	return AltiModule::FAILURE;     
      }
    }      
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class CTPBunchGroupDataRead : public MenuItem {
public:
  CTPBunchGroupDataRead() { setName("read  Bunch-Group data"); }
  int action() {
    
    int rtnv(0);
    unsigned int bgrp = enterInt("BGRP number", 0, AltiModule::BGRP_NUMBER-1);
    unsigned int bcid = enterInt("BCID number (4096=all)", 0, AltiModule::BCID_NUMBER);
    
    if(bcid == AltiModule::BCID_NUMBER) {
      std::vector<bool> ena;
      if((rtnv = alti->CTPBunchGroupDataRead(bgrp, ena)) != AltiModule::SUCCESS) {
	CERR("reading BGRP%02d data", bgrp);
	return(rtnv);
      }
      for(unsigned int i = 0; i < AltiModule::BCID_NUMBER; i++) {
	std::printf("BGRP%02d[%4d] = %s\n",bgrp, i, ena[i] ? " ENABLED" : "DISABLED");
      }
    }
    
    else {
      unsigned int num = enterInt("BCID range", 0, AltiModule::BCID_NUMBER-bcid);
      bool ena;
      for(unsigned int i = bcid; i < (bcid+num); i++) {
	if((rtnv = alti->CTPBunchGroupDataRead(bgrp, i, ena)) != AltiModule::SUCCESS) {
	  CERR("reading BGRP%02d data of BCID %d", bgrp, i);
	  return(rtnv);
	}
	std::printf("BGRP%02d[%4d] = %s\n", bgrp, i, ena ? " ENABLED" : "DISABLED");
      }
    }
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class CTPBunchGroupDataWrite : public MenuItem {
public:
  CTPBunchGroupDataWrite() { setName("write Bunch-Group data"); }
  int action() {
    
    int rtnv(0);
    unsigned int bgrp = enterInt("BGRP number (2=all)", 0, AltiModule::BGRP_NUMBER);
    unsigned int bcid = enterInt("BCID number (4096=all)", 0, AltiModule::BCID_NUMBER);
    unsigned int xgrp, ygrp, igrp;
    
    xgrp = (bgrp == AltiModule::BGRP_NUMBER) ? 0 : bgrp;
    ygrp = (bgrp == AltiModule::BGRP_NUMBER) ? AltiModule::BGRP_NUMBER : (bgrp += 1);
    
    if(bcid == AltiModule::BCID_NUMBER) {
      bool ena = (bool)enterInt("Enable (0=no, 1=yes)", 0, 1);
      std::vector<bool> v_ena(AltiModule::BCID_NUMBER, ena); 
      for(igrp = xgrp; igrp < ygrp; igrp++) {
	if((rtnv = alti->CTPBunchGroupDataWrite(igrp, v_ena)) != AltiModule::SUCCESS) {
	  CERR("writing BGRP%02d", igrp);
	  return(rtnv);
	}
      }
    }
    
    else {
      unsigned int num = enterInt("BCID range", 0, AltiModule::BCID_NUMBER-bcid);
      bool ena = (bool)enterInt("Enable (0=no, 1=yes)", 0, 1);
      for(igrp = xgrp; igrp < ygrp; igrp++) {
	for(unsigned int i = bcid; i < (bcid+num); i++) {
	  if((rtnv = alti->CTPBunchGroupDataWrite(igrp, i, ena)) != AltiModule::SUCCESS) {
	    CERR("writing BGRP%02d data of BCID %d", igrp, i);
	    return(rtnv);
	  }
	}
      }
    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class CTPBunchGroupDataReadFile : public MenuItem {
public:
  CTPBunchGroupDataReadFile() { setName("read  Bunch-Group data [into file]"); }
  int action() {
    
    int rtnv(0);
    std::string fn = enterString("File name");
     
    if((rtnv = alti->CTPBunchGroupDataRead(fn)) != AltiModule::SUCCESS) {
      CERR("reading BGRP data into file \"%s\"", fn.c_str());
      return(rtnv);
    }
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class CTPBunchGroupDataWriteFile : public MenuItem {
public:
  CTPBunchGroupDataWriteFile() { setName("write Bunch-Group data [from file]"); }
  int action() {
    
    int rtnv(0);
    std::string fn = enterString("File name");
    
    if((rtnv = alti->CTPBunchGroupDataWrite(fn)) != AltiModule::SUCCESS) {
      CERR("writing BGRP data into file \"%s\"", fn.c_str());
      return(rtnv);
    }
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class CTPBunchGroupMaskRead : public MenuItem {
public:
  CTPBunchGroupMaskRead() { setName("read  Bunch-Group mask"); }
  int action() {
    
    int rtnv(0);
    bool bg0, bg1;
    printf("----------------------------\n");
    printf("| item |  BGRP-0 |  BGRP-1 | \n");
    for (u_int item = 0; item < AltiModule::ITEM_NUMBER; item++) {
      if((rtnv = alti->CTPBunchGroupMaskRead(item, bg0, bg1)) != AltiModule::SUCCESS) {
      CERR("reading BGRP mask for item %d", item);
      return(rtnv);
      } 
      printf("|  %d   |  %5s  |  %5s  | \n", item, bg0 ? "true" : "false", bg1 ? "true" : "false");
    }
    printf("----------------------------");

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class CTPBunchGroupMaskWrite : public MenuItem {
public:
  CTPBunchGroupMaskWrite() { setName("write Bunch-Group mask"); }
  int action() {
    
    int rtnv(0);
    unsigned int item = enterInt("Trigger item number (8=all)", 0, AltiModule::ITEM_NUMBER);
    unsigned int mask = enterInt("Bunch-group mask (0 = None, 1 = BGRP-0, 2 = BGRP-1, 3 = both)", 0 ,3);

    if(item == AltiModule::ITEM_NUMBER) {
      for (u_int i = 0; i < AltiModule::ITEM_NUMBER; i++) {
	if((rtnv = alti->CTPBunchGroupMaskWrite(i, mask)) != AltiModule::SUCCESS) {
	  CERR("writing BGRP mask for item %d", i);
	  return(rtnv);
	} 
      }
    }
    else {
      if((rtnv = alti->CTPBunchGroupMaskWrite(item, mask)) != AltiModule::SUCCESS) {
	CERR("writing BGRP mask for item %d", item);
	return(rtnv);
      } 
    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class CTPBunchGroupBcidOffsetRead : public MenuItem {
public:
  CTPBunchGroupBcidOffsetRead() { setName("read  Bunch-Group BCID offset"); }
  int action() {
    
    int rtnv(0);
    unsigned int off;
    
    if((rtnv = alti->CTPBunchGroupBcidOffsetRead(off)) != AltiModule::SUCCESS) {
      CERR("reading BGRP BCID offset","");
      return(rtnv);
    }
    std::printf("BGRP BCID offset = %4d (= 0x%03x)\n", off, off);
        
    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class CTPBunchGroupBcidOffsetWrite : public MenuItem {
public:
  CTPBunchGroupBcidOffsetWrite() { setName("write Bunch-Group BCID offset"); }
  int action() {
    
    int rtnv(0);
    unsigned int off = enterInt("BGRP BCID offset", 0, AltiModule::BCID_NUMBER);
    
    if((rtnv = alti->CTPBunchGroupBcidOffsetWrite(off)) != AltiModule::SUCCESS) {
      CERR("writing BGRP BCID offset","");
      return(rtnv);
    }
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class CTPBunchGroupItemEnableRead : public MenuItem {
  public:
    CTPBunchGroupItemEnableRead() { setName("read  Bunch-Group Item enabled"); }
    int action() {

        int rtnv(0);
	bool ena;
	for (u_int item = 0; item < AltiModule::ITEM_NUMBER; item++) {
	  if((rtnv = alti->CTPBunchGroupItemEnabledRead(item, ena)) != AltiModule::SUCCESS) {
	  CERR("reading bunch-group item %d enable", item);
	  return(rtnv);
	  }     
	  std::printf("Bunch-group item veto %d = %s\n", item, ena?" ENABLED" : "DISABLED");
	}
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPBunchGroupItemEnableWrite : public MenuItem {
  public:
    CTPBunchGroupItemEnableWrite() { setName("write Bunch-Group Item enabled"); }
    int action() {

        int rtnv(0);
   	
	unsigned int itm = enterInt("Trigger item number (8=all)",0,AltiModule::ITEM_NUMBER);
	bool         ena = enterInt("Bunch-Group Item Veto enable (0=disable, 1=enable)", 0, 1);

	unsigned int xck, yck, ick;
        xck = (itm == AltiModule::ITEM_NUMBER) ? 0 : itm;
        yck = (itm == AltiModule::ITEM_NUMBER) ? AltiModule::ITEM_NUMBER : (itm+1);

	for(ick=xck; ick<yck; ick++) {
	  if((rtnv = alti->CTPBunchGroupItemEnabledWrite(ick, ena)) != AltiModule::SUCCESS) {
	    CERR("writing bunch-group item %d enable", ick);
	    return(rtnv);
	  }     
	}
        return(rtnv);
	}
};

//------------------------------------------------------------------------------

class CTPRandomTriggerThresholdRead : public MenuItem {
  public:
    CTPRandomTriggerThresholdRead() { setName("read  RNDM threshold"); }
    int action() {

        int rtnv(0);
        unsigned int rnd, thr;
        double frq;

        for(rnd=0; rnd<AltiModule::RNDM_NUMBER; rnd++) {
            if((rtnv = alti->CTPRandomTriggerThresholdRead(rnd,thr)) != AltiModule::SUCCESS) {
                CERR("reading RNDM%d threshold",rnd);
                return(rtnv);
            }
            if((rtnv = alti->CTPThresholdToPrescaler(thr,frq)) != AltiModule::SUCCESS) {
                CERR("converting RNDM%d threshold",rnd);
                return(rtnv);
            }
            frq *= AltiCommon::BC_FREQUENCY;
            std::printf("RNDM%d threshold = %8d (=0x%06x) => frequency = %12.3f Hz\n",rnd,thr,thr,frq);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPRandomTriggerThresholdWrite : public MenuItem {
  public:
    CTPRandomTriggerThresholdWrite() { setName("write RNDM threshold"); }
    int action() {

        int rtnv(0);
        unsigned int rnd = enterInt("RNDM number (2=all)",0,AltiModule::RNDM_NUMBER);
        unsigned int num;

        unsigned int sel = enterInt("RNDM definition (0=threshold, 1=frequency)",0,1);
        if(sel == 0) {
            num = enterNumber("RNDM threshold",0,AltiModule::RNDM_THRESHOLD_MASK);
        }
        else if(sel == 1) {
            double freq;
            char buf[AltiCommon::STRING_LENGTH];
            std::sprintf(buf,"RNDM frequency [0..%.3f][Hz]",AltiCommon::BC_FREQUENCY);
	    std::string str = enterString(buf);
	    if(AltiCommon::readNumber(str,freq) == 0) {
	      if(freq < 3) freq = 3;
	      else if(freq > AltiCommon::BC_FREQUENCY) freq = AltiCommon::BC_FREQUENCY;
	      if((rtnv = alti->CTPPrescalerToThreshold(freq/AltiCommon::BC_FREQUENCY,num)) != AltiModule::SUCCESS) {
		CERR("converting %d RNDM frequency",freq);
		return(rtnv);
	      }
	    }
	}

        unsigned int xnd = (rnd==AltiModule::RNDM_NUMBER) ? 0 : rnd;
        unsigned int ynd = (rnd==AltiModule::RNDM_NUMBER) ? AltiModule::RNDM_NUMBER: (rnd+1);

        for(unsigned int ind=xnd; ind<ynd; ind++) {
            if((rtnv = alti->CTPRandomTriggerThresholdWrite(ind,num)) != AltiModule::SUCCESS) {
                CERR("writing RNDM%d threshold",ind);
                return(rtnv);
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPRandomTriggerSeedWrite : public MenuItem {
  public:
    CTPRandomTriggerSeedWrite() { setName("write RNDM seed"); }
    int action() {

        int rtnv(0);
        unsigned int rnd = enterInt("RNDM number (2=all)",0,AltiModule::RNDM_NUMBER);
        unsigned int num = enterNumber("RNDM seed",0,AltiModule::RNDM_SEED_MASK);
        unsigned int xnd, ynd, ind;
        xnd = (rnd==AltiModule::RNDM_NUMBER) ? 0 : rnd;
        ynd = (rnd==AltiModule::RNDM_NUMBER) ? AltiModule::RNDM_NUMBER: (rnd+1);

        for(ind=xnd; ind<ynd; ind++) {
            if((rtnv = alti->CTPRandomTriggerSeedWrite(ind,num)) != AltiModule::SUCCESS) {
                CERR("writing RNDM%d seed",ind);
                return(rtnv);
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class CTPTriggerLutRead : public MenuItem {
  public:
    CTPTriggerLutRead() { setName("read  Trigger Item LUT"); }
    int action() {

        int rtnv(0);

	std::vector<unsigned int> lut;
	if((rtnv = alti->CTPTriggerLutRead(lut)) != AltiModule::SUCCESS) {
	  CERR("reading CTP Trigger Item LUT", "");
	  return(rtnv);
	}
	
	for(unsigned int i : lut) {
	  std::printf("0x%08x \n",i);
	}

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPTriggerLutReadFile : public MenuItem {
  public:
    CTPTriggerLutReadFile() { setName("read  Trigger Item LUT into file"); }
    int action() {

        int rtnv(0);
	
	std::string fn = enterString("File name");
	
        if((rtnv = alti->CTPTriggerLutRead(fn)) != AltiModule::SUCCESS) {
            CERR("reading CTP Trigger Item LUT into file \"%s\"",fn.c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPTriggerLutWriteFile : public MenuItem {
  public:
    CTPTriggerLutWriteFile() { setName("write Trigger Item LUT from file"); }
    int action() {

        int rtnv(0);
	
	std::string fn = enterString("File name");
	
        if((rtnv = alti->CTPTriggerLutWrite(fn)) != AltiModule::SUCCESS) {
            CERR("writing CTP Trigger Item LUT into file \"%s\"",fn.c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPTtypLutRead : public MenuItem {
  public:
    CTPTtypLutRead() { setName("read  Trigger Type LUT"); }
    int action() {

        int rtnv(0);

	std::vector<unsigned int> lut;
	if((rtnv = alti->CTPTtypLutRead(lut)) != AltiModule::SUCCESS) {
	  CERR("reading CTP Trigger Type LUT", "");
	  return(rtnv);
	}
	
	for(unsigned int i : lut) {
	  std::printf("0x%08x \n",i);
	}

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPTtypLutReadFile : public MenuItem {
  public:
    CTPTtypLutReadFile() { setName("read  Trigger Type LUT into file"); }
    int action() {

        int rtnv(0);
	
	std::string fn = enterString("File name");
	
        if((rtnv = alti->CTPTtypLutRead(fn)) != AltiModule::SUCCESS) {
            CERR("reading CTP Trigger Type LUT into file \"%s\"",fn.c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPTtypLutWriteFile : public MenuItem {
  public:
    CTPTtypLutWriteFile() { setName("write Trigger Type LUT from file"); }
    int action() {

        int rtnv(0);
	
	std::string fn = enterString("File name");
	
        if((rtnv = alti->CTPTtypLutWrite(fn)) != AltiModule::SUCCESS) {
            CERR("writing CTP Trigger Type LUT into file \"%s\"",fn.c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPPrescalerThresholdRead : public MenuItem {
  public:
    CTPPrescalerThresholdRead() { setName("read  PRSC threshold"); }
    int action() {

        int rtnv(0);
        double prob;

	unsigned int thr;
	for(unsigned int i = 0; i < AltiModule::ITEM_NUMBER; i++) {
	  if((rtnv = alti->CTPPrescalerThresholdRead(i,thr)) != AltiModule::SUCCESS) {
	    CERR("reading PRSC threshold for item %d",i);
	    return(rtnv);
	  }
	  if((rtnv = alti->CTPThresholdToPrescaler(thr,prob)) != AltiModule::SUCCESS) {
	    CERR("converting PRSC threshold for item %d",i);
	    return(rtnv);
	  }
	  std::printf("PRSC[%3d] threshold = %8d (= 0x%06x) => probability = %10.8f, factor = %17.8f\n",i,thr,thr,prob,1.0/prob);
	}

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPPrescalerThresholdWrite : public MenuItem {
  public:
    CTPPrescalerThresholdWrite() { setName("write PRSC threshold"); }
    int action() {

        int rtnv(0);
        unsigned int itm = enterInt("Trigger item number (8=all)",0,AltiModule::ITEM_NUMBER);
        unsigned int num(0);

        if(itm != AltiModule::ITEM_NUMBER) {
            num = enterInt("Trigger item range",0,AltiModule::ITEM_NUMBER-itm);
        }

        unsigned int sel = enterInt("PRSC definition (0=threshold, 1=probability, 2=factor)",0,2);
        unsigned int thr;
        if(sel == 0) {
            thr = enterNumber("PRSC threshold",0,AltiModule::PRSC_THRESHOLD_MASK);
        }
        else if(sel == 1) {
            double prob, pmin, pmax;
			if((rtnv = alti->CTPThresholdToPrescaler(AltiModule::PRSC_THRESHOLD_MASK,pmin)) != AltiModule::SUCCESS) {
                CERR("converting PRSC threshold", "");
                return(rtnv);
            }
            if((rtnv = alti->CTPThresholdToPrescaler(0,pmax)) != AltiModule::SUCCESS) {
                CERR("converting PRSC threshold", "");
                return(rtnv);
            }
            char buf[AltiCommon::STRING_LENGTH];
            std::sprintf(buf,"PRSC probability [%.8f..%.8f]",pmin,pmax);
            while(true) {
                std::string str = enterString(buf);
                if(AltiCommon::readNumber(str,prob) == 0) {
                    if((pmin<=prob) and (prob <= pmax)) {
                        if((rtnv = alti->CTPPrescalerToThreshold(prob,thr)) != AltiModule::SUCCESS) {
                            CERR("converting PRSC prescaler","");
                            return(rtnv);
                        }
                        break;
                    }
                }
            }
        }
        else if(sel == 2) {
            double fact, fmin, fmax;
            if((rtnv = alti->CTPThresholdToPrescaler(0,fmin)) != AltiModule::SUCCESS) {
                CERR("converting PRSC threshold","");
                return(rtnv);
            }
			fmin = 1.0/fmin;
            if((rtnv = alti->CTPThresholdToPrescaler(AltiModule::PRSC_THRESHOLD_MASK,fmax)) != AltiModule::SUCCESS) {
                CERR("converting PRSC threshold", "");
                return(rtnv);
            }
			fmax = 1.0/fmax;
            char buf[AltiCommon::STRING_LENGTH];
            std::sprintf(buf,"PRSC factor [%.3f..%.3f]",fmin,fmax);
            while(true) {
                std::string str = enterString(buf);
                if(AltiCommon::readNumber(str,fact) == 0) {
                    if((fmin<= fact) and (fact <= fmax)) {
                        if((rtnv = alti->CTPPrescalerToThreshold(1.0/fact,thr)) != AltiModule::SUCCESS) {
                            CERR("converting PRSC threshold", "");
                            return(rtnv);
                        }
                        break;
                    }
                }
            }
        }

        if(itm == AltiModule::ITEM_NUMBER) {
            if((rtnv = alti->CTPPrescalerThresholdWrite(thr)) != AltiModule::SUCCESS) {
                CERR("writing PRSC threshold for all trigger items","");
                return(rtnv);
            }
        }
        else {
            for(unsigned int i=itm; i<(itm+num); i++) {
                if((rtnv = alti->CTPPrescalerThresholdWrite(i,thr)) != AltiModule::SUCCESS) {
                    CERR("writing PRSC threshold for trigger item %d",i);
                    return(rtnv);
                }
            }
        }

        return(rtnv);
    }
};


//------------------------------------------------------------------------------

class CTPPrescalerThresholdReadFile : public MenuItem {
  public:
    CTPPrescalerThresholdReadFile() { setName("read  PRSC threshold [file]"); }
    int action() {

        int rtnv(0);
        std::string fn = enterString("File name");

        if((rtnv = alti->CTPPrescalerThresholdRead(fn)) != AltiModule::SUCCESS) {
            CERR("reading PRSC threshold into file \"%s\"",fn.c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPPrescalerThresholdWriteFile : public MenuItem {
  public:
    CTPPrescalerThresholdWriteFile() { setName("write PRSC threshold [file]"); }
    int action() {

        int rtnv(0);
        std::string fn = enterString("File name");

        if((rtnv = alti->CTPPrescalerThresholdWrite(fn)) != AltiModule::SUCCESS) {
            CERR("writing PRSC threshold from file \"%s\"",fn.c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPPrescalerSeedWrite : public MenuItem {
  public:
    CTPPrescalerSeedWrite() { setName("write PRSC seed"); }
    int action() {

        int rtnv(0);
        unsigned int itm = enterInt("Trigger item number (8=all)",0,AltiModule::ITEM_NUMBER);

        if(itm == AltiModule::ITEM_NUMBER) {
            unsigned int seed = enterNumber("PRSC seed",0,AltiModule::PRSC_SEED_MASK);
            if((rtnv = alti->CTPPrescalerSeedWrite(seed)) != AltiModule::SUCCESS) {
                CERR("writing PRSC seeds for all trigger items","");
                return(rtnv);
            }
        }

        else {
            unsigned int num = enterInt("Trigger item range",0,AltiModule::ITEM_NUMBER-itm);
            unsigned int seed = enterNumber("PRSC seed",0,AltiModule::PRSC_SEED_MASK);
            for(unsigned int i=itm; i<(itm+num); i++) {
                if((rtnv = alti->CTPPrescalerSeedWrite(i,seed)) != AltiModule::SUCCESS) {
                    CERR("writing PRSC seed for trigger item %d",i);
                    return(rtnv);
                }
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPPrescalerSeedWriteFile : public MenuItem {
  public:
    CTPPrescalerSeedWriteFile() { setName("write PRSC seed [file]"); }
    int action() {

        int rtnv(0);
        std::string fn = enterString("File name");

        if((rtnv = alti->CTPPrescalerSeedWrite(fn)) != AltiModule::SUCCESS) {
            CERR("writing PRSC seed from file \"%s\"",fn.c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPDeadtimeConfigRead : public MenuItem {
  public:
    CTPDeadtimeConfigRead() { setName("read  Deadtime configuration"); }
    int action() {

        int rtnv(0);
	u_int width;
	if((rtnv = alti->CTPSimpleDeadtimeWidthRead(width)) != AltiModule::SUCCESS) {
	  CERR("reading simple deadtime width", "");
	  return(rtnv);
        }

	u_int rate[4], level[4];	
	bool lb_ena[4];
	for(u_int bck = 0; bck < AltiModule::LEAKY_BUCKET_NUMBER; bck++) {
	  if((rtnv = alti->CTPLeakyBucketConfigRead(bck, rate[bck], level[bck])) != AltiModule::SUCCESS) {
	    CERR("reading leaky bucket %d configuration", bck);
	    return(rtnv);
	  }
	  if((rtnv = alti->CTPLeakyBucketEnableRead(bck, lb_ena[bck])) != AltiModule::SUCCESS) {
	    CERR("reading leaky bucket %d enabled", bck);
	    return(rtnv);
	  }
	}
	
	u_int size, number;	
	bool sw_ena;
	if((rtnv = alti->CTPSlidingWindowConfigRead(size, number)) != AltiModule::SUCCESS) {
	  CERR("reading slidingg window configuration", "");
	  return(rtnv);
	}
	if((rtnv = alti->CTPSlidingWindowEnableRead(sw_ena)) != AltiModule::SUCCESS) {
	  CERR("reading leaky bucket %d enabled", "");
	  return(rtnv);
	}

	printf(" - simple deadtime width = %d \n", width);
	for(u_int bck = 0; bck < AltiModule::LEAKY_BUCKET_NUMBER; bck++) {
	  printf(" - leaky bucket %d = %d/%d : %s \n", bck, level[bck], rate[bck], lb_ena[bck] ? "enabled" : "disabled");
	}	
	printf(" - sliding window = %d/%d : %s \n", number, size, sw_ena ? "enabled" : "disabled");

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPSimpleDeadtimeWrite : public MenuItem {
  public:
    CTPSimpleDeadtimeWrite() { setName("write Simple deadtime configuration"); }
    int action() {

        int rtnv(0);
   
	unsigned int width = enterInt("Simple deadtime length", 0, AltiModule::SIMPLE_LENGTH);
	if((rtnv = alti->CTPSimpleDeadtimeWidthWrite(width)) != AltiModule::SUCCESS) {
	  CERR("writing simple deadtime width", "");
	  return(rtnv);
        }
	
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPLeackyBucketConfigWrite : public MenuItem {
  public:
    CTPLeackyBucketConfigWrite() { setName("write Leaky bucket configuration"); }
    int action() {

        int rtnv(0);
	unsigned int bck = enterInt("Leaky bucket number (4=all)", 0, AltiModule::LEAKY_BUCKET_NUMBER);
        unsigned int lvl = enterInt("Leaky bucket level", 0, AltiModule::LEAKY_BUCKET_LEVEL);
	unsigned int rte = enterInt("Leaky bucket rate", 0, AltiModule::LEAKY_BUCKET_RATE);

	unsigned int xck, yck, ick;
        xck = (bck == AltiModule::LEAKY_BUCKET_NUMBER) ? 0 : bck;
        yck = (bck == AltiModule::LEAKY_BUCKET_NUMBER) ? AltiModule::LEAKY_BUCKET_NUMBER : (bck+1);
	for(ick=xck; ick<yck; ick++) {
	  if((rtnv = alti->CTPLeakyBucketConfigWrite(ick, rte, lvl)) != AltiModule::SUCCESS) {
	    CERR("writing leaky bucket %d configuration", bck);
	    return(rtnv);
	  }
	}
        return(rtnv);
    }
};


//------------------------------------------------------------------------------

class CTPLeackyBucketEnableWrite : public MenuItem {
  public:
    CTPLeackyBucketEnableWrite() { setName("write Leaky bucket enabled"); }
    int action() {

        int rtnv(0);
	unsigned int bck = enterInt("Leaky bucket number (4=all)", 0, AltiModule::LEAKY_BUCKET_NUMBER);
	bool         ena = enterInt("Leaky bucket enable (0=disable, 1=enable)", 0, 1);

	unsigned int xck, yck, ick;
        xck = (bck == AltiModule::LEAKY_BUCKET_NUMBER) ? 0 : bck;
        yck = (bck == AltiModule::LEAKY_BUCKET_NUMBER) ? AltiModule::LEAKY_BUCKET_NUMBER : (bck+1);

	for(ick=xck; ick<yck; ick++) {
	  if((rtnv = alti->CTPLeakyBucketEnableWrite(ick, ena)) != AltiModule::SUCCESS) {
	    CERR("writing leaky bucket %d enable", bck);
	    return(rtnv);
	  }
	}
        return(rtnv);
    }
};


//------------------------------------------------------------------------------

class CTPSlidingWindowConfigWrite : public MenuItem {
  public:
    CTPSlidingWindowConfigWrite() { setName("write Sliding window configuration"); }
    int action() {

        int rtnv(0);
        unsigned int numb = enterInt("Sliding window L1A number", 0, AltiModule::SLIDING_WINDOW_NUMBER);
	unsigned int size = enterInt("Sliding window size", 0, AltiModule::SLIDING_WINDOW_SIZE);

	if((rtnv = alti->CTPSlidingWindowConfigWrite(size, numb)) != AltiModule::SUCCESS) {
	  CERR("writing sliding window configuration", "");
	  return(rtnv);
	}     
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPSlidingWindowEnableWrite : public MenuItem {
  public:
    CTPSlidingWindowEnableWrite() { setName("write Sliding window enabled"); }
    int action() {

        int rtnv(0);
	bool         ena = enterInt("Sliding window enable (0=disable, 1=enable)", 0, 1);

	if((rtnv = alti->CTPSlidingWindowEnableWrite(ena)) != AltiModule::SUCCESS) {
	  CERR("writing sliding window enable", "");
	  return(rtnv);
	}     
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPTriggerAfterVetoEnableRead : public MenuItem {
  public:
    CTPTriggerAfterVetoEnableRead() { setName("read  Trigger After Veto enabled"); }
    int action() {

        int rtnv(0);
	bool ena;
	for (u_int item = 0; item < AltiModule::ITEM_NUMBER; item++) {
	  if((rtnv = alti->CTPTavEnableRead(item, ena)) != AltiModule::SUCCESS) {
	  CERR("reading Trigger After Veto item %d enable", item);
	  return(rtnv);
	  }     
	  std::printf("Trigger After Veto for item %d = %s\n", item, ena?" ENABLED" : "DISABLED");
	}
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPTriggerAfterVetoEnableWrite : public MenuItem {
  public:
    CTPTriggerAfterVetoEnableWrite() { setName("write Trigger After Veto enabled"); }
    int action() {

        int rtnv(0);
   	
	unsigned int itm = enterInt("Trigger item number (8=all)",0,AltiModule::ITEM_NUMBER);
	bool         ena = enterInt("Trigger After Veto enable (0=disable, 1=enable)", 0, 1);

	unsigned int xck, yck, ick;
        xck = (itm == AltiModule::ITEM_NUMBER) ? 0 : itm;
        yck = (itm == AltiModule::ITEM_NUMBER) ? AltiModule::ITEM_NUMBER : (itm+1);

	for(ick=xck; ick<yck; ick++) {
	  if((rtnv = alti->CTPTavEnableWrite(ick, ena)) != AltiModule::SUCCESS) {
	    CERR("writing Trigger After Veto item %d enable", ick);
	    return(rtnv);
	  }     
	}
        return(rtnv);
	}
};

//------------------------------------------------------------------------------

class CTPCntBcidMaskRead : public MenuItem {
  public:
    CTPCntBcidMaskRead() { setName("read  CNT BCID mask"); }
    int action() {

        int rtnv(0);
        unsigned int bcid = enterInt("BCID number (4096=all)", 0, AltiModule::BCID_NUMBER);

        if (bcid == AltiModule::BCID_NUMBER) {
            std::vector<bool> ena;
            if ((rtnv = alti->CTPCntBcidMaskRead(ena)) != AltiModule::SUCCESS) {
                CERR("reading CNT BCID mask", "");
                return (rtnv);
            }
            std::printf("CNT:\n");
            for (unsigned int i = 0; i < AltiModule::BCID_NUMBER; i++) {
                std::printf("BCID %4d = %s\n", i, ena[i] ? " ENABLED" : "DISABLED");
            }
        }

        else {
            unsigned int num = enterInt("BCID range", 0, AltiModule::BCID_NUMBER - bcid);
            bool ena;
            std::printf("CNT:\n");
            for (unsigned int i = bcid; i < (bcid + num); i++) {
                if ((rtnv = alti->CTPCntBcidMaskRead(i, ena)) != AltiModule::SUCCESS) {
                    CERR("reading CNT BCID mask %d", i);
                    return (rtnv);
                }
                std::printf("BCID %4d = %s\n", i, ena ? " ENABLED" : "DISABLED");
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntBcidMaskWrite : public MenuItem {
  public:
    CTPCntBcidMaskWrite() { setName("write CNT BCID mask"); }
    int action() {

        int rtnv(0);
        unsigned int bcid = enterInt("BCID number (4096=all)", 0, AltiModule::BCID_NUMBER);

        if (bcid == AltiModule::BCID_NUMBER) {
            bool ena = (bool) enterInt("Enable (0=no, 1=yes)", 0, 1);
            std::vector<bool> fna(AltiModule::BCID_NUMBER);
            for (unsigned int i = 0; i < AltiModule::BCID_NUMBER; i++) fna[i] = ena;
            if ((rtnv = alti->CTPCntBcidMaskWrite(fna)) != AltiModule::SUCCESS) {
                CERR("writing CNT BCID mask", "");
                return (rtnv);
            }
        }

        else {
            unsigned int num = enterInt("BCID range", 0, AltiModule::BCID_NUMBER - bcid);
            bool ena = (bool) enterInt("Enable (0=no, 1=yes)", 0, 1);
            for (unsigned int i = bcid; i < (bcid + num); i++) {
                if ((rtnv = alti->CTPCntBcidMaskWrite(i, ena)) != AltiModule::SUCCESS) {
                    CERR("writing CNT BCID mask %d", i);
                    return (rtnv);
                }
            }
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntBcidMaskReadFile : public MenuItem {
  public:
    CTPCntBcidMaskReadFile() { setName("read  CNT BCID mask [into file]"); }
    int action() {

        int rtnv(0);
        //unsigned int type = enterInt("CNT type (0=LUT, 1=TBP, 2=TAP, 3=TAV)",0,CtpcorePlusModule::CNT_NUMBER-1);
        std::string fn = enterString("File name");

        if ((rtnv = alti->CTPCntBcidMaskRead(fn)) != AltiModule::SUCCESS) {
            CERR("reading CNT BCID mask into file \"%s\"", fn.c_str());
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntBcidMaskWriteFile : public MenuItem {
  public:
    CTPCntBcidMaskWriteFile() { setName("write CNT BCID mask [from file]"); }
    int action() {

        int rtnv(0);
        //unsigned int type = enterInt("CNT type (0=LUT, 1=TBP, 2=TAP, 3=TAV)",0,CtpcorePlusModule::CNT_NUMBER-1);
        std::string fn = enterString("File name");

        if ((rtnv = alti->CTPCntBcidMaskWrite(fn)) != AltiModule::SUCCESS) {
            CERR("writing CNT BCID mask from file \"%s\"", fn.c_str());
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntBcidOffsetRead : public MenuItem {
  public:
    CTPCntBcidOffsetRead() { setName("read  CNT BCID offset"); }
    int action() {

        int rtnv(0);
        unsigned int off;

        if ((rtnv = alti->CTPCntBcidOffsetRead(off)) != AltiModule::SUCCESS) {
            CERR("reading CNT BCID offset", "");
            return (rtnv);
        }
        std::printf("CNT BCID offset = %4d (= 0x%03x)\n", off, off);

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntBcidOffsetWrite : public MenuItem {
  public:
    CTPCntBcidOffsetWrite() { setName("write CNT BCID offset"); }
    int action() {

        int rtnv(0);
        
        unsigned int off = enterInt("CNT BCID offset", 0, AltiModule::BCID_NUMBER);

        if ((rtnv = alti->CTPCntBcidOffsetWrite(off)) != AltiModule::SUCCESS) {
             CERR("writing CNT BCID offset", "");
             return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntControlPrint : public MenuItem {
  public:
    CTPCntControlPrint() { setName("print CNT control"); }
    int action() {

        int rtnv(0);

        std::printf("\nCNT:\n");
        if ((rtnv = alti->CTPCntControlPrint()) != AltiModule::SUCCESS) {
            CERR("printing CNT control", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntEnableRead : public MenuItem {
  public:
    CTPCntEnableRead() { setName("read  CNT enable"); }
    int action() {

        int rtnv(0);
        bool ena, sta;

        if ((rtnv = alti->CTPCntEnableRead(ena, sta)) != AltiModule::SUCCESS) {
            CERR("reading CNT control", "");
            return (rtnv);
        }
        std::printf("CNT: %s, status = %s\n", ena ? " ENABLED" : "DISABLED", sta ? " ENABLED" : "DISABLED");

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntEnableWrite : public MenuItem {
  public:
    CTPCntEnableWrite() { setName("write CNT enable"); }
    int action() {

        int rtnv(0);
        bool ena = (bool) enterInt("Enable (0=no, 1=yes)", 0, 1);

        if ((rtnv = alti->CTPCntEnableWrite(ena)) != AltiModule::SUCCESS) {
            CERR("writing CNT enable", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntCopyClearWrite : public MenuItem {
  public:
    CTPCntCopyClearWrite() { setName("write CNT copy/clear"); }
    int action() {

        int rtnv(0);

        if ((rtnv = alti->CTPCntCopyClearWrite()) != AltiModule::SUCCESS) {
            CERR("writing CNT copy/clear", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntTurnCountRead : public MenuItem {
  public:
    CTPCntTurnCountRead() { setName("read  CNT TURN count"); }
    int action() {

        int rtnv(0);
        unsigned int turn;
        if ((rtnv = alti->CTPCntTurnCountRead(turn)) != AltiModule::SUCCESS) {
            CERR("reading CNT TURN counter", "");
            return (rtnv);
        }
        std::printf("CNT TURN counter = 0x%08x (= %10d)\n", turn, turn);

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntCounterRead : public MenuItem {
  public:
    CTPCntCounterRead() { setName("read  CNT values"); }
    int action() {

        int rtnv(0);
	
	u_int itm, cnt_tbp, cnt_tap, cnt_tav, cnt_l1a, turn;
	float rate_tbp, rate_tap, rate_tav, rate_l1a;
	float tap_tbp, tav_tap;
	
	if ((rtnv = alti->CTPCntTurnCountRead(turn)) != AltiModule::SUCCESS) {
	  CERR("reading CNT TURN counter", "");
	  return (rtnv);
        }
     
	if ((rtnv = alti->CTPCntL1aCounterRead(cnt_l1a)) != AltiModule::SUCCESS) {
	  CERR("reading CNT L1A counter", "");
	  return (rtnv);
        }
	rate_l1a = (turn <= 0) ? 0. : static_cast<float>((cnt_l1a/turn)*11.2455);

	std::cout << "|--------------------------------------------------------------------------------------------------------------|" << std::endl;
        std::cout << "| item |           TBP             |           TAP             |           TAV             | TAP/TBP | TAV/TAP |" << std::endl;
	for(itm = 0; itm < AltiModule::ITEM_NUMBER; itm++) {
	  
	  if ((rtnv = alti->CTPCntTbpCounterRead(itm, cnt_tbp)) != AltiModule::SUCCESS) {
            CERR("reading CNT TBP counter", "");
            return (rtnv);
	  }
	  rate_tbp = (turn <= 0) ? 0. : ((float)cnt_tbp / (float)turn)*11.2455;

	  if ((rtnv = alti->CTPCntTapCounterRead(itm, cnt_tap)) != AltiModule::SUCCESS) {
            CERR("reading CNT TAP counter", "");
            return (rtnv);
	  }
  rate_tap = (turn <= 0) ? 0. : ((float)cnt_tap / (float)turn)*11.2455;

	  if ((rtnv = alti->CTPCntTavCounterRead(itm, cnt_tav)) != AltiModule::SUCCESS) {
            CERR("reading CNT TAV counter", "");
            return (rtnv);
	  }
rate_tav = (turn <= 0) ? 0. : ((float)cnt_tav / (float)turn)*11.2455;

	  tap_tbp = (cnt_tbp <= 0) ? 0. : (float)cnt_tap / (float)cnt_tbp;
	  tav_tap = (cnt_tap <= 0) ? 0. : (float)cnt_tav / (float)cnt_tap;	  
	  std::printf("|   %d  | %10d = %8.3f kHz | %10d = %8.3f kHz | %10d = %8.3f kHz |  %.3f  |  %.3f  |\n", itm, cnt_tbp, rate_tbp, cnt_tap, rate_tap, cnt_tav, rate_tav, tap_tbp, tav_tap);  
	}
      	std::cout << "|--------------------------------------------------------------------------------------------------------------|" << std::endl;
	std::printf("|  L1a |  %10d = %3.3f kHz                                                                              |\n", cnt_l1a, rate_l1a); 
	std::printf("| turn |  %10d                                                                                           |\n", turn); 
	std::cout << "|--------------------------------------------------------------------------------------------------------------|" << std::endl;

        return (rtnv);
    }
};

//------------------------------------------------------------------------------

class CTPCntRequestPrint : public MenuItem {
  public:
    CTPCntRequestPrint() { setName("print CNT request"); }
    int action() {

        int rtnv(0);

        std::printf("\nCNT:\n");
        if ((rtnv = alti->CTPCntRequestPrint()) != AltiModule::SUCCESS) {
            CERR("printing CNT request", "");
            return (rtnv);
        }

        return (rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


int main(int argc,char** argv) {

    Menu menu("AltiModule main menu");

    // menu for AltiModule
    menu.init(new AltiModuleOpen);
    menu.exit(new AltiModuleClose);
    
    // menu for CSR
    Menu menuCSR("[\"CSR\" menu] VME64x CR/CSR space");
    menuCSR.add(new AltiCSRManufacturerIdRead);
    menuCSR.add(new AltiCSRBoardIdRead);
    menuCSR.add(new AltiCSRRevisionIdRead);
    menuCSR.add(new AltiCSRBARRead);
    menuCSR.add(new AltiCSRADERRead);
    menuCSR.add(new AltiCSRADERWrite);
    menuCSR.add(new AltiSerialNumber);
    menu.add(&menuCSR);
    
    // menu for CFG
    Menu menuCFG("[\"CFG\" menu] Global configuration");
    menuCFG.add(new AltiReset);
    menuCFG.add(new AltiSetup);
    menuCFG.add(new AltiCheck);

    menuCFG.add(new AltiConfigRead);
    menuCFG.add(new AltiConfigReadFile);
    menuCFG.add(new AltiConfigWriteFile);
    menuCFG.add(new AltiConfigWrite);
    menuCFG.add(new AltiConfigWritePredefined);
    menu.add(&menuCFG);
    
    // menu for CLK
    Menu menuCLK("[\"CLK\" menu] Clocking");
    menuCLK.add(new AltiCLKStatusRead);
    menuCLK.add(new AltiCLKSelectWrite);
    menuCLK.add(new AltiCLKSelectToggle);
    menuCLK.add(new AltiCLKPhaseShift);
    menuCLK.add(new AltiCLKPhaseReset);
    menuCLK.add(new AltiCLKReset);
    menuCLK.add(new AltiCLKJitterCleanerInSelWrite);
    menuCLK.add(new AltiCLKJitterCleanerConfigRead);
    menuCLK.add(new AltiCLKJitterCleanerConfigWrite);
    menuCLK.add(new AltiCLKJitterCleanerConfigWriteDefault);
    menuCLK.add(new AltiCLKJitterCleanerReset);
    menuCLK.add(new AltiCLKJitterCleanerResync);
    menuCLK.add(new AltiCLKStatusClear);
    menu.add(&menuCLK);

    // menu for SWX + EQZ : SIG
    Menu menuSIG("[\"SIG\" menu] Signals I/O");
    menuSIG.add(new AltiSIGSwitchConfigRead);
    menuSIG.add(new AltiSIGSwitchConfigWrite);
    menuSIG.add(new AltiSIGSwitchMasterConfigurationWrite);
    menuSIG.add(new AltiSIGSwitchCtpSlaveConfigurationWrite);
    menuSIG.add(new AltiSIGSwitchAltiSlaveConfigurationWrite);
    menuSIG.add(new AltiSIGSwitchLemoSlaveConfigurationWrite);
    menuSIG.add(new AltiSIGEqualizersConfigRead);
    menuSIG.add(new AltiSIGEqualizersPredefinedModeWrite);
    menuSIG.add(new AltiSIGEqualizersConfigWrite);
    menuSIG.add(new AltiSIGInputSyncHistogramsRead);
    menuSIG.add(new AltiSIGInputSyncHistogramsReset);
    menuSIG.add(new AltiSIGInputSyncStatusRead);
    menuSIG.add(new AltiSIGInputSyncEnableWrite);
    menuSIG.add(new AltiSIGSyncShapeStatusRead);
    menuSIG.add(new AltiSIGSyncShapeEnableWrite);
    menuSIG.add(new AltiSIGMuxBGOOutputLEMORead);
    menuSIG.add(new AltiSIGMuxBGOOutputLEMOWrite);
    menuSIG.add(new AltiSIGMuxTTRInputLEMORead);
    menuSIG.add(new AltiSIGOutputSelectRead);
    menuSIG.add(new AltiSIGOutputSelectWrite);
    menuSIG.add(new AltiSIGOrbInputSelectRead);
    menuSIG.add(new AltiSIGOrbInputSelectWrite);
    menuSIG.add(new AltiSIGTurnCounterOrbitSourceRead);
    menuSIG.add(new AltiSIGTurnCounterOrbitSourceWrite);
    menuSIG.add(new AltiSIGTurnCounterMaskRead);
    menuSIG.add(new AltiSIGTurnCounterMaskWrite);
    menuSIG.add(new AltiSIGTurnCounterReset);
    menuSIG.add(new AltiSIGTurnCounterFreqRead);
    menuSIG.add(new AltiSIGTurnCounterFreqWrite);
    menuSIG.add(new AltiSIGTurnCounterValueRead);
    menuSIG.add(new AltiSIGBGo2L1aDelayRead);
    menuSIG.add(new AltiSIGBGo2L1aDelayWrite);
    menuSIG.add(new AltiSIGBGo2L1aCounterRead);
    menu.add(&menuSIG);
   
    // menu for Busy Monitoring
    Menu menuBSY("[\"BSY\" menu] Busy ");
    menuBSY.add(new AltiBSYStatusRead);
    menuBSY.add(new AltiBSYInputSelectWrite);
    menuBSY.add(new AltiBSYOutputSelectWrite);
    menuBSY.add(new AltiBSYMaskingWrite);
    menuBSY.add(new AltiBSYMaskingPatWrite);
    menuBSY.add(new AltiBSYFrontPanelLevelWrite);
    menuBSY.add(new AltiBSYConstLevelRegWrite);

    menuBSY.add(new AltiBSYMonitoringControlRead);
    menuBSY.add(new AltiBSYMonitoringControlWrite);
    menuBSY.add(new AltiBSYSelectRead);
    menuBSY.add(new AltiBSYSelectWrite);
    menuBSY.add(new AltiBSYCounterReset);
    menuBSY.add(new AltiBSYIntervalRead);
    menuBSY.add(new AltiBSYIntervalWrite);
    menuBSY.add(new AltiBSYFifoWatermarkRead);
    menuBSY.add(new AltiBSYFifoWatermarkWrite);
    menuBSY.add(new AltiBSYFifoWrite);
    menuBSY.add(new AltiBSYFifoStatus);
    menuBSY.add(new AltiBSYFifoRead);
    menuBSY.add(new AltiBSYFifoReadCounter);
    menuBSY.add(new AltiBSYFifoReadAverage);
    menuBSY.add(new AltiBSYFifoReset);

    menu.add(&menuBSY);

    // menu for CRQ
    Menu menuCRQ("[\"CRQ\" menu] Calibration requests");
    menuCRQ.add(new AltiCRQInputSyncHistogramsRead);
    menuCRQ.add(new AltiCRQInputSyncHistogramsReset);
    menuCRQ.add(new AltiCRQInputSyncStatusRead);
    menuCRQ.add(new AltiCRQInputSyncEnableWrite);
    menuCRQ.add(new AltiCRQRJ45InputShapeRead);
    menuCRQ.add(new AltiCRQRJ45InputShapeWrite);
    menuCRQ.add(new AltiCRQLocalSourceRead);
    menuCRQ.add(new AltiCRQLocalSourceWrite);
    menuCRQ.add(new AltiCRQOutputSelectRead);
    menuCRQ.add(new AltiCRQOutputSelectWrite);
    menuCRQ.add(new AltiCRQConstLevelRegRead);
    menuCRQ.add(new AltiCRQConstLevelRegWrite);
    menu.add(&menuCRQ);

    // menu for PAT
    Menu menuPAT("[\"PAT\" menu] Pattern generation memory");
    menuPAT.add(new AltiPATStatusRead);
    menuPAT.add(new AltiPATSetModePattern);
    menuPAT.add(new AltiPATGenerationEnable);
    menuPAT.add(new AltiPATGenerationDisable);
    menuPAT.add(new AltiPATGenerationRepeatWrite);
    menuPAT.add(new AltiPATGenerationStartAddressWrite);
    menuPAT.add(new AltiPATGenerationStopAddressWrite);
    menuPAT.add(new AltiPATGenerationTrigCondRead);
    menuPAT.add(new AltiPATGenerationTrigCondWrite);
    menuPAT.add(new AltiPATRead);
    menuPAT.add(new AltiPATReadFile);
    menuPAT.add(new AltiPATWriteFile);
    Menu menuPATlowlevelMEM("MEM       menu");
    menuPATlowlevelMEM.add(new AltiMEMDataPATSNPReadNumber);
    menuPATlowlevelMEM.add(new AltiMEMDataPATSNPWriteNumber);
    menuPATlowlevelMEM.add(new AltiMEMDataPATSNPReadVector);
    menuPATlowlevelMEM.add(new AltiMEMDataPATSNPWriteVector);
    menuPATlowlevelMEM.add(new AltiMEMDataPATSNPReadBlock);
    menuPATlowlevelMEM.add(new AltiMEMDataPATSNPWriteBlock);
    menuPAT.add(&menuPATlowlevelMEM);
    menu.add(&menuPAT);

    // menu for SNP
    Menu menuSNP("[\"SNP\" menu] Snapshot memory");
    menuSNP.add(new AltiSNPStatusRead);
    menuSNP.add(new AltiSNPSetModeSnapshot);
    menuSNP.add(new AltiSNPEnable);
    menuSNP.add(new AltiSNPDisable);
    menuSNP.add(new AltiSNPMaskWrite);
    menuSNP.add(new AltiSNPTrigCondRead);
    menuSNP.add(new AltiSNPTrigCondWrite);
    menuSNP.add(new AltiSNPRead);
    menuSNP.add(new AltiSNPReadFile);
    Menu menuSNPlowlevelMEM("MEM     menu");
    menuSNP.add(&menuSNPlowlevelMEM);
    menuSNPlowlevelMEM.add(new AltiMEMDataPATSNPReadNumber);
    menuSNPlowlevelMEM.add(new AltiMEMDataPATSNPWriteNumber);
    menuSNPlowlevelMEM.add(new AltiMEMDataPATSNPReadVector);
    menuSNPlowlevelMEM.add(new AltiMEMDataPATSNPWriteVector);
    menuSNPlowlevelMEM.add(new AltiMEMDataPATSNPReadBlock);
    menuSNPlowlevelMEM.add(new AltiMEMDataPATSNPWriteBlock);
    menu.add(&menuSNP);
 
    // menu for TTC encoder
    Menu menuENC("[\"ENC\" menu] TTC encoder");
    menuENC.add(new AltiENCTransmitterStatusRead);
    menuENC.add(new AltiENCTransmitterEnable);
    menuENC.add(new AltiENCTransmitterDelayWrite);
    menuENC.add(new AltiENCTransmitterFaultClear);
    menuENC.add(new AltiENCSourceRead);
    menuENC.add(new AltiENCL1aSourceWrite);
    menuENC.add(new AltiENCOrbitSourceWrite);
    menuENC.add(new AltiENCBgoSourceWrite);
    menuENC.add(new AltiENCTtypSourceWrite);
    menuENC.add(new AltiENCInhibitParamsRead);
    menuENC.add(new AltiENCInhibitParamsWrite);
    menuENC.add(new AltiENCTriggerWordStatusRead);
    menuENC.add(new AltiENCTriggerWordDelayWrite);
    menuENC.add(new AltiENCTriggerWordControlWrite);
    menuENC.add(new AltiENCTriggerWordCounterSourceWrite);
    menuENC.add(new AltiENCTriggerWordCounterReset);
    menuENC.add(new AltiENCTriggerWordCycleEnable);
    menuENC.add(new AltiENCTriggerWordCycleDisable);
    menuENC.add(new AltiENCBgoCommandModeRead);
    menuENC.add(new AltiENCBgoCommandModeWrite);
    menuENC.add(new AltiENCBgoCommandModeHighLevelWrite);
    menuENC.add(new AltiENCBgoFifoRetransmitSelect);
    menuENC.add(new AltiENCFifoStatusRead);
    menuENC.add(new AltiENCFifoRead); 
    menuENC.add(new AltiENCFifoReset);
    menuENC.add(new AltiENCBgoGenerate);
    menuENC.add(new AltiENCBgoCommandPut);
    menuENC.add(new AltiENCAsyncCommandPut);
    menuENC.add(new AltiENCAsyncCommandPending);
    menuENC.add(new AltiENCRequestStatus);
    menuENC.add(new AltiENCRequestReset);
    menuENC.add(new AltiENCRequestCounterRead);
    menuENC.add(new AltiENCRequestCounterReset);
    menu.add(&menuENC);

    // menu for TTC decoder
    Menu menuDEC("[\"DEC\" menu] TTC decoder");
    menuDEC.add(new AltiDECStatusRead);
    menuDEC.add(new AltiDECStatusClear);
    menuDEC.add(new AltiDECCounterReset);
    menuDEC.add(new AltiDECReset);
    menuDEC.add(new AltiDECEnable);
    menuDEC.add(new AltiDECDisable);
    menuDEC.add(new AltiDECSetBcrWord);
    menuDEC.add(new AltiDECSetLoop);
    menuDEC.add(new AltiDECSetTrigCond);
    menuDEC.add(new AltiDECRead);
    menuDEC.add(new AltiDECReadFile);
    Menu menuDEClowlevelMEM("MEM     menu");
    menuDEClowlevelMEM.add(new AltiMEMDataDECReadNumber);
    menuDEClowlevelMEM.add(new AltiMEMDataDECWriteNumber);
    menuDEClowlevelMEM.add(new AltiMEMDataDECReadVector);
    menuDEClowlevelMEM.add(new AltiMEMDataDECWriteVector);
    menuDEClowlevelMEM.add(new AltiMEMDataDECReadBlock);
    menuDEClowlevelMEM.add(new AltiMEMDataDECWriteBlock);
    menuDEC.add(&menuDEClowlevelMEM);
    menu.add(&menuDEC);

    // menu for ECR
    Menu menuECR("[\"ECR\" menu] ECR generation");
    menuECR.add(new ECRGenerationRead);
    menuECR.add(new ECRGenerationWrite);
    menuECR.add(new ECRGenerate);
    menu.add(&menuECR);
    // menu for T2L
    Menu menuT2L("[\"T2L\" menu] TTC to LAN");
    menuT2L.add(new T2LControlReset);
    menuT2L.add(new T2LEventIdentifierRead);
    menuT2L.add(new T2LFifoStatusRead);
    menuT2L.add(new T2LFifoWrite);
    menuT2L.add(new T2LFifoRead);
    menu.add(&menuT2L);
    // menu for CNT
    Menu menuCNT("[\"CNT\" menu] Monitoring counters");
    menuCNT.add(new CNTBcidMaskRead);
    menuCNT.add(new CNTBcidMaskWrite);
    menuCNT.add(new CNTBcidMaskReadFile);
    menuCNT.add(new CNTBcidMaskWriteFile);
    menuCNT.add(new CNTBcidOffsetRead);
    menuCNT.add(new CNTBcidOffsetWrite);
    menuCNT.add(new CNTEdgeEnableRead);
    menuCNT.add(new CNTEdgeEnableWrite);
    menuCNT.add(new CNTOrbitCounterReset);
    menuCNT.add(new CNTOrbitCounterRead);
    menuCNT.add(new CNTEnableRead);
    menuCNT.add(new CNTEnableWrite);
    menuCNT.add(new CNTCopyClearWrite);
    menuCNT.add(new CNTTurnCountRead);
    menuCNT.add(new CNTCounterRead);
    menuCNT.add(new CNTControlPrint);
    menuCNT.add(new CNTRequestPrint);
    menuCNT.add(new CNTTtypLutReadFile);
    menuCNT.add(new CNTTtypLutWriteFile);
    menuCNT.add(new CNTTtypLutPrint);
    menu.add(&menuCNT);
    // menu for mini-CTP
    Menu menuCTP("[\"CTP\" menu] Mini-CTP");
    menuCTP.add(new CTPInputRead);
    menuCTP.add(new CTPInputWrite);
    menuCTP.add(new CTPBunchGroupDataRead);
    menuCTP.add(new CTPBunchGroupDataWrite);
    menuCTP.add(new CTPBunchGroupDataReadFile);
    menuCTP.add(new CTPBunchGroupDataWriteFile);
    menuCTP.add(new CTPBunchGroupMaskRead);
    menuCTP.add(new CTPBunchGroupMaskWrite);
    menuCTP.add(new CTPBunchGroupBcidOffsetRead);
    menuCTP.add(new CTPBunchGroupBcidOffsetWrite);
    menuCTP.add(new CTPBunchGroupItemEnableRead);
    menuCTP.add(new CTPBunchGroupItemEnableWrite);
    menuCTP.add(new CTPRandomTriggerThresholdRead);
    menuCTP.add(new CTPRandomTriggerThresholdWrite);
    menuCTP.add(new CTPTriggerLutRead);
    menuCTP.add(new CTPTriggerLutReadFile);
    menuCTP.add(new CTPTriggerLutWriteFile);
    menuCTP.add(new CTPTtypLutRead);
    menuCTP.add(new CTPTtypLutReadFile);
    menuCTP.add(new CTPTtypLutWriteFile);
    menuCTP.add(new CTPPrescalerThresholdRead);
    menuCTP.add(new CTPPrescalerThresholdWrite);
    menuCTP.add(new CTPPrescalerThresholdReadFile);
    menuCTP.add(new CTPPrescalerThresholdWriteFile);
    menuCTP.add(new CTPPrescalerSeedWrite);
    menuCTP.add(new CTPPrescalerSeedWriteFile);
    menuCTP.add(new CTPDeadtimeConfigRead);
    menuCTP.add(new CTPSimpleDeadtimeWrite);
    menuCTP.add(new CTPLeackyBucketConfigWrite);
    menuCTP.add(new CTPLeackyBucketEnableWrite);
    menuCTP.add(new CTPSlidingWindowConfigWrite);
    menuCTP.add(new CTPSlidingWindowEnableWrite);
    menuCTP.add(new CTPTriggerAfterVetoEnableRead);
    menuCTP.add(new CTPTriggerAfterVetoEnableWrite);
    Menu menuCTPCNT("-- Menu Counters --");
    menuCTPCNT.add(new CTPCntBcidMaskRead);
    menuCTPCNT.add(new CTPCntBcidMaskWrite);
    menuCTPCNT.add(new CTPCntBcidMaskReadFile);
    menuCTPCNT.add(new CTPCntBcidMaskWriteFile);
    menuCTPCNT.add(new CTPCntBcidOffsetRead);
    menuCTPCNT.add(new CTPCntBcidOffsetWrite);
    menuCTPCNT.add(new CTPCntEnableRead);
    menuCTPCNT.add(new CTPCntEnableWrite);
    menuCTPCNT.add(new CTPCntCopyClearWrite);
    menuCTPCNT.add(new CTPCntTurnCountRead);
    menuCTPCNT.add(new CTPCntCounterRead);
    menuCTPCNT.add(new CTPCntControlPrint);
    menuCTPCNT.add(new CTPCntRequestPrint);
    menuCTP.add(&menuCTPCNT);
    menu.add(&menuCTP);

    // menu for VLT + TMP : MON
    Menu menuMON("[\"MON\" menu] Hardware monitoring");
    menuMON.add(new AltiMONAlarmsRead);
    menuMON.add(new AltiMONVoltageRead);
    menuMON.add(new AltiMONTempRead);
    menu.add(&menuMON);

    // menu for I2C
    Menu menuI2C("[\"I2C\" menu] I2C network");
    menuI2C.add(new AltiI2CSetup);
    menuI2C.add(new AltiI2CDump);
    menuI2C.add(new AltiI2CRead);
    //menuI2C.add(new I2CReadNode);
    menuI2C.add(new AltiI2CReadDevice);
    menuI2C.add(new AltiI2CReadAll);
    menuI2C.add(new AltiI2CWrite);
    menu.add(&menuI2C);
    
    Menu menuPBM("[\"PBM\" menu] Per-bunch monitoring");
    menuPBM.add(new PBMCountersStatus);
    menuPBM.add(new PBMCountersEnable);
    menuPBM.add(new PBMCountersClear);
    menuPBM.add(new PBMWriteOffset);
    menuPBM.add(new PBMReadMemory);
    menu.add(&menuPBM);

    // start menu
    menu.execute();

    exit(0);
}
