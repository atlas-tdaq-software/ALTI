//******************************************************************************
// file: testAltiInitial.cc
// desc: test program for ALTI initialization
// auth: 09-MAR-2018 P. Kuzmanovic
//*****************************************************************************

#include "ALTI/TestAltiInitial.h"

using namespace LVL1;

//------------------------------------------------------------------------------

int main(int argc, char* argv[]) {

    TestAltiInitial tctp;
    TestAltiInitial::Configuration tcfg;

    // read command-line options
    tcfg.config(argc, argv);

    // run TestAltiInitial
    if(tctp.test(tcfg) != daq::tmgr::TmPass) {
        exit(-1);
    }

    exit(0);
}
