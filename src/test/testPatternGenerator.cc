//******************************************************************************
// file: testPatternGenerator.cc
// desc: test program for pattern generation by miniCTP
// auth: 11-NOV-2019 M. Saimpert
//*****************************************************************************

#include "ALTI/PatternGenerator.h"

//------------------------------------------------------------------------------

int main(int argc, char* argv[]) {

    // PatternGenerator object
    PatternGenerator *pg = new PatternGenerator();

    // Configuration struct
    PatternGenerator::Configuration cfg;

    std::cout << std::endl << "*****PATTERNGENERATOR TEST STARTED*****" << std::endl;

    // Check arguments
    if( argc > 1){

      std::cout << "Custom configuration found ..." << std::endl;
      int status = cfg.config(argc,argv);
      
      if(status==1) std::cout << "read successfully" << std::endl;
      else{

        std::cout << "ERROR reading configuration file, exiting." << std::endl;
        exit(-1); 

      }

    }
    else{ // default config

      std::cout << "WARNING: no custom configuration detected ... will use default test config." << std::endl;

      // Default config
      //cfg.output_file = "pg_alti.dat";
      //cfg.rate = 1; // [kHz]
      //cfg.rate_ecr = 2e-04; // [kHz]
      //cfg.smpl_deadtime = 4; // [BC]
      //cfg.seed = -99;
      //cfg.applyBCRVeto = true;
      //cfg.applyCalibReq = true;
      //cfg.applySplitORBVeto = false;
      //cfg.loopTTYP = true;
  
      // Sliding window
      cfg.sliding_window.first = 15;    // [LIA]
      cfg.sliding_window.second = 3600; // [BC]
  
      // Leaky buckets
      PatternGenerator::LeakyBucket lb_l1calo_csc;
      lb_l1calo_csc.size = 15;      // [L1A]
      lb_l1calo_csc.inv_rate = 370; // [BC]
      cfg.bucket_vector.push_back(lb_l1calo_csc);
  
      PatternGenerator::LeakyBucket lb_trt;
      lb_trt.size = 42;
      lb_trt.inv_rate = 384;
      cfg.bucket_vector.push_back(lb_trt);
  
      PatternGenerator::LeakyBucket lb_lar_pix_sct;
      lb_lar_pix_sct.size = 9;
      lb_lar_pix_sct.inv_rate = 351;
      cfg.bucket_vector.push_back(lb_lar_pix_sct);
  
      PatternGenerator::LeakyBucket lb_l1topo;
      lb_l1topo.size = 14;
      lb_l1topo.inv_rate = 260;
  
      cfg.bucket_vector.push_back(lb_l1topo);

    }

    // Print configuration
    //cfg.print();
    cfg.print_detail();

    // Run PatterGenerator
    pg->init(cfg);
    pg->generate();
    pg->stat();
    pg->exit();

    exit(0);
}

