//*****************************************************************************
// file: EventCounter.cc
// desc: class for Level1 event counter
// auth: 12-JAN-2010 R. Spiwoks, taken on 15-OCT-2018 by P. Kuzmanovic
//*****************************************************************************

// $Id$

#include <iostream>
#include <iomanip>

#include "RCDUtilities/RCDUtilities.h"
//#include "MuctpiCommon/MuctpiCommon.h"
#include "ALTI/AltiCommon.h"
#include "ALTI/EventCounter.h"

using namespace LVL1;

//------------------------------------------------------------------------------

EventCounter::EventCounter(const unsigned int data) : m_mask(MASK) {

    // reset
    reset(data);
}

//------------------------------------------------------------------------------

EventCounter::EventCounter(const std::string& name, const unsigned int data) : m_name(name), m_mask(MASK) {

    // reset
    reset(data);
}

//------------------------------------------------------------------------------

EventCounter::EventCounter(const EventCounter& cnt) {

    // call assignment operator
    *this = cnt;
}

//------------------------------------------------------------------------------

EventCounter::~EventCounter() {

    // nothing to be done
}

//------------------------------------------------------------------------------

EventCounter& EventCounter::operator=(const EventCounter& cnt) {

    // return if same object
    if(this == &cnt) return(*this);

    // copy mask
    m_mask = cnt.m_mask;

    // copy data
    m_data = cnt.m_data;
    m_data_old = cnt.m_data_old;

    // copy time
    m_time = cnt.m_time;
    m_time_old = cnt.m_time_old;

    return(*this);
}

//------------------------------------------------------------------------------

void EventCounter::mask(const unsigned int mask) {

    // set mask
    m_mask = mask;
}

//------------------------------------------------------------------------------

void EventCounter::data(const unsigned int data) {

    // set data
    m_data_old = m_data;
    m_data = data;
    if(m_data_num<2) m_data_num++;

    // set time
    m_time_old = m_time;
    m_time = AltiCommon::getTime();
}

//------------------------------------------------------------------------------

void EventCounter::data(const unsigned int data, const double time) {

    // set data
    m_data_old = m_data;
    m_data = data;
    if(m_data_num<2) m_data_num++;

    // set time
    m_time_old = m_time;
    m_time = time;
}

//------------------------------------------------------------------------------

void EventCounter::time(const double time) {

    // set time
    m_time_old = m_time;
    m_time = time;
}

//------------------------------------------------------------------------------

double EventCounter::rate() const {

    unsigned int dnew, dold;
    double rate;

    CDBG("data = %10u (%10u), time = %10.3f (%10.3f)",m_data,m_data_old,m_time,m_time_old);

    if(m_data_num < 2) {
        rate = 0;
    }
    else {
        dnew = m_data & m_mask;
        dold = m_data_old & m_mask;
        rate = (dnew >= dold) ? static_cast<double>(dnew - dold)/(m_time - m_time_old) : static_cast<double>(m_mask - dold + dnew + 1)/(m_time - m_time_old);
    }

    return(rate);
}

//------------------------------------------------------------------------------

std::string EventCounter::string() const {

    char os[AltiCommon::STRING_LENGTH];

    if(m_data_num == 0) {
        std::sprintf(os,"   <EMPTY>               ");
    }
    else if(m_data_num == 1) {
        std::sprintf(os,"%10u               ",m_data);
    }
    else {
        std::sprintf(os,"%10u => %s",data(), AltiCommon::getFrequency(rate()).c_str());
    }

    return(os);
}

//------------------------------------------------------------------------------

void EventCounter::reset(const unsigned int data) {

    // set data
    m_data = data;
    m_data_old = m_data;
    m_data_num = 0;

    // set time
    m_time = AltiCommon::getTime();
    m_time_old = m_time;
}

//------------------------------------------------------------------------------

void EventCounter::dump(std::ostream& s) const {

    unsigned int dnew, dold;
    char os[AltiCommon::STRING_LENGTH];

    s << "********************************************************************************************" << std::endl;
    std::sprintf(os,"EventCounter \"%s\": mask = 0x%08x, values = %d%s\n",m_name.c_str(),m_mask,m_data_num,m_data_num==0?"(EMPTY)":""); s << os;
    s << "--------------------------------------------------------------------------------------------" << std::endl;
    dnew = m_data & m_mask;
    dold = m_data_old & m_mask;
    std::sprintf(os,"data = %16u    , data(old) = %16u     => diff = %16u\n",dnew,dold,dnew>=dold?dnew-dold:m_mask-dold+dnew+1); s << os;
    std::sprintf(os,"time = %16.3f sec, time(old) = %16.3f sec => diff = %16.3f sec\n",m_time,m_time_old,m_time-m_time_old); s << os;
}

