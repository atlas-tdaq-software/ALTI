//******************************************************************************
// file: PatternGenerator.h
// desc: test class for pattern generation by miniCTP
// auth: 08-NOV-2019 M. Saimpert
//******************************************************************************
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <sstream>
#include <iomanip>

#include "ALTI/PatternGenerator.h"
const float PatternGenerator::lhc_rev_freq=11.2455; // kHz

// public type
int PatternGenerator::Configuration::config(int argc, char* argv[]){

  // only one argument needed
  if(argc>2) std::cout << "more than 1 argument found, only the first one will be considered." << std::endl;

  // config file
  std::string cfg_file = argv[1]; 
  if(argc>2) std::cout << "Configuration file: " << cfg_file << std::endl;

  // read config file line by line
  std::ifstream readFromFile(cfg_file);
  std::string line;
  while (std::getline(readFromFile, line)){

    if(line.empty()) continue;
    if(line.at(0)==' ' || line.at(0)=='#') continue;

    std::istringstream iss(line);
    std::string a,b;
    if (!(iss >> a >> b)){

      std::cout << "ERROR: config file format not recognized." << std::endl;  
      return -1; 

    }

    // 1 argument expected
    if(a=="output_file")        output_file   = b;
    else if(a=="rate")          rate          = (std::stoi(b))/1000.;
    else if(a=="smpl_deadtime") smpl_deadtime = std::stoi(b);
    else if(a=="seed")          seed          = std::stoi(b);
    else if(a=="applyBCRVeto")  applyBCRVeto  = (b== "1");
    else if(a=="applyCalibReq") applyCalibReq = (b== "1");
    else if(a=="loopTTYP")      loopTTYP      = (b== "1");

    // 2 arguments expected
    else if(a=="sliding_window" || a.find("LeakyBucket")!= std::string::npos){

      std::string c;
      std::istringstream iss2(line);
      if (!(iss2 >> a >> b >> c)){

        std::cout << "ERROR: config file format not recognized." << std::endl;  
        return -1; 

      } 

      if(a=="sliding_window"){

        sliding_window.first =  std::stoi(b);
        sliding_window.second = std::stoi(c);

      }
      else if(a.find("LeakyBucket")!= std::string::npos){

        PatternGenerator::LeakyBucket lb;
        lb.size =     std::stoi(b); 
        lb.inv_rate = std::stoi(c); 
        bucket_vector.push_back(lb);
      }

    } // 2 arguments

  } // end line by line reading

  return 1;
}

void PatternGenerator::Configuration::print(){

  std::cout << std::endl << "*****Configuration object print*****" << std::endl;
  std::cout << "name of file output:                                    " << output_file   << std::endl;
  std::cout << "simulated L1 trigger rate (bef. deadtime) [kHz]:        " << rate          << std::endl;
  std::cout << "simulated simple deadtime [BC]:                         " << smpl_deadtime << std::endl;
  // leaky bucket
  if(!bucket_vector.empty()) std::cout << "simulated complex deadtime [Nbucket]:                   " << bucket_vector.size() << std::endl;
  // sw window
  if(sliding_window.first>0 && sliding_window.second>0) std::cout << "simulated sliding window:                               YES" << std::endl;
  else                                                  std::cout << "simulated sliding window:          i                    NO" << std::endl;
  // BCRVeto
  if(applyBCRVeto) std::cout << "apply BCRVeto [BC 0-3539 & 3561-3563]:                  " << "YES" << std::endl;
  else             std::cout << "apply BCRVeto [BC 0-3539 & 3561-3563]:                  " << "NO" << std::endl;
  // CalibReq
  if(applyCalibReq) std::cout << "apply CalibReq [BC 3445-3514]:                          " << "YES" << std::endl;
  else              std::cout << "apply CalibReq [BC 3445-3514]:                          " << "NO" << std::endl;
  // loop over TTYP
  if(loopTTYP) std::cout << "loop over TTYP:                                         " << "YES" << std::endl;
  else std::cout << "loop over TTYP:                                         " << "NO" << std::endl;

  std::cout << "seed for random generator:                              " << seed<< std::endl;

}

void PatternGenerator::Configuration::print_detail(){

  print();
  std::cout << std::endl << "*****Configuration object print (detailed)*****" << std::endl;

  std::cout << "***Complex deadtime***" << std::endl;
  if(!bucket_vector.empty()){

    for (unsigned i_bucket=0; i_bucket<bucket_vector.size(); ++i_bucket){

      std::cout << "*leaky bucket " << std::to_string(i_bucket+1) << std::endl;
      std::cout << "size [L1A]                 : " << bucket_vector.at(i_bucket).size << std::endl;
      std::cout << "inverse leaking rate [BC]  : " << bucket_vector.at(i_bucket).inv_rate << std::endl;

    }

  }
  if(sliding_window.first>0 && sliding_window.second>0 ){

     std::cout << std::endl << "***Sliding window***" << std::endl;
     std::cout << "size [NL1A, NWindow]       : " << sliding_window.first << "," << sliding_window.second << std::endl;

  }

}

// private type
void PatternGenerator::Command::reset(){

  ORB=0;
  BUSY=0;
  CREQ[0]=0; CREQ[1]=0; CREQ[2]=0;
  TTYP=getTTYP(0);
  BGO[0]=0; BGO[1]=0; BGO[2]=0; BGO[3]=0;
  TTR[0]=0; TTR[1]=0; TTR[2]=0;
  L1A=0;
  MULTIPLICITY=0;

}

std::string PatternGenerator::Command::getWord(){

  std::string word = " " + std::to_string(ORB) + 
                     " " + std::to_string(BUSY) + 
                            std::to_string(CREQ[0]) + std::to_string(CREQ[1]) + std::to_string(CREQ[2]) +
                     " " + TTYP + 
                     " " + std::to_string(BGO[0]) + std::to_string(BGO[1]) + std::to_string(BGO[2]) + std::to_string(BGO[3]) + 
                     " " + std::to_string(TTR[0]) + std::to_string(TTR[1]) + std::to_string(TTR[2]) + std::to_string(L1A) +
                     " " + std::to_string(MULTIPLICITY);

  return word; 

}

std::string PatternGenerator::Command::getTTYP(unsigned int dec){

  std::stringstream stream;
  stream << "0x" << std::setfill('0') << std::setw(2) << std::hex << dec;
  return stream.str();

}

void PatternGenerator::LeakyBucket::reset(){

  size=0;
  content=0;
  inv_rate=0;
  counter=0;
  nbusy=0;

}

// constructor
PatternGenerator::PatternGenerator(){

  // dummy config
  Configuration dummy_config;
  dummy_config.output_file="";
  dummy_config.rate=-99;
  dummy_config.seed=99;

  // initialization of class members
  m_config = dummy_config;
  
  m_nl1a_orbit=-99;
  m_nlines_orbit =-99.99;

  m_norbit_tot =-99;
  m_nl1a_tot=-99;
  m_nbcid_tot=-99;

  m_bcid_l1a_tot_vector.clear();
  m_bcid_l1a_act_vector.clear();

  m_nveto_bcr = -99; 
  m_nveto_calibreq = -99; 
  m_nbusy_smpl = -99;
  m_nbusy_sw = -99; 
  m_nbusy_cmplx = -99; 
  m_nbusy_tot = -99; 

  m_nl1a_orbit_act = -99;
  m_trig_rate_act = -99;

  m_is_sw = false;
}

// destructor
PatternGenerator::~PatternGenerator() {

}

// private method
void PatternGenerator::init(const Configuration& cfg){

  // load config
  m_config = cfg;

  // seed for random generator
  if(m_config.seed<=0) std::srand(std::time(nullptr)); // use current time as seed
  else        std::srand(m_config.seed);

  // reset leaky buckets if needed
  resetBuckets();

  // create output file
  m_output_file.open(m_config.output_file);

  // print
  std::cout << std::endl << "*****PatternGenerator configuration****" << std::endl;
  std::cout << "Output file:              " << m_config.output_file << std::endl;
  std::cout << "Random pattern rate:      " << m_config.rate << " kHz" << std::endl;

  // sw on/off
  m_is_sw = (m_config.sliding_window.first>0 && m_config.sliding_window.second>0); 

  // compute parameters
  computeParameters();

}

void PatternGenerator::generate(){

  std::cout << std::endl << "*****PatternGenerator: generation****" << std::endl;

  // write header
  writeHeader();

  // command
  Command cmd;
  cmd.reset();

  // trigger type
  int ttyp_dec = 129; // random trigger (BCM)

  // leaky buckets
  resetBuckets();

  // sliding window
  std::vector<int> sw_vector;
  sw_vector.clear();

  // global bc counter
  unsigned int bc_counter=0;

  // global l1a counter
  m_bcid_l1a_tot_vector.clear();
  m_bcid_l1a_act_vector.clear();

  // busy counter
  m_nveto_bcr = 0;
  m_nveto_calibreq = 0;
  m_nbusy_smpl = 0;
  m_nbusy_sw = 0;
  m_nbusy_cmplx = 0;
  m_nbusy_tot = 0;

  // signal counters
  int flag_l1a = 0;
  int flag_ttyp = 0;
  int flag_orb = 0;

  // loop over orbit 
  for(unsigned int orbit_current=1; orbit_current <= m_norbit_tot; ++orbit_current){
    // loop over bcid within orbit
    for(int bcid_current=1; bcid_current <= PatternGenerator::nbunch_orbit; bcid_current++){

      // update bc_counter
      ++bc_counter;

      // BGK conditions: BCRVeto
      bool BCRVeto = m_config.applyBCRVeto && (bcid_current > 3540 && bcid_current < 3561);
      if(BCRVeto) ++m_nveto_bcr;
      // BGK conditions: CalibReq
      bool CalibReq = m_config.applyCalibReq && (bcid_current > 3445 && bcid_current < 3516);
      if(CalibReq) ++m_nveto_calibreq;

      // new signal?
      bool end_orb  = (flag_orb==0 && cmd.ORB==1); 
      bool orb_sgn  = (bcid_current==1); 

      bool end_ttyp = (flag_ttyp==0 && cmd.TTYP!=cmd.getTTYP(0));

      bool end_l1a  = (flag_l1a==0 && cmd.L1A==1); 

      bool l1a_sgn = false;
      if(!BCRVeto && !CalibReq) l1a_sgn = drawL1ASignal();

      // BUSY signals and impact on l1a_sgn
      if(l1a_sgn) m_bcid_l1a_tot_vector.push_back(bc_counter);  // bef. deadtime

      bool isDeadtime = false;
      // simple deadtime?
      if(flag_ttyp>0){ 

        ++m_nbusy_smpl;
        l1a_sgn = false;
        isDeadtime = true; 
      }
      // complex deadtime?
      if(fullBuckets() || // call only once this (increment bucket busy counter) 
         (m_is_sw && sw_vector.size() == (unsigned int)m_config.sliding_window.first) ){

        ++m_nbusy_cmplx;
        l1a_sgn = false;
        isDeadtime = true; 

        // sliding window deadtime?
        if(m_is_sw && sw_vector.size() == (unsigned int)m_config.sliding_window.first) ++m_nbusy_sw;

      }

      if(isDeadtime) ++m_nbusy_tot;
      if(l1a_sgn) m_bcid_l1a_act_vector.push_back(bc_counter);  // aft. deadtime

      // flush cmd if change of state
      if(orb_sgn || end_orb || end_ttyp || l1a_sgn || end_l1a){

        fillCommand(cmd); 
        cmd.MULTIPLICITY=0;

      }        

      // orb
      if(orb_sgn){

        cmd.ORB = 1;
        flag_orb = PatternGenerator::nbunch_orbit_sgn;

      }
      else if(end_orb) cmd.ORB=0;

      // ttyp
      if(end_ttyp) cmd.TTYP=cmd.getTTYP(0);

      // l1a
      if(l1a_sgn){

        cmd.L1A = 1;
        flag_l1a = PatternGenerator::nbunch_l1a_sgn;

        // simple deadtime
        cmd.TTYP = cmd.getTTYP(ttyp_dec%256); // 256 <= 8-bit trigger word
        if(m_config.loopTTYP){ 
          ++ttyp_dec;
          if(ttyp_dec%256==0) ++ttyp_dec; // avoid 0x00 for L1A
        }

        flag_ttyp = PatternGenerator::nbunch_l1a_sgn + m_config.smpl_deadtime;

        // complex deadtime
        fillBuckets(); 

        // sliding window deadtime
        if(m_is_sw) sw_vector.push_back(m_config.sliding_window.second);

        if(m_bcid_l1a_act_vector.size()%5000==0) std::cout << m_bcid_l1a_act_vector.size() << " L1A generated" << std::endl;

      }
      else if(end_l1a) cmd.L1A=0;

      // update multiplcity
      cmd.MULTIPLICITY++;

      // update signal counters
      flag_orb--;
      flag_l1a--;
      flag_ttyp--;

      // update bucket content
      leakBuckets();

      // update sliding window
      if(m_is_sw){
        std::transform(std::begin(sw_vector),std::end(sw_vector),std::begin(sw_vector),[](int x){return x-1;});
        sw_vector.erase(std::remove(sw_vector.begin(), sw_vector.end(), 0), sw_vector.end());
      }
    } // end bcid loop 
  } // end orbit loop
  // fill last orbit
  fillCommand(cmd); 

  std::cout << "generation completed, " << m_bcid_l1a_act_vector.size() << " total L1A recorded." << std::endl;

}

void PatternGenerator::stat(){

  double m_nl1a_orbit_act = (double) m_bcid_l1a_act_vector.size() / m_norbit_tot;
  double m_sig_nl1a_orbit_act = (double) std::sqrt(m_bcid_l1a_act_vector.size()) / m_norbit_tot;

  double m_trig_rate_act = m_nl1a_orbit_act * PatternGenerator::lhc_rev_freq; 
  double m_sig_trig_rate_act = m_sig_nl1a_orbit_act * PatternGenerator::lhc_rev_freq; 

  double physics_deadtime = (double) m_bcid_l1a_act_vector.size() / m_bcid_l1a_tot_vector.size();

  // compute stuff related to e.g. deatime impact here
  std::cout << std::endl << "*****PatternGenerator: stat****" << std::endl;
  std::cout << "Nb of L1A generated in pattern (after deadtime): " << m_bcid_l1a_act_vector.size() << std::endl;
  std::cout << "simulated L1 trigger rate (aft. deadtime) [kHz]: " << m_trig_rate_act  << " +/- " << m_sig_trig_rate_act << std::endl;

  std::cout << "**" << std::endl;

  if(m_config.applyBCRVeto) std::cout << std::setprecision(3) << "BCRVeto fraction [%BC]:                          " << 100. * (double) m_nveto_bcr / m_nbcid_tot << std::endl;
  if(m_config.applyBCRVeto) std::cout << std::setprecision(3) << "CalibReq fraction [%BC]:                         " << 100. * (double) m_nveto_calibreq / m_nbcid_tot << std::endl;

  std::cout << "**" << std::endl;

  std::cout << std::setprecision(3) << "simple deadtime fraction [%BC]:                  " << 100. * (double) m_nbusy_smpl  / m_nbcid_tot << std::endl;
  std::cout << std::setprecision(3) << "complex deadtime fraction [%BC]:                 " << 100. * (double) m_nbusy_cmplx / m_nbcid_tot << std::endl;

  std::cout << "**" << std::endl;

  if(!m_config.bucket_vector.empty()){
    for (unsigned i_bucket=0; i_bucket<m_config.bucket_vector.size(); ++i_bucket){

      std::cout << std::setprecision(3) << "LeakyBucket " << std::to_string(i_bucket) << "[%BC]:                              " << 100. * (double) m_config.bucket_vector.at(i_bucket).nbusy / m_nbcid_tot << std::endl;

    }

  }  

  if(m_is_sw) std::cout << std::setprecision(3) << "sliding window deadtime fraction [%BC]:          " << 100. * (double) m_nbusy_sw    / m_nbcid_tot << std::endl;

  std::cout << "**" << std::endl;

  std::cout << std::setprecision(3) << "total   deadtime fraction  [%BC]:                " << 100. * (double) (m_nbusy_tot) / m_nbcid_tot << std::endl;
  std::cout << std::setprecision(3) << "physics deadtime fraction [%L1A]:                " << 100. * ( 1 - physics_deadtime ) << std::endl;

}

void PatternGenerator::exit(){

  m_output_file.close();

}

// private auxiliary methods
void PatternGenerator::computeParameters(){

  // compute class member values
  m_nl1a_orbit   = m_config.rate/PatternGenerator::lhc_rev_freq; // usually < 1 
  m_nlines_orbit = PatternGenerator::line_orbit_sgn + PatternGenerator::line_l1a_sgn*m_nl1a_orbit; // guess mem nlines per orbit, float 

  m_norbit_tot = std::floor(0.001*PatternGenerator::mem_size/m_nlines_orbit) -1; // max norbits for full mem 
  m_nbcid_tot = m_norbit_tot * PatternGenerator::nbunch_orbit - PatternGenerator::nbcid_for_mem_reset; // some nbcid needed for mem reset
  m_nl1a_tot   = std::ceil(m_norbit_tot*m_nl1a_orbit); // total l1a for full mem
 
  // inputs L1 rate is aft. BGK and bef. deadtime
  // 20 bunches per orbit are vetoed by BCRVeto
  // 70 bunches per orbit are vetoed by CalibReq
  unsigned int prob_norm_fact = PatternGenerator::nbunch_orbit;
  if(m_config.applyBCRVeto)  prob_norm_fact -= 20;
  if(m_config.applyCalibReq) prob_norm_fact -= 70;
  m_prob_l1a_bcid = (double)m_nl1a_tot / (m_norbit_tot*prob_norm_fact - PatternGenerator::nbcid_for_mem_reset); 

  std::cout << std::endl << "*****PatternGenerator parameters****" << std::endl;
  std::cout << "Nb of orbits in pattern (set by mem):        " << m_norbit_tot << std::endl;
  std::cout << "Nb of BCID in pattern (set by orbits):       " << m_nbcid_tot << std::endl;
  std::cout << "Nb of L1A expected in pattern (set by rate): " << m_nl1a_tot << std::endl;

}

// private auxiliary methods
void PatternGenerator::resetBuckets(){

  if(!m_config.bucket_vector.empty()){

    for (unsigned int i_bucket=0; i_bucket<m_config.bucket_vector.size(); ++i_bucket){

      m_config.bucket_vector.at(i_bucket).nbusy = 0;
      m_config.bucket_vector.at(i_bucket).counter = -1;

      // start empty
      m_config.bucket_vector.at(i_bucket).content = 0;

    }
  }

}

void PatternGenerator::fillBuckets(){

  if(!m_config.bucket_vector.empty()){

    for (unsigned int i_bucket=0; i_bucket<m_config.bucket_vector.size(); ++i_bucket){ 

      if(m_config.bucket_vector.at(i_bucket).content<m_config.bucket_vector.at(i_bucket).size){

        ++m_config.bucket_vector.at(i_bucket).content;

      }
      else{

        std::cout << "ERROR: try to fill full bucket! Exiting ..." << std::endl;
        exit();
      }

    }

  }

}

void PatternGenerator::leakBuckets(){

  if(!m_config.bucket_vector.empty()){

    for (unsigned i_bucket=0; i_bucket<m_config.bucket_vector.size(); ++i_bucket){

      // increase counter if non-empty, else reset counter
      if(m_config.bucket_vector.at(i_bucket).content>0) ++m_config.bucket_vector.at(i_bucket).counter;
      else                                              m_config.bucket_vector.at(i_bucket).counter = -1;

      // leak bucket (and reset counter) if it is time
      if(m_config.bucket_vector.at(i_bucket).counter > m_config.bucket_vector.at(i_bucket).inv_rate){

        if(m_config.bucket_vector.at(i_bucket).content>0) --m_config.bucket_vector.at(i_bucket).content;

        // reset counter
        m_config.bucket_vector.at(i_bucket).counter=0;

      }

    }

  }

}

bool PatternGenerator::fullBuckets(){

  bool flag = false;

  if(!m_config.bucket_vector.empty()){

    for (unsigned i_bucket=0; i_bucket<m_config.bucket_vector.size(); ++i_bucket){

      if(m_config.bucket_vector.at(i_bucket).content == m_config.bucket_vector.at(i_bucket).size){

        flag = true;
        ++m_config.bucket_vector.at(i_bucket).nbusy;

      }

    }

  }

  return flag;

}

void PatternGenerator::printBuckets(){

  if(!m_config.bucket_vector.empty()){

    for (unsigned i_bucket=0; i_bucket<m_config.bucket_vector.size(); ++i_bucket){

      m_output_file << "*leaky bucket " << std::to_string(i_bucket+1) << std::endl;
      m_output_file << "size [L1A]                : " << m_config.bucket_vector.at(i_bucket).size << std::endl;
      m_output_file << "content [L1A]             : " << m_config.bucket_vector.at(i_bucket).content << std::endl;
      m_output_file << "inverse leaking rate [BC] : " << m_config.bucket_vector.at(i_bucket).inv_rate << std::endl;
      m_output_file << "nBusy [BC]                : " << m_config.bucket_vector.at(i_bucket).nbusy << std::endl;

    }

  }

}

void PatternGenerator::writeHeader(){

  if(!m_output_file.is_open()) m_output_file.open(m_config.output_file);

  m_output_file << "#-----------------------" << std::endl; 
  m_output_file << "#                      M" << std::endl;
  m_output_file << "#                      u" << std::endl;
  m_output_file << "#                      l" << std::endl;
  m_output_file << "#                      t" << std::endl;
  m_output_file << "#                      i" << std::endl;
  m_output_file << "#                      p" << std::endl;
  m_output_file << "#                      l" << std::endl;
  m_output_file << "#   CCC                i" << std::endl;
  m_output_file << "#  BRRR    T BBBB TTT  c" << std::endl;
  m_output_file << "#O UEEE    T GGGG TTTL i" << std::endl;
  m_output_file << "#R SQQQ    Y OOOO RRR1 t" << std::endl;
  m_output_file << "#B Y210    P 3210 321A y" << std::endl;
  m_output_file << "#-----------------------" << std::endl;

}

bool PatternGenerator::drawL1ASignal(){

  double rnum = ((double)rand()/RAND_MAX);
  if(rnum <= m_prob_l1a_bcid) return true;
  else                        return false;

}

void PatternGenerator::fillCommand(Command& cmd){

  // check something to write
  if(cmd.MULTIPLICITY<=0) return;

  // check file open
  if(!m_output_file.is_open()) m_output_file.open(m_config.output_file); 

  m_output_file << cmd.getWord() << std::endl;

  // uncomment for debugging
  //printBuckets();

}

