//******************************************************************************
// file: TestAltiInitial.cc desc: test class for ALTI initialization
// auth: 09-MAR-2018
// P. Kuzmanovic *****************************************************************************

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <getopt.h>
#include <sys/time.h>
#include "unistd.h"

#include "RCDUtilities/RCDUtilities.h"
#include "ALTI/TestAltiInitial.h"
#include "ALTI/AltiConfiguration.h"
//#include "CTRP/CTRP.h"

using namespace LVL1;

//------------------------------------------------------------------------------

TestAltiInitial::Configuration::Configuration() {

    // default values
    module_reset                = false;
    module_setup                = false;
    module_check                = false;
    module_config               = false;
    config_file                 = "";
    module_rconf                = false;
    //////////////////////////////////////////////////
    rconf_file                  = "";
    module_slot                 = 13;
    module_base                 = 0x10000000;
    module_setbase              = false;
    //////////////////////////////////////////////////
}

//------------------------------------------------------------------------------

TestAltiInitial::Configuration::~Configuration() {

    // nothing to do
}

//------------------------------------------------------------------------------

void TestAltiInitial::Configuration::config(int argc, char* argv[]) {

    int c;

    // read command-line options
    while ((c = getopt(argc, argv, "s:b:BcCf:o:rRSh")) != -1) {
        switch (c) {
        case 's':
            module_slot = atoi(optarg);
            break;

        case 'b':
            module_base = strtol(optarg,(char **)0,16);
            break;

        case 'B':
            module_setbase = true;
            break;

        case 'c':
            module_check = true;
            break;

        case 'C':
            module_config = true;
            break;

        case 'f':
            config_file = optarg;
            break;

        case 'o':
            rconf_file = optarg;
            break;

        case 'r':
            module_rconf = true;
            break;

        case 'R':
            module_reset = true;
            break;

        case 'S':
            module_setup = true;
            break;

        case 'h':
            std::cout << "--------------------------------------------------------------------------------" << std::endl;
            std::cout << "testAltiInitial <OPTIONS>:" << std::endl;
            std::cout << "--------------------------------------------------------------------------------" << std::endl;
            std::printf(" -s        => ALTI slot number             (def = %02d)\n", module_slot);
            std::cout << "--------------------------------------------------------------------------------" << std::endl;
            std::printf(" -R        => Reset  ALTI                  (def = \"%s\")\n", module_reset ? "YES" : "NO");
            std::printf(" -S        => Setup  ALTI                  (def = \"%s\")\n", module_setup ? "YES" : "NO");
            std::printf(" -C        => Config ALTI                  (def = \"%s\")\n", module_config ? "YES" : "NO");
            std::printf(" -f <file> => Config ALTI file             (def = %s)\n", config_file.empty() ? "\"NO\" => default config" : ("\"" + config_file + "\"").c_str());
            std::printf(" -B        => Change ALTI VME base address (def = \"%s\")\n", module_setbase ? "YES" : "NO");
            std::printf(" -b        => ALTI VME base address        (def = 0x%08x)\n", module_base);
            std::cout << "--------------------------------------------------------------------------------" << std::endl;
            std::printf(" -c        => Check  ALTI                  (def = \"%s\")\n", module_check ? "YES" : "NO");
            std::printf(" -r        => Read   ALTI config           (def = \"%s\")\n", module_rconf ? "YES" : "NO");
            std::printf(" -o <file> => Read   ALTI config file      (def = %s)\n", rconf_file.empty() ? "\"NO\" => terminal printout" : ("\"" + rconf_file + "\"").c_str());
            std::cout << "--------------------------------------------------------------------------------" << std::endl;
            std::exit(0);
            break;

        default:
            break;
        }
    }
}

//------------------------------------------------------------------------------

void TestAltiInitial::Configuration::print() const {

    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::cout << "testAltiInitial:" << std::endl;
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::printf("ALTI slot number             = %02d\n", module_slot);
    std::printf("ALTI VME base address        = 0x%08x\n", module_base);
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::printf("Reset  ALTI                  = \"%s\"\n", module_reset ? "YES" : "NO");
    std::printf("Setup  ALTI                  = \"%s\"\n", module_setup ? "YES" : "NO");
    if (module_config) {
        std::printf("Config ALTI                  = \"YES\", %s\n", config_file.empty() ? "default config" : ("\"" + config_file + "\"").c_str());
    }
    else {
        std::printf("Config ALTI                  = \"NO\"\n");
    }
    if (module_setbase) {
        std::printf("Change ALTI VME base address = \"YES\", 0x%08x\n", module_base);
    }
    else {
        std::printf("Change ALTI VME base address = \"NO\"\n");
    }
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::printf("Check  ALTI                  = \"%s\"\n", module_check ? "YES" : "NO");
    if (module_rconf) {
        std::printf("Read   ALTI config           = \"YES\", %s\n", rconf_file.empty() ? "terminal printout" : ("\"" + rconf_file + "\"").c_str());
    }
    else {
        std::printf("Read   ALTI config           = \"NO\"\n");
    }
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
}

//------------------------------------------------------------------------------

int TestAltiInitial::Configuration::check() const {

    int fail = 0;

    // slot number
    if (((unsigned int) module_slot < AltiModule::ALTI_SLOT_MIN) || ((unsigned int) module_slot > AltiModule::ALTI_SLOT_MAX)) {
        CERR("slot number (%d) not in allowed range [%d..%d]", module_slot, AltiModule::ALTI_SLOT_MAX, AltiModule::ALTI_SLOT_MAX);
        fail++;
    }

    return(fail);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

TestAltiInitial::TestAltiInitial() : m_result(daq::tmgr::TmUnresolved) {

    // nothing to be done
}

//------------------------------------------------------------------------------

TestAltiInitial::~TestAltiInitial() {
  // delete AltiModule
  if (m_alti) delete m_alti;
}

//------------------------------------------------------------------------------

daq::tmgr::TestResult TestAltiInitial::init() {

    // check and dump parameters
    if (m_cfg.check() != 0) {
        return(daq::tmgr::TmUnresolved);
    }
    COUT("","");
    m_cfg.print();

    // change ALTI VME base address if necessary
    if(m_cfg.module_setbase) {
        if(AltiModule::setBaseAddress(m_cfg.module_slot, m_cfg.module_base)) {
            CERR("setting up ALTI VME base address","");
            printResult(__FUNCTION__,daq::tmgr::TmFail);
            return(daq::tmgr::TmFail);
        }
    }

    // create ALTI
    m_alti = new AltiModule(m_cfg.module_slot);
    ////////////////////
    if((*m_alti)() != AltiModule::SUCCESS) {
        CERR("opening ALTI","");
        return(daq::tmgr::TmFail);
    }

    // print result
    printResult(__FUNCTION__,daq::tmgr::TmPass);
    return(daq::tmgr::TmPass);
}

//------------------------------------------------------------------------------

daq::tmgr::TestResult TestAltiInitial::exec() {
  
  // configure everything on primary FXO clock
  if (m_alti->CLKInputSelectWriteJitterCleaner(AltiModule::CLK_JC_TYPE::OSCILLATOR)) { 
    CERR("seting the Jitter Cleaner from the internal ALTI clock","");
    printResult(__FUNCTION__,daq::tmgr::TmFail);
    return(daq::tmgr::TmFail);
  }
  if (m_alti->CLKInputSelectWritePLL(AltiModule::CLK_PLL_TYPE::JITTER_CLEANER)) { 
    CERR("seting the PLL from the Jitter Cleaner","");
    printResult(__FUNCTION__,daq::tmgr::TmFail);
    return(daq::tmgr::TmFail);
  }

  // wait for PLL to lock
  usleep(1);
  
  // reset ALTI if necessary
  if(m_cfg.module_reset) {
        if(m_alti->AltiReset()) {
            CERR("resetting ALTI","");
            printResult(__FUNCTION__,daq::tmgr::TmFail);
            return(daq::tmgr::TmFail);
        }
    }

    // setup ALTI if necessary
    if(m_cfg.module_setup) {
        if(m_alti->AltiSetup()) {
            CERR("setting up  ALTI","");
            printResult(__FUNCTION__,daq::tmgr::TmFail);
            return(daq::tmgr::TmFail);
        }
    }

    // config ALTI if necessary
    if(m_cfg.module_config) {
      AltiConfiguration cfg;
      if(!m_cfg.config_file.empty()) {
	if(cfg.read(m_cfg.config_file) != AltiModule::SUCCESS) {
	  CERR("reading ALTI config from file \"%s\"",m_cfg.config_file.c_str());
	  return(daq::tmgr::TmUnresolved);
	}
      }
      
      // setup the ALTI board if not already done 
      if(!m_cfg.module_setup) {
	if(m_alti->AltiSetup()) {
	  CERR("setting up  ALTI","");
	  printResult(__FUNCTION__,daq::tmgr::TmFail);
	  return(daq::tmgr::TmFail);
	}
      }

      if(m_alti->AltiConfigWrite(cfg)) {
	CERR("writing ALTI config","");
	printResult(__FUNCTION__,daq::tmgr::TmFail);
	return(daq::tmgr::TmFail);
      }
    }
      sleep(1);
    // check ALTI if necessary
    if(m_cfg.module_check) {
        if(m_alti->AltiCheck()) {
            CERR("checking ALTI","");
            printResult(__FUNCTION__,daq::tmgr::TmFail);
            return(daq::tmgr::TmFail);
        }
    }

  
    // read ALTI configuration if necessary
    if(m_cfg.module_rconf) {
        AltiConfiguration cfg; cfg.clearFileNames();
        if(m_alti->AltiConfigRead(cfg) != AltiModule::SUCCESS) {
            CERR("reading ALTI config","");
            return(daq::tmgr::TmFail);
        }
        if (m_cfg.rconf_file.empty()) {
            cfg.print();
        }
        else {
            int rtnv(0);
            if((rtnv = cfg.write(m_cfg.rconf_file)) != 0) {
                CERR("reading ALTI config into file \"%s\"",m_cfg.rconf_file.c_str());
                return(daq::tmgr::TmFail);
            }
        }
    }
  
    // print result
    printResult(__FUNCTION__,daq::tmgr::TmPass);
    return(daq::tmgr::TmPass);
}

//------------------------------------------------------------------------------

daq::tmgr::TestResult TestAltiInitial::exit() {

  // delete AltiModule
  if (m_alti) delete m_alti;
  return(daq::tmgr::TmPass);
}

//------------------------------------------------------------------------------

daq::tmgr::TestResult TestAltiInitial::test(const Configuration& cfg) {

    daq::tmgr::TestResult rslt = daq::tmgr::TmPass;

    // set configuration
    m_cfg = cfg;

    // initialize TestAltiInitial
    if (init() != daq::tmgr::TmPass) {
        printResult(__FUNCTION__,daq::tmgr::TmFail);
        return(daq::tmgr::TmFail);
    }

    // execute TestAltiInitial
    if (exec() != daq::tmgr::TmPass) {
        printResult(__FUNCTION__,daq::tmgr::TmFail);
        rslt = daq::tmgr::TmFail;
    }

    // exit TestAltiInitial
    if (exit() != daq::tmgr::TmPass) {
        printResult(__FUNCTION__,daq::tmgr::TmFail);
        rslt = daq::tmgr::TmFail;
    }

    if (rslt != daq::tmgr::TmPass) {
        printResult(__FUNCTION__,daq::tmgr::TmFail);
        return(daq::tmgr::TmFail);
    }

    printResult(__FUNCTION__,daq::tmgr::TmPass);
    return(daq::tmgr::TmPass);
}

//------------------------------------------------------------------------------

void TestAltiInitial::printResult(const std::string& test, daq::tmgr::TestResult rslt) {

    std::ostringstream  buf;
    int                 len;
    int                 i;

    buf << ">> TestAltiInitial::" << test << " ";
    len = buf.str().size();

    m_result = rslt;
    if (m_result == daq::tmgr::TmPass) {
        for (i = 0; i < (RESULT_LENGTH - 7 - len); i++) buf << ".";
        buf << " [PASS]";
    }
    else if (m_result == daq::tmgr::TmFail) {
        for (i = 0; i < (RESULT_LENGTH - 7 - len); i++) buf << ".";
        buf << " [FAIL]";
    }
    else if (m_result == daq::tmgr::TmUnresolved) {
        for (i = 0; i < (RESULT_LENGTH - 13 - len); i++) buf << ".";
        buf << " [UNRESOLVED]";
    }
    else {
        for (i = 0; i < (RESULT_LENGTH - 10 - len); i++) buf << ".";
        buf << " [UNKNOWN]";
    }

    std::cout << buf.str() << std::endl;
    buf.str(""); for (i = 0; i < RESULT_LENGTH; i++) buf << "-";
    std::cout << buf.str() << std::endl;
}
