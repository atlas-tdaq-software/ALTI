//*****************************************************************************
// file: EventIdentifier.cc
// desc: class for Level1 event identifier
// auth: 10-AUG-2009 R. Spiwoks, taken on 15-OCT-2018 by P. Kuzmanovic
//*****************************************************************************

// $Id$

#include <iostream>
#include <iomanip>

#include "RCDUtilities/RCDUtilities.h"
#include "ALTI/EventIdentifier.h"

using namespace LVL1;

//------------------------------------------------------------------------------

const u_int EventIdentifier::L1ID_MAX = EventIdentifier::L1ID_MASK;
const u_int EventIdentifier::ECRC_MAX = EventIdentifier::ECRC_MASK >> EventIdentifier::ECRC_SHFT;

//------------------------------------------------------------------------------

EventIdentifier::EventIdentifier(const uint32_t data) {

    // create data
    m_data = new uint32_t(data);
    m_data_flag = true;
}

//------------------------------------------------------------------------------

EventIdentifier::EventIdentifier(const EventIdentifier& eid) {

    // copy-construct data
    if(eid.m_data_flag) {
        m_data = new uint32_t;
        m_data_flag = true;
        *m_data = *(eid.m_data);
    }
    else {
        m_data = eid.m_data;
        m_data_flag = false;
    }
}

//------------------------------------------------------------------------------

EventIdentifier::~EventIdentifier() {

    // delete data if necessary
    if(m_data_flag) delete m_data;
}

//------------------------------------------------------------------------------

EventIdentifier& EventIdentifier::operator=(const EventIdentifier& eid) {

    // return if same object
    if(this == &eid) return(*this);

    // copy data
    if(m_data_flag) {
        if(eid.m_data_flag) {
            *m_data = *(eid.m_data);
        }
        else {
            delete m_data;
            m_data = eid.m_data;
            m_data_flag = false;
        }
    }
    else {
        if(eid.m_data_flag) {
            m_data = new uint32_t;
            m_data_flag = true;
            *m_data = *(eid.m_data);
        }
        else {
            m_data = eid.m_data;
        }
    }

    return(*this);
}

//------------------------------------------------------------------------------

EventIdentifier& EventIdentifier::operator=(const uint32_t data) {

    // set data
    *m_data = data;

    return(*this);
}

//------------------------------------------------------------------------------

EventIdentifier::operator uint32_t() const {

    // get data
    return(*m_data);
}

//------------------------------------------------------------------------------

void EventIdentifier::addr(uint32_t* addr) {

    // change data to external
    if(m_data_flag) delete m_data;
    m_data = addr;
    m_data_flag = false;
}

//------------------------------------------------------------------------------

uint32_t EventIdentifier::ECRC() const {

    return(((*m_data) & ECRC_MASK) >> ECRC_SHFT);
}

//------------------------------------------------------------------------------

void EventIdentifier::ECRC(const uint32_t ecrc) {

    *m_data = ((*m_data) & (~ECRC_MASK)) | ((ecrc << ECRC_SHFT) & ECRC_MASK);
}

//------------------------------------------------------------------------------

uint32_t EventIdentifier::L1ID() const {

    return((*m_data) & L1ID_MASK);
}

//------------------------------------------------------------------------------

void EventIdentifier::L1ID(const uint32_t l1id) {

    *m_data = ((*m_data) & (~L1ID_MASK)) | (l1id & L1ID_MASK);
}

//------------------------------------------------------------------------------

int EventIdentifier::read(std::istream& s) {

    std::string buf;

    while(true) {

        // read from input stream
        if(!(s >> buf)) {
            if(!s.eof()) CERR("reading Level1 event identifier from input stream","");
            return(ERROR_FILE);
        }

        // read comment if necessary
        if(buf[0] == '#') {
            if(!getline(s,buf)) {
                CERR("reading comment before Level1 event identifier from input stream","");
                return(ERROR_FILE);
            }
            continue;
        }

        // scan input
        if(sscanf(buf.c_str(),"%x",m_data) != 1) {
            CERR("scanning Level1 event identifier from input stream","");
            return(ERROR_WRONGSCAN);
        }
        break;
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int EventIdentifier::write(std::ostream& s) {

    // set format
    s << std::hex << std::setfill('0');

    // write word
    if(!(s << "0x" << std::setw(8) << (*m_data) << std::endl)) {
        CERR("writing Level1 event identifier to output stream","");
        return(ERROR_FILE);
    }

    // reset format
    s << std::setfill(' ') << std::dec;

    return(0);
}

//------------------------------------------------------------------------------

void EventIdentifier::dump(std::ostream& s) const {

    char os[EventIdentifier::STRING_LENGTH];

    std::sprintf(os,"0x%02x (%3d) | 0x%06x (%8d)n",ECRC(),ECRC(),L1ID(),L1ID());
    s << os;
}

//------------------------------------------------------------------------------

void EventIdentifier::dumpLine(std::ostream& s) const {

    char os[EventIdentifier::STRING_LENGTH];

    std::sprintf(os,"ECRC = 0x%02x (%3d), L1ID = 0x%06x (%8d)",ECRC(),ECRC(),L1ID(),L1ID());
    s << os;
}

//------------------------------------------------------------------------------

void EventIdentifier::dumpHeader(std::ostream& s) {

    s << "   ECRC   |     L1ID" << std::endl;
}

