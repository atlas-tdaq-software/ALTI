//******************************************************************************
// file: AltiBusyCounter.cc
// desc: clAss for CTPCOREPLUS busy counter
// auth: 10-NOV-2014 R. Spiwoks, from MirodBusyCounter.cc
//******************************************************************************

#include <iostream>
#include <sstream>
#include <limits>

#include "RCDUtilities/RCDUtilities.h" //?
#include "ALTI/AltiCommon.h"
#include "ALTI/AltiBusyCounter.h"

using namespace LVL1;

//------------------------------------------------------------------------------

const std::string AltiBusyCounter::BUSY_NAME[BUSY_NUMBER] = {
    "CTP", "ALTI", "FP", "LOCAL", "PG", "VME", "ECR", "INTERVAL"
};

//------------------------------------------------------------------------------

AltiBusyCounter::AltiBusyCounter() : m_cseg(0) {

    // create data array
    m_data = new uint32_t[SIZE];

    // reset control
    resetCTRL();

    // reset data
    resetDATA();
}

//------------------------------------------------------------------------------

AltiBusyCounter::AltiBusyCounter(RCD::CMEMSegment* s) : m_cseg(s) {

    m_data = (uint32_t*)m_cseg->VirtualAddress();

    // reset control
    resetCTRL();

    // reset data
    resetDATA();
}

//------------------------------------------------------------------------------

AltiBusyCounter::AltiBusyCounter(const AltiBusyCounter& cnt) {

    // call assignment operator
    *this = cnt;
}

//------------------------------------------------------------------------------

AltiBusyCounter::~AltiBusyCounter() {

    // delete data if necessary
    if(!m_cseg) delete m_data;
}

//------------------------------------------------------------------------------

AltiBusyCounter& AltiBusyCounter::operator=(const AltiBusyCounter& cnt) {

    unsigned int i;

    if(this != &cnt) {

        if(m_cseg) {
            if(cnt.m_cseg) {
                m_cseg = cnt.m_cseg;
                m_data = cnt.m_data;
            }
            else {
                m_cseg = 0;
                m_data = new uint32_t[SIZE];
                for(i=0; i<size(); i++) m_data[i] = cnt.m_data[i];
            }
        }
        else {
            if(cnt.m_cseg) {
                delete [] m_data;
                m_cseg = cnt.m_cseg;
                m_data = cnt.m_data;
            }
            else {
                for(i=0; i<size(); i++) m_data[i] = cnt.m_data[i];
            }
        }
        m_wmod          = cnt.m_wmod;
        m_rmod          = cnt.m_rmod;
        for(unsigned int b=0; b<BUSY_NUMBER; b++) m_select[b] = cnt.m_select[b];
        m_interval      = cnt.m_interval;
        m_values        = cnt.m_values;
        m_overflow      = cnt.m_overflow;
        m_status        = cnt.m_status;
    }

    return(*this);
}

//------------------------------------------------------------------------------

void AltiBusyCounter::segment(RCD::CMEMSegment* seg) {

    if(!m_cseg) delete [] m_data;

    m_cseg = seg;
    m_data = (uint32_t*)m_cseg->VirtualAddress();
}

//------------------------------------------------------------------------------

unsigned int AltiBusyCounter::selectSize() const {

    unsigned int n = 0;

    // count active selects
    for(unsigned int b=0; b<BUSY_NUMBER; b++) if(m_select[b]) n++;

    return(n);
}

//------------------------------------------------------------------------------

void AltiBusyCounter::resetCTRL() {

    m_wmod = false;
    m_rmod = true;
    for(unsigned int b=0; b<BUSY_NUMBER; b++) m_select[b] = false;
    m_interval = 0;
    m_overflow = false;
    m_status = false;
}

//------------------------------------------------------------------------------

void AltiBusyCounter::resetDATA() {

    m_values = 0;
}

//------------------------------------------------------------------------------

void AltiBusyCounter::setStatus(const unsigned int data) {

    unsigned int b, msk;

    msk = 0x00000001;
    for(b=0; b<(BUSY_NUMBER-1); b++, msk=msk<<1) {
        m_select[b] = true;
        m_data[b] = ((data&msk) == msk);
    }
    m_select[b] = false;
    m_data[b] = 0;
    m_interval = 1;
    m_values = 1;
    m_status = true;
}

//------------------------------------------------------------------------------

void AltiBusyCounter::dump() const {

    unsigned int b, i, s;
    unsigned int msk = selectSize();
    static const int LINE = 104;

    // status
    if(m_status) {
        dumpStatus();
        return;
    }

    // header
    std::cout << AltiCommon::rightFillString("",LINE,'=') << std::endl;
    std::printf("ALTI BUSY counter - type = %s, W/%s, R/%s ovfl = %s, values = %d\n",m_cseg?"CMEM":"DATA",m_wmod?"AUTO":"MANU",m_rmod?"FIRST,":" LAST, ",m_overflow?"OVFL":"  no",m_values);
    std::cout << AltiCommon::rightFillString("",LINE,'-') << std::endl;
    if((!msk) || (!m_values)) {
        std::printf(">>>> EMPTY COUNTER!!!!\n");
        std::cout << AltiCommon::rightFillString("",LINE,'-') << std::endl;
        return;
    }

    // header row
    std::printf(" NUM:");
    for(b=0; b<BUSY_NUMBER; b++) {
        if(m_select[b]) std::printf("  %9s",BUSY_NAME[b].c_str());
    }
    std::cout << std::endl;

    // data row(s)
    for(s=0,i=0; s<m_values; s++) {
        std::printf("%4d:",s);
        for(b=0; b<msk; b++,i++) {
            std::printf("   0x%06x",m_data[i]&BSY_CNT_MASK);
        }
        std::cout << std::endl;
    }
}

//------------------------------------------------------------------------------

void AltiBusyCounter::selectDump() const {

    unsigned int b;
    std::ostringstream buf;

    for(b=0; b<BUSY_NUMBER; b++) {
        buf.str(""); buf << "BSY[\"" << BUSY_NAME[b] << "\"]";
        std::printf("%-16s: %s\n",buf.str().c_str(),m_select[b]?" \"ON\"":"\"off\"");
    }
}

//------------------------------------------------------------------------------

std::string AltiBusyCounter::selectPrint() const {

    std::ostringstream buf;
    bool space = false;
    bool aflag = true;
    unsigned int b;

    for(b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) {
        if(m_select[b]) {
            if(space) buf << " | ";
            buf << BUSY_NAME[b];
            space = true;
        }
        else {
            aflag = false;
        }
    }
    if(aflag) {
        buf.str(""); buf << "ALL";
    }
    if(!space) buf << "NONE";

    return(buf.str());
}

//------------------------------------------------------------------------------

void AltiBusyCounter::dumpStatus() const {

    unsigned int b;
    static const int LINE = 93;

    // header
    std::cout << AltiCommon::rightFillString("",LINE,'=') << std::endl;
    std::printf("ALTI BUSY status:\n");
    std::cout << AltiCommon::rightFillString("",LINE,'-') << std::endl;

    // header row
    for(b=0; b<BUSY_NUMBER; b++) {
        if(m_select[b]) {
            if(b!=0) std::cout << " | ";
            std::printf("%9s",BUSY_NAME[b].c_str());
         }
    }
    std::cout << std::endl;

    // data row
    for(b=0; b<selectSize(); b++) {
        std::cout << ((b==0) ? " " : "    |  ");
        std::printf("  %s",(m_data[b]&BSY_CNT_MASK)?" ON":"off");
    }
    std::cout << std::endl;
}

//------------------------------------------------------------------------------

bool AltiBusyCounter::check() const {

    // check BSY select
    if(selectSize() == 0) {
        CERR("NO BSY select","");
        return(false);
    }

    // check MANUAL and INTERVAL
    if(!autoMode() && !select(INTERVAL)) {
        CERR("MANUAL mode and INTERVAL NOT selected","");
        return(false);
    }

    return(true);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

const std::string AltiBusyCounterRate::CTRL_NAME[CTRL_NUMBER] = {
    "UNKNOWN", "DISABLED", "ENABLED"
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

AltiBusyCounterRate::AltiBusyCounterRate() {

    // create data arrays
    m_data = new double[SIZE];
    m_dmin = new double[AltiBusyCounter::BUSY_NUMBER];
    m_dmax = new double[AltiBusyCounter::BUSY_NUMBER];

    // reset
    reset();
}

//------------------------------------------------------------------------------

AltiBusyCounterRate::AltiBusyCounterRate(const AltiBusyCounter& cnt) {

    // create data arrays
    m_data = new double[SIZE];
    m_dmin = new double[AltiBusyCounter::BUSY_NUMBER];
    m_dmax = new double[AltiBusyCounter::BUSY_NUMBER];

    // reset
    reset();

    // call assignment operator
    *this = cnt;
}

//------------------------------------------------------------------------------

AltiBusyCounterRate::~AltiBusyCounterRate() {

    // delete data arrays
    delete [] m_dmax;
    delete [] m_dmin;
    delete [] m_data;
}

//------------------------------------------------------------------------------

AltiBusyCounterRate& AltiBusyCounterRate::operator=(const AltiBusyCounter& cnt) {

    unsigned int msk(0), s, m, i;

    // mode and select
    m_wmod = cnt.autoMode();
    m_rmod = cnt.firstMode();
    for(unsigned int b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) {
        m_select[b] = cnt.select(static_cast<AltiBusyCounter::BUSY>(b));
        if(m_select[b]) msk++;
    }
    m_values = cnt.values();

    // interval counter selected
    if(m_select[AltiBusyCounter::INTERVAL]) {
        double itv;
        for(s=0, i=0; s<m_values; s++) {
            itv = static_cast<double>(cnt[(s+1)*msk-1]);
            for(m=0; m<(msk-1); m++, i++) {
                m_data[i] = (itv) ? static_cast<double>(cnt[i]) / itv * 100.0 : 0.0;
                CDBG("s = %d, i = %d, m = %d => data = %7.3f%%, MIN = %7.3f%%, MAX = %7.3f%%",s,i,m,m_data[i],m_dmin[m],m_dmax[m]);
                if(m_data[i] < m_dmin[m]) m_dmin[m] = m_data[i];
                if(m_data[i] > m_dmax[m]) m_dmax[m] = m_data[i];
            }
            m_data[i] = itv / AltiCommon::BC_FREQUENCY;
            CDBG("s = %d, i = %d, m = %d => data = %7.3f%%, MIN = %7.3f%%, MAX = %7.3f%%",s,i,m,m_data[i],m_dmin[m],m_dmax[m]);
            if(m_data[i] < m_dmin[msk-1]) m_dmin[msk-1] = m_data[i];
            if(m_data[i] > m_dmax[msk-1]) m_dmax[msk-1] = m_data[i];
            i++;
        }
    }

    // interval counter NOT selected
    else {
        m_interval = static_cast<double>(cnt.interval());
        if(m_interval <= 0.0) {
            CERR("INTERVAL counter NOT selected and interval value NOT set!!!!","");
            reset();
            return(*this);
        }
        for(s=0, i=0; s<m_values; s++) {
            for(m=0; m<msk; m++, i++) {
                m_data[i] = static_cast<double>(cnt[i]) / m_interval * 100.0;
                CDBG("s = %d, i = %d, m = %d => data = %7.3f%%, MIN = %7.3f%%, MAX = %7.3f%%",s,i,m,m_data[i],m_dmin[m],m_dmax[m]);
                if(m_data[i] < m_dmin[m]) m_dmin[m] = m_data[i];
                if(m_data[i] > m_dmax[m]) m_dmax[m] = m_data[i];
            }
        }
        m_interval = m_interval / AltiCommon::BC_FREQUENCY;
    }

    // reset some flags
    m_overflow = cnt.overflow();
    m_average = false;

    return(*this);
}

//------------------------------------------------------------------------------

AltiBusyCounterRate& AltiBusyCounterRate::operator=(const AltiBusyCounterRate& rhs) {

    unsigned int msk(0), s, m, i;

    if(this != &rhs) {
        m_wmod = rhs.m_wmod;
        m_rmod = rhs.m_rmod;
        for(unsigned int b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) {
            m_select[b] = rhs.m_select[b];
            if(m_select[b]) msk++;
        }

        for(s=0, i=0; s<rhs.m_values; s++) {
            for(m=0; m<msk; m++, i++) {
                m_data[i] = rhs.m_data[i];
            }
        }
        for(m=0; m<msk; m++) {
            m_dmin[m] = rhs.m_dmin[m];
            m_dmax[m] = rhs.m_dmax[m];
        }

        m_interval = rhs.m_interval;
        m_values = rhs.m_values;
        m_overflow = rhs.m_overflow;
        m_average = rhs.m_average;
    }

    return(*this);
}

//------------------------------------------------------------------------------

AltiBusyCounterRate& AltiBusyCounterRate::operator+(const AltiBusyCounter& cnt) {

    AltiBusyCounterRate rte(cnt);
#ifdef DEBUG
    rte.dump();
#endif  // DEBUG

    return((*this) + rte);
}

//------------------------------------------------------------------------------

AltiBusyCounterRate& AltiBusyCounterRate::operator+(const AltiBusyCounterRate& rte) {

    this->average();
    this->average(rte);

    return(*this);
}

//------------------------------------------------------------------------------

void AltiBusyCounterRate::reset() {

    m_wmod = false;
    m_rmod = true;
    for(unsigned int b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) m_select[b] = false;
    m_interval = 0.0;
    m_values = 0;

    for(unsigned int i=0; i<SIZE; i++) m_data[i] = 0.0;
    for(unsigned int i=0; i<AltiBusyCounter::BUSY_NUMBER; i++) {
        m_dmin[i] = std::numeric_limits<double>::max();
        m_dmax[i] = -1.0;
    }

    m_overflow = false;
    m_average = false;
}

//------------------------------------------------------------------------------

void AltiBusyCounterRate::scale(double scl) {

    unsigned int i;

    for(i=0; i<SIZE; i++) m_data[i] = m_data[i]*scl;
}

//------------------------------------------------------------------------------

void AltiBusyCounterRate::normalize() {

    unsigned int i;

    if(m_values) {
        for(i=0; i<SIZE; i++) m_data[i] = m_data[i]/static_cast<double>(m_values);
    }
}

//------------------------------------------------------------------------------

void AltiBusyCounterRate::average() {

    unsigned int msk, m, s;
    double bsy, itv;

    // return if nothing to be done
    if(m_average || (m_values == 0)) return;

    msk = selectSize();
    CDBG("msk = %d",msk);

    // interval counter selected <---------------------------- INTERVAL?
    if(m_select[AltiBusyCounter::INTERVAL]) {
        CDBG("INTERVAL selected","");
        itv = 0.0;
        for(s=0; s<m_values; s++) {
            double dat = m_data[(s+1)*msk-1];
            CDBG("s = %d, i = %d, m = %d => data = %7.3f%%, MIN = %7.3f%%, MAX = %7.3f%%",s,(s+1)*msk-1,msk-1,dat,m_dmin[msk-1],m_dmax[msk-1]);
            itv += dat;
            if(dat < m_dmin[msk-1]) m_dmin[msk-1] = dat;
            if(dat > m_dmax[msk-1]) m_dmax[msk-1] = dat;
        }
        CDBG("itv = %12.3f sec",itv);
        for(m=0; m<(msk-1); m++) {
            bsy = 0.0;
            for(s=0; s<m_values; s++) {
                double dat = m_data[s*msk+m];
                CDBG("s = %d, i = %d, m = %d => data = %7.3f%%, MIN = %7.3f%%, MAX = %7.3f%%",s,s*msk+m,m,dat,m_dmin[m],m_dmax[m]);
                bsy += dat * m_data[(s+1)*msk-1];
                if(dat < m_dmin[m]) m_dmin[m] = dat;
                if(dat > m_dmax[m]) m_dmax[m] = dat;
            }
            m_data[m] = bsy / itv;
            CDBG("m = %d, bsy = %20.3f%% => rte = %9.5f%%",m,bsy,m_data[m]);
        }
        m_interval = itv;
    }

    // interval counter NOT selected
    else {
        CDBG("INTERVAL not selected","");
        itv = static_cast<double>(m_values) * static_cast<double>(m_interval);
        CDBG("itv = %12.3f sec",itv);
        for(m=0; m<msk; m++) {
            bsy = 0.0;
            for(s=0; s<m_values; s++) {
                double dat = m_data[s*msk+m];
                CDBG("s = %d, i = %d, m = %d => data = %7.3f%%, MIN = %7.3f%%, MAX = %7.3f%%",s,s*msk+m,m,dat,m_dmin[m],m_dmax[m]);
                bsy += dat;
                if(dat < m_dmin[m]) m_dmin[m] = dat;
                if(dat > m_dmax[m]) m_dmax[m] = dat;
            }
            m_data[m] = bsy / m_values;
            CDBG("m = %d, bsy = %20.3f%% => rte = %9.5f%%",m,bsy,m_data[m]);
        }
        m_interval = itv;
    }

    // mark average
    m_average = true;
}

//------------------------------------------------------------------------------

void AltiBusyCounterRate::average(const AltiBusyCounterRate& rte) {

    unsigned int msk, m, s;
    double bsy, itv;

    if(m_values == 0) {

        // mode and select
        m_wmod = rte.m_wmod;
        m_rmod = rte.m_rmod;
        for(unsigned int b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) m_select[b] = rte.m_select[b];
        m_interval = 0.0;
    }

    msk = selectSize();
    CDBG("msk = %d",msk);

    // interval counter selected
    if(m_select[AltiBusyCounter::INTERVAL]) {
        CDBG("INTERVAL selected","");
        itv = m_interval;
        for(s=0; s<rte.m_values; s++) {
            double dat = rte.m_data[(s+1)*msk-1];
            CDBG("s = %d, i = %d, m = %d => data = %7.3f%%, MIN = %7.3f%%, MAX = %7.3f%%",s,(s+1)*msk-1,msk-1,dat,m_dmin[msk-1],m_dmax[msk-1]);
            itv += dat;
            if(dat < m_dmin[msk-1]) m_dmin[msk-1] = dat;
            if(dat > m_dmax[msk-1]) m_dmax[msk-1] = dat;
        }
        CDBG("itv = %12.3f sec",itv);
        for(m=0; m<(msk-1); m++) {
            bsy = 0.0;
            for(s=0; s<rte.m_values; s++) {
                double dat = rte.m_data[s*msk+m];
                CDBG("s = %d, i = %d, m = %d => data = %7.3f%%, MIN = %7.3f%%, MAX = %7.3f%%",s,s*msk+m,m,dat,m_dmin[m],m_dmax[m]);
                bsy += dat * rte.m_data[(s+1)*msk-1];
                if(dat < m_dmin[m]) m_dmin[m] = dat;
                if(dat > m_dmax[m]) m_dmax[m] = dat;
            }
            m_data[m] = (bsy + m_data[m] * m_interval) / itv;
            CDBG("m = %d, bsy = %20.3f%% => rte = %9.5f%%",m,bsy,m_data[m]);
        }
        m_interval = itv;
    }

    // interval counter NOT selected
    else {
        CDBG("INTERVAL not selected","");
        itv = m_interval + static_cast<double>(rte.m_values) * rte.m_interval;
        CDBG("itv = %12.3f sec",itv);
        for(m=0; m<msk; m++) {
            bsy = 0.0;
            for(s=0; s<rte.m_values; s++) {
                double dat = rte.m_data[s*msk+m];
                CDBG("s = %d, i = %d, m = %d => data = %7.3f%%, MIN = %7.3f%%, MAX = %7.3f%%",s,s*msk+m,m,dat,m_dmin[m],m_dmax[m]);
                bsy += dat;
                if(dat < m_dmin[m]) m_dmin[m] = dat;
                if(dat > m_dmax[m]) m_dmax[m] = dat;
            }
            m_data[m] = (bsy * rte.m_interval + m_data[m] * m_interval) / itv;
            CDBG("m = %d, bsy = %20.3f%% => rte = %9.5f%%",m,bsy,m_data[m]);
        }
        m_interval = itv;
    }

    // update number of values
    m_values += rte.m_values;

    // mark average
    m_average = true;
}

//------------------------------------------------------------------------------

unsigned int AltiBusyCounterRate::selectSize() const {

    unsigned int msk(0);

    // count active select
    for(unsigned int b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) {
        if(m_select[b]) msk++;
    }

    return(msk);
}

//------------------------------------------------------------------------------

void AltiBusyCounterRate::selectDump() const {

    unsigned int b;
    std::ostringstream buf;

    for(b=0; b<AltiBusyCounter::BUSY_NUMBER; b++) {
        buf.str(""); buf << "BSY[\"" << AltiBusyCounter::BUSY_NAME[b] << "\"]";
        std::printf("%-16s: %s\n",buf.str().c_str(),m_select[b]?" \"ON\"":"\"OFF\"");
    }
}

//------------------------------------------------------------------------------

void AltiBusyCounterRate::dump() const {

    // dump average if necessary
    if(m_average) {
        dumpAverage();
        return;
    }

    unsigned int s, m, i;
    unsigned int msk = selectSize();
    static const int LINE = 123;
    double tim;

    // header <---------------------------------
    std::cout << AltiCommon::rightFillString("",LINE,'=') << std::endl;
    std::printf("ALTI BUSY counter rate - W/%s, R/%s ovfl = %s, values = %d\n",m_wmod?"AUTO":"MANU",m_rmod?"FIRST,":" LAST, ",m_overflow?"OVFL":"  no",m_values);
    std::cout << AltiCommon::rightFillString("",LINE,'-') << std::endl;
    if(!m_select[AltiBusyCounter::INTERVAL] && (m_interval <= 0.0)) {
        std::printf(">>>> ERROR: INTERVAL counter NOT selected and interval value NOT set!!!!\n");
        std::cout << AltiCommon::rightFillString("",LINE,'-') << std::endl;
        return;
    }
    if(!msk || !m_values) {
        std::printf(">>>> EMPTY COUNTER!!!!\n");
        std::cout << AltiCommon::rightFillString("",LINE,'-') << std::endl;
        return;
    }

    // header row
    std::cout << " NUM:  ACCU_TIME[s]";
    for(unsigned int b=0; b<(AltiBusyCounter::BUSY_NUMBER-1); b++) {
        if(m_select[b]) std::printf(" %12s",(AltiBusyCounter::BUSY_NAME[b]+"[%]").c_str());
    }
    std::cout << std::endl;

    // interval counter selected
    if(m_select[AltiBusyCounter::INTERVAL]) {
        tim = 0.0;
        for(s=0, i=0; s<m_values; s++) {
            tim += m_data[(s+1)*msk-1];
            std::printf("%4d: %13.9f ",s,tim);
            for(m=0; m<(msk-1); m++, i++) std::printf("  %9.5f  ",m_data[i]);
            std::cout << std::endl;
            i++;
        }
    }

    // interval counter NOT selected
    else {
        for(s=0, i=0; s<m_values; s++) {
            std::printf("%4d: %13.9f ",s,m_interval*(s+1));
            for(m=0; m<msk; m++, i++) std::printf("  %9.5f  ",m_data[i]);
            std::cout << std::endl;
        }
    }
}

//------------------------------------------------------------------------------

void AltiBusyCounterRate::dumpBusy() const {

    if(!m_average) {
        std::printf("NO AVERAGE !!!!\n");
        return;
    }

//    char s[AltiCommon::STRING_LENGTH];

    // R. Spiwoks, 10-JUN-2015, all counters must be selected !!!!
    // R. Spiwoks, 10-JUN-2015, all counters must be selected !!!!
    // R. Spiwoks, 10-JUN-2015, all counters must be selected !!!!

    std::cout << "   ============================================================================================" << std::endl;
    std::printf("   BUSY - W/%s, R/%s interval = %12s, ovfl = %s, values = %d\n",m_wmod?"AUTO":"MANU",m_rmod?"FIRST,":"LAST, ",getTime(m_interval/static_cast<double>(m_values)).c_str(),m_overflow?"OVFL":"  no",m_values);
    std::cout << "   ============================================================================================" << std::endl;
   // std::printf("   ON DEMAND               :        %8s\n",CTRL_NAME[m_busy_ondemand].c_str());
    std::printf("   CTP_IN                  :        %7.3f%%  (MIN=%7.3f%%, MAX=%7.3f%%)\n",m_data[AltiBusyCounter::CTP],m_dmin[AltiBusyCounter::CTP],m_dmax[AltiBusyCounter::CTP]);
    std::printf("   ALTI_IN                 :        %7.3f%%  (MIN=%7.3f%%, MAX=%7.3f%%)\n",m_data[AltiBusyCounter::ALTI],m_dmin[AltiBusyCounter::ALTI],m_dmax[AltiBusyCounter::ALTI]);
    std::printf("   FRONT PANEL             :        %7.3f%%  (MIN=%7.3f%%, MAX=%7.3f%%)\n",m_data[AltiBusyCounter::FP],m_dmin[AltiBusyCounter::FP],m_dmax[AltiBusyCounter::FP]);
    std::printf("   LOCAL                   :        %7.3f%%  (MIN=%7.3f%%, MAX=%7.3f%%)\n",m_data[AltiBusyCounter::LOCAL],m_dmin[AltiBusyCounter::LOCAL],m_dmax[AltiBusyCounter::LOCAL]);
    std::printf("   PAT GEN                 :        %7.3f%%  (MIN=%7.3f%%, MAX=%7.3f%%)\n",m_data[AltiBusyCounter::PG],m_dmin[AltiBusyCounter::PG],m_dmax[AltiBusyCounter::PG]);
    std::printf("   VME                     :        %7.3f%%  (MIN=%7.3f%%, MAX=%7.3f%%)\n",m_data[AltiBusyCounter::VME],m_dmin[AltiBusyCounter::VME],m_dmax[AltiBusyCounter::VME]);
	std::printf("   ECR                     :        %7.3f%%  (MIN=%7.3f%%, MAX=%7.3f%%)\n",m_data[AltiBusyCounter::ECR],m_dmin[AltiBusyCounter::ECR],m_dmax[AltiBusyCounter::ECR]);
    std::cout << "   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - " << std::endl;
}

//------------------------------------------------------------------------------

void AltiBusyCounterRate::dumpAverage() const {

    unsigned int b, m;
    unsigned int msk = selectSize();
    static const int LINE = 123;

    // header <-----------------------------------------------------------------
    std::cout << AltiCommon::rightFillString("",LINE,'=') << std::endl;
    std::printf("ALTI BUSY counter rate - W/%s, R/%s interval = %12s, ovfl = %s, values = %d\n",m_wmod?"AUTO":"MANU",m_rmod?"FIRST,":" LAST, ",getTime(m_interval/static_cast<double>(m_values)).c_str(),m_overflow?"OVFL":"  no",m_values);
    std::cout << AltiCommon::rightFillString("",LINE,'-') << std::endl;
    if(!m_select[AltiBusyCounter::INTERVAL] && (m_interval <= 0.0)) {
        std::printf(">>>> ERROR: INTERVAL counter NOT selected and interval value NOT set!!!!\n");
        std::cout << AltiCommon::rightFillString("",LINE,'-') << std::endl;
        return;
    }
    if(!msk || !m_values) {
        std::printf(">>>> EMPTY COUNTER!!!!\n");
        std::cout << AltiCommon::rightFillString("",LINE,'-') << std::endl;
        return;
    }

    // header row
    std::cout << " AVG:  ACCU_TIME[s]";
    for(b=0; b<(AltiBusyCounter::BUSY_NUMBER-1); b++) {
        if(m_select[b]) std::printf(" %12s",(AltiBusyCounter::BUSY_NAME[b]+"[%]").c_str());
    }
    std::cout << std::endl;

    // average row
    std::printf("      %13.9f ",m_interval);
    if(m_select[AltiBusyCounter::INTERVAL]) {                    // interval counter selected
        for(m=0; m<(msk-1); m++) std::printf("  %9.5f  ",m_data[m]);
    }
    else {                                                              // interval counter NOT selected
        for(m=0; m<msk; m++) std::printf("  %9.5f  ",m_data[m]);
    }
    std::cout << std::endl;
}

//------------------------------------------------------------------------------

std::string AltiBusyCounterRate::getTime(const double t) {

    char s[AltiCommon::STRING_LENGTH];

    if(t < 0.000001) {
        std::sprintf(s,"%7.3f nsec",t*1000000000.0);
    }
    else if(t < 0.001) {
        std::sprintf(s,"%7.3f usec",t*1000000.0);
    }
    else if(t < 1.000) {
        std::sprintf(s,"%7.3f msec",t*1000.0);
    }
    else {
        std::sprintf(s,"%7.3f  sec",t);
    }

    return(s);
}
