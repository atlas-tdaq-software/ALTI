#!/usr/bin/env python

from RCDVme import *
from ALTI import *

vme = VME.Open()
alti = ALTI(vme,0x08000000)

print("CNT BCID offset: %d"% alti.CNT_BcidOffset_Read()[1])
alti.CNT_BcidOffset_Write(123)
print("CNT BCID offset: %d"% alti.CNT_BcidOffset_Read()[1])

print("CNT control status: 0x%02x"% alti.CNT_Control_Read()[1])
ctl = ALTI_CNT_CONTROL_BITSTRING()
alti.CNT_Control_Read(ctl)
ctl._print()
