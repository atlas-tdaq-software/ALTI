#!/usr/bin/env python

import argparse
import random

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()

parser.add_argument("-i", "--input_file", help="Input LTP pattern file", type=str, default="pg_ltp.dat")
parser.add_argument("-o", "--output_file", help="Output ALTI pattern file", type=str, default="pg_alti.dat")

args = parser.parse_args()

file_in = open(args.input_file, 'r')
file_out = open(args.output_file, 'w')

# write ALTI header
file_out.write("#-----------------------\n")
file_out.write("#                      M\n")
file_out.write("#                      u\n")
file_out.write("#                      l\n")
file_out.write("#                      t\n")
file_out.write("#                      i\n")
file_out.write("#                      p\n")
file_out.write("#                      l\n")
file_out.write("#   CCC                i\n")
file_out.write("#  BRRR    T BBBB TTT  c\n")
file_out.write("#O UEEE    T GGGG TTTL i\n")
file_out.write("#R SQQQ    Y OOOO RRR1 t\n")
file_out.write("#B Y210    P 3210 321A y\n")
file_out.write("#-----------------------\n")

# convert LTP format to ALTI format
nb_lines = 0

for line in file_in:
    line = line.replace(' ', '')
    if(line[0]) == '#':
        continue
    else:
        busy    = line[0]
        inhib   = line[1]
        calreq2 = line[2]
        calreq1 = line[3]
        calreq0 = line[4]
        bgo3    = line[5]
        bgo2    = line[6]
        bgo1    = line[7]
        bgo0    = line[8]
        ttyp1   = line[9]
        ttyp0   = line[10]
        tt3     = line[11]
        tt2     = line[12]
        tt1     = line[13]
        l1a     = line[14]
        orb     = line[15]
        mult    = line[16:]

        file_out.write(' ' + orb +
                       ' ' + busy + calreq2 + calreq1 + calreq0 + 
                       ' 0x' + ttyp1 + ttyp0 +    
                       ' ' + bgo3 + bgo2 + bgo1 + bgo0 +
                       ' ' + tt3 + tt2 + tt1 + l1a + 
                       ' ' + mult )
        nb_lines += int(mult)
        
if(nb_lines%3564 != 0):
    nb_last = nb_lines%3564
    print("WARNING: there are " + str(nb_lines) + " BCIDs in the pattern, i.e. " + str(nb_last) + " BCIDs in the last orbit.") 
    print("         To have a periodic pattern, there should be 3564 BCIDs per orbit")

file_in.close()
file_out.close()
