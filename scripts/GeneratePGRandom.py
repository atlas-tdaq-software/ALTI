#!/usr/bin/env python

import argparse
from numpy.random import randint
import math
import ProgressBar

def parse_options():

    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    
    parser.add_argument("-o", "--output_file", help="Output ALTI pattern file", type=str, default="pg_alti.dat")
    parser.add_argument("-r", "--rate", help="random trigger rate [kHz]", type=float, default="1")
    
    args = parser.parse_args()
    
    return args
    
########################################################################################################

def fill_empty_orbit(file):
    file.write(" 1 0000 0x00 0000 0000 40\n")
    file.write(" 0 0000 0x00 0000 0000 3524\n")

########################################################################################################

def main():

    args = parse_options()
    file_out = open(args.output_file, 'w')
    
    # write ALTI header
    file_out.write("#-----------------------\n")
    file_out.write("#                      M\n")
    file_out.write("#                      u\n")
    file_out.write("#                      l\n")
    file_out.write("#                      t\n")
    file_out.write("#                      i\n")
    file_out.write("#                      p\n")
    file_out.write("#                      l\n")
    file_out.write("#   CCC                i\n")
    file_out.write("#  BRRR    T BBBB TTT  c\n")
    file_out.write("#O UEEE    T GGGG TTTL i\n")
    file_out.write("#R SQQQ    Y OOOO RRR1 t\n")
    file_out.write("#B Y210    P 3210 321A y\n")
    file_out.write("#-----------------------\n")
    
    # average number of L1A in 10000000 orbits
    nb_L1A = math.floor(1000000*args.rate/11.2455)
    nb_L1A_orbit = nb_L1A / 1000000
    # average number of lines per orbit
    nb_lines = 3 + 3*nb_L1A_orbit
    # number of orbits in the pattern (3 lines needed per orbit)
    nb_orbits = math.floor(1048575/nb_lines)
    # total number of L1A in the pattern
    nb_L1A_tot = int(math.ceil(nb_L1A_orbit * nb_orbits))
    # total number of bcids in the pattern
    nb_bcid = int(nb_orbits * 3564)
    L1A_bcid = randint(41, nb_bcid, nb_L1A_tot)
    L1A_bcid.sort()

    print("Generate random pattern at " + str(args.rate) + " kHz")
    print("nb orbits in the pattern = " + str(int(nb_orbits)))
    print("total number of L1A = " + str(nb_L1A_tot))
   
    args.progress_count = ProgressBar.progress(0, 100)    
    current_bcid = 1
    current_L1A = 0
    for L1A in L1A_bcid: 
        L1A_written = False
        while(L1A_written == False):
            nb_empty_bcid = L1A - current_bcid
            if(nb_empty_bcid < 0):
                break
            nb_remaining_bcid_orbit = 3564 - (current_bcid%3564) + 1
            if(nb_empty_bcid > nb_remaining_bcid_orbit):
                if(current_bcid%3564 == 1):
                    fill_empty_orbit(file_out)
                    current_bcid += 3564
                else:
                    # fill the rest of the orbit with empty bcid
                    file_out.write(" 0 0000 0x00 0000 0000 " + str(nb_remaining_bcid_orbit) + "\n")
                    current_bcid += nb_remaining_bcid_orbit
            else:
                if (current_bcid%3564 == 1):
                    # orbit signal 
                    file_out.write(" 1 0000 0x00 0000 0000 40\n")
                    current_bcid += 40
                if ( (L1A - current_bcid) > 0):
                    # skip L1A in the orbit signal
                    file_out.write(" 0 0000 0x00 0000 0000 " + str(nb_empty_bcid) + "\n")
                    file_out.write(" 0 0000 0x81 0000 0001 1\n")
                    file_out.write(" 0 0000 0x81 0000 0000 4\n")
                    current_bcid += (nb_empty_bcid + 5) 
                L1A_written = True
                current_L1A += 1
                args.progress_count = ProgressBar.progress(100 * current_L1A/nb_L1A_tot, 100)    

    #  # fill the rest of the orbit with empty bcid
    nb_remaining_bcid = 3564 - (current_bcid%3564) + 1 - 3
    file_out.write(" 0 0000 0x00 0000 0000 " + str(nb_remaining_bcid) + "\n")
    args.progress_count = ProgressBar.progress(100, 100)
    print('')

if __name__ == "__main__":
    main()
