<!ELEMENT module (bitstring*, (block | register | memory | fifo)+)>
<!ATTLIST module    name CDATA #REQUIRED
                    addr CDATA #REQUIRED
                    size CDATA #REQUIRED
                    amod ( A16 | A24 | A32 | CRCSR | USER1 | USER2 ) #IMPLIED
                    type CDATA #IMPLIED
                    desc CDATA #IMPLIED>

<!ELEMENT block ((block | register | memory | fifo)+)>
<!ATTLIST block     name CDATA #REQUIRED
                    addr CDATA #IMPLIED
                    type CDATA #IMPLIED
                    modf ( R | W | RW ) #IMPLIED
                    multiple CDATA #IMPLIED
                    offset   CDATA #IMPLIED>

<!ELEMENT register (bitstring | field*)>
<!ATTLIST register  name CDATA #REQUIRED
                    addr CDATA #REQUIRED
                    type ( D32 | D16 | D08 ) #IMPLIED
                    mask CDATA #IMPLIED
                    modf ( R | W | RW ) #IMPLIED
                    multiple CDATA #IMPLIED
                    offset   CDATA #IMPLIED
                    bitstring IDREF #IMPLIED>

<!ELEMENT memory (bitstring | field*)>
<!ATTLIST memory    name CDATA #REQUIRED
                    addr CDATA #REQUIRED
                    size CDATA #REQUIRED
                    type ( D32 | D16 | D08 ) #IMPLIED
                    mask CDATA #IMPLIED
                    modf ( R | W | RW ) #IMPLIED 
                    dma (YES | Y | NO | N) #IMPLIED
                    multiple CDATA #IMPLIED
                    offset   CDATA #IMPLIED
                    bitstring IDREF #IMPLIED>

<!ELEMENT fifo (bitstring | field*)>
<!ATTLIST fifo      name CDATA #REQUIRED
                    addr CDATA #REQUIRED
                    size CDATA #REQUIRED
                    type ( D32 | D16 | D08 ) #IMPLIED
                    mask CDATA #IMPLIED
                    modf ( R | W | RW ) #IMPLIED
                    interface ( VECTOR | BLOCK | BOTH ) #IMPLIED
                    multiple CDATA #IMPLIED
                    offset   CDATA #IMPLIED
                    bitstring IDREF #IMPLIED>

<!ELEMENT bitstring (field+)>
<!ATTLIST bitstring name ID #REQUIRED
                    modf ( R | W | RW ) #IMPLIED>

<!ELEMENT field (value*)>
<!ATTLIST field     name CDATA #REQUIRED
                    mask CDATA #REQUIRED
                    form ( STRING | S | NUMBER | N | BOOLEAN | B ) #IMPLIED
                    modf ( R | W | RW ) #IMPLIED
                    interface ( BLOCK | VECTOR | BOTH ) #IMPLIED
                    multiple CDATA #IMPLIED
                    offset   CDATA #IMPLIED>

<!ELEMENT value EMPTY>
<!ATTLIST value     name CDATA #REQUIRED
                    data CDATA #REQUIRED>
